<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>

<!-- JSTL taglibs -->
<%@ taglib prefix="core" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="frmt" uri="http://java.sun.com/jstl/fmt" %>

<!-- Struts taglibs -->
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>
		<core:set var="title"><tiles:getAsString name="title"/></core:set>
		<core:if test="${not empty title}">
			<bean:message key="${title}"  bundle="messages"/>
		</core:if>
		<core:if test="${empty title}"> </core:if>

	</title>

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	
	<!-- CSS Resources -->
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/ext-all.css"/>"/>

	<!-- <link rel="stylesheet" type="text/css" href="<core:url value="/css/xtheme-gray.css"/>"/> -->
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/iconos.css"/>"/>
	
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/commons.css"/>"/>
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/user.css"/>"/>
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/file-upload.css"/>"/>
	
	<!-- JS Resources -->
	<script type="text/javascript" src="<core:url value="/js/tera-base.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-all-debug.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-miframe.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-lang-es.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-file-upload-field.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/mootools-1.2.1-core-yc.js"/>"></script>
</head>
 
<script type="text/javascript">
var configObject = {
		entity: '',
		MenuDataUrl:	'../app/menu.do?method=loadMenu'
	};
	

//La barra de tabs, del cuerpo
var tabs = new Tera.TabPanel({
            region:'center',
	        margins:'35 5 5 0',
    	    cmargins:'35 5 5 5',
            deferredRender:false,
            activeTab:0,
            autoDestroy:true,
            enableTabScroll:true,
           // defaults: {preventBodyReset: true},
            items:[{
            	id:'tabInicio',
                contentEl:'center1',
                title: 'Inicio',
                closable:false,
                autoScroll:true
            }]
});

//Arbol para el menu
var menuIzq=new Tera.tree.TreePanel({
	id:'im-tree',
	title: 'Menu',
	loader: new Tera.tree.TreeLoader(),
	rootVisible:false,
	lines:false,
	autoScroll:true,
	iconCls:'nav',
	root: new Tera.tree.AsyncTreeNode({
	    text:'Online',
	    children:[{
	        text:'Gesti&oacute;',
	        id: 'raiz',
	        expanded:true,
	        children:[
	        {
	            text:'Provincias',
	            name:'../pages/contents/provincia/contentProvincia',
	            iconCls:'app_warning',
	            leaf:true,
	            listeners :{
	            	click: function(){('pages/contents/provincia/contentProvincia', 'Provincia')}
	            }
	        },
	        
	        {
	        	
	            text:'Profesionales',
	            name:'../pages/contents/profesional/contentProfesional',  
	            iconCls:'app_warning',
	            leaf:true,
	             listeners :{
	            	click: function(){('pages/contents/profesional/profesional','Profesionales')}
	            }
	        }
			,
	        
	        {
	        	
	            text:'Tipo de Contacto',
	            name:'../pages/contents/contacto/ContactotipoAdmin',  
	            iconCls:'app_warning',
	            leaf:true,
	             listeners :{
	            	click: function(){('../pages/contents/contacto/ContactotipoAdmin','Tipo de Contacto')}
	            }
	        }
	 
	        
	        
	     ]
	    }  
	    
	    ]
	})
});
	
Tera.onReady(function(){

	
	
	
//Tera.state.Manager.setProvider(new Tera.state.CookieProvider());

viewport = new Tera.Viewport({
     layout:'border',
     items:[{
         region:'west',
         id:'west-panel',
         title:'Menu',
         split:true,
         width: 200,
         minSize: 175,
         maxSize: 400,
         collapsible: true,
         margins:'35 0 5 5',
         cmargins:'35 5 5 5',
         layout:'accordion',
         layoutConfig:{
             animate:true
         },
         items: [
	     menuIzq
         ]
     },tabs]
	});



	//Para mostrar los avisos de validaciÃ³n
	Tera.QuickTips.init();
	// turn on validation errors beside the field globally
    Tera.form.Field.prototype.msgTarget = 'side';
    Tera.BLANK_IMAGE_URL = '../img/default/s.gif';

    });







  	
   	menuIzq.addListener('click', function(nodo, e){
       
   		var id = nodo.id;
        var tab = tabs.getItem(id);
       if(nodo.id!='raiz'){
        if(!tab){
              tab = tabs.add({
                    contentEl: 'center1',
                    id: nodo.id,
                    title: nodo.text,
                    iconCls: 'tabs',
                    closable:true,
                    autoScroll:true,
                    autoLoad: {url: nodo.attributes.name+'.jsp', discardUrl:false, nocache:true, text:'Cargando...', timeout:60, scope:this, loadScripts:true, scripts:true}
              });
        }
        tabs.setActiveTab(tab);
       }
   

  	
  	
	function cerrarTabs(){
		tabs.items.each(function(tab){
			if(tab.id!='tabInicio')
				tabs.remove(tab);
		});

	}

    }); 

	</script>
<body class="x-window-mc">
<div> 
	
</div>
  <div id="north" class="x-panel-header" style="margin:0px 5px 0px 5px; padding: 2px 2px 2px 10px; ">
  	<div style="float:left; width: 500px; font-size:18px; line-height:22px; ">
	    Fadeuda
	</div>
 
  </div>

 


  <div id="center1" style="padding:10px">
  
  </div>


<div id="msg" ></div>
</html>