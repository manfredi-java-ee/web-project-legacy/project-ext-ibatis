<%@ page language="java" contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>

<!-- JSTL taglibs -->
<%@ taglib prefix="core" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="frmt" uri="http://java.sun.com/jstl/fmt" %>

<!-- Struts taglibs -->
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>
		<core:set var="title"><tiles:getAsString name="title"/></core:set>
		<core:if test="${not empty title}">
			<bean:message key="${title}"  bundle="messages"/>
		</core:if>
		<core:if test="${empty title}"> </core:if>

	</title>

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	
	<!-- CSS Resources -->
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/tera-all.css"/>"/>
	<!-- <link rel="stylesheet" type="text/css" href="<core:url value="/css/xtheme-gray.css"/>"/> -->
	
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/commons.css"/>"/>
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/menu.css"/>"/>
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/user.css"/>"/>
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/file-upload.css"/>"/>
	
	<!-- JS Resources -->
	<script type="text/javascript" src="<core:url value="/js/tera-base.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-all-debug.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-miframe.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-lang-es.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-file-upload-field.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/mootools-1.2.1-core-yc.js"/>"></script>
</head>
<body>
<!--  
	<table border="0" width="100%" cellspacing="0" cellpadding="0" height="100%">
		<tr>
			<td width="200" valign="top">
				<div class="logo">
					<html:img page="/images/telefonica.png" alt="Logo" height="100px"/>
				</div>
	
				<!-- Menu -->
				<tiles:insert name="leftBar" attribute="leftBar"/>	
				<!-- Menu -->
				
			</td>
			<td valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0" height="100%">
				<tr>
					<td class="userContainer" valign="top">
	
					<!-- Usuario -->
					<tiles:insert name="topBar" attribute="topBar"/>					
					<!-- Usuario -->
					
					</td>
				</tr>
				<tr>
					<td valign="top" style="padding-left: 5px">

					<!-- Page Container -->
					<table cellpadding="0" cellspacing="0" style="width: 100%">
						<tr>
							<td style="width: 580px; height: 19px;">
								<span class="arial18blanco">
									<core:set var="pageTitle"><tiles:getAsString name="pageTitle"/></core:set>
									<core:if test="${not empty pageTitle}">
										<bean:message key="${pageTitle}"  bundle="messages"/>
									</core:if>
									<core:if test="${empty pageTitle}">&nbsp;</core:if>
								</span>
								<span class="esquinero_relative">
									<div class="foto_chica_absolute">
										<html:img page="/images/user-256.png" height="100px" />
									</div>
									<div class="esquinero_absolute">
										<html:img page="/images/esquinero.gif"/>
									</div>
								</span>
							</td>
						</tr>
						<tr>
							<td style="padding-top: 5px">
							<table border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><html:img page="/images/tab_1.gif"/></td>
																									
									<td class="titulo_listado_cnt">
										<div class="titulo_listado">
											<core:set var="tabPagetitle"><tiles:getAsString name="tabPagetitle"/></core:set>
											
											<core:if test="${not empty tabPagetitle}">
												<bean:message key="${tabPagetitle}"  bundle="messages"/>
											</core:if>
											<core:if test="${empty tabPagetitle}"><span>&nbsp;</span></core:if>
										</div>
									</td>
									<td><html:img page="/images/tab_3.gif"/></td>
								</tr>
							</table>
							<table cellpadding="0" cellspacing="0" class="contenedor">
								<tr>
									<td valign="top">
					
										<div id="bloque1">
											<table cellpadding="0" cellspacing="0" style="width: 100%; height: 140px">
												<tr>
													<td valign="top" style="padding: 4px;">
													
													<!-- Container -->
													<tiles:insert name="container" attribute="container"/>					
													<!-- Container -->
													
													</td>

												</tr>
											</table>
										</div>
										
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					<!-- Page Container -->
					
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
-->
</body>
</html>