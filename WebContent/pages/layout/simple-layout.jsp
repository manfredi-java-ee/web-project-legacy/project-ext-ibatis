<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<!-- JSTL taglibs -->
<%@ taglib prefix="core" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="frmt" uri="http://java.sun.com/jstl/fmt" %>

<!-- Struts taglibs -->
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>
		<core:set var="title"><tiles:getAsString name="title"/></core:set>
		<core:if test="${not empty title}">
			<bean:message key="${title}"  bundle="messages"/>
		</core:if>
		<core:if test="${empty title}"> </core:if>
	
	</title>

	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	
	<!-- CSS Resources -->
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/tera-all.css"/>"/>
	<link rel="stylesheet" type="text/css" href="<core:url value="/css/commons.css"/>">
	
	<!-- JS Resources -->
	<script type="text/javascript" src="<core:url value="/js/tera-base.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-all-debug.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/tera-lang-es.js"/>"></script>
	<script type="text/javascript" src="<core:url value="/js/mootools-1.2.1-core-yc.js"/>"></script>
</head>
<body>
	<tiles:insert name="body" attribute="body"/>
</body>
</html>