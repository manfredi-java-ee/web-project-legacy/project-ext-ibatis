<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>

<%@ taglib prefix="tiles" uri="http://struts.apache.org/tags-tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>

<!-- CSS Resources -->
<!-- JS Resources -->
	
<table cellpadding="0" cellspacing="0" style="width: 100%">
	<tr>
		<td style="height: 30px; width: 580px;"><span class="verdana22"><tiles:insert attribute="pageTitle" name="pageTitle"/></span></td>
	</tr>
	<tr>
		<td style="padding-top: 5px">
		<!-- Pestania de listados -->
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="../images/tab_1.gif"></td>
				<td class="titulo_listado_cnt">
					<div class="titulo_listado"><span class="verdana22"><tiles:insert attribute="tabPagetitle" name="tabPagetitle"/></span></div>
				</td>
				<td><img src="../images/tab_3.gif"></td>
			</tr>
		</table>
		<!-- Pestania de listados -->
		<table cellpadding="0" cellspacing="0" class="contenedor">
			<tr>
				<td valign="top">

					<div id="bloque1">
						<table cellpadding="0" cellspacing="0" style="width: 100%; height: 140px">
							<tr>
								<td valign="top" style="padding: 4px;">
								<!-- Container -->
								<tiles:insert attribute="container" name="container"/>					
								<!-- Container -->
								</td>
								<td style="width: 42px" valign="top">
									<!-- Esquinero / Logo Chica -->
									<div class="esquinero_relative">
										<div class="foto_chica_absolute">
											<img src="../images/foto_chica.gif">
										</div>
										<div class="esquinero_absolute">
											<img alt="Esquina" src="../images/pico_tabla.jpg">
										</div>
									</div>
									<!-- Esquinero / Logo Chica -->
								</td>
							</tr>
						</table>
					</div>
					
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>