<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>






<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelProvincia',
		panelId: 'panelProvincia',
		addUrl:			'./app/provincia.do?method=altaProvincia',
		modifyUrl:		'./app/provincia.do?method=modificarProvincia',
		removeUrl: 		'./app/provincia.do?method=bajaProvincia',
		loadDataUrl:	'./app/provincia.do?method=loadData',
		filterDataUrl:	'./app/provincia.do?method=buscar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: './app/provincia.do?method=buscar',
			pagingToolbarUrl: 		'./app/provincia.do?method=loadData',
			exportToolbarUrl: './app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			idprovinciaName: 'idprovincia'
		}
	};
	

//DEFINICION DE STORES 
  	
	    var store = new Ext.data.JsonStore({
	    	remoteSort: true,
	    	 
	    	baseParams:{
	   			idprovinciaFilter: ''
				,
	   			nombreFilter: ''
				,
	   			idpaisFilter: ''
	    	},	
	        url: configObject.loadDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codProvincia',
	        fields: [
	        	'idprovincia'
				,
	        	'nombre'
				,
	        	'idpais'
	        ]
	    });

//DEFINICION DE STORES 	



///// INICIO - FORMPANELBUSQUEDA 
Ext.onReady(function(){
	
	//Para mostrar los avisos de validaciÃ³n
	Ext.QuickTips.init();
	// turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
   

  	Ext.BLANK_IMAGE_URL = './img/default/s.gif';
 
  	 var PanelDescripcion = new Ext.Panel({ 
         id:'IdPanelDescripcion', 
         height: 60,
         minWidth: 150, 
         border: false, 
         renderTo: 'paneldescripcionProvincia',
         frame:true,
         
        	 items: [{
        		
        		 //iconCls : 'information',
                 html: "<p>Permite gestionar las provincias.</p>",
                 xtype: "panel"
             } ]
     });
  	 
  	 
  	 
  	 
	//var filtroBusqueda = new Ext.form.FormPanel({
		var filtroBusqueda = new Ext.form.FormPanel({
		
		    title:'Panel de Busqueda',
		    renderTo: configObject.renderTo,
		     
	        bodyBorder: false,
	        border: false,
	       // bodyStyle: 'padding:15px;background:transparent', 
	        bodyStyle: 'padding:15px ',
  	        buttonAlign: 'left',
	        //width: 550,
	        labelWidth: 50,
	        labelAlign: 'left',
	        layout: 'form',
	        height: 200, 
	        monitorValid:true,
	        //clientValidation: true,
			//validationEvent:true,
		    //baseCls: 'x-plain',
		    //baseCls:"x-panel",
	        items: [
 			 
				{
	             xtype: 'textfield', 
				 width: 98,
				 fieldLabel: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>',
				 name: 'nombreFilter',
				 id: 'nombreFilter',
				
				 height: 21,
				 maxLength: 21,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
			     allowBlank: false,
			     value:''            //Set the value so that the validation will trigger
			    }
			    ,
				{
			    xtype: 'textfield', 
				 width: 98,
				 fieldLabel: '<bean:message key="provincia.idpais_corto"  bundle="fields"/>',
				 name: 'idpaisFilter',
				 id: 'idpaisFilter',
				 height: 21,
				 maxLength: 21,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
				 allowBlank: false,
				 value:''            //Set the value so that the validation will trigger
				}
           			 
			 
	 		 ],
	 		 
	 		 
	    buttons: [{
	        text:'Filtrar',
	        id: 'btnfiltrar',
	        type: 'submit',
	        //iconCls: './resources-ext/images/default/icons/Zoom.gif',
		    iconCls: 'zoom',
	        formBind:true ,   //If validation fails disable the button
	        value:'' ,           //Set the value so that the validation 
	       
	        	handler: function (){
 	        		filtrarGrilla(filtroBusqueda);
				
	    		}
	      },{
	          text: 'Limpiar',
	          id: 'btnlimpiar',
	          handler: function(){
	        	  filtroBusqueda.getForm().reset();
	          }
		}]
		 
	    });
		
		 
		

		
///// FIN- FORMPANELBUSQUEDA  

///// INICIO - PANELGRILLA  

		var bbar1 = new Ext.PagingToolbar({
			 
		     pageSize: 25,
		     store: store,
	         displayInfo: true,
             displayMsg: 'Mostrando   {0} - {1} de {2}',
             emptyMsg: 'No existen registros a mostrar.'

		});


	    var cmProvincia = new Ext.grid.ColumnModel([
		    new Ext.grid.RowNumberer(),
		    {id: 'idprovincia', header: '<bean:message key="provincia.idprovincia_corto"  bundle="fields"/>', dataIndex: 'idprovincia', width: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>'.length * 10, align:'center'},
		    {id: 'nombre', header: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>', dataIndex: 'nombre', width: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>'.length * 10, align:'center'},
		    {id: 'idpais', header: '<bean:message key="provincia.idpais_corto"  bundle="fields"/>', dataIndex: 'idpais', width: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>'.length * 10, align:'center'}
			   
	    ]);
	    
	    cmProvincia.defaultSortable = true;
  
	    
  var gridProvincia = new Ext.grid.GridPanel({
      width: 500,
      height: 245,
      store: store,
      cm: cmProvincia,
     // renderTo: 'panelProvincia',
           trackMouseOver: true,
      sm: new Ext.grid.RowSelectionModel( { singleSelect: true } ),
      stripeRows: true,
      viewConfig: {
			forceFit: false,
          enableRowBody: false
      },
         bbar: bbar1
  });
		
	function changeNumber(val){
   	var original=parseFloat(val);
		var result = original.toFixed(3);
   	if(val < 0){
	           	return '<span style="color:red;">' + result + '</span>';
   	}

   	return result;
	}


	gridProvincia.addListener('click', function(param) {
		if(gridProvincia.getSelectionModel().hasSelection()){
  
   	} else{
    
   	}
   });
			
///// FIN - PANELGRILLA  	

  	 var PanelGrilla= new Ext.Panel({ 
         id:'IdPanelGrilla', 
         //height: 60,
         minWidth: 150, 
         border: false, 
         bodyStyle: 'padding:15px ',
         renderTo: configObject.renderTo,
        // frame:true,
         
        	 items: [ gridProvincia ]
     });




	function filtrarGrilla(form){
		if(!form.getForm().isValid())
			return;

		form.getForm().submit({
			waitMsg: '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
			url: configObject.filterDataUrl,
			success: function(response, options){
				var responseText = Ext.decode(options.response.responseText);
				if (responseText.data != 'undefined' || responseText.data != null){
					gridProvincia.getStore().loadData(responseText);
				}
			
			},
			failure: function(response, options){
				var responseText = Ext.decode(options.response.responseText);
				Ext.MessageBox.alert('<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>', responseText.failure); 
			}
		});
	}

 
 

});// Fin OnReadyFunction

 
</script>
<div id="paneldescripcionProvincia"></div>
<div id="panelProvincia"></div>  
<div id="GrillaPrincipal" style="width: 80%; height: 90%;"></div>