<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>


<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelProvincia',
		panelId: 'panelProvincia',
		addUrl:			'./app/provincia.do?method=altaProvincia',
		modifyUrl:		'./app/provincia.do?method=modificarProvincia',
		removeUrl: 		'./app/provincia.do?method=bajaProvincia',
		loadDataUrl:	'./app/provincia.do?method=loadData',
		filterDataUrl:	'./app/provincia.do?method=buscar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: './app/provincia.do?method=buscar',
			pagingToolbarUrl: 		'./app/provincia.do?method=loadData',
			exportToolbarUrl: './app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			idprovinciaName: 'idprovincia'
		}
	};
 
	 	  
	 // invalid markers to sides 
	 Ext.form.Field.prototype.msgTarget = 'side'; 

			
		
		var filtroBusqueda = new Ext.form.FormPanel({
// 			mappedFields: [ 
// 				'idprovinciaFilter'
// 				,
// 				'nombreFilter'
// 				,
// 				'idpaisFilter'
// 			],
			filterConfig: filterConfig,
	        bodyBorder: false,
	        border: false,
	        bodyStyle: 'padding:10px',
	        buttonAlign: 'right',
	        width: 550,
	        baseCls: 'x-plain',
	        labelWidth: 130,
	        labelAlign: 'top',
	        height: 230,
	        items: [
 			 
				{
	             xtype: 'textfield', 
				 width: 98,
				 fieldLabel: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>',
				 name: 'nombreFilter',
				 id: 'nombreFilter',
				 height: 21,
				 maxLength: 21,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres'
			    }
			    ,
				{
				 width: 98,
				 fieldLabel: '<bean:message key="provincia.idpais_corto"  bundle="fields"/>',
				 name: 'idpaisFilter',
				 id: 'idpaisFilter',
				 height: 21,
				 maxLength: 21,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres'
			    }
	 		 ]
	    });
	    var store = new Ext.data.JsonStore({
	    	remoteSort: true,
	    	baseParams:{
	   			idprovinciaFilter: ''
				,
	   			nombreFilter: ''
				,
	   			idpaisFilter: ''
	    	},	
	        url: configObject.loadDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codProvincia',
	        fields: [
	        	'idprovincia'
				,
	        	'nombre'
				,
	        	'idpais'
	        ]
	    });
	
	 	function changeNumber(val){
        	var original=parseFloat(val);
			var result = original.toFixed(3);
        	if(val < 0){
 	           	return '<span style="color:red;">' + result + '</span>';
        	}
    
        	return result;
    	}
	
	    var cmProvincia = new Ext.grid.ColumnModel([
		    new Ext.grid.RowNumberer(),
		    {id: 'nombre', header: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>', dataIndex: 'nombre', width: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
	    ]);
	    cmProvincia.defaultSortable = true;
	
	
	
			var bbar1 = new Ext.PagingToolbar({
	        	filterPanel: filtroBusqueda,
	        	exportFile: false,
	                exportGridName: 'Provincia',
	                pageSize: 10,
	                store: store,
	                displayInfo: false,
	                displayMsg: 'Mostrando   {0} - {1} de {2}',
	                emptyMsg: 'No existen registros a mostrar.'
	
			});
	
	
	
	
	    var gridProvincia = new Ext.grid.GridPanel({
	        width: 625,
	        height: 245,
	        store: store,
	        cm: cmProvincia,
	        trackMouseOver: true,
	        sm: new Ext.grid.RowSelectionModel( { singleSelect: true } ),
	        stripeRows: true,
	        viewConfig: {
    			forceFit: false,
	            enableRowBody: false
	        },
	           bbar: bbar1
	    });
	    
	    	    
	    

		gridProvincia.addListener('click', function(param) {
			if(gridProvincia.getSelectionModel().hasSelection()){
	   			buttonBaja.setDisabled(false);
	   			buttonModificar.setDisabled(false); 
	   			buttonVisualizar.setDisabled(false);
	    	} else{
	    		buttonBaja.setDisabled(true);
	   			buttonModificar.setDisabled(true);  
	   			buttonVisualizar.setDisabled(true);
	    	}
	    });

//store.load({params: {start: 0,limit: 10}}); 
	    
	    var buttonAlta = new Ext.Button({text: '<bean:message key="app.label.alta"  bundle="messages"/>', minWidth: 80});
	    var buttonBaja = new Ext.Button({text: '<bean:message key="app.label.baja"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonModificar = new Ext.Button({text: '<bean:message key="app.label.modificar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonVisualizar = new Ext.Button({text: '<bean:message key="app.label.visualizar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    	    
	    var buttonFiltrar = new Ext.Button({
	        text: 'Filtrar',
			minWidth: 80,
			handler: FiltrarSubmit
		});
		
			
		
		    function FiltrarSubmit() {
				
					// Si alg�n campo tiene alg�n error 
			if( filtroBusqueda.getForm().isValid()==false)
				{
						
			Ext.Msg
			.show({
				title : 'Informaci�n', //<- el t�tulo del di�logo   
				msg : 'Verifique la longitud de los campos que est�n se�alizados en color rojo.', //<- El mensaje   
				buttons : Ext.Msg.OK, //<- Botones de SI y NO   
				icon : Ext.Msg.INFO, // <- un �cono de error   
				fn : this.callback
			//<- la funci�n que se ejecuta cuando se da clic   
			});
			
	
				}
				
	    
				filtroBusqueda.getForm().submit({
					waitMsg: '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
					url: configObject.filterDataUrl,
					success: function(response, options){
						var responseText = Ext.decode(options.response.responseText);
						if (responseText.data != 'undefined' || responseText.data != null){
							gridProvincia.getStore().loadData(responseText);
				   			 store.baseParams.idprovinciaFilter = Ext.getCmp('idprovinciaFilter').getValue();
				   			 store.baseParams.nombreFilter = Ext.getCmp('nombreFilter').getValue();
				   			 store.baseParams.idpaisFilter = Ext.getCmp('idpaisFilter').getValue();
						}
					
					},
					failure: function(response, options){
						var responseText = Ext.decode(options.response.responseText);
						Ext.MessageBox.alert('<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>', responseText.failure); 
					}
				});
			}
		
		
		
		
		
		
		
		
		
	 // Funci�n para que cuando aprete enter realice la misma accion que el boton filtrar

		var map = new Ext.KeyMap(Ext.getBody(), {
		    key: Ext.EventObject.ENTER,
		    fn: function(){
		        FiltrarSubmit();
		    },
		    scope: this
		});		
		
		
		
		
		
	    
	    var buttonLimpiar = new Ext.Button({text: '<bean:message key="app.label.limpiar"  bundle="messages"/>',
			minWidth: 80,
			handler: function(){
		   		store.baseParams.idprovinciaFilter = '';
		   		store.baseParams.nombreFilter = '';
		   		store.baseParams.idpaisFilter = '';
		
		
			//reseteo los campos TextField del formulario 
			
			filtroBusqueda.getForm().reset();
		 
					
			//Blanqueo el label de resultado
		 	
		 	labelresultado.setText(' '); 
		
			filtroBusqueda.clearForm();
	    		gridProvincia.getStore().load();
			
			//Reseteo la barra del grid PagingToolbar
	  			
	  		    bbar1.field.dom.value = '1';
			    bbar1.afterTextEl.el.innerHTML = 'de 1'; 
			    bbar1.store.removeAll();
			    bbar1.first.setDisabled(true);
			    bbar1.prev.setDisabled(true);
			    bbar1.next.setDisabled(true);
			    bbar1.last.setDisabled(true); 
			 
			    bbar1.updateInfo();
			
		
		
		
			}
		});
	    
	    
		Ext.onReady(function(){

		 	
		 	Ext.BLANK_IMAGE_URL = './img/default/s.gif';
		 	Ext.QuickTips.init();
		 	
		 	
		var panel = new Ext.Panel({
	    	id: configObject.panelId,
	    	renderTo: configObject.renderTo,
	    	border: false,
	        defaults: {
	        	border: false
		    },
		    items:[{
	    	    border: false,
	    	    defaults: {
		        	border: false
			    },	    	    
	    	    items:[{
	    	    	layout:"column",
	    	    	border: false,
	    	    	defaults: {
	    	        	border: false
	    		    },
			        items:[
			        {
						columnWidth: 1,
					    items : [filtroBusqueda]
					},{
						width: 113,
						items: [{
					        border: false,
					    	defaults: {
					    		border: false
					    	},
							items: [
								{ items: [buttonFiltrar], style: 'margin-top: 10px;' },
								{ items: [buttonLimpiar], style: 'margin-top: 3px;' }
							]
						}]
					}]
				}]
	        },
		{
			layout : "fit",
			border : false,
			style : 'padding: 0px 5px 0px 5px;' 
		}	 
	 
		 
		,
	        	        
	        {
	        	layout:"fit",
	    	    border: false,	            
				style: 'padding: 0px 5px 0px 5px;',
	            items: [ gridProvincia ]
	        },{
	        	layout:"fit",
	    	    border: false,
				items:[{
				    layout:"column",
				    style: 'margin-top: 8px;',
				    defaults: {
				    	border: false
					},
					border: false,
				    items:[{
				        width:85,
				        items:[buttonAlta]
				      },{
				        width:85,
				        items:[buttonBaja]
				      },{
				        width:85,
				        items:[buttonModificar]
				      },{
				        width:85,
				        items:[buttonVisualizar]
				      }]
				  }]
			}]
	    });
			
   buttonAlta.on('click', function(){
	  
			var aceptar = new Ext.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				disabled: false, 
				handler: function() {
			    	Ext.MessageBox.confirm('<bean:message key="app.label.confirmar"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_a"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
								form.getForm().submit({
				                	url: configObject.addUrl,
				                	success: function(response, options) {
				                		win.close();
			                			buttonFiltrar.handler.call(buttonFiltrar.scope);
				                	},
				                	failure: function(response, options) {
				                		var responseText = Ext.decode(options.response.responseText);
				                		Ext.MessageBox.alert('<bean:message key="app.validaciones.errorAlta"  bundle="messages"/>', responseText.failure);
			                	}
		                    });
					    }
					});
				}
			});
				
			var cancelar = new Ext.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80, handler: function() { win.close(); }});

			var form = new Ext.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
					{
					 width: 98,
					 fieldLabel: '<bean:message key="provincia.idprovincia_corto"  bundle="fields"/>',
					 name: 'idprovincia',
					 id: 'idprovincia',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
		             xtype: 'textfield',
					 width: 20,
					 fieldLabel: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>',
					 name: 'nombre',
					 id: 'nombre',
					 maxLength: 1,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
					 width: 98,
					 fieldLabel: '<bean:message key="provincia.idpais_corto"  bundle="fields"/>',
					 name: 'idpais',
					 id: 'idpais',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				 ]         
			});
			
			var win = new Ext.Window({
		    	title: '<bean:message key="app.label.alta"  bundle="messages"/>',
		        border: false,
				modal: true,
				width: 288 + 50,
				height: 175 + 15,
				resizable: false,
				layout: 'fit',
				resizable: false,
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				items: form,
				buttons: [aceptar, cancelar]
			});
			win.show(this);
		});
	    
  buttonModificar.on('click', function(){
		    	if (!gridProvincia.getSelectionModel().hasSelection()) {
		    		return false;
		    	}
		    	var aceptar = new Ext.Button({
					text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
					minWidth: 80,
					disabled: false, 
					handler: function() {
				    	Ext.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_m"  bundle="messages"/>', function(btn) {
				    		if ('yes' == btn) {
				    			form.getForm().submit({
									url: configObject.modifyUrl,
									success: function(response, options) {
										win.close();
			                			buttonFiltrar.handler.call(buttonFiltrar.scope);
									},
									failure: function(response, options) {
										var responseText = Ext.decode(options.response.responseText);
										Ext.MessageBox.alert('<bean:message key="app.validaciones.errorModificacion"  bundle="messages"/>', responseText.failure);
									}
								});
						    }
						});
					}
				});

		    	var cancelar = new Ext.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			    	
		 	    	
	    	    var recordToModify = gridProvincia.getSelectionModel().getSelected().data;

	 	        var form = new Ext.form.FormPanel({
	 				baseCls: 'x-plain',
	 				labelWidth: 130,
	 				monitorValid: true,
	 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
	 				items: [
 					    {
  	 				     width: 98,
	 				     fieldLabel: '<bean:message key="provincia.idprovincia_corto"  bundle="fields"/>',
	 				     name: 'idprovincia',
	 				     id: 'idprovincia',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.idprovincia 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 20,
	 				     fieldLabel: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>',
	 				     name: 'nombre',
	 				     id: 'nombre',
	 				     height: 21,
	 				     maxLength: 1,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.nombre 
	 			        }
 				        ,
 					    {
  	 				     width: 98,
	 				     fieldLabel: '<bean:message key="provincia.idpais_corto"  bundle="fields"/>',
	 				     name: 'idpais',
	 				     id: 'idpais',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.idpais 
	 			        }
 		 		]         
		 	});

 			var win = new Ext.Window({
 	        	title: '<bean:message key="app.label.modificacion"  bundle="messages"/>',
 			    modal: true,
 				width: 288 + 50,
 				height: 175+ 15,
 				resizable: false,
 			    layout: 'fit',
 			    plain:true,
 			    bodyStyle:'padding: 10px;',
 			    buttonAlign: 'right',
 				border: false,
 				items: [ form ],
 				buttons: [aceptar, cancelar]
 			});
 	        win.show();
	  });
	    
  buttonBaja.on('click', function(e){
	    	if (!gridProvincia.getSelectionModel().hasSelection()) { 
					return false;
	    	}

			var aceptar = new Ext.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				handler: function() {
			    	Ext.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_b"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
			    			form.getForm().submit({
								  url: configObject.removeUrl,
								  success: function(response, options) {
									win.close();
			 						buttonFiltrar.handler.call(buttonFiltrar.scope);
								  },
								  failure: function(response, options) {
									var responseText = Ext.decode(options.response.responseText);
									Ext.MessageBox.alert('<bean:message key="app.validaciones.errorBaja"  bundle="messages"/>', responseText.failure);
								  }
							});
					    }
					});
				}
			});
	      	
   			var cancelar = new Ext.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>' ,minWidth: 80, handler: function() { win.close(); }});
	    	
			var recordToShow = gridProvincia.getSelectionModel().getSelected().data;

			var form = new Ext.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="provincia.idprovincia_corto"  bundle="fields"/>',
		  			 width: 98 + 17,
		  			 id: 'idprovincia',
		  			 name: 'idprovincia',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.idprovinciaName,
		             value: recordToShow.idprovincia
	  			   }
			       ,
 				   {
		            xtype: 'textfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 20,
					fieldLabel: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>',
					id: 'nombre',
					readOnly: true,
					name: 'nombre',
					height: 21,
					value: recordToShow.nombre 
				   }
				   ,
 				   {
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="provincia.idpais_corto"  bundle="fields"/>',
					id: 'idpais',
					readOnly: true,
					name: 'idpais',
					height: 21,
					value: recordToShow.idpais 
				   }
 				]
			});
			
			var win = new Ext.Window({
				title: '<bean:message key="app.label.baja"  bundle="messages"/>',
				width: 288 + 50,
				height: 175 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [aceptar,cancelar]
			});
	        win.show(this);
			
	    });
	    
  buttonVisualizar.on('click', function(){
			if (!gridProvincia.getSelectionModel().hasSelection()){
				return false;
			}

			var cerrar = new Ext.Button({text: '<bean:message key="app.label.cerrar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			
			var recordToShow = gridProvincia.getSelectionModel().getSelected().data;

			var form = new Ext.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				items: [
			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="provincia.idprovincia_corto"  bundle="fields"/>',
		  			 width: 98 + 17,
		  			 id: 'idprovincia',
		  			 name: 'idprovincia',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.idprovinciaName,
		             value: recordToShow.idprovincia
	  			   }
 				   ,
				   {
		            xtype: 'textfield', 
					width: 20,
					fieldLabel: '<bean:message key="provincia.nombre_corto"  bundle="fields"/>',
					id: 'nombre',
					readOnly: true,
					name: 'nombre',
					height: 21,
					value: recordToShow.nombre 
				   }
 				   ,
				   {
					width: 98,
					fieldLabel: '<bean:message key="provincia.idpais_corto"  bundle="fields"/>',
					id: 'idpais',
					readOnly: true,
					name: 'idpais',
					height: 21,
					value: recordToShow.idpais 
				   }
 				]
			});

			
			var win = new Ext.Window({
 	        	title: '<bean:message key="app.label.visualizacion"  bundle="messages"/>',
				width: 288 + 50,
				height: 175 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [cerrar]
			});
	        win.show(this);
	    });
	    
  });
</script>
	
<div id="panelProvincia"></div>