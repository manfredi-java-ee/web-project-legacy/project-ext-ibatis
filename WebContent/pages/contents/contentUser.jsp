<%@ page language="java" import="java.util.Properties" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib prefix="frmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="html" uri="http://struts.apache.org/tags-html" %>

<!-- Instancio la fecha del dia -->
<jsp:useBean id="now" class="java.util.Date" />

<% Properties prop = new Properties(); %>
<% prop.load(pageContext.getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF")); %>

<div id="user">
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td style="width: 250px"><span class="version"><span class="negrita">Versi&oacute;n:&nbsp;</span><%=prop.getProperty("Implementation-Version")%></span></td>
			<td style="width: 20px">
				<html:img page="/images/salir.gif" alt="LogOut"/></td>
			<td style="width: 50px">
				<span class="verdana11"><a class="verdana11 logout" href="<core:url value="/app/logout.do?method=doLogout"/>">Salir</a></span>
			</td>
		
			<td style="width: 20px">
				<html:img page="/images/calendario.gif" />
			</td>
			<td>
				<span class="verdana11">
					<frmt:formatDate value="${now}" pattern="dd/MM/yyyy"/>
				</span>
				&nbsp;
			</td>
			
			<td style="width: 20px">
				<html:img page="/images/user.gif" alt="Usuario" /></td>
			<td style="width: 70px">
				<span class="verdana11"><core:out value="${APP_USUARIO.nombre}"></core:out></span>
			</td>
			
		</tr>
	</table>
</div>