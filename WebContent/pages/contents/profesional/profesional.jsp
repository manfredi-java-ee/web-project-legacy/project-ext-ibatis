<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>


<div id="festivos" style="overflow: auto;">
	<div id="grid_festivos" style="width: 100%; height: 100%; "></div>
</div>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelMovimientoFisico',
		panelId: 'panelMovimientoFisico',
		addUrl:			'../app/movimientoFisico.do?method=altaMovimientoFisico',
		modifyUrl:		'../app/movimientoFisico.do?method=modificarMovimientoFisico',
		removeUrl: 		'../app/movimientoFisico.do?method=bajaMovimientoFisico',
		loadDataUrl:	'../app/movimientoFisico.do?method=loadData',
		filterDataUrl:	'../app/movimientoFisico.do?method=buscar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/movimientoFisico.do?method=buscar',
			pagingToolbarUrl: 		'../app/movimientoFisico.do?method=loadData',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			adabas_isnName: 'SfaMovimientosadabasisn'
		    ,
			fa_tipo_factName: 'TipoFacturacion'
		}
	};
	
	
	
function mostrarFicha(divName, urlDestino, id){
		var open=Ext.get(divName);
	if(!open){
		var msg = Ext.get('msg');
		msg.load({
			url: urlDestino,
			scripts: true,
			params: 'id='+id,
			text: 'Cargando...'
		});
		msg.show();
	}
}
 
	
	
function procesarResBorradoFestivo(result, request) {
	try{
		var json = doJSON(result.responseText);
		var param = getParametro(json.parametros, "msg");
		Ext.utiles.msg('Mensaje ',Text2Html(param.valor));
		storeFestivos.reload();
		return;
	}catch(Exception){
		Ext.MessageBox.alert('Error', 'Problemas durante el proceso. Perdone las molestias');
	}
}


function borrarFestivo(){
    var selected = gridFestivos.getSelectionModel().getSelections();
    if(selected.length==0)
    	return;

	Ext.Ajax.request({
			url : 'EliminarFestivo' ,
			params : "id="+selected[0].get("id"),
			method: 'POST',
			success: procesarResBorradoFestivo,
			failure: procesarResBorradoFestivo
	});
}


// create the Data Store
storeFestivos = new Ext.data.Store({
    // load using script tags for cross domain, if the data in on the same domain as
    // this page, an HttpProxy would be better
    proxy: new Ext.data.HttpProxy({
        url: 'BuscarFestivos'
    }),

    // create reader that reads the Topic records
    reader: new Ext.data.JsonReader({
        root: 'festivos',
        totalProperty: 'totalCount',
        fields: [
			{name: 'id', type: 'int'},
			{name: 'fecha'},
			{name: 'repetir_anualmente', type: 'int'},
			{name: 'descripcion'}
		]
    }),
	sortInfo: {field: 'id', direction: 'desc'},
    // turn on remote sorting
    remoteSort: true
});

var cm=new Ext.grid.ColumnModel([
	{id:'id', dataIndex:'id', header:'C&oacute;digo', sortable: true, hidden:true, width:60},
	{id:'fecha', dataIndex:'fecha', header:'Fecha', sortable: true, hidden:false, width:100},
	{id:'repetir_anualmente', dataIndex:'repetir_anualmente', header:'Repetir Anualmente', sortable: true, hidden:false, width:100, align:'center',
		renderer:function(data){
       	if(data>0)
	        return '<img align="absmiddle" src="images/accept.png" />';
		}
	},
	{id:'descripcion', dataIndex:'descripcion', header:'Descripcion', sortable: true, hidden:false, width:500}
]);


// var filters = new Ext.ux.grid.GridFilters({filters:[
// 	{type: 'numeric',  dataIndex: 'id'},
// 	{type: 'string',  dataIndex: 'fecha'},
// 	{type: 'numeric',  dataIndex: 'repetir_anualmente'},
// 	{type: 'string',  dataIndex: 'descripcion'}
// ]});

function getRegSelected(){
	var selected = gridFestivos.getSelectionModel().getSelections();
    if(selected.length==0)
    	return null;

    return selected[0];
}


 var gridFestivos = new Ext.grid.GridPanel({
 	 el:'grid_festivos',
 	 autoHeight:true,
 	 autoWidth:true,
 	 autoScroll:true,
     border:false,
     loadMask: true,
     store:storeFestivos,
   //  plugins: filters,
     cm: cm,
     viewConfig: {
		autoFill:true
     },
     tbar:[{
        text:'Nuevo',
        tooltip:'Nuevo Festivo',
        iconCls:'add',
		listeners :{
        	click: function(){mostrarFicha('newFestivo', 'MostrarFestivo', 0);}
        }
     },'-',{
        text:'Eliminar',
        tooltip:'Eliminar Festivo',
        iconCls:'remove',
		listeners :{
        	click: function(){
             	Ext.Msg.confirm('Confirmar', 'Seguro que desea eliminar este Registro.',
		    	function(btn){
	                if(btn == 'yes')
		        		borrarFestivo();
        		});
        	}
        }
     }],
     bbar: new Ext.PagingToolbar({
        pageSize: 25,
        store: storeFestivos,
       // plugins: filters,
        displayInfo: true
    })
   });

	gridFestivos.on('rowdblclick', function(grid, rowIndex, e) {
        //Asi obtenemos el valor de una celda
        var record = grid.getStore().getAt(rowIndex);
        //var fieldName = grid.getColumnModel().getDataIndex(0);
        var id = record.get("id");
		mostrarFicha('newFestivo', 'MostrarFestivo', id);
    });

    // trigger the data store load
	storeFestivos.load({params:{start:0, limit:25}});
   	// render it
    gridFestivos.render();
</script>

 