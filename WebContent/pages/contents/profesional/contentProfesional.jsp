<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	

	Tera.onReady(function(){

		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		
    	
		
	
			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				items: [
				    	{
				             xtype: 'numberfield',
							 width: 98,
							 fieldLabel: '<bean:message key="profesional.idpersona_corto"  bundle="fields"/>',
							 name: 'idpersonaFilter',
							 id: 'idpersonaFilter',
							 height: 21,
							 maxLength: 10,
							 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres'
						}
 				  
 				] 
 				        
			});

			
			
			var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80,handler
       : function(){	 
    	   
    	  var  astoreContacto  = Tera.getCmp("gridContactotipo");
    	 
    	 
    	  storeContacto.reload(); 
    	 
    	  Tera.MessageBox.alert('Store',astoreContacto +"" );
    	 
    	   
       }});
		    
			
			
			var ventanaCliente = new Tera.Window({
				el:'newCliente',
				title:'Cliente',
		        layout:'fit',
		        bodyStyle:'padding:5px;',
		        width:600,
		        height:400,
		        minWidth: 600,
			    minHeight: 400,
			    autoScroll: true,
		        maximizable:false,
		       // maximized:true,
		        modal: true,
		        plain: true,
		        items: [form],
		        buttons:[cancelar]
		});

		
			ventanaCliente.show();
	});
</script>
	
<div id="newCliente"></div>
<div id="panelProfesional"></div>