<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<script type="text/javascript">

	function doAccept(){
		document.forms['form'].submit();
	}
	
	function doCancel(){
		document.forms['form'].action = "startApp.do?method=viewLogin";
		document.forms['form'].submit();
	}
	
	Tera.onReady(function(){
		var chkAgree = new Tera.form.Checkbox({ 
			renderTo : 'chkAgree',
			listeners : {
				'check' : function(obj, checked) {
					(checked) ? btnAceptar.enable() : btnAceptar.disable();  
				}
			}
		});
		
		var btnAceptar = new Tera.Button({ text : 'Aceptar', disabled : true, minWidth : 80, handler : function() { doAccept(); }});
		var btnCancelar = new Tera.Button({ text : 'Cancelar', minWidth : 80, handler : function() { doCancel(); } });
		
		var btnPanel = new Tera.Panel({
			renderTo : 'btnPanel',
			baseCls: 'x-plain',
			border : false,
			layout : 'table',
			layoutConfig: { columns : 2 },
			defaults : {
				style : 'margin: 10px 5px 10px 10px;'
			},
			items : [btnAceptar, btnCancelar]
		});
	});
</script>

<html:form action="/startApp.do?method=acceptAgreement" styleId="form" onsubmit="return false;">
<div align="center" style="padding-top: 40px;">
	<table border="0" width="80%" cellspacing="0" cellpadding="0" style="border: solid 1px">
		<tr style="background-color: #00457C;">
			<td height="30" style="font-size: 15px; color: #FFFFFF; font-weight: bold; padding-left: 15px">T&#233;rminos de Uso</td>
		</tr>
		<tr style="background-color: #FFFFFF;">
			<td height="400" valign="top" style="padding: 15px">
				<div style="overflow: auto; height: 400px; width: 100%; font-size: 12px"; >
				Est� accediendo al Sistema de Consultas sobre la Base de Datos SIMAF (Proyecto de  Conversi�n_ACMEFADEUDAS)  <br><br> 
			<br><br>
			<br><br>
				<!-- 
				<b>Su �ltimo acceso correcto se realiz� el:</b> AAAA-MM-DD hh:mm:ss<br>
				Desde la direcci�n IP: XX.XX.XXX.XX
				<br><br>
				<b>Su �ltimo acceso err�neo se realiz� el:</b> AAAA-MM-DD hh:mm:ss<br>
				Desde la direcci�n IP: XX.XX.XXX.XX
				 -->
				</div>
			</td>
		</tr>
		<tr>
			<td height="30" style="padding-bottom: 15px; padding-top: 20px; background-color: #FFFFFF;">
			<div style="padding-left: 15px; background-color: #FFFFFF;">
				<table border="0" width="400" cellspacing="0" cellpadding="0">
					<tr>
						<td width="25"><div id="chkAgree"/></td>
						<td><span class="verdana11Negro">Comprendo y acepto los t&#233;rminos de licencia.</span></td>
					</tr>
				</table>
			</div>
			</td>
		</tr>
		<tr>
			<td height="40" style="background-color: #00457C;">
				<div id="btnPanel"/>
			</td>
		</tr>
	</table>
</div>
</html:form>