<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript"> 

	
	var configObject = {
		entity: '',
		renderTo: 'panelSfaFisicoReintegros',
		panelId: 'panelSfaFisicoReintegros',
		addUrl:			'../app/sfaFisicoReintegros.do?method=altaSfaFisicoReintegros',
		modifyUrl:		'../app/sfaFisicoReintegros.do?method=PrescribirReintegro',
		removeUrl: 		'../app/sfaFisicoReintegros.do?method=bajaSfaFisicoReintegros',
		loadDataUrl:	'../app/sfaFisicoReintegros.do?method=loadData',
		filterDataUrl:	'../app/sfaFisicoReintegros.do?method=buscar'
	};

	var configObjectMiscelaneos = {
			entity: '',
			renderTo: 'panelSfaMMiscelaneosFisico',
			panelId: 'panelSfaMMiscelaneosFisico',
			addUrl:			'../app/sfaMMiscelaneosFisico.do?method=altaSfaMMiscelaneosFisico',
			modifyUrl:		'../app/sfaMMiscelaneosFisico.do?method=modificarSfaMMiscelaneosFisico',
			removeUrl: 		'../app/sfaMMiscelaneosFisico.do?method=bajaSfaMMiscelaneosFisico',
			loadDataUrl:	'../app/sfaMMiscelaneosFisico.do?method=loadData',
			filterDataUrl:	'../app/sfaMMiscelaneosFisico.do?method=buscar'
		};	

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/sfaFisicoReintegros.do?method=Buscar',		
			pagingToolbarUrl: 		'../app/sfaFisicoReintegros.do?method=actualizarSfaFisicoReintegros',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};

	
    var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			adabas_isnName: 'adabasisnSFA_FISICO_REINTEGROS'
		}
	};

	var estaPrescripto;
	var win;

	
	Tera.onReady(function(){

		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		
		
				
		var labelresultado = new Tera.form.Label	(
	      {	
	         xtype: 'label', 
		 id: 'labelresultado',
	     	 cls: 'x-form-item myBold',
		 style: 'font-weight:bold;color:black;' 
 
	      }
	      );  
				
		

	/////////  Formulario Búsqueda -INICIO		

				var filtroBusqueda = new Tera.form.FilterPanel(
						{
							mappedFields : [ 'fa_re_interurbFilter',
									'fa_re_urbFilter', 'fa_re_lineaFilter',
									'fa_re_num_reintegroFilter' ],
							filterConfig : filterConfig,
							bodyBorder : false,
							border : false,
							bodyStyle : 'padding:10px',
							buttonAlign : 'right',
							width : 600,
							baseCls : 'x-plain',
							labelWidth : 130,
							labelAlign : 'top',
							height : 230,
							items : [ //230

									/////// Primera Fila titulo		                
									{
										layout : 'column',
										border : false,
										items : [ {
											columnWidth : .3,
											layout : 'form',
											border : false,
											items : [ {
												xtype : 'label',
												id : 'labelcriterios',
												cls : 'x-form-item myBold',
												style : 'font-weight:bold;',
												text : 'Criterios de Búsqueda',
												labelAlign : 'left'
											} ]
										}, {
											columnWidth : .3,
											layout : 'form',
											border : false

										}, {
											columnWidth : .3,
											layout : 'form',
											border : false
										} ]
									} // ACA TERMINA LA LLAVE DE PRIMERA FILA
									,

									///Segunda Fila

									{
										layout : 'column',
										border : false,
										items : [
												{

													columnWidth : .1,
													layout : 'form',
													border : false,
													items : [ {

														xtype : 'radio',
														name : 'radio1',
														id : 'radio1',
														Value : 'page',
														boxLabel : '-',
														inputValue : 'IUL',
														labelSeparator : "",
														width : 80
													} ]

												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'textfield',
														width : 110,
														fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_interurb_corto"  bundle="fields"/>',
														name : 'fa_re_interurbFilter',
														id : 'fa_re_interurbFilter',
														height : 21,
														maxLength : 4,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
														disabled : true

													} ]
												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'textfield',
														width : 110,
														fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_urb_corto"  bundle="fields"/>',
														name : 'fa_re_urbFilter',
														id : 'fa_re_urbFilter',
														height : 21,
														maxLength : 4,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
														disabled : true

													} ]
												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {

														xtype : 'textfield',
														width : 110,
														fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_linea_corto"  bundle="fields"/>',
														name : 'fa_re_lineaFilter',
														id : 'fa_re_lineaFilter',
														height : 21,
														maxLength : 4,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
														disabled : true

													} ]
												} ]
									//ACA TERMINA EL CORCHETE DEL PRIMER LAYAOUT COLUMN DE LA SEGUND FILA

									} //ACA TERMINA SEUNDA FILA
									,

									/////  Tercera Fila

									{
										layout : 'column',
										border : false,
										items : [
												{
													columnWidth : .1,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'radio',
														name : 'radio1',
														id : 'radioREINT',
														Value : 'page',
														boxLabel : '-',
														inputValue : 'REINTEGRO',
														labelSeparator : "",
														width : 200
													} ]

												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'numberfield',
														width : 110,
														fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_num_reintegro_corto"  bundle="fields"/>',
														name : 'fa_re_num_reintegroFilter',
														id : 'fa_re_num_reintegroFilter',
														height : 21,
														maxLength : 10,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
														disabled : true

													} ]
												} ]

									} ]
						});

				/////////  Formulario Busqueda -FIN	

				radio8 = Tera.getCmp("radio1");

				////Eventos del Radio Button	
				radio8.addListener('check', function(obj) {

					//Blanqueo ellabel de resultado
					labelresultado.setText(' ');

					//Reseteo la barra del grid PagingToolbar
					bbar1.field.dom.value = '1';
					bbar1.afterTextEl.el.innerHTML = 'de 1';
					bbar1.store.removeAll();

					bbar1.first.setDisabled(true);
					bbar1.prev.setDisabled(true);
					bbar1.next.setDisabled(true);
					bbar1.last.setDisabled(true);

					// Blanqueo el Tipo de Facturacion (creo q esto no va)

					var valor = filtroBusqueda.getForm().getValues()['radio1'];

					store.removeAll();

					InterFilter = Tera.getCmp("fa_re_interurbFilter");
					UrbFilter = Tera.getCmp("fa_re_urbFilter");
					LineaFilter = Tera.getCmp("fa_re_lineaFilter");

					NroReintegroFilter = Tera
							.getCmp("fa_re_num_reintegroFilter");

					//Limpio los campos de todos los criterios
					InterFilter.setValue('');
					UrbFilter.setValue('');
					LineaFilter.setValue('');
					NroReintegroFilter.setValue('');

					switch (valor) {
					case 'IUL':
						// habilito los campos para consulta IUL

						InterFilter.allowBlank = false;
						InterFilter.validateValue(InterFilter.getValue()); //force update
						InterFilter.enable();

						UrbFilter.allowBlank = false;
						UrbFilter.validateValue(UrbFilter.getValue()); //force update
						UrbFilter.enable();

						LineaFilter.allowBlank = false;
						LineaFilter.validateValue(LineaFilter.getValue()); //force update
						LineaFilter.enable();

						// deshabilito los campos para consulta por numero de reintegro

						NroReintegroFilter.allowBlank = true;
						NroReintegroFilter
								.validateValue(InterFilter.getValue()); //force update
						NroReintegroFilter.disable();

						break;

					case 'REINTEGRO':

						// habilito los campos para consulta por numero de Reintegro

						NroReintegroFilter.allowBlank = false;
						NroReintegroFilter
								.validateValue(InterFilter.getValue()); //force update
						NroReintegroFilter.enable();

						// deshabilito los campos para consulta IUL

						InterFilter.allowBlank = true;
						InterFilter.validateValue(InterFilter.getValue()); //force update
						InterFilter.disable();

						UrbFilter.allowBlank = true;
						UrbFilter.validateValue(UrbFilter.getValue()); //force update
						UrbFilter.disable();

						LineaFilter.allowBlank = true;
						LineaFilter.validateValue(LineaFilter.getValue()); //force update
						LineaFilter.disable();

						break;
					default:

					}

				});

				////Fin de Eventos del Radio Button 

				///STORE GRILLA PRINCIPAL SfaFisicoReintegros - INICIO

				var store = new Tera.data.JsonStore({
					remoteSort : true,
					baseParams : {
						fa_re_interurbFilter : '',
						fa_re_urbFilter : '',
						fa_re_lineaFilter : '',
						fa_re_num_reintegroFilter : ''
					},
					url : configObject.filterDataUrl,
					root : 'data',
					totalProperty : 'totalCount',
					id : 'codSfaFisicoReintegros',
					fields : [ 'campo_no_usado_17', 'fa_no_hora_alta_nc',
							'campo_no_usado_18', 'fa_no_hora_alta_nd',
							'campo_no_usado_19', 'fa_no_hora_mod_nc',
							'campo_no_usado_20', 'fa_no_hora_mod_nd',
							'fa_no_cat_gran_cli', 'fa_no_nro_gran_cli',
							'fa_no_interurb', 'fa_no_urb', 'fa_no_linea',
							'fa_no_ident_abo', 'fa_re_interurb', 'fa_re_urb',
							'fa_re_linea', 'fa_re_ident_abo',
							'fa_re_cod_postal_loc',
							'fa_re_cod_postal_loc_benf',
							'fa_re_fecha_solic_nvo', 'fa_re_fecha_emis_op_nvo',
							'fa_re_fecha_pago_nvo',
							'fa_re_fecha_gener_cheque_nvo',
							'fa_re_fecha_para_dolariz_nvo',
							'fa_re_fecha_alta_nvo', 'fa_re_fecha_modific_nvo',
							'fa_re_fecha_baja_nvo', 'fa_re_fecha_superv_nvo',
							'fa_re_fecha_autoriz_nvo', 'fa_re_fecha_pago_misc',
							'fa_re_origen', 'fa_no_fecha_alta_reintegro_nvo',
							'fa_no_fec_ing_contable_nc_nvo',
							'fa_no_fec_ing_contable_nd_nvo',
							'fa_no_fecha_para_dolariz_nvo',
							'fa_no_fecha_envio_gl_nc_nvo',
							'fa_no_fecha_envio_gl_nd_nvo',
							'fa_no_fecha_alta_nc_nvo',
							'fa_no_fecha_alta_nd_nvo',
							'fa_no_fecha_mod_nc_nvo', 'fa_no_fecha_mod_nd_nvo',
							'fa_re_imputacion_estado', 'fa_re_cod_barra',
							'fa_re_e_marca_tarea', 'fa_re_e_banco',
							'fa_re_e_imp_pagado', 'fa_re_e_fecha_acreditac',
							'fa_re_e_fecha_proceso', 'fa_re_e_sucursal',
							'fa_re_e_fecha_pres', 'fa_re_e_fecha_pago',
							'fa_re_fecha_bloqueo', 'fa_re_usuario_bloqueo',
							'fa_re_hora_bloqueo', 'fa_re_pago_duplicado',
							'fa_re_pcia_base_perc', 'fa_re_nro_movimiento',
							'fa_re_fecha_desbloqueo', 'fa_re_usu_desbloqueo',
							'fa_re_fecha_migracion', 'fa_re_cliente_atis',
							'fa_re_cuenta_atis', 'fa_re_motivo_transf',
							'fa_re_operador_loc', 'fa_re_gp_apynom',
							'fa_re_gp_tipo_doc', 'fa_re_gp_nro_doc',
							'fa_re_gp_domic', 'fa_re_gp_cod_postal',
							'fa_re_gp_localidad', 'fa_re_gp_provincia',
							'fa_re_numero_pago', 'adabas_isn',
							'fa_co_clave_numero', 'fa_co_prox_numero',
							'fa_co_ident_abo', 'fa_re_num_reintegro',
							'fa_re_zona_solic', 'campo_no_usado_1',
							'fa_re_zona', 'fa_re_oficina', 'fa_re_abonado',
							'fa_re_tip_fact', 'fa_re_anio_lote',
							'fa_re_serie_meg', 'fa_re_n_contrato_meg',
							'fa_re_n_cuota_meg', 'fa_re_nro_factura',
							'campo_no_usado_2', 'fa_re_motivo', 'fa_re_estado',
							'fa_re_nro_copia', 'fa_re_importe_tot_concepto',
							'fa_re_importe_tot_iva',
							'fa_re_importe_tot_iva_adic', 'fa_re_importe_mora',
							'fa_re_importe_tot_actualiz',
							'fa_re_importe_tot_ing_b_adic',
							'fa_re_apell_nom_lin1', 'fa_re_an_domic_lin2',
							'fa_re_domicil_lin3', 'fa_re_codpos_loc_lin4',
							'fa_re_dpto_lin6', 'fa_re_categoria',
							'fa_re_tip_contrib', 'fa_re_nro_contrib',
							'fa_re_apell_nom_lin1_benf',
							'fa_re_an_domic_lin2_benf',
							'fa_re_domicil_lin3_benf',
							'fa_re_codpos_loc_lin4_benf',
							'fa_re_dpto_loc_lin5_benf',
							'fa_re_tipo_contrib_loc', 'fa_re_nro_contrib_loc',
							'fa_re_tipo_documento', 'fa_re_num_documento',
							'fa_re_n_cuenta_clie_gob', 'fa_re_dias_actualiz',
							'fa_re_valor_pulso', 'campo_no_usado_3',
							'fa_re_zona_pago', 'fa_re_cajero',
							'fa_re_tipo_pago', 'campo_no_usado_4',
							'campo_no_usado_5', 'fa_re_marca_tarea',
							'fa_re_nro_orden_pago', 'campo_no_usado_6',
							'fa_re_hora_alta', 'fa_re_usuario_alta',
							'campo_no_usado_7', 'fa_re_hora_modific',
							'fa_re_usuario_modific', 'campo_no_usado_8',
							'fa_re_hora_baja', 'fa_re_usuario_baja',
							'fa_re_usuario_superv', 'campo_no_usado_9',
							'fa_re_hora_superv', 'fa_re_usuario_autoriz',
							'campo_no_usado_10', 'fa_re_hora_autoriz',
							'fa_re_cat_gran_cli', 'fa_re_nro_gran_cli',
							'fa_re_pcia_porc', 'fa_re_fecha_impre',
							'fa_re_porc_iva_rg17', 'fa_no_nro_reintegro',
							'campo_no_usado_11', 'fa_no_nro_nc',
							'campo_no_usado_12', 'fa_no_nro_nd',
							'campo_no_usado_13', 'campo_no_usado_14',
							'fa_no_motivo', 'fa_no_zona', 'fa_no_oficina',
							'fa_no_abonado', 'fa_no_tipo_fact',
							'fa_no_ano_lote', 'fa_no_serie_meg',
							'fa_no_n_contrato_meg', 'fa_no_n_cuota_meg',
							'fa_no_categoria', 'fa_no_tipo_contrib',
							'fa_no_tipo_contrib_nvo', 'campo_no_usado_15',
							'campo_no_usado_16', 'fa_no_marca_tarea',
							'FECHA_PAGO_DINAMICA', 'FA_RE_FECHAPAGO_PRESC',
							'PRESCRIPTO',
							'FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION',
							'FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION',
							'FA_RE_NRO_COPIA_DESCRIPCION',
							'FA_RE_MOTIVO_DESCRIPCION',
							'FA_RE_ESTADO_DESCRIPCION',
							'FA_RE_TIPO_PAGO_DESCRIPCION'

					]
				});

				///STORE GRILLA PRINCIPAL SfaFisicoReintegros - FIN	    

				function changeNumber(val) {
					var original = parseFloat(val);
					var result = original.toFixed(3);
					if (val < 0) {
						return '<span style="color:red;">' + result + '</span>';
					}

					return result;
				}

				function validarEstado(valestado) {

					var campoestado = parseInt(valestado);

					switch (campoestado) {
					case 0:
						campoestado = 'ALTA';
						return (campoestado);
						break;
					case 1:
						campoestado = 'AUTORIZADO';
						return (campoestado);
						break;
					case 2:
						campoestado = 'CON CARTA DE PAGO';
						return (campoestado);
						break;
					case 3:
						campoestado = 'ANULADO';
						return (campoestado);
						break;
					case 4:
						campoestado = 'PAGADO';
						return (campoestado);
						break;
					case 5:
						campoestado = 'ANULADO C/O. PAGO';
						return (campoestado);
						break;
					case 6:
						campoestado = 'PAGADO POR MISCE';
						return (campoestado);
						break;
					case 7:
						campoestado = 'AUTORIZADO RAPIPAGO';
						return (campoestado);
						break;
					case 8:
						campoestado = 'PAGADO RAPIPAGO';
						return (campoestado);
						break;
					case 34:
						campoestado = 'MIGRANDO A ATIS';
						return (campoestado);
						break;
					case 35:
						campoestado = 'MIGRADO A ATIS';
						return (campoestado);
						break;
					default:
						campoestado = 'DESCONOCIDO';
						return (campoestado);

					}
					;
				}

				///// FUNCION PARA TIPO DE PAGO

				function validarTipopago(valpago) {

					var tpago = parseInt(valpago);

					switch (tpago) {
					case 1:
						tpago = 'EFECTIVO';
						return (tpago);
						break;
					case 2:
						tpago = 'CHEQUE';
						return (tpago);
						break;
					case 3:
						tpago = 'RECURSOS';
						return (tpago);
						break;
					case 4:
						tpago = 'MISCELAN';
						return (tpago);
						break;
					case ' ':
						tpago = 'INEXISTENTE';
						return (tpago);
						break;
					default:
					}
					;
				}

				// GRilla Principal gridSfaFisicoReintegros - INICIO

				var cmSfaFisicoReintegros = new Tera.grid.ColumnModel(
						[
								new Tera.grid.RowNumberer(),

								{
									id : 'fa_re_interurb', //SI
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_interurb_corto"  bundle="fields"/>',
									dataIndex : 'fa_re_interurb',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_interurb_corto"  bundle="fields"/>'.length * 8,
									align : 'center'
								},
								{
									id : 'fa_re_urb', //SIII
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_urb_corto"  bundle="fields"/>',
									dataIndex : 'fa_re_urb',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_urb_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								},
								{
									id : 'fa_re_linea', //SIII
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_linea_corto"  bundle="fields"/>',
									dataIndex : 'fa_re_linea',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_linea_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								},
								{
									id : 'fa_re_tip_fact', //SIII
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_tip_fact_corto"  bundle="fields"/>',
									dataIndex : 'fa_re_tip_fact',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_tip_fact_corto"  bundle="fields"/>'.length * 7,
									align : 'center'
								},
								{
									id : 'fa_re_anio_lote', ///SIII
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_anio_lote_corto"  bundle="fields"/>',
									dataIndex : 'fa_re_anio_lote',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_anio_lote_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								},
								{
									id : 'fa_re_num_reintegro', //SIII
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_num_reintegro_corto"  bundle="fields"/>',
									dataIndex : 'fa_re_num_reintegro',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_num_reintegro_corto"  bundle="fields"/>'.length * 7,
									align : 'center'
								},
								{
									id : 'fa_re_estado', ///SIIII
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_estado_corto"  bundle="fields"/>',
									renderer : validarEstado,
									dataIndex : 'fa_re_estado',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_estado_corto"  bundle="fields"/>'.length * 20,
									align : 'center'
								},
								{
									id : 'fa_re_tipo_pago', //SIIII 
									header : '<bean:message key="sfa_fisico_reintegros.fa_re_tipo_pago_corto"  bundle="fields"/>',
									renderer : validarTipopago,
									dataIndex : 'fa_re_tipo_pago',
									width : '<bean:message key="sfa_fisico_reintegros.fa_re_tipo_pago_corto"  bundle="fields"/>'.length * 12,
									align : 'center'
								},
								{
									id : 'FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION',
									header : 'APELLIDO DESCRIPCION',
									dataIndex : 'FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION',
									width : 'APELLIDO DESC'.length * 12,
									hidden : true,
									sortable : true,
									align : 'center'

								},
								{
									id : 'FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION',
									header : 'DOMICILIO DESCRIPCION',
									dataIndex : 'FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION',
									width : 'DOMICILIO DESC'.length * 12,
									hidden : true,
									sortable : true,
									align : 'center'

								}, {
									id : 'FA_RE_NRO_COPIA_DESCRIPCION',
									header : 'NRO COPIA DESCRIPCION',
									dataIndex : 'FA_RE_NRO_COPIA_DESCRIPCION',
									width : 'NRO COPIA DESC'.length * 12,
									hidden : true,
									sortable : true,
									align : 'center'

								}, {
									id : 'FA_RE_ESTADO_DESCRIPCION',
									header : 'EST DESCRIPCION',
									dataIndex : 'FA_RE_ESTADO_DESCRIPCION',
									width : 'ESTADO DESC'.length * 12,
									hidden : true,
									sortable : true,
									align : 'center'

								}, {
									id : 'FA_RE_TIPO_PAGO_DESCRIPCION',
									header : 'TIP DESCRIPCION',
									dataIndex : 'FA_RE_TIPO_PAGO_DESCRIPCION',
									width : 'TIPO PAGO DESC'.length * 12,
									hidden : true,
									sortable : true,
									align : 'center'

								}, {
									id : 'PRESCRIPTO',
									header : 'PRES',
									dataIndex : 'PRESCRIPTO',
									width : 'PESC TIP'.length * 12,
									hidden : true,
									sortable : true,
									align : 'center'
								}

						]);

				cmSfaFisicoReintegros.defaultSortable = true;

				var bbar1 = new Tera.PagingToolbar({
					filterPanel : filtroBusqueda,
					exportFile : false,
					exportGridName : 'SfaFisicoReintegros',
					pageSize : 10,
					store : store,
					displayInfo : false,
					displayMsg : 'Mostrando   {0} - {1} de {2}',
					emptyMsg : 'No existen registros a mostrar.'

				});

				var gridSfaFisicoReintegros = new Tera.grid.GridPanel({
					width : 625,
					height : 245,
					store : store,
					cm : cmSfaFisicoReintegros,
					trackMouseOver : true,
					sm : new Tera.grid.RowSelectionModel({
						singleSelect : true
					}),
					stripeRows : true,
					viewConfig : {
						forceFit : false,
						enableRowBody : false
					},
					bbar : bbar1
				});

				/// GRilla Principal gridSfaFisicoReintegros - FIN

				/// Evento Click  de la grilla Principal - INICIO

				gridSfaFisicoReintegros.addListener('click', function(param) {
					if (gridSfaFisicoReintegros.getSelectionModel()
							.hasSelection()) {
						buttonVisualizar.setDisabled(false);

					} else {
						buttonVisualizar.setDisabled(true);
					}

				});

				//Evento Click  de la grilla Principal - FIN            

				// Evento Boton visualizar - INICIO      	    
				var buttonVisualizar = new Tera.Button(
						{
							text : '<bean:message key="app.label.consultarReintegro"  bundle="messages"/>',
							width : 300,
							minWidth : 10,
							disabled : true
						});

				// Evento Boton Filtrar - INICIO
				var buttonFiltrar = new Tera.Button(
						{
							text : 'Buscar',
							minWidth : 80,
							handler : function() {

								// Si algún campo tiene algún error 
								if (filtroBusqueda.getForm().isValid() == false) {

									Tera.Msg
											.show({
												title : 'Información', //<- el título del diálogo   
												msg : 'Verifique la longitud de los campos que están señalizados en color rojo.', //<- El mensaje   
												buttons : Tera.Msg.OK, //<- Botones de SI y NO   
												icon : Tera.Msg.INFO, // <- un ícono de error   
												fn : this.callback
											//<- la función que se ejecuta cuando se da clic   
											});
								}

								// recupero el valor del radioButton     
								var seleccionado = filtroBusqueda.getForm()
										.getValues()['radio1'];

								// Si el campo para consultar por ZOA esta checkeado
								if (seleccionado == 'IUL') {
									//Evaluo que los campos para consultar por ZOA esten completos.  
									//Si estan vacios muestro advertencia
									if (Tera.getCmp('fa_re_interurbFilter')
											.getValue() == ""
											|| Tera.getCmp('fa_re_urbFilter')
													.getValue() == ""
											|| Tera.getCmp('fa_re_lineaFilter')
													.getValue() == "") {

										///Creacion de una mensaje de usuario personalizado
										Tera.Msg
												.show({
													title : 'Información', //<- el título del diálogo   
													msg : 'Para realizar la consulta, por favor ingrese Interurbano,Urbano y Linea.', //<- El mensaje   
													buttons : Tera.Msg.OK, //<- Botones de SI y NO   
													icon : Tera.Msg.INFO, // <- un ícono de error   
													fn : this.callback
												//<- la función que se ejecuta cuando se da clic   
												});

										return false;
									}
								} else

								if (seleccionado == 'REINTEGRO') {

									//Evaluo que los campos para consultar por IUL esten completos.  
									//Si estan vacios muestro advertencia 
									if (Tera
											.getCmp('fa_re_num_reintegroFilter')
											.getValue() == "")

									{

										///Creacion de una mensaje de usuario personalizado
										Tera.Msg
												.show({
													title : 'Información', //<- el título del diálogo   
													msg : 'Para realizar la consulta, por favor ingrese Numero de Reintegro.',
													buttons : Tera.Msg.OK, //<- Botones de SI y NO   
													icon : Tera.Msg.INFO, // <- un ícono de error   
													fn : this.callback
												//<- la función que se ejecuta cuando se da clic   
												});

										return false;
									}
								} else
								// Sino fue seleccionado ningun radio Button   

								{

									///Creacion de una mensaje de usuario personalizado
									Tera.Msg
											.show({
												title : 'Información', //<- el título del diálogo   
												msg : 'Para realizar la consulta, por favor seleccione algún criterio de búsqueda.', //<- El mensaje   
												buttons : Tera.Msg.OK, //<- Botones de SI y NO   
												icon : Tera.Msg.INFO, // <- un ícono de error   
												fn : this.callback
											//<- la función que se ejecuta cuando se da clic   
											});

									return false;

								}

								///
								filtroBusqueda
										.submit({
											waitMsg : '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
											url : configObject.filterDataUrl,
											success : function(response,
													options) {
												var responseText = Tera
														.decode(options.response.responseText);
												if (responseText.data != 'undefined'
														|| responseText.data != null
														|| responseText.data != 0) {
													gridSfaFisicoReintegros
															.getStore()
															.loadData(
																	responseText);
													store.baseParams.fa_re_interurbFilter = Tera
															.getCmp(
																	'fa_re_interurbFilter')
															.getValue();
													store.baseParams.fa_re_urbFilter = Tera
															.getCmp(
																	'fa_re_urbFilter')
															.getValue();
													store.baseParams.fa_re_lineaFilter = Tera
															.getCmp(
																	'fa_re_lineaFilter')
															.getValue();
													store.baseParams.fa_re_num_reintegroFilter = Tera
															.getCmp(
																	'fa_re_num_reintegroFilter')
															.getValue();

												}
												//Evaluo si el resultado de la consulta y muestro la leyenda
												var resultado; // Resultado de la consulta 
												var cant = 0;
												cant = store.getTotalCount();

												if (cant > 0) {
													resultado = 'El resultado para el criterio de búsqueda seleccionado es:';
													labelresultado
															.setText(resultado);
												} else {
													resultado = 'No se encontraron resultados : ';
													labelresultado
															.setText(resultado);
												}

											},
											failure : function(response,
													options) {
												var responseText = Tera
														.decode(options.response.responseText);
												Tera.MessageBox
														.alert(
																'<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>',
																responseText.failure);
											}
										});
							}
						});

				// Evento Boton Filtrar - FIN

				// Evento Boton Limpiar - INICIO	

				var buttonLimpiar = new Tera.Button(
						{
							text : '<bean:message key="app.label.limpiar"  bundle="messages"/>',
							minWidth : 80,
							handler : function() {
								store.baseParams.fa_re_interurbFilter = '';
								store.baseParams.fa_re_urbFilter = '';
								store.baseParams.fa_re_lineaFilter = '';
								store.baseParams.fa_re_num_reintegroFilter = '';

								//reseteo los campos TextField del formulario 

								filtroBusqueda.getForm().reset();

								//Blanqueo el label de resultado

								labelresultado.setText(' ');

								//Reseteo los campos numberfield

								InterurbanoFilter = Tera
										.getCmp("fa_re_interurbFilter");
								InterurbanoFilter.setDisabled(true);
								UrbanoFilter = Tera.getCmp("fa_re_urbFilter");
								UrbanoFilter.setDisabled(true);
								LineaFilter = Tera.getCmp("fa_re_lineaFilter");
								LineaFilter.setDisabled(true);
								DocumentoFilter = Tera
										.getCmp("fa_re_num_reintegroFilter");
								DocumentoFilter.setDisabled(true);

								//Reseteo la barra del grid PagingToolbar

								bbar1.field.dom.value = '1';
								bbar1.afterTextEl.el.innerHTML = 'de 1';
								bbar1.store.removeAll();
								bbar1.first.setDisabled(true);
								bbar1.prev.setDisabled(true);
								bbar1.next.setDisabled(true);
								bbar1.last.setDisabled(true);

								bbar1.updateInfo();

								storeMiscelaneos.removeAll();

							}
						});

				/////Evento del Botón Limpiar -- FIN

				//STORE DE MISCELANEOS

				var storeMiscelaneos = new Tera.data.JsonStore(
						{
							remoteSort : false,
							url : configObjectMiscelaneos.filterDataUrl,

							//Agregado
							setBaseParamMiscelaneos : function(name, value) {
								this.baseParams = this.baseParams || {};
								this.baseParams[name] = value;
							},
							remoteSort : true,
							root : 'data',
							totalProperty : 'totalCount',
							id : 'codSfaMMiscelaneosFisico',
							fields : [ 'adabas_isn', 'fa_m_zona',
									'fa_m_oficina', 'fa_m_abonado',
									'fa_m_tipo_factura', 'fa_m_anio_lote',
									'fa_m_zona_pago', 'fa_m_fecha_pago',
									'fa_m_fecha_imputacion',
									'fa_m_fecha_proceso_tarea',
									'fa_m_marca_tarea',
									'fa_m_importe_miscelaneos',
									'fa_m_estado_bajada', 'fa_m_concepto_fact',
									'fa_m_zona_facturada',
									'fa_m_oficina_facturada',
									'fa_m_abonado_facturado',
									'fa_m_anio_lote_facturado',
									'fa_m_tipo_servicio_facturado',
									'fa_m_fecha_alta', 'fa_m_usuario_alta',
									'fa_m_fecha_envio', 'fa_m_usuario_envio',
									'fa_m_fecha_baja', 'fa_m_usuario_baja',
									'fa_m_fecha_vencimiento',
									'fa_m_fecha_facturacion',
									'fa_m_usuario_facturacion',
									'fa_m_nro_reinteg', 'fa_m_cpto_zona',
									'fa_m_cpto_oficina', 'fa_m_cpto_abonado',
									'fa_m_cpto_tipo_fact',
									'fa_m_cpto_anio_lote',
									'fa_m_cpto_concepto',
									'fa_m_cpto_cod_tercero',
									'fa_m_cpto_estado', 'fa_m_cpto_tarea',
									'fa_m_cpto_importe',
									'fa_m_cpto_fecha_alta',
									'fa_m_cpto_usuario_alta',
									'fa_m_cpto_fecha_aplicacion',
									'fa_m_cpto_usuario_aplicacion',
									'fa_m_interurb', 'fa_m_urb', 'fa_m_linea',
									'fa_m_ident_abo',
									'fa_m_interurb_facturado',
									'fa_m_urb_facturado',
									'fa_m_linea_facturada', 'USUARIO_DINAMICO',
									'FECHA_DINAMICA', 'ESTADO_DINAMICO',
									'DESC_MISC_R' ]

						});

				//PANEL PRINCIPAL- inicio

				var panel = new Tera.Panel({
					id : configObject.panelId,
					renderTo : configObject.renderTo,
					border : false,
					defaults : {
						border : false
					},
					items : [ {
						border : false,
						defaults : {
							border : false
						},
						items : [ {
							layout : "column",
							border : false,
							defaults : {
								border : false
							},
							items : [ {
								columnWidth : 1,
								items : [ filtroBusqueda ]
							}, {
								width : 113,
								items : [ {
									border : false,
									defaults : {
										border : false
									},
									items : [ {
										items : [ buttonFiltrar ],
										style : 'margin-top: 10px;'
									}, {
										items : [ buttonLimpiar ],
										style : 'margin-top: 3px;'
									} ]
								} ]
							} ]
						} ]
					}, {
						layout : "fit",
						border : false,
						style : 'padding: 0px 5px 0px 5px;'
					}, {
						layout : "fit",
						border : false,
						style : 'padding: 0px 5px 0px 5px;',
						items : [ labelresultado ]
					},

					{
						layout : "fit",
						border : false,
						style : 'padding: 0px 5px 0px 5px;',
						items : [ gridSfaFisicoReintegros ]
					}, {
						layout : "fit",
						border : false,
						items : [ {
							layout : "column",
							style : 'margin-top: 8px;',
							defaults : {
								border : false
							},
							border : false,
							items : [ {
								width : 150,
								items : [ buttonVisualizar ]
							} ]
						} ]
					} ]
				});

				/////Empieza buttonVisualizar

				buttonVisualizar
						.on(
								'click',
								function() {
									if (!gridSfaFisicoReintegros
											.getSelectionModel().hasSelection()) {
										return false;
									}

									var recordToShow = gridSfaFisicoReintegros
											.getSelectionModel().getSelected().data;

									/// VALIDACION PARA LA ZONA DE PAGO

									if (!(recordToShow.fa_re_tipo_pago != 4)
											&& (recordToShow.fa_re_fecha_pago_nvo != '99999999')) {
										recordToShow.fa_re_zona_pago = 0;
									}

									///// VALIDACION PARA FA_RE_CAJERO

									if (!(recordToShow.fa_re_tipo_pago != 4)
											&& (recordToShow.fa_re_fecha_pago_nvo != '99999999')) {
										recordToShow.fa_re_cajero = 0;
									}

									if (recordToShow.PRESCRIPTO == false) {
										estaPrescripto = false;
									} else {
										estaPrescripto = true;
									}

									/////VALIDACION PARA FECHA REVAL.
									if (!(recordToShow.fa_re_fecha_gener_cheque_nvo > recordToShow.fa_re_fecha_emis_op_nvo)) {
										recordToShow.fa_re_fecha_gener_cheque_nvo = 0;
									}

									//Store completo 

									var tipago = recordToShow.fa_re_tipo_pago;

									if (tipago == 4) {
										var reintegro = parseInt(recordToShow.fa_re_num_reintegro);

										storeMiscelaneos
												.setBaseParamMiscelaneos(
														'fa_m_nro_reintegFilter',
														reintegro);
									}

									storeMiscelaneos.load({
										callback : findMiscelaneo
									}, {
										params : {
											start : 0,
											limit : 7
										}
									});

									function findMiscelaneo(dataRecord,
											options, success) {

										var cantMiscelaneos;

										cantMiscelaneos = storeMiscelaneos
												.getTotalCount();

										if (cantMiscelaneos != 0) {

											var estadoBaja;
											var fechaBaja;
											var usuarioBaja;

											estadoBaja = dataRecord[0]
													.get('ESTADO_DINAMICO');

											recordToShow.ESTADO_DINAMICO = estadoBaja;

											fechaBaja = dataRecord[0]
													.get('FECHA_DINAMICA');

											recordToShow.FECHA_DINAMICA = fechaBaja;

											usuarioBaja = dataRecord[0]
													.get('USUARIO_DINAMICO');

											recordToShow.USUARIO_DINAMICO = usuarioBaja;

										}

										var formvisualizar = new Tera.form.FormPanel(
												{

													bodyBorder : true,
													border : true,
													bodyStyle : 'padding:10px',
													buttonAlign : 'right',
													baseCls : 'x-plain',
													labelWidth : 130,
													height : 280,
													width : 1000,
													labelAlign : 'left',
													items : [

															///////Titulo
															{
																layout : 'column',
																border : false,
																items : [ {
																	columnWidth : .3,
																	layout : 'form',
																	border : false,
																	items : [ {
																		xtype : 'label',
																		id : 'labelInfoCliente',
																		cls : 'x-form-item myBold',
																		style : 'font-weight:bold;',
																		text : 'Informacion del Cliente',
																		labelAlign : 'left'
																	} ]

																} ]
															},
															{

																////Primera Fila						 	        
																layout : 'column',
																border : false,
																items : [ {

																	columnWidth : .5,
																	layout : 'form',
																	border : false,
																	items : [
																			{
																				xtype : 'textfield',
																				width : 250,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_apell_nom_lin1_benf_corto"  bundle="fields"/>',
																				name : 'FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION',
																				id : 'FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
																			},
																			{ //AGREGADO DEL ISN OCULTO///

																				width : 1,
																				name : 'adabas_isn',
																				id : 'adabas_isn',
																				hidden : true,
																				sortable : true,
																				value : recordToShow.adabas_isn

																			} ]
																} ]
															},
															{

																////Segunda Fila						 	        
																layout : 'column',
																border : false,
																items : [ {

																	columnWidth : .5,
																	layout : 'form',
																	border : false,
																	items : [ {
																		xtype : 'textfield',
																		width : 250,
																		fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_an_domic_lin2_benf_corto"  bundle="fields"/>',
																		name : 'FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION',
																		id : 'FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION',
																		readOnly : true,
																		height : 21,
																		value : recordToShow.FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
																	} ]
																} ]
															},
															{
																///Tercera Fila
																layout : 'column',
																border : false,
																items : [
																		{

																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_cod_postal_loc_benf_corto"  bundle="fields"/>',
																				name : 'fa_re_cod_postal_loc_benf',
																				id : 'fa_re_cod_postal_loc_benf',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_cod_postal_loc_benf
																			} ]

																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_codpos_loc_lin4_benf_corto"  bundle="fields"/>',
																				name : 'fa_re_codpos_loc_lin4_benf',
																				id : 'fa_re_codpos_loc_lin4_benf',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_codpos_loc_lin4_benf
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_ident_abo_corto"  bundle="fields"/>',
																				name : 'fa_re_ident_abo',
																				id : 'fa_re_ident_abo',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_ident_abo
																			} ]
																		} ]
															},
															{
																///Cuarta Fila
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_tip_contrib_corto"  bundle="fields"/>',
																				name : 'fa_re_tip_contrib',
																				id : 'fa_re_tip_contrib',
																				readOnly : true,
																				height : 21,
																				maxLength : 33,
																				value : recordToShow.fa_re_tip_contrib
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_nro_contrib_corto"  bundle="fields"/>',
																				name : 'fa_re_nro_contrib',
																				id : 'fa_re_nro_contrib',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_nro_contrib
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_categoria_corto"  bundle="fields"/>',
																				name : 'fa_re_categoria',
																				id : 'fa_re_categoria',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_categoria
																			} ]
																		} ]
															},
															{
																//// Renglon en blanco		 	
																layout : 'column',
																border : false,
																items : [ {
																	columnWidth : .5,
																	layout : 'form',
																	border : false,
																	items : [ {
																		xtype : 'label',
																		id : 'labelblanco',
																		cls : 'x-form-item myBold',
																		style : 'font-weight:bold;',
																		text : '                   ',
																		labelAlign : 'left'
																	} ]
																} ]
															},
															//// SEGUNDO TITULO
															{
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'label',
																				id : 'labelinforeintegro',
																				cls : 'x-form-item myBold',
																				style : 'font-weight:bold;',
																				text : 'Informacion del Reintegro',
																				labelAlign : 'left'
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false
																		} ]
															},
															{
																///Quinta Fila
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'numberfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_num_reintegro_corto"  bundle="fields"/>',
																				name : 'fa_re_num_reintegro',
																				id : 'fa_re_num_reintegro',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_num_reintegro
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_motivo_corto"  bundle="fields"/>',
																				name : 'FA_RE_MOTIVO_DESCRIPCION',
																				id : 'FA_RE_MOTIVO_DESCRIPCION',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FA_RE_MOTIVO_DESCRIPCION
																			} ]
																		},
																		////////checkbox para prescripto
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'checkbox',
																				name : 'checkpres',
																				id : 'checkpres',
																				fieldLabel : 'Prescripto',
																				labelAlign : 'rigth',
																				anchor : '40%',
																				height : '20px',
																				disabled : false,
																				checked : false
																			//,																																													   
																			// 											 	listeners : {  
																			// 											 		check : {    
																			// 											 			fn : function(checkbox, checked)
																			// 											 			{      

																			// 											 				evaluarCheckBox(checked);
																			// 											 			} 
																			// 											 		} 
																			// 											 		}//Fin listener

																			}

																			]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 80,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.FA_RE_FECHAPAGO_PRESC_corto"  bundle="fields"/>',
																				name : 'FA_RE_FECHAPAGO_PRESC',
																				id : 'FA_RE_FECHAPAGO_PRESC',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FA_RE_FECHAPAGO_PRESC

																			}

																			]
																		} ]
															},
															{
																////SEXTA FILA			   
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_zona_solic_corto"  bundle="fields"/>',
																				name : 'fa_re_zona_solic',
																				id : 'fa_re_zona_solic',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_zona_solic
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 80,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_fecha_solic_nvo_corto"  bundle="fields"/>',
																				name : 'fa_re_fecha_solic_nvo',
																				id : 'fa_re_fecha_solic_nvo',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_fecha_solic_nvo

																			} ]
																		} ]
															}// TERMINA SEXTA FILA
															,
															{
																///SEPTIMA FILA
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_estado_corto"  bundle="fields"/>',
																				name : 'FA_RE_ESTADO_DESCRIPCION',
																				id : 'FA_RE_ESTADO_DESCRIPCION',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FA_RE_ESTADO_DESCRIPCION
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 80,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_fecha_alta_nvo_corto"  bundle="fields"/>',
																				name : 'fa_re_fecha_alta_nvo',
																				id : 'fa_re_fecha_alta_nvo',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_fecha_alta_nvo

																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 80,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_fecha_desbloqueo_corto"  bundle="fields"/>',
																				name : 'fa_re_fecha_desbloqueo',
																				id : 'fa_re_fecha_desbloqueo',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_fecha_desbloqueo

																			} ]
																		} ]
															},
															{
																///FILA 8
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ { /// INTERURBANO + URBANO + LINEA								 	 			
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : 'In-Ur-Li',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_interurb
																						+ ' '
																						+ recordToShow.fa_re_urb
																						+ ' '
																						+ recordToShow.fa_re_linea
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ { /// ZONA + OFICINA +ABONADO
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : 'Zo-Of-Ab',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_zona
																						+ ' '
																						+ recordToShow.fa_re_oficina
																						+ ' '
																						+ recordToShow.fa_re_abonado
																			} ]
																		},
																		{
																			columnWidth : .2,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 40,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_tip_fact_corto"  bundle="fields"/>',
																				name : 'fa_re_tip_fact',
																				id : 'fa_re_tip_fact',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_tip_fact
																			} ]
																		},
																		{
																			columnWidth : .2,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 45,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_anio_lote_corto"  bundle="fields"/>',
																				name : 'fa_re_anio_lote',
																				id : 'fa_re_anio_lote',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_anio_lote
																			} ]
																		} ]
															},
															{
																///FILA 9
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_serie_meg_corto"  bundle="fields"/>',
																				name : 'fa_re_serie_meg',
																				id : 'fa_re_serie_meg',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_serie_meg
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_n_contrato_meg_corto"  bundle="fields"/>',
																				name : 'fa_re_n_contrato_meg',
																				id : 'fa_re_n_contrato_meg',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_n_contrato_meg
																			} ]
																		},
																		{
																			columnWidth : .2,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 40,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_n_cuota_meg_corto"  bundle="fields"/>',
																				name : 'fa_re_n_cuota_meg',
																				id : 'fa_re_n_cuota_meg',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_n_cuota_meg
																			} ]
																		},
																		{
																			columnWidth : .2,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 45,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_marca_tarea_corto"  bundle="fields"/>',
																				name : 'fa_re_marca_tarea',
																				id : 'fa_re_marca_tarea',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_marca_tarea
																			} ]
																		} ]
															},
															{
																///FILA 10
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_usuario_alta_corto"  bundle="fields"/>',
																				name : 'fa_re_usuario_alta',
																				id : 'fa_re_usuario_alta',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_usuario_alta
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_usuario_superv_corto"  bundle="fields"/>',
																				name : 'fa_re_usuario_superv',
																				id : 'fa_re_usuario_superv',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_usuario_superv
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_usuario_autoriz_corto"  bundle="fields"/>',
																				name : 'fa_re_usuario_autoriz',
																				id : 'fa_re_usuario_autoriz',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_usuario_autoriz
																			} ]
																		} ]
															},

															/////FILA CON CAMPOS DINAMICOS DE LA TABLA MISCELANEOS								             
															{
																///FILA 10
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_m_miscelaneos_fisico.ESTADO_DINAMICO_corto"  bundle="fields"/>',
																				name : 'ESTADO_DINAMICO',
																				id : 'ESTADO_DINAMICO',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.ESTADO_DINAMICO
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 80,
																				fieldLabel : '<bean:message key="sfa_m_miscelaneos_fisico.FECHA_DINAMICA_corto"  bundle="fields"/>',
																				name : 'FECHA_DINAMICA',
																				id : 'FECHA_DINAMICA',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FECHA_DINAMICA

																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_m_miscelaneos_fisico.USUARIO_DINAMICO_corto"  bundle="fields"/>',
																				name : 'USUARIO_DINAMICO',
																				id : 'USUARIO_DINAMICO',
																				height : 21,
																				value : recordToShow.USUARIO_DINAMICO
																			} ]
																		} ]
															},

															{
																// FILA 12
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_importe_tot_concepto_corto"  bundle="fields"/>',
																				name : 'fa_re_importe_tot_concepto',
																				id : 'fa_re_importe_tot_concepto',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_importe_tot_concepto
																			} ]
																		},
																		{ ///FECHA DE PAGO MISCELANEO
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 80,
																				fieldLabel : '',
																				name : 'FECHA_PAGO_DINAMICA',
																				id : 'FECHA_PAGO_DINAMICA',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FECHA_PAGO_DINAMICA

																			} ]
																		} ]
															},
															{
																/// FILA 13
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_importe_tot_iva_corto"  bundle="fields"/>',
																				name : 'fa_re_importe_tot_iva',
																				id : 'fa_re_importe_tot_iva',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_importe_tot_iva
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_zona_pago_corto"  bundle="fields"/>',
																				name : 'fa_re_zona_pago',
																				id : 'fa_re_zona_pago',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_zona_pago
																			} ]
																		} ]
															},
															{
																/// FILA 14
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_importe_tot_iva_adic_corto"  bundle="fields"/>',
																				name : 'fa_re_importe_tot_iva_adic',
																				id : 'fa_re_importe_tot_iva_adic',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_importe_tot_iva_adic
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_cajero_corto"  bundle="fields"/>',
																				name : 'fa_re_cajero',
																				id : 'fa_re_cajero',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_cajero
																			} ]
																		} ]
															},
															{
																/// FILA 15
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_importe_mora_corto"  bundle="fields"/>',
																				name : 'fa_re_importe_mora',
																				id : 'fa_re_importe_mora',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_importe_mora
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ { /// FALTA LA VALIDACION DE ESTE CAMPO
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_tipo_pago_corto"  bundle="fields"/>',
																				name : 'FA_RE_TIPO_PAGO_DESCRIPCION',
																				id : 'FA_RE_TIPO_PAGO_DESCRIPCION',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FA_RE_TIPO_PAGO_DESCRIPCION
																			} ]
																		} ]
															},
															{
																/// FILA 16
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_importe_tot_ing_b_adic_corto"  bundle="fields"/>',
																				name : 'fa_re_importe_tot_ing_b_adic',
																				id : 'fa_re_importe_tot_ing_b_adic',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_importe_tot_ing_b_adic
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_nro_copia_corto"  bundle="fields"/>',
																				name : 'FA_RE_NRO_COPIA_DESCRIPCION',
																				id : 'FA_RE_NRO_COPIA_DESCRIPCION',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.FA_RE_NRO_COPIA_DESCRIPCION
																			} ]
																		} ]
															},
															{
																/// FILA 17
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_importe_tot_actualiz_corto"  bundle="fields"/>',
																				name : 'fa_re_importe_tot_actualiz',
																				id : 'fa_re_importe_tot_actualiz',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_importe_tot_actualiz
																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 80,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_fecha_gener_cheque_nvo_corto"  bundle="fields"/>',
																				name : 'fa_re_fecha_gener_cheque_nvo',
																				id : 'fa_re_fecha_gener_cheque_nvo',
																				readOnly : true,
																				height : 21,
																				allowBlank : true,
																				value : recordToShow.fa_re_fecha_gener_cheque_nvo

																			} ]
																		} ]
															},
															{
																/// FILA 18
																layout : 'column',
																border : false,
																items : [
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : 'Importe a Reintegrar',
																				//		name: 'fa_re_importe_tot_actualiz',
																				//		id: 'fa_re_importe_tot_actualiz',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_importe_tot_concepto
																						+ recordToShow.fa_re_importe_tot_iva_adic
																						+ recordToShow.fa_re_importe_tot_iva
																						+ recordToShow.fa_re_importe_tot_actualiz
																						+ recordToShow.fa_re_importe_mora
																						+ recordToShow.fa_re_importe_tot_ing_b_adic

																			} ]
																		},
																		{
																			columnWidth : .3,
																			layout : 'form',
																			border : false,
																			items : [ {
																				xtype : 'textfield',
																				width : 150,
																				fieldLabel : '<bean:message key="sfa_fisico_reintegros.fa_re_dias_actualiz_corto"  bundle="fields"/>',
																				name : 'fa_re_dias_actualiz',
																				id : 'fa_re_dias_actualiz',
																				readOnly : true,
																				height : 21,
																				value : recordToShow.fa_re_dias_actualiz
																			}

																			]
																		}

																]
															},
															{ // Capturo el valor de PRESCRIPTO y lo oculto

																xtype : 'textfield',
																width : 150,
																//	fieldLabel: '<bean:message key="sfa_fisico_reintegros.fa_re_dias_actualiz_corto"  bundle="fields"/>',
																name : 'PRESCRIPTO',
																id : 'PRESCRIPTO',
																readOnly : true,
																height : 21,
																hidden : true,
																sortable : true,
																value : recordToShow.PRESCRIPTO

															}

													]
												//FINAL DEL ITEM PRINCIPAL		     
												});

										// Deshabilita el checkbox si tiene fecha de prescripcion
										if (estaPrescripto == true) {
											var check2 = Tera
													.getCmp("checkpres");
											check2.checked = true;
											check2.disable();
										}
										;

										var cerrar = new Tera.Button(
												{
													text : '<bean:message key="app.label.cerrar"  bundle="messages"/>',
													minWidth : 80,
													handler : function() {
														win.close();
													}
												});
										win = new Tera.Window(
												{
													title : '<bean:message key="app.label.Detalle"  bundle="messages"/>',
													width : 1100,
													height : 680,
													resizable : false,
													modal : true,
													layout : 'fit',
													plain : true,
													bodyStyle : 'padding: 10px;',
													buttonAlign : 'right',
													border : false,
													items : [ formvisualizar ],
													buttons : [ cerrar ]
												});

										var check1 = Tera.getCmp("checkpres");
										////Eventos del ckeck box 

										//Actualiza y checkbox	

										check1.addListener('change', function(
												obj, newValue, oldValue) {

											var nuevo = newValue;
											var viejo = oldValue;

											evaluarCheckBox(obj.checked);

										}); //fin de evento check

										function evaluarCheckBox(checked) {
											var chequeado = checked;

											if (chequeado == false) {
												return;
											}

											Tera.MessageBox
													.confirm(
															'confirma',
															'Desea prescribir este Reintegro',

															function(btn) {

																if ('yes' == btn) {
																	formvisualizar
																			.getForm()
																			.submit(
																					{
																						url : configObject.modifyUrl,
																						success : function(
																								response,
																								options) {

																							check1
																									.disable();
																							check1.checked = true;

																							Tera.MessageBox
																									.alert('Modificacion Exitosa');

																						},
																						failure : function(
																								response,
																								options) {
																							var responseText = Tera
																									.decode(options.response.responseText);
																							Tera.MessageBox
																									.alert(
																											'<bean:message key="app.validaciones.errorModificacion"  bundle="messages"/>',
																											responseText.failure);
																						}

																					});

																}
															}

													);//Fin de message box		

										}
										;

										win.show(this);
									}
									; // Funcion callback-FIN  					
								}); // Final boton visualizar   

			});
</script>

<div id="panelSfaFisicoReintegros"></div>