<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelSfaMMiscelaneosFisico',
		panelId: 'panelSfaMMiscelaneosFisico',
		addUrl:			'../app/sfaMMiscelaneosFisico.do?method=altaSfaMMiscelaneosFisico',
		modifyUrl:		'../app/sfaMMiscelaneosFisico.do?method=modificarSfaMMiscelaneosFisico',
		removeUrl: 		'../app/sfaMMiscelaneosFisico.do?method=bajaSfaMMiscelaneosFisico',
		loadDataUrl:	'../app/sfaMMiscelaneosFisico.do?method=loadData',
		filterDataUrl:	'../app/sfaMMiscelaneosFisico.do?method=Buscar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/sfaMMiscelaneosFisico.do?method=Buscar',
			pagingToolbarUrl: 		'../app/sfaMMiscelaneosFisico.do?method=loadData',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			adabas_isnName: 'adabasisn'
		}
	};
	Tera.onReady(function(){

		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		
		
		
	      var labelresultado = new Tera.form.Label	(
	      {	
	         xtype: 'label', 
		 id: 'labelresultado',
	     	 cls: 'x-form-item myBold',
		 style: 'font-weight:bold;color:red;' 
 
	      }
	      );  
		
		
		
		
		
		
	    var store = new Tera.data.JsonStore({
	    	remoteSort: true,
	    	baseParams:{
	    	},	
	        url: configObject.loadDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codSfaMMiscelaneosFisico',
	        fields: [
	        	'adabas_isn'
				,
	        	'fa_m_zona'
				,
	        	'fa_m_oficina'
				,
	        	'fa_m_abonado'
				,
	        	'fa_m_tipo_factura'
				,
	        	'fa_m_anio_lote'
				,
	        	'fa_m_zona_pago'
				,
	        	'fa_m_fecha_pago'
				,
	        	'fa_m_fecha_imputacion'
				,
	        	'fa_m_fecha_proceso_tarea'
				,
	        	'fa_m_marca_tarea'
				,
	        	'fa_m_importe_miscelaneos'
				,
	        	'fa_m_estado_bajada'
				,
	        	'fa_m_concepto_fact'
				,
	        	'fa_m_zona_facturada'
				,
	        	'fa_m_oficina_facturada'
				,
	        	'fa_m_abonado_facturado'
				,
	        	'fa_m_anio_lote_facturado'
				,
	        	'fa_m_tipo_servicio_facturado'
				,
	        	'fa_m_fecha_alta'
				,
	        	'fa_m_usuario_alta'
				,
	        	'fa_m_fecha_envio'
				,
	        	'fa_m_usuario_envio'
				,
	        	'fa_m_fecha_baja'
				,
	        	'fa_m_usuario_baja'
				,
	        	'fa_m_fecha_vencimiento'
				,
	        	'fa_m_fecha_facturacion'
				,
	        	'fa_m_usuario_facturacion'
				,
	        	'fa_m_nro_reinteg'
				,
	        	'fa_m_cpto_zona'
				,
	        	'fa_m_cpto_oficina'
				,
	        	'fa_m_cpto_abonado'
				,
	        	'fa_m_cpto_tipo_fact'
				,
	        	'fa_m_cpto_anio_lote'
				,
	        	'fa_m_cpto_concepto'
				,
	        	'fa_m_cpto_cod_tercero'
				,
	        	'fa_m_cpto_estado'
				,
	        	'fa_m_cpto_tarea'
				,
	        	'fa_m_cpto_importe'
				,
	        	'fa_m_cpto_fecha_alta'
				,
	        	'fa_m_cpto_usuario_alta'
				,
	        	'fa_m_cpto_fecha_aplicacion'
				,
	        	'fa_m_cpto_usuario_aplicacion'
				,
	        	'fa_m_interurb'
				,
	        	'fa_m_urb'
				,
	        	'fa_m_linea'
				,
	        	'fa_m_ident_abo'
				,
	        	'fa_m_interurb_facturado'
				,
	        	'fa_m_urb_facturado'
				,
	        	'fa_m_linea_facturada'
	        ]
	    });
	
	 	function changeNumber(val){
        	var original=parseFloat(val);
			var result = original.toFixed(3);
        	if(val < 0){
 	           	return '<span style="color:red;">' + result + '</span>';
        	}
    
        	return result;
    	}
	
	    var cmSfaMMiscelaneosFisico = new Tera.grid.ColumnModel([
		    new Tera.grid.RowNumberer(),
		    {id: 'adabas_isn', header: '<bean:message key="sfa_m_miscelaneos_fisico.adabas_isn_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'adabas_isn', width: '<bean:message key="sfa_m_miscelaneos_fisico.adabas_isn_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_zona', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_zona_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_zona', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_zona_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_oficina', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_oficina_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_oficina', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_oficina_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_abonado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_abonado_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_abonado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_abonado_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_tipo_factura', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_tipo_factura_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_tipo_factura', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_tipo_factura_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_anio_lote', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_anio_lote_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_anio_lote', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_anio_lote_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_zona_pago', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_zona_pago_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_zona_pago', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_zona_pago_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_fecha_pago', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_pago_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_pago', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_pago_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_fecha_imputacion', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_imputacion_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_imputacion', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_imputacion_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_fecha_proceso_tarea', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_proceso_tarea_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_proceso_tarea', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_proceso_tarea_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_marca_tarea', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_marca_tarea_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_marca_tarea', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_marca_tarea_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_importe_miscelaneos', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_importe_miscelaneos_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_importe_miscelaneos', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_importe_miscelaneos_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_estado_bajada', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_estado_bajada_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_estado_bajada', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_estado_bajada_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_concepto_fact', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_concepto_fact_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_concepto_fact', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_concepto_fact_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_zona_facturada', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_zona_facturada_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_zona_facturada', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_zona_facturada_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_oficina_facturada', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_oficina_facturada_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_oficina_facturada', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_oficina_facturada_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_abonado_facturado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_abonado_facturado_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_abonado_facturado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_abonado_facturado_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_anio_lote_facturado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_anio_lote_facturado_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_anio_lote_facturado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_anio_lote_facturado_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_tipo_servicio_facturado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_tipo_servicio_facturado_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_tipo_servicio_facturado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_tipo_servicio_facturado_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_fecha_alta', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_alta_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_alta', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_alta_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_usuario_alta', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_alta_corto"  bundle="fields"/>', dataIndex: 'fa_m_usuario_alta', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_alta_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_fecha_envio', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_envio_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_envio', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_envio_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_usuario_envio', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_envio_corto"  bundle="fields"/>', dataIndex: 'fa_m_usuario_envio', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_envio_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_fecha_baja', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_baja_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_baja', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_baja_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_usuario_baja', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_baja_corto"  bundle="fields"/>', dataIndex: 'fa_m_usuario_baja', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_baja_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_fecha_vencimiento', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_vencimiento_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_vencimiento', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_vencimiento_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_fecha_facturacion', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_facturacion_corto"  bundle="fields"/>', dataIndex: 'fa_m_fecha_facturacion', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_facturacion_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_usuario_facturacion', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_facturacion_corto"  bundle="fields"/>', dataIndex: 'fa_m_usuario_facturacion', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_facturacion_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_nro_reinteg', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_nro_reinteg_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_nro_reinteg', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_nro_reinteg_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_zona', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_zona_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_zona', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_zona_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_oficina', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_oficina_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_oficina', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_oficina_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_abonado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_abonado_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_abonado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_abonado_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_tipo_fact', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_tipo_fact_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_tipo_fact', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_tipo_fact_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_anio_lote', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_anio_lote_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_anio_lote', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_anio_lote_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_concepto', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_concepto_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_concepto', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_concepto_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_cod_tercero', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_cod_tercero_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_cod_tercero', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_cod_tercero_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_estado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_estado_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_estado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_estado_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_tarea', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_tarea_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_tarea', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_tarea_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_importe', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_importe_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_cpto_importe', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_importe_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_cpto_fecha_alta', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_fecha_alta_corto"  bundle="fields"/>', dataIndex: 'fa_m_cpto_fecha_alta', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_fecha_alta_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_cpto_usuario_alta', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_usuario_alta_corto"  bundle="fields"/>', dataIndex: 'fa_m_cpto_usuario_alta', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_usuario_alta_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_cpto_fecha_aplicacion', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_fecha_aplicacion_corto"  bundle="fields"/>', dataIndex: 'fa_m_cpto_fecha_aplicacion', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_fecha_aplicacion_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_cpto_usuario_aplicacion', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_usuario_aplicacion_corto"  bundle="fields"/>', dataIndex: 'fa_m_cpto_usuario_aplicacion', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_cpto_usuario_aplicacion_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_interurb', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_interurb_corto"  bundle="fields"/>', dataIndex: 'fa_m_interurb', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_interurb_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_urb', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_urb_corto"  bundle="fields"/>', dataIndex: 'fa_m_urb', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_urb_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_linea', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_linea_corto"  bundle="fields"/>', dataIndex: 'fa_m_linea', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_linea_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_ident_abo', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_ident_abo_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fa_m_ident_abo', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_ident_abo_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fa_m_interurb_facturado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_interurb_facturado_corto"  bundle="fields"/>', dataIndex: 'fa_m_interurb_facturado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_interurb_facturado_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_urb_facturado', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_urb_facturado_corto"  bundle="fields"/>', dataIndex: 'fa_m_urb_facturado', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_urb_facturado_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fa_m_linea_facturada', header: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_linea_facturada_corto"  bundle="fields"/>', dataIndex: 'fa_m_linea_facturada', width: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_linea_facturada_corto"  bundle="fields"/>'.length * 10, align:'center'}
	    ]);
	    cmSfaMMiscelaneosFisico.defaultSortable = true;
	
	
	
			var bbar1 = new Tera.PagingToolbar({
			exportUrl: filterConfig.urls.exportToolbarUrl,
	        	exportFile: false,
	                exportGridName: 'SfaMMiscelaneosFisico',
	                pageSize: 10,
	                store: store,
	                displayInfo: false,
	                displayMsg: 'Mostrando   {0} - {1} de {2}',
	                emptyMsg: 'No existen registros a mostrar.'
	
			});
	
	
	
	
	    var gridSfaMMiscelaneosFisico = new Tera.grid.GridPanel({
	        width: 625,
	        height: 245,
	        store: store,
	        cm: cmSfaMMiscelaneosFisico,
	        trackMouseOver: true,
	        sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	        stripeRows: true,
	        viewConfig: {
    			forceFit: false,
	            enableRowBody: false
	        },
	           bbar: bbar1
	    });
	    
	    	    
	    

		gridSfaMMiscelaneosFisico.addListener('click', function(param) {
			if(gridSfaMMiscelaneosFisico.getSelectionModel().hasSelection()){
	   			buttonBaja.setDisabled(false);
	   			buttonModificar.setDisabled(false); 
	   			buttonVisualizar.setDisabled(false);
	    	} else{
	    		buttonBaja.setDisabled(true);
	   			buttonModificar.setDisabled(true);  
	   			buttonVisualizar.setDisabled(true);
	    	}
	    });

//store.load({params: {start: 0,limit: 10}}); 
	    
	    var buttonAlta = new Tera.Button({text: '<bean:message key="app.label.alta"  bundle="messages"/>', minWidth: 80});
	    var buttonBaja = new Tera.Button({text: '<bean:message key="app.label.baja"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonModificar = new Tera.Button({text: '<bean:message key="app.label.modificar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonVisualizar = new Tera.Button({text: '<bean:message key="app.label.visualizar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    	    
		var panel = new Tera.Panel({ //1
	    	id: configObject.panelId,
	    	renderTo: configObject.renderTo,
	    	border: false,
	        defaults: {
	        	border: false
		    },
		    items:[{ //2
	    	    border: false,
	    	    defaults: {
		        	border: false
			    },	    	    
	    	    items:[{
	    	    	layout:"column",
	    	    	border: false,
	    	    	defaults: {
	    	        	border: false
	    		    },
			        items:[
		{
			layout : "fit",
			border : false,
			style : 'padding: 0px 5px 0px 5px;' 
		}	 
		,
		{
			layout : "fit",
			border : false,
			style : 'padding: 0px 5px 0px 5px;',
			items : [ labelresultado  ]
		}
		,
	        	        
	        {
	        	layout:"fit",
	    	    border: false,	            
				style: 'padding: 0px 5px 0px 5px;',
	            items: [ gridSfaMMiscelaneosFisico ]
	        },{
	        	layout:"fit",
	    	    border: false,
				items:[{
				    layout:"column",
				    style: 'margin-top: 8px;',
				    defaults: {
				    	border: false
					},
					border: false,
				    items:[{
				        width:85,
				        items:[buttonAlta]
				      },{
				        width:85,
				        items:[buttonBaja]
				      },{
				        width:85,
				        items:[buttonModificar]
				      },{
				        width:85,
				        items:[buttonVisualizar]
				      }]
				  }]
			   }]	    			
	         }]
		   }] 
	}); 
			
   buttonAlta.on('click', function(){
	  
			var aceptar = new Tera.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				disabled: false, 
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmar"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_a"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
								form.getForm().submit({
				                	url: configObject.addUrl,
				                	success: function(response, options) {
				                		win.close();
		                				gridSfaMMiscelaneosFisico.getStore().load({callback: function(recordArray, opts, success){}});
				                	},
				                	failure: function(response, options) {
				                		var responseText = Tera.decode(options.response.responseText);
				                		Tera.MessageBox.alert('<bean:message key="app.validaciones.errorAlta"  bundle="messages"/>', responseText.failure);
			                	}
		                    });
					    }
					});
				}
			});
				
			var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80, handler: function() { win.close(); }});

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
					{
					 xtype:'datefield',
					 format: 'd/m/Y',
					 width: 98,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_alta_corto"  bundle="fields"/>',
					 name: 'fa_m_fecha_alta',
					 id: 'fa_m_fecha_alta',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
		             xtype: 'textfield',
					 width: 80,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_alta_corto"  bundle="fields"/>',
					 name: 'fa_m_usuario_alta',
					 id: 'fa_m_usuario_alta',
					 maxLength: 8,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
					 xtype:'datefield',
					 format: 'd/m/Y',
					 width: 98,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_envio_corto"  bundle="fields"/>',
					 name: 'fa_m_fecha_envio',
					 id: 'fa_m_fecha_envio',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
		             xtype: 'textfield',
					 width: 80,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_envio_corto"  bundle="fields"/>',
					 name: 'fa_m_usuario_envio',
					 id: 'fa_m_usuario_envio',
					 maxLength: 8,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
					 xtype:'datefield',
					 format: 'd/m/Y',
					 width: 98,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_baja_corto"  bundle="fields"/>',
					 name: 'fa_m_fecha_baja',
					 id: 'fa_m_fecha_baja',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
		             xtype: 'textfield',
					 width: 80,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_baja_corto"  bundle="fields"/>',
					 name: 'fa_m_usuario_baja',
					 id: 'fa_m_usuario_baja',
					 maxLength: 8,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
					 xtype:'datefield',
					 format: 'd/m/Y',
					 width: 98,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_facturacion_corto"  bundle="fields"/>',
					 name: 'fa_m_fecha_facturacion',
					 id: 'fa_m_fecha_facturacion',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
		             xtype: 'textfield',
					 width: 80,
					 fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_facturacion_corto"  bundle="fields"/>',
					 name: 'fa_m_usuario_facturacion',
					 id: 'fa_m_usuario_facturacion',
					 maxLength: 8,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				 ]         
			});
			
			var win = new Tera.Window({
		    	title: '<bean:message key="app.label.alta"  bundle="messages"/>',
		        border: false,
				modal: true,
				width: 288 + 50,
				height: 300 + 15,
				resizable: false,
				layout: 'fit',
				resizable: false,
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				items: form,
				buttons: [aceptar, cancelar]
			});
			win.show(this);
		});
	    
  buttonModificar.on('click', function(){
		    	if (!gridSfaMMiscelaneosFisico.getSelectionModel().hasSelection()) {
		    		return false;
		    	}
		    	var aceptar = new Tera.Button({
					text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
					minWidth: 80,
					disabled: false, 
					handler: function() {
				    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_m"  bundle="messages"/>', function(btn) {
				    		if ('yes' == btn) {
				    			form.getForm().submit({
									url: configObject.modifyUrl,
									success: function(response, options) {
										win.close();
		                				gridSfaMMiscelaneosFisico.getStore().load({callback: function(recordArray, opts, success){}});
									},
									failure: function(response, options) {
										var responseText = Tera.decode(options.response.responseText);
										Tera.MessageBox.alert('<bean:message key="app.validaciones.errorModificacion"  bundle="messages"/>', responseText.failure);
									}
								});
						    }
						});
					}
				});

		    	var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			    	
		 	    	
	    	    var recordToModify = gridSfaMMiscelaneosFisico.getSelectionModel().getSelected().data;

	 	        var form = new Tera.form.FormPanel({
	 				baseCls: 'x-plain',
	 				labelWidth: 130,
	 				monitorValid: true,
	 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
	 				items: [
 					    {
	 				     xtype:'datefield',
	 				     format: 'd/m/Y',
   		 			         	 				     width: 98,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_alta_corto"  bundle="fields"/>',
	 				     name: 'fa_m_fecha_alta',
	 				     id: 'fa_m_fecha_alta',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_fecha_alta 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 80,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_alta_corto"  bundle="fields"/>',
	 				     name: 'fa_m_usuario_alta',
	 				     id: 'fa_m_usuario_alta',
	 				     height: 21,
	 				     maxLength: 8,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_usuario_alta 
	 			        }
 				        ,
 					    {
	 				     xtype:'datefield',
	 				     format: 'd/m/Y',
   		 			         	 				     width: 98,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_envio_corto"  bundle="fields"/>',
	 				     name: 'fa_m_fecha_envio',
	 				     id: 'fa_m_fecha_envio',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_fecha_envio 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 80,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_envio_corto"  bundle="fields"/>',
	 				     name: 'fa_m_usuario_envio',
	 				     id: 'fa_m_usuario_envio',
	 				     height: 21,
	 				     maxLength: 8,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_usuario_envio 
	 			        }
 				        ,
 					    {
	 				     xtype:'datefield',
	 				     format: 'd/m/Y',
   		 			         	 				     width: 98,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_baja_corto"  bundle="fields"/>',
	 				     name: 'fa_m_fecha_baja',
	 				     id: 'fa_m_fecha_baja',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_fecha_baja 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 80,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_baja_corto"  bundle="fields"/>',
	 				     name: 'fa_m_usuario_baja',
	 				     id: 'fa_m_usuario_baja',
	 				     height: 21,
	 				     maxLength: 8,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_usuario_baja 
	 			        }
 				        ,
 					    {
	 				     xtype:'datefield',
	 				     format: 'd/m/Y',
   		 			         	 				     width: 98,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_facturacion_corto"  bundle="fields"/>',
	 				     name: 'fa_m_fecha_facturacion',
	 				     id: 'fa_m_fecha_facturacion',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_fecha_facturacion 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 80,
	 				     fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_facturacion_corto"  bundle="fields"/>',
	 				     name: 'fa_m_usuario_facturacion',
	 				     id: 'fa_m_usuario_facturacion',
	 				     height: 21,
	 				     maxLength: 8,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 8 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fa_m_usuario_facturacion 
	 			        }
 		 		]         
		 	});

 			var win = new Tera.Window({
 	        	title: '<bean:message key="app.label.modificacion"  bundle="messages"/>',
 			    modal: true,
 				width: 288 + 50,
 				height: 300+ 15,
 				resizable: false,
 			    layout: 'fit',
 			    plain:true,
 			    bodyStyle:'padding: 10px;',
 			    buttonAlign: 'right',
 				border: false,
 				items: [ form ],
 				buttons: [aceptar, cancelar]
 			});
 	        win.show();
	  });
	    
  buttonBaja.on('click', function(e){
	    	if (!gridSfaMMiscelaneosFisico.getSelectionModel().hasSelection()) { 
					return false;
	    	}

			var aceptar = new Tera.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_b"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
			    			form.getForm().submit({
								  url: configObject.removeUrl,
								  success: function(response, options) {
									win.close();
			      					gridSfaMMiscelaneosFisico.getStore().load({callback: function(recordArray, opts, success){}});
								  },
								  failure: function(response, options) {
									var responseText = Tera.decode(options.response.responseText);
									Tera.MessageBox.alert('<bean:message key="app.validaciones.errorBaja"  bundle="messages"/>', responseText.failure);
								  }
							});
					    }
					});
				}
			});
	      	
   			var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>' ,minWidth: 80, handler: function() { win.close(); }});
	    	
			var recordToShow = gridSfaMMiscelaneosFisico.getSelectionModel().getSelected().data;

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
				   {
					xtype:'textfield', format: 'd/m/Y', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_alta_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_alta',
					readOnly: true,
					name: 'fa_m_fecha_alta',
					height: 21,
					value: recordToShow.fa_m_fecha_alta 
				   }
				   ,
 				   {
		            xtype: 'textfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_alta_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_alta',
					readOnly: true,
					name: 'fa_m_usuario_alta',
					height: 21,
					value: recordToShow.fa_m_usuario_alta 
				   }
				   ,
 				   {
					xtype:'textfield', format: 'd/m/Y', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_envio_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_envio',
					readOnly: true,
					name: 'fa_m_fecha_envio',
					height: 21,
					value: recordToShow.fa_m_fecha_envio 
				   }
				   ,
 				   {
		            xtype: 'textfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_envio_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_envio',
					readOnly: true,
					name: 'fa_m_usuario_envio',
					height: 21,
					value: recordToShow.fa_m_usuario_envio 
				   }
				   ,
 				   {
					xtype:'textfield', format: 'd/m/Y', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_baja_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_baja',
					readOnly: true,
					name: 'fa_m_fecha_baja',
					height: 21,
					value: recordToShow.fa_m_fecha_baja 
				   }
				   ,
 				   {
		            xtype: 'textfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_baja_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_baja',
					readOnly: true,
					name: 'fa_m_usuario_baja',
					height: 21,
					value: recordToShow.fa_m_usuario_baja 
				   }
				   ,
 				   {
					xtype:'textfield', format: 'd/m/Y', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_facturacion_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_facturacion',
					readOnly: true,
					name: 'fa_m_fecha_facturacion',
					height: 21,
					value: recordToShow.fa_m_fecha_facturacion 
				   }
				   ,
 				   {
		            xtype: 'textfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_facturacion_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_facturacion',
					readOnly: true,
					name: 'fa_m_usuario_facturacion',
					height: 21,
					value: recordToShow.fa_m_usuario_facturacion 
				   }
 				]
			});
			
			var win = new Tera.Window({
				title: '<bean:message key="app.label.baja"  bundle="messages"/>',
				width: 288 + 50,
				height: 300 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [aceptar,cancelar]
			});
	        win.show(this);
			
	    });
	    
  buttonVisualizar.on('click', function(){
			if (!gridSfaMMiscelaneosFisico.getSelectionModel().hasSelection()){
				return false;
			}

			var cerrar = new Tera.Button({text: '<bean:message key="app.label.cerrar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			
			var recordToShow = gridSfaMMiscelaneosFisico.getSelectionModel().getSelected().data;

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				items: [
				   {
					xtype:'textfield', 
					format: 'd/m/Y', 
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_alta_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_alta',
					readOnly: true,
					name: 'fa_m_fecha_alta',
					height: 21,
					value: recordToShow.fa_m_fecha_alta 
				   }
 				   ,
				   {
		            xtype: 'textfield', 
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_alta_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_alta',
					readOnly: true,
					name: 'fa_m_usuario_alta',
					height: 21,
					value: recordToShow.fa_m_usuario_alta 
				   }
 				   ,
				   {
					xtype:'textfield', 
					format: 'd/m/Y', 
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_envio_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_envio',
					readOnly: true,
					name: 'fa_m_fecha_envio',
					height: 21,
					value: recordToShow.fa_m_fecha_envio 
				   }
 				   ,
				   {
		            xtype: 'textfield', 
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_envio_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_envio',
					readOnly: true,
					name: 'fa_m_usuario_envio',
					height: 21,
					value: recordToShow.fa_m_usuario_envio 
				   }
 				   ,
				   {
					xtype:'textfield', 
					format: 'd/m/Y', 
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_baja_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_baja',
					readOnly: true,
					name: 'fa_m_fecha_baja',
					height: 21,
					value: recordToShow.fa_m_fecha_baja 
				   }
 				   ,
				   {
		            xtype: 'textfield', 
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_baja_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_baja',
					readOnly: true,
					name: 'fa_m_usuario_baja',
					height: 21,
					value: recordToShow.fa_m_usuario_baja 
				   }
 				   ,
				   {
					xtype:'textfield', 
					format: 'd/m/Y', 
					width: 98,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_fecha_facturacion_corto"  bundle="fields"/>',
					id: 'fa_m_fecha_facturacion',
					readOnly: true,
					name: 'fa_m_fecha_facturacion',
					height: 21,
					value: recordToShow.fa_m_fecha_facturacion 
				   }
 				   ,
				   {
		            xtype: 'textfield', 
					width: 80,
					fieldLabel: '<bean:message key="sfa_m_miscelaneos_fisico.fa_m_usuario_facturacion_corto"  bundle="fields"/>',
					id: 'fa_m_usuario_facturacion',
					readOnly: true,
					name: 'fa_m_usuario_facturacion',
					height: 21,
					value: recordToShow.fa_m_usuario_facturacion 
				   }
 				]
			});

			
			var win = new Tera.Window({
 	        	title: '<bean:message key="app.label.visualizacion"  bundle="messages"/>',
				width: 288 + 50,
				height: 300 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [cerrar]
			});
	        win.show(this);
	    });
	    
	});
</script>
	
<div id="panelSfaMMiscelaneosFisico"></div>