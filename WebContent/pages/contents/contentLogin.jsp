<%@ page language="java" import="java.util.Properties" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<!-- CSS Resources -->
<link rel="stylesheet" type="text/css" href="<core:url value="/css/login.css"/>">

<script type="text/javascript" src="<core:url value="/pages/contents/contentLogin.js"/>"></script>

<% Properties prop = new Properties(); %>
<% prop.load(pageContext.getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF")); %>

<div style="padding-top: 195px; padding-left: 110px;" >
	<img src="./images/img_log3.jpg" />
	<div class="tabla_login" id="tabla_login">
		<table height="51" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td rowspan="2"><div id="loginFields"></div></td>
				<td style="padding-left: 15px" valign="top"><img src="./images/boton_acceder.gif" width="76" height="20" style="cursor:pointer" onclick="doLogin()"/></td>
			</tr>
			<tr>
				<td style="padding-left: 15px" valign="top"><img src="./images/boton_borrar.gif" width="76" height="20" style="cursor:pointer" onclick="doClear()"/></td>
			</tr>
		</table>
	</div>
	
<script language="javascript">
Tera.onReady(function() {
	
	  var mensaje = '<html:errors bundle="messages"/>';
	  if (mensaje != '') {
		   	Tera.MessageBox.show({
		        title:'Error',
		        msg: mensaje,
		        width: 480,
		        buttons: Tera.MessageBox.OK,
		        icon: Tera.MessageBox.ERROR
		    });
	  };
});
</script>
</div>
