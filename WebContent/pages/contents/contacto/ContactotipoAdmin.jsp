<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelContactotipo',
		panelId: 'panelContactotipo',
		addUrl:			'../app/contactotipo.do?method=altaContactotipo',
		modifyUrl:		'../app/contactotipo.do?method=modificarContactotipo',
		removeUrl: 		'../app/contactotipo.do?method=bajaContactotipo',
		loadDataUrl:	'../app/contactotipo.do?method=loadData',
		filterDataUrl:	'../app/contactotipo.do?method=filtrar',
		findDataUrl:	'../app/contactotipo.do?method=buscar'		
	};

 
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			idcontactotipoName: 'idcontactotipo'
		}
	};
	Tera.onReady(function(){

		
		
		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		 
//DEFINICION DE STORES - INICIO		

	     storeContacto = new Tera.data.JsonStore({
	    	remoteSort: false,
	    	baseParams:{
	   			idcontactotipoFilter: ''
				,
	   			descripcionFilter: ''
	    	},	
	        url: configObject.loadDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codContactotipo',
	        fields: [
	        	'idcontactotipo'
				,
	        	'descripcion'
	        ]
	    });
//DEFINICION DE STORES - FIN			
		
	var PanelDescripcion = new Tera.Panel({ 
         id:'IdPanelDescripcion', 
         height: 60,
         minWidth: 150, 
         iconCls: 'information',
         border: false, 
         renderTo: 'paneldescripcionContactotipo',
         frame:true,
         
        	 items: [{
        		 
                 html: "<p>Permite gestionar los tipos de contacto.</p>",
                 xtype: "panel"
             } ]
     });
		
		
		
///// FIN- FORMPANELBUSQUEDA  		
		
	var filtroBusqueda = new Tera.form.FormPanel({
		
		title:'Panel de Busqueda',
	    renderTo: configObject.renderTo,
	    bodyBorder: true,
        border: false,
        bodyStyle: 'padding:20px ',
        buttonAlign: 'left',
        labelWidth: 80,
        forceToUpper: true,
        labelAlign: 'left',
        layout: 'form',
        height: 120, 
        monitorValid:true,
        
        items: [ 				 
				{
	             xtype: 'textfield', 
				 width: 100,
				 border: true,
				 fieldLabel: '<bean:message key="contactotipo.descripcion_corto"  bundle="fields"/>',
				 name: 'descripcionFilter',
				 id: 'descripcionFilter',
				 height: 20,
				 maxLength: 100,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
				 allowBlank: false,
				 value:''
			    }
	 		 ],
	 		 
	 		buttons: [{
	 	        text:'',
	 	        id: 'buttonFiltrar',
	 	        type: 'submit',
	 	        style: 'margin-left: 10px;',
	 	   	    iconCls: 'zoom',
	 	        formBind:true ,   //If validation fails disable the button
	 	   
	 	      
	 	       
	 	        	handler: function (){
	  	        		filtrarGrilla(filtroBusqueda);
	 				}
	 	      },{
	 	          text: 'Limpiar',
	 	          id: 'btnlimpiar',
	 	          style: 'margin-left: 10px;',
	 	          handler: function(){
	 	        	 
	 	          filtroBusqueda.getForm().reset();
	 	          
	 	          
	 	          descripcionFilter = Tera.getCmp("codContactotipo");
	 	          descripcionFilter.setValue('');
	 	          //bbar1.store.removeAll();
 
	 	          
	 	          }
	 		},
	 		{
	 	          text: 'Cargar Todos',
	 	          id: 'btnMostrarTodos',
	 	          style: 'margin-left: 200px;',
	 	          
	 	          handler: function(){
	 	        	 
	 	        	 var id = 'idcliente';
	 	        	 var urlDestino = '../pages/contents/profesional/contentProfesional.jsp';
	 	        	 var  divName = 'newCliente';
	 	        	  
	 	        	 
	 	        	 //	 var serverPath = window.location.href; 
	 	            //   var serverPathIndex = serverPath.lastIndexOf("/"); 
	 	            //  window.open(serverPath.substring(0, serverPathIndex) + '/pages/contents/profesional/contentProfesional.jsp','_blank','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,width=700,height=600'); 

	 	        	 
	 	        	  mostrarFicha(divName, urlDestino, id);  
	 	        	  
	 	        	  
	 	        	  
	 	        /* 	  filtroBusqueda.getForm().reset();
	 	        	  buttonBaja.setDisabled(true);
	 		   		  buttonModificar.setDisabled(true); 
	 		   		  buttonVisualizar.setDisabled(true);	
		 	          
	 		   		  descripcionFilter = Tera.getCmp("descripcionFilter");
		 	          descripcionFilter.setValue('');
	 	        	  
		 	          store.load({params: {start: 0,limit: 10}});  */
	 	        		
	 	          
	 	          }
	 		}
	 	      
	 	      
	 	      
	 	      
	 	      
	 	      
	 	      ]
	 		 
	 	    });
		
///// FIN- FORMPANELBUSQUEDA  
	
	
	
	
	
	
	 	function changeNumber(val){
        	var original=parseFloat(val);
			var result = original.toFixed(3);
        	if(val < 0){
 	           	return '<span style="color:red;">' + result + '</span>';
        	}
    
        	return result;
    	}
    	
    	
///// INICIO - GRILLA  

	    var cmContactotipo = new Tera.grid.ColumnModel([
		    new Tera.grid.RowNumberer(),
		    {
		    	id: 'idcontactotipo', 
		    	header: '<bean:message key="contactotipo.idcontactotipo_corto"  bundle="fields"/>',
		    	dataIndex: 'idcontactotipo', 
		    	width: '<bean:message key="contactotipo.idcontactotipo_corto"  bundle="fields"/>'.length * 10, 
		    	align:'center'
		    },
		    {
				id: 'descripcion', 
				header: '<bean:message key="contactotipo.descripcion_corto"  bundle="fields"/>',
				dataIndex: 'descripcion', 
				width: '<bean:message key="contactotipo.descripcion_corto"  bundle="fields"/>'.length * 10, 
				align:'center'
		    }
	   
		    
		    ]);
		    
	    cmContactotipo.defaultSortable = true;
	
	
	
			var bbar1 = new Tera.PagingToolbar({
	       			id: 'bbar1',
				    pageSize: 10,
	                store: storeContacto,
	                loading: true,
	                displayInfo: true,
	                displayMsg: 'Mostrando   {0} - {1} de {2}',
	                emptyMsg: 'No existen registros a mostrar.' 

	
			});
	
			
	
			 
	
	    var gridContactotipo = new Tera.grid.GridPanel({
	        width: 625,
	        height: 245,
	        store: storeContacto,
	        cm: cmContactotipo,
	        trackMouseOver: true,
	        loadMask: true,
	        sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	        stripeRows: true,
	        viewConfig: {
    			forceFit: false,
	            enableRowBody: false
	        },
	           bbar: bbar1
	    });
	    
	    	    
	    

		gridContactotipo.addListener('click', function(param) {
			if(gridContactotipo.getSelectionModel().hasSelection()){
	   		 
				buttonBaja.setDisabled(false);
	   			buttonModificar.setDisabled(false); 
	   			buttonVisualizar.setDisabled(false);
	    	} else{
	    		desabilitarBotones();
	    	
	    	}
	    });
		
 
		 

		
		
	///// FIN - PANELGRILLA  	

	  	 var PanelGrilla= new Tera.Panel({ 
	         id:'IdPanelGrilla', 
	         minWidth: 150, 
	         border: false, 
	         bodyStyle: 'padding:15px ',
	         renderTo: configObject.renderTo,
	         
	        	 items: [ gridContactotipo ]
	     });
		

	  //oculto el boton del paggintoolbar 	 
	   Tera.getCmp('bbar1').loading.hide();	
       
	   
	   
	   storeContacto.load({params: {start: 0,limit: 10}}); 
	    
	    var buttonAlta = new Tera.Button(
   		{
   			text: '<bean:message key="app.label.alta"  bundle="messages"/>',
   			minWidth: 80,
   			height:80, 
   			iconCls: 'add'
   			
   		});
	    
	    var buttonBaja = new Tera.Button(
	    {
	    	text: '<bean:message key="app.label.baja"  bundle="messages"/>', 
	    	minWidth: 80, 
	    	disabled: true,
	    	iconCls: 'delete'
	    });
	    
	    var buttonModificar = new Tera.Button(
	    {
	    	text: '<bean:message key="app.label.modificar"  bundle="messages"/>',
	    	minWidth: 80, 
	    	disabled: true,
	    	height:80, 
	    	iconCls: 'update'
	    });
	    var buttonVisualizar = new Tera.Button(
    	{
    		text: '<bean:message key="app.label.visualizar"  bundle="messages"/>',
    		minWidth: 80, 
    		disabled: true,
    		iconCls: 'view'
    	});
	    	
	 	function desabilitarBotones(){
      
	 		buttonBaja.setDisabled(true);
   			buttonModificar.setDisabled(true); 
   			buttonVisualizar.setDisabled(true);	 		
	 		
	 		
    	}
	    
	    
	    
	    
		var FormBotones = new Tera.form.FormPanel({
	    	id: configObject.panelId,
	    	renderTo: configObject.renderTo,
	    	bodyStyle: 'padding:15px ',
	    	height: 100,
	    	border: false,
	        defaults: {
	        	border: false
		    },
		    items:[
		   {
	        	layout:"fit",
	    	    border: false,
	    	    height: 50,
				items:[{
				    layout:"column",
				    style: 'margin-left: 10px;',
				    defaults: {
				    	border: false
					},
					border: false,
				    items:[{
				        width:85,
				        items:[buttonAlta]
				      },{
				        width:85,
				        items:[buttonBaja]
				      },{
				        width:85,
				        items:[buttonModificar]
				      },{
				        width:85,
				        items:[buttonVisualizar]
				      }]
				  }]
			}]
	    });
			
   buttonAlta.on('click', function(){
	  
			var guardarAlta = new Tera.Button({
				text: '<bean:message key="app.label.guardar"  bundle="messages"/>', 
				minWidth: 80,
				iconCls: 'save',
				labelAlign: 'left',
				disabled: false, 
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmar"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_a"  bundle="messages"/>', 
			    	function(btn) {
			    		if ('yes' == btn) {
			    			formAlta.getForm().submit({
				                	url: configObject.addUrl,
				                	success: function(response, options) {
				                		winAlta.close();
				                		storeContacto.load({params: {start: 0,limit: 10}}); 
				                		desabilitarBotones();
				                	},
				                	failure: function(response, options) {
				                		var responseText = Tera.decode(options.response.responseText);
				                		Tera.MessageBox.alert('<bean:message key="app.validaciones.errorAlta"  bundle="messages"/>', responseTTera.failure);
			                	}
		                    });
					    }
					});
				}
			});
				
			var cancelarAlta = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80, handler: function()
			{
		      winAlta.close();
			}});

			var formAlta = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				
				labelWidth: 130,
				monitorValid: true,
				listeners : {'clientvalidation':
					function(pForm, pState){(
							(pState)?guardarAlta.enable():
								guardarAlta.disable())}},
				items: [
					
					{
		             xtype: 'textfield',
					 width: 100,
					 fieldLabel: '<bean:message key="contactotipo.descripcion_corto"  bundle="fields"/>',
					 name: 'descripcion',
					 id: 'descripcion',
					 maxLength: 100,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
				     value:''  ,          //Set the value so that the validation will trigger
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				 ]         
			});
			
			var winAlta = new Tera.Window({
		    	title: '<bean:message key="app.label.alta"  bundle="messages"/>',
		    	iconCls: 'add',
		        border: false,
				modal: true,
				width: 288 + 50,
				height: 150 + 15,
				resizable: false,
				layout: 'fit',
				resizable: false,
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				items: formAlta,
				buttons: [guardarAlta, cancelarAlta]
			});
			winAlta.show(this);
		});
	    
  buttonModificar.on('click', function(){
		    	if (!gridContactotipo.getSelectionModel().hasSelection()) {
		    		return false;
		    	}
		    	var guardarModifica = new Tera.Button({
					text: '<bean:message key="app.label.guardar"  bundle="messages"/>', 
					minWidth: 80,
					iconCls: 'save',
					disabled: false, 
					handler: function() {
				    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_m"  bundle="messages"/>', function(btn) {
				    		if ('yes' == btn) {
				    			formModificar.getForm().submit({
									url: configObject.modifyUrl,
									success: function(response, options) {
										winModifica.close();
										store.load({params: {start: 0,limit: 10}}); 
										desabilitarBotones();
									},
									failure: function(response, options) {
										var responseText = Tera.decode(options.response.responseText);
										Tera.MessageBox.alert('<bean:message key="app.validaciones.errorModificacion"  bundle="messages"/>', responseText.failure);
									}
								});
						    }
						});
					}
				});

		    	var cancelarModifica = new Tera.Button(
		    	{
		    	 text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',
		    	 minWidth: 80,handler : 
		    	 function(){	
		    		 		winModifica.close();
		    				}
		    	 });
			    	
		 	    	
	    	    var recordToModify = gridContactotipo.getSelectionModel().getSelected().data;

	 	        var formModificar = new Tera.form.FormPanel({
	 				baseCls: 'x-plain',
	 				labelWidth: 100,
	 				monitorValid: true,
	 				listeners : {'clientvalidation':function(pForm, pState){
	 					((pState)?
	 							guardarModifica.enable():
	 							guardarModifica.disable()
	 					)}},
	 				items: [
 					    {
 					     xtype: 'textfield', 
	 				     fieldLabel: '<bean:message key="contactotipo.idcontactotipo_corto"  bundle="fields"/>',
	 				     width: 100,
	 				     name: 'idcontactotipo',
	 				     id: 'idcontactotipo',
	 				     height: 21,
	 				     maxLength: 20,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: true,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.idcontactotipo 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 100,
	 				     fieldLabel: '<bean:message key="contactotipo.descripcion_corto"  bundle="fields"/>',
	 				     name: 'descripcion',
	 				     id: 'descripcion',
	 				     height: 21,
	 				     maxLength: 105,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.descripcion 
	 			        }
 		 		]         
		 	});

 			var winModifica = new Tera.Window({
 	        	title: '<bean:message key="app.label.modificacion"  bundle="messages"/>',
 	        	iconCls: 'update',
 			    modal: true,
 				width: 288 + 50,
 				height: 150+ 15,
 				resizable: false,
 			    layout: 'fit',
 			    plain:true,
 			    bodyStyle:'padding: 10px;',
 			    buttonAlign: 'right',
 				border: false,
 				items: [ formModificar ],
 				buttons: [guardarModifica, cancelarModifica]
 			});
 			winModifica.show();
	  });
	    
  buttonBaja.on('click', function(e){
	    	if (!gridContactotipo.getSelectionModel().hasSelection()) { 
					return false;
	    	}

			var guardarBaja = new Tera.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_b"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
			    			formBaja.getForm().submit({
								  url: configObject.removeUrl,
								  success: function(response, options) {
									winBaja.close();
									store.load({params: {start: 0,limit: 10}}); 
									desabilitarBotones();
								  },
								  failure: function(response, options) {
									var responseText = Tera.decode(options.response.responseText);
									Tera.MessageBox.alert('<bean:message key="app.validaciones.errorBaja"  bundle="messages"/>', responseText.failure);
								  }
							});
					    }
					});
				}
			});
	      	
   			var cancelarBaja = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>' ,minWidth: 80, handler: function()
   				{
   				winBaja.close();
   				}
   			});
	    	
			var recordToShow = gridContactotipo.getSelectionModel().getSelected().data;

			var formBaja = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
 				listeners : {'clientvalidation':function(pForm, pState){((pState)?guardarBaja.enable():guardarBaja.disable())}},
				items: [
			       {
	             	 xtype: 'textfield',
		  			 fieldLabel: '<bean:message key="contactotipo.idcontactotipo_corto"  bundle="fields"/>',
		  			 allowBlank: false,  
					 msgTarget: 'side', 
				     blankText: 'El campo es requerido',  
		  			 width: 98 + 17,
		  			 id: 'idcontactotipo',
		  			 name: 'idcontactotipo',
		  			 readOnly: true,
		  			 value: recordToShow.idcontactotipo
	  			   }
			       ,
 				   {
		            xtype: 'textfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 100,
					fieldLabel: '<bean:message key="contactotipo.descripcion_corto"  bundle="fields"/>',
					id: 'descripcion',
					readOnly: true,
					name: 'descripcion',
					height: 21,
					value: recordToShow.descripcion 
				   }
 				]
			});
			
			var winBaja = new Tera.Window({
				title: '<bean:message key="app.label.baja"  bundle="messages"/>',
				iconCls: 'delete',
				width: 288 + 50,
				height: 150 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ formBaja ],
				buttons: [guardarBaja,cancelarBaja]
			});
	        winBaja.show(this);
			
	    });
	    
  buttonVisualizar.on('click', function(){
			if (!gridContactotipo.getSelectionModel().hasSelection()){
				return false;
			}

			var cerrarVisualizar = new Tera.Button({text: '<bean:message key="app.label.cerrar"  bundle="messages"/>',minWidth: 80,handler : function(){	winVisualizar.close();}});
			
			var recordToShow = gridContactotipo.getSelectionModel().getSelected().data;

			var formVisualizar = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				items: [
			     
				   {
		            xtype: 'textfield', 
					width: 100,
					fieldLabel: '<bean:message key="contactotipo.descripcion_corto"  bundle="fields"/>',
					id: 'descripcion',
					readOnly: true,
					name: 'descripcion',
					height: 21,
					value: recordToShow.descripcion 
				   }
 				]
			});

			
			var winVisualizar = new Tera.Window({
 	        	title: '<bean:message key="app.label.visualizacion"  bundle="messages"/>',
 	        	iconCls: 'view',
				width: 288 + 50,
				height: 150 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ formVisualizar ],
				buttons: [cerrarVisualizar]
			});
			winVisualizar.show(this);
	    });
  
  
  
  
	function filtrarGrilla(form){
		if(!form.getForm().isValid())
			return;

		form.getForm().submit({
			waitMsg: '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
			url: configObject.filterDataUrl,
			success: function(response, options){
				var responseText = Tera.decode(options.response.responseText);
				if (responseText.data != 'undefined' || responseText.data != null){
					gridContactotipo.getStore().loadData(responseText);
									 
				}
			
			},
			failure: function(response, options){
				var responseText = Tera.decode(options.response.responseText);
				Tera.MessageBox.alert('<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>', responseText.failure); 
			}
		});
	}
	
	function mostrarFicha(divName, urlDestino, id){
  		
		var open=Tera.get(divName);
		
		if(!open){
			var msg = Tera.get('msg');
			msg.load({
				url: urlDestino,
				scripts: true,
				params: 'id='+id,
				text: 'Cargando...'
			});
			msg.show();
		}
	}

	
	
	
	
	
	
	
	
	});
</script>
	
<div id="paneldescripcionContactotipo"></div>	
<div id="panelContactotipo"></div>