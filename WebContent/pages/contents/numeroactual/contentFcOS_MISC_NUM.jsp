<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelFcOS_MISC_NUM',
		panelId: 'panelFcOS_MISC_NUM',
		addUrl:			'../app/fcOS_MISC_NUM.do?method=altaFcOS_MISC_NUM',
		modifyUrl:		'../app/fcOS_MISC_NUM.do?method=modificarFcOS_MISC_NUM',
		removeUrl: 		'../app/fcOS_MISC_NUM.do?method=bajaFcOS_MISC_NUM',
		loadDataUrl:	'../app/fcOS_MISC_NUM.do?method=loadData',
		filterDataUrl:	'../app/fcOS_MISC_NUM.do?method=buscar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/fcOS_MISC_NUM.do?method=buscar',
			pagingToolbarUrl: 		'../app/fcOS_MISC_NUM.do?method=loadData',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			adabas_isnName: 'adabasisn'
		}
	};
	Tera.onReady(function(){

		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		
		
		
		var labelresultado = new Tera.form.Label	(
				{
					xtype: 'label', 
				    id: 'labelresultado',
				   	cls: 'x-form-item myBold',
				   	style: 'font-weight:bold;' 
		 	   }
		      );  

		
		
		
		
		
		
		var filtroBusqueda = new Tera.form.FilterPanel({
			mappedFields: [ 
			   											
				'os_interurb_actFilter'
				,
				'os_urb_actFilter'
				,
				'os_linea_actFilter'
			],
			filterConfig: filterConfig,
	        bodyBorder: true,
	        border: false,
	        bodyStyle: 'padding:10px',
	        buttonAlign: 'right',
	        width: 650,
	        baseCls: 'x-plain',
	        labelWidth: 50,
	        labelAlign: 'top',
	        // keys: [{key: [10,13],handler: buscarsubmit}],//utilizado para que cuando aprete enter haga lo mismo que el boton buscar
	        bodyStyle:'padding:10px',
	        height: 100,
	        items: [
			///Primera Fila		                
			{
			    layout:'column',
			    border:false,
			    items:[{
			        columnWidth:.3,
			        layout: 'form',
			        border:false,
			       items: 
			    	    [{
			    	    	xtype: 'label', 
				            id: 'labelcriterios',
			    	        cls: 'x-form-item myBold',
			    	        style: 'font-weight:bold;',
				      	    text: 'Criterios de Búsqueda',
				            labelAlign: 'left'
			    	    }
			    	    ]
			   
			    },{
			        columnWidth:.3,
			        layout: 'form',
			        border:false
			
			    },{
			        columnWidth:.3,
			        layout: 'form',
			        border:false
			    }]
			},
			
	 		///segunda Fila        
			{
			layout:'column',
			border: false,
			items:[{
			columnWidth:.1,
			layout: 'form',
			border:false
			},{
			columnWidth:.3,
			layout: 'form',
			border:false,
			items: [
			      	{
		 	             xtype: 'textfield', 
		 				 width: 80,
		 				 fieldLabel: '<bean:message key="fcos_misc_num.os_interurb_act_corto"  bundle="fields"/>',
		 				 name: 'os_interurb_actFilter',
		 				 id: 'os_interurb_actFilter',
		 				 height: 21,
		 				 maxLength: 4,
		 				 maxLengthText: 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',					                  
		 				 msgTarget: 'side',
		 				 allowBlank: false
							
					}
			        ]
			
			},{
			columnWidth:.3,
			layout: 'form',
			border:false,
				
			items: [ 	
			    {
	 		         xtype: 'textfield', 
	 				 width: 80,
	 				 fieldLabel: '<bean:message key="fcos_misc_num.os_urb_act_corto"  bundle="fields"/>',
	 				 name: 'os_urb_actFilter',
	 				 id: 'os_urb_actFilter',
	 				 height: 21,
	 				 maxLength: 4,
	 				 maxLengthText: 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
	 				 msgTarget: 'side',
	 				 allowBlank: false
			    
			    }
			
			]
			}
			,{
			columnWidth:.3,
			layout: 'form',
			border:false,
				
			items: [ 	
					{
		 	             xtype: 'textfield', 
		 				 width: 80,
		 				 fieldLabel: '<bean:message key="fcos_misc_num.os_linea_act_corto"  bundle="fields"/>',
		 				 name: 'os_linea_actFilter',
		 				 id: 'os_linea_actFilter',
		 				 height: 21,
		 				 maxLength: 4,
		 				 maxLengthText: 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
		 			    msgTarget: 'side',
					    allowBlank: false
					}
			  
			]
			}
			]
			
			}
	      
 
	 		 ]
	    });
		
		
	
		
		
		
		
		
		
	    var store = new Tera.data.JsonStore({
	    	remoteSort: true,
	    	baseParams:{
	   			os_urb_actFilter: ''
				,
	   			os_interurb_actFilter: ''
				,
	   			os_linea_actFilter: ''
	    	},	
	        url: configObject.filterDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codFcOS_MISC_NUM',
	        fields: [
	        	'adabas_isn'
				,
	        	'os_cod_concepto'
				,
	        	'os_marca_cl'
				,
	        	'os_descripcion'
				,
	        	'os_act_u_clase'
				,
	        	'os_num_sec_num'
				,
	        	'os_num_os_misc'
				,
	        	'os_interurb_ant'
				,
	        	'os_estado_renm'
				,
	        	'os_act_est_medidor'
				,
	        	'os_importe'
				,
	        	'os_urb_act'
				,
	        	'os_estado_misc'
				,
	        	'os_abonado'
				,
	        	'os_fecha_cambio_renm'
				,
	        	'os_facturado'
				,
	        	'os_u_medida'
				,
	        	'os_oficina'
				,
	        	'os_urb_ant'
				,
	        	'os_periodo_misc'
				,
	        	'os_fecha_expir'
				,
	        	'os_cod_mov_num'
				,
	        	'os_num_sec_misc'
				,
	        	'os_cod_mov_renm'
				,
	        	'os_linea_ant'
				,
	        	'os_ant_u_clase'
				,
	        	'os_interurb_act'
				,
	        	'os_ant_cod_clie'
				,
	        	'os_anio_misc'
				,
	        	'os_act_cod_clie'
				,
	        	'os_ident_abo'
				,
	        	'os_num_os_renm'
				,
	        	'os_num_os_num'
				,
	        	'os_num_sec_renm'
				,
	        	'os_cod_mov_misc'
				,
	        	'os_importe_mensual'
				,
	        	'os_zona'
				,
	        	'os_act_oficina'
				,
	        	'os_act_zona'
				,
	        	'os_ant_zona'
				,
	        	'os_vto_misc'
				,
	        	'os_ant_est_medidor'
				,
	        	'os_act_abonado'
				,
	        	'os_fecha_empalme'
				,
	        	'os_fecha_expir_renm'
				,
	        	'os_fecha_cambio'
				,
	        	'os_fecha_cumplim'
				,
	        	'os_ant_oficina'
				,
	        	'os_ant_abonado'
				,
	        	'os_estado_num'
				,
	        	'os_linea_act'
				,
	        	'os_fecha_bajada_misc'
	        ]
	    });
	
	 	function changeNumber(val){
        	var original=parseFloat(val);
			var result = original.toFixed(3);
        	if(val < 0){
 	           	return '<span style="color:red;">' + result + '</span>';
        	}
    
        	return result;
    	}
	
	    var cmFcOS_MISC_NUM = new Tera.grid.ColumnModel([
		    new Tera.grid.RowNumberer(),
		    
		    {id: 'os_interurb_ant', header: '<bean:message key="fcos_misc_num.os_interurb_ant_corto"  bundle="fields"/>', dataIndex: 'os_interurb_ant', width: '<bean:message key="fcos_misc_num.os_interurb_ant_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'os_urb_ant', header: '<bean:message key="fcos_misc_num.os_urb_ant_corto"  bundle="fields"/>', dataIndex: 'os_urb_ant', width: '<bean:message key="fcos_misc_num.os_urb_ant_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'os_linea_ant', header: '<bean:message key="fcos_misc_num.os_linea_ant_corto"  bundle="fields"/>', dataIndex: 'os_linea_ant', width: '<bean:message key="fcos_misc_num.os_linea_ant_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'os_ident_abo', header: '<bean:message key="fcos_misc_num.os_ident_abo_corto"  bundle="fields"/>', dataIndex: 'os_ident_abo', width: '<bean:message key="fcos_misc_num.os_ident_abo_corto"  bundle="fields"/>'.length * 10, align:'center'}		   
		    ,
		    {id: 'os_num_os_num', header: '<bean:message key="fcos_misc_num.os_num_os_num_corto"  bundle="fields"/>', dataIndex: 'os_num_os_num', width: '<bean:message key="fcos_misc_num.os_num_os_num_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'os_fecha_cambio', header: '<bean:message key="fcos_misc_num.os_fecha_cambio_corto"  bundle="fields"/>', dataIndex: 'os_fecha_cambio', width: '<bean:message key="fcos_misc_num.os_fecha_cambio_corto"  bundle="fields"/>'.length * 10, align:'center'}
			
	    ]);
	    cmFcOS_MISC_NUM.defaultSortable = true;
	
	    
		var bbar1 = new Tera.PagingToolbar({
			filterPanel: filtroBusqueda,
        	exportFile: false,
            exportGridName: 'FcOS_MISC_NUM',
            pageSize: 10,
            store: store,
            displayInfo: false,
            displayMsg: 'Mostrando   {0} - {1} de {2}',
            emptyMsg: 'No existen registros a mostrar.'
		});
	    
	    var gridFcOS_MISC_NUM = new Tera.grid.GridPanel({
	        width: 625,
	        height: 245,
	        store: store,
	        cm: cmFcOS_MISC_NUM,
	        trackMouseOver: true,
	        sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	        stripeRows: true,
	        viewConfig: {
    			forceFit: false,
	            enableRowBody: false
	        },
	        bbar: bbar1
	    });

	    
	    
	    
	    
	    
		gridFcOS_MISC_NUM.addListener('click', function(param) {
			 gridFcOS_MISC_NUM.getSelectionModel().hasSelection();
	   	 
	    });
       	    
	    var buttonFiltrar = new Tera.Button({
	        text: 'Buscar',
			minWidth: 80,
			handler: FiltrarSubmit
		});
	    
	    
	    
	    function FiltrarSubmit() {
			
			// Si algún campo tiene algún error 
			if( filtroBusqueda.getForm().isValid()==false)
				{
						
			Tera.Msg
			.show({
				title : 'Información', //<- el título del diálogo   
				msg : 'Verifique la longitud de los campos que están señalizados en color rojo.', //<- El mensaje   
				buttons : Tera.Msg.OK, //<- Botones de SI y NO   
				icon : Tera.Msg.INFO, // <- un ícono de error   
				fn : this.callback
			//<- la función que se ejecuta cuando se da clic   
			});
			
	
				}
			
			
			
			
    	//Evaluo que los campos requeridos sean completados, si alguno esta vacio informo 
    	//  un mensaje de error;
        if(Tera.getCmp('os_interurb_actFilter').getValue()==""   
       	|| Tera.getCmp('os_urb_actFilter').getValue()==""         
        || Tera.getCmp('os_linea_actFilter').getValue()=="")                    
        
    	{
    
         ///Creacion de una mensaje de usuario personalizado
         Tera.Msg.show({   
         title: 'Información', //<- el título del diálogo   
         msg: 'Para realizar la consulta, por favor ingrese Interurbano, Urbano y Línea.', //<- El mensaje   
         buttons: Tera.Msg.OK, //<- Botones de SI y NO   
         icon: Tera.Msg.INFO, // <- un ícono de error   
              fn: this.callback //<- la función que se ejecuta cuando se da clic   
        });   
                                
              return false;
         }

			filtroBusqueda.submit({
				waitMsg: '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
				url: configObject.filterDataUrl,
				success: function(response, options){
					var responseText = Tera.decode(options.response.responseText);
					if (responseText.data != 'undefined' || responseText.data != null){
						gridFcOS_MISC_NUM.getStore().loadData(responseText);
			   			 store.baseParams.os_urb_actFilter = Tera.getCmp('os_urb_actFilter').getValue();
			   			 store.baseParams.os_interurb_actFilter = Tera.getCmp('os_interurb_actFilter').getValue();
			   			 store.baseParams.os_linea_actFilter = Tera.getCmp('os_linea_actFilter').getValue();
					}
				  		
					
					//Evaluo si el resultado de la consulta y muestro la leyenda
	                var cant = 0 ;
					cant = 	store.getTotalCount();
	                
		             if (cant > 0)
	                 {
	                	labelresultado.setText(' Resultado de la consulta : ' );
		     			
		            												
	                 }
	                else
	                	
	                 {
	                  	
	                	labelresultado.setText('<font color="#FF0000">'+ 'No existen datos que cumplan con los criterios de búsqueda ingresados.' +'</font>',false);
	                 	
	                
	                 }
					
					
					
				},
				failure: function(response, options){
					var responseText = Tera.decode(options.response.responseText);
					Tera.MessageBox.alert('<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>', responseText.failure); 
				}
			});
		}
	    
	    
	    
	    
	 // Función para que cuando aprete enter realice la misma accion que el boton filtrar

		var map = new Tera.KeyMap(Tera.getBody(), {
		    key: Tera.EventObject.ENTER,
		    fn: function(){
		        FiltrarSubmit();
		    },
		    scope: this
		});
	    
	    
	    var buttonLimpiar = new Tera.Button({text: '<bean:message key="app.label.limpiar"  bundle="messages"/>',
			minWidth: 80,
			handler: function(){
			
				//reseteo los campos TextField del formulario 
				filtroBusqueda.getForm().reset();
	 
				
				//Blanqueo ellabel de resultado
			 	labelresultado.setText(' '); 
				//vacio el Store		
			    bbar1.store.removeAll();
				
  				//Reseteo la barra del grid PagingToolbar
  	  			bbar1.field.dom.value = '1';
			    bbar1.afterTextEl.el.innerHTML = 'de 1'; 
			     
			    bbar1.first.setDisabled(true);
				bbar1.prev.setDisabled(true);
				bbar1.next.setDisabled(true);
				bbar1.last.setDisabled(true); 
			 	
			
			 	bbar1.updateInfo();
				
			}
		});
	    
	   
	    
	    
	    
	    
	    
	    
		var panel = new Tera.Panel({
	    	id: configObject.panelId,
	    	renderTo: configObject.renderTo,
	    	border: false,
	        defaults: {
	        	border: false
		    },
		    items:[{
	    	    border: false,
	    	    defaults: {
		        	border: false
			    },	    	    
	    	    items:[{
	    	    	layout:"column",
	    	    	border: false,
	    	    	defaults: {
	    	        	border: false
	    		    },
			        items:[
			        {
						columnWidth: 1,
					    items : [filtroBusqueda]
					},{
						width: 113,
						items: [{
					        border: false,
					    	defaults: {
					    		border: false
					    	},
							items: [
								{ items: [buttonFiltrar], style: 'margin-top: 10px;' },
								{ items: [buttonLimpiar], style: 'margin-top: 3px;' }
							]
						}]
					}]
				}]
	        },
			{
				layout : "fit",
				border : false,
				style : 'padding: 0px 5px 0px 5px;' 
			}	 
			,
			{
				layout : "fit",
				border : false,
				style : 'padding: 0px 5px 0px 5px;',
				items : [ labelresultado  ]
			}
			,
			{
				layout : "fit",
				border : false,
				style : 'padding: 0px 5px 0px 5px;',
				items : [ gridFcOS_MISC_NUM  ]
			}
		 
	        
	        ]
	    });
			

	    
	});
</script>
	
<div id="panelFcOS_MISC_NUM"></div>