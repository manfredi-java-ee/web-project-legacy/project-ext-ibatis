<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelLocalidad',
		panelId: 'panelLocalidad',
		addUrl:			'../app/localidad.do?method=altaLocalidad',
		modifyUrl:		'../app/localidad.do?method=modificarLocalidad',
		removeUrl: 		'../app/localidad.do?method=bajaLocalidad',
		loadDataUrl:	'../app/localidad.do?method=loadData',
		filterDataUrl:	'../app/localidad.do?method=buscar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/localidad.do?method=buscar',
			pagingToolbarUrl: 		'../app/localidad.do?method=loadData',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			idlocalidadName: 'idlocalidad'
		}
	};
	Tera.onReady(function(){

		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		
		
		
	      var labelresultado = new Tera.form.Label	(
	      {	
	         xtype: 'label', 
		 id: 'labelresultado',
	     	 cls: 'x-form-item myBold',
		 style: 'font-weight:bold;color:red;' 
 
	      }
	      );  
		
		
		
		
		
		
		var filtroBusqueda = new Tera.form.FilterPanel({
			mappedFields: [ 
				'idlocalidadFilter'
				,
				'nombreFilter'
				,
				'idprovinciaFilter'
				,
				'codigopostalFilter'
			],
			filterConfig: filterConfig,
	        bodyBorder: false,
	        border: false,
	        bodyStyle: 'padding:10px',
	        buttonAlign: 'right',
	        width: 550,
	        baseCls: 'x-plain',
	        labelWidth: 130,
	        labelAlign: 'top',
	        height: 230,
	        items: [
 				{
             	 xtype: 'lupafield',
	  			 fieldLabel: '<bean:message key="localidad.idlocalidad_corto"  bundle="fields"/>',
				 width: 98 + 17,
	  			 id: 'idlocalidadFilter',
	  			 name: 'idlocalidadFilter',
	  			 limitDescription:30,	
	  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
	             url: lupaConfig.urls.url,
	             entityName: lupaConfig.entities.idlocalidadName
  				}
         		,
				{
	             xtype: 'textfield', 
				 width: 20,
				 fieldLabel: '<bean:message key="localidad.nombre_corto"  bundle="fields"/>',
				 name: 'nombreFilter',
				 id: 'nombreFilter',
				 height: 21,
				 maxLength: 1,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres'
			    }
			    ,
				{
				 width: 98,
				 fieldLabel: '<bean:message key="localidad.idprovincia_corto"  bundle="fields"/>',
				 name: 'idprovinciaFilter',
				 id: 'idprovinciaFilter',
				 height: 21,
				 maxLength: ,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres'
			    }
			    ,
				{
				 width: 98,
				 fieldLabel: '<bean:message key="localidad.codigopostal_corto"  bundle="fields"/>',
				 name: 'codigopostalFilter',
				 id: 'codigopostalFilter',
				 height: 21,
				 maxLength: ,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres'
			    }
	 		 ]
	    });
	    var store = new Tera.data.JsonStore({
	    	remoteSort: true,
	    	baseParams:{
	   			idlocalidadFilter: ''
				,
	   			nombreFilter: ''
				,
	   			idprovinciaFilter: ''
				,
	   			codigopostalFilter: ''
	    	},	
	        url: configObject.loadDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codLocalidad',
	        fields: [
	        	'idlocalidad'
				,
	        	'nombre'
				,
	        	'idprovincia'
				,
	        	'codigopostal'
	        ]
	    });
	
	 	function changeNumber(val){
        	var original=parseFloat(val);
			var result = original.toFixed(3);
        	if(val < 0){
 	           	return '<span style="color:red;">' + result + '</span>';
        	}
    
        	return result;
    	}
	
	    var cmLocalidad = new Tera.grid.ColumnModel([
		    new Tera.grid.RowNumberer(),
		    {id: 'idlocalidad', header: '<bean:message key="localidad.idlocalidad_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'idlocalidad', width: '<bean:message key="localidad.idlocalidad_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'nombre', header: '<bean:message key="localidad.nombre_corto"  bundle="fields"/>', dataIndex: 'nombre', width: '<bean:message key="localidad.nombre_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
	    ]);
	    cmLocalidad.defaultSortable = true;
	
	
	
			var bbar1 = new Tera.PagingToolbar({
	        	filterPanel: filtroBusqueda,
	        	exportFile: false,
	                exportGridName: 'Localidad',
	                pageSize: 10,
	                store: store,
	                displayInfo: false,
	                displayMsg: 'Mostrando   {0} - {1} de {2}',
	                emptyMsg: 'No existen registros a mostrar.'
	
			});
	
	
	
	
	    var gridLocalidad = new Tera.grid.GridPanel({
	        width: 625,
	        height: 245,
	        store: store,
	        cm: cmLocalidad,
	        trackMouseOver: true,
	        sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	        stripeRows: true,
	        viewConfig: {
    			forceFit: false,
	            enableRowBody: false
	        },
	           bbar: bbar1
	    });
	    
	    	    
	    

		gridLocalidad.addListener('click', function(param) {
			if(gridLocalidad.getSelectionModel().hasSelection()){
	   			buttonBaja.setDisabled(false);
	   			buttonModificar.setDisabled(false); 
	   			buttonVisualizar.setDisabled(false);
	    	} else{
	    		buttonBaja.setDisabled(true);
	   			buttonModificar.setDisabled(true);  
	   			buttonVisualizar.setDisabled(true);
	    	}
	    });

//store.load({params: {start: 0,limit: 10}}); 
	    
	    var buttonAlta = new Tera.Button({text: '<bean:message key="app.label.alta"  bundle="messages"/>', minWidth: 80});
	    var buttonBaja = new Tera.Button({text: '<bean:message key="app.label.baja"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonModificar = new Tera.Button({text: '<bean:message key="app.label.modificar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonVisualizar = new Tera.Button({text: '<bean:message key="app.label.visualizar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    	    
	    var buttonFiltrar = new Tera.Button({
	        text: 'Filtrar',
			minWidth: 80,
			handler: 
		});
		
		
		
		
		
		
		    function FiltrarSubmit() {
				
					// Si alg�n campo tiene alg�n error 
			if( filtroBusqueda.getForm().isValid()==false)
				{
						
			Tera.Msg
			.show({
				title : 'Informaci�n', //<- el t�tulo del di�logo   
				msg : 'Verifique la longitud de los campos que est�n se�alizados en color rojo.', //<- El mensaje   
				buttons : Tera.Msg.OK, //<- Botones de SI y NO   
				icon : Tera.Msg.INFO, // <- un �cono de error   
				fn : this.callback
			//<- la funci�n que se ejecuta cuando se da clic   
			});
			
	
				}
				
				
			      // Ejemplo para evaluar los campos requeridos  
			      //Evaluo que los campos requeridos sean completados, si alguno esta vacio informo 
			      //  un mensaje de error;
			      //     if(Tera.getCmp('os_interurb_actFilter').getValue()==""   
			      //  	|| Tera.getCmp('os_urb_actFilter').getValue()=="")         
			      //  || Tera.getCmp('os_linea_actFilter').getValue()=="")                    
			      //	{
			      //Creacion de una mensaje de usuario personalizado
			      //   Tera.Msg.show({   
			      //  title: 'Informaci�n', //<- el t�tulo del di�logo   
			      //  msg: 'Debe completar los campos requeridos para realizar la consulta', //<- El mensaje   
			      //  buttons: Tera.Msg.OK, //<- Botones de SI y NO   
			      //  icon: Tera.Msg.INFO, // <- un �cono de error   
			      //         fn: this.callback //<- la funci�n que se ejecuta cuando se da clic   
			      //  });   
			      //        return false;
			      //   }
					
		
		
				filtroBusqueda.submit({
					waitMsg: '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
					url: configObject.filterDataUrl,
					success: function(response, options){
						var responseText = Tera.decode(options.response.responseText);
						if (responseText.data != 'undefined' || responseText.data != null){
							gridLocalidad.getStore().loadData(responseText);
				   			 store.baseParams.idlocalidadFilter = Tera.getCmp('idlocalidadFilter').getValue();
				   			 store.baseParams.nombreFilter = Tera.getCmp('nombreFilter').getValue();
				   			 store.baseParams.idprovinciaFilter = Tera.getCmp('idprovinciaFilter').getValue();
				   			 store.baseParams.codigopostalFilter = Tera.getCmp('codigopostalFilter').getValue();
						}
						
						
						
						//Evaluo si el resultado de la consulta y muestro la leyenda
				                var cant = store.getTotalCount();
				                
				                if (cant>0)
				                 {
					            	labelresultado.setText(' Resultado de la consulta : ' );
				     															
				                 }
				                else
				                	
				                 {
				                	
				                	labelresultado.setText('<font color="#FF0000">'+ 'No existen datos que cumplan con los criterios de b�squeda ingresados.' +'</font>',false);
				     				
				                	
				                 }	
						
						
						
						
						
						
						
						
					},
					failure: function(response, options){
						var responseText = Tera.decode(options.response.responseText);
						Tera.MessageBox.alert('<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>', responseText.failure); 
					}
				});
			}
		
		
		
		
		
		
		
		
		
	 // Funci�n para que cuando aprete enter realice la misma accion que el boton filtrar

		var map = new Tera.KeyMap(Tera.getBody(), {
		    key: Tera.EventObject.ENTER,
		    fn: function(){
		        FiltrarSubmit();
		    },
		    scope: this
		});		
		
		
		
		
		
	    
	    var buttonLimpiar = new Tera.Button({text: '<bean:message key="app.label.limpiar"  bundle="messages"/>',
			minWidth: 80,
			handler: function(){
		   		store.baseParams.idlocalidadFilter = '';
		   		store.baseParams.nombreFilter = '';
		   		store.baseParams.idprovinciaFilter = '';
		   		store.baseParams.codigopostalFilter = '';
		
		
			//reseteo los campos TextField del formulario 
			
			filtroBusqueda.getForm().reset();
		 
					
			//Blanqueo el label de resultado
		 	
		 	labelresultado.setText(' '); 
		
			filtroBusqueda.clearForm();
	    		gridLocalidad.getStore().load();
			
			//Reseteo la barra del grid PagingToolbar
	  			
	  		    bbar1.field.dom.value = '1';
			    bbar1.afterTextEl.el.innerHTML = 'de 1'; 
			    bbar1.store.removeAll();
			    bbar1.first.setDisabled(true);
			    bbar1.prev.setDisabled(true);
			    bbar1.next.setDisabled(true);
			    bbar1.last.setDisabled(true); 
			 
			    bbar1.updateInfo();
			
		
		
		
			}
		});
		var panel = new Tera.Panel({
	    	id: configObject.panelId,
	    	renderTo: configObject.renderTo,
	    	border: false,
	        defaults: {
	        	border: false
		    },
		    items:[{
	    	    border: false,
	    	    defaults: {
		        	border: false
			    },	    	    
	    	    items:[{
	    	    	layout:"column",
	    	    	border: false,
	    	    	defaults: {
	    	        	border: false
	    		    },
			        items:[
			        {
						columnWidth: 1,
					    items : [filtroBusqueda]
					},{
						width: 113,
						items: [{
					        border: false,
					    	defaults: {
					    		border: false
					    	},
							items: [
								{ items: [buttonFiltrar], style: 'margin-top: 10px;' },
								{ items: [buttonLimpiar], style: 'margin-top: 3px;' }
							]
						}]
					}]
				}]
	        },
		{
			layout : "fit",
			border : false,
			style : 'padding: 0px 5px 0px 5px;' 
		}	 
		,
		{
			layout : "fit",
			border : false,
			style : 'padding: 0px 5px 0px 5px;',
			items : [ labelresultado  ]
		}
		,
	        	        
	        {
	        	layout:"fit",
	    	    border: false,	            
				style: 'padding: 0px 5px 0px 5px;',
	            items: [ gridLocalidad ]
	        },{
	        	layout:"fit",
	    	    border: false,
				items:[{
				    layout:"column",
				    style: 'margin-top: 8px;',
				    defaults: {
				    	border: false
					},
					border: false,
				    items:[{
				        width:85,
				        items:[buttonAlta]
				      },{
				        width:85,
				        items:[buttonBaja]
				      },{
				        width:85,
				        items:[buttonModificar]
				      },{
				        width:85,
				        items:[buttonVisualizar]
				      }]
				  }]
			}]
	    });
			
   buttonAlta.on('click', function(){
	  
			var aceptar = new Tera.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				disabled: false, 
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmar"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_a"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
								form.getForm().submit({
				                	url: configObject.addUrl,
				                	success: function(response, options) {
				                		win.close();
			                			buttonFiltrar.handler.call(buttonFiltrar.scope);
				                	},
				                	failure: function(response, options) {
				                		var responseText = Tera.decode(options.response.responseText);
				                		Tera.MessageBox.alert('<bean:message key="app.validaciones.errorAlta"  bundle="messages"/>', responseText.failure);
			                	}
		                    });
					    }
					});
				}
			});
				
			var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80, handler: function() { win.close(); }});

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
					{
	    	         xtype: 'numberfield',
					 width: 98,
					 fieldLabel: '<bean:message key="localidad.idlocalidad_corto"  bundle="fields"/>',
					 name: 'idlocalidad',
					 id: 'idlocalidad',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
		             xtype: 'textfield',
					 width: 20,
					 fieldLabel: '<bean:message key="localidad.nombre_corto"  bundle="fields"/>',
					 name: 'nombre',
					 id: 'nombre',
					 maxLength: 1,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
					 width: 98,
					 fieldLabel: '<bean:message key="localidad.idprovincia_corto"  bundle="fields"/>',
					 name: 'idprovincia',
					 id: 'idprovincia',
					 maxLength: ,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				    ,
					{
					 width: 98,
					 fieldLabel: '<bean:message key="localidad.codigopostal_corto"  bundle="fields"/>',
					 name: 'codigopostal',
					 id: 'codigopostal',
					 maxLength: ,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
					 height: 21
					 ,allowBlank: false,
					 msgTarget: 'side', 
					 blankText: 'El campo es requerido' 
				    }
 				 ]         
			});
			
			var win = new Tera.Window({
		    	title: '<bean:message key="app.label.alta"  bundle="messages"/>',
		        border: false,
				modal: true,
				width: 288 + 50,
				height: 200 + 15,
				resizable: false,
				layout: 'fit',
				resizable: false,
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				items: form,
				buttons: [aceptar, cancelar]
			});
			win.show(this);
		});
	    
  buttonModificar.on('click', function(){
		    	if (!gridLocalidad.getSelectionModel().hasSelection()) {
		    		return false;
		    	}
		    	var aceptar = new Tera.Button({
					text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
					minWidth: 80,
					disabled: false, 
					handler: function() {
				    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_m"  bundle="messages"/>', function(btn) {
				    		if ('yes' == btn) {
				    			form.getForm().submit({
									url: configObject.modifyUrl,
									success: function(response, options) {
										win.close();
			                			buttonFiltrar.handler.call(buttonFiltrar.scope);
									},
									failure: function(response, options) {
										var responseText = Tera.decode(options.response.responseText);
										Tera.MessageBox.alert('<bean:message key="app.validaciones.errorModificacion"  bundle="messages"/>', responseText.failure);
									}
								});
						    }
						});
					}
				});

		    	var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			    	
		 	    	
	    	    var recordToModify = gridLocalidad.getSelectionModel().getSelected().data;

	 	        var form = new Tera.form.FormPanel({
	 				baseCls: 'x-plain',
	 				labelWidth: 130,
	 				monitorValid: true,
	 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
	 				items: [
 					    {
  	 	                 xtype: 'numberfield', 
 	 				     width: 98,
	 				     fieldLabel: '<bean:message key="localidad.idlocalidad_corto"  bundle="fields"/>',
	 				     name: 'idlocalidad',
	 				     id: 'idlocalidad',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.idlocalidad 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 20,
	 				     fieldLabel: '<bean:message key="localidad.nombre_corto"  bundle="fields"/>',
	 				     name: 'nombre',
	 				     id: 'nombre',
	 				     height: 21,
	 				     maxLength: 1,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.nombre 
	 			        }
 				        ,
 					    {
  	 				     width: 98,
	 				     fieldLabel: '<bean:message key="localidad.idprovincia_corto"  bundle="fields"/>',
	 				     name: 'idprovincia',
	 				     id: 'idprovincia',
	 				     height: 21,
	 				     maxLength: ,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.idprovincia 
	 			        }
 				        ,
 					    {
  	 				     width: 98,
	 				     fieldLabel: '<bean:message key="localidad.codigopostal_corto"  bundle="fields"/>',
	 				     name: 'codigopostal',
	 				     id: 'codigopostal',
	 				     height: 21,
	 				     maxLength: ,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de  car&aacute;cteres',
	 				     allowBlank: false,  
	 				     readOnly: false,  
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.codigopostal 
	 			        }
 		 		]         
		 	});

 			var win = new Tera.Window({
 	        	title: '<bean:message key="app.label.modificacion"  bundle="messages"/>',
 			    modal: true,
 				width: 288 + 50,
 				height: 200+ 15,
 				resizable: false,
 			    layout: 'fit',
 			    plain:true,
 			    bodyStyle:'padding: 10px;',
 			    buttonAlign: 'right',
 				border: false,
 				items: [ form ],
 				buttons: [aceptar, cancelar]
 			});
 	        win.show();
	  });
	    
  buttonBaja.on('click', function(e){
	    	if (!gridLocalidad.getSelectionModel().hasSelection()) { 
					return false;
	    	}

			var aceptar = new Tera.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_b"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
			    			form.getForm().submit({
								  url: configObject.removeUrl,
								  success: function(response, options) {
									win.close();
			 						buttonFiltrar.handler.call(buttonFiltrar.scope);
								  },
								  failure: function(response, options) {
									var responseText = Tera.decode(options.response.responseText);
									Tera.MessageBox.alert('<bean:message key="app.validaciones.errorBaja"  bundle="messages"/>', responseText.failure);
								  }
							});
					    }
					});
				}
			});
	      	
   			var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>' ,minWidth: 80, handler: function() { win.close(); }});
	    	
			var recordToShow = gridLocalidad.getSelectionModel().getSelected().data;

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="localidad.idlocalidad_corto"  bundle="fields"/>',
		  			 width: 98 + 17,
		  			 id: 'idlocalidad',
		  			 name: 'idlocalidad',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.idlocalidadName,
		             value: recordToShow.idlocalidad
	  			   }
			       ,
 				   {
		            xtype: 'textfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 20,
					fieldLabel: '<bean:message key="localidad.nombre_corto"  bundle="fields"/>',
					id: 'nombre',
					readOnly: true,
					name: 'nombre',
					height: 21,
					value: recordToShow.nombre 
				   }
				   ,
 				   {
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="localidad.idprovincia_corto"  bundle="fields"/>',
					id: 'idprovincia',
					readOnly: true,
					name: 'idprovincia',
					height: 21,
					value: recordToShow.idprovincia 
				   }
				   ,
 				   {
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="localidad.codigopostal_corto"  bundle="fields"/>',
					id: 'codigopostal',
					readOnly: true,
					name: 'codigopostal',
					height: 21,
					value: recordToShow.codigopostal 
				   }
 				]
			});
			
			var win = new Tera.Window({
				title: '<bean:message key="app.label.baja"  bundle="messages"/>',
				width: 288 + 50,
				height: 200 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [aceptar,cancelar]
			});
	        win.show(this);
			
	    });
	    
  buttonVisualizar.on('click', function(){
			if (!gridLocalidad.getSelectionModel().hasSelection()){
				return false;
			}

			var cerrar = new Tera.Button({text: '<bean:message key="app.label.cerrar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			
			var recordToShow = gridLocalidad.getSelectionModel().getSelected().data;

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				items: [
			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="localidad.idlocalidad_corto"  bundle="fields"/>',
		  			 width: 98 + 17,
		  			 id: 'idlocalidad',
		  			 name: 'idlocalidad',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.idlocalidadName,
		             value: recordToShow.idlocalidad
	  			   }
 				   ,
				   {
		            xtype: 'textfield', 
					width: 20,
					fieldLabel: '<bean:message key="localidad.nombre_corto"  bundle="fields"/>',
					id: 'nombre',
					readOnly: true,
					name: 'nombre',
					height: 21,
					value: recordToShow.nombre 
				   }
 				   ,
				   {
					width: 98,
					fieldLabel: '<bean:message key="localidad.idprovincia_corto"  bundle="fields"/>',
					id: 'idprovincia',
					readOnly: true,
					name: 'idprovincia',
					height: 21,
					value: recordToShow.idprovincia 
				   }
 				   ,
				   {
					width: 98,
					fieldLabel: '<bean:message key="localidad.codigopostal_corto"  bundle="fields"/>',
					id: 'codigopostal',
					readOnly: true,
					name: 'codigopostal',
					height: 21,
					value: recordToShow.codigopostal 
				   }
 				]
			});

			
			var win = new Tera.Window({
 	        	title: '<bean:message key="app.label.visualizacion"  bundle="messages"/>',
				width: 288 + 50,
				height: 200 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [cerrar]
			});
	        win.show(this);
	    });
	    
	});
</script>
	
<div id="panelLocalidad"></div>