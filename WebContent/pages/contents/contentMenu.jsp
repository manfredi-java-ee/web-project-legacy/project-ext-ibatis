<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<link rel="stylesheet" type="text/css" href="<core:url value="/css/menu.css"/>">

<div id="panel"></div>
<script language="javascript">

	function openWindow(url) {
        window.open (url, "accrosWindow", "status=1,resizable=1,toolbar=0,location=0,menubar=0,directories=0"); 
	}
	
	Tera.BLANK_IMAGE_URL = '../img/default/s.gif';

	Tera.onReady(function(){
		
		var accordion = {
				id :'accordion',
				layout :'accordion', 
				border: false, 
				layoutConfig: {
					titleCollapse: true, animate: false, fill: false
				}, 
				items:[

					<core:set var="countTotal" value="${0}"/>
										
					<core:forEach items="${APP_MENU.items}" var="aplicacion">
						<core:set var="countTotal" value="${countTotal+1}"/>	
						{
							title:'${aplicacion.label}', border:false, autoHeight:true, collapsible:true,
							items:[
									
							<core:set var="countMenu" value="${0}"/>
														
							<core:forEach items="${aplicacion.items}" var="menu">
								<core:if test="${fn:length(menu.items) > 0}">
								
										<core:set var="countMenu" value="${countMenu+1}"/>	
										{
											xtype:'menupanel',collapsed:true,border:false,collapsible:true,
											title:'${menu.label}',
											id: '${menu.id}',
											plugins: new Tera.ux.plugins.MenuState({animate:false}),
											items:[
												
												<core:set var="countSubMenu"  value="${0}"/>
																								
												<core:forEach items="${menu.items}" var="submenu">
													<core:set var="countSubMenu" value="${countSubMenu+1}"/>
	
													{
														xtype:'menuitem',
														id: '${submenu.id}',
														url: "<core:url value="${submenu.path}"/>", 
														text: '${submenu.label}',
														selected: ${submenu.selected}
													}
													<core:if test="${fn:length(menu.items) != countSubMenu}">,</core:if>
													
												</core:forEach>
											]
										}
										<core:if test="${fn:length(aplicacion.items) != countMenu}">,</core:if>
										
								</core:if>
								
								<core:if test="${fn:length(menu.items) == 0}">
									<core:set var="countMenu" value="${countMenu+1}"/>
										
										{
											xtype:'menuitem',
											id: '${menu.id}',
											url: "<core:url value="${menu.path}"/>", 
											text: '${menu.label}',
											selected: ${menu.selected}
										}
										<core:if test="${fn:length(aplicacion.items) != countMenu}">,</core:if>
									
								</core:if>
								
							</core:forEach>
							
							]
						}<core:if test="${fn:length(APP_MENU.items) != countTotal}">,</core:if>
						
					</core:forEach>
				]
		};

		var menuContainer = new Tera.Panel( {
			collapsible :true,
			border :false,
			renderTo :'panel',
			width :180,
			style :'margin-left: 15px',
			items : [ accordion ]
		});

		<core:forEach items="${APP_MENU.items}" var="menu">
			<core:forEach items="${menu.items}" var="submenu">
				<core:forEach items="${submenu.items}" var="subsubmenu">
					<core:if test="${subsubmenu.render}">new Tera.ToolTip({target:'${subsubmenu.id}',html:'${subsubmenu.tooltip}',trackMouse:true,interceptTitles:true});</core:if>
				</core:forEach>
			</core:forEach>
		</core:forEach>
	});
</script>

