<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelMovimientoFisico',
		panelId: 'panelMovimientoFisico',
		addUrl:			'../app/movimientoFisico.do?method=altaMovimientoFisico',
		modifyUrl:		'../app/movimientoFisico.do?method=modificarMovimientoFisico',
		removeUrl: 		'../app/movimientoFisico.do?method=bajaMovimientoFisico',
		loadDataUrl:	'../app/movimientoFisico.do?method=loadData',
		filterDataUrl:	'../app/movimientoFisico.do?method=buscar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/movimientoFisico.do?method=buscar',
			pagingToolbarUrl: 		'../app/movimientoFisico.do?method=loadData',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			adabas_isnName: 'SfaMovimientosadabasisn'
		    ,
			fa_tipo_factName: 'TipoFacturacion'
		}
	};
	
	
	
	Tera.onReady(function(){

		
		
	  	Tera.QuickTips.init();
	  	
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';

		
    
		
		var labelresultado = new Tera.form.Label	(
			{
				xtype: 'label', 
			    id: 'labelresultado',
			   	cls: 'x-form-item myBold',
				 //   style: 'font-weight:bold;color:red;' 
			   	style: 'font-weight:bold;' 
	 	   }
	      );  

		
		

	/////////  Formulario Búsqueda -INICIO			
				var filtroBusqueda = new Tera.form.FilterPanel(
						{

							mappedFields : [ 'fa_urbFilter',
									'fa_tipo_factFilter', 'fa_oficinaFilter',
									'fa_lineaFilter', 'fa_abonadoFilter',
									'fa_interurbFilter', 'fa_zonaFilter' ],
							filterConfig : filterConfig,
							bodyBorder : true,
							border : false,
							bodyStyle : 'padding:10px',
							buttonAlign : 'right',
							width : 650,
							baseCls : 'x-plain',
							labelWidth : 50,
							labelAlign : 'top',
							bodyStyle : 'padding:10px',
							height : 230,

							items : [

									///Primera Fila		                
									{
										layout : 'column',
										border : false,
										items : [ {
											columnWidth : .3,
											layout : 'form',
											border : false,
											items : [ {
												xtype : 'label',
												id : 'labelcriterios',
												cls : 'x-form-item myBold',
												style : 'font-weight:bold;',
												text : 'Criterios de Búsqueda',
												labelAlign : 'left'
											} ]

										}, {
											columnWidth : .3,
											layout : 'form',
											border : false

										}, {
											columnWidth : .3,
											layout : 'form',
											border : false
										} ]
									},

									///Segunda Fila
									{
										layout : 'column',
										border : false,
										items : [
												{
													columnWidth : .1,
													layout : 'form',
													border : false,
													items : [

													{

														xtype : 'radio',

														name : 'radio1',
														id : 'radio1',

														Value : 'page',

														boxLabel : 'IUL',
														inputValue : 'IUL',
														labelSeparator : "",

														width : 80

													}

													]

												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'textfield',
														width : 80,
														maxWidth : 40,
														fieldLabel : '<bean:message key="sfa_movimientos_fisico.fa_interurb_corto"  bundle="fields"/>',
														name : 'fa_interurbFilter',
														id : 'fa_interurbFilter',
														height : 20,
														maxLength : 4,
														enforceMaxLength : true,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
														allowBlank : false,
														msgTarget : 'side',
														labelAlign : 'left',
														disabled : true

													}

													]

												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'textfield',
														width : 80,
														maxWidth : 40,
														fieldLabel : '<bean:message key="sfa_movimientos_fisico.fa_urb_corto"  bundle="fields"/>',
														name : 'fa_urbFilter',
														id : 'fa_urbFilter',
														height : 21,
														maxLength : 4,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
														allowBlank : false,
														msgTarget : 'side',
														disabled : true

													} ]
												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'textfield',
														width : 80,
														maxWidth : 40,
														fieldLabel : '<bean:message key="sfa_movimientos_fisico.fa_linea_corto"  bundle="fields"/>',
														name : 'fa_lineaFilter',
														id : 'fa_lineaFilter',
														height : 21,
														maxLength : 4,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 4 car&aacute;cteres',
														allowBlank : false,
														msgTarget : 'side',
														disabled : true
													}

													]

												} ]
									},
									///Tercera Fila        
									{
										layout : 'column',
										border : false,
										items : [
												{
													columnWidth : .1,
													layout : 'form',
													border : false,
													items : [ {

														xtype : 'radio',

														name : 'radio1',
														id : 'radio3',
														Value : 'page2',

														boxLabel : 'ZOA',
														labelSeparator : "",
														//fieldLabel: '',
														inputValue : 'ZOA',
														width : 200

													}

													]
												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,
													items : [ {
														xtype : 'numberfield',
														width : 98,
														fieldLabel : '<bean:message key="sfa_movimientos_fisico.fa_zona_corto"  bundle="fields"/>',
														name : 'fa_zonaFilter',
														id : 'fa_zonaFilter',
														height : 21,
														maxLength : 3,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 3 car&aacute;cteres',
														allowBlank : false,
														msgTarget : 'side',
														disabled : true
													} ]

												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,

													items : [ {
														xtype : 'numberfield',
														width : 98,
														fieldLabel : '<bean:message key="sfa_movimientos_fisico.fa_oficina_corto"  bundle="fields"/>',
														name : 'fa_oficinaFilter',
														id : 'fa_oficinaFilter',
														height : 21,
														maxLength : 3,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 3 car&aacute;cteres',
														allowBlank : false,
														msgTarget : 'side',
														disabled : true

													}

													]
												},
												{
													columnWidth : .3,
													layout : 'form',
													border : false,

													items : [ {
														xtype : 'numberfield',
														width : 98,
														fieldLabel : '<bean:message key="sfa_movimientos_fisico.fa_abonado_corto"  bundle="fields"/>',
														name : 'fa_abonadoFilter',
														id : 'fa_abonadoFilter',
														height : 21,
														maxLength : 5,
														maxLengthText : 'El campo tiene un m&aacute;ximo de 5 car&aacute;cteres',
														allowBlank : false,
														msgTarget : 'side',
														disabled : true
													}

													]
												} ]

									}

									,
									///cuarta Fila        
									{
										layout : 'column',
										border : false,
										items : [ {
											columnWidth : .3,
											layout : 'form',
											border : false,

											items : [ {
												xtype : 'label',
												id : 'labelZoa',
												cls : 'x-form-item myBold',
												style : 'font-weight:bold;',
												text : 'Filtros Opcionales'
											} ]

										}, {
											columnWidth : .6,
											layout : 'form',
											border : false

										}, {
											columnWidth : .3,
											layout : 'form',
											border : false
										}, {
											columnWidth : .3,
											layout : 'form',
											border : false
										}

										]

									},
									///quinta Fila        
									{
										layout : 'column',
										border : false,
										items : [
												{
													columnWidth : .1,
													layout : 'form',
													border : false

												},
												{
													columnWidth : .6,
													layout : 'form',
													border : false,
													items : [

													{
														xtype : 'lupafield',
														fieldLabel : '<bean:message key="sfa_movimientos_fisico.fa_tipo_fact_corto"  bundle="fields"/>',
														width : 98 + 17,
														id : 'fa_tipo_factFilter',
														name : 'fa_tipo_factFilter',
														limitDescription : 30,
														descriptionRetrival : lupaConfig.urls.descriptionRetrival,
														url : lupaConfig.urls.url,
														entityName : lupaConfig.entities.fa_tipo_factName
													}

													]
												}, {
													columnWidth : .3,
													layout : 'form',
													border : false
												}, {
													columnWidth : .3,
													layout : 'form',
													border : false
												} ]

									} ]

						}

				);

				/////////  Formulario Busqueda -FIN		

				radio = Tera.getCmp("radio1");

				////Eventos del Radio Button	
				radio.addListener('check', function(obj) {

					// Obtengo el Valor del checkBoxSeleccionado 

					//Blanqueo ellabel de resultado
					labelresultado.setText(' ');

					//Reseteo la barra del grid PagingToolbar
					bbar1.field.dom.value = '1';
					bbar1.afterTextEl.el.innerHTML = 'de 1';
					bbar1.store.removeAll();

					bbar1.first.setDisabled(true);
					bbar1.prev.setDisabled(true);
					bbar1.next.setDisabled(true);
					bbar1.last.setDisabled(true);

					// Blanqueo el Tipo de Facturacion

					var valor = filtroBusqueda.getForm().getValues()['radio1'];

					store.removeAll();

					InterFilter = Tera.getCmp("fa_interurbFilter");
					UrbFilter = Tera.getCmp("fa_urbFilter");
					LineaFilter = Tera.getCmp("fa_lineaFilter");

					ZonaFilter = Tera.getCmp("fa_zonaFilter");
					OficinaFilter = Tera.getCmp("fa_oficinaFilter");
					AbonadoFilter = Tera.getCmp("fa_abonadoFilter");

					TipoFactFilter = Tera.getCmp("fa_tipo_factFilter");

					//Limpio los campos de ambos criterios
					InterFilter.setValue('');
					UrbFilter.setValue('');
					LineaFilter.setValue('');

					OficinaFilter.setValue('');
					ZonaFilter.setValue('');
					AbonadoFilter.setValue('');
					TipoFactFilter.setValue('');

					if (valor == 'IUL') {

						// habilito los campos para consulta IUL

						InterFilter.allowBlank = false;
						InterFilter.validateValue(InterFilter.getValue()); //force update
						InterFilter.enable();

						UrbFilter.allowBlank = false;
						UrbFilter.validateValue(UrbFilter.getValue()); //force update
						UrbFilter.enable();

						LineaFilter.allowBlank = false;
						LineaFilter.validateValue(LineaFilter.getValue()); //force update
						LineaFilter.enable();

						// desabilito los campos para consulta ZOA

						ZonaFilter.allowBlank = true;
						ZonaFilter.validateValue(ZonaFilter.getValue()); //force update
						ZonaFilter.disable();

						OficinaFilter.allowBlank = true;
						OficinaFilter.validateValue(OficinaFilter.getValue()); //force update
						OficinaFilter.disable();

						AbonadoFilter.allowBlank = true;
						AbonadoFilter.validateValue(AbonadoFilter.getValue()); //force update
						AbonadoFilter.disable();

					} else

					{

						// desabilito los campos para consulta IUL

						InterFilter.allowBlank = true;
						InterFilter.validateValue(InterFilter.getValue()); //force update
						InterFilter.disable();

						UrbFilter.allowBlank = true;
						UrbFilter.validateValue(UrbFilter.getValue()); //force update
						UrbFilter.disable();

						LineaFilter.allowBlank = true;
						LineaFilter.validateValue(LineaFilter.getValue()); //force update
						LineaFilter.disable();

						// habilito los campos para consulta ZOA

						ZonaFilter.allowBlank = false;
						ZonaFilter.validateValue(ZonaFilter.getValue()); //force update
						ZonaFilter.enable();

						OficinaFilter.allowBlank = false;
						OficinaFilter.validateValue(OficinaFilter.getValue()); //force update
						OficinaFilter.enable();

						AbonadoFilter.allowBlank = false;
						AbonadoFilter.validateValue(AbonadoFilter.getValue()); //force update
						AbonadoFilter.enable();
						AbonadoFilter.setValue('');

					}

				});

				////Fin de Eventos del Radio Button	

				var store = new Tera.data.JsonStore({
					remoteSort : true,
					baseParams : {
						fa_urbFilter : '',
						fa_tipo_factFilter : '',
						fa_oficinaFilter : '',
						fa_lineaFilter : '',
						fa_abonadoFilter : '',
						fa_interurbFilter : '',
						fa_zonaFilter : ''

					},
					url : configObject.filterDataUrl,
					root : 'data',
					totalProperty : 'totalCount',
					id : 'codMovimientoFisico',
					fields : [ 'adabas_isn', 'fa_r_oficina',
							'fa_c_importe_total', 'fa_abono', 'fa_r_cajero',
							'fa_nro_tarjeta', 'campo_no_usado_3',
							'fa_r_usuario', 'fa_r_tipo_pago',
							'fa_c_estado_conciliacion',
							'fa_r_total_sin_recargo', 'fa_ident_abo',
							'fa_fecha_acreditacion_nvo', 'fa_localidad',
							'fa_r_zona_pago', 'fa_marca_desimputacion',
							'fa_c_usuario_baja', 'fa_c_fecha_pago_nvo',
							'fa_r_fecha_proceso_nvo',
							'fa_r_categoria_gran_cliente', 'fa_urb',
							'fa_c_fecha_hora_alta', 'fa_c_zona_pago',
							'fa_c_fecha_hora_modif', 'campo_no_usado_7',
							'fa_fecha_marca_nvo', 'fa_serv_medido',
							'fa_r_factura', 'fa_c_tipo_registro',
							'fa_r_terminal', 'campo_no_usado',
							'fa_razon_social', 'campo_no_usado_8',
							'fa_r_iva_mora_r', 'fa_domicilio',
							'fa_r_nro_nota_cred', 'fa_c_cant_concp_c_apert',
							'fa_r_marca_tarea', 'fa_r_hora_ingreso',
							'fa_r_marca_carta_motivo', 'fa_cod_entrega',
							'fa_r_ident_abo', 'fa_r_fecha_reconcil_nvo',
							'fa_r_reconciliacion', 'fa_fecha_desimputacion',
							'campo_no_usado_5', 'fa_c_ident_aper_man',
							'fa_c_nro_conciliacion', 'fa_tipo_contrib',
							'fa_r_importe_fuera_term', 'campo_no_usado_4',
							'fa_r_linea', 'fa_cod_postal', 'fa_tipo_fact',
							'fa_c_cod_banco_depos', 'fa_total_facturado',
							'fa_r_fecha_pago_orig', 'fa_c_usuario_modif',
							'fa_oficina', 'fa_c_nro_boleta_deposito',
							'fa_linea', 'fa_r_anio_lote',
							'fa_r_codigo_movimiento', 'fa_r_importe_cobrado',
							'fa_importe_pdte_gire', 'fa_r_zona_pago_orig',
							'fa_c_usuario_alta', 'fa_numero_pago',
							'campo_no_usado_9', 'fa_cod_banco',
							'fa_c_cant_reg', 'fa_r_urb', 'fa_marca_tarea',
							'fa_r_codigo_iva', 'fa_r_nro_reintegro',
							'fa_tipo_contrib_nvo', 'fa_r_abonado',
							'fa_r_tipo_facturacion', 'fa_c_fecha_hora_baja',
							'fa_r_numero_pago', 'fa_codigo_movimiento',
							'fa_anio_lote', 'fa_banco_zona',
							'fa_c_estado_asiento', 'fa_abonado',
							'campo_no_usado_6', 'fa_r_interurb',
							'fa_r_nro_copia', 'fa_r_fecha_pago_nvo',
							'fa_r_marca_pago', 'fa_zona_banco_pago_doble',
							'fa_usuario_terminal', 'fa_fecha_proc_ctgo',
							'fa_c_marca_tarea', 'fa_fecha_entrega',
							'fa_marca_recargo', 'fa_c_cod_concepto',
							'fa_interurb', 'fa_r_zona',
							'fa_r_fecha_ingreso_nvo', 'fa_hora_ingreso',
							'fa_c_fecha_env_contab_nvo',
							'fa_r_estado_real_base', 'fa_importe_iva',
							'fa_zona', 'fa_fecha_ingreso_nvo',
							'desc_movimiento' ]

				});

				var cmMovimientoFisico = new Tera.grid.ColumnModel(
						[
								new Tera.grid.RowNumberer(),
								{
									id : 'fa_anio_lote',

									header : '<bean:message key="sfa_movimientos_fisico.fa_anio_lote_corto"  bundle="fields"/>',
									renderer : formateoaniolote,
									dataIndex : 'fa_anio_lote',
									width : '<bean:message key="sfa_movimientos_fisico.fa_anio_lote_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								},

								{
									id : 'fa_tipo_fact',
									header : '<bean:message key="sfa_movimientos_fisico.fa_tipo_fact_corto"  bundle="fields"/>',
									dataIndex : 'fa_tipo_fact',
									width : '<bean:message key="sfa_movimientos_fisico.fa_tipo_fact_corto"  bundle="fields"/>'.length * 8,
									align : 'center'
								},

								{
									id : 'fa_codigo_movimiento',
									renderer : formateoCodigoMovimiento,
									header : '<bean:message key="sfa_movimientos_fisico.fa_codigo_movimiento_corto"  bundle="fields"/>',
									dataIndex : 'fa_codigo_movimiento',
									width : '<bean:message key="sfa_movimientos_fisico.fa_codigo_movimiento_corto"  bundle="fields"/>'.length * 8,
									align : 'center'
								},
								{
									id : 'desc_movimiento',
									header : '<bean:message key="sfa_movimientos_fisico.desc_codigo_movimiento_corto"  bundle="fields"/>',
									dataIndex : 'desc_movimiento',
									width : '<bean:message key="sfa_movimientos_fisico.fa_codigo_movimiento_corto"  bundle="fields"/>'.length * 20,
									align : 'center'
								}

								,

								{
									id : 'fa_fecha_ingreso_nvo',
									header : '<bean:message key="sfa_movimientos_fisico.fa_fecha_ingreso_nvo_corto"  bundle="fields"/>',
									dataIndex : 'fa_fecha_ingreso_nvo',
									width : '<bean:message key="sfa_movimientos_fisico.fa_fecha_ingreso_nvo_corto"  bundle="fields"/>'.length * 24,
									align : 'center'
								}

								,

								{
									id : 'fa_usuario_terminal',
									renderer : formateousuario,
									header : '<bean:message key="sfa_movimientos_fisico.fa_usuario_terminal_corto"  bundle="fields"/>',
									dataIndex : 'fa_usuario_terminal',
									width : '<bean:message key="sfa_movimientos_fisico.fa_usuario_terminal_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								},

								{
									id : 'fa_interurb',
									header : '<bean:message key="sfa_movimientos_fisico.fa_interurb_corto"  bundle="fields"/>',
									dataIndex : 'fa_interurb',
									width : '<bean:message key="sfa_movimientos_fisico.fa_interurb_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								},
								{
									id : 'fa_urb',
									header : '<bean:message key="sfa_movimientos_fisico.fa_urb_corto"  bundle="fields"/>',
									dataIndex : 'fa_urb',
									width : '<bean:message key="sfa_movimientos_fisico.fa_urb_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								},

								{
									id : 'fa_linea',
									header : '<bean:message key="sfa_movimientos_fisico.fa_linea_corto"  bundle="fields"/>',
									dataIndex : 'fa_linea',
									width : '<bean:message key="sfa_movimientos_fisico.fa_linea_corto"  bundle="fields"/>'.length * 10,
									align : 'center'
								}

						]);
				cmMovimientoFisico.defaultSortable = true;

				function formateoCodigoMovimiento(val) {

					//fuerzo al numero a un string 

					var inicio = parseInt(val.length - 2);
					var finale = val.length;

					var movcortado = val.substring(inicio, finale);

					return movcortado;
				}

				function formateoaniolote(val) {

					//fuerzo al numero a un string 
					var original = "" + val;
					var result = original;
					var formateado;

					//Relleno con ceros el campo 
					while (result.length < 5) {
						result = "0" + result;
					}

					//obtengo el lote 
					var lote = result.substring(parseInt(result.length - 3), 5);
					var anio = result.substring(0, 2);

					formateado = anio + '-' + lote;

					return formateado;
				}

				function formateousuario(val) {

					var formateado = val.substring(0, 6);

					return formateado;
				}

				var bbar1 = new Tera.PagingToolbar({
					filterPanel : filtroBusqueda,
					exportFile : false,
					exportGridName : 'MovimientoFisico',
					pageSize : 10,
					store : store,
					loading : false,
					displayInfo : false,
					displayMsg : 'Mostrando   {0} - {1} de {2}',
					emptyMsg : 'No existen registros a mostrar .'

				});

				var gridMovimientoFisico = new Tera.grid.GridPanel({
					width : 625,
					height : 245,
					store : store,
					cm : cmMovimientoFisico,
					trackMouseOver : true,
					sm : new Tera.grid.RowSelectionModel({
						singleSelect : true
					}),
					stripeRows : true,
					viewConfig : {
						forceFit : false,
						enableRowBody : false
					},

					bbar : bbar1

				});

				gridMovimientoFisico.addListener('click', function(param) {

					gridMovimientoFisico.getSelectionModel().hasSelection();

				});

				/////Evento del Botón Filtrar -- INICIO			

				var buttonFiltrar = new Tera.Button({
					text : 'Buscar',
					minWidth : 80,
					handler : FiltrarSubmit
				});

				function FiltrarSubmit() {

					// Si algún campo tiene algún error 
					if (filtroBusqueda.getForm().isValid() == false) {

						Tera.Msg
								.show({
									title : 'Información', //<- el título del diálogo   
									msg : 'Verifique la longitud de los campos que están señalizados en color rojo.', //<- El mensaje   
									buttons : Tera.Msg.OK, //<- Botones de SI y NO   
									icon : Tera.Msg.INFO, // <- un ícono de error   
									fn : this.callback
								//<- la función que se ejecuta cuando se da clic   
								});

					}

					var seleccionado = filtroBusqueda.getForm().getValues()['radio1'];

					// Si el campo para consultar por ZOA esta checkeado
					if (seleccionado == 'ZOA') {

						//Evaluo que los campos para consultar por ZOA esten completos.	
						//Si estan vacios muestro advertencia

						if (Tera.getCmp('fa_zonaFilter').getValue() == ""
								|| Tera.getCmp('fa_oficinaFilter').getValue() == ""
								|| Tera.getCmp('fa_abonadoFilter').getValue() == "") {

							///Creacion de una mensaje de usuario personalizado
							Tera.Msg
									.show({
										title : 'Información', //<- el título del diálogo   
										msg : 'Para realizar la consulta, por favor ingrese Zona, Oficina y Abonado.', //<- El mensaje   
										buttons : Tera.Msg.OK, //<- Botones de SI y NO   
										icon : Tera.Msg.INFO, // <- un ícono de error   
										fn : this.callback
									//<- la función que se ejecuta cuando se da clic   
									});

							return false;
						}

					} else

					if (seleccionado == 'IUL') {

						//Evaluo que los campos para consultar por IUL esten completos.	 
						//Si estan vacios muestro advertencia 
						if (Tera.getCmp('fa_interurbFilter').getValue() == ""
								|| Tera.getCmp('fa_urbFilter').getValue() == ""
								|| Tera.getCmp('fa_lineaFilter').getValue() == "") {

							///Creacion de una mensaje de usuario personalizado
							Tera.Msg
									.show({
										title : 'Información', //<- el título del diálogo   
										msg : 'Para realizar la consulta, por favor ingrese Interurbano, Urbano y Línea.', //<- El mensaje   
										buttons : Tera.Msg.OK, //<- Botones de SI y NO   
										icon : Tera.Msg.INFO, // <- un ícono de error   
										fn : this.callback
									//<- la función que se ejecuta cuando se da clic   
									});

							return false;
						}

					} else
					// Sino fue seleccionado ningun radio Button	

					{

						///Creacion de una mensaje de usuario personalizado
						Tera.Msg
								.show({
									title : 'Información', //<- el título del diálogo   
									msg : 'Para realizar la consulta, por favor seleccione algún criterio de búsqueda.', //<- El mensaje   
									buttons : Tera.Msg.OK, //<- Botones de SI y NO   
									icon : Tera.Msg.INFO, // <- un ícono de error   
									fn : this.callback
								//<- la función que se ejecuta cuando se da clic   
								});

						return false;

					}

					getJsonOfStore(store);

					filtroBusqueda
							.submit({

								waitMsg : '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
								url : configObject.filterDataUrl,
								success : function(response, options) {

									var responseText = Tera
											.decode(options.response.responseText);

									if (responseText.data != 'undefined'
											|| responseText.data != null)

									{
										gridMovimientoFisico.getStore()
												.loadData(responseText);

										store.baseParams.fa_urbFilter = Tera
												.getCmp('fa_urbFilter')
												.getValue();
										store.baseParams.fa_tipo_factFilter = Tera
												.getCmp('fa_tipo_factFilter')
												.getValue();
										store.baseParams.fa_oficinaFilter = Tera
												.getCmp('fa_oficinaFilter')
												.getValue();
										store.baseParams.fa_lineaFilter = Tera
												.getCmp('fa_lineaFilter')
												.getValue();
										store.baseParams.fa_abonadoFilter = Tera
												.getCmp('fa_abonadoFilter')
												.getValue();
										store.baseParams.fa_interurbFilter = Tera
												.getCmp('fa_interurbFilter')
												.getValue();
										store.baseParams.fa_zonaFilter = Tera
												.getCmp('fa_zonaFilter')
												.getValue();

									}

									//Evaluo si el resultado de la consulta y muestro la leyenda
									var cant = 0;
									cant = store.getTotalCount();

									if (cant > 0) {
										labelresultado
												.setText(' Resultado para el criterio de búsqueda seleccionado : ');

									} else

									{

										labelresultado
												.setText(
														'<font color="#FF0000">'
																+ 'No existen datos que cumplan con los criterios de búsqueda ingresados.'
																+ '</font>',
														false);

									}

								},
								failure : function(response, options) {
									var responseText = Tera
											.decode(options.response.responseText);
									Tera.MessageBox
											.alert(
													'<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>',
													responseText.failure);
								}
							});
				}

				// Función para que cuando aprete enter realice la misma accion que el boton filtrar

				var map = new Tera.KeyMap(Tera.getBody(), {
					key : Tera.EventObject.ENTER,
					fn : function() {

						VentanaLupa = Tera.getCmp("fa_tipo_factFilter");

						if (VentanaLupa.visualize == false) {
							FiltrarSubmit();
						}

					},
					scope : this
				});

				/////Evento del Botón Filtrar -- FIN						

				/////Evento del Botón Limpiar -- INICIO

				var buttonLimpiar = new Tera.Button(
						{
							text : '<bean:message key="app.label.limpiar"  bundle="messages"/>',
							minWidth : 80,
							handler : function() {

								//reseteo los campos TextField del formulario 
								filtroBusqueda.getForm().reset();

								//Blanqueo ellabel de resultado
								labelresultado.setText(' ');

								//Reseteo los campos numberfield

								ZonaFilter = Tera.getCmp("fa_zonaFilter");
								ZonaFilter.setDisabled(true);
								OficinaFilter = Tera.getCmp("fa_oficinaFilter");
								OficinaFilter.setDisabled(true);
								AbonadoFilter = Tera.getCmp("fa_abonadoFilter");
								AbonadoFilter.setDisabled(true);

								//Reseteo la barra del grid PagingToolbar
								bbar1.field.dom.value = '1';
								bbar1.afterTextEl.el.innerHTML = 'de 1';
								bbar1.store.removeAll();

								bbar1.first.setDisabled(true);
								bbar1.prev.setDisabled(true);
								bbar1.next.setDisabled(true);
								bbar1.last.setDisabled(true);

								bbar1.updateInfo();

							}

						});

				/////Evento del Botón Limpiar -- FIN

				var panel = new Tera.Panel({
					id : configObject.panelId,
					renderTo : configObject.renderTo,
					border : false,
					defaults : {
						border : false
					},
					items : [ {
						border : false,
						defaults : {
							border : false
						},
						items : [ {
							layout : "column",
							border : false,
							defaults : {
								border : false
							},
							items : [ {
								columnWidth : 1,
								items : [ filtroBusqueda ]
							}, {
								width : 113,
								items : [ {
									border : false,
									defaults : {
										border : false
									},
									items : [ {
										items : [ buttonFiltrar ],
										style : 'margin-top: 10px;'
									}, {
										items : [ buttonLimpiar ],
										style : 'margin-top: 3px;'
									} ]
								} ]
							} ]
						} ]
					},

					{
						layout : "fit",
						border : false,
						style : 'padding: 0px 5px 0px 5px;'
					}, {
						layout : "fit",
						border : false,
						style : 'padding: 0px 5px 0px 5px;',
						items : [ labelresultado ]
					}, {
						layout : "fit",
						border : false,
						style : 'padding: 0px 5px 0px 5px;',
						items : [ gridMovimientoFisico ]
					} ]
				});

			});
</script>
	
<div id="panelMovimientoFisico"></div>