var loginPanel = null;

function doLogin() {
	loginPanel.getForm().getEl().dom.action = 'login.do?method=login';
	loginPanel.getForm().getEl().dom.method = 'POST';
	loginPanel.getForm().submit();
}

function doClear() {
   	document.getElementById('usuario').value = '';
   	document.getElementById('password').value = '';
}

Tera.onReady(function() {

    loginPanel = new Tera.form.FormPanel({
        id:'main-panel',
        renderTo: 'loginFields',
        standardSubmit: true,
        baseCls: 'x-plain',
		border: false,
		listeners : {
    		"afterlayout" : function() {
    			Tera.getCmp('usuario').focus();
    		}
    	},
        items:[{
				xtype: 'textfield',
				labelStyle: 'font-weight: bold; color: #000000',
				id: 'usuario',
				name: 'usuario',
				maxLength: 16,
				fieldLabel: 'Usuario',
				listeners : {
					"specialkey" : function(field, ev){
			        	if(ev.getKey() == ev.ENTER){
			        		ev.preventDefault();
			        		if (!Tera.isEmpty(field.getValue().trim())) {
			        			Tera.getCmp('password').focus();
			        		}
			        	}
					}
	    		}
			},{
				xtype: 'textfield',
				inputType: 'password',
				labelStyle: 'font-weight: bold; color: #000000',
				id: 'password',
				maxLength: 16,
				fieldLabel: 'Contrase&ntilde;a',
				listeners : {
					"specialkey" : function(field, ev){
			        	if(ev.getKey() == ev.ENTER){
			        		ev.preventDefault();
			        		
			        		doLogin();
			        	}
					}
			    }
			}
		]
    });
});