<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelZoNAOFICINAPFNN',
		panelId: 'panelZoNAOFICINAPFNN',
		addUrl:			'../app/zoNAOFICINAPFNN.do?method=altaZoNAOFICINAPFNN',
		modifyUrl:		'../app/zoNAOFICINAPFNN.do?method=modificarZoNAOFICINAPFNN',
		removeUrl: 		'../app/zoNAOFICINAPFNN.do?method=bajaZoNAOFICINAPFNN',
		loadDataUrl:	'../app/zoNAOFICINAPFNN.do?method=loadData',
		filterDataUrl:	'../app/zoNAOFICINAPFNN.do?method=buscar'
	};
	
	var configObjectDetalle = {
			entity: '',
			renderTo: 'panelFeNROABONHABILITADOS',
			panelId: 'panelFeNROABONHABILITADOS',
			addUrl:			'../app/feNROABONHABILITADOS.do?method=altaFeNROABONHABILITADOS',
			modifyUrl:		'../app/feNROABONHABILITADOS.do?method=modificarFeNROABONHABILITADOS',
			removeUrl: 		'../app/feNROABONHABILITADOS.do?method=bajaFeNROABONHABILITADOS',
			loadDataUrl:	'../app/feNROABONHABILITADOS.do?method=loadData',
			filterDataUrl:	'../app/feNROABONHABILITADOS.do?method=ObtenerNumerosHabilitados'
		};

	
	var configObjectTipoFact = {
			entity: '',
			renderTo: 'panelFeTIPOSERV',
			panelId: 'panelFeTIPOSERV',
			addUrl:			'../app/feTIPOSERV.do?method=altaFeTIPOSERV',
			modifyUrl:		'../app/feTIPOSERV.do?method=modificarFeTIPOSERV',
			removeUrl: 		'../app/feTIPOSERV.do?method=bajaFeTIPOSERV',
			loadDataUrl:	'../app/feTIPOSERV.do?method=loadData',
			filterDataUrl:	'../app/feTIPOSERV.do?method=buscar'
		};
	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/zoNAOFICINAPFNN.do?method=Buscar',
			pagingToolbarUrl: 		'../app/zoNAOFICINAPFNN.do?method=Buscar',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			adabas_isnName: 'adabasisnZonaOficina'
		}
	};
	Tera.onReady(function(){

		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		
		
		var labelresultado = new Tera.form.Label	(
				{
					xtype: 'label', 
				    id: 'labelresultado',
				   	cls: 'x-form-item myBold',
				 //   style: 'font-weight:bold;color:red;' 
				   	style: 'font-weight:bold;' 
	 
		 	   }
		      );  

// PaneldeBusqueda - INICIO 
		var filtroBusqueda = new Tera.form.FilterPanel({
			mappedFields: [ 
				'fe_nro_fundamentalFilter'
			],
			filterConfig: filterConfig,
	        bodyBorder: false,
	        border: false,
	        bodyStyle: 'padding:5px',
	        buttonAlign: 'right',
	        width: 550,
	        baseCls: 'x-plain',
	        labelWidth: 130,
	       // keys: [{key: [10,13],handler: buscarsubmit}],//utilizado para que cuando aprete enter haga lo mismo que el boton buscar
	        height: 100,
	        
	        items: [
	                
				{
					xtype: 'label', 
				    id: 'labelcriterios',
				    cls: 'x-form-item myBold',
				    style: 'font-weight:bold;',
					    text: 'Criterios de Búsqueda',
				    labelAlign: 'left'
				},
				{
					xtype: 'label', 
				    id: 'labelspacio',
				    cls: 'x-form-item myBold',
				    style: 'font-weight:bold;',
					    text: '',
				    labelAlign: 'left'
				},               
	                
				{
	             xtype: 'textfield', 
				 width: 60,
				 fieldLabel: '<bean:message key="fe_zona_oficina_pfnn.fe_nro_fundamental_corto"  bundle="fields"/>',
				 name: 'fe_nro_fundamentalFilter',
				 id: 'fe_nro_fundamentalFilter',
				 height: 21,
				 maxLength: 6,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de 6 car&aacute;cteres',
				 msgTarget: 'side',
				 allowBlank: false,
				 blankText: 'El campo es requerido'

		  }
			 ]
	 		 
	    });
// PaneldeBusqueda - FIN 


// Store GRILLA Principal ZoNAOFICINAPFNN - INICIO 
	var store = new Tera.data.JsonStore({
	    	remoteSort: true,
	    	baseParams:{
	   			fe_nro_fundamentalFilter: ''
	    	},	
	        url: configObject.filterDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codZoNAOFICINAPFNN',
	        fields: [
	        	'adabas_isn'
	        	,
	         	'fe_grupo_medido'
				,
	        	'fe_descripcion'
				,
	        	'fe_zona_ofic_pla'
				,
	        	'fe_cf_canal_dd'
				,
	        	'fe_long_abo'
				,
	        	'fe_oficina_baja_tf'
				,
	        	'fe_ciclo_sigeco'
				,
	        	'fe_provincia'
				,
	        	'fe_zona'
				,
	        	'fe_zona_baja'
				,
	        	'fe_zona_baja_tf'
				,
	        	'fe_marca_cdn'
				,
	        	'fe_cod_interurbano'
				,
	        	'fe_marca_sigeco'
				,
	        	'fe_nro_area'
				,
	        	'fe_long_abo_tec'
				,
	        	'fe_cf_canal_hh24'
				,
	        	'fe_fecha_vigencia'
				,
	        	'fe_zona_uo'
				,
	        	'fe_recolector_info'
				,
	        	'fe_adherido_843'
				,
	        	'fe_fundamental_baja'
				,
	        	'fe_cod_estado_zo_ofi'
				,
	        	'fe_oficina'
				,
	        	'fe_region'
				,
	        	'fe_cod_vto_proc'
				,
	        	'fe_territorio'
				,
	        	'fe_marca_ddi'
				,
	        	'fe_tecnologia'
				,
	        	'fe_oficina_baja'
				,
	        	'fe_cod_empresa'
				,
	        	'fe_unidad_central'
				,
	        	'fe_cod_muni'
				,
	        	'fe_fecha_carga'
				,
	        	'fe_nro_fundamental'
				,
	        	'fe_usuario'
				,
	        	'fe_cod_vto_real'
	        	,
	        	'oficinaVar'
	        	,
	        	'zonaVar'
	        	,
	        	'leyenda'
				,
				'tipoServicio'
				
	        ]
	    	
	    	,
	 		listeners : { 
			    'load': function(store, records, options ) {
			
			    	Tera.MessageBox.hide();	
			
					storeDetalle.removeAll();
					
				  	gridFeNROABONHABILITADOS.setTitle('Información de Rangos de Facturación Habilitados');
					gridFeTIPOSERV.setTitle('Información de Tipos de Servicio');
					bbar1.updateInfo();
			 	}	  
 			} 
 	    });
// Store GRILLA Principal ZoNAOFICINAPFNN - FIN  	    
 	    
	
 
    	
    	
// GRilla Principal gridZoNAOFICINAPFNN - INICIO 

	    var cmZoNAOFICINAPFNN = new Tera.grid.ColumnModel([
		    new Tera.grid.RowNumberer(),
		 		    
		    {id: 'adabas_isn', header: '<bean:message key="fe_zona_oficina_pfnn.adabas_isn_corto"  bundle="fields"/>', dataIndex: 'adabas_isn', width: '<bean:message key="fe_zona_oficina_pfnn.adabas_isn_corto"  bundle="fields"/>'.length * 10, align:'center',  hidden: true}
			,
			{id: 'zonaVar', header: '<bean:message key="fe_zona_oficina_pfnn.fe_zona_corto"  bundle="fields"/>', dataIndex: 'zonaVar', width: '<bean:message key="fe_zona_oficina_pfnn.fe_zona_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'oficinaVar', header: '<bean:message key="fe_zona_oficina_pfnn.fe_oficina_corto"  bundle="fields"/>',  dataIndex: 'oficinaVar', width: '<bean:message key="fe_zona_oficina_pfnn.fe_oficina_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fe_descripcion', header: '<bean:message key="fe_zona_oficina_pfnn.fe_descripcion_corto"  bundle="fields"/>',  dataIndex: 'fe_descripcion', width: '<bean:message key="fe_zona_oficina_pfnn.fe_descripcion"  bundle="fields"/>'.length * 5, align:'center'}
 	  	    ,
		    {id: 'fe_long_abo', header: '<bean:message key="fe_zona_oficina_pfnn.fe_long_abo_corto"  bundle="fields"/>',  dataIndex: 'fe_long_abo', width: '<bean:message key="fe_zona_oficina_pfnn.fe_long_abo_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
			{id: 'fe_long_abo_tec', header: '<bean:message key="fe_zona_oficina_pfnn.fe_long_abo_tec_corto"  bundle="fields"/>',  dataIndex: 'fe_long_abo_tec', width: '<bean:message key="fe_zona_oficina_pfnn.fe_long_abo_tec_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
			{id: 'leyenda', header: '<bean:message key="fe_zona_oficina_pfnn.leyenda"  bundle="fields"/>',  dataIndex: 'leyenda', width: '<bean:message key="fe_zona_oficina_pfnn.fe_oficina_corto"  bundle="fields"/>'.length * 30, align:'center'}
			
			]);
	    cmZoNAOFICINAPFNN.defaultSortable = true;
	
	    var bbar1 = new Tera.PagingToolbar({
        	filterPanel: filtroBusqueda,
        	exportFile: false,
            exportGridName: 'ZoNAOFICINAPFNN',
            pageSize: 10,
            store: store,
            displayInfo: false,
            displayMsg: 'Mostrando   {0} - {1} de {2}',
            emptyMsg: '<span style="color:red;">' + 'El número Fundamental ingresado no existe.' + '</span>' 
        });
	    
	    
	    var gridZoNAOFICINAPFNN = new Tera.grid.GridPanel({
 	     	width: 645,
		 	height: 245,
	     	store: store,
	        cm: cmZoNAOFICINAPFNN,
	        trackMouseOver: true,
	        sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	        stripeRows: true,
	        viewConfig: {
    			forceFit: false,
	            enableRowBody: false
	        },
	        bbar: bbar1
	    });

// GRilla Principal gridZoNAOFICINAPFNN - FIN     
    


//Evento Click  de la grilla Principal - INICIO
	    
	    
		gridZoNAOFICINAPFNN.addListener('click', function(param) {
			 
			
			if(gridZoNAOFICINAPFNN.getSelectionModel().hasSelection()){
	    
   				var record_err = gridZoNAOFICINAPFNN.getSelectionModel().getSelected();
   			 
 	   		 	if (!Tera.isEmpty(record_err)){ 
	   			 	 
	   				var isn = record_err.get('adabas_isn');
	   				var nrofundamental = Tera.getCmp('fe_nro_fundamentalFilter').getValue();
	   				
	   				
	   				
 	   				gridFeNROABONHABILITADOS.setTitle('Información de Rangos de Facturación Habilitados nro : '+ nrofundamental);
 	   				gridFeTIPOSERV.setTitle('Información de Tipos de Servicio nro : ' + nrofundamental);
 				
 	   				storeDetalle.setBaseParamDetalleGrilla('adabas_isnFilter',isn);
 	   				storeTipoFact.setBaseParamDetalleTipoFact('adabas_isnFilter',isn);
 	   				storeDetalle.load({params: {start: 0,limit: 7}});
	   				storeTipoFact.load({params: {start: 0,limit: 7}});

	   			    bbar3.exportUrl = filterConfig.exportToolbarUrl;
	   				Tera.MessageBox.hide();				
	   			} 
	   			
	    	}    
	    	
	    	});
		
//Evento Click  de la grilla Principal - FIN
    	
    	
    	
    	
// Evento Boton Filtrar - INICIO 	      	    
	    var buttonFiltrar = new Tera.Button({
	        text: 'Buscar',
			minWidth: 80,
			handler: FiltrarSubmit
		});

	    function FiltrarSubmit(){
			
			
			// Si algún campo tiene algún error 
			if( filtroBusqueda.getForm().isValid()==false)
				{
						
			Tera.Msg
			.show({
				title : 'Información', //<- el título del diálogo   
				msg : 'Verifique la longitud de los campos que están señalizados en color rojo.', //<- El mensaje   
				buttons : Tera.Msg.OK, //<- Botones de SI y NO   
				icon : Tera.Msg.INFO, // <- un ícono de error   
				fn : this.callback
			//<- la función que se ejecuta cuando se da clic   
			});
			
	
				}
			
			
			var numeroFundamental = Tera.getCmp('fe_nro_fundamentalFilter').getValue();
      
			//Evaluo que los campos requeridos sean completados, si alguno esta vacion informo un mensaje de error
			if(numeroFundamental== "" )	 
			                    
			 {
			 	 
				 ///Creacion de una mensaje de usuario personalizado
				 Tera.Msg.show({   
		         title: 'Información', //<- el título del diálogo   
    			 msg: 'Para realizar la consulta, por favor ingrese un Número Fundamental', //<- El mensaje   
    		     buttons: Tera.Msg.OK, //<- Botones de SI y NO   
    			 icon: Tera.Msg.INFO, // <- un ícono de error   
   				 fn: this.callback //<- la función que se ejecuta cuando se da clic   
				});   
				 
				
				 
				 
				 return false;
			 }
		 
			
			
			filtroBusqueda.submit({
				waitMsg: '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
				url: configObject.filterDataUrl,
				
				//Success
				success: function(response, options){
					var responseText = Tera.decode(options.response.responseText);
					if (responseText.data != 'undefined' || responseText.data != null){
						gridZoNAOFICINAPFNN.getStore().loadData(responseText);
			   			 store.baseParams.fe_nro_fundamentalFilter = Tera.getCmp('fe_nro_fundamentalFilter').getValue();
			   		     storeDetalle.removeAll();	
			   		     storeTipoFact.removeAll();	
					}
					//Evaluo si el resultado de la consulta y muestro la leyenda
	                var cant = 0 ;
					cant = 	store.getTotalCount();
	                
	                if (cant > 0)
	                 {
	                	labelresultado.setText(' Resultado de la consulta : ' );
		     			
		            												
	                 }
	                else
	                	
	                 {
	                  	
	                	labelresultado.setText('<font color="#FF0000">'+ 'No existen datos que cumplan con los criterios de búsqueda ingresados.' +'</font>',false);
	                 	
	                
	                 }
				 	
				},
				//Failure					
				failure: function(response, options){
					var responseText = Tera.decode(options.response.responseText);
					Tera.MessageBox.alert('<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>', responseText.failure); 
				}
			});
		}

// Evento Boton Filtrar - FIN 	
	    
 		var map = new Tera.KeyMap(Tera.getBody(), {
		    key: Tera.EventObject.ENTER,
		    fn: function(){
		        FiltrarSubmit();
		    },
		    scope: this
		});  
	    
	    
	    
	    
	    
// Evento Boton Limpiar - INICIO 		    
	    var buttonLimpiar = new Tera.Button({text: '<bean:message key="app.label.limpiar"  bundle="messages"/>',
			minWidth: 80,
			handler: function(){
	  
				filtroBusqueda.getForm().reset();
				
				
				//Blanqueo ellabel de resultado
			 	labelresultado.setText(' '); 
				
				//Reseteo la barra del grid PagingToolbar
  	  			bbar1.field.dom.value = '1';
			    bbar1.afterTextEl.el.innerHTML = 'de 1'; 
			    bbar1.store.removeAll();
			    
			    bbar1.first.setDisabled(true);
				bbar1.prev.setDisabled(true);
				bbar1.next.setDisabled(true);
				bbar1.last.setDisabled(true); 
			 	
			
				
			 	bbar1.updateInfo();
				
	    		gridFeNROABONHABILITADOS.setTitle('Información de Rangos de Facturación Habilitados');
	    		gridFeTIPOSERV.setTitle('Información de Tipos de Servicio');
	    		
	    	
				store.removeAll();
				storeTipoFact.removeAll();
				storeDetalle.removeAll();
			 
				
			}
		});
		
// Evento Boton Limpiar - FIN 		
 








  
// Store Grilla Detalle  FeNROABONHABILITADOS - INICIO   
  
	 
	
 	 var storeDetalle = new Tera.data.JsonStore({
	    	remoteSort: true,
	    	url: configObjectDetalle.filterDataUrl,
		    //Agregado
	    	setBaseParamDetalleGrilla : function (name, value){
				this.baseParams = this.baseParams || {};
				this.baseParams[name] = value;
	  		},
		  	setColumnHeader : function(col, header){
		        this.config[col].header = header;
		        this.fireEvent("headerchange", this, col, header);
			},	
	  		
			remoteSort: true,
	        url: configObjectDetalle.filterDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codFeNROABONHABILITADOS',
	        fields: [
	        	'adabas_isn'
				,
	        	'dps_pe_seq'
				,
	        	'fe_nro_habil_hasta'
				,
	        	'fe_nro_habil_desde'
	        ]
	        
	        	        
	    });
 // Store Grilla Detalle  FeNROABONHABILITADOS - FIN   
  
 
 
 
 
// Store Grilla Detalle  FeTIPOSERV - INICIO 


	    var storeTipoFact = new Tera.data.JsonStore({
	    	remoteSort: true,
		    //Agregado
	    	setBaseParamDetalleTipoFact : function (name, value){
				this.baseParams = this.baseParams || {};
				this.baseParams[name] = value;
	  		},
		  	setColumnHeader : function(col, header){
		        this.config[col].header = header;
		        this.fireEvent("headerchange", this, col, header);
			},	
	        url: configObjectTipoFact.filterDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codFeTIPOSERV',
	        fields: [
	        	'adabas_isn'
				,
	        	'dps_mu_seq'
				,
	        	'fe_tipo_serv'
	        ]
	    });
	
// Store Grilla Detalle  FeTIPOSERV - INICIO    
 
 



// Grilla gridFeNROABONHABILITADOS - INICIO 
	var cmFeNROABONHABILITADOS = new Tera.grid.ColumnModel([
	                                         		    new Tera.grid.RowNumberer(),
	                                           		{id: 'fe_nro_habil_desde', header: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>',renderer: formateoRango , dataIndex: 'fe_nro_habil_desde', width: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>'.length * 10, align:'center'}
	                                                 ,
	                                           		{id: 'fe_nro_habil_hasta', header: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>',renderer: formateoRango , dataIndex: 'fe_nro_habil_hasta', width: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>'.length * 10, align:'center'}
	                                         		 	    ]);
	                                         	    cmFeNROABONHABILITADOS.defaultSortable = true;
	                            
	                                       	     var bbar3 = new Tera.PagingToolbar({
	                                       	     	
	                                 			    exportFile: false,
	                                 	            exportGridName: 'FeNROABONHABILITADOS',
	                                 	            pageSize: 10,
	                                 	            store: storeDetalle,
	                                 	            displayInfo: false,
	                                 	           displayMsg: 'Mostrando   {0} - {1} de {2}',
	                                 	            emptyMsg: 'No existen registros a mostrar.'
	                                 	        });
	                                 	                                       	    
	                                         	    
	                                         	    
	                                         	    
	                                         	    
	     var gridFeNROABONHABILITADOS = new Tera.grid.GridPanel({
	    	  						 width: 350,
      	       						 height: 200,
	                                  store: storeDetalle,
	                               	  cm: cmFeNROABONHABILITADOS,
	                               	  plugins: [new Tera.plugins.CopyPasteableGrid(), new Tera.plugins.FocusDetector()],
	                                  trackMouseOver: true,
	                                  sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	                                  loadMask: false,
	                  		          stripeRows: true,
	                  		          frame: false,
	                  		          title: 'Información de Rangos de Facturación Habilitados'
	                             ,
	                             bbar: bbar3
	                             });
	     
	     var bbar4 = new Tera.PagingToolbar({
	        	filterPanel: filtroBusqueda,
	        	exportFile: false,
	            exportGridName: 'FeTIPOSERV',
	            pageSize: 10,
	            store: storeTipoFact,
	            displayInfo: false,
	            displayMsg: 'Mostrando   {0} - {1} de {2}',
	            emptyMsg: 'No existen registros a mostrar.'
	        });
	     
	     function formateoRango(val) {
				
				
				//fuerzo al numero a un string 
				var original = "" + val;
				var result = original ;
		  			 
				
				//Relleno con ceros el campo 
			     while(result.length < 5 ){ 
			            result = "0"+ result; 
			        } 
 
			
		        return result;
			}
	     
// Grilla gridFeNROABONHABILITADOS - FIN  


// Grilla gridFeTIPOSERV - INICIO   
	     var cmFeTIPOSERV = new Tera.grid.ColumnModel([
	                                       		    new Tera.grid.RowNumberer(),
	                                       		//    {id: 'adabas_isn', header: '<bean:message key="fe_tipo_serv_1.adabas_isn_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'adabas_isn', width: '<bean:message key="fe_tipo_serv_1.adabas_isn_corto"  bundle="fields"/>'.length * 10, align:'center'}
	                                       		 ///   ,
	                                       		 ///   {id: 'dps_mu_seq', header: '<bean:message key="fe_tipo_serv_1.dps_mu_seq_corto"  bundle="fields"/>', dataIndex: 'dps_mu_seq', width: '<bean:message key="fe_tipo_serv_1.dps_mu_seq_corto"  bundle="fields"/>'.length * 10, align:'center'}
	                                       		//	,
	                                       		    {id: 'fe_tipo_serv', header: '<bean:message key="fe_tipo_serv_1.fe_tipo_serv_corto"  bundle="fields"/>', dataIndex: 'fe_tipo_serv', width: '<bean:message key="fe_tipo_serv_1.fe_tipo_serv_corto"  bundle="fields"/>'.length * 10, align:'center'}
	                                       	    ]);
	                                       	    cmFeTIPOSERV.defaultSortable = true;
	                                       	
	                                       	    var gridFeTIPOSERV = new Tera.grid.GridPanel({
	                                       	        width: 350,
	                                       	        height: 200,
	                                       	        store: storeTipoFact,
	                                       	        cm: cmFeTIPOSERV,
	                                       	     	plugins: [new Tera.plugins.CopyPasteableGrid(), new Tera.plugins.FocusDetector()],
	       	                                 		 trackMouseOver: true,
	                                       	        
	                                       	        sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	                                       	        stripeRows: true,
	                                       	        viewConfig: {
	                                           			forceFit: false,
	                                       	            enableRowBody: false
	                                       	        }
	                                       	      ,
	                                       			 title: 'Información de Tipos de Servicio'
	                                       	       ,
	                                       	        bbar: bbar4 
	                                       	    });     
// Grilla gridFeTIPOSERV - FIN   	                                       	    
	                                       	    
	                                       	    
	                                       	    
 
 
 // PANEL PRINCIPAL - INICIO 
	  
	                                         		var panel = new Tera.Panel({
	                                        	    	id: configObject.panelId,
	                                        	    	renderTo: configObject.renderTo,
	                                        	    //	layout: 'form',
	                                        	    	border: false,
	                                        	   		baseClass: 'x-plain',
	                                        			bodyStyle:'padding: 20px 0px 0px 0px',
	                                        			autoHeight:true,
	                                        	    	
	                                        	    	
	                                        	    	
	                                        	    	
	                                        	        defaults: {
	                                        	        	border: false
	                                        		    },
	                                        		    items:[{
	                                        	    	    border: false,
	                                        	    	    defaults: {
	                                        		        	border: false
	                                        			    },	    	    
	                                        	    	    items:[{
	                                        	    	    	layout:"column",
	                                        	    	    	border: false,
	                                        	    	    	defaults: {
	                                        	    	        	border: false
	                                        	    		    },
	                                        			        items:[
	                                        			        {
	                                        						columnWidth: 1,
	                                        					    items : [filtroBusqueda]
	                                        					},{
	                                        						width: 113,
	                                        						items: [{
	                                        					        border: false,
	                                        					    	defaults: {
	                                        					    		border: false
	                                        					    	},
	                                        							items: [
	                                        								{ items: [buttonFiltrar], style: 'margin-top: 10px;' },
	                                        								{ items: [buttonLimpiar], style: 'margin-top: 3px;' }
	                                        								
	                                        								
	                                        							]
	                                        						}]
	                                        					}]
	                                        				}]
	                                        	        }
	                                        			,
	                                        	        {
	                                        	        	layout:"fit",
	                                        	    	    border: false,	            
	                                        				style: 'padding: 0px 5px 0px 5px;',
	                                        	  	        items: [ labelresultado ]
	                                     
	                                        	        }
	                                        		    
	                                        		    ,
	                                        	        {
	                                        	        	layout:"fit",
	                                        	    	    border: false,	            
	                                        				style: 'padding: 0px 5px 0px 5px;',
	                                        	  	        items: [ gridZoNAOFICINAPFNN ]
	                                     
	                                        	        }
	                                        			 , 
	                                        
		                                         	        {
		                                         	        	layout:"fit",
		                                         	    	    border: false,
		                                         				items:[{
		                                         				    layout:"column",
		                                         				    style: 'margin-top: 8px;',
		                                         				    defaults: {
		                                         				    	border: false
		                                         					},
		                                         					border: false,
		                                         				    items:[{
		                                         				
		                                         				        width:100 
		                                         				       
		                                         				      }]
		                                         				  }]
		                                         			}
	                                        			
                                         	     	   ,
                                         	     	 
                                         	     	   
                                      
                                         	     	   
                                         	    	{
	                                         	         	layout:"column",
	                                         	    	    border: true,	            
	                                         				style: 'padding: 0px 2px 0px 2px;',
	                                         	            items: [ gridFeTIPOSERV,
	                                         	                    gridFeNROABONHABILITADOS
	                                         	                     ]
	                                         	     }
        
	                                        	        ]
	                                        		    
	                                        		 	    
	                                        	    }); 
	                                         		
// PANEL PRINCIPAL - FIN                                          		
	                                 
	});
</script>

<div id="panelZoNAOFICINAPFNN"></div>

