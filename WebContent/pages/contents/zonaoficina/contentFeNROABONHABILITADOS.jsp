<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<!-- JS Resources -->
<script type="text/javascript">
	
	var configObject = {
		entity: '',
		renderTo: 'panelFeNROABONHABILITADOS',
		panelId: 'panelFeNROABONHABILITADOS',
		addUrl:			'../app/feNROABONHABILITADOS.do?method=altaFeNROABONHABILITADOS',
		modifyUrl:		'../app/feNROABONHABILITADOS.do?method=modificarFeNROABONHABILITADOS',
		removeUrl: 		'../app/feNROABONHABILITADOS.do?method=bajaFeNROABONHABILITADOS',
		loadDataUrl:	'../app/feNROABONHABILITADOS.do?method=loadData',
		filterDataUrl:	'../app/feNROABONHABILITADOS.do?method=filtrar'
	};

	var filterConfig = {
		urls: {
		    pagingToolbarFilterUrl: '../app/feNROABONHABILITADOS.do?method=filtrar',
			pagingToolbarUrl: 		'../app/feNROABONHABILITADOS.do?method=loadData',
			exportToolbarUrl: '../app/export.do?method=exportExcel'
		}
	};
var lupaConfig =  {
		urls: {
	        descriptionRetrival: '../app/lupas.do?method=getDescription',
	        url: '../app/lupas.do?method=filtrar'
		},
		entities: {
			adabas_isnName: 'adabasisnNroHabilitado'
		    ,
			dps_pe_seqName: 'dpspeseqNroHabilitado'
		}
	};
	Tera.onReady(function(){

		Tera.QuickTips.init();
		Tera.BLANK_IMAGE_URL = '../img/default/s.gif';
		var filtroBusqueda = new Tera.form.FilterPanel({
			mappedFields: [ 
				'fe_nro_habil_hastaFilter'
				,
				'fe_nro_habil_desdeFilter'
			],
			filterConfig: filterConfig,
	        bodyBorder: false,
	        border: false,
	        bodyStyle: 'padding:10px',
	        buttonAlign: 'right',
	        width: 550,
	        baseCls: 'x-plain',
	        labelWidth: 130,
	        items: [
				{
	             xtype: 'numberfield',
				 width: 98,
				 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>',
				 name: 'fe_nro_habil_hastaFilter',
				 id: 'fe_nro_habil_hastaFilter',
				 height: 21,
				 maxLength: 10,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres'
			    }
			    ,
				{
	             xtype: 'numberfield',
				 width: 98,
				 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>',
				 name: 'fe_nro_habil_desdeFilter',
				 id: 'fe_nro_habil_desdeFilter',
				 height: 21,
				 maxLength: 10,
				 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres'
			    }
	 		 ]
	    });
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	    var store = new Tera.data.JsonStore({
	    	remoteSort: true,
	    	baseParams:{
	   			fe_nro_habil_hastaFilter: ''
				,
	   			fe_nro_habil_desdeFilter: ''
	    	},	
	        url: configObject.loadDataUrl,
	        root: 'data',
	        totalProperty: 'totalCount',
	        id: 'codFeNROABONHABILITADOS',
	        fields: [
	        	'adabas_isn'
				,
	        	'dps_pe_seq'
				,
	        	'fe_nro_habil_hasta'
				,
	        	'fe_nro_habil_desde'
	        ]
	    });
	
	 	function changeNumber(val){
        	var original=parseFloat(val);
			var result = original.toFixed(3);
        	if(val < 0){
 	           	return '<span style="color:red;">' + result + '</span>';
        	}
    
        	return result;
    	}
	
	    var cmFeNROABONHABILITADOS = new Tera.grid.ColumnModel([
		    new Tera.grid.RowNumberer(),
		    {id: 'adabas_isn', header: '<bean:message key="fe_nro_abon_habilitados_1.adabas_isn_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'adabas_isn', width: '<bean:message key="fe_nro_abon_habilitados_1.adabas_isn_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'dps_pe_seq', header: '<bean:message key="fe_nro_abon_habilitados_1.dps_pe_seq_corto"  bundle="fields"/>', dataIndex: 'dps_pe_seq', width: '<bean:message key="fe_nro_abon_habilitados_1.dps_pe_seq_corto"  bundle="fields"/>'.length * 10, align:'center'}
			,
		    {id: 'fe_nro_habil_hasta', header: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fe_nro_habil_hasta', width: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>'.length * 10, align:'center'}
		    ,
		    {id: 'fe_nro_habil_desde', header: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>', renderer: changeNumber, dataIndex: 'fe_nro_habil_desde', width: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>'.length * 10, align:'center'}
	    ]);
	    cmFeNROABONHABILITADOS.defaultSortable = true;
	
	    var gridFeNROABONHABILITADOS = new Tera.grid.GridPanel({
	        width: 625,
	        height: 245,
	        store: store,
	        cm: cmFeNROABONHABILITADOS,
	        trackMouseOver: true,
	        sm: new Tera.grid.RowSelectionModel( { singleSelect: true } ),
	        stripeRows: true,
	        viewConfig: {
    			forceFit: false,
	            enableRowBody: false
	        },
	        bbar: new Tera.PagingToolbar({
	        	filterPanel: filtroBusqueda,
	        	exportFile: true,
	            exportGridName: 'FeNROABONHABILITADOS',
	            pageSize: 10,
	            store: store,
	            displayInfo: true,
	            displayMsg: 'Mostrando   {0} - {1} de {2}',
	            emptyMsg: 'No existen registros a mostrar.'
	        })
	    });

		gridFeNROABONHABILITADOS.addListener('click', function(param) {
			if(gridFeNROABONHABILITADOS.getSelectionModel().hasSelection()){
	   			buttonBaja.setDisabled(false);
	   			buttonModificar.setDisabled(false); 
	   			buttonVisualizar.setDisabled(false);
	    	} else{
	    		buttonBaja.setDisabled(true);
	   			buttonModificar.setDisabled(true);  
	   			buttonVisualizar.setDisabled(true);
	    	}
	    });

    	store.load({params: {start: 0,limit: 10}}); 
	    
	    var buttonAlta = new Tera.Button({text: '<bean:message key="app.label.alta"  bundle="messages"/>', minWidth: 80});
	    var buttonBaja = new Tera.Button({text: '<bean:message key="app.label.baja"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonModificar = new Tera.Button({text: '<bean:message key="app.label.modificar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    var buttonVisualizar = new Tera.Button({text: '<bean:message key="app.label.visualizar"  bundle="messages"/>', minWidth: 80, disabled: true});
	    	    
	    var buttonFiltrar = new Tera.Button({
	        text: 'Filtrar',
			minWidth: 80,
			handler: function(){
				filtroBusqueda.submit({
					waitMsg: '<bean:message key="app.label.cargandoDatos"  bundle="messages"/>',
					url: configObject.filterDataUrl,
					success: function(response, options){
						var responseText = Tera.decode(options.response.responseText);
						if (responseText.data != 'undefined' || responseText.data != null){
							gridFeNROABONHABILITADOS.getStore().loadData(responseText);
				   			 store.baseParams.fe_nro_habil_hastaFilter = Tera.getCmp('fe_nro_habil_hastaFilter').getValue();
				   			 store.baseParams.fe_nro_habil_desdeFilter = Tera.getCmp('fe_nro_habil_desdeFilter').getValue();
						}
					},
					failure: function(response, options){
						var responseText = Tera.decode(options.response.responseText);
						Tera.MessageBox.alert('<bean:message key="app.label.cargando.errorFiltrado"  bundle="messages"/>', responseText.failure); 
					}
				});
			}
		});
	    
	    var buttonLimpiar = new Tera.Button({text: '<bean:message key="app.label.limpiar"  bundle="messages"/>',
			minWidth: 80,
			handler: function(){
		   		store.baseParams.fe_nro_habil_hastaFilter = '';
		   		store.baseParams.fe_nro_habil_desdeFilter = '';
				filtroBusqueda.clearForm();
	    		gridFeNROABONHABILITADOS.getStore().load();
			}
		});
		var panel = new Tera.Panel({
	    	id: configObject.panelId,
	    	renderTo: configObject.renderTo,
	    	border: false,
	        defaults: {
	        	border: false
		    },
		    items:[{
	    	    border: false,
	    	    defaults: {
		        	border: false
			    },	    	    
	    	    items:[{
	    	    	layout:"column",
	    	    	border: false,
	    	    	defaults: {
	    	        	border: false
	    		    },
			        items:[
			        {
						columnWidth: 1,
					    items : [filtroBusqueda]
					},{
						width: 113,
						items: [{
					        border: false,
					    	defaults: {
					    		border: false
					    	},
							items: [
								{ items: [buttonFiltrar], style: 'margin-top: 10px;' },
								{ items: [buttonLimpiar], style: 'margin-top: 3px;' }
							]
						}]
					}]
				}]
	        },
	        {
	        	layout:"fit",
	    	    border: false,	            
				style: 'padding: 0px 5px 0px 5px;',
	            items: [ gridFeNROABONHABILITADOS ]
	        },{
	        	layout:"fit",
	    	    border: false,
				items:[{
				    layout:"column",
				    style: 'margin-top: 8px;',
				    defaults: {
				    	border: false
					},
					border: false,
				    items:[{
				        width:85,
				        items:[buttonAlta]
				      },{
				        width:85,
				        items:[buttonBaja]
				      },{
				        width:85,
				        items:[buttonModificar]
				      },{
				        width:85,
				        items:[buttonVisualizar]
				      }]
				  }]
			}]
	    });
			
   buttonAlta.on('click', function(){
	  
			var aceptar = new Tera.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				disabled: false, 
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmar"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_a"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
								form.getForm().submit({
				                	url: configObject.addUrl,
				                	success: function(response, options) {
				                		win.close();
			                			buttonFiltrar.handler.call(buttonFiltrar.scope);
				                	},
				                	failure: function(response, options) {
				                		var responseText = Tera.decode(options.response.responseText);
				                		Tera.MessageBox.alert('<bean:message key="app.validaciones.errorAlta"  bundle="messages"/>', responseText.failure);
			                	}
		                    });
					    }
					});
				}
			});
				
			var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80, handler: function() { win.close(); }});

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
					{
	    	         xtype: 'numberfield',
					 width: 98,
					 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.adabas_isn_corto"  bundle="fields"/>',
					 name: 'adabas_isn',
					 id: 'adabas_isn',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
				    }
 				    ,
					{
		             xtype: 'textfield',
					 width: 20,
					 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.dps_pe_seq_corto"  bundle="fields"/>',
					 name: 'dps_pe_seq',
					 id: 'dps_pe_seq',
					 maxLength: 1,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
					 height: 21
				    }
 				    ,
					{
	    	         xtype: 'numberfield',
					 width: 98,
					 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>',
					 name: 'fe_nro_habil_hasta',
					 id: 'fe_nro_habil_hasta',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
				    }
 				    ,
					{
	    	         xtype: 'numberfield',
					 width: 98,
					 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>',
					 name: 'fe_nro_habil_desde',
					 id: 'fe_nro_habil_desde',
					 maxLength: 10,
					 maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
					 height: 21
				    }
 				 ]         
			});
			
			var win = new Tera.Window({
		    	title: '<bean:message key="app.label.alta"  bundle="messages"/>',
		        border: false,
				modal: true,
				width: 288 + 50,
				height: 200 + 15,
				resizable: false,
				layout: 'fit',
				resizable: false,
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				items: form,
				buttons: [aceptar, cancelar]
			});
			win.show(this);
		});
	    
  buttonModificar.on('click', function(){
		    	if (!gridFeNROABONHABILITADOS.getSelectionModel().hasSelection()) {
		    		return false;
		    	}
		    	var aceptar = new Tera.Button({
					text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
					minWidth: 80,
					disabled: false, 
					handler: function() {
				    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_m"  bundle="messages"/>', function(btn) {
				    		if ('yes' == btn) {
				    			form.getForm().submit({
									url: configObject.modifyUrl,
									success: function(response, options) {
										win.close();
			                			buttonFiltrar.handler.call(buttonFiltrar.scope);
									},
									failure: function(response, options) {
										var responseText = Tera.decode(options.response.responseText);
										Tera.MessageBox.alert('<bean:message key="app.validaciones.errorModificacion"  bundle="messages"/>', responseText.failure);
									}
								});
						    }
						});
					}
				});

		    	var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			    	
		 	    	
	    	    var recordToModify = gridFeNROABONHABILITADOS.getSelectionModel().getSelected().data;

	 	        var form = new Tera.form.FormPanel({
	 				baseCls: 'x-plain',
	 				labelWidth: 130,
	 				monitorValid: true,
	 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
	 				items: [
 					    {
  	 	                 xtype: 'numberfield', 
 	 				     width: 98,
	 				     fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.adabas_isn_corto"  bundle="fields"/>',
	 				     name: 'adabas_isn',
	 				     id: 'adabas_isn',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     readOnly: true,
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.adabas_isn 
	 			        }
 				        ,
 					    {
 	 	                 xtype: 'textfield', 
  	 				     width: 20,
	 				     fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.dps_pe_seq_corto"  bundle="fields"/>',
	 				     name: 'dps_pe_seq',
	 				     id: 'dps_pe_seq',
	 				     height: 21,
	 				     maxLength: 1,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 1 car&aacute;cteres',
	 				     readOnly: true,
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.dps_pe_seq 
	 			        }
 				        ,
 					    {
  	 	                 xtype: 'numberfield', 
 	 				     width: 98,
	 				     fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>',
	 				     name: 'fe_nro_habil_hasta',
	 				     id: 'fe_nro_habil_hasta',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     readOnly: true,
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fe_nro_habil_hasta 
	 			        }
 				        ,
 					    {
  	 	                 xtype: 'numberfield', 
 	 				     width: 98,
	 				     fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>',
	 				     name: 'fe_nro_habil_desde',
	 				     id: 'fe_nro_habil_desde',
	 				     height: 21,
	 				     maxLength: 10,
	 				     maxLengthText: 'El campo tiene un m&aacute;ximo de 10 car&aacute;cteres',
	 				     readOnly: true,
	 				     msgTarget: 'side', 
	 				     blankText: 'El campo es requerido',  
  	 				     value: recordToModify.fe_nro_habil_desde 
	 			        }
 		 		]         
		 	});

 			var win = new Tera.Window({
 	        	title: '<bean:message key="app.label.modificacion"  bundle="messages"/>',
 			    modal: true,
 				width: 288 + 50,
 				height: 200+ 15,
 				resizable: false,
 			    layout: 'fit',
 			    plain:true,
 			    bodyStyle:'padding: 10px;',
 			    buttonAlign: 'right',
 				border: false,
 				items: [ form ],
 				buttons: [aceptar, cancelar]
 			});
 	        win.show();
	  });
	    
  buttonBaja.on('click', function(e){
	    	if (!gridFeNROABONHABILITADOS.getSelectionModel().hasSelection()) { 
					return false;
	    	}

			var aceptar = new Tera.Button({
				text: '<bean:message key="app.label.aceptar"  bundle="messages"/>', 
				minWidth: 80,
				handler: function() {
			    	Tera.MessageBox.confirm('<bean:message key="app.label.confirmacion"  bundle="messages"/>', '<bean:message key="app.confirmacion.mensaje_b"  bundle="messages"/>', function(btn) {
			    		if ('yes' == btn) {
			    			form.getForm().submit({
								  url: configObject.removeUrl,
								  success: function(response, options) {
									win.close();
			 						buttonFiltrar.handler.call(buttonFiltrar.scope);
								  },
								  failure: function(response, options) {
									var responseText = Tera.decode(options.response.responseText);
									Tera.MessageBox.alert('<bean:message key="app.validaciones.errorBaja"  bundle="messages"/>', responseText.failure);
								  }
							});
					    }
					});
				}
			});
	      	
   			var cancelar = new Tera.Button({text: '<bean:message key="app.label.cancelar"  bundle="messages"/>' ,minWidth: 80, handler: function() { win.close(); }});
	    	
			var recordToShow = gridFeNROABONHABILITADOS.getSelectionModel().getSelected().data;

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				monitorValid: true,
 				listeners : {'clientvalidation':function(pForm, pState){((pState)?aceptar.enable():aceptar.disable())}},
				items: [
			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.adabas_isn_corto"  bundle="fields"/>',
		  			 width: 98 + 17,
		  			 id: 'adabas_isn',
		  			 name: 'adabas_isn',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.adabas_isnName,
		             value: recordToShow.adabas_isn
	  			   }
			       ,
 			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.dps_pe_seq_corto"  bundle="fields"/>',
		  			 width: 20 + 17,
		  			 id: 'dps_pe_seq',
		  			 name: 'dps_pe_seq',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.dps_pe_seqName,
		             value: recordToShow.dps_pe_seq
	  			   }
			       ,
 				   {
	    	        xtype: 'numberfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>',
					id: 'fe_nro_habil_hasta',
					readOnly: true,
					name: 'fe_nro_habil_hasta',
					height: 21,
					value: recordToShow.fe_nro_habil_hasta 
				   }
				   ,
 				   {
	    	        xtype: 'numberfield', 
					allowBlank: false,  
					msgTarget: 'side', 
					blankText: 'El campo es requerido',  
					width: 98,
					fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>',
					id: 'fe_nro_habil_desde',
					readOnly: true,
					name: 'fe_nro_habil_desde',
					height: 21,
					value: recordToShow.fe_nro_habil_desde 
				   }
 				]
			});
			
			var win = new Tera.Window({
				title: '<bean:message key="app.label.baja"  bundle="messages"/>',
				width: 288 + 50,
				height: 200 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [aceptar,cancelar]
			});
	        win.show(this);
			
	    });
	    
  buttonVisualizar.on('click', function(){
			if (!gridFeNROABONHABILITADOS.getSelectionModel().hasSelection()){
				return false;
			}

			var cerrar = new Tera.Button({text: '<bean:message key="app.label.cerrar"  bundle="messages"/>',minWidth: 80,handler : function(){	win.close();}});
			
			var recordToShow = gridFeNROABONHABILITADOS.getSelectionModel().getSelected().data;

			var form = new Tera.form.FormPanel({
				baseCls: 'x-plain',
				labelWidth: 130,
				items: [
			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.adabas_isn_corto"  bundle="fields"/>',
		  			 width: 98 + 17,
		  			 id: 'adabas_isn',
		  			 name: 'adabas_isn',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.adabas_isnName,
		             value: recordToShow.adabas_isn
	  			   }
 				   ,
			       {
	             	 xtype: 'lupafield',
		  			 fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.dps_pe_seq_corto"  bundle="fields"/>',
		  			 width: 20 + 17,
		  			 id: 'dps_pe_seq',
		  			 name: 'dps_pe_seq',
		  			 showClearButton: false,
					 showSearchButton: false,
 					 readOnly:true,
		  			 limitDescription:30,	
		  			 descriptionRetrival: lupaConfig.urls.descriptionRetrival,
		             url: lupaConfig.urls.url,
		             entityName: lupaConfig.entities.dps_pe_seqName,
		             value: recordToShow.dps_pe_seq
	  			   }
 				   ,
				   {
	    	        xtype: 'numberfield', 
					width: 98,
					fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_hasta_corto"  bundle="fields"/>',
					id: 'fe_nro_habil_hasta',
					readOnly: true,
					name: 'fe_nro_habil_hasta',
					height: 21,
					value: recordToShow.fe_nro_habil_hasta 
				   }
 				   ,
				   {
	    	        xtype: 'numberfield', 
					width: 98,
					fieldLabel: '<bean:message key="fe_nro_abon_habilitados_1.fe_nro_habil_desde_corto"  bundle="fields"/>',
					id: 'fe_nro_habil_desde',
					readOnly: true,
					name: 'fe_nro_habil_desde',
					height: 21,
					value: recordToShow.fe_nro_habil_desde 
				   }
 				]
			});

			
			var win = new Tera.Window({
 	        	title: '<bean:message key="app.label.visualizacion"  bundle="messages"/>',
				width: 288 + 50,
				height: 200 + 15,
				resizable: false,
				modal: true,
				layout: 'fit',
				plain: true,
				bodyStyle: 'padding: 10px;',
				buttonAlign: 'right',
				border: false,
				items: [ form ],
				buttons: [cerrar]
			});
	        win.show(this);
	    });
	    
	});
</script>
	
<div id="panelFeNROABONHABILITADOS"></div>