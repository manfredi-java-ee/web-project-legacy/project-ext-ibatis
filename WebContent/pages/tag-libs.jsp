<!-- JSTL taglibs -->
<%@ taglib prefix="core" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="frmt" uri="http://java.sun.com/jstl/fmt" %>

<!-- JSON taglibs -->
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<!-- Struts taglibs -->
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>