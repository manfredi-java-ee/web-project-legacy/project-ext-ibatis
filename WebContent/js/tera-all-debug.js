/*
 *
 * 
 * 
 * 
 */

Tera.DomHelper = function() {
	var tempTableEl = null;
	var emptyTags = /^(?:br|frame|hr|img|input|link|meta|range|spacer|wbr|area|param|col)$/i;
	var tableRe = /^table|tbody|tr|td$/i;

	var createHtml = function(o) {
		if (typeof o == 'string') {
			return o;
		}
		var b = "";
		if (Tera.isArray(o)) {
			for ( var i = 0, l = o.length; i < l; i++) {
				b += createHtml(o[i]);
			}
			return b;
		}
		if (!o.tag) {
			o.tag = "div";
		}
		b += "<" + o.tag;
		for ( var attr in o) {
			if (attr == "tag" || attr == "children" || attr == "cn" || attr == "html" || typeof o[attr] == "function")
				continue;
			if (attr == "style") {
				var s = o["style"];
				if (typeof s == "function") {
					s = s.call();
				}
				if (typeof s == "string") {
					b += ' style="' + s + '"';
				} else if (typeof s == "object") {
					b += ' style="';
					for ( var key in s) {
						if (typeof s[key] != "function") {
							b += key + ":" + s[key] + ";";
						}
					}
					b += '"';
				}
			} else {
				if (attr == "cls") {
					b += ' class="' + o["cls"] + '"';
				} else if (attr == "htmlFor") {
					b += ' for="' + o["htmlFor"] + '"';
				} else {
					b += " " + attr + '="' + o[attr] + '"';
				}
			}
		}
		if (emptyTags.test(o.tag)) {
			b += "/>";
		} else {
			b += ">";
			var cn = o.children || o.cn;
			if (cn) {
				b += createHtml(cn);
			} else if (o.html) {
				b += o.html;
			}
			b += "</" + o.tag + ">";
		}
		return b;
	};

	var createDom = function(o, parentNode) {
		var el;
		if (Tera.isArray(o)) {
			el = document.createDocumentFragment();
			for ( var i = 0, l = o.length; i < l; i++) {
				createDom(o[i], el);
			}
		} else if (typeof o == "string") {
			el = document.createTextNode(o);
		} else {
			el = document.createElement(o.tag || 'div');
			var useSet = !!el.setAttribute;
			for ( var attr in o) {
				if (attr == "tag" || attr == "children" || attr == "cn" || attr == "html" || attr == "style" || typeof o[attr] == "function")
					continue;
				if (attr == "cls") {
					el.className = o["cls"];
				} else {
					if (useSet)
						el.setAttribute(attr, o[attr]);
					else
						el[attr] = o[attr];
				}
			}
			Tera.DomHelper.applyStyles(el, o.style);
			var cn = o.children || o.cn;
			if (cn) {
				createDom(cn, el);
			} else if (o.html) {
				el.innerHTML = o.html;
			}
		}
		if (parentNode) {
			parentNode.appendChild(el);
		}
		return el;
	};

	var ieTable = function(depth, s, h, e) {
		tempTableEl.innerHTML = [ s, h, e ].join('');
		var i = -1, el = tempTableEl;
		while (++i < depth) {
			el = el.firstChild;
		}
		return el;
	};

	var ts = '<table>', te = '</table>', tbs = ts + '<tbody>', tbe = '</tbody>' + te, trs = tbs + '<tr>', tre = '</tr>' + tbe;

	var insertIntoTable = function(tag, where, el, html) {
		if (!tempTableEl) {
			tempTableEl = document.createElement('div');
		}
		var node;
		var before = null;
		if (tag == 'td') {
			if (where == 'afterbegin' || where == 'beforeend') {
				return;
			}
			if (where == 'beforebegin') {
				before = el;
				el = el.parentNode;
			} else {
				before = el.nextSibling;
				el = el.parentNode;
			}
			node = ieTable(4, trs, html, tre);
		} else if (tag == 'tr') {
			if (where == 'beforebegin') {
				before = el;
				el = el.parentNode;
				node = ieTable(3, tbs, html, tbe);
			} else if (where == 'afterend') {
				before = el.nextSibling;
				el = el.parentNode;
				node = ieTable(3, tbs, html, tbe);
			} else {
				if (where == 'afterbegin') {
					before = el.firstChild;
				}
				node = ieTable(4, trs, html, tre);
			}
		} else if (tag == 'tbody') {
			if (where == 'beforebegin') {
				before = el;
				el = el.parentNode;
				node = ieTable(2, ts, html, te);
			} else if (where == 'afterend') {
				before = el.nextSibling;
				el = el.parentNode;
				node = ieTable(2, ts, html, te);
			} else {
				if (where == 'afterbegin') {
					before = el.firstChild;
				}
				node = ieTable(3, tbs, html, tbe);
			}
		} else {
			if (where == 'beforebegin' || where == 'afterend') {
				return;
			}
			if (where == 'afterbegin') {
				before = el.firstChild;
			}
			node = ieTable(2, ts, html, te);
		}
		el.insertBefore(node, before);
		return node;
	};

	return {

		useDom :false,

		markup : function(o) {
			return createHtml(o);
		},

		applyStyles : function(el, styles) {
			if (styles) {
				el = Tera.fly(el);
				if (typeof styles == "string") {
					var re = /\s?([a-z\-]*)\:\s?([^;]*);?/gi;
					var matches;
					while ((matches = re.exec(styles)) != null) {
						el.setStyle(matches[1], matches[2]);
					}
				} else if (typeof styles == "object") {
					for ( var style in styles) {
						el.setStyle(style, styles[style]);
					}
				} else if (typeof styles == "function") {
					Tera.DomHelper.applyStyles(el, styles.call());
				}
			}
		},

		insertHtml : function(where, el, html) {
			where = where.toLowerCase();
			if (el.insertAdjacentHTML) {
				if (tableRe.test(el.tagName)) {
					var rs;
					if (rs = insertIntoTable(el.tagName.toLowerCase(), where, el, html)) {
						return rs;
					}
				}
				switch (where) {
					case "beforebegin":
						el.insertAdjacentHTML('BeforeBegin', html);
						return el.previousSibling;
					case "afterbegin":
						el.insertAdjacentHTML('AfterBegin', html);
						return el.firstChild;
					case "beforeend":
						el.insertAdjacentHTML('BeforeEnd', html);
						return el.lastChild;
					case "afterend":
						el.insertAdjacentHTML('AfterEnd', html);
						return el.nextSibling;
				}
				throw 'Illegal insertion point -> "' + where + '"';
			}
			var range = el.ownerDocument.createRange();
			var frag;
			switch (where) {
				case "beforebegin":
					range.setStartBefore(el);
					frag = range.createContextualFragment(html);
					el.parentNode.insertBefore(frag, el);
					return el.previousSibling;
				case "afterbegin":
					if (el.firstChild) {
						range.setStartBefore(el.firstChild);
						frag = range.createContextualFragment(html);
						el.insertBefore(frag, el.firstChild);
						return el.firstChild;
					} else {
						el.innerHTML = html;
						return el.firstChild;
					}
				case "beforeend":
					if (el.lastChild) {
						range.setStartAfter(el.lastChild);
						frag = range.createContextualFragment(html);
						el.appendChild(frag);
						return el.lastChild;
					} else {
						el.innerHTML = html;
						return el.lastChild;
					}
				case "afterend":
					range.setStartAfter(el);
					frag = range.createContextualFragment(html);
					el.parentNode.insertBefore(frag, el.nextSibling);
					return el.nextSibling;
			}
			throw 'Illegal insertion point -> "' + where + '"';
		},

		insertBefore : function(el, o, returnElement) {
			return this.doInsert(el, o, returnElement, "beforeBegin");
		},

		insertAfter : function(el, o, returnElement) {
			return this.doInsert(el, o, returnElement, "afterEnd", "nextSibling");
		},

		insertFirst : function(el, o, returnElement) {
			return this.doInsert(el, o, returnElement, "afterBegin", "firstChild");
		},

		doInsert : function(el, o, returnElement, pos, sibling) {
			el = Tera.getDom(el);
			var newNode;
			if (this.useDom) {
				newNode = createDom(o, null);
				(sibling === "firstChild" ? el : el.parentNode).insertBefore(newNode, sibling ? el[sibling] : el);
			} else {
				var html = createHtml(o);
				newNode = this.insertHtml(pos, el, html);
			}
			return returnElement ? Tera.get(newNode, true) : newNode;
		},

		append : function(el, o, returnElement) {
			el = Tera.getDom(el);
			var newNode;
			if (this.useDom) {
				newNode = createDom(o, null);
				el.appendChild(newNode);
			} else {
				var html = createHtml(o);
				newNode = this.insertHtml("beforeEnd", el, html);
			}
			return returnElement ? Tera.get(newNode, true) : newNode;
		},

		overwrite : function(el, o, returnElement) {
			el = Tera.getDom(el);
			el.innerHTML = createHtml(o);
			return returnElement ? Tera.get(el.firstChild, true) : el.firstChild;
		},

		createTemplate : function(o) {
			var html = createHtml(o);
			return new Tera.Template(html);
		}
	};
}();

Tera.Template = function(html) {
	var a = arguments;
	if (Tera.isArray(html)) {
		html = html.join("");
	} else if (a.length > 1) {
		var buf = [];
		for ( var i = 0, len = a.length; i < len; i++) {
			if (typeof a[i] == 'object') {
				Tera.apply(this, a[i]);
			} else {
				buf[buf.length] = a[i];
			}
		}
		html = buf.join('');
	}

	this.html = html;
	if (this.compiled) {
		this.compile();
	}
};
Tera.Template.prototype = {

	applyTemplate : function(values) {
		if (this.compiled) {
			return this.compiled(values);
		}
		var useF = this.disableFormats !== true;
		var fm = Tera.util.Format, tpl = this;
		var fn = function(m, name, format, args) {
			if (format && useF) {
				if (format.substr(0, 5) == "this.") {
					return tpl.call(format.substr(5), values[name], values);
				} else {
					if (args) {
						var re = /^\s*['"](.*)["']\s*$/;
						args = args.split(',');
						for ( var i = 0, len = args.length; i < len; i++) {
							args[i] = args[i].replace(re, "$1");
						}
						args = [ values[name] ].concat(args);
					} else {
						args = [ values[name] ];
					}
					return fm[format].apply(fm, args);
				}
			} else {
				return values[name] !== undefined ? values[name] : "";
			}
		};
		return this.html.replace(this.re, fn);
	},

	set : function(html, compile) {
		this.html = html;
		this.compiled = null;
		if (compile) {
			this.compile();
		}
		return this;
	},

	disableFormats :false,

	re :/\{([\w-]+)(?:\:([\w\.]*)(?:\((.*?)?\))?)?\}/g,

	compile : function() {
		var fm = Tera.util.Format;
		var useF = this.disableFormats !== true;
		var sep = Tera.isGecko ? "+" : ",";
		var fn = function(m, name, format, args) {
			if (format && useF) {
				args = args ? ',' + args : "";
				if (format.substr(0, 5) != "this.") {
					format = "fm." + format + '(';
				} else {
					format = 'this.call("' + format.substr(5) + '", ';
					args = ", values";
				}
			} else {
				args = '';
				format = "(values['" + name + "'] == undefined ? '' : ";
			}
			return "'" + sep + format + "values['" + name + "']" + args + ")" + sep + "'";
		};
		var body;
		if (Tera.isGecko) {
			body = "this.compiled = function(values){ return '"
					+ this.html.replace(/\\/g, '\\\\').replace(/(\r\n|\n)/g, '\\n').replace(/'/g, "\\'").replace(this.re, fn) + "';};";
		} else {
			body = [ "this.compiled = function(values){ return ['" ];
			body.push(this.html.replace(/\\/g, '\\\\').replace(/(\r\n|\n)/g, '\\n').replace(/'/g, "\\'").replace(this.re, fn));
			body.push("'].join('');};");
			body = body.join('');
		}
		eval(body);
		return this;
	},

	call : function(fnName, value, allValues) {
		return this[fnName](value, allValues);
	},

	insertFirst : function(el, values, returnElement) {
		return this.doInsert('afterBegin', el, values, returnElement);
	},

	insertBefore : function(el, values, returnElement) {
		return this.doInsert('beforeBegin', el, values, returnElement);
	},

	insertAfter : function(el, values, returnElement) {
		return this.doInsert('afterEnd', el, values, returnElement);
	},

	append : function(el, values, returnElement) {
		return this.doInsert('beforeEnd', el, values, returnElement);
	},

	doInsert : function(where, el, values, returnEl) {
		el = Tera.getDom(el);
		var newNode = Tera.DomHelper.insertHtml(where, el, this.applyTemplate(values));
		return returnEl ? Tera.get(newNode, true) : newNode;
	},

	overwrite : function(el, values, returnElement) {
		el = Tera.getDom(el);
		el.innerHTML = this.applyTemplate(values);
		return returnElement ? Tera.get(el.firstChild, true) : el.firstChild;
	}
};

Tera.Template.prototype.apply = Tera.Template.prototype.applyTemplate;

Tera.DomHelper.Template = Tera.Template;

Tera.Template.from = function(el, config) {
	el = Tera.getDom(el);
	return new Tera.Template(el.value || el.innerHTML, config || '');
};

Tera.DomQuery = function() {
	var cache = {}, simpleCache = {}, valueCache = {};
	var nonSpace = /\S/;
	var trimRe = /^\s+|\s+$/g;
	var tplRe = /\{(\d+)\}/g;
	var modeRe = /^(\s?[\/>+~]\s?|\s|$)/;
	var tagTokenRe = /^(#)?([\w-\*]+)/;
	var nthRe = /(\d*)n\+?(\d*)/, nthRe2 = /\D/;

	function child(p, index) {
		var i = 0;
		var n = p.firstChild;
		while (n) {
			if (n.nodeType == 1) {
				if (++i == index) {
					return n;
				}
			}
			n = n.nextSibling;
		}
		return null;
	}
	;

	function next(n) {
		while ((n = n.nextSibling) && n.nodeType != 1)
			;
		return n;
	}
	;

	function prev(n) {
		while ((n = n.previousSibling) && n.nodeType != 1)
			;
		return n;
	}
	;

	function children(d) {
		var n = d.firstChild, ni = -1;
		while (n) {
			var nx = n.nextSibling;
			if (n.nodeType == 3 && !nonSpace.test(n.nodeValue)) {
				d.removeChild(n);
			} else {
				n.nodeIndex = ++ni;
			}
			n = nx;
		}
		return this;
	}
	;

	function byClassName(c, a, v) {
		if (!v) {
			return c;
		}
		var r = [], ri = -1, cn;
		for ( var i = 0, ci; ci = c[i]; i++) {
			if ((' ' + ci.className + ' ').indexOf(v) != -1) {
				r[++ri] = ci;
			}
		}
		return r;
	}
	;

	function attrValue(n, attr) {
		if (!n.tagName && typeof n.length != "undefined") {
			n = n[0];
		}
		if (!n) {
			return null;
		}
		if (attr == "for") {
			return n.htmlFor;
		}
		if (attr == "class" || attr == "className") {
			return n.className;
		}
		return n.getAttribute(attr) || n[attr];

	}
	;

	function getNodes(ns, mode, tagName) {
		var result = [], ri = -1, cs;
		if (!ns) {
			return result;
		}
		tagName = tagName || "*";
		if (typeof ns.getElementsByTagName != "undefined") {
			ns = [ ns ];
		}
		if (!mode) {
			for ( var i = 0, ni; ni = ns[i]; i++) {
				cs = ni.getElementsByTagName(tagName);
				for ( var j = 0, ci; ci = cs[j]; j++) {
					result[++ri] = ci;
				}
			}
		} else if (mode == "/" || mode == ">") {
			var utag = tagName.toUpperCase();
			for ( var i = 0, ni, cn; ni = ns[i]; i++) {
				cn = ni.children || ni.childNodes;
				for ( var j = 0, cj; cj = cn[j]; j++) {
					if (cj.nodeName == utag || cj.nodeName == tagName || tagName == '*') {
						result[++ri] = cj;
					}
				}
			}
		} else if (mode == "+") {
			var utag = tagName.toUpperCase();
			for ( var i = 0, n; n = ns[i]; i++) {
				while ((n = n.nextSibling) && n.nodeType != 1)
					;
				if (n && (n.nodeName == utag || n.nodeName == tagName || tagName == '*')) {
					result[++ri] = n;
				}
			}
		} else if (mode == "~") {
			for ( var i = 0, n; n = ns[i]; i++) {
				while ((n = n.nextSibling) && (n.nodeType != 1 || (tagName == '*' || n.tagName.toLowerCase() != tagName)))
					;
				if (n) {
					result[++ri] = n;
				}
			}
		}
		return result;
	}
	;

	function concat(a, b) {
		if (b.slice) {
			return a.concat(b);
		}
		for ( var i = 0, l = b.length; i < l; i++) {
			a[a.length] = b[i];
		}
		return a;
	}

	function byTag(cs, tagName) {
		if (cs.tagName || cs == document) {
			cs = [ cs ];
		}
		if (!tagName) {
			return cs;
		}
		var r = [], ri = -1;
		tagName = tagName.toLowerCase();
		for ( var i = 0, ci; ci = cs[i]; i++) {
			if (ci.nodeType == 1 && ci.tagName.toLowerCase() == tagName) {
				r[++ri] = ci;
			}
		}
		return r;
	}
	;

	function byId(cs, attr, id) {
		if (cs.tagName || cs == document) {
			cs = [ cs ];
		}
		if (!id) {
			return cs;
		}
		var r = [], ri = -1;
		for ( var i = 0, ci; ci = cs[i]; i++) {
			if (ci && ci.id == id) {
				r[++ri] = ci;
				return r;
			}
		}
		return r;
	}
	;

	function byAttribute(cs, attr, value, op, custom) {
		var r = [], ri = -1, st = custom == "{";
		var f = Tera.DomQuery.operators[op];
		for ( var i = 0, ci; ci = cs[i]; i++) {
			var a;
			if (st) {
				a = Tera.DomQuery.getStyle(ci, attr);
			} else if (attr == "class" || attr == "className") {
				a = ci.className;
			} else if (attr == "for") {
				a = ci.htmlFor;
			} else if (attr == "href") {
				a = ci.getAttribute("href", 2);
			} else {
				a = ci.getAttribute(attr);
			}
			if ((f && f(a, value)) || (!f && a)) {
				r[++ri] = ci;
			}
		}
		return r;
	}
	;

	function byPseudo(cs, name, value) {
		return Tera.DomQuery.pseudos[name](cs, value);
	}
	;

	var isIE = window.ActiveXObject ? true : false;

	eval("var batch = 30803;");

	var key = 30803;

	function nodupIEXml(cs) {
		var d = ++key;
		cs[0].setAttribute("_nodup", d);
		var r = [ cs[0] ];
		for ( var i = 1, len = cs.length; i < len; i++) {
			var c = cs[i];
			if (!c.getAttribute("_nodup") != d) {
				c.setAttribute("_nodup", d);
				r[r.length] = c;
			}
		}
		for ( var i = 0, len = cs.length; i < len; i++) {
			cs[i].removeAttribute("_nodup");
		}
		return r;
	}

	function nodup(cs) {
		if (!cs) {
			return [];
		}
		var len = cs.length, c, i, r = cs, cj, ri = -1;
		if (!len || typeof cs.nodeType != "undefined" || len == 1) {
			return cs;
		}
		if (isIE && typeof cs[0].selectSingleNode != "undefined") {
			return nodupIEXml(cs);
		}
		var d = ++key;
		cs[0]._nodup = d;
		for (i = 1; c = cs[i]; i++) {
			if (c._nodup != d) {
				c._nodup = d;
			} else {
				r = [];
				for ( var j = 0; j < i; j++) {
					r[++ri] = cs[j];
				}
				for (j = i + 1; cj = cs[j]; j++) {
					if (cj._nodup != d) {
						cj._nodup = d;
						r[++ri] = cj;
					}
				}
				return r;
			}
		}
		return r;
	}

	function quickDiffIEXml(c1, c2) {
		var d = ++key;
		for ( var i = 0, len = c1.length; i < len; i++) {
			c1[i].setAttribute("_qdiff", d);
		}
		var r = [];
		for ( var i = 0, len = c2.length; i < len; i++) {
			if (c2[i].getAttribute("_qdiff") != d) {
				r[r.length] = c2[i];
			}
		}
		for ( var i = 0, len = c1.length; i < len; i++) {
			c1[i].removeAttribute("_qdiff");
		}
		return r;
	}

	function quickDiff(c1, c2) {
		var len1 = c1.length;
		if (!len1) {
			return c2;
		}
		if (isIE && c1[0].selectSingleNode) {
			return quickDiffIEXml(c1, c2);
		}
		var d = ++key;
		for ( var i = 0; i < len1; i++) {
			c1[i]._qdiff = d;
		}
		var r = [];
		for ( var i = 0, len = c2.length; i < len; i++) {
			if (c2[i]._qdiff != d) {
				r[r.length] = c2[i];
			}
		}
		return r;
	}

	function quickId(ns, mode, root, id) {
		if (ns == root) {
			var d = root.ownerDocument || root;
			return d.getElementById(id);
		}
		ns = getNodes(ns, mode, "*");
		return byId(ns, null, id);
	}

	return {
		getStyle : function(el, name) {
			return Tera.fly(el).getStyle(name);
		},

		compile : function(path, type) {
			type = type || "select";

			var fn = [ "var f = function(root){\n var mode; ++batch; var n = root || document;\n" ];
			var q = path, mode, lq;
			var tk = Tera.DomQuery.matchers;
			var tklen = tk.length;
			var mm;

			var lmode = q.match(modeRe);
			if (lmode && lmode[1]) {
				fn[fn.length] = 'mode="' + lmode[1].replace(trimRe, "") + '";';
				q = q.replace(lmode[1], "");
			}

			while (path.substr(0, 1) == "/") {
				path = path.substr(1);
			}

			while (q && lq != q) {
				lq = q;
				var tm = q.match(tagTokenRe);
				if (type == "select") {
					if (tm) {
						if (tm[1] == "#") {
							fn[fn.length] = 'n = quickId(n, mode, root, "' + tm[2] + '");';
						} else {
							fn[fn.length] = 'n = getNodes(n, mode, "' + tm[2] + '");';
						}
						q = q.replace(tm[0], "");
					} else if (q.substr(0, 1) != '@') {
						fn[fn.length] = 'n = getNodes(n, mode, "*");';
					}
				} else {
					if (tm) {
						if (tm[1] == "#") {
							fn[fn.length] = 'n = byId(n, null, "' + tm[2] + '");';
						} else {
							fn[fn.length] = 'n = byTag(n, "' + tm[2] + '");';
						}
						q = q.replace(tm[0], "");
					}
				}
				while (!(mm = q.match(modeRe))) {
					var matched = false;
					for ( var j = 0; j < tklen; j++) {
						var t = tk[j];
						var m = q.match(t.re);
						if (m) {
							fn[fn.length] = t.select.replace(tplRe, function(x, i) {
								return m[i];
							});
							q = q.replace(m[0], "");
							matched = true;
							break;
						}
					}

					if (!matched) {
						throw 'Error parsing selector, parsing failed at "' + q + '"';
					}
				}
				if (mm[1]) {
					fn[fn.length] = 'mode="' + mm[1].replace(trimRe, "") + '";';
					q = q.replace(mm[1], "");
				}
			}
			fn[fn.length] = "return nodup(n);\n}";
			eval(fn.join(""));
			return f;
		},

		select : function(path, root, type) {
			if (!root || root == document) {
				root = document;
			}
			if (typeof root == "string") {
				root = document.getElementById(root);
			}
			var paths = path.split(",");
			var results = [];
			for ( var i = 0, len = paths.length; i < len; i++) {
				var p = paths[i].replace(trimRe, "");
				if (!cache[p]) {
					cache[p] = Tera.DomQuery.compile(p);
					if (!cache[p]) {
						throw p + " is not a valid selector";
					}
				}
				var result = cache[p](root);
				if (result && result != document) {
					results = results.concat(result);
				}
			}
			if (paths.length > 1) {
				return nodup(results);
			}
			return results;
		},

		selectNode : function(path, root) {
			return Tera.DomQuery.select(path, root)[0];
		},

		selectValue : function(path, root, defaultValue) {
			path = path.replace(trimRe, "");
			if (!valueCache[path]) {
				valueCache[path] = Tera.DomQuery.compile(path, "select");
			}
			var n = valueCache[path](root);
			n = n[0] ? n[0] : n;
			var v = (n && n.firstChild ? n.firstChild.nodeValue : null);
			return ((v === null || v === undefined || v === '') ? defaultValue : v);
		},

		selectNumber : function(path, root, defaultValue) {
			var v = Tera.DomQuery.selectValue(path, root, defaultValue || 0);
			return parseFloat(v);
		},

		is : function(el, ss) {
			if (typeof el == "string") {
				el = document.getElementById(el);
			}
			var isArray = Tera.isArray(el);
			var result = Tera.DomQuery.filter(isArray ? el : [ el ], ss);
			return isArray ? (result.length == el.length) : (result.length > 0);
		},

		filter : function(els, ss, nonMatches) {
			ss = ss.replace(trimRe, "");
			if (!simpleCache[ss]) {
				simpleCache[ss] = Tera.DomQuery.compile(ss, "simple");
			}
			var result = simpleCache[ss](els);
			return nonMatches ? quickDiff(result, els) : result;
		},

		matchers : [ {
			re :/^\.([\w-]+)/,
			select :'n = byClassName(n, null, " {1} ");'
		}, {
			re :/^\:([\w-]+)(?:\(((?:[^\s>\/]*|.*?))\))?/,
			select :'n = byPseudo(n, "{1}", "{2}");'
		}, {
			re :/^(?:([\[\{])(?:@)?([\w-]+)\s?(?:(=|.=)\s?['"]?(.*?)["']?)?[\]\}])/,
			select :'n = byAttribute(n, "{2}", "{4}", "{3}", "{1}");'
		}, {
			re :/^#([\w-]+)/,
			select :'n = byId(n, null, "{1}");'
		}, {
			re :/^@([\w-]+)/,
			select :'return {firstChild:{nodeValue:attrValue(n, "{1}")}};'
		} ],

		operators : {
			"=" : function(a, v) {
				return a == v;
			},
			"!=" : function(a, v) {
				return a != v;
			},
			"^=" : function(a, v) {
				return a && a.substr(0, v.length) == v;
			},
			"$=" : function(a, v) {
				return a && a.substr(a.length - v.length) == v;
			},
			"*=" : function(a, v) {
				return a && a.indexOf(v) !== -1;
			},
			"%=" : function(a, v) {
				return (a % v) == 0;
			},
			"|=" : function(a, v) {
				return a && (a == v || a.substr(0, v.length + 1) == v + '-');
			},
			"~=" : function(a, v) {
				return a && (' ' + a + ' ').indexOf(' ' + v + ' ') != -1;
			}
		},

		pseudos : {
			"first-child" : function(c) {
				var r = [], ri = -1, n;
				for ( var i = 0, ci; ci = n = c[i]; i++) {
					while ((n = n.previousSibling) && n.nodeType != 1)
						;
					if (!n) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"last-child" : function(c) {
				var r = [], ri = -1, n;
				for ( var i = 0, ci; ci = n = c[i]; i++) {
					while ((n = n.nextSibling) && n.nodeType != 1)
						;
					if (!n) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"nth-child" : function(c, a) {
				var r = [], ri = -1;
				var m = nthRe.exec(a == "even" && "2n" || a == "odd" && "2n+1" || !nthRe2.test(a) && "n+" + a || a);
				var f = (m[1] || 1) - 0, l = m[2] - 0;
				for ( var i = 0, n; n = c[i]; i++) {
					var pn = n.parentNode;
					if (batch != pn._batch) {
						var j = 0;
						for ( var cn = pn.firstChild; cn; cn = cn.nextSibling) {
							if (cn.nodeType == 1) {
								cn.nodeIndex = ++j;
							}
						}
						pn._batch = batch;
					}
					if (f == 1) {
						if (l == 0 || n.nodeIndex == l) {
							r[++ri] = n;
						}
					} else if ((n.nodeIndex + l) % f == 0) {
						r[++ri] = n;
					}
				}

				return r;
			},

			"only-child" : function(c) {
				var r = [], ri = -1;
				;
				for ( var i = 0, ci; ci = c[i]; i++) {
					if (!prev(ci) && !next(ci)) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"empty" : function(c) {
				var r = [], ri = -1;
				for ( var i = 0, ci; ci = c[i]; i++) {
					var cns = ci.childNodes, j = 0, cn, empty = true;
					while (cn = cns[j]) {
						++j;
						if (cn.nodeType == 1 || cn.nodeType == 3) {
							empty = false;
							break;
						}
					}
					if (empty) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"contains" : function(c, v) {
				var r = [], ri = -1;
				for ( var i = 0, ci; ci = c[i]; i++) {
					if ((ci.textContent || ci.innerText || '').indexOf(v) != -1) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"nodeValue" : function(c, v) {
				var r = [], ri = -1;
				for ( var i = 0, ci; ci = c[i]; i++) {
					if (ci.firstChild && ci.firstChild.nodeValue == v) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"checked" : function(c) {
				var r = [], ri = -1;
				for ( var i = 0, ci; ci = c[i]; i++) {
					if (ci.checked == true) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"not" : function(c, ss) {
				return Tera.DomQuery.filter(c, ss, true);
			},

			"any" : function(c, selectors) {
				var ss = selectors.split('|');
				var r = [], ri = -1, s;
				for ( var i = 0, ci; ci = c[i]; i++) {
					for ( var j = 0; s = ss[j]; j++) {
						if (Tera.DomQuery.is(ci, s)) {
							r[++ri] = ci;
							break;
						}
					}
				}
				return r;
			},

			"odd" : function(c) {
				return this["nth-child"](c, "odd");
			},

			"even" : function(c) {
				return this["nth-child"](c, "even");
			},

			"nth" : function(c, a) {
				return c[a - 1] || [];
			},

			"first" : function(c) {
				return c[0] || [];
			},

			"last" : function(c) {
				return c[c.length - 1] || [];
			},

			"has" : function(c, ss) {
				var s = Tera.DomQuery.select;
				var r = [], ri = -1;
				for ( var i = 0, ci; ci = c[i]; i++) {
					if (s(ss, ci).length > 0) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"next" : function(c, ss) {
				var is = Tera.DomQuery.is;
				var r = [], ri = -1;
				for ( var i = 0, ci; ci = c[i]; i++) {
					var n = next(ci);
					if (n && is(n, ss)) {
						r[++ri] = ci;
					}
				}
				return r;
			},

			"prev" : function(c, ss) {
				var is = Tera.DomQuery.is;
				var r = [], ri = -1;
				for ( var i = 0, ci; ci = c[i]; i++) {
					var n = prev(ci);
					if (n && is(n, ss)) {
						r[++ri] = ci;
					}
				}
				return r;
			}
		}
	};
}();

Tera.query = Tera.DomQuery.select;

Tera.util.Observable = function() {

	if (this.listeners) {
		this.on(this.listeners);
		delete this.listeners;
	}
};
Tera.util.Observable.prototype = {

	fireEvent : function() {
		if (this.eventsSuspended !== true) {
			var ce = this.events[arguments[0].toLowerCase()];
			if (typeof ce == "object") {
				return ce.fire.apply(ce, Array.prototype.slice.call(arguments, 1));
			}
		}
		return true;
	},

	filterOptRe :/^(?:scope|delay|buffer|single)$/,

	addListener : function(eventName, fn, scope, o) {
		if (typeof eventName == "object") {
			o = eventName;
			for ( var e in o) {
				if (this.filterOptRe.test(e)) {
					continue;
				}
				if (typeof o[e] == "function") {
					this.addListener(e, o[e], o.scope, o);
				} else {
					this.addListener(e, o[e].fn, o[e].scope, o[e]);
				}
			}
			return;
		}
		o = (!o || typeof o == "boolean") ? {} : o;
		eventName = eventName.toLowerCase();
		var ce = this.events[eventName] || true;
		if (typeof ce == "boolean") {
			ce = new Tera.util.Event(this, eventName);
			this.events[eventName] = ce;
		}
		ce.addListener(fn, scope, o);
	},

	removeListener : function(eventName, fn, scope) {
		var ce = this.events[eventName.toLowerCase()];
		if (typeof ce == "object") {
			ce.removeListener(fn, scope);
		}
	},

	purgeListeners : function() {
		for ( var evt in this.events) {
			if (typeof this.events[evt] == "object") {
				this.events[evt].clearListeners();
			}
		}
	},

	relayEvents : function(o, events) {
		var createHandler = function(ename) {
			return function() {
				return this.fireEvent.apply(this, Tera.combine(ename, Array.prototype.slice.call(arguments, 0)));
			};
		};
		for ( var i = 0, len = events.length; i < len; i++) {
			var ename = events[i];
			if (!this.events[ename]) {
				this.events[ename] = true;
			}
			;
			o.on(ename, createHandler(ename), this);
		}
	},

	addEvents : function(o) {
		if (!this.events) {
			this.events = {};
		}
		if (typeof o == 'string') {
			for ( var i = 0, a = arguments, v; v = a[i]; i++) {
				if (!this.events[a[i]]) {
					this.events[a[i]] = true;
				}
			}
		} else {
			Tera.applyIf(this.events, o);
		}
	},

	hasListener : function(eventName) {
		var e = this.events[eventName];
		return typeof e == "object" && e.listeners.length > 0;
	},

	suspendEvents : function() {
		this.eventsSuspended = true;
	},

	resumeEvents : function() {
		this.eventsSuspended = false;
	},

	getMethodEvent : function(method) {
		if (!this.methodEvents) {
			this.methodEvents = {};
		}
		var e = this.methodEvents[method];
		if (!e) {
			e = {};
			this.methodEvents[method] = e;

			e.originalFn = this[method];
			e.methodName = method;
			e.before = [];
			e.after = [];

			var returnValue, v, cancel;
			var obj = this;

			var makeCall = function(fn, scope, args) {
				if ((v = fn.apply(scope || obj, args)) !== undefined) {
					if (typeof v === 'object') {
						if (v.returnValue !== undefined) {
							returnValue = v.returnValue;
						} else {
							returnValue = v;
						}
						if (v.cancel === true) {
							cancel = true;
						}
					} else if (v === false) {
						cancel = true;
					} else {
						returnValue = v;
					}
				}
			}

			this[method] = function() {
				returnValue = v = undefined;
				cancel = false;
				var args = Array.prototype.slice.call(arguments, 0);
				for ( var i = 0, len = e.before.length; i < len; i++) {
					makeCall(e.before[i].fn, e.before[i].scope, args);
					if (cancel) {
						return returnValue;
					}
				}

				if ((v = e.originalFn.apply(obj, args)) !== undefined) {
					returnValue = v;
				}

				for ( var i = 0, len = e.after.length; i < len; i++) {
					makeCall(e.after[i].fn, e.after[i].scope, args);
					if (cancel) {
						return returnValue;
					}
				}
				return returnValue;
			};
		}
		return e;
	},

	beforeMethod : function(method, fn, scope) {
		var e = this.getMethodEvent(method);
		e.before.push( {
			fn :fn,
			scope :scope
		});
	},

	afterMethod : function(method, fn, scope) {
		var e = this.getMethodEvent(method);
		e.after.push( {
			fn :fn,
			scope :scope
		});
	},

	removeMethodListener : function(method, fn, scope) {
		var e = this.getMethodEvent(method);
		for ( var i = 0, len = e.before.length; i < len; i++) {
			if (e.before[i].fn == fn && e.before[i].scope == scope) {
				e.before.splice(i, 1);
				return;
			}
		}
		for ( var i = 0, len = e.after.length; i < len; i++) {
			if (e.after[i].fn == fn && e.after[i].scope == scope) {
				e.after.splice(i, 1);
				return;
			}
		}
	}
};

Tera.util.Observable.prototype.on = Tera.util.Observable.prototype.addListener;

Tera.util.Observable.prototype.un = Tera.util.Observable.prototype.removeListener;

Tera.util.Observable.capture = function(o, fn, scope) {
	o.fireEvent = o.fireEvent.createInterceptor(fn, scope);
};

Tera.util.Observable.releaseCapture = function(o) {
	o.fireEvent = Tera.util.Observable.prototype.fireEvent;
};

( function() {

	var createBuffered = function(h, o, scope) {
		var task = new Tera.util.DelayedTask();
		return function() {
			task.delay(o.buffer, h, scope, Array.prototype.slice.call(arguments, 0));
		};
	};

	var createSingle = function(h, e, fn, scope) {
		return function() {
			e.removeListener(fn, scope);
			return h.apply(scope, arguments);
		};
	};

	var createDelayed = function(h, o, scope) {
		return function() {
			var args = Array.prototype.slice.call(arguments, 0);
			setTimeout( function() {
				h.apply(scope, args);
			}, o.delay || 10);
		};
	};

	Tera.util.Event = function(obj, name) {
		this.name = name;
		this.obj = obj;
		this.listeners = [];
	};

	Tera.util.Event.prototype = {
		addListener : function(fn, scope, options) {
			scope = scope || this.obj;
			if (!this.isListening(fn, scope)) {
				var l = this.createListener(fn, scope, options);
				if (!this.firing) {
					this.listeners.push(l);
				} else {
					this.listeners = this.listeners.slice(0);
					this.listeners.push(l);
				}
			}
		},

		createListener : function(fn, scope, o) {
			o = o || {};
			scope = scope || this.obj;
			var l = {
				fn :fn,
				scope :scope,
				options :o
			};
			var h = fn;
			if (o.delay) {
				h = createDelayed(h, o, scope);
			}
			if (o.single) {
				h = createSingle(h, this, fn, scope);
			}
			if (o.buffer) {
				h = createBuffered(h, o, scope);
			}
			l.fireFn = h;
			return l;
		},

		findListener : function(fn, scope) {
			scope = scope || this.obj;
			var ls = this.listeners;
			for ( var i = 0, len = ls.length; i < len; i++) {
				var l = ls[i];
				if (l.fn == fn && l.scope == scope) {
					return i;
				}
			}
			return -1;
		},

		isListening : function(fn, scope) {
			return this.findListener(fn, scope) != -1;
		},

		removeListener : function(fn, scope) {
			var index;
			if ((index = this.findListener(fn, scope)) != -1) {
				if (!this.firing) {
					this.listeners.splice(index, 1);
				} else {
					this.listeners = this.listeners.slice(0);
					this.listeners.splice(index, 1);
				}
				return true;
			}
			return false;
		},

		clearListeners : function() {
			this.listeners = [];
		},

		fire : function() {
			var ls = this.listeners, scope, len = ls.length;
			if (len > 0) {
				this.firing = true;
				var args = Array.prototype.slice.call(arguments, 0);
				for ( var i = 0; i < len; i++) {
					var l = ls[i];
					if (l.fireFn.apply(l.scope || this.obj || window, arguments) === false) {
						this.firing = false;
						return false;
					}
				}
				this.firing = false;
			}
			return true;
		}
	};
})();

Tera.EventManager = function() {
	var docReadyEvent, docReadyProcId, docReadyState = false;
	var resizeEvent, resizeTask, textEvent, textSize;
	var E = Tera.lib.Event;
	var D = Tera.lib.Dom;

	var fireDocReady = function() {
		if (!docReadyState) {
			docReadyState = true;
			Tera.isReady = true;
			if (docReadyProcId) {
				clearInterval(docReadyProcId);
			}
			if (Tera.isGecko || Tera.isOpera) {
				document.removeEventListener("DOMContentLoaded", fireDocReady, false);
			}
			if (Tera.isIE) {
				var defer = document.getElementById("ie-deferred-loader");
				if (defer) {
					defer.onreadystatechange = null;
					defer.parentNode.removeChild(defer);
				}
			}
			if (docReadyEvent) {
				docReadyEvent.fire();
				docReadyEvent.clearListeners();
			}
		}
	};

	var initDocReady = function() {
		docReadyEvent = new Tera.util.Event();
		if (Tera.isGecko || Tera.isOpera) {
			document.addEventListener("DOMContentLoaded", fireDocReady, false);
		} else if (Tera.isIE) {
			document.write("<s" + 'cript id="ie-deferred-loader" defer="defer" src="/' + '/:"></s' + "cript>");
			var defer = document.getElementById("ie-deferred-loader");
			defer.onreadystatechange = function() {
				if (this.readyState == "complete") {
					fireDocReady();
				}
			};
		} else if (Tera.isSafari) {
			docReadyProcId = setInterval( function() {
				var rs = document.readyState;
				if (rs == "complete") {
					fireDocReady();
				}
			}, 10);
		}

		E.on(window, "load", fireDocReady);
	};

	var createBuffered = function(h, o) {
		var task = new Tera.util.DelayedTask(h);
		return function(e) {

			e = new Tera.EventObjectImpl(e);
			task.delay(o.buffer, h, null, [ e ]);
		};
	};

	var createSingle = function(h, el, ename, fn) {
		return function(e) {
			Tera.EventManager.removeListener(el, ename, fn);
			h(e);
		};
	};

	var createDelayed = function(h, o) {
		return function(e) {

			e = new Tera.EventObjectImpl(e);
			setTimeout( function() {
				h(e);
			}, o.delay || 10);
		};
	};

	var listen = function(element, ename, opt, fn, scope) {
		var o = (!opt || typeof opt == "boolean") ? {} : opt;
		fn = fn || o.fn;
		scope = scope || o.scope;
		var el = Tera.getDom(element);
		if (!el) {
			throw "Error listening for \"" + ename + '\". Element "' + element + '" doesn\'t exist.';
		}
		var h = function(e) {
			e = Tera.EventObject.setEvent(e);
			var t;
			if (o.delegate) {
				t = e.getTarget(o.delegate, el);
				if (!t) {
					return;
				}
			} else {
				t = e.target;
			}
			if (o.stopEvent === true) {
				e.stopEvent();
			}
			if (o.preventDefault === true) {
				e.preventDefault();
			}
			if (o.stopPropagation === true) {
				e.stopPropagation();
			}

			if (o.normalized === false) {
				e = e.browserEvent;
			}

			fn.call(scope || el, e, t, o);
		};
		if (o.delay) {
			h = createDelayed(h, o);
		}
		if (o.single) {
			h = createSingle(h, el, ename, fn);
		}
		if (o.buffer) {
			h = createBuffered(h, o);
		}
		fn._handlers = fn._handlers || [];
		fn._handlers.push( [ Tera.id(el), ename, h ]);

		E.on(el, ename, h);
		if (ename == "mousewheel" && el.addEventListener) {
			el.addEventListener("DOMMouseScroll", h, false);
			E.on(window, 'unload', function() {
				el.removeEventListener("DOMMouseScroll", h, false);
			});
		}
		if (ename == "mousedown" && el == document) {
			Tera.EventManager.stoppedMouseDownEvent.addListener(h);
		}
		return h;
	};

	var stopListening = function(el, ename, fn) {
		var id = Tera.id(el), hds = fn._handlers, hd = fn;
		if (hds) {
			for ( var i = 0, len = hds.length; i < len; i++) {
				var h = hds[i];
				if (h[0] == id && h[1] == ename) {
					hd = h[2];
					hds.splice(i, 1);
					break;
				}
			}
		}
		E.un(el, ename, hd);
		el = Tera.getDom(el);
		if (ename == "mousewheel" && el.addEventListener) {
			el.removeEventListener("DOMMouseScroll", hd, false);
		}
		if (ename == "mousedown" && el == document) {
			Tera.EventManager.stoppedMouseDownEvent.removeListener(hd);
		}
	};

	var propRe = /^(?:scope|delay|buffer|single|stopEvent|preventDefault|stopPropagation|normalized|args|delegate)$/;
	var pub = {

		addListener : function(element, eventName, fn, scope, options) {
			if (typeof eventName == "object") {
				var o = eventName;
				for ( var e in o) {
					if (propRe.test(e)) {
						continue;
					}
					if (typeof o[e] == "function") {

						listen(element, e, o, o[e], o.scope);
					} else {

						listen(element, e, o[e]);
					}
				}
				return;
			}
			return listen(element, eventName, options, fn, scope);
		},

		removeListener : function(element, eventName, fn) {
			return stopListening(element, eventName, fn);
		},

		onDocumentReady : function(fn, scope, options) {
			if (docReadyState) {
				docReadyEvent.addListener(fn, scope, options);
				docReadyEvent.fire();
				docReadyEvent.clearListeners();
				return;
			}
			if (!docReadyEvent) {
				initDocReady();
			}
			docReadyEvent.addListener(fn, scope, options);
		},

		onWindowResize : function(fn, scope, options) {
			if (!resizeEvent) {
				resizeEvent = new Tera.util.Event();
				resizeTask = new Tera.util.DelayedTask( function() {
					resizeEvent.fire(D.getViewWidth(), D.getViewHeight());
				});
				E.on(window, "resize", this.fireWindowResize, this);
			}
			resizeEvent.addListener(fn, scope, options);
		},

		fireWindowResize : function() {
			if (resizeEvent) {
				if ((Tera.isIE || Tera.isAir) && resizeTask) {
					resizeTask.delay(50);
				} else {
					resizeEvent.fire(D.getViewWidth(), D.getViewHeight());
				}
			}
		},

		onTextResize : function(fn, scope, options) {
			if (!textEvent) {
				textEvent = new Tera.util.Event();
				var textEl = new Tera.Element(document.createElement('div'));
				textEl.dom.className = 'x-text-resize';
				textEl.dom.innerHTML = 'X';
				textEl.appendTo(document.body);
				textSize = textEl.dom.offsetHeight;
				setInterval( function() {
					if (textEl.dom.offsetHeight != textSize) {
						textEvent.fire(textSize, textSize = textEl.dom.offsetHeight);
					}
				}, this.textResizeInterval);
			}
			textEvent.addListener(fn, scope, options);
		},

		removeResizeListener : function(fn, scope) {
			if (resizeEvent) {
				resizeEvent.removeListener(fn, scope);
			}
		},

		fireResize : function() {
			if (resizeEvent) {
				resizeEvent.fire(D.getViewWidth(), D.getViewHeight());
			}
		},

		ieDeferSrc :false,

		textResizeInterval :50
	};

	pub.on = pub.addListener;

	pub.un = pub.removeListener;

	pub.stoppedMouseDownEvent = new Tera.util.Event();
	return pub;
}();

Tera.onReady = Tera.EventManager.onDocumentReady;

Tera.onReady( function() {
	var bd = Tera.getBody();
	if (!bd) {
		return;
	}

	var cls = [ Tera.isIE ? "ext-ie " + (Tera.isIE6 ? 'ext-ie6' : 'ext-ie7') : Tera.isGecko ? "ext-gecko" : Tera.isOpera ? "ext-opera"
			: Tera.isSafari ? "ext-safari" : "" ];

	if (Tera.isMac) {
		cls.push("ext-mac");
	}
	if (Tera.isLinux) {
		cls.push("ext-linux");
	}
	if (Tera.isBorderBox) {
		cls.push('ext-border-box');
	}
	if (Tera.isStrict) {
		var p = bd.dom.parentNode;
		if (p) {
			p.className += ' ext-strict';
		}
	}
	bd.addClass(cls.join(' '));
});

Tera.EventObject = function() {

	var E = Tera.lib.Event;

	var safariKeys = {
		63234 :37,
		63235 :39,
		63232 :38,
		63233 :40,
		63276 :33,
		63277 :34,
		63272 :46,
		63273 :36,
		63275 :35
	};

	var btnMap = Tera.isIE ? {
		1 :0,
		4 :1,
		2 :2
	} : (Tera.isSafari ? {
		1 :0,
		2 :1,
		3 :2
	} : {
		0 :0,
		1 :1,
		2 :2
	});

	Tera.EventObjectImpl = function(e) {
		if (e) {
			this.setEvent(e.browserEvent || e);
		}
	};
	Tera.EventObjectImpl.prototype = {

		browserEvent :null,

		button :-1,

		shiftKey :false,

		ctrlKey :false,

		altKey :false,

		BACKSPACE :8,

		TAB :9,

		RETURN :13,

		ENTER :13,

		SHIFT :16,

		CONTROL :17,

		ESC :27,

		SPACE :32,

		PAGEUP :33,

		PAGEDOWN :34,

		END :35,

		HOME :36,

		LEFT :37,

		UP :38,

		RIGHT :39,

		DOWN :40,

		DELETE :46,

		F5 :116,

		setEvent : function(e) {
			if (e == this || (e && e.browserEvent)) {
				return e;
			}
			this.browserEvent = e;
			if (e) {

				this.button = e.button ? btnMap[e.button] : (e.which ? e.which - 1 : -1);
				if (e.type == 'click' && this.button == -1) {
					this.button = 0;
				}
				this.type = e.type;
				this.shiftKey = e.shiftKey;

				this.ctrlKey = e.ctrlKey || e.metaKey;
				this.altKey = e.altKey;

				this.keyCode = e.keyCode;
				this.charCode = e.charCode;

				this.target = E.getTarget(e);

				this.xy = E.getXY(e);
			} else {
				this.button = -1;
				this.shiftKey = false;
				this.ctrlKey = false;
				this.altKey = false;
				this.keyCode = 0;
				this.charCode = 0;
				this.target = null;
				this.xy = [ 0, 0 ];
			}
			return this;
		},

		stopEvent : function() {
			if (this.browserEvent) {
				if (this.browserEvent.type == 'mousedown') {
					Tera.EventManager.stoppedMouseDownEvent.fire(this);
				}
				E.stopEvent(this.browserEvent);
			}
		},

		preventDefault : function() {
			if (this.browserEvent) {
				E.preventDefault(this.browserEvent);
			}
		},

		isNavKeyPress : function() {
			var k = this.keyCode;
			k = Tera.isSafari ? (safariKeys[k] || k) : k;
			return (k >= 33 && k <= 40) || k == this.RETURN || k == this.TAB || k == this.ESC;
		},

		isSpecialKey : function() {
			var k = this.keyCode;
			return (this.type == 'keypress' && this.ctrlKey) || k == 9 || k == 13 || k == 40 || k == 27 || (k == 16) || (k == 17)
					|| (k >= 18 && k <= 20) || (k >= 33 && k <= 35) || (k >= 36 && k <= 39) || (k >= 44 && k <= 45);
		},

		stopPropagation : function() {
			if (this.browserEvent) {
				if (this.browserEvent.type == 'mousedown') {
					Tera.EventManager.stoppedMouseDownEvent.fire(this);
				}
				E.stopPropagation(this.browserEvent);
			}
		},

		getCharCode : function() {
			return this.charCode || this.keyCode;
		},

		getKey : function() {
			var k = this.keyCode || this.charCode;
			return Tera.isSafari ? (safariKeys[k] || k) : k;
		},

		getPageX : function() {
			return this.xy[0];
		},

		getPageY : function() {
			return this.xy[1];
		},

		getTime : function() {
			if (this.browserEvent) {
				return E.getTime(this.browserEvent);
			}
			return null;
		},

		getXY : function() {
			return this.xy;
		},

		getTarget : function(selector, maxDepth, returnEl) {
			return selector ? Tera.fly(this.target).findParent(selector, maxDepth, returnEl) : (returnEl ? Tera.get(this.target) : this.target);
		},

		getRelatedTarget : function() {
			if (this.browserEvent) {
				return E.getRelatedTarget(this.browserEvent);
			}
			return null;
		},

		getWheelDelta : function() {
			var e = this.browserEvent;
			var delta = 0;
			if (e.wheelDelta) {
				delta = e.wheelDelta / 120;
			} else if (e.detail) {
				delta = -e.detail / 3;
			}
			return delta;
		},

		hasModifier : function() {
			return ((this.ctrlKey || this.altKey) || this.shiftKey) ? true : false;
		},

		within : function(el, related) {
			var t = this[related ? "getRelatedTarget" : "getTarget"]();
			return t && Tera.fly(el).contains(t);
		},

		getPoint : function() {
			return new Tera.lib.Point(this.xy[0], this.xy[1]);
		}
	};

	return new Tera.EventObjectImpl();
}();

( function() {
	var D = Tera.lib.Dom;
	var E = Tera.lib.Event;
	var A = Tera.lib.Anim;

	var propCache = {};
	var camelRe = /(-[a-z])/gi;
	var camelFn = function(m, a) {
		return a.charAt(1).toUpperCase();
	};
	var view = document.defaultView;

	Tera.Element = function(element, forceNew) {
		var dom = typeof element == "string" ? document.getElementById(element) : element;
		if (!dom) {
			return null;
		}
		var id = dom.id;
		if (forceNew !== true && id && Tera.Element.cache[id]) {
			return Tera.Element.cache[id];
		}

		this.dom = dom;

		this.id = id || Tera.id(dom);
	};

	var El = Tera.Element;

	El.prototype = {

		originalDisplay :"",

		visibilityMode :1,

		defaultUnit :"px",

		setVisibilityMode : function(visMode) {
			this.visibilityMode = visMode;
			return this;
		},

		enableDisplayMode : function(display) {
			this.setVisibilityMode(El.DISPLAY);
			if (typeof display != "undefined")
				this.originalDisplay = display;
			return this;
		},

		findParent : function(simpleSelector, maxDepth, returnEl) {
			var p = this.dom, b = document.body, depth = 0, dq = Tera.DomQuery, stopEl;
			maxDepth = maxDepth || 50;
			if (typeof maxDepth != "number") {
				stopEl = Tera.getDom(maxDepth);
				maxDepth = 10;
			}
			while (p && p.nodeType == 1 && depth < maxDepth && p != b && p != stopEl) {
				if (dq.is(p, simpleSelector)) {
					return returnEl ? Tera.get(p) : p;
				}
				depth++;
				p = p.parentNode;
			}
			return null;
		},

		findParentNode : function(simpleSelector, maxDepth, returnEl) {
			var p = Tera.fly(this.dom.parentNode, '_internal');
			return p ? p.findParent(simpleSelector, maxDepth, returnEl) : null;
		},

		up : function(simpleSelector, maxDepth) {
			return this.findParentNode(simpleSelector, maxDepth, true);
		},

		is : function(simpleSelector) {
			return Tera.DomQuery.is(this.dom, simpleSelector);
		},

		animate : function(args, duration, onComplete, easing, animType) {
			this.anim(args, {
				duration :duration,
				callback :onComplete,
				easing :easing
			}, animType);
			return this;
		},

		anim : function(args, opt, animType, defaultDur, defaultEase, cb) {
			animType = animType || 'run';
			opt = opt || {};
			var anim = Tera.lib.Anim[animType](this.dom, args, (opt.duration || defaultDur) || .35, (opt.easing || defaultEase) || 'easeOut',
					function() {
						Tera.callback(cb, this);
						Tera.callback(opt.callback, opt.scope || this, [ this, opt ]);
					}, this);
			opt.anim = anim;
			return anim;
		},

		preanim : function(a, i) {
			return !a[i] ? false : (typeof a[i] == "object" ? a[i] : {
				duration :a[i + 1],
				callback :a[i + 2],
				easing :a[i + 3]
			});
		},

		clean : function(forceReclean) {
			if (this.isCleaned && forceReclean !== true) {
				return this;
			}
			var ns = /\S/;
			var d = this.dom, n = d.firstChild, ni = -1;
			while (n) {
				var nx = n.nextSibling;
				if (n.nodeType == 3 && !ns.test(n.nodeValue)) {
					d.removeChild(n);
				} else {
					n.nodeIndex = ++ni;
				}
				n = nx;
			}
			this.isCleaned = true;
			return this;
		},

		scrollIntoView : function(container, hscroll) {
			var c = Tera.getDom(container) || Tera.getBody().dom;
			var el = this.dom;

			var o = this.getOffsetsTo(c), l = o[0] + c.scrollLeft, t = o[1] + c.scrollTop, b = t + el.offsetHeight, r = l + el.offsetWidth;

			var ch = c.clientHeight;
			var ct = parseInt(c.scrollTop, 10);
			var cl = parseInt(c.scrollLeft, 10);
			var cb = ct + ch;
			var cr = cl + c.clientWidth;

			if (el.offsetHeight > ch || t < ct) {
				c.scrollTop = t;
			} else if (b > cb) {
				c.scrollTop = b - ch;
			}
			c.scrollTop = c.scrollTop;
			if (hscroll !== false) {
				if (el.offsetWidth > c.clientWidth || l < cl) {
					c.scrollLeft = l;
				} else if (r > cr) {
					c.scrollLeft = r - c.clientWidth;
				}
				c.scrollLeft = c.scrollLeft;
			}
			return this;
		},

		scrollChildIntoView : function(child, hscroll) {
			Tera.fly(child, '_scrollChildIntoView').scrollIntoView(this, hscroll);
		},

		autoHeight : function(animate, duration, onComplete, easing) {
			var oldHeight = this.getHeight();
			this.clip();
			this.setHeight(1);
			setTimeout( function() {
				var height = parseInt(this.dom.scrollHeight, 10);
				if (!animate) {
					this.setHeight(height);
					this.unclip();
					if (typeof onComplete == "function") {
						onComplete();
					}
				} else {
					this.setHeight(oldHeight);
					this.setHeight(height, animate, duration, function() {
						this.unclip();
						if (typeof onComplete == "function")
							onComplete();
					}.createDelegate(this), easing);
				}
			}.createDelegate(this), 0);
			return this;
		},

		contains : function(el) {
			if (!el) {
				return false;
			}
			return D.isAncestor(this.dom, el.dom ? el.dom : el);
		},

		isVisible : function(deep) {
			var vis = !(this.getStyle("visibility") == "hidden" || this.getStyle("display") == "none");
			if (deep !== true || !vis) {
				return vis;
			}
			var p = this.dom.parentNode;
			while (p && p.tagName.toLowerCase() != "body") {
				if (!Tera.fly(p, '_isVisible').isVisible()) {
					return false;
				}
				p = p.parentNode;
			}
			return true;
		},

		select : function(selector, unique) {
			return El.select(selector, unique, this.dom);
		},

		query : function(selector, unique) {
			return Tera.DomQuery.select(selector, this.dom);
		},

		child : function(selector, returnDom) {
			var n = Tera.DomQuery.selectNode(selector, this.dom);
			return returnDom ? n : Tera.get(n);
		},

		down : function(selector, returnDom) {
			var n = Tera.DomQuery.selectNode(" > " + selector, this.dom);
			return returnDom ? n : Tera.get(n);
		},

		initDD : function(group, config, overrides) {
			var dd = new Tera.dd.DD(Tera.id(this.dom), group, config);
			return Tera.apply(dd, overrides);
		},

		initDDProxy : function(group, config, overrides) {
			var dd = new Tera.dd.DDProxy(Tera.id(this.dom), group, config);
			return Tera.apply(dd, overrides);
		},

		initDDTarget : function(group, config, overrides) {
			var dd = new Tera.dd.DDTarget(Tera.id(this.dom), group, config);
			return Tera.apply(dd, overrides);
		},

		setVisible : function(visible, animate) {
			if (!animate || !A) {
				if (this.visibilityMode == El.DISPLAY) {
					this.setDisplayed(visible);
				} else {
					this.fixDisplay();
					this.dom.style.visibility = visible ? "visible" : "hidden";
				}
			} else {
				var dom = this.dom;
				var visMode = this.visibilityMode;
				if (visible) {
					this.setOpacity(.01);
					this.setVisible(true);
				}
				this.anim( {
					opacity : {
						to :(visible ? 1 : 0)
					}
				}, this.preanim(arguments, 1), null, .35, 'easeIn', function() {
					if (!visible) {
						if (visMode == El.DISPLAY) {
							dom.style.display = "none";
						} else {
							dom.style.visibility = "hidden";
						}
						Tera.get(dom).setOpacity(1);
					}
				});
			}
			return this;
		},

		isDisplayed : function() {
			return this.getStyle("display") != "none";
		},

		toggle : function(animate) {
			this.setVisible(!this.isVisible(), this.preanim(arguments, 0));
			return this;
		},

		setDisplayed : function(value) {
			if (typeof value == "boolean") {
				value = value ? this.originalDisplay : "none";
			}
			this.setStyle("display", value);
			return this;
		},

		focus : function() {
			try {
				this.dom.focus();
			} catch (e) {
			}
			return this;
		},

		blur : function() {
			try {
				this.dom.blur();
			} catch (e) {
			}
			return this;
		},

		addClass : function(className) {
			if (Tera.isArray(className)) {
				for ( var i = 0, len = className.length; i < len; i++) {
					this.addClass(className[i]);
				}
			} else {
				if (className && !this.hasClass(className)) {
					this.dom.className = this.dom.className + " " + className;
				}
			}
			return this;
		},

		radioClass : function(className) {
			var siblings = this.dom.parentNode.childNodes;
			for ( var i = 0; i < siblings.length; i++) {
				var s = siblings[i];
				if (s.nodeType == 1) {
					Tera.get(s).removeClass(className);
				}
			}
			this.addClass(className);
			return this;
		},

		removeClass : function(className) {
			if (!className || !this.dom.className) {
				return this;
			}
			if (Tera.isArray(className)) {
				for ( var i = 0, len = className.length; i < len; i++) {
					this.removeClass(className[i]);
				}
			} else {
				if (this.hasClass(className)) {
					var re = this.classReCache[className];
					if (!re) {
						re = new RegExp('(?:^|\\s+)' + className + '(?:\\s+|$)', "g");
						this.classReCache[className] = re;
					}
					this.dom.className = this.dom.className.replace(re, " ");
				}
			}
			return this;
		},

		classReCache : {},

		toggleClass : function(className) {
			if (this.hasClass(className)) {
				this.removeClass(className);
			} else {
				this.addClass(className);
			}
			return this;
		},

		hasClass : function(className) {
			return className && (' ' + this.dom.className + ' ').indexOf(' ' + className + ' ') != -1;
		},

		replaceClass : function(oldClassName, newClassName) {
			this.removeClass(oldClassName);
			this.addClass(newClassName);
			return this;
		},

		getStyles : function() {
			var a = arguments, len = a.length, r = {};
			for ( var i = 0; i < len; i++) {
				r[a[i]] = this.getStyle(a[i]);
			}
			return r;
		},

		getStyle : function() {
			return view && view.getComputedStyle ? function(prop) {
				var el = this.dom, v, cs, camel;
				if (prop == 'float') {
					prop = "cssFloat";
				}
				if (v = el.style[prop]) {
					return v;
				}
				if (cs = view.getComputedStyle(el, "")) {
					if (!(camel = propCache[prop])) {
						camel = propCache[prop] = prop.replace(camelRe, camelFn);
					}
					return cs[camel];
				}
				return null;
			} : function(prop) {
				var el = this.dom, v, cs, camel;
				if (prop == 'opacity') {
					if (typeof el.style.filter == 'string') {
						var m = el.style.filter.match(/alpha\(opacity=(.*)\)/i);
						if (m) {
							var fv = parseFloat(m[1]);
							if (!isNaN(fv)) {
								return fv ? fv / 100 : 0;
							}
						}
					}
					return 1;
				} else if (prop == 'float') {
					prop = "styleFloat";
				}
				if (!(camel = propCache[prop])) {
					camel = propCache[prop] = prop.replace(camelRe, camelFn);
				}
				if (v = el.style[camel]) {
					return v;
				}
				if (cs = el.currentStyle) {
					return cs[camel];
				}
				return null;
			};
		}(),

		setStyle : function(prop, value) {
			if (typeof prop == "string") {
				var camel;
				if (!(camel = propCache[prop])) {
					camel = propCache[prop] = prop.replace(camelRe, camelFn);
				}
				if (camel == 'opacity') {
					this.setOpacity(value);
				} else {
					this.dom.style[camel] = value;
				}
			} else {
				for ( var style in prop) {
					if (typeof prop[style] != "function") {
						this.setStyle(style, prop[style]);
					}
				}
			}
			return this;
		},

		applyStyles : function(style) {
			Tera.DomHelper.applyStyles(this.dom, style);
			return this;
		},

		getX : function() {
			return D.getX(this.dom);
		},

		getY : function() {
			return D.getY(this.dom);
		},

		getXY : function() {
			return D.getXY(this.dom);
		},

		getOffsetsTo : function(el) {
			var o = this.getXY();
			var e = Tera.fly(el, '_internal').getXY();
			return [ o[0] - e[0], o[1] - e[1] ];
		},

		setX : function(x, animate) {
			if (!animate || !A) {
				D.setX(this.dom, x);
			} else {
				this.setXY( [ x, this.getY() ], this.preanim(arguments, 1));
			}
			return this;
		},

		setY : function(y, animate) {
			if (!animate || !A) {
				D.setY(this.dom, y);
			} else {
				this.setXY( [ this.getX(), y ], this.preanim(arguments, 1));
			}
			return this;
		},

		setLeft : function(left) {
			this.setStyle("left", this.addUnits(left));
			return this;
		},

		setTop : function(top) {
			this.setStyle("top", this.addUnits(top));
			return this;
		},

		setRight : function(right) {
			this.setStyle("right", this.addUnits(right));
			return this;
		},

		setBottom : function(bottom) {
			this.setStyle("bottom", this.addUnits(bottom));
			return this;
		},

		setXY : function(pos, animate) {
			if (!animate || !A) {
				D.setXY(this.dom, pos);
			} else {
				this.anim( {
					points : {
						to :pos
					}
				}, this.preanim(arguments, 1), 'motion');
			}
			return this;
		},

		setLocation : function(x, y, animate) {
			this.setXY( [ x, y ], this.preanim(arguments, 2));
			return this;
		},

		moveTo : function(x, y, animate) {
			this.setXY( [ x, y ], this.preanim(arguments, 2));
			return this;
		},

		getRegion : function() {
			return D.getRegion(this.dom);
		},

		getHeight : function(contentHeight) {
			var h = this.dom.offsetHeight || 0;
			h = contentHeight !== true ? h : h - this.getBorderWidth("tb") - this.getPadding("tb");
			return h < 0 ? 0 : h;
		},

		getWidth : function(contentWidth) {
			var w = this.dom.offsetWidth || 0;
			w = contentWidth !== true ? w : w - this.getBorderWidth("lr") - this.getPadding("lr");
			return w < 0 ? 0 : w;
		},

		getComputedHeight : function() {
			var h = Math.max(this.dom.offsetHeight, this.dom.clientHeight);
			if (!h) {
				h = parseInt(this.getStyle('height'), 10) || 0;
				if (!this.isBorderBox()) {
					h += this.getFrameWidth('tb');
				}
			}
			return h;
		},

		getComputedWidth : function() {
			var w = Math.max(this.dom.offsetWidth, this.dom.clientWidth);
			if (!w) {
				w = parseInt(this.getStyle('width'), 10) || 0;
				if (!this.isBorderBox()) {
					w += this.getFrameWidth('lr');
				}
			}
			return w;
		},

		getSize : function(contentSize) {
			return {
				width :this.getWidth(contentSize),
				height :this.getHeight(contentSize)
			};
		},

		getStyleSize : function() {
			var w, h, d = this.dom, s = d.style;
			if (s.width && s.width != 'auto') {
				w = parseInt(s.width, 10);
				if (Tera.isBorderBox) {
					w -= this.getFrameWidth('lr');
				}
			}
			if (s.height && s.height != 'auto') {
				h = parseInt(s.height, 10);
				if (Tera.isBorderBox) {
					h -= this.getFrameWidth('tb');
				}
			}
			return {
				width :w || this.getWidth(true),
				height :h || this.getHeight(true)
			};

		},

		getViewSize : function() {
			var d = this.dom, doc = document, aw = 0, ah = 0;
			if (d == doc || d == doc.body) {
				return {
					width :D.getViewWidth(),
					height :D.getViewHeight()
				};
			} else {
				return {
					width :d.clientWidth,
					height :d.clientHeight
				};
			}
		},

		getValue : function(asNumber) {
			return asNumber ? parseInt(this.dom.value, 10) : this.dom.value;
		},

		adjustWidth : function(width) {
			if (typeof width == "number") {
				if (this.autoBoxAdjust && !this.isBorderBox()) {
					width -= (this.getBorderWidth("lr") + this.getPadding("lr"));
				}
				if (width < 0) {
					width = 0;
				}
			}
			return width;
		},

		adjustHeight : function(height) {
			if (typeof height == "number") {
				if (this.autoBoxAdjust && !this.isBorderBox()) {
					height -= (this.getBorderWidth("tb") + this.getPadding("tb"));
				}
				if (height < 0) {
					height = 0;
				}
			}
			return height;
		},

		setWidth : function(width, animate) {
			width = this.adjustWidth(width);
			if (!animate || !A) {
				this.dom.style.width = this.addUnits(width);
			} else {
				this.anim( {
					width : {
						to :width
					}
				}, this.preanim(arguments, 1));
			}
			return this;
		},

		setHeight : function(height, animate) {
			height = this.adjustHeight(height);
			if (!animate || !A) {
				this.dom.style.height = this.addUnits(height);
			} else {
				this.anim( {
					height : {
						to :height
					}
				}, this.preanim(arguments, 1));
			}
			return this;
		},

		setSize : function(width, height, animate) {
			if (typeof width == "object") {
				height = width.height;
				width = width.width;
			}
			width = this.adjustWidth(width);
			height = this.adjustHeight(height);
			if (!animate || !A) {
				this.dom.style.width = this.addUnits(width);
				this.dom.style.height = this.addUnits(height);
			} else {
				this.anim( {
					width : {
						to :width
					},
					height : {
						to :height
					}
				}, this.preanim(arguments, 2));
			}
			return this;
		},

		setBounds : function(x, y, width, height, animate) {
			if (!animate || !A) {
				this.setSize(width, height);
				this.setLocation(x, y);
			} else {
				width = this.adjustWidth(width);
				height = this.adjustHeight(height);
				this.anim( {
					points : {
						to : [ x, y ]
					},
					width : {
						to :width
					},
					height : {
						to :height
					}
				}, this.preanim(arguments, 4), 'motion');
			}
			return this;
		},

		setRegion : function(region, animate) {
			this.setBounds(region.left, region.top, region.right - region.left, region.bottom - region.top, this.preanim(arguments, 1));
			return this;
		},

		addListener : function(eventName, fn, scope, options) {
			Tera.EventManager.on(this.dom, eventName, fn, scope || this, options);
		},

		removeListener : function(eventName, fn) {
			Tera.EventManager.removeListener(this.dom, eventName, fn);
			return this;
		},

		removeAllListeners : function() {
			E.purgeElement(this.dom);
			return this;
		},

		relayEvent : function(eventName, observable) {
			this.on(eventName, function(e) {
				observable.fireEvent(eventName, e);
			});
		},

		setOpacity : function(opacity, animate) {
			if (!animate || !A) {
				var s = this.dom.style;
				if (Tera.isIE) {
					s.zoom = 1;
					s.filter = (s.filter || '').replace(/alpha\([^\)]*\)/gi, "") + (opacity == 1 ? "" : " alpha(opacity=" + opacity * 100 + ")");
				} else {
					s.opacity = opacity;
				}
			} else {
				this.anim( {
					opacity : {
						to :opacity
					}
				}, this.preanim(arguments, 1), null, .35, 'easeIn');
			}
			return this;
		},

		getLeft : function(local) {
			if (!local) {
				return this.getX();
			} else {
				return parseInt(this.getStyle("left"), 10) || 0;
			}
		},

		getRight : function(local) {
			if (!local) {
				return this.getX() + this.getWidth();
			} else {
				return (this.getLeft(true) + this.getWidth()) || 0;
			}
		},

		getTop : function(local) {
			if (!local) {
				return this.getY();
			} else {
				return parseInt(this.getStyle("top"), 10) || 0;
			}
		},

		getBottom : function(local) {
			if (!local) {
				return this.getY() + this.getHeight();
			} else {
				return (this.getTop(true) + this.getHeight()) || 0;
			}
		},

		position : function(pos, zIndex, x, y) {
			if (!pos) {
				if (this.getStyle('position') == 'static') {
					this.setStyle('position', 'relative');
				}
			} else {
				this.setStyle("position", pos);
			}
			if (zIndex) {
				this.setStyle("z-index", zIndex);
			}
			if (x !== undefined && y !== undefined) {
				this.setXY( [ x, y ]);
			} else if (x !== undefined) {
				this.setX(x);
			} else if (y !== undefined) {
				this.setY(y);
			}
		},

		clearPositioning : function(value) {
			value = value || '';
			this.setStyle( {
				"left" :value,
				"right" :value,
				"top" :value,
				"bottom" :value,
				"z-index" :"",
				"position" :"static"
			});
			return this;
		},

		getPositioning : function() {
			var l = this.getStyle("left");
			var t = this.getStyle("top");
			return {
				"position" :this.getStyle("position"),
				"left" :l,
				"right" :l ? "" : this.getStyle("right"),
				"top" :t,
				"bottom" :t ? "" : this.getStyle("bottom"),
				"z-index" :this.getStyle("z-index")
			};
		},

		getBorderWidth : function(side) {
			return this.addStyles(side, El.borders);
		},

		getPadding : function(side) {
			return this.addStyles(side, El.paddings);
		},

		setPositioning : function(pc) {
			this.applyStyles(pc);
			if (pc.right == "auto") {
				this.dom.style.right = "";
			}
			if (pc.bottom == "auto") {
				this.dom.style.bottom = "";
			}
			return this;
		},

		fixDisplay : function() {
			if (this.getStyle("display") == "none") {
				this.setStyle("visibility", "hidden");
				this.setStyle("display", this.originalDisplay);
				if (this.getStyle("display") == "none") {
					this.setStyle("display", "block");
				}
			}
		},

		setOverflow : function(v) {
			if (v == 'auto' && Tera.isMac && Tera.isGecko) {
				this.dom.style.overflow = 'hidden';
				( function() {
					this.dom.style.overflow = 'auto';
				}).defer(1, this);
			} else {
				this.dom.style.overflow = v;
			}
		},

		setLeftTop : function(left, top) {
			this.dom.style.left = this.addUnits(left);
			this.dom.style.top = this.addUnits(top);
			return this;
		},

		move : function(direction, distance, animate) {
			var xy = this.getXY();
			direction = direction.toLowerCase();
			switch (direction) {
				case "l":
				case "left":
					this.moveTo(xy[0] - distance, xy[1], this.preanim(arguments, 2));
					break;
				case "r":
				case "right":
					this.moveTo(xy[0] + distance, xy[1], this.preanim(arguments, 2));
					break;
				case "t":
				case "top":
				case "up":
					this.moveTo(xy[0], xy[1] - distance, this.preanim(arguments, 2));
					break;
				case "b":
				case "bottom":
				case "down":
					this.moveTo(xy[0], xy[1] + distance, this.preanim(arguments, 2));
					break;
			}
			return this;
		},

		clip : function() {
			if (!this.isClipped) {
				this.isClipped = true;
				this.originalClip = {
					"o" :this.getStyle("overflow"),
					"x" :this.getStyle("overflow-x"),
					"y" :this.getStyle("overflow-y")
				};
				this.setStyle("overflow", "hidden");
				this.setStyle("overflow-x", "hidden");
				this.setStyle("overflow-y", "hidden");
			}
			return this;
		},

		unclip : function() {
			if (this.isClipped) {
				this.isClipped = false;
				var o = this.originalClip;
				if (o.o) {
					this.setStyle("overflow", o.o);
				}
				if (o.x) {
					this.setStyle("overflow-x", o.x);
				}
				if (o.y) {
					this.setStyle("overflow-y", o.y);
				}
			}
			return this;
		},

		getAnchorXY : function(anchor, local, s) {

			var w, h, vp = false;
			if (!s) {
				var d = this.dom;
				if (d == document.body || d == document) {
					vp = true;
					w = D.getViewWidth();
					h = D.getViewHeight();
				} else {
					w = this.getWidth();
					h = this.getHeight();
				}
			} else {
				w = s.width;
				h = s.height;
			}
			var x = 0, y = 0, r = Math.round;
			switch ((anchor || "tl").toLowerCase()) {
				case "c":
					x = r(w * .5);
					y = r(h * .5);
					break;
				case "t":
					x = r(w * .5);
					y = 0;
					break;
				case "l":
					x = 0;
					y = r(h * .5);
					break;
				case "r":
					x = w;
					y = r(h * .5);
					break;
				case "b":
					x = r(w * .5);
					y = h;
					break;
				case "tl":
					x = 0;
					y = 0;
					break;
				case "bl":
					x = 0;
					y = h;
					break;
				case "br":
					x = w;
					y = h;
					break;
				case "tr":
					x = w;
					y = 0;
					break;
			}
			if (local === true) {
				return [ x, y ];
			}
			if (vp) {
				var sc = this.getScroll();
				return [ x + sc.left, y + sc.top ];
			}
			var o = this.getXY();
			return [ x + o[0], y + o[1] ];
		},

		getAlignToXY : function(el, p, o) {
			el = Tera.get(el);
			if (!el || !el.dom) {
				throw "Element.alignToXY with an element that doesn't exist";
			}
			var d = this.dom;
			var c = false;
			var p1 = "", p2 = "";
			o = o || [ 0, 0 ];

			if (!p) {
				p = "tl-bl";
			} else if (p == "?") {
				p = "tl-bl?";
			} else if (p.indexOf("-") == -1) {
				p = "tl-" + p;
			}
			p = p.toLowerCase();
			var m = p.match(/^([a-z]+)-([a-z]+)(\?)?$/);
			if (!m) {
				throw "Element.alignTo with an invalid alignment " + p;
			}
			p1 = m[1];
			p2 = m[2];
			c = !!m[3];

			var a1 = this.getAnchorXY(p1, true);
			var a2 = el.getAnchorXY(p2, false);

			var x = a2[0] - a1[0] + o[0];
			var y = a2[1] - a1[1] + o[1];

			if (c) {
				var w = this.getWidth(), h = this.getHeight(), r = el.getRegion();
				var dw = D.getViewWidth() - 5, dh = D.getViewHeight() - 5;

				var p1y = p1.charAt(0), p1x = p1.charAt(p1.length - 1);
				var p2y = p2.charAt(0), p2x = p2.charAt(p2.length - 1);
				var swapY = ((p1y == "t" && p2y == "b") || (p1y == "b" && p2y == "t"));
				var swapX = ((p1x == "r" && p2x == "l") || (p1x == "l" && p2x == "r"));

				var doc = document;
				var scrollX = (doc.documentElement.scrollLeft || doc.body.scrollLeft || 0) + 5;
				var scrollY = (doc.documentElement.scrollTop || doc.body.scrollTop || 0) + 5;

				if ((x + w) > dw + scrollX) {
					x = swapX ? r.left - w : dw + scrollX - w;
				}
				if (x < scrollX) {
					x = swapX ? r.right : scrollX;
				}
				if ((y + h) > dh + scrollY) {
					y = swapY ? r.top - h : dh + scrollY - h;
				}
				if (y < scrollY) {
					y = swapY ? r.bottom : scrollY;
				}
			}
			return [ x, y ];
		},

		getConstrainToXY : function() {
			var os = {
				top :0,
				left :0,
				bottom :0,
				right :0
			};

			return function(el, local, offsets, proposedXY) {
				el = Tera.get(el);
				offsets = offsets ? Tera.applyIf(offsets, os) : os;

				var vw, vh, vx = 0, vy = 0;
				if (el.dom == document.body || el.dom == document) {
					vw = Tera.lib.Dom.getViewWidth();
					vh = Tera.lib.Dom.getViewHeight();
				} else {
					vw = el.dom.clientWidth;
					vh = el.dom.clientHeight;
					if (!local) {
						var vxy = el.getXY();
						vx = vxy[0];
						vy = vxy[1];
					}
				}

				var s = el.getScroll();

				vx += offsets.left + s.left;
				vy += offsets.top + s.top;

				vw -= offsets.right;
				vh -= offsets.bottom;

				var vr = vx + vw;
				var vb = vy + vh;

				var xy = proposedXY || (!local ? this.getXY() : [ this.getLeft(true), this.getTop(true) ]);
				var x = xy[0], y = xy[1];
				var w = this.dom.offsetWidth, h = this.dom.offsetHeight;

				var moved = false;

				if ((x + w) > vr) {
					x = vr - w;
					moved = true;
				}
				if ((y + h) > vb) {
					y = vb - h;
					moved = true;
				}
				if (x < vx) {
					x = vx;
					moved = true;
				}
				if (y < vy) {
					y = vy;
					moved = true;
				}
				return moved ? [ x, y ] : false;
			};
		}(),

		adjustForConstraints : function(xy, parent, offsets) {
			return this.getConstrainToXY(parent || document, false, offsets, xy) || xy;
		},

		alignTo : function(element, position, offsets, animate) {
			var xy = this.getAlignToXY(element, position, offsets);
			this.setXY(xy, this.preanim(arguments, 3));
			return this;
		},

		anchorTo : function(el, alignment, offsets, animate, monitorScroll, callback) {
			var action = function() {
				this.alignTo(el, alignment, offsets, animate);
				Tera.callback(callback, this);
			};
			Tera.EventManager.onWindowResize(action, this);
			var tm = typeof monitorScroll;
			if (tm != 'undefined') {
				Tera.EventManager.on(window, 'scroll', action, this, {
					buffer :tm == 'number' ? monitorScroll : 50
				});
			}
			action.call(this);
			return this;
		},

		clearOpacity : function() {
			if (window.ActiveXObject) {
				if (typeof this.dom.style.filter == 'string' && (/alpha/i).test(this.dom.style.filter)) {
					this.dom.style.filter = "";
				}
			} else {
				this.dom.style.opacity = "";
				this.dom.style["-moz-opacity"] = "";
				this.dom.style["-khtml-opacity"] = "";
			}
			return this;
		},

		hide : function(animate) {
			this.setVisible(false, this.preanim(arguments, 0));
			return this;
		},

		show : function(animate) {
			this.setVisible(true, this.preanim(arguments, 0));
			return this;
		},

		addUnits : function(size) {
			return Tera.Element.addUnits(size, this.defaultUnit);
		},

		update : function(html, loadScripts, callback) {
			if (typeof html == "undefined") {
				html = "";
			}
			if (loadScripts !== true) {
				this.dom.innerHTML = html;
				if (typeof callback == "function") {
					callback();
				}
				return this;
			}
			var id = Tera.id();
			var dom = this.dom;

			html += '<span id="' + id + '"></span>';

			E.onAvailable(id, function() {
				var hd = document.getElementsByTagName("head")[0];
				var re = /(?:<script([^>]*)?>)((\n|\r|.)*?)(?:<\/script>)/ig;
				var srcRe = /\ssrc=([\'\"])(.*?)\1/i;
				var typeRe = /\stype=([\'\"])(.*?)\1/i;

				var match;
				while (match = re.exec(html)) {
					var attrs = match[1];
					var srcMatch = attrs ? attrs.match(srcRe) : false;
					if (srcMatch && srcMatch[2]) {
						var s = document.createElement("script");
						s.src = srcMatch[2];
						var typeMatch = attrs.match(typeRe);
						if (typeMatch && typeMatch[2]) {
							s.type = typeMatch[2];
						}
						hd.appendChild(s);
					} else if (match[2] && match[2].length > 0) {
						if (window.execScript) {
							window.execScript(match[2]);
						} else {
							window.eval(match[2]);
						}
					}
				}
				var el = document.getElementById(id);
				if (el) {
					Tera.removeNode(el);
				}
				if (typeof callback == "function") {
					callback();
				}
			});
			dom.innerHTML = html.replace(/(?:<script.*?>)((\n|\r|.)*?)(?:<\/script>)/ig, "");
			return this;
		},

		load : function() {
			var um = this.getUpdater();
			um.update.apply(um, arguments);
			return this;
		},

		getUpdater : function() {
			if (!this.updateManager) {
				this.updateManager = new Tera.Updater(this);
			}
			return this.updateManager;
		},

		unselectable : function() {
			this.dom.unselectable = "on";
			this.swallowEvent("selectstart", true);
			this.applyStyles("-moz-user-select:none;-khtml-user-select:none;");
			this.addClass("x-unselectable");
			return this;
		},

		getCenterXY : function() {
			return this.getAlignToXY(document, 'c-c');
		},

		center : function(centerIn) {
			this.alignTo(centerIn || document, 'c-c');
			return this;
		},

		isBorderBox : function() {
			return noBoxAdjust[this.dom.tagName.toLowerCase()] || Tera.isBorderBox;
		},

		getBox : function(contentBox, local) {
			var xy;
			if (!local) {
				xy = this.getXY();
			} else {
				var left = parseInt(this.getStyle("left"), 10) || 0;
				var top = parseInt(this.getStyle("top"), 10) || 0;
				xy = [ left, top ];
			}
			var el = this.dom, w = el.offsetWidth, h = el.offsetHeight, bx;
			if (!contentBox) {
				bx = {
					x :xy[0],
					y :xy[1],
					0 :xy[0],
					1 :xy[1],
					width :w,
					height :h
				};
			} else {
				var l = this.getBorderWidth("l") + this.getPadding("l");
				var r = this.getBorderWidth("r") + this.getPadding("r");
				var t = this.getBorderWidth("t") + this.getPadding("t");
				var b = this.getBorderWidth("b") + this.getPadding("b");
				bx = {
					x :xy[0] + l,
					y :xy[1] + t,
					0 :xy[0] + l,
					1 :xy[1] + t,
					width :w - (l + r),
					height :h - (t + b)
				};
			}
			bx.right = bx.x + bx.width;
			bx.bottom = bx.y + bx.height;
			return bx;
		},

		getFrameWidth : function(sides, onlyContentBox) {
			return onlyContentBox && Tera.isBorderBox ? 0 : (this.getPadding(sides) + this.getBorderWidth(sides));
		},

		setBox : function(box, adjust, animate) {
			var w = box.width, h = box.height;
			if ((adjust && !this.autoBoxAdjust) && !this.isBorderBox()) {
				w -= (this.getBorderWidth("lr") + this.getPadding("lr"));
				h -= (this.getBorderWidth("tb") + this.getPadding("tb"));
			}
			this.setBounds(box.x, box.y, w, h, this.preanim(arguments, 2));
			return this;
		},

		repaint : function() {
			var dom = this.dom;
			this.addClass("x-repaint");
			setTimeout( function() {
				Tera.get(dom).removeClass("x-repaint");
			}, 1);
			return this;
		},

		getMargins : function(side) {
			if (!side) {
				return {
					top :parseInt(this.getStyle("margin-top"), 10) || 0,
					left :parseInt(this.getStyle("margin-left"), 10) || 0,
					bottom :parseInt(this.getStyle("margin-bottom"), 10) || 0,
					right :parseInt(this.getStyle("margin-right"), 10) || 0
				};
			} else {
				return this.addStyles(side, El.margins);
			}
		},

		addStyles : function(sides, styles) {
			var val = 0, v, w;
			for ( var i = 0, len = sides.length; i < len; i++) {
				v = this.getStyle(styles[sides.charAt(i)]);
				if (v) {
					w = parseInt(v, 10);
					if (w) {
						val += (w >= 0 ? w : -1 * w);
					}
				}
			}
			return val;
		},

		createProxy : function(config, renderTo, matchBox) {
			config = typeof config == "object" ? config : {
				tag :"div",
				cls :config
			};

			var proxy;
			if (renderTo) {
				proxy = Tera.DomHelper.append(renderTo, config, true);
			} else {
				proxy = Tera.DomHelper.insertBefore(this.dom, config, true);
			}
			if (matchBox) {
				proxy.setBox(this.getBox());
			}
			return proxy;
		},

		mask : function(msg, msgCls) {
			if (this.getStyle("position") == "static") {
				this.setStyle("position", "relative");
			}
			if (this._maskMsg) {
				this._maskMsg.remove();
			}
			if (this._mask) {
				this._mask.remove();
			}

			this._mask = Tera.DomHelper.append(this.dom, {
				cls :"ext-el-mask"
			}, true);

			this.addClass("x-masked");
			this._mask.setDisplayed(true);
			if (typeof msg == 'string') {
				this._maskMsg = Tera.DomHelper.append(this.dom, {
					cls :"ext-el-mask-msg",
					cn : {
						tag :'div'
					}
				}, true);
				var mm = this._maskMsg;
				mm.dom.className = msgCls ? "ext-el-mask-msg " + msgCls : "ext-el-mask-msg";
				mm.dom.firstChild.innerHTML = msg;
				mm.setDisplayed(true);
				mm.center(this);
			}
			if (Tera.isIE && !(Tera.isIE7 && Tera.isStrict) && this.getStyle('height') == 'auto') {
				this._mask.setSize(this.dom.clientWidth, this.getHeight());
			}
			return this._mask;
		},

		unmask : function() {
			if (this._mask) {
				if (this._maskMsg) {
					this._maskMsg.remove();
					delete this._maskMsg;
				}
				this._mask.remove();
				delete this._mask;
			}
			this.removeClass("x-masked");
		},

		isMasked : function() {
			return this._mask && this._mask.isVisible();
		},

		createShim : function() {
			var el = document.createElement('iframe');
			el.frameBorder = 'no';
			el.className = 'ext-shim';
			if (Tera.isIE && Tera.isSecure) {
				el.src = Tera.SSL_SECURE_URL;
			}
			var shim = Tera.get(this.dom.parentNode.insertBefore(el, this.dom));
			shim.autoBoxAdjust = false;
			return shim;
		},

		remove : function() {
			Tera.removeNode(this.dom);
			delete El.cache[this.dom.id];
		},

		hover : function(overFn, outFn, scope) {
			var preOverFn = function(e) {
				if (!e.within(this, true)) {
					overFn.apply(scope || this, arguments);
				}
			};
			var preOutFn = function(e) {
				if (!e.within(this, true)) {
					outFn.apply(scope || this, arguments);
				}
			};
			this.on("mouseover", preOverFn, this.dom);
			this.on("mouseout", preOutFn, this.dom);
			return this;
		},

		addClassOnOver : function(className) {
			this.hover( function() {
				Tera.fly(this, '_internal').addClass(className);
			}, function() {
				Tera.fly(this, '_internal').removeClass(className);
			});
			return this;
		},

		addClassOnFocus : function(className) {
			this.on("focus", function() {
				Tera.fly(this, '_internal').addClass(className);
			}, this.dom);
			this.on("blur", function() {
				Tera.fly(this, '_internal').removeClass(className);
			}, this.dom);
			return this;
		},

		addClassOnClick : function(className) {
			var dom = this.dom;
			this.on("mousedown", function() {
				Tera.fly(dom, '_internal').addClass(className);
				var d = Tera.getDoc();
				var fn = function() {
					Tera.fly(dom, '_internal').removeClass(className);
					d.removeListener("mouseup", fn);
				};
				d.on("mouseup", fn);
			});
			return this;
		},

		swallowEvent : function(eventName, preventDefault) {
			var fn = function(e) {
				e.stopPropagation();
				if (preventDefault) {
					e.preventDefault();
				}
			};
			if (Tera.isArray(eventName)) {
				for ( var i = 0, len = eventName.length; i < len; i++) {
					this.on(eventName[i], fn);
				}
				return this;
			}
			this.on(eventName, fn);
			return this;
		},

		parent : function(selector, returnDom) {
			return this.matchNode('parentNode', 'parentNode', selector, returnDom);
		},

		next : function(selector, returnDom) {
			return this.matchNode('nextSibling', 'nextSibling', selector, returnDom);
		},

		prev : function(selector, returnDom) {
			return this.matchNode('previousSibling', 'previousSibling', selector, returnDom);
		},

		first : function(selector, returnDom) {
			return this.matchNode('nextSibling', 'firstChild', selector, returnDom);
		},

		last : function(selector, returnDom) {
			return this.matchNode('previousSibling', 'lastChild', selector, returnDom);
		},

		matchNode : function(dir, start, selector, returnDom) {
			var n = this.dom[start];
			while (n) {
				if (n.nodeType == 1 && (!selector || Tera.DomQuery.is(n, selector))) {
					return !returnDom ? Tera.get(n) : n;
				}
				n = n[dir];
			}
			return null;
		},

		appendChild : function(el) {
			el = Tera.get(el);
			el.appendTo(this);
			return this;
		},

		createChild : function(config, insertBefore, returnDom) {
			config = config || {
				tag :'div'
			};
			if (insertBefore) {
				return Tera.DomHelper.insertBefore(insertBefore, config, returnDom !== true);
			}
			return Tera.DomHelper[!this.dom.firstChild ? 'overwrite' : 'append'](this.dom, config, returnDom !== true);
		},

		appendTo : function(el) {
			el = Tera.getDom(el);
			el.appendChild(this.dom);
			return this;
		},

		insertBefore : function(el) {
			el = Tera.getDom(el);
			el.parentNode.insertBefore(this.dom, el);
			return this;
		},

		insertAfter : function(el) {
			el = Tera.getDom(el);
			el.parentNode.insertBefore(this.dom, el.nextSibling);
			return this;
		},

		insertFirst : function(el, returnDom) {
			el = el || {};
			if (typeof el == 'object' && !el.nodeType && !el.dom) {
				return this.createChild(el, this.dom.firstChild, returnDom);
			} else {
				el = Tera.getDom(el);
				this.dom.insertBefore(el, this.dom.firstChild);
				return !returnDom ? Tera.get(el) : el;
			}
		},

		insertSibling : function(el, where, returnDom) {
			var rt;
			if (Tera.isArray(el)) {
				for ( var i = 0, len = el.length; i < len; i++) {
					rt = this.insertSibling(el[i], where, returnDom);
				}
				return rt;
			}
			where = where ? where.toLowerCase() : 'before';
			el = el || {};
			var refNode = where == 'before' ? this.dom : this.dom.nextSibling;

			if (typeof el == 'object' && !el.nodeType && !el.dom) {
				if (where == 'after' && !this.dom.nextSibling) {
					rt = Tera.DomHelper.append(this.dom.parentNode, el, !returnDom);
				} else {
					rt = Tera.DomHelper[where == 'after' ? 'insertAfter' : 'insertBefore'](this.dom, el, !returnDom);
				}

			} else {
				rt = this.dom.parentNode.insertBefore(Tera.getDom(el), refNode);
				if (!returnDom) {
					rt = Tera.get(rt);
				}
			}
			return rt;
		},

		wrap : function(config, returnDom) {
			if (!config) {
				config = {
					tag :"div"
				};
			}
			var newEl = Tera.DomHelper.insertBefore(this.dom, config, !returnDom);
			newEl.dom ? newEl.dom.appendChild(this.dom) : newEl.appendChild(this.dom);
			return newEl;
		},

		replace : function(el) {
			el = Tera.get(el);
			this.insertBefore(el);
			el.remove();
			return this;
		},

		replaceWith : function(el) {
			if (typeof el == 'object' && !el.nodeType && !el.dom) {
				el = this.insertSibling(el, 'before');
			} else {
				el = Tera.getDom(el);
				this.dom.parentNode.insertBefore(el, this.dom);
			}
			El.uncache(this.id);
			this.dom.parentNode.removeChild(this.dom);
			this.dom = el;
			this.id = Tera.id(el);
			El.cache[this.id] = this;
			return this;
		},

		insertHtml : function(where, html, returnEl) {
			var el = Tera.DomHelper.insertHtml(where, this.dom, html);
			return returnEl ? Tera.get(el) : el;
		},

		set : function(o, useSet) {
			var el = this.dom;
			useSet = typeof useSet == 'undefined' ? (el.setAttribute ? true : false) : useSet;
			for ( var attr in o) {
				if (attr == "style" || typeof o[attr] == "function")
					continue;
				if (attr == "cls") {
					el.className = o["cls"];
				} else if (o.hasOwnProperty(attr)) {
					if (useSet)
						el.setAttribute(attr, o[attr]);
					else
						el[attr] = o[attr];
				}
			}
			if (o.style) {
				Tera.DomHelper.applyStyles(el, o.style);
			}
			return this;
		},

		addKeyListener : function(key, fn, scope) {
			var config;
			if (typeof key != "object" || Tera.isArray(key)) {
				config = {
					key :key,
					fn :fn,
					scope :scope
				};
			} else {
				config = {
					key :key.key,
					shift :key.shift,
					ctrl :key.ctrl,
					alt :key.alt,
					fn :fn,
					scope :scope
				};
			}
			return new Tera.KeyMap(this, config);
		},

		addKeyMap : function(config) {
			return new Tera.KeyMap(this, config);
		},

		isScrollable : function() {
			var dom = this.dom;
			return dom.scrollHeight > dom.clientHeight || dom.scrollWidth > dom.clientWidth;
		},

		scrollTo : function(side, value, animate) {
			var prop = side.toLowerCase() == "left" ? "scrollLeft" : "scrollTop";
			if (!animate || !A) {
				this.dom[prop] = value;
			} else {
				var to = prop == "scrollLeft" ? [ value, this.dom.scrollTop ] : [ this.dom.scrollLeft, value ];
				this.anim( {
					scroll : {
						"to" :to
					}
				}, this.preanim(arguments, 2), 'scroll');
			}
			return this;
		},

		scroll : function(direction, distance, animate) {
			if (!this.isScrollable()) {
				return;
			}
			var el = this.dom;
			var l = el.scrollLeft, t = el.scrollTop;
			var w = el.scrollWidth, h = el.scrollHeight;
			var cw = el.clientWidth, ch = el.clientHeight;
			direction = direction.toLowerCase();
			var scrolled = false;
			var a = this.preanim(arguments, 2);
			switch (direction) {
				case "l":
				case "left":
					if (w - l > cw) {
						var v = Math.min(l + distance, w - cw);
						this.scrollTo("left", v, a);
						scrolled = true;
					}
					break;
				case "r":
				case "right":
					if (l > 0) {
						var v = Math.max(l - distance, 0);
						this.scrollTo("left", v, a);
						scrolled = true;
					}
					break;
				case "t":
				case "top":
				case "up":
					if (t > 0) {
						var v = Math.max(t - distance, 0);
						this.scrollTo("top", v, a);
						scrolled = true;
					}
					break;
				case "b":
				case "bottom":
				case "down":
					if (h - t > ch) {
						var v = Math.min(t + distance, h - ch);
						this.scrollTo("top", v, a);
						scrolled = true;
					}
					break;
			}
			return scrolled;
		},

		translatePoints : function(x, y) {
			if (typeof x == 'object' || Tera.isArray(x)) {
				y = x[1];
				x = x[0];
			}
			var p = this.getStyle('position');
			var o = this.getXY();

			var l = parseInt(this.getStyle('left'), 10);
			var t = parseInt(this.getStyle('top'), 10);

			if (isNaN(l)) {
				l = (p == "relative") ? 0 : this.dom.offsetLeft;
			}
			if (isNaN(t)) {
				t = (p == "relative") ? 0 : this.dom.offsetTop;
			}

			return {
				left :(x - o[0] + l),
				top :(y - o[1] + t)
			};
		},

		getScroll : function() {
			var d = this.dom, doc = document;
			if (d == doc || d == doc.body) {
				var l, t;
				if (Tera.isIE && Tera.isStrict) {
					l = doc.documentElement.scrollLeft || (doc.body.scrollLeft || 0);
					t = doc.documentElement.scrollTop || (doc.body.scrollTop || 0);
				} else {
					l = window.pageXOffset || (doc.body.scrollLeft || 0);
					t = window.pageYOffset || (doc.body.scrollTop || 0);
				}
				return {
					left :l,
					top :t
				};
			} else {
				return {
					left :d.scrollLeft,
					top :d.scrollTop
				};
			}
		},

		getColor : function(attr, defaultValue, prefix) {
			var v = this.getStyle(attr);
			if (!v || v == "transparent" || v == "inherit") {
				return defaultValue;
			}
			var color = typeof prefix == "undefined" ? "#" : prefix;
			if (v.substr(0, 4) == "rgb(") {
				var rvs = v.slice(4, v.length - 1).split(",");
				for ( var i = 0; i < 3; i++) {
					var h = parseInt(rvs[i]);
					var s = h.toString(16);
					if (h < 16) {
						s = "0" + s;
					}
					color += s;
				}
			} else {
				if (v.substr(0, 1) == "#") {
					if (v.length == 4) {
						for ( var i = 1; i < 4; i++) {
							var c = v.charAt(i);
							color += c + c;
						}
					} else if (v.length == 7) {
						color += v.substr(1);
					}
				}
			}
			return (color.length > 5 ? color.toLowerCase() : defaultValue);
		},

		boxWrap : function(cls) {
			cls = cls || 'x-box';
			var el = Tera.get(this.insertHtml('beforeBegin', String.format('<div class="{0}">' + El.boxMarkup + '</div>', cls)));
			el.child('.' + cls + '-mc').dom.appendChild(this.dom);
			return el;
		},

		getAttributeNS :Tera.isIE ? function(ns, name) {
			var d = this.dom;
			var type = typeof d[ns + ":" + name];
			if (type != 'undefined' && type != 'unknown') {
				return d[ns + ":" + name];
			}
			return d[name];
		} : function(ns, name) {
			var d = this.dom;
			return d.getAttributeNS(ns, name) || d.getAttribute(ns + ":" + name) || d.getAttribute(name) || d[name];
		},

		getTextWidth : function(text, min, max) {
			return (Tera.util.TextMetrics.measure(this.dom, Tera.value(text, this.dom.innerHTML, true)).width).constrain(min || 0, max || 1000000);
		}
	};

	var ep = El.prototype;

	ep.on = ep.addListener;
	ep.mon = ep.addListener;

	ep.getUpdateManager = ep.getUpdater;

	ep.un = ep.removeListener;

	ep.autoBoxAdjust = true;

	El.unitPattern = /\d+(px|em|%|en|ex|pt|in|cm|mm|pc)$/i;

	El.addUnits = function(v, defaultUnit) {
		if (v === "" || v == "auto") {
			return v;
		}
		if (v === undefined) {
			return '';
		}
		if (typeof v == "number" || !El.unitPattern.test(v)) {
			return v + (defaultUnit || 'px');
		}
		return v;
	};

	El.boxMarkup = '<div class="{0}-tl"><div class="{0}-tr"><div class="{0}-tc"></div></div></div><div class="{0}-ml"><div class="{0}-mr"><div class="{0}-mc"></div></div></div><div class="{0}-bl"><div class="{0}-br"><div class="{0}-bc"></div></div></div>';

	El.VISIBILITY = 1;

	El.DISPLAY = 2;

	El.borders = {
		l :"border-left-width",
		r :"border-right-width",
		t :"border-top-width",
		b :"border-bottom-width"
	};
	El.paddings = {
		l :"padding-left",
		r :"padding-right",
		t :"padding-top",
		b :"padding-bottom"
	};
	El.margins = {
		l :"margin-left",
		r :"margin-right",
		t :"margin-top",
		b :"margin-bottom"
	};

	El.cache = {};

	var docEl;

	El.get = function(el) {
		var ex, elm, id;
		if (!el) {
			return null;
		}
		if (typeof el == "string") {
			if (!(elm = document.getElementById(el))) {
				return null;
			}
			if (ex = El.cache[el]) {
				ex.dom = elm;
			} else {
				ex = El.cache[el] = new El(elm);
			}
			return ex;
		} else if (el.tagName) {
			if (!(id = el.id)) {
				id = Tera.id(el);
			}
			if (ex = El.cache[id]) {
				ex.dom = el;
			} else {
				ex = El.cache[id] = new El(el);
			}
			return ex;
		} else if (el instanceof El) {
			if (el != docEl) {
				el.dom = document.getElementById(el.id) || el.dom;
				El.cache[el.id] = el;
			}
			return el;
		} else if (el.isComposite) {
			return el;
		} else if (Tera.isArray(el)) {
			return El.select(el);
		} else if (el == document) {
			if (!docEl) {
				var f = function() {
				};
				f.prototype = El.prototype;
				docEl = new f();
				docEl.dom = document;
			}
			return docEl;
		}
		return null;
	};

	El.uncache = function(el) {
		for ( var i = 0, a = arguments, len = a.length; i < len; i++) {
			if (a[i]) {
				delete El.cache[a[i].id || a[i]];
			}
		}
	};

	El.garbageCollect = function() {
		if (!Tera.enableGarbageCollector) {
			clearInterval(El.collectorThread);
			return;
		}
		for ( var eid in El.cache) {
			var el = El.cache[eid], d = el.dom;
			if (!d || !d.parentNode || (!d.offsetParent && !document.getElementById(eid))) {
				delete El.cache[eid];
				if (d && Tera.enableListenerCollection) {
					E.purgeElement(d);
				}
			}
		}
	}
	El.collectorThreadId = setInterval(El.garbageCollect, 30000);

	var flyFn = function() {
	};
	flyFn.prototype = El.prototype;
	var _cls = new flyFn();

	El.Flyweight = function(dom) {
		this.dom = dom;
	};

	El.Flyweight.prototype = _cls;
	El.Flyweight.prototype.isFlyweight = true;

	El._flyweights = {};

	El.fly = function(el, named) {
		named = named || '_global';
		el = Tera.getDom(el);
		if (!el) {
			return null;
		}
		if (!El._flyweights[named]) {
			El._flyweights[named] = new El.Flyweight();
		}
		El._flyweights[named].dom = el;
		return El._flyweights[named];
	};

	Tera.get = El.get;

	Tera.fly = El.fly;

	var noBoxAdjust = Tera.isStrict ? {
		select :1
	} : {
		input :1,
		select :1,
		textarea :1
	};
	if (Tera.isIE || Tera.isGecko) {
		noBoxAdjust['button'] = 1;
	}

	Tera.EventManager.on(window, 'unload', function() {
		delete El.cache;
		delete El._flyweights;
	});
})();

Tera.enableFx = true;

Tera.Fx = {

	slideIn : function(anchor, o) {
		var el = this.getFxEl();
		o = o || {};

		el.queueFx(o, function() {

			anchor = anchor || "t";

			this.fixDisplay();

			var r = this.getFxRestore();
			var b = this.getBox();
			this.setSize(b);

			var wrap = this.fxWrap(r.pos, o, "hidden");

			var st = this.dom.style;
			st.visibility = "visible";
			st.position = "absolute";

			var after = function() {
				el.fxUnwrap(wrap, r.pos, o);
				st.width = r.width;
				st.height = r.height;
				el.afterFx(o);
			};
			var a, pt = {
				to : [ b.x, b.y ]
			}, bw = {
				to :b.width
			}, bh = {
				to :b.height
			};

			switch (anchor.toLowerCase()) {
				case "t":
					wrap.setSize(b.width, 0);
					st.left = st.bottom = "0";
					a = {
						height :bh
					};
					break;
				case "l":
					wrap.setSize(0, b.height);
					st.right = st.top = "0";
					a = {
						width :bw
					};
					break;
				case "r":
					wrap.setSize(0, b.height);
					wrap.setX(b.right);
					st.left = st.top = "0";
					a = {
						width :bw,
						points :pt
					};
					break;
				case "b":
					wrap.setSize(b.width, 0);
					wrap.setY(b.bottom);
					st.left = st.top = "0";
					a = {
						height :bh,
						points :pt
					};
					break;
				case "tl":
					wrap.setSize(0, 0);
					st.right = st.bottom = "0";
					a = {
						width :bw,
						height :bh
					};
					break;
				case "bl":
					wrap.setSize(0, 0);
					wrap.setY(b.y + b.height);
					st.right = st.top = "0";
					a = {
						width :bw,
						height :bh,
						points :pt
					};
					break;
				case "br":
					wrap.setSize(0, 0);
					wrap.setXY( [ b.right, b.bottom ]);
					st.left = st.top = "0";
					a = {
						width :bw,
						height :bh,
						points :pt
					};
					break;
				case "tr":
					wrap.setSize(0, 0);
					wrap.setX(b.x + b.width);
					st.left = st.bottom = "0";
					a = {
						width :bw,
						height :bh,
						points :pt
					};
					break;
			}
			this.dom.style.visibility = "visible";
			wrap.show();

			arguments.callee.anim = wrap.fxanim(a, o, 'motion', .5, 'easeOut', after);
		});
		return this;
	},

	slideOut : function(anchor, o) {
		var el = this.getFxEl();
		o = o || {};

		el.queueFx(o, function() {

			anchor = anchor || "t";

			var r = this.getFxRestore();

			var b = this.getBox();
			this.setSize(b);

			var wrap = this.fxWrap(r.pos, o, "visible");

			var st = this.dom.style;
			st.visibility = "visible";
			st.position = "absolute";

			wrap.setSize(b);

			var after = function() {
				if (o.useDisplay) {
					el.setDisplayed(false);
				} else {
					el.hide();
				}

				el.fxUnwrap(wrap, r.pos, o);

				st.width = r.width;
				st.height = r.height;

				el.afterFx(o);
			};

			var a, zero = {
				to :0
			};
			switch (anchor.toLowerCase()) {
				case "t":
					st.left = st.bottom = "0";
					a = {
						height :zero
					};
					break;
				case "l":
					st.right = st.top = "0";
					a = {
						width :zero
					};
					break;
				case "r":
					st.left = st.top = "0";
					a = {
						width :zero,
						points : {
							to : [ b.right, b.y ]
						}
					};
					break;
				case "b":
					st.left = st.top = "0";
					a = {
						height :zero,
						points : {
							to : [ b.x, b.bottom ]
						}
					};
					break;
				case "tl":
					st.right = st.bottom = "0";
					a = {
						width :zero,
						height :zero
					};
					break;
				case "bl":
					st.right = st.top = "0";
					a = {
						width :zero,
						height :zero,
						points : {
							to : [ b.x, b.bottom ]
						}
					};
					break;
				case "br":
					st.left = st.top = "0";
					a = {
						width :zero,
						height :zero,
						points : {
							to : [ b.x + b.width, b.bottom ]
						}
					};
					break;
				case "tr":
					st.left = st.bottom = "0";
					a = {
						width :zero,
						height :zero,
						points : {
							to : [ b.right, b.y ]
						}
					};
					break;
			}

			arguments.callee.anim = wrap.fxanim(a, o, 'motion', .5, "easeOut", after);
		});
		return this;
	},

	puff : function(o) {
		var el = this.getFxEl();
		o = o || {};

		el.queueFx(o, function() {
			this.clearOpacity();
			this.show();

			var r = this.getFxRestore();
			var st = this.dom.style;

			var after = function() {
				if (o.useDisplay) {
					el.setDisplayed(false);
				} else {
					el.hide();
				}

				el.clearOpacity();

				el.setPositioning(r.pos);
				st.width = r.width;
				st.height = r.height;
				st.fontSize = '';
				el.afterFx(o);
			};

			var width = this.getWidth();
			var height = this.getHeight();

			arguments.callee.anim = this.fxanim( {
				width : {
					to :this.adjustWidth(width * 2)
				},
				height : {
					to :this.adjustHeight(height * 2)
				},
				points : {
					by : [ -(width * .5), -(height * .5) ]
				},
				opacity : {
					to :0
				},
				fontSize : {
					to :200,
					unit :"%"
				}
			}, o, 'motion', .5, "easeOut", after);
		});
		return this;
	},

	switchOff : function(o) {
		var el = this.getFxEl();
		o = o || {};

		el.queueFx(o, function() {
			this.clearOpacity();
			this.clip();

			var r = this.getFxRestore();
			var st = this.dom.style;

			var after = function() {
				if (o.useDisplay) {
					el.setDisplayed(false);
				} else {
					el.hide();
				}

				el.clearOpacity();
				el.setPositioning(r.pos);
				st.width = r.width;
				st.height = r.height;

				el.afterFx(o);
			};

			this.fxanim( {
				opacity : {
					to :0.3
				}
			}, null, null, .1, null, function() {
				this.clearOpacity();
				( function() {
					this.fxanim( {
						height : {
							to :1
						},
						points : {
							by : [ 0, this.getHeight() * .5 ]
						}
					}, o, 'motion', 0.3, 'easeIn', after);
				}).defer(100, this);
			});
		});
		return this;
	},

	highlight : function(color, o) {
		var el = this.getFxEl();
		o = o || {};

		el.queueFx(o, function() {
			color = color || "ffff9c";
			var attr = o.attr || "backgroundColor";

			this.clearOpacity();
			this.show();

			var origColor = this.getColor(attr);
			var restoreColor = this.dom.style[attr];
			var endColor = (o.endColor || origColor) || "ffffff";

			var after = function() {
				el.dom.style[attr] = restoreColor;
				el.afterFx(o);
			};

			var a = {};
			a[attr] = {
				from :color,
				to :endColor
			};
			arguments.callee.anim = this.fxanim(a, o, 'color', 1, 'easeIn', after);
		});
		return this;
	},

	frame : function(color, count, o) {
		var el = this.getFxEl();
		o = o || {};

		el.queueFx(o, function() {
			color = color || "#C3DAF9";
			if (color.length == 6) {
				color = "#" + color;
			}
			count = count || 1;
			var duration = o.duration || 1;
			this.show();

			var b = this.getBox();
			var animFn = function() {
				var proxy = Tera.getBody().createChild( {
					style : {
						visbility :"hidden",
						position :"absolute",
						"z-index" :"35000",
						border :"0px solid " + color
					}
				});
				var scale = Tera.isBorderBox ? 2 : 1;
				proxy.animate( {
					top : {
						from :b.y,
						to :b.y - 20
					},
					left : {
						from :b.x,
						to :b.x - 20
					},
					borderWidth : {
						from :0,
						to :10
					},
					opacity : {
						from :1,
						to :0
					},
					height : {
						from :b.height,
						to :(b.height + (20 * scale))
					},
					width : {
						from :b.width,
						to :(b.width + (20 * scale))
					}
				}, duration, function() {
					proxy.remove();
					if (--count > 0) {
						animFn();
					} else {
						el.afterFx(o);
					}
				});
			};
			animFn.call(this);
		});
		return this;
	},

	pause : function(seconds) {
		var el = this.getFxEl();
		var o = {};

		el.queueFx(o, function() {
			setTimeout( function() {
				el.afterFx(o);
			}, seconds * 1000);
		});
		return this;
	},

	fadeIn : function(o) {
		var el = this.getFxEl();
		o = o || {};
		el.queueFx(o, function() {
			this.setOpacity(0);
			this.fixDisplay();
			this.dom.style.visibility = 'visible';
			var to = o.endOpacity || 1;
			arguments.callee.anim = this.fxanim( {
				opacity : {
					to :to
				}
			}, o, null, .5, "easeOut", function() {
				if (to == 1) {
					this.clearOpacity();
				}
				el.afterFx(o);
			});
		});
		return this;
	},

	fadeOut : function(o) {
		var el = this.getFxEl();
		o = o || {};
		el.queueFx(o, function() {
			arguments.callee.anim = this.fxanim( {
				opacity : {
					to :o.endOpacity || 0
				}
			}, o, null, .5, "easeOut", function() {
				if (this.visibilityMode == Tera.Element.DISPLAY || o.useDisplay) {
					this.dom.style.display = "none";
				} else {
					this.dom.style.visibility = "hidden";
				}
				this.clearOpacity();
				el.afterFx(o);
			});
		});
		return this;
	},

	scale : function(w, h, o) {
		this.shift(Tera.apply( {}, o, {
			width :w,
			height :h
		}));
		return this;
	},

	shift : function(o) {
		var el = this.getFxEl();
		o = o || {};
		el.queueFx(o, function() {
			var a = {}, w = o.width, h = o.height, x = o.x, y = o.y, op = o.opacity;
			if (w !== undefined) {
				a.width = {
					to :this.adjustWidth(w)
				};
			}
			if (h !== undefined) {
				a.height = {
					to :this.adjustHeight(h)
				};
			}
			if (o.left !== undefined) {
				a.left = {
					to :o.left
				};
			}
			if (o.top !== undefined) {
				a.top = {
					to :o.top
				};
			}
			if (o.right !== undefined) {
				a.right = {
					to :o.right
				};
			}
			if (o.bottom !== undefined) {
				a.bottom = {
					to :o.bottom
				};
			}
			if (x !== undefined || y !== undefined) {
				a.points = {
					to : [ x !== undefined ? x : this.getX(), y !== undefined ? y : this.getY() ]
				};
			}
			if (op !== undefined) {
				a.opacity = {
					to :op
				};
			}
			if (o.xy !== undefined) {
				a.points = {
					to :o.xy
				};
			}
			arguments.callee.anim = this.fxanim(a, o, 'motion', .35, "easeOut", function() {
				el.afterFx(o);
			});
		});
		return this;
	},

	ghost : function(anchor, o) {
		var el = this.getFxEl();
		o = o || {};

		el.queueFx(o, function() {
			anchor = anchor || "b";

			var r = this.getFxRestore();
			var w = this.getWidth(), h = this.getHeight();

			var st = this.dom.style;

			var after = function() {
				if (o.useDisplay) {
					el.setDisplayed(false);
				} else {
					el.hide();
				}

				el.clearOpacity();
				el.setPositioning(r.pos);
				st.width = r.width;
				st.height = r.height;

				el.afterFx(o);
			};

			var a = {
				opacity : {
					to :0
				},
				points : {}
			}, pt = a.points;
			switch (anchor.toLowerCase()) {
				case "t":
					pt.by = [ 0, -h ];
					break;
				case "l":
					pt.by = [ -w, 0 ];
					break;
				case "r":
					pt.by = [ w, 0 ];
					break;
				case "b":
					pt.by = [ 0, h ];
					break;
				case "tl":
					pt.by = [ -w, -h ];
					break;
				case "bl":
					pt.by = [ -w, h ];
					break;
				case "br":
					pt.by = [ w, h ];
					break;
				case "tr":
					pt.by = [ w, -h ];
					break;
			}

			arguments.callee.anim = this.fxanim(a, o, 'motion', .5, "easeOut", after);
		});
		return this;
	},

	syncFx : function() {
		this.fxDefaults = Tera.apply(this.fxDefaults || {}, {
			block :false,
			concurrent :true,
			stopFx :false
		});
		return this;
	},

	sequenceFx : function() {
		this.fxDefaults = Tera.apply(this.fxDefaults || {}, {
			block :false,
			concurrent :false,
			stopFx :false
		});
		return this;
	},

	nextFx : function() {
		var ef = this.fxQueue[0];
		if (ef) {
			ef.call(this);
		}
	},

	hasActiveFx : function() {
		return this.fxQueue && this.fxQueue[0];
	},

	stopFx : function() {
		if (this.hasActiveFx()) {
			var cur = this.fxQueue[0];
			if (cur && cur.anim && cur.anim.isAnimated()) {
				this.fxQueue = [ cur ];
				cur.anim.stop(true);
			}
		}
		return this;
	},

	beforeFx : function(o) {
		if (this.hasActiveFx() && !o.concurrent) {
			if (o.stopFx) {
				this.stopFx();
				return true;
			}
			return false;
		}
		return true;
	},

	hasFxBlock : function() {
		var q = this.fxQueue;
		return q && q[0] && q[0].block;
	},

	queueFx : function(o, fn) {
		if (!this.fxQueue) {
			this.fxQueue = [];
		}
		if (!this.hasFxBlock()) {
			Tera.applyIf(o, this.fxDefaults);
			if (!o.concurrent) {
				var run = this.beforeFx(o);
				fn.block = o.block;
				this.fxQueue.push(fn);
				if (run) {
					this.nextFx();
				}
			} else {
				fn.call(this);
			}
		}
		return this;
	},

	fxWrap : function(pos, o, vis) {
		var wrap;
		if (!o.wrap || !(wrap = Tera.get(o.wrap))) {
			var wrapXY;
			if (o.fixPosition) {
				wrapXY = this.getXY();
			}
			var div = document.createElement("div");
			div.style.visibility = vis;
			wrap = Tera.get(this.dom.parentNode.insertBefore(div, this.dom));
			wrap.setPositioning(pos);
			if (wrap.getStyle("position") == "static") {
				wrap.position("relative");
			}
			this.clearPositioning('auto');
			wrap.clip();
			wrap.dom.appendChild(this.dom);
			if (wrapXY) {
				wrap.setXY(wrapXY);
			}
		}
		return wrap;
	},

	fxUnwrap : function(wrap, pos, o) {
		this.clearPositioning();
		this.setPositioning(pos);
		if (!o.wrap) {
			wrap.dom.parentNode.insertBefore(this.dom, wrap.dom);
			wrap.remove();
		}
	},

	getFxRestore : function() {
		var st = this.dom.style;
		return {
			pos :this.getPositioning(),
			width :st.width,
			height :st.height
		};
	},

	afterFx : function(o) {
		if (o.afterStyle) {
			this.applyStyles(o.afterStyle);
		}
		if (o.afterCls) {
			this.addClass(o.afterCls);
		}
		if (o.remove === true) {
			this.remove();
		}
		Tera.callback(o.callback, o.scope, [ this ]);
		if (!o.concurrent) {
			this.fxQueue.shift();
			this.nextFx();
		}
	},

	getFxEl : function() {
		return Tera.get(this.dom);
	},

	fxanim : function(args, opt, animType, defaultDur, defaultEase, cb) {
		animType = animType || 'run';
		opt = opt || {};
		var anim = Tera.lib.Anim[animType](this.dom, args, (opt.duration || defaultDur) || .35, (opt.easing || defaultEase) || 'easeOut', function() {
			Tera.callback(cb, this);
		}, this);
		opt.anim = anim;
		return anim;
	}
};

Tera.Fx.resize = Tera.Fx.scale;

Tera.apply(Tera.Element.prototype, Tera.Fx);

Tera.CompositeElement = function(els) {
	this.elements = [];
	this.addElements(els);
};
Tera.CompositeElement.prototype = {
	isComposite :true,
	addElements : function(els) {
		if (!els)
			return this;
		if (typeof els == "string") {
			els = Tera.Element.selectorFunction(els);
		}
		var yels = this.elements;
		var index = yels.length - 1;
		for ( var i = 0, len = els.length; i < len; i++) {
			yels[++index] = Tera.get(els[i]);
		}
		return this;
	},

	fill : function(els) {
		this.elements = [];
		this.add(els);
		return this;
	},

	filter : function(selector) {
		var els = [];
		this.each( function(el) {
			if (el.is(selector)) {
				els[els.length] = el.dom;
			}
		});
		this.fill(els);
		return this;
	},

	invoke : function(fn, args) {
		var els = this.elements;
		for ( var i = 0, len = els.length; i < len; i++) {
			Tera.Element.prototype[fn].apply(els[i], args);
		}
		return this;
	},

	add : function(els) {
		if (typeof els == "string") {
			this.addElements(Tera.Element.selectorFunction(els));
		} else if (els.length !== undefined) {
			this.addElements(els);
		} else {
			this.addElements( [ els ]);
		}
		return this;
	},

	each : function(fn, scope) {
		var els = this.elements;
		for ( var i = 0, len = els.length; i < len; i++) {
			if (fn.call(scope || els[i], els[i], this, i) === false) {
				break;
			}
		}
		return this;
	},

	item : function(index) {
		return this.elements[index] || null;
	},

	first : function() {
		return this.item(0);
	},

	last : function() {
		return this.item(this.elements.length - 1);
	},

	getCount : function() {
		return this.elements.length;
	},

	contains : function(el) {
		return this.indexOf(el) !== -1;
	},

	indexOf : function(el) {
		return this.elements.indexOf(Tera.get(el));
	},

	removeElement : function(el, removeDom) {
		if (Tera.isArray(el)) {
			for ( var i = 0, len = el.length; i < len; i++) {
				this.removeElement(el[i]);
			}
			return this;
		}
		var index = typeof el == 'number' ? el : this.indexOf(el);
		if (index !== -1 && this.elements[index]) {
			if (removeDom) {
				var d = this.elements[index];
				if (d.dom) {
					d.remove();
				} else {
					Tera.removeNode(d);
				}
			}
			this.elements.splice(index, 1);
		}
		return this;
	},

	replaceElement : function(el, replacement, domReplace) {
		var index = typeof el == 'number' ? el : this.indexOf(el);
		if (index !== -1) {
			if (domReplace) {
				this.elements[index].replaceWith(replacement);
			} else {
				this.elements.splice(index, 1, Tera.get(replacement))
			}
		}
		return this;
	},

	clear : function() {
		this.elements = [];
	}
};
( function() {
	Tera.CompositeElement.createCall = function(proto, fnName) {
		if (!proto[fnName]) {
			proto[fnName] = function() {
				return this.invoke(fnName, arguments);
			};
		}
	};
	for ( var fnName in Tera.Element.prototype) {
		if (typeof Tera.Element.prototype[fnName] == "function") {
			Tera.CompositeElement.createCall(Tera.CompositeElement.prototype, fnName);
		}
	}
	;
})();

Tera.CompositeElementLite = function(els) {
	Tera.CompositeElementLite.superclass.constructor.call(this, els);
	this.el = new Tera.Element.Flyweight();
};
Tera.extend(Tera.CompositeElementLite, Tera.CompositeElement, {
	addElements : function(els) {
		if (els) {
			if (Tera.isArray(els)) {
				this.elements = this.elements.concat(els);
			} else {
				var yels = this.elements;
				var index = yels.length - 1;
				for ( var i = 0, len = els.length; i < len; i++) {
					yels[++index] = els[i];
				}
			}
		}
		return this;
	},
	invoke : function(fn, args) {
		var els = this.elements;
		var el = this.el;
		for ( var i = 0, len = els.length; i < len; i++) {
			el.dom = els[i];
			Tera.Element.prototype[fn].apply(el, args);
		}
		return this;
	},

	item : function(index) {
		if (!this.elements[index]) {
			return null;
		}
		this.el.dom = this.elements[index];
		return this.el;
	},

	addListener : function(eventName, handler, scope, opt) {
		var els = this.elements;
		for ( var i = 0, len = els.length; i < len; i++) {
			Tera.EventManager.on(els[i], eventName, handler, scope || els[i], opt);
		}
		return this;
	},

	each : function(fn, scope) {
		var els = this.elements;
		var el = this.el;
		for ( var i = 0, len = els.length; i < len; i++) {
			el.dom = els[i];
			if (fn.call(scope || el, el, this, i) === false) {
				break;
			}
		}
		return this;
	},

	indexOf : function(el) {
		return this.elements.indexOf(Tera.getDom(el));
	},

	replaceElement : function(el, replacement, domReplace) {
		var index = typeof el == 'number' ? el : this.indexOf(el);
		if (index !== -1) {
			replacement = Tera.getDom(replacement);
			if (domReplace) {
				var d = this.elements[index];
				d.parentNode.insertBefore(replacement, d);
				Tera.removeNode(d);
			}
			this.elements.splice(index, 1, replacement);
		}
		return this;
	}
});
Tera.CompositeElementLite.prototype.on = Tera.CompositeElementLite.prototype.addListener;
if (Tera.DomQuery) {
	Tera.Element.selectorFunction = Tera.DomQuery.select;
}

Tera.Element.select = function(selector, unique, root) {
	var els;
	if (typeof selector == "string") {
		els = Tera.Element.selectorFunction(selector, root);
	} else if (selector.length !== undefined) {
		els = selector;
	} else {
		throw "Invalid selector";
	}
	if (unique === true) {
		return new Tera.CompositeElement(els);
	} else {
		return new Tera.CompositeElementLite(els);
	}
};

Tera.select = Tera.Element.select;

Tera.data.Connection = function(config) {
	Tera.apply(this, config);
	this.addEvents(

	"beforerequest",

	"requestcomplete",

	"requestexception");
	Tera.data.Connection.superclass.constructor.call(this);
};

Tera.extend(Tera.data.Connection, Tera.util.Observable, {

	//timeout : 300000,
	timeout : 1800000,

	autoAbort :false,

	disableCaching :true,

	request : function(o) {
		if (this.fireEvent("beforerequest", this, o) !== false) {
			var p = o.params;

			if (typeof p == "function") {
				p = p.call(o.scope || window, o);
			}
			if (typeof p == "object") {
				p = Tera.urlEncode(p);
			}
			if (this.extraParams) {
				var extras = Tera.urlEncode(this.extraParams);
				p = p ? (p + '&' + extras) : extras;
			}

			var url = o.url || this.url;
			if (typeof url == 'function') {
				url = url.call(o.scope || window, o);
			}

			if (o.form) {
				var form = Tera.getDom(o.form);
				url = url || form.action;

				var enctype = form.getAttribute("enctype");
				if (o.isUpload || (enctype && enctype.toLowerCase() == 'multipart/form-data')) {
					return this.doFormUpload(o, p, url);
				}
				var f = Tera.lib.Ajax.serializeForm(form);
				p = p ? (p + '&' + f) : f;
			}

			var hs = o.headers;
			if (this.defaultHeaders) {
				hs = Tera.apply(hs || {}, this.defaultHeaders);
				if (!o.headers) {
					o.headers = hs;
				}
			}

			var cb = {
				success :this.handleResponse,
				failure :this.handleFailure,
				scope :this,
				argument : {
					options :o
				},
				timeout :o.timeout || this.timeout
			};

			var method = o.method || this.method || (p ? "POST" : "GET");

			if (method == 'GET' && (this.disableCaching && o.disableCaching !== false) || o.disableCaching === true) {
				url += (url.indexOf('?') != -1 ? '&' : '?') + '_dc=' + (new Date().getTime());
			}

			if (typeof o.autoAbort == 'boolean') {
				if (o.autoAbort) {
					this.abort();
				}
			} else if (this.autoAbort !== false) {
				this.abort();
			}
			if ((method == 'GET' || o.xmlData || o.jsonData) && p) {
				url += (url.indexOf('?') != -1 ? '&' : '?') + p;
				p = '';
			}
			this.transId = Tera.lib.Ajax.request(method, url, cb, p, o);
			return this.transId;
		} else {
			Tera.callback(o.callback, o.scope, [ o, null, null ]);
			return null;
		}
	},

	isLoading : function(transId) {
		if (transId) {
			return Tera.lib.Ajax.isCallInProgress(transId);
		} else {
			return this.transId ? true : false;
		}
	},

	abort : function(transId) {
		if (transId || this.isLoading()) {
			Tera.lib.Ajax.abort(transId || this.transId);
		}
	},

	handleResponse : function(response) {
		this.transId = false;
		var options = response.argument.options;
		response.argument = options ? options.argument : null;
		this.fireEvent("requestcomplete", this, response, options);
		Tera.callback(options.success, options.scope, [ response, options ]);
		Tera.callback(options.callback, options.scope, [ options, true, response ]);
	},

	handleFailure : function(response, e) {
		this.transId = false;
		var options = response.argument.options;
		response.argument = options ? options.argument : null;
		this.fireEvent("requestexception", this, response, options, e);
		Tera.callback(options.failure, options.scope, [ response, options ]);
		Tera.callback(options.callback, options.scope, [ options, false, response ]);
	},

	doFormUpload : function(o, ps, url) {
		var id = Tera.id();
		var frame = document.createElement('iframe');
		frame.id = id;
		frame.name = id;
		frame.className = 'x-hidden';
		if (Tera.isIE) {
			frame.src = Tera.SSL_SECURE_URL;
		}
		document.body.appendChild(frame);

		if (Tera.isIE) {
			document.frames[id].name = id;
		}

		var form = Tera.getDom(o.form);
		form.target = id;
		form.method = 'POST';
		form.enctype = form.encoding = 'multipart/form-data';
		if (url) {
			form.action = url;
		}

		var hiddens, hd;
		if (ps) {
			hiddens = [];
			ps = Tera.urlDecode(ps, false);
			for ( var k in ps) {
				if (ps.hasOwnProperty(k)) {
					hd = document.createElement('input');
					hd.type = 'hidden';
					hd.name = k;
					hd.value = ps[k];
					form.appendChild(hd);
					hiddens.push(hd);
				}
			}
		}

		function cb() {
			var r = {
				responseText :'',
				responseXML :null
			};

			r.argument = o ? o.argument : null;

			try {
				var doc;
				if (Tera.isIE) {
					doc = frame.contentWindow.document;
				} else {
					doc = (frame.contentDocument || window.frames[id].document);
				}
				if (doc && doc.body) {
					r.responseText = doc.body.innerHTML;
				}
				if (doc && doc.XMLDocument) {
					r.responseXML = doc.XMLDocument;
				} else {
					r.responseXML = doc;
				}
			} catch (e) {
			}

			Tera.EventManager.removeListener(frame, 'load', cb, this);

			this.fireEvent("requestcomplete", this, r, o);

			Tera.callback(o.success, o.scope, [ r, o ]);
			Tera.callback(o.callback, o.scope, [ o, true, r ]);

			setTimeout( function() {
				Tera.removeNode(frame);
			}, 100);
		}

		Tera.EventManager.on(frame, 'load', cb, this);
		form.submit();

		if (hiddens) {
			for ( var i = 0, len = hiddens.length; i < len; i++) {
				Tera.removeNode(hiddens[i]);
			}
		}
	}
});

Tera.Ajax = new Tera.data.Connection( {

	autoAbort :false,

	serializeForm : function(form) {
		return Tera.lib.Ajax.serializeForm(form);
	}
});

Tera.Updater = function(el, forceNew) {
	el = Tera.get(el);
	if (!forceNew && el.updateManager) {
		return el.updateManager;
	}

	this.el = el;

	this.defaultUrl = null;

	this.addEvents(

	"beforeupdate",

	"update",

	"failure");
	var d = Tera.Updater.defaults;

	this.sslBlankUrl = d.sslBlankUrl;

	this.disableCaching = d.disableCaching;

	this.indicatorText = d.indicatorText;

	this.showLoadIndicator = d.showLoadIndicator;

	this.timeout = d.timeout;

	this.loadScripts = d.loadScripts;

	this.transaction = null;

	this.refreshDelegate = this.refresh.createDelegate(this);

	this.updateDelegate = this.update.createDelegate(this);

	this.formUpdateDelegate = this.formUpdate.createDelegate(this);

	if (!this.renderer) {

		this.renderer = new Tera.Updater.BasicRenderer();
	}
	Tera.Updater.superclass.constructor.call(this);
};

Tera.extend(Tera.Updater, Tera.util.Observable, {

	getEl : function() {
		return this.el;
	},

	update : function(url, params, callback, discardUrl) {
		if (this.fireEvent("beforeupdate", this.el, url, params) !== false) {
			var cfg, callerScope;
			if (typeof url == "object") {
				cfg = url;
				url = cfg.url;
				params = params || cfg.params;
				callback = callback || cfg.callback;
				discardUrl = discardUrl || cfg.discardUrl;
				callerScope = cfg.scope;
				if (typeof cfg.nocache != "undefined") {
					this.disableCaching = cfg.nocache;
				}
				;
				if (typeof cfg.text != "undefined") {
					this.indicatorText = '<div class="loading-indicator">' + cfg.text + "</div>";
				}
				;
				if (typeof cfg.scripts != "undefined") {
					this.loadScripts = cfg.scripts;
				}
				;
				if (typeof cfg.timeout != "undefined") {
					this.timeout = cfg.timeout;
				}
				;
			}
			this.showLoading();

			if (!discardUrl) {
				this.defaultUrl = url;
			}
			if (typeof url == "function") {
				url = url.call(this);
			}

			var o = Tera.apply(cfg || {}, {
				url :url,
				params :(typeof params == "function" && callerScope) ? params.createDelegate(callerScope) : params,
				success :this.processSuccess,
				failure :this.processFailure,
				scope :this,
				callback :undefined,
				timeout :(this.timeout * 1000),
				disableCaching :this.disableCaching,
				argument : {
					"options" :cfg,
					"url" :url,
					"form" :null,
					"callback" :callback,
					"scope" :callerScope || window,
					"params" :params
				}
			});

			this.transaction = Tera.Ajax.request(o);
		}
	},

	formUpdate : function(form, url, reset, callback) {
		if (this.fireEvent("beforeupdate", this.el, form, url) !== false) {
			if (typeof url == "function") {
				url = url.call(this);
			}
			form = Tera.getDom(form)
			this.transaction = Tera.Ajax.request( {
				form :form,
				url :url,
				success :this.processSuccess,
				failure :this.processFailure,
				scope :this,
				timeout :(this.timeout * 1000),
				argument : {
					"url" :url,
					"form" :form,
					"callback" :callback,
					"reset" :reset
				}
			});
			this.showLoading.defer(1, this);
		}
	},

	refresh : function(callback) {
		if (this.defaultUrl == null) {
			return;
		}
		this.update(this.defaultUrl, null, callback, true);
	},

	startAutoRefresh : function(interval, url, params, callback, refreshNow) {
		if (refreshNow) {
			this.update(url || this.defaultUrl, params, callback, true);
		}
		if (this.autoRefreshProcId) {
			clearInterval(this.autoRefreshProcId);
		}
		this.autoRefreshProcId = setInterval(this.update.createDelegate(this, [ url || this.defaultUrl, params, callback, true ]), interval * 1000);
	},

	stopAutoRefresh : function() {
		if (this.autoRefreshProcId) {
			clearInterval(this.autoRefreshProcId);
			delete this.autoRefreshProcId;
		}
	},

	isAutoRefreshing : function() {
		return this.autoRefreshProcId ? true : false;
	},

	showLoading : function() {
		if (this.showLoadIndicator) {
			this.el.update(this.indicatorText);
		}
	},

	processSuccess : function(response) {
		this.transaction = null;
		if (response.argument.form && response.argument.reset) {
			try {
				response.argument.form.reset();
			} catch (e) {
			}
		}
		if (this.loadScripts) {
			this.renderer.render(this.el, response, this, this.updateComplete.createDelegate(this, [ response ]));
		} else {
			this.renderer.render(this.el, response, this);
			this.updateComplete(response);
		}
	},

	updateComplete : function(response) {
		this.fireEvent("update", this.el, response);
		if (typeof response.argument.callback == "function") {
			response.argument.callback.call(response.argument.scope, this.el, true, response, response.argument.options);
		}
	},

	processFailure : function(response) {
		this.transaction = null;
		this.fireEvent("failure", this.el, response);
		if (typeof response.argument.callback == "function") {
			response.argument.callback.call(response.argument.scope, this.el, false, response, response.argument.options);
		}
	},

	setRenderer : function(renderer) {
		this.renderer = renderer;
	},

	getRenderer : function() {
		return this.renderer;
	},

	setDefaultUrl : function(defaultUrl) {
		this.defaultUrl = defaultUrl;
	},

	abort : function() {
		if (this.transaction) {
			Tera.Ajax.abort(this.transaction);
		}
	},

	isUpdating : function() {
		if (this.transaction) {
			return Tera.Ajax.isLoading(this.transaction);
		}
		return false;
	}
});

Tera.Updater.defaults = {

	timeout :30,

	loadScripts :false,

	sslBlankUrl :(Tera.SSL_SECURE_URL || "javascript:false"),

	disableCaching :false,

	showLoadIndicator :true,

	indicatorText :'<div class="loading-indicator">Loading...</div>'
};

Tera.Updater.updateElement = function(el, url, params, options) {
	var um = Tera.get(el).getUpdater();
	Tera.apply(um, options);
	um.update(url, params, options ? options.callback : null);
};

Tera.Updater.BasicRenderer = function() {
};

Tera.Updater.BasicRenderer.prototype = {

	render : function(el, response, updateManager, callback) {
		el.update(response.responseText, updateManager.loadScripts, callback);
	}
};

Tera.UpdateManager = Tera.Updater;

Date.parseFunctions = {
	count :0
};
Date.parseRegexes = [];
Date.formatFunctions = {
	count :0
};

Date.prototype.dateFormat = function(format) {
	if (Date.formatFunctions[format] == null) {
		Date.createNewFormat(format);
	}
	var func = Date.formatFunctions[format];
	return this[func]();
};

Date.prototype.format = Date.prototype.dateFormat;

Date.createNewFormat = function(format) {
	var funcName = "format" + Date.formatFunctions.count++;
	Date.formatFunctions[format] = funcName;
	var code = "Date.prototype." + funcName + " = function(){return ";
	var special = false;
	var ch = '';
	for ( var i = 0; i < format.length; ++i) {
		ch = format.charAt(i);
		if (!special && ch == "\\") {
			special = true;
		} else if (special) {
			special = false;
			code += "'" + String.escape(ch) + "' + ";
		} else {
			code += Date.getFormatCode(ch) + " + ";
		}
	}
	eval(code.substring(0, code.length - 3) + ";}");
};

Date.formatCodes = {
	d :"String.leftPad(this.getDate(), 2, '0')",
	D :"Date.getShortDayName(this.getDay())",
	j :"this.getDate()",
	l :"Date.dayNames[this.getDay()]",
	N :"(this.getDay() ? this.getDay() : 7)",
	S :"this.getSuffix()",
	w :"this.getDay()",
	z :"this.getDayOfYear()",
	W :"String.leftPad(this.getWeekOfYear(), 2, '0')",
	F :"Date.monthNames[this.getMonth()]",
	m :"String.leftPad(this.getMonth() + 1, 2, '0')",
	M :"Date.getShortMonthName(this.getMonth())",
	n :"(this.getMonth() + 1)",
	t :"this.getDaysInMonth()",
	L :"(this.isLeapYear() ? 1 : 0)",
	o :"(this.getFullYear() + (this.getWeekOfYear() == 1 && this.getMonth() > 0 ? +1 : (this.getWeekOfYear() >= 52 && this.getMonth() < 11 ? -1 : 0)))",
	Y :"this.getFullYear()",
	y :"('' + this.getFullYear()).substring(2, 4)",
	a :"(this.getHours() < 12 ? 'am' : 'pm')",
	A :"(this.getHours() < 12 ? 'AM' : 'PM')",
	g :"((this.getHours() % 12) ? this.getHours() % 12 : 12)",
	G :"this.getHours()",
	h :"String.leftPad((this.getHours() % 12) ? this.getHours() % 12 : 12, 2, '0')",
	H :"String.leftPad(this.getHours(), 2, '0')",
	i :"String.leftPad(this.getMinutes(), 2, '0')",
	s :"String.leftPad(this.getSeconds(), 2, '0')",
	u :"String.leftPad(this.getMilliseconds(), 3, '0')",
	O :"this.getGMTOffset()",
	P :"this.getGMTOffset(true)",
	T :"this.getTimezone()",
	Z :"(this.getTimezoneOffset() * -60)",
	c : function() {
		for ( var c = "Y-m-dTH:i:sP", code = [], i = 0, l = c.length; i < l; ++i) {
			var e = c.charAt(i);
			code.push(e == "T" ? "'T'" : Date.getFormatCode(e));
		}
		return code.join(" + ");
	},

	U :"Math.round(this.getTime() / 1000)"
}

Date.getFormatCode = function(character) {
	var f = Date.formatCodes[character];

	if (f) {
		f = Tera.type(f) == 'function' ? f() : f;
		Date.formatCodes[character] = f;
	}

	return f || ("'" + String.escape(character) + "'");
};

Date.parseDate = function(input, format) {
	if (Date.parseFunctions[format] == null) {
		Date.createParser(format);
	}
	var func = Date.parseFunctions[format];
	return Date[func](input);
};

Date.createParser = function(format) {
	var funcName = "parse" + Date.parseFunctions.count++;
	var regexNum = Date.parseRegexes.length;
	var currentGroup = 1;
	Date.parseFunctions[format] = funcName;

	var code = "Date." + funcName + " = function(input){\n" + "var y = -1, m = -1, d = -1, h = -1, i = -1, s = -1, ms = -1, o, z, u, v;\n"
			+ "input = String(input);var d = new Date();\n" + "y = d.getFullYear();\n" + "m = d.getMonth();\n" + "d = d.getDate();\n"
			+ "var results = input.match(Date.parseRegexes[" + regexNum + "]);\n" + "if (results && results.length > 0) {";
	var regex = "";

	var special = false;
	var ch = '';
	for ( var i = 0; i < format.length; ++i) {
		ch = format.charAt(i);
		if (!special && ch == "\\") {
			special = true;
		} else if (special) {
			special = false;
			regex += String.escape(ch);
		} else {
			var obj = Date.formatCodeToRegex(ch, currentGroup);
			currentGroup += obj.g;
			regex += obj.s;
			if (obj.g && obj.c) {
				code += obj.c;
			}
		}
	}

	code += "if (u){\n" + "v = new Date(u * 1000);\n" + "}else if (y >= 0 && m >= 0 && d > 0 && h >= 0 && i >= 0 && s >= 0 && ms >= 0){\n"
			+ "v = new Date(y, m, d, h, i, s, ms);\n" + "}else if (y >= 0 && m >= 0 && d > 0 && h >= 0 && i >= 0 && s >= 0){\n"
			+ "v = new Date(y, m, d, h, i, s);\n" + "}else if (y >= 0 && m >= 0 && d > 0 && h >= 0 && i >= 0){\n" + "v = new Date(y, m, d, h, i);\n"
			+ "}else if (y >= 0 && m >= 0 && d > 0 && h >= 0){\n" + "v = new Date(y, m, d, h);\n" + "}else if (y >= 0 && m >= 0 && d > 0){\n"
			+ "v = new Date(y, m, d);\n" + "}else if (y >= 0 && m >= 0){\n" + "v = new Date(y, m);\n" + "}else if (y >= 0){\n" + "v = new Date(y);\n"
			+ "}\n}\nreturn (v && Tera.type(z || o) == 'number')?"
			+ " (Tera.type(z) == 'number' ? v.add(Date.SECOND, (v.getTimezoneOffset() * 60) + z) :"
			+ " v.add(Date.HOUR, (v.getGMTOffset() / 100) + (o / -100))) : v;\n" + "}";

	Date.parseRegexes[regexNum] = new RegExp("^" + regex + "$", "i");
	eval(code);
};

Date.parseCodes = {

	d : {
		g :1,
		c :"d = parseInt(results[{0}], 10);\n",
		s :"(\\d{2})"
	},
	j : function() {
		return Tera.applyIf( {
			s :"(\\d{1,2})"
		}, Date.parseCodes["d"]);
	},
	D : function() {
		for ( var a = [], i = 0; i < 7; a.push(Date.getShortDayName(i)), ++i)
			;
		return {
			g :0,
			c :null,
			s :"(?:" + a.join("|") + ")"
		}
	},
	l : function() {
		return {
			g :0,
			c :null,
			s :"(?:" + Date.dayNames.join("|") + ")"
		}
	},
	N : {
		g :0,
		c :null,
		s :"[1-7]"
	},
	S : {
		g :0,
		c :null,
		s :"(?:st|nd|rd|th)"
	},
	w : {
		g :0,
		c :null,
		s :"[0-6]"
	},
	z : {
		g :0,
		c :null,
		s :"(?:\\d{1,3}"
	},
	W : {
		g :0,
		c :null,
		s :"(?:\\d{2})"
	},
	F : function() {
		return {
			g :1,
			c :"m = parseInt(Date.getMonthNumber(results[{0}]), 10);\n",
			s :"(" + Date.monthNames.join("|") + ")"
		}
	},
	M : function() {
		for ( var a = [], i = 0; i < 12; a.push(Date.getShortMonthName(i)), ++i)
			;
		return Tera.applyIf( {
			s :"(" + a.join("|") + ")"
		}, Date.parseCodes["F"]);
	},
	m : {
		g :1,
		c :"m = parseInt(results[{0}], 10) - 1;\n",
		s :"(\\d{2})"
	},
	n : function() {
		return Tera.applyIf( {
			s :"(\\d{1,2})"
		}, Date.parseCodes["m"]);
	},
	t : {
		g :0,
		c :null,
		s :"(?:\\d{2})"
	},
	L : {
		g :0,
		c :null,
		s :"(?:1|0)"
	},
	o : function() {
		return Date.parseCodes["Y"];
	},
	Y : {
		g :1,
		c :"y = parseInt(results[{0}], 10);\n",
		s :"(\\d{4})"
	},
	y : {
		g :1,
		c :"var ty = parseInt(results[{0}], 10);\n" + "y = ty > Date.y2kYear ? 1900 + ty : 2000 + ty;\n",
		s :"(\\d{1,2})"
	},
	a : {
		g :1,
		c :"if (results[{0}] == 'am') {\n" + "if (h == 12) { h = 0; }\n" + "} else { if (h < 12) { h += 12; }}",
		s :"(am|pm)"
	},
	A : {
		g :1,
		c :"if (results[{0}] == 'AM') {\n" + "if (h == 12) { h = 0; }\n" + "} else { if (h < 12) { h += 12; }}",
		s :"(AM|PM)"
	},
	g : function() {
		return Date.parseCodes["G"];
	},
	G : {
		g :1,
		c :"h = parseInt(results[{0}], 10);\n",
		s :"(\\d{1,2})"
	},
	h : function() {
		return Date.parseCodes["H"];
	},
	H : {
		g :1,
		c :"h = parseInt(results[{0}], 10);\n",
		s :"(\\d{2})"
	},
	i : {
		g :1,
		c :"i = parseInt(results[{0}], 10);\n",
		s :"(\\d{2})"
	},
	s : {
		g :1,
		c :"s = parseInt(results[{0}], 10);\n",
		s :"(\\d{2})"
	},
	u : {
		g :1,
		c :"ms = parseInt(results[{0}], 10);\n",
		s :"(\\d{3})"
	},
	O : {
		g :1,
		c : [ "o = results[{0}];", "var sn = o.substring(0,1);", "var hr = o.substring(1,3)*1 + Math.floor(o.substring(3,5) / 60);",
				"var mn = o.substring(3,5) % 60;",
				"o = ((-12 <= (hr*60 + mn)/60) && ((hr*60 + mn)/60 <= 14))? (sn + String.leftPad(hr, 2, '0') + String.leftPad(mn, 2, '0')) : null;\n" ]
				.join("\n"),
		s :"([+\-]\\d{4})"
	},
	P : function() {
		return Tera.applyIf( {
			s :"([+\-]\\d{2}:\\d{2})"
		}, Date.parseCodes["O"]);
	},
	T : {
		g :0,
		c :null,
		s :"[A-Z]{1,4}"
	},
	Z : {
		g :1,
		c :"z = results[{0}] * 1;\n" + "z = (-43200 <= z && z <= 50400)? z : null;\n",
		s :"([+\-]?\\d{1,5})"
	},
	c : function() {
		var df = Date.formatCodeToRegex, calc = [];
		var arr = [ df("Y", 1), df("m", 2), df("d", 3), df("h", 4), df("i", 5), df("s", 6), {
			c :"if(results[7] == 'Z'){\no = 0;\n}else{\n" + df("P", 7).c + "\n}"
		} ];
		for ( var i = 0, l = arr.length; i < l; ++i) {
			calc.push(arr[i].c);
		}

		return {
			g :1,
			c :calc.join(""),
			s :arr[0].s + "-" + arr[1].s + "-" + arr[2].s + "T" + arr[3].s + ":" + arr[4].s + ":" + arr[5].s + "(" + df("P", 7).s + "|Z)"
		}
	},
	U : {
		g :1,
		c :"u = parseInt(results[{0}], 10);\n",
		s :"(-?\\d+)"
	}
}

Date.formatCodeToRegex = function(character, currentGroup) {
	var p = Date.parseCodes[character];

	if (p) {
		p = Tera.type(p) == 'function' ? p() : p;
		Date.parseCodes[character] = p;
		if (p.c) {
			p.c = String.format(p.c, currentGroup);
		}
	}

	return p || {
		g :0,
		c :null,
		s :Tera.escapeRe(character)
	}
};

Date.prototype.getTimezone = function() {
	return this.toString().replace(/^.* (?:\((.*)\)|([A-Z]{1,4})(?:[\-+][0-9]{4})?(?: -?\d+)?)$/, "$1$2").replace(/[^A-Z]/g, "");
};

Date.prototype.getGMTOffset = function(colon) {
	return (this.getTimezoneOffset() > 0 ? "-" : "+") + String.leftPad(Math.abs(Math.floor(this.getTimezoneOffset() / 60)), 2, "0")
			+ (colon ? ":" : "") + String.leftPad(this.getTimezoneOffset() % 60, 2, "0");
};

Date.prototype.getDayOfYear = function() {
	var num = 0;
	Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
	for ( var i = 0; i < this.getMonth(); ++i) {
		num += Date.daysInMonth[i];
	}
	return num + this.getDate() - 1;
};

Date.prototype.getWeekOfYear = function() {
	var ms1d = 864e5;
	var ms7d = 7 * ms1d;
	var DC3 = Date.UTC(this.getFullYear(), this.getMonth(), this.getDate() + 3) / ms1d;
	var AWN = Math.floor(DC3 / 7);
	var Wyr = new Date(AWN * ms7d).getUTCFullYear();
	return AWN - Math.floor(Date.UTC(Wyr, 0, 7) / ms7d) + 1;
};

Date.prototype.isLeapYear = function() {
	var year = this.getFullYear();
	return !!((year & 3) == 0 && (year % 100 || (year % 400 == 0 && year)));
};

Date.prototype.getFirstDayOfMonth = function() {
	var day = (this.getDay() - (this.getDate() - 1)) % 7;
	return (day < 0) ? (day + 7) : day;
};

Date.prototype.getLastDayOfMonth = function() {
	var day = (this.getDay() + (Date.daysInMonth[this.getMonth()] - this.getDate())) % 7;
	return (day < 0) ? (day + 7) : day;
};

Date.prototype.getFirstDateOfMonth = function() {
	return new Date(this.getFullYear(), this.getMonth(), 1);
};

Date.prototype.getLastDateOfMonth = function() {
	return new Date(this.getFullYear(), this.getMonth(), this.getDaysInMonth());
};

Date.prototype.getDaysInMonth = function() {
	Date.daysInMonth[1] = this.isLeapYear() ? 29 : 28;
	return Date.daysInMonth[this.getMonth()];
};

Date.prototype.getSuffix = function() {
	switch (this.getDate()) {
		case 1:
		case 21:
		case 31:
			return "st";
		case 2:
		case 22:
			return "nd";
		case 3:
		case 23:
			return "rd";
		default:
			return "th";
	}
};

Date.prototype.compareDates = function(firstDate, secondDate, format, clearTime) {
	var d1, d2;
	var usedFormat;
	if (!Tera.isEmpty(format)) {
		usedFormat = 'd/m/Y';
	}	
	if (Tera.type(firstDate)=='string') {
		d1 = Date.parseDate(firstDate, usedFormat);
	}
	if (Tera.type(secondDate)=='string') {
		d2 = Date.parseDate(secondDate, usedFormat);
	}
	
	d1 = firstDate.format(format);
	d2 = secondDate.format(format);
	
	if (Tera.isEmpty(clearTime)) {
		d1 = d1.clearTime();
		d2 = d2.clearTime();
	}
	return (firstDate>secondDate);
}

Date.daysInMonth = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

Date.monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

Date.getShortMonthName = function(month) {
	return Date.monthNames[month].substring(0, 3);
}

Date.dayNames = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];

Date.getShortDayName = function(day) {
	return Date.dayNames[day].substring(0, 3);
}

Date.y2kYear = 50;

Date.monthNumbers = {
	Jan :0,
	Feb :1,
	Mar :2,
	Apr :3,
	May :4,
	Jun :5,
	Jul :6,
	Aug :7,
	Sep :8,
	Oct :9,
	Nov :10,
	Dec :11
};

Date.getMonthNumber = function(name) {
	return Date.monthNumbers[name.substring(0, 1).toUpperCase() + name.substring(1, 3).toLowerCase()];
}

Date.prototype.clone = function() {
	return new Date(this.getTime());
};

Date.prototype.clearTime = function(clone) {
	if (clone) {
		return this.clone().clearTime();
	}
	this.setHours(0);
	this.setMinutes(0);
	this.setSeconds(0);
	this.setMilliseconds(0);
	return this;
};

if (Tera.isSafari) {
	Date.brokenSetMonth = Date.prototype.setMonth;
	Date.prototype.setMonth = function(num) {
		if (num <= -1) {
			var n = Math.ceil(-num);
			var back_year = Math.ceil(n / 12);
			var month = (n % 12) ? 12 - n % 12 : 0;
			this.setFullYear(this.getFullYear() - back_year);
			return Date.brokenSetMonth.call(this, month);
		} else {
			return Date.brokenSetMonth.apply(this, arguments);
		}
	};
}

Date.MILLI = "ms";

Date.SECOND = "s";

Date.MINUTE = "mi";

Date.HOUR = "h";

Date.DAY = "d";

Date.MONTH = "mo";

Date.YEAR = "y";

Date.prototype.add = function(interval, value) {
	var d = this.clone();
	if (!interval || value === 0)
		return d;
	switch (interval.toLowerCase()) {
		case Date.MILLI:
			d.setMilliseconds(this.getMilliseconds() + value);
			break;
		case Date.SECOND:
			d.setSeconds(this.getSeconds() + value);
			break;
		case Date.MINUTE:
			d.setMinutes(this.getMinutes() + value);
			break;
		case Date.HOUR:
			d.setHours(this.getHours() + value);
			break;
		case Date.DAY:
			d.setDate(this.getDate() + value);
			break;
		case Date.MONTH:
			var day = this.getDate();
			if (day > 28) {
				day = Math.min(day, this.getFirstDateOfMonth().add('mo', value).getLastDateOfMonth().getDate());
			}
			d.setDate(day);
			d.setMonth(this.getMonth() + value);
			break;
		case Date.YEAR:
			d.setFullYear(this.getFullYear() + value);
			break;
	}
	return d;
};

Date.prototype.between = function(start, end) {
	var t = this.getTime();
	return start.getTime() <= t && t <= end.getTime();
}

Tera.util.DelayedTask = function(fn, scope, args) {
	var id = null, d, t;

	var call = function() {
		var now = new Date().getTime();
		if (now - t >= d) {
			clearInterval(id);
			id = null;
			fn.apply(scope, args || []);
		}
	};

	this.delay = function(delay, newFn, newScope, newArgs) {
		if (id && delay != d) {
			this.cancel();
		}
		d = delay;
		t = new Date().getTime();
		fn = newFn || fn;
		scope = newScope || scope;
		args = newArgs || args;
		if (!id) {
			id = setInterval(call, d);
		}
	};

	this.cancel = function() {
		if (id) {
			clearInterval(id);
			id = null;
		}
	};
};

Tera.util.TaskRunner = function(interval) {
	interval = interval || 10;
	var tasks = [], removeQueue = [];
	var id = 0;
	var running = false;

	var stopThread = function() {
		running = false;
		clearInterval(id);
		id = 0;
	};

	var startThread = function() {
		if (!running) {
			running = true;
			id = setInterval(runTasks, interval);
		}
	};

	var removeTask = function(t) {
		removeQueue.push(t);
		if (t.onStop) {
			t.onStop.apply(t.scope || t);
		}
	};

	var runTasks = function() {
		if (removeQueue.length > 0) {
			for ( var i = 0, len = removeQueue.length; i < len; i++) {
				tasks.remove(removeQueue[i]);
			}
			removeQueue = [];
			if (tasks.length < 1) {
				stopThread();
				return;
			}
		}
		var now = new Date().getTime();
		for ( var i = 0, len = tasks.length; i < len; ++i) {
			var t = tasks[i];
			var itime = now - t.taskRunTime;
			if (t.interval <= itime) {
				var rt = t.run.apply(t.scope || t, t.args || [ ++t.taskRunCount ]);
				t.taskRunTime = now;
				if (rt === false || t.taskRunCount === t.repeat) {
					removeTask(t);
					return;
				}
			}
			if (t.duration && t.duration <= (now - t.taskStartTime)) {
				removeTask(t);
			}
		}
	};

	this.start = function(task) {
		tasks.push(task);
		task.taskStartTime = new Date().getTime();
		task.taskRunTime = 0;
		task.taskRunCount = 0;
		startThread();
		return task;
	};

	this.stop = function(task) {
		removeTask(task);
		return task;
	};

	this.stopAll = function() {
		stopThread();
		for ( var i = 0, len = tasks.length; i < len; i++) {
			if (tasks[i].onStop) {
				tasks[i].onStop();
			}
		}
		tasks = [];
		removeQueue = [];
	};
};

Tera.TaskMgr = new Tera.util.TaskRunner();

Tera.util.MixedCollection = function(allowFunctions, keyFn) {
	this.items = [];
	this.map = {};
	this.keys = [];
	this.length = 0;
	this.addEvents(

	"clear",

	"add",

	"replace",

	"remove", "sort");
	this.allowFunctions = allowFunctions === true;
	if (keyFn) {
		this.getKey = keyFn;
	}
	Tera.util.MixedCollection.superclass.constructor.call(this);
};

Tera.extend(Tera.util.MixedCollection, Tera.util.Observable, {
	allowFunctions :false,

	add : function(key, o) {
		if (arguments.length == 1) {
			o = arguments[0];
			key = this.getKey(o);
		}
		if (typeof key == "undefined" || key === null) {
			this.length++;
			this.items.push(o);
			this.keys.push(null);
		} else {
			var old = this.map[key];
			if (old) {
				return this.replace(key, o);
			}
			this.length++;
			this.items.push(o);
			this.map[key] = o;
			this.keys.push(key);
		}
		this.fireEvent("add", this.length - 1, o, key);
		return o;
	},

	getKey : function(o) {
		return o.id;
	},

	replace : function(key, o) {
		if (arguments.length == 1) {
			o = arguments[0];
			key = this.getKey(o);
		}
		var old = this.item(key);
		if (typeof key == "undefined" || key === null || typeof old == "undefined") {
			return this.add(key, o);
		}
		var index = this.indexOfKey(key);
		this.items[index] = o;
		this.map[key] = o;
		this.fireEvent("replace", key, old, o);
		return o;
	},

	addAll : function(objs) {
		if (arguments.length > 1 || Tera.isArray(objs)) {
			var args = arguments.length > 1 ? arguments : objs;
			for ( var i = 0, len = args.length; i < len; i++) {
				this.add(args[i]);
			}
		} else {
			for ( var key in objs) {
				if (this.allowFunctions || typeof objs[key] != "function") {
					this.add(key, objs[key]);
				}
			}
		}
	},

	each : function(fn, scope) {
		var items = [].concat(this.items);
		for ( var i = 0, len = items.length; i < len; i++) {
			if (fn.call(scope || items[i], items[i], i, len) === false) {
				break;
			}
		}
	},

	eachKey : function(fn, scope) {
		for ( var i = 0, len = this.keys.length; i < len; i++) {
			fn.call(scope || window, this.keys[i], this.items[i], i, len);
		}
	},

	find : function(fn, scope) {
		for ( var i = 0, len = this.items.length; i < len; i++) {
			if (fn.call(scope || window, this.items[i], this.keys[i])) {
				return this.items[i];
			}
		}
		return null;
	},

	insert : function(index, key, o) {
		if (arguments.length == 2) {
			o = arguments[1];
			key = this.getKey(o);
		}
		if (index >= this.length) {
			return this.add(key, o);
		}
		this.length++;
		this.items.splice(index, 0, o);
		if (typeof key != "undefined" && key != null) {
			this.map[key] = o;
		}
		this.keys.splice(index, 0, key);
		this.fireEvent("add", index, o, key);
		return o;
	},

	remove : function(o) {
		return this.removeAt(this.indexOf(o));
	},

	removeAt : function(index) {
		if (index < this.length && index >= 0) {
			this.length--;
			var o = this.items[index];
			this.items.splice(index, 1);
			var key = this.keys[index];
			if (typeof key != "undefined") {
				delete this.map[key];
			}
			this.keys.splice(index, 1);
			this.fireEvent("remove", o, key);
			return o;
		}
		return false;
	},

	removeKey : function(key) {
		return this.removeAt(this.indexOfKey(key));
	},

	getCount : function() {
		return this.length;
	},

	indexOf : function(o) {
		return this.items.indexOf(o);
	},

	indexOfKey : function(key) {
		return this.keys.indexOf(key);
	},

	item : function(key) {
		var item = typeof this.map[key] != "undefined" ? this.map[key] : this.items[key];
		return typeof item != 'function' || this.allowFunctions ? item : null;
	},

	itemAt : function(index) {
		return this.items[index];
	},

	key : function(key) {
		return this.map[key];
	},

	contains : function(o) {
		return this.indexOf(o) != -1;
	},

	containsKey : function(key) {
		return typeof this.map[key] != "undefined";
	},

	clear : function() {
		this.length = 0;
		this.items = [];
		this.keys = [];
		this.map = {};
		this.fireEvent("clear");
	},

	first : function() {
		return this.items[0];
	},

	last : function() {
		return this.items[this.length - 1];
	},

	_sort : function(property, dir, fn) {
		var dsc = String(dir).toUpperCase() == "DESC" ? -1 : 1;
		fn = fn || function(a, b) {
			return a - b;
		};
		var c = [], k = this.keys, items = this.items;
		for ( var i = 0, len = items.length; i < len; i++) {
			c[c.length] = {
				key :k[i],
				value :items[i],
				index :i
			};
		}
		c.sort( function(a, b) {
			var v = fn(a[property], b[property]) * dsc;
			if (v == 0) {
				v = (a.index < b.index ? -1 : 1);
			}
			return v;
		});
		for ( var i = 0, len = c.length; i < len; i++) {
			items[i] = c[i].value;
			k[i] = c[i].key;
		}
		this.fireEvent("sort", this);
	},

	sort : function(dir, fn) {
		this._sort("value", dir, fn);
	},

	keySort : function(dir, fn) {
		this._sort("key", dir, fn || function(a, b) {
			return String(a).toUpperCase() - String(b).toUpperCase();
		});
	},

	getRange : function(start, end) {
		var items = this.items;
		if (items.length < 1) {
			return [];
		}
		start = start || 0;
		end = Math.min(typeof end == "undefined" ? this.length - 1 : end, this.length - 1);
		var r = [];
		if (start <= end) {
			for ( var i = start; i <= end; i++) {
				r[r.length] = items[i];
			}
		} else {
			for ( var i = start; i >= end; i--) {
				r[r.length] = items[i];
			}
		}
		return r;
	},

	filter : function(property, value, anyMatch, caseSensitive) {
		if (Tera.isEmpty(value, false)) {
			return this.clone();
		}
		value = this.createValueMatcher(value, anyMatch, caseSensitive);
		return this.filterBy( function(o) {
			return o && value.test(o[property]);
		});
	},

	filterBy : function(fn, scope) {
		var r = new Tera.util.MixedCollection();
		r.getKey = this.getKey;
		var k = this.keys, it = this.items;
		for ( var i = 0, len = it.length; i < len; i++) {
			if (fn.call(scope || this, it[i], k[i])) {
				r.add(k[i], it[i]);
			}
		}
		return r;
	},

	findIndex : function(property, value, start, anyMatch, caseSensitive) {
		if (Tera.isEmpty(value, false)) {
			return -1;
		}
		value = this.createValueMatcher(value, anyMatch, caseSensitive);
		return this.findIndexBy( function(o) {
			return o && value.test(o[property]);
		}, null, start);
	},

	findIndexBy : function(fn, scope, start) {
		var k = this.keys, it = this.items;
		for ( var i = (start || 0), len = it.length; i < len; i++) {
			if (fn.call(scope || this, it[i], k[i])) {
				return i;
			}
		}
		if (typeof start == 'number' && start > 0) {
			for ( var i = 0; i < start; i++) {
				if (fn.call(scope || this, it[i], k[i])) {
					return i;
				}
			}
		}
		return -1;
	},

	createValueMatcher : function(value, anyMatch, caseSensitive) {
		if (!value.exec) {
			value = String(value);
			value = new RegExp((anyMatch === true ? '' : '^') + Tera.escapeRe(value), caseSensitive ? '' : 'i');
		}
		return value;
	},

	clone : function() {
		var r = new Tera.util.MixedCollection();
		var k = this.keys, it = this.items;
		for ( var i = 0, len = it.length; i < len; i++) {
			r.add(k[i], it[i]);
		}
		r.getKey = this.getKey;
		return r;
	}
});

Tera.util.MixedCollection.prototype.get = Tera.util.MixedCollection.prototype.item;

Tera.util.JSON = new ( function() {
	var useHasOwn = !! {}.hasOwnProperty;

	var pad = function(n) {
		return n < 10 ? "0" + n : n;
	};

	var m = {
		"\b" :'\\b',
		"\t" :'\\t',
		"\n" :'\\n',
		"\f" :'\\f',
		"\r" :'\\r',
		'"' :'\\"',
		"\\" :'\\\\'
	};

	var encodeString = function(s) {
		if (/["\\\x00-\x1f]/.test(s)) {
			return '"' + s.replace(/([\x00-\x1f\\"])/g, function(a, b) {
				var c = m[b];
				if (c) {
					return c;
				}
				c = b.charCodeAt();
				return "\\u00" + Math.floor(c / 16).toString(16) + (c % 16).toString(16);
			}) + '"';
		}
		return '"' + s + '"';
	};

	var encodeArray = function(o) {
		var a = [ "[" ], b, i, l = o.length, v;
		for (i = 0; i < l; i += 1) {
			v = o[i];
			switch (typeof v) {
				case "undefined":
				case "function":
				case "unknown":
					break;
				default:
					if (b) {
						a.push(',');
					}
					a.push(v === null ? "null" : Tera.util.JSON.encode(v));
					b = true;
			}
		}
		a.push("]");
		return a.join("");
	};

	this.encodeDate = function(o,f) {
		if (!Tera.isEmpty(f)) {
			return '"' + o.format(f.date) + '"';	
		} else {
			return '"' + o.getFullYear() + "-" + pad(o.getMonth() + 1) + "-" + pad(o.getDate()) + "T" + pad(o.getHours()) + ":" + pad(o.getMinutes()) + ":" + pad(o.getSeconds()) + '"';
		}
	};

	this.encode = function(o,f) {
		if (typeof o == "undefined" || o === null) {
			return "null";
		} else if (Tera.isArray(o)) {
			return encodeArray(o);
		} else if (Tera.isDate(o)) {
			return Tera.util.JSON.encodeDate(o,f);
		} else if (typeof o == "string") {
			return encodeString(o);
		} else if (typeof o == "number") {
			return isFinite(o) ? String(o) : "null";
		} else if (typeof o == "boolean") {
			return String(o);
		} else {
			var a = [ "{" ], b, i, v;
			for (i in o) {
				if (!useHasOwn || o.hasOwnProperty(i)) {
					v = o[i];
					switch (typeof v) {
						case "undefined":
						case "function":
						case "unknown":
							break;
						default:
							if (b) {
								a.push(',');
							}
							a.push(this.encode(i,f), ":", v === null ? "null" : this.encode(v,f));
							b = true;
					}
				}
			}
			a.push("}");
			return a.join("");
		}
	};

	this.decode = function(json) {
		return eval("(" + json + ")");
	};
})();

Tera.encode = Tera.util.JSON.encode;

Tera.decode = Tera.util.JSON.decode;

Tera.util.Format = function() {
	var trimRe = /^\s+|\s+$/g;
	return {

		ellipsis : function(value, len) {
			if (value && value.length > len) {
				return value.substr(0, len - 3) + "...";
			}
			return value;
		},

		undef : function(value) {
			return value !== undefined ? value : "";
		},

		defaultValue : function(value, defaultValue) {
			return value !== undefined && value !== '' ? value : defaultValue;
		},

		htmlEncode : function(value) {
			return !value ? value : String(value).replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
		},

		htmlDecode : function(value) {
			return !value ? value : String(value).replace(/&amp;/g, "&").replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"');
		},

		trim : function(value) {
			return String(value).replace(trimRe, "");
		},

		substr : function(value, start, length) {
			return String(value).substr(start, length);
		},

		lowercase : function(value) {
			return String(value).toLowerCase();
		},

		uppercase : function(value) {
			return String(value).toUpperCase();
		},

		capitalize : function(value) {
			return !value ? value : value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
		},

		call : function(value, fn) {
			if (arguments.length > 2) {
				var args = Array.prototype.slice.call(arguments, 2);
				args.unshift(value);
				return eval(fn).apply(window, args);
			} else {
				return eval(fn).call(window, value);
			}
		},

		usMoney : function(v) {
			v = (Math.round((v - 0) * 100)) / 100;
			v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
			v = String(v);
			var ps = v.split('.');
			var whole = ps[0];
			var sub = ps[1] ? '.' + ps[1] : '.00';
			var r = /(\d+)(\d{3})/;
			while (r.test(whole)) {
				whole = whole.replace(r, '$1' + ',' + '$2');
			}
			v = whole + sub;
			if (v.charAt(0) == '-') {
				return '-$' + v.substr(1);
			}
			return "$" + v;
		},

		date : function(v, format) {
			if (!v) {
				return "";
			}
			if (!Tera.isDate(v)) {
				v = new Date(Date.parse(v));
			}
			return v.dateFormat(format || "m/d/Y");
		},

		dateRenderer : function(format) {
			return function(v) {
				return Tera.util.Format.date(v, format);
			};
		},

		stripTagsRE :/<\/?[^>]+>/gi,

		stripTags : function(v) {
			return !v ? v : String(v).replace(this.stripTagsRE, "");
		},

		stripScriptsRe :/(?:<script.*?>)((\n|\r|.)*?)(?:<\/script>)/ig,

		stripScripts : function(v) {
			return !v ? v : String(v).replace(this.stripScriptsRe, "");
		},

		fileSize : function(size) {
			if (size < 1024) {
				return size + " bytes";
			} else if (size < 1048576) {
				return (Math.round(((size * 10) / 1024)) / 10) + " KB";
			} else {
				return (Math.round(((size * 10) / 1048576)) / 10) + " MB";
			}
		},

		math : function() {
			var fns = {};
			return function(v, a) {
				if (!fns[a]) {
					fns[a] = new Function('v', 'return v ' + a + ';');
				}
				return fns[a](v);
			}
		}()
	};
}();

Tera.XTemplate = function() {
	Tera.XTemplate.superclass.constructor.apply(this, arguments);
	var s = this.html;

	s = [ '<tpl>', s, '</tpl>' ].join('');

	var re = /<tpl\b[^>]*>((?:(?=([^<]+))\2|<(?!tpl\b[^>]*>))*?)<\/tpl>/;

	var nameRe = /^<tpl\b[^>]*?for="(.*?)"/;
	var ifRe = /^<tpl\b[^>]*?if="(.*?)"/;
	var execRe = /^<tpl\b[^>]*?exec="(.*?)"/;
	var m, id = 0;
	var tpls = [];

	while (m = s.match(re)) {
		var m2 = m[0].match(nameRe);
		var m3 = m[0].match(ifRe);
		var m4 = m[0].match(execRe);
		var exp = null, fn = null, exec = null;
		var name = m2 && m2[1] ? m2[1] : '';
		if (m3) {
			exp = m3 && m3[1] ? m3[1] : null;
			if (exp) {
				fn = new Function('values', 'parent', 'xindex', 'xcount', 'with(values){ return ' + (Tera.util.Format.htmlDecode(exp)) + '; }');
			}
		}
		if (m4) {
			exp = m4 && m4[1] ? m4[1] : null;
			if (exp) {
				exec = new Function('values', 'parent', 'xindex', 'xcount', 'with(values){ ' + (Tera.util.Format.htmlDecode(exp)) + '; }');
			}
		}
		if (name) {
			switch (name) {
				case '.':
					name = new Function('values', 'parent', 'with(values){ return values; }');
					break;
				case '..':
					name = new Function('values', 'parent', 'with(values){ return parent; }');
					break;
				default:
					name = new Function('values', 'parent', 'with(values){ return ' + name + '; }');
			}
		}
		tpls.push( {
			id :id,
			target :name,
			exec :exec,
			test :fn,
			body :m[1] || ''
		});
		s = s.replace(m[0], '{xtpl' + id + '}');
		++id;
	}
	for ( var i = tpls.length - 1; i >= 0; --i) {
		this.compileTpl(tpls[i]);
	}
	this.master = tpls[tpls.length - 1];
	this.tpls = tpls;
};
Tera.extend(Tera.XTemplate, Tera.Template, {
	re :/\{([\w-\.\#]+)(?:\:([\w\.]*)(?:\((.*?)?\))?)?(\s?[\+\-\*\\]\s?[\d\.\+\-\*\\\(\)]+)?\}/g,
	codeRe :/\{\[((?:\\\]|.|\n)*?)\]\}/g,

	applySubTemplate : function(id, values, parent, xindex, xcount) {
		var t = this.tpls[id];
		if (t.test && !t.test.call(this, values, parent, xindex, xcount)) {
			return '';
		}
		if (t.exec && t.exec.call(this, values, parent, xindex, xcount)) {
			return '';
		}
		var vs = t.target ? t.target.call(this, values, parent) : values;
		parent = t.target ? values : parent;
		if (t.target && Tera.isArray(vs)) {
			var buf = [];
			for ( var i = 0, len = vs.length; i < len; i++) {
				buf[buf.length] = t.compiled.call(this, vs[i], parent, i + 1, len);
			}
			return buf.join('');
		}
		return t.compiled.call(this, vs, parent, xindex, xcount);
	},

	compileTpl : function(tpl) {
		var fm = Tera.util.Format;
		var useF = this.disableFormats !== true;
		var sep = Tera.isGecko ? "+" : ",";
		var fn = function(m, name, format, args, math) {
			if (name.substr(0, 4) == 'xtpl') {
				return "'" + sep + 'this.applySubTemplate(' + name.substr(4) + ', values, parent, xindex, xcount)' + sep + "'";
			}
			var v;
			if (name === '.') {
				v = 'values';
			} else if (name === '#') {
				v = 'xindex';
			} else if (name.indexOf('.') != -1) {
				v = name;
			} else {
				v = "values['" + name + "']";
			}
			if (math) {
				v = '(' + v + math + ')';
			}
			if (format && useF) {
				args = args ? ',' + args : "";
				if (format.substr(0, 5) != "this.") {
					format = "fm." + format + '(';
				} else {
					format = 'this.call("' + format.substr(5) + '", ';
					args = ", values";
				}
			} else {
				args = '';
				format = "(" + v + " === undefined ? '' : ";
			}
			return "'" + sep + format + v + args + ")" + sep + "'";
		};
		var codeFn = function(m, code) {
			return "'" + sep + '(' + code + ')' + sep + "'";
		};

		var body;
		if (Tera.isGecko) {
			body = "tpl.compiled = function(values, parent, xindex, xcount){ return '"
					+ tpl.body.replace(/(\r\n|\n)/g, '\\n').replace(/'/g, "\\'").replace(this.re, fn).replace(this.codeRe, codeFn) + "';};";
		} else {
			body = [ "tpl.compiled = function(values, parent, xindex, xcount){ return ['" ];
			body.push(tpl.body.replace(/(\r\n|\n)/g, '\\n').replace(/'/g, "\\'").replace(this.re, fn).replace(this.codeRe, codeFn));
			body.push("'].join('');};");
			body = body.join('');
		}
		eval(body);
		return this;
	},

	applyTemplate : function(values) {
		return this.master.compiled.call(this, values, {}, 1, 1);
	},

	compile : function() {
		return this;
	}

});

Tera.XTemplate.prototype.apply = Tera.XTemplate.prototype.applyTemplate;

Tera.XTemplate.from = function(el) {
	el = Tera.getDom(el);
	return new Tera.XTemplate(el.value || el.innerHTML);
};

Tera.util.CSS = function() {
	var rules = null;
	var doc = document;

	var camelRe = /(-[a-z])/gi;
	var camelFn = function(m, a) {
		return a.charAt(1).toUpperCase();
	};

	return {

		createStyleSheet : function(cssText, id) {
			var ss;
			var head = doc.getElementsByTagName("head")[0];
			var rules = doc.createElement("style");
			rules.setAttribute("type", "text/css");
			if (id) {
				rules.setAttribute("id", id);
			}
			if (Tera.isIE) {
				head.appendChild(rules);
				ss = rules.styleSheet;
				ss.cssText = cssText;
			} else {
				try {
					rules.appendChild(doc.createTextNode(cssText));
				} catch (e) {
					rules.cssText = cssText;
				}
				head.appendChild(rules);
				ss = rules.styleSheet ? rules.styleSheet : (rules.sheet || doc.styleSheets[doc.styleSheets.length - 1]);
			}
			this.cacheStyleSheet(ss);
			return ss;
		},

		removeStyleSheet : function(id) {
			var existing = doc.getElementById(id);
			if (existing) {
				existing.parentNode.removeChild(existing);
			}
		},

		swapStyleSheet : function(id, url) {
			this.removeStyleSheet(id);
			var ss = doc.createElement("link");
			ss.setAttribute("rel", "stylesheet");
			ss.setAttribute("type", "text/css");
			ss.setAttribute("id", id);
			ss.setAttribute("href", url);
			doc.getElementsByTagName("head")[0].appendChild(ss);
		},

		refreshCache : function() {
			return this.getRules(true);
		},

		cacheStyleSheet : function(ss) {
			if (!rules) {
				rules = {};
			}
			try {
				var ssRules = ss.cssRules || ss.rules;
				for ( var j = ssRules.length - 1; j >= 0; --j) {
					rules[ssRules[j].selectorText] = ssRules[j];
				}
			} catch (e) {
			}
		},

		getRules : function(refreshCache) {
			if (rules == null || refreshCache) {
				rules = {};
				var ds = doc.styleSheets;
				for ( var i = 0, len = ds.length; i < len; i++) {
					try {
						this.cacheStyleSheet(ds[i]);
					} catch (e) {
					}
				}
			}
			return rules;
		},

		getRule : function(selector, refreshCache) {
			var rs = this.getRules(refreshCache);
			if (!Tera.isArray(selector)) {
				return rs[selector];
			}
			for ( var i = 0; i < selector.length; i++) {
				if (rs[selector[i]]) {
					return rs[selector[i]];
				}
			}
			return null;
		},

		updateRule : function(selector, property, value) {
			if (!Tera.isArray(selector)) {
				var rule = this.getRule(selector);
				if (rule) {
					rule.style[property.replace(camelRe, camelFn)] = value;
					return true;
				}
			} else {
				for ( var i = 0; i < selector.length; i++) {
					if (this.updateRule(selector[i], property, value)) {
						return true;
					}
				}
			}
			return false;
		}
	};
}();

Tera.util.ClickRepeater = function(el, config) {
	this.el = Tera.get(el);
	this.el.unselectable();

	Tera.apply(this, config);

	this.addEvents(

	"mousedown",

	"click",

	"mouseup");

	this.el.on("mousedown", this.handleMouseDown, this);
	if (this.preventDefault || this.stopDefault) {
		this.el.on("click", function(e) {
			if (this.preventDefault) {
				e.preventDefault();
			}
			if (this.stopDefault) {
				e.stopEvent();
			}
		}, this);
	}

	if (this.handler) {
		this.on("click", this.handler, this.scope || this);
	}

	Tera.util.ClickRepeater.superclass.constructor.call(this);
};

Tera.extend(Tera.util.ClickRepeater, Tera.util.Observable, {
	interval :20,
	delay :250,
	preventDefault :true,
	stopDefault :false,
	timer :0,

	handleMouseDown : function() {
		clearTimeout(this.timer);
		this.el.blur();
		if (this.pressClass) {
			this.el.addClass(this.pressClass);
		}
		this.mousedownTime = new Date();

		Tera.getDoc().on("mouseup", this.handleMouseUp, this);
		this.el.on("mouseout", this.handleMouseOut, this);

		this.fireEvent("mousedown", this);
		this.fireEvent("click", this);

		if (this.accelerate) {
			this.delay = 400;
		}
		this.timer = this.click.defer(this.delay || this.interval, this);
	},

	click : function() {
		this.fireEvent("click", this);
		this.timer = this.click.defer(this.accelerate ? this.easeOutExpo(this.mousedownTime.getElapsed(), 400, -390, 12000) : this.interval, this);
	},

	easeOutExpo : function(t, b, c, d) {
		return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
	},

	handleMouseOut : function() {
		clearTimeout(this.timer);
		if (this.pressClass) {
			this.el.removeClass(this.pressClass);
		}
		this.el.on("mouseover", this.handleMouseReturn, this);
	},

	handleMouseReturn : function() {
		this.el.un("mouseover", this.handleMouseReturn);
		if (this.pressClass) {
			this.el.addClass(this.pressClass);
		}
		this.click();
	},

	handleMouseUp : function() {
		clearTimeout(this.timer);
		this.el.un("mouseover", this.handleMouseReturn);
		this.el.un("mouseout", this.handleMouseOut);
		Tera.getDoc().un("mouseup", this.handleMouseUp);
		this.el.removeClass(this.pressClass);
		this.fireEvent("mouseup", this);
	}
});

Tera.KeyNav = function(el, config) {
	this.el = Tera.get(el);
	Tera.apply(this, config);
	if (!this.disabled) {
		this.disabled = true;
		this.enable();
	}
};

Tera.KeyNav.prototype = {

	disabled :false,

	defaultEventAction :"stopEvent",

	forceKeyDown :false,

	prepareEvent : function(e) {
		var k = e.getKey();
		var h = this.keyToHandler[k];
		if (Tera.isSafari2 && h && k >= 37 && k <= 40) {
			e.stopEvent();
		}
	},

	relay : function(e) {
		var k = e.getKey();
		var h = this.keyToHandler[k];
		if (h && this[h]) {
			if (this.doRelay(e, this[h], h) !== true) {
				e[this.defaultEventAction]();
			}
		}
	},

	doRelay : function(e, h, hname) {
		return h.call(this.scope || this, e);
	},

	enter :false,
	left :false,
	right :false,
	up :false,
	down :false,
	tab :false,
	esc :false,
	pageUp :false,
	pageDown :false,
	del :false,
	home :false,
	end :false,

	keyToHandler : {
		37 :"left",
		39 :"right",
		38 :"up",
		40 :"down",
		33 :"pageUp",
		34 :"pageDown",
		46 :"del",
		36 :"home",
		35 :"end",
		13 :"enter",
		27 :"esc",
		9 :"tab"
	},

	enable : function() {
		if (this.disabled) {
			if (this.forceKeyDown || Tera.isIE || Tera.isSafari3 || Tera.isAir) {
				this.el.on("keydown", this.relay, this);
			} else {
				this.el.on("keydown", this.prepareEvent, this);
				this.el.on("keypress", this.relay, this);
			}
			this.disabled = false;
		}
	},

	disable : function() {
		if (!this.disabled) {
			if (this.forceKeyDown || Tera.isIE || Tera.isSafari3 || Tera.isAir) {
				this.el.un("keydown", this.relay);
			} else {
				this.el.un("keydown", this.prepareEvent);
				this.el.un("keypress", this.relay);
			}
			this.disabled = true;
		}
	}
};

Tera.KeyMap = function(el, config, eventName) {
	this.el = Tera.get(el);
	this.eventName = eventName || "keydown";
	this.bindings = [];
	if (config) {
		this.addBinding(config);
	}
	this.enable();
};

Tera.KeyMap.prototype = {

	stopEvent :false,

	addBinding : function(config) {
		if (Tera.isArray(config)) {
			for ( var i = 0, len = config.length; i < len; i++) {
				this.addBinding(config[i]);
			}
			return;
		}
		var keyCode = config.key, shift = config.shift, ctrl = config.ctrl, alt = config.alt, fn = config.fn || config.handler, scope = config.scope;

		if (typeof keyCode == "string") {
			var ks = [];
			var keyString = keyCode.toUpperCase();
			for ( var j = 0, len = keyString.length; j < len; j++) {
				ks.push(keyString.charCodeAt(j));
			}
			keyCode = ks;
		}
		var keyArray = Tera.isArray(keyCode);

		var handler = function(e) {
			if ((!shift || e.shiftKey) && (!ctrl || e.ctrlKey) && (!alt || e.altKey)) {
				var k = e.getKey();
				if (keyArray) {
					for ( var i = 0, len = keyCode.length; i < len; i++) {
						if (keyCode[i] == k) {
							if (this.stopEvent) {
								e.stopEvent();
							}
							fn.call(scope || window, k, e);
							return;
						}
					}
				} else {
					if (k == keyCode) {
						if (this.stopEvent) {
							e.stopEvent();
						}
						fn.call(scope || window, k, e);
					}
				}
			}
		};
		this.bindings.push(handler);
	},

	on : function(key, fn, scope) {
		var keyCode, shift, ctrl, alt;
		if (typeof key == "object" && !Tera.isArray(key)) {
			keyCode = key.key;
			shift = key.shift;
			ctrl = key.ctrl;
			alt = key.alt;
		} else {
			keyCode = key;
		}
		this.addBinding( {
			key :keyCode,
			shift :shift,
			ctrl :ctrl,
			alt :alt,
			fn :fn,
			scope :scope
		})
	},

	handleKeyDown : function(e) {
		if (this.enabled) {
			var b = this.bindings;
			for ( var i = 0, len = b.length; i < len; i++) {
				b[i].call(this, e);
			}
		}
	},

	isEnabled : function() {
		return this.enabled;
	},

	enable : function() {
		if (!this.enabled) {
			this.el.on(this.eventName, this.handleKeyDown, this);
			this.enabled = true;
		}
	},

	disable : function() {
		if (this.enabled) {
			this.el.removeListener(this.eventName, this.handleKeyDown, this);
			this.enabled = false;
		}
	}
};

Tera.util.TextMetrics = function() {
	var shared;
	return {

		measure : function(el, text, fixedWidth) {
			if (!shared) {
				shared = Tera.util.TextMetrics.Instance(el, fixedWidth);
			}
			shared.bind(el);
			shared.setFixedWidth(fixedWidth || 'auto');
			return shared.getSize(text);
		},

		createInstance : function(el, fixedWidth) {
			return Tera.util.TextMetrics.Instance(el, fixedWidth);
		}
	};
}();

Tera.util.TextMetrics.Instance = function(bindTo, fixedWidth) {
	var ml = new Tera.Element(document.createElement('div'));
	document.body.appendChild(ml.dom);
	ml.position('absolute');
	ml.setLeftTop(-1000, -1000);
	ml.hide();

	if (fixedWidth) {
		ml.setWidth(fixedWidth);
	}

	var instance = {

		getSize : function(text) {
			ml.update(text);
			var s = ml.getSize();
			ml.update('');
			return s;
		},

		bind : function(el) {
			ml.setStyle(Tera.fly(el).getStyles('font-size', 'font-style', 'font-weight', 'font-family', 'line-height', 'text-transform',
					'letter-spacing'));
		},

		setFixedWidth : function(width) {
			ml.setWidth(width);
		},

		getWidth : function(text) {
			ml.dom.style.width = 'auto';
			return this.getSize(text).width;
		},

		getHeight : function(text) {
			return this.getSize(text).height;
		}
	};

	instance.bind(bindTo);

	return instance;
};

Tera.Element.measureText = Tera.util.TextMetrics.measure;

( function() {

	var Event = Tera.EventManager;
	var Dom = Tera.lib.Dom;

	Tera.dd.DragDrop = function(id, sGroup, config) {
		if (id) {
			this.init(id, sGroup, config);
		}
	};

	Tera.dd.DragDrop.prototype = {

		id :null,

		config :null,

		dragElId :null,

		handleElId :null,

		invalidHandleTypes :null,

		invalidHandleIds :null,

		invalidHandleClasses :null,

		startPageX :0,

		startPageY :0,

		groups :null,

		locked :false,

		lock : function() {
			this.locked = true;
		},

		unlock : function() {
			this.locked = false;
		},

		isTarget :true,

		padding :null,

		_domRef :null,

		__ygDragDrop :true,

		constrainX :false,

		constrainY :false,

		minX :0,

		maxX :0,

		minY :0,

		maxY :0,

		maintainOffset :false,

		xTicks :null,

		yTicks :null,

		primaryButtonOnly :true,

		available :false,

		hasOuterHandles :false,

		b4StartDrag : function(x, y) {
		},

		startDrag : function(x, y) {
		},

		b4Drag : function(e) {
		},

		onDrag : function(e) {
		},

		onDragEnter : function(e, id) {
		},

		b4DragOver : function(e) {
		},

		onDragOver : function(e, id) {
		},

		b4DragOut : function(e) {
		},

		onDragOut : function(e, id) {
		},

		b4DragDrop : function(e) {
		},

		onDragDrop : function(e, id) {
		},

		onInvalidDrop : function(e) {
		},

		b4EndDrag : function(e) {
		},

		endDrag : function(e) {
		},

		b4MouseDown : function(e) {
		},

		onMouseDown : function(e) {
		},

		onMouseUp : function(e) {
		},

		onAvailable : function() {
		},

		defaultPadding : {
			left :0,
			right :0,
			top :0,
			bottom :0
		},

		constrainTo : function(constrainTo, pad, inContent) {
			if (typeof pad == "number") {
				pad = {
					left :pad,
					right :pad,
					top :pad,
					bottom :pad
				};
			}
			pad = pad || this.defaultPadding;
			var b = Tera.get(this.getEl()).getBox();
			var ce = Tera.get(constrainTo);
			var s = ce.getScroll();
			var c, cd = ce.dom;
			if (cd == document.body) {
				c = {
					x :s.left,
					y :s.top,
					width :Tera.lib.Dom.getViewWidth(),
					height :Tera.lib.Dom.getViewHeight()
				};
			} else {
				var xy = ce.getXY();
				c = {
					x :xy[0] + s.left,
					y :xy[1] + s.top,
					width :cd.clientWidth,
					height :cd.clientHeight
				};
			}

			var topSpace = b.y - c.y;
			var leftSpace = b.x - c.x;

			this.resetConstraints();
			this.setXConstraint(leftSpace - (pad.left || 0), c.width - leftSpace - b.width - (pad.right || 0), this.xTickSize);
			this.setYConstraint(topSpace - (pad.top || 0), c.height - topSpace - b.height - (pad.bottom || 0), this.yTickSize);
		},

		getEl : function() {
			if (!this._domRef) {
				this._domRef = Tera.getDom(this.id);
			}

			return this._domRef;
		},

		getDragEl : function() {
			return Tera.getDom(this.dragElId);
		},

		init : function(id, sGroup, config) {
			this.initTarget(id, sGroup, config);
			Event.on(this.id, "mousedown", this.handleMouseDown, this);

		},

		initTarget : function(id, sGroup, config) {

			this.config = config || {};

			this.DDM = Tera.dd.DDM;

			this.groups = {};

			if (typeof id !== "string") {
				id = Tera.id(id);
			}

			this.id = id;

			this.addToGroup((sGroup) ? sGroup : "default");

			this.handleElId = id;

			this.setDragElId(id);

			this.invalidHandleTypes = {
				A :"A"
			};
			this.invalidHandleIds = {};
			this.invalidHandleClasses = [];

			this.applyConfig();

			this.handleOnAvailable();
		},

		applyConfig : function() {

			this.padding = this.config.padding || [ 0, 0, 0, 0 ];
			this.isTarget = (this.config.isTarget !== false);
			this.maintainOffset = (this.config.maintainOffset);
			this.primaryButtonOnly = (this.config.primaryButtonOnly !== false);

		},

		handleOnAvailable : function() {
			this.available = true;
			this.resetConstraints();
			this.onAvailable();
		},

		setPadding : function(iTop, iRight, iBot, iLeft) {

			if (!iRight && 0 !== iRight) {
				this.padding = [ iTop, iTop, iTop, iTop ];
			} else if (!iBot && 0 !== iBot) {
				this.padding = [ iTop, iRight, iTop, iRight ];
			} else {
				this.padding = [ iTop, iRight, iBot, iLeft ];
			}
		},

		setInitPosition : function(diffX, diffY) {
			var el = this.getEl();

			if (!this.DDM.verifyEl(el)) {
				return;
			}

			var dx = diffX || 0;
			var dy = diffY || 0;

			var p = Dom.getXY(el);

			this.initPageX = p[0] - dx;
			this.initPageY = p[1] - dy;

			this.lastPageX = p[0];
			this.lastPageY = p[1];

			this.setStartPosition(p);
		},

		setStartPosition : function(pos) {
			var p = pos || Dom.getXY(this.getEl());
			this.deltaSetXY = null;

			this.startPageX = p[0];
			this.startPageY = p[1];
		},

		addToGroup : function(sGroup) {
			this.groups[sGroup] = true;
			this.DDM.regDragDrop(this, sGroup);
		},

		removeFromGroup : function(sGroup) {
			if (this.groups[sGroup]) {
				delete this.groups[sGroup];
			}

			this.DDM.removeDDFromGroup(this, sGroup);
		},

		setDragElId : function(id) {
			this.dragElId = id;
		},

		setHandleElId : function(id) {
			if (typeof id !== "string") {
				id = Tera.id(id);
			}
			this.handleElId = id;
			this.DDM.regHandle(this.id, id);
		},

		setOuterHandleElId : function(id) {
			if (typeof id !== "string") {
				id = Tera.id(id);
			}
			Event.on(id, "mousedown", this.handleMouseDown, this);
			this.setHandleElId(id);

			this.hasOuterHandles = true;
		},

		unreg : function() {
			Event.un(this.id, "mousedown", this.handleMouseDown);
			this._domRef = null;
			this.DDM._remove(this);
		},

		destroy : function() {
			this.unreg();
		},

		isLocked : function() {
			return (this.DDM.isLocked() || this.locked);
		},

		handleMouseDown : function(e, oDD) {
			if (this.primaryButtonOnly && e.button != 0) {
				return;
			}

			if (this.isLocked()) {
				return;
			}

			this.DDM.refreshCache(this.groups);

			var pt = new Tera.lib.Point(Tera.lib.Event.getPageX(e), Tera.lib.Event.getPageY(e));
			if (!this.hasOuterHandles && !this.DDM.isOverTarget(pt, this)) {
			} else {
				if (this.clickValidator(e)) {

					this.setStartPosition();

					this.b4MouseDown(e);
					this.onMouseDown(e);

					this.DDM.handleMouseDown(e, this);

					this.DDM.stopEvent(e);
				} else {

				}
			}
		},

		clickValidator : function(e) {
			var target = e.getTarget();
			return (this.isValidHandleChild(target) && (this.id == this.handleElId || this.DDM.handleWasClicked(target, this.id)));
		},

		addInvalidHandleType : function(tagName) {
			var type = tagName.toUpperCase();
			this.invalidHandleTypes[type] = type;
		},

		addInvalidHandleId : function(id) {
			if (typeof id !== "string") {
				id = Tera.id(id);
			}
			this.invalidHandleIds[id] = id;
		},

		addInvalidHandleClass : function(cssClass) {
			this.invalidHandleClasses.push(cssClass);
		},

		removeInvalidHandleType : function(tagName) {
			var type = tagName.toUpperCase();

			delete this.invalidHandleTypes[type];
		},

		removeInvalidHandleId : function(id) {
			if (typeof id !== "string") {
				id = Tera.id(id);
			}
			delete this.invalidHandleIds[id];
		},

		removeInvalidHandleClass : function(cssClass) {
			for ( var i = 0, len = this.invalidHandleClasses.length; i < len; ++i) {
				if (this.invalidHandleClasses[i] == cssClass) {
					delete this.invalidHandleClasses[i];
				}
			}
		},

		isValidHandleChild : function(node) {

			var valid = true;

			var nodeName;
			try {
				nodeName = node.nodeName.toUpperCase();
			} catch (e) {
				nodeName = node.nodeName;
			}
			valid = valid && !this.invalidHandleTypes[nodeName];
			valid = valid && !this.invalidHandleIds[node.id];

			for ( var i = 0, len = this.invalidHandleClasses.length; valid && i < len; ++i) {
				valid = !Dom.hasClass(node, this.invalidHandleClasses[i]);
			}

			return valid;

		},

		setXTicks : function(iStartX, iTickSize) {
			this.xTicks = [];
			this.xTickSize = iTickSize;

			var tickMap = {};

			for ( var i = this.initPageX; i >= this.minX; i = i - iTickSize) {
				if (!tickMap[i]) {
					this.xTicks[this.xTicks.length] = i;
					tickMap[i] = true;
				}
			}

			for (i = this.initPageX; i <= this.maxX; i = i + iTickSize) {
				if (!tickMap[i]) {
					this.xTicks[this.xTicks.length] = i;
					tickMap[i] = true;
				}
			}

			this.xTicks.sort(this.DDM.numericSort);
		},

		setYTicks : function(iStartY, iTickSize) {
			this.yTicks = [];
			this.yTickSize = iTickSize;

			var tickMap = {};

			for ( var i = this.initPageY; i >= this.minY; i = i - iTickSize) {
				if (!tickMap[i]) {
					this.yTicks[this.yTicks.length] = i;
					tickMap[i] = true;
				}
			}

			for (i = this.initPageY; i <= this.maxY; i = i + iTickSize) {
				if (!tickMap[i]) {
					this.yTicks[this.yTicks.length] = i;
					tickMap[i] = true;
				}
			}

			this.yTicks.sort(this.DDM.numericSort);
		},

		setXConstraint : function(iLeft, iRight, iTickSize) {
			this.leftConstraint = iLeft;
			this.rightConstraint = iRight;

			this.minX = this.initPageX - iLeft;
			this.maxX = this.initPageX + iRight;
			if (iTickSize) {
				this.setXTicks(this.initPageX, iTickSize);
			}

			this.constrainX = true;
		},

		clearConstraints : function() {
			this.constrainX = false;
			this.constrainY = false;
			this.clearTicks();
		},

		clearTicks : function() {
			this.xTicks = null;
			this.yTicks = null;
			this.xTickSize = 0;
			this.yTickSize = 0;
		},

		setYConstraint : function(iUp, iDown, iTickSize) {
			this.topConstraint = iUp;
			this.bottomConstraint = iDown;

			this.minY = this.initPageY - iUp;
			this.maxY = this.initPageY + iDown;
			if (iTickSize) {
				this.setYTicks(this.initPageY, iTickSize);
			}

			this.constrainY = true;

		},

		resetConstraints : function() {

			if (this.initPageX || this.initPageX === 0) {

				var dx = (this.maintainOffset) ? this.lastPageX - this.initPageX : 0;
				var dy = (this.maintainOffset) ? this.lastPageY - this.initPageY : 0;

				this.setInitPosition(dx, dy);

			} else {
				this.setInitPosition();
			}

			if (this.constrainX) {
				this.setXConstraint(this.leftConstraint, this.rightConstraint, this.xTickSize);
			}

			if (this.constrainY) {
				this.setYConstraint(this.topConstraint, this.bottomConstraint, this.yTickSize);
			}
		},

		getTick : function(val, tickArray) {

			if (!tickArray) {

				return val;
			} else if (tickArray[0] >= val) {

				return tickArray[0];
			} else {
				for ( var i = 0, len = tickArray.length; i < len; ++i) {
					var next = i + 1;
					if (tickArray[next] && tickArray[next] >= val) {
						var diff1 = val - tickArray[i];
						var diff2 = tickArray[next] - val;
						return (diff2 > diff1) ? tickArray[i] : tickArray[next];
					}
				}

				return tickArray[tickArray.length - 1];
			}
		},

		toString : function() {
			return ("DragDrop " + this.id);
		}

	};

})();

if (!Tera.dd.DragDropMgr) {

	Tera.dd.DragDropMgr = function() {

		var Event = Tera.EventManager;

		return {

			ids : {},

			handleIds : {},

			dragCurrent :null,

			dragOvers : {},

			deltaX :0,

			deltaY :0,

			preventDefault :true,

			stopPropagation :true,

			initialized :false,

			locked :false,

			init : function() {
				this.initialized = true;
			},

			POINT :0,

			INTERSECT :1,

			mode :0,

			_execOnAll : function(sMethod, args) {
				for ( var i in this.ids) {
					for ( var j in this.ids[i]) {
						var oDD = this.ids[i][j];
						if (!this.isTypeOfDD(oDD)) {
							continue;
						}
						oDD[sMethod].apply(oDD, args);
					}
				}
			},

			_onLoad : function() {

				this.init();

				Event.on(document, "mouseup", this.handleMouseUp, this, true);
				Event.on(document, "mousemove", this.handleMouseMove, this, true);
				Event.on(window, "unload", this._onUnload, this, true);
				Event.on(window, "resize", this._onResize, this, true);

			},

			_onResize : function(e) {
				this._execOnAll("resetConstraints", []);
			},

			lock : function() {
				this.locked = true;
			},

			unlock : function() {
				this.locked = false;
			},

			isLocked : function() {
				return this.locked;
			},

			locationCache : {},

			useCache :true,

			clickPixelThresh :3,

			clickTimeThresh :350,

			dragThreshMet :false,

			clickTimeout :null,

			startX :0,

			startY :0,

			regDragDrop : function(oDD, sGroup) {
				if (!this.initialized) {
					this.init();
				}

				if (!this.ids[sGroup]) {
					this.ids[sGroup] = {};
				}
				this.ids[sGroup][oDD.id] = oDD;
			},

			removeDDFromGroup : function(oDD, sGroup) {
				if (!this.ids[sGroup]) {
					this.ids[sGroup] = {};
				}

				var obj = this.ids[sGroup];
				if (obj && obj[oDD.id]) {
					delete obj[oDD.id];
				}
			},

			_remove : function(oDD) {
				for ( var g in oDD.groups) {
					if (g && this.ids[g][oDD.id]) {
						delete this.ids[g][oDD.id];
					}
				}
				delete this.handleIds[oDD.id];
			},

			regHandle : function(sDDId, sHandleId) {
				if (!this.handleIds[sDDId]) {
					this.handleIds[sDDId] = {};
				}
				this.handleIds[sDDId][sHandleId] = sHandleId;
			},

			isDragDrop : function(id) {
				return (this.getDDById(id)) ? true : false;
			},

			getRelated : function(p_oDD, bTargetsOnly) {
				var oDDs = [];
				for ( var i in p_oDD.groups) {
					for (j in this.ids[i]) {
						var dd = this.ids[i][j];
						if (!this.isTypeOfDD(dd)) {
							continue;
						}
						if (!bTargetsOnly || dd.isTarget) {
							oDDs[oDDs.length] = dd;
						}
					}
				}

				return oDDs;
			},

			isLegalTarget : function(oDD, oTargetDD) {
				var targets = this.getRelated(oDD, true);
				for ( var i = 0, len = targets.length; i < len; ++i) {
					if (targets[i].id == oTargetDD.id) {
						return true;
					}
				}

				return false;
			},

			isTypeOfDD : function(oDD) {
				return (oDD && oDD.__ygDragDrop);
			},

			isHandle : function(sDDId, sHandleId) {
				return (this.handleIds[sDDId] && this.handleIds[sDDId][sHandleId]);
			},

			getDDById : function(id) {
				for ( var i in this.ids) {
					if (this.ids[i][id]) {
						return this.ids[i][id];
					}
				}
				return null;
			},

			handleMouseDown : function(e, oDD) {
				if (Tera.QuickTips) {
					Tera.QuickTips.disable();
				}
				this.currentTarget = e.getTarget();

				this.dragCurrent = oDD;

				var el = oDD.getEl();

				this.startX = e.getPageX();
				this.startY = e.getPageY();

				this.deltaX = this.startX - el.offsetLeft;
				this.deltaY = this.startY - el.offsetTop;

				this.dragThreshMet = false;

				this.clickTimeout = setTimeout( function() {
					var DDM = Tera.dd.DDM;
					DDM.startDrag(DDM.startX, DDM.startY);
				}, this.clickTimeThresh);
			},

			startDrag : function(x, y) {
				clearTimeout(this.clickTimeout);
				if (this.dragCurrent) {
					this.dragCurrent.b4StartDrag(x, y);
					this.dragCurrent.startDrag(x, y);
				}
				this.dragThreshMet = true;
			},

			handleMouseUp : function(e) {

				if (Tera.QuickTips) {
					Tera.QuickTips.enable();
				}
				if (!this.dragCurrent) {
					return;
				}

				clearTimeout(this.clickTimeout);

				if (this.dragThreshMet) {
					this.fireEvents(e, true);
				} else {
				}

				this.stopDrag(e);

				this.stopEvent(e);
			},

			stopEvent : function(e) {
				if (this.stopPropagation) {
					e.stopPropagation();
				}

				if (this.preventDefault) {
					e.preventDefault();
				}
			},

			stopDrag : function(e) {

				if (this.dragCurrent) {
					if (this.dragThreshMet) {
						this.dragCurrent.b4EndDrag(e);
						this.dragCurrent.endDrag(e);
					}

					this.dragCurrent.onMouseUp(e);
				}

				this.dragCurrent = null;
				this.dragOvers = {};
			},

			handleMouseMove : function(e) {
				if (!this.dragCurrent) {
					return true;
				}

				if (Tera.isIE && (e.button !== 0 && e.button !== 1 && e.button !== 2)) {
					this.stopEvent(e);
					return this.handleMouseUp(e);
				}

				if (!this.dragThreshMet) {
					var diffX = Math.abs(this.startX - e.getPageX());
					var diffY = Math.abs(this.startY - e.getPageY());
					if (diffX > this.clickPixelThresh || diffY > this.clickPixelThresh) {
						this.startDrag(this.startX, this.startY);
					}
				}

				if (this.dragThreshMet) {
					this.dragCurrent.b4Drag(e);
					this.dragCurrent.onDrag(e);
					if (!this.dragCurrent.moveOnly) {
						this.fireEvents(e, false);
					}
				}

				this.stopEvent(e);

				return true;
			},

			fireEvents : function(e, isDrop) {
				var dc = this.dragCurrent;

				if (!dc || dc.isLocked()) {
					return;
				}

				var pt = e.getPoint();

				var oldOvers = [];

				var outEvts = [];
				var overEvts = [];
				var dropEvts = [];
				var enterEvts = [];

				for ( var i in this.dragOvers) {

					var ddo = this.dragOvers[i];

					if (!this.isTypeOfDD(ddo)) {
						continue;
					}

					if (!this.isOverTarget(pt, ddo, this.mode)) {
						outEvts.push(ddo);
					}

					oldOvers[i] = true;
					delete this.dragOvers[i];
				}

				for ( var sGroup in dc.groups) {

					if ("string" != typeof sGroup) {
						continue;
					}

					for (i in this.ids[sGroup]) {
						var oDD = this.ids[sGroup][i];
						if (!this.isTypeOfDD(oDD)) {
							continue;
						}

						if (oDD.isTarget && !oDD.isLocked() && oDD != dc) {
							if (this.isOverTarget(pt, oDD, this.mode)) {

								if (isDrop) {
									dropEvts.push(oDD);

								} else {

									if (!oldOvers[oDD.id]) {
										enterEvts.push(oDD);

									} else {
										overEvts.push(oDD);
									}

									this.dragOvers[oDD.id] = oDD;
								}
							}
						}
					}
				}

				if (this.mode) {
					if (outEvts.length) {
						dc.b4DragOut(e, outEvts);
						dc.onDragOut(e, outEvts);
					}

					if (enterEvts.length) {
						dc.onDragEnter(e, enterEvts);
					}

					if (overEvts.length) {
						dc.b4DragOver(e, overEvts);
						dc.onDragOver(e, overEvts);
					}

					if (dropEvts.length) {
						dc.b4DragDrop(e, dropEvts);
						dc.onDragDrop(e, dropEvts);
					}

				} else {

					var len = 0;
					for (i = 0, len = outEvts.length; i < len; ++i) {
						dc.b4DragOut(e, outEvts[i].id);
						dc.onDragOut(e, outEvts[i].id);
					}

					for (i = 0, len = enterEvts.length; i < len; ++i) {

						dc.onDragEnter(e, enterEvts[i].id);
					}

					for (i = 0, len = overEvts.length; i < len; ++i) {
						dc.b4DragOver(e, overEvts[i].id);
						dc.onDragOver(e, overEvts[i].id);
					}

					for (i = 0, len = dropEvts.length; i < len; ++i) {
						dc.b4DragDrop(e, dropEvts[i].id);
						dc.onDragDrop(e, dropEvts[i].id);
					}

				}

				if (isDrop && !dropEvts.length) {
					dc.onInvalidDrop(e);
				}

			},

			getBestMatch : function(dds) {
				var winner = null;

				var len = dds.length;

				if (len == 1) {
					winner = dds[0];
				} else {

					for ( var i = 0; i < len; ++i) {
						var dd = dds[i];

						if (dd.cursorIsOver) {
							winner = dd;
							break;

						} else {
							if (!winner || winner.overlap.getArea() < dd.overlap.getArea()) {
								winner = dd;
							}
						}
					}
				}

				return winner;
			},

			refreshCache : function(groups) {
				for ( var sGroup in groups) {
					if ("string" != typeof sGroup) {
						continue;
					}
					for ( var i in this.ids[sGroup]) {
						var oDD = this.ids[sGroup][i];

						if (this.isTypeOfDD(oDD)) {

							var loc = this.getLocation(oDD);
							if (loc) {
								this.locationCache[oDD.id] = loc;
							} else {
								delete this.locationCache[oDD.id];

							}
						}
					}
				}
			},

			verifyEl : function(el) {
				if (el) {
					var parent;
					if (Tera.isIE) {
						try {
							parent = el.offsetParent;
						} catch (e) {
						}
					} else {
						parent = el.offsetParent;
					}
					if (parent) {
						return true;
					}
				}

				return false;
			},

			getLocation : function(oDD) {
				if (!this.isTypeOfDD(oDD)) {
					return null;
				}

				var el = oDD.getEl(), pos, x1, x2, y1, y2, t, r, b, l;

				try {
					pos = Tera.lib.Dom.getXY(el);
				} catch (e) {
				}

				if (!pos) {
					return null;
				}

				x1 = pos[0];
				x2 = x1 + el.offsetWidth;
				y1 = pos[1];
				y2 = y1 + el.offsetHeight;

				t = y1 - oDD.padding[0];
				r = x2 + oDD.padding[1];
				b = y2 + oDD.padding[2];
				l = x1 - oDD.padding[3];

				return new Tera.lib.Region(t, r, b, l);
			},

			isOverTarget : function(pt, oTarget, intersect) {

				var loc = this.locationCache[oTarget.id];
				if (!loc || !this.useCache) {
					loc = this.getLocation(oTarget);
					this.locationCache[oTarget.id] = loc;

				}

				if (!loc) {
					return false;
				}

				oTarget.cursorIsOver = loc.contains(pt);

				var dc = this.dragCurrent;
				if (!dc || !dc.getTargetCoord || (!intersect && !dc.constrainX && !dc.constrainY)) {
					return oTarget.cursorIsOver;
				}

				oTarget.overlap = null;

				var pos = dc.getTargetCoord(pt.x, pt.y);

				var el = dc.getDragEl();
				var curRegion = new Tera.lib.Region(pos.y, pos.x + el.offsetWidth, pos.y + el.offsetHeight, pos.x);

				var overlap = curRegion.intersect(loc);

				if (overlap) {
					oTarget.overlap = overlap;
					return (intersect) ? true : oTarget.cursorIsOver;
				} else {
					return false;
				}
			},

			_onUnload : function(e, me) {
				Tera.dd.DragDropMgr.unregAll();
			},

			unregAll : function() {

				if (this.dragCurrent) {
					this.stopDrag();
					this.dragCurrent = null;
				}

				this._execOnAll("unreg", []);

				for ( var i in this.elementCache) {
					delete this.elementCache[i];
				}

				this.elementCache = {};
				this.ids = {};
			},

			elementCache : {},

			getElWrapper : function(id) {
				var oWrapper = this.elementCache[id];
				if (!oWrapper || !oWrapper.el) {
					oWrapper = this.elementCache[id] = new this.ElementWrapper(Tera.getDom(id));
				}
				return oWrapper;
			},

			getElement : function(id) {
				return Tera.getDom(id);
			},

			getCss : function(id) {
				var el = Tera.getDom(id);
				return (el) ? el.style : null;
			},

			ElementWrapper : function(el) {

				this.el = el || null;

				this.id = this.el && el.id;

				this.css = this.el && el.style;
			},

			getPosX : function(el) {
				return Tera.lib.Dom.getX(el);
			},

			getPosY : function(el) {
				return Tera.lib.Dom.getY(el);
			},

			swapNode : function(n1, n2) {
				if (n1.swapNode) {
					n1.swapNode(n2);
				} else {
					var p = n2.parentNode;
					var s = n2.nextSibling;

					if (s == n1) {
						p.insertBefore(n1, n2);
					} else if (n2 == n1.nextSibling) {
						p.insertBefore(n2, n1);
					} else {
						n1.parentNode.replaceChild(n2, n1);
						p.insertBefore(n1, s);
					}
				}
			},

			getScroll : function() {
				var t, l, dde = document.documentElement, db = document.body;
				if (dde && (dde.scrollTop || dde.scrollLeft)) {
					t = dde.scrollTop;
					l = dde.scrollLeft;
				} else if (db) {
					t = db.scrollTop;
					l = db.scrollLeft;
				} else {

				}
				return {
					top :t,
					left :l
				};
			},

			getStyle : function(el, styleProp) {
				return Tera.fly(el).getStyle(styleProp);
			},

			getScrollTop : function() {
				return this.getScroll().top;
			},

			getScrollLeft : function() {
				return this.getScroll().left;
			},

			moveToEl : function(moveEl, targetEl) {
				var aCoord = Tera.lib.Dom.getXY(targetEl);
				Tera.lib.Dom.setXY(moveEl, aCoord);
			},

			numericSort : function(a, b) {
				return (a - b);
			},

			_timeoutCount :0,

			_addListeners : function() {
				var DDM = Tera.dd.DDM;
				if (Tera.lib.Event && document) {
					DDM._onLoad();
				} else {
					if (DDM._timeoutCount > 2000) {
					} else {
						setTimeout(DDM._addListeners, 10);
						if (document && document.body) {
							DDM._timeoutCount += 1;
						}
					}
				}
			},

			handleWasClicked : function(node, id) {
				if (this.isHandle(id, node.id)) {
					return true;
				} else {

					var p = node.parentNode;

					while (p) {
						if (this.isHandle(id, p.id)) {
							return true;
						} else {
							p = p.parentNode;
						}
					}
				}

				return false;
			}

		};

	}();

	Tera.dd.DDM = Tera.dd.DragDropMgr;
	Tera.dd.DDM._addListeners();

}

Tera.dd.DD = function(id, sGroup, config) {
	if (id) {
		this.init(id, sGroup, config);
	}
};

Tera.extend(Tera.dd.DD, Tera.dd.DragDrop, {

	scroll :true,

	autoOffset : function(iPageX, iPageY) {
		var x = iPageX - this.startPageX;
		var y = iPageY - this.startPageY;
		this.setDelta(x, y);
	},

	setDelta : function(iDeltaX, iDeltaY) {
		this.deltaX = iDeltaX;
		this.deltaY = iDeltaY;
	},

	setDragElPos : function(iPageX, iPageY) {

		var el = this.getDragEl();
		this.alignElWithMouse(el, iPageX, iPageY);
	},

	alignElWithMouse : function(el, iPageX, iPageY) {
		var oCoord = this.getTargetCoord(iPageX, iPageY);
		var fly = el.dom ? el : Tera.fly(el, '_dd');
		if (!this.deltaSetXY) {
			var aCoord = [ oCoord.x, oCoord.y ];
			fly.setXY(aCoord);
			var newLeft = fly.getLeft(true);
			var newTop = fly.getTop(true);
			this.deltaSetXY = [ newLeft - oCoord.x, newTop - oCoord.y ];
		} else {
			fly.setLeftTop(oCoord.x + this.deltaSetXY[0], oCoord.y + this.deltaSetXY[1]);
		}

		this.cachePosition(oCoord.x, oCoord.y);
		this.autoScroll(oCoord.x, oCoord.y, el.offsetHeight, el.offsetWidth);
		return oCoord;
	},

	cachePosition : function(iPageX, iPageY) {
		if (iPageX) {
			this.lastPageX = iPageX;
			this.lastPageY = iPageY;
		} else {
			var aCoord = Tera.lib.Dom.getXY(this.getEl());
			this.lastPageX = aCoord[0];
			this.lastPageY = aCoord[1];
		}
	},

	autoScroll : function(x, y, h, w) {

		if (this.scroll) {

			var clientH = Tera.lib.Dom.getViewHeight();

			var clientW = Tera.lib.Dom.getViewWidth();

			var st = this.DDM.getScrollTop();

			var sl = this.DDM.getScrollLeft();

			var bot = h + y;

			var right = w + x;

			var toBot = (clientH + st - y - this.deltaY);

			var toRight = (clientW + sl - x - this.deltaX);

			var thresh = 40;

			var scrAmt = (document.all) ? 80 : 30;

			if (bot > clientH && toBot < thresh) {
				window.scrollTo(sl, st + scrAmt);
			}

			if (y < st && st > 0 && y - st < thresh) {
				window.scrollTo(sl, st - scrAmt);
			}

			if (right > clientW && toRight < thresh) {
				window.scrollTo(sl + scrAmt, st);
			}

			if (x < sl && sl > 0 && x - sl < thresh) {
				window.scrollTo(sl - scrAmt, st);
			}
		}
	},

	getTargetCoord : function(iPageX, iPageY) {

		var x = iPageX - this.deltaX;
		var y = iPageY - this.deltaY;

		if (this.constrainX) {
			if (x < this.minX) {
				x = this.minX;
			}
			if (x > this.maxX) {
				x = this.maxX;
			}
		}

		if (this.constrainY) {
			if (y < this.minY) {
				y = this.minY;
			}
			if (y > this.maxY) {
				y = this.maxY;
			}
		}

		x = this.getTick(x, this.xTicks);
		y = this.getTick(y, this.yTicks);

		return {
			x :x,
			y :y
		};
	},

	applyConfig : function() {
		Tera.dd.DD.superclass.applyConfig.call(this);
		this.scroll = (this.config.scroll !== false);
	},

	b4MouseDown : function(e) {

		this.autoOffset(e.getPageX(), e.getPageY());
	},

	b4Drag : function(e) {
		this.setDragElPos(e.getPageX(), e.getPageY());
	},

	toString : function() {
		return ("DD " + this.id);
	}

});

Tera.dd.DDProxy = function(id, sGroup, config) {
	if (id) {
		this.init(id, sGroup, config);
		this.initFrame();
	}
};

Tera.dd.DDProxy.dragElId = "ygddfdiv";

Tera.extend(Tera.dd.DDProxy, Tera.dd.DD, {

	resizeFrame :true,

	centerFrame :false,

	createFrame : function() {
		var self = this;
		var body = document.body;

		if (!body || !body.firstChild) {
			setTimeout( function() {
				self.createFrame();
			}, 50);
			return;
		}

		var div = this.getDragEl();

		if (!div) {
			div = document.createElement("div");
			div.id = this.dragElId;
			var s = div.style;

			s.position = "absolute";
			s.visibility = "hidden";
			s.cursor = "move";
			s.border = "2px solid #aaa";
			s.zIndex = 999;

			body.insertBefore(div, body.firstChild);
		}
	},

	initFrame : function() {
		this.createFrame();
	},

	applyConfig : function() {
		Tera.dd.DDProxy.superclass.applyConfig.call(this);

		this.resizeFrame = (this.config.resizeFrame !== false);
		this.centerFrame = (this.config.centerFrame);
		this.setDragElId(this.config.dragElId || Tera.dd.DDProxy.dragElId);
	},

	showFrame : function(iPageX, iPageY) {
		var el = this.getEl();
		var dragEl = this.getDragEl();
		var s = dragEl.style;

		this._resizeProxy();

		if (this.centerFrame) {
			this.setDelta(Math.round(parseInt(s.width, 10) / 2), Math.round(parseInt(s.height, 10) / 2));
		}

		this.setDragElPos(iPageX, iPageY);

		Tera.fly(dragEl).show();
	},

	_resizeProxy : function() {
		if (this.resizeFrame) {
			var el = this.getEl();
			Tera.fly(this.getDragEl()).setSize(el.offsetWidth, el.offsetHeight);
		}
	},

	b4MouseDown : function(e) {
		var x = e.getPageX();
		var y = e.getPageY();
		this.autoOffset(x, y);
		this.setDragElPos(x, y);
	},

	b4StartDrag : function(x, y) {

		this.showFrame(x, y);
	},

	b4EndDrag : function(e) {
		Tera.fly(this.getDragEl()).hide();
	},

	endDrag : function(e) {

		var lel = this.getEl();
		var del = this.getDragEl();

		del.style.visibility = "";

		this.beforeMove();

		lel.style.visibility = "hidden";
		Tera.dd.DDM.moveToEl(lel, del);
		del.style.visibility = "hidden";
		lel.style.visibility = "";

		this.afterDrag();
	},

	beforeMove : function() {

	},

	afterDrag : function() {

	},

	toString : function() {
		return ("DDProxy " + this.id);
	}

});

Tera.dd.DDTarget = function(id, sGroup, config) {
	if (id) {
		this.initTarget(id, sGroup, config);
	}
};

Tera.extend(Tera.dd.DDTarget, Tera.dd.DragDrop, {
	toString : function() {
		return ("DDTarget " + this.id);
	}
});

Tera.dd.DragTracker = function(config) {
	Tera.apply(this, config);
	this.addEvents('mousedown', 'mouseup', 'mousemove', 'dragstart', 'dragend', 'drag');

	this.dragRegion = new Tera.lib.Region(0, 0, 0, 0);

	if (this.el) {
		this.initEl(this.el);
	}
}

Tera.extend(Tera.dd.DragTracker, Tera.util.Observable, {
	active :false,
	tolerance :5,
	autoStart :false,

	initEl : function(el) {
		this.el = Tera.get(el);
		el.on('mousedown', this.onMouseDown, this, this.delegate ? {
			delegate :this.delegate
		} : undefined);
	},

	destroy : function() {
		this.el.un('mousedown', this.onMouseDown, this);
	},

	onMouseDown : function(e, target) {
		if (this.fireEvent('mousedown', this, e) !== false && this.onBeforeStart(e) !== false) {
			this.startXY = this.lastXY = e.getXY();
			this.dragTarget = this.delegate ? target : this.el.dom;
			e.preventDefault();
			var doc = Tera.getDoc();
			doc.on('mouseup', this.onMouseUp, this);
			doc.on('mousemove', this.onMouseMove, this);
			doc.on('selectstart', this.stopSelect, this);
			if (this.autoStart) {
				this.timer = this.triggerStart.defer(this.autoStart === true ? 1000 : this.autoStart, this);
			}
		}
	},

	onMouseMove : function(e, target) {
		e.preventDefault();
		var xy = e.getXY(), s = this.startXY;
		this.lastXY = xy;
		if (!this.active) {
			if (Math.abs(s[0] - xy[0]) > this.tolerance || Math.abs(s[1] - xy[1]) > this.tolerance) {
				this.triggerStart();
			} else {
				return;
			}
		}
		this.fireEvent('mousemove', this, e);
		this.onDrag(e);
		this.fireEvent('drag', this, e);
	},

	onMouseUp : function(e) {
		var doc = Tera.getDoc();
		doc.un('mousemove', this.onMouseMove, this);
		doc.un('mouseup', this.onMouseUp, this);
		doc.un('selectstart', this.stopSelect, this);
		e.preventDefault();
		this.clearStart();
		this.active = false;
		delete this.elRegion;
		this.fireEvent('mouseup', this, e);
		this.onEnd(e);
		this.fireEvent('dragend', this, e);
	},

	triggerStart : function(isTimer) {
		this.clearStart();
		this.active = true;
		this.onStart(this.startXY);
		this.fireEvent('dragstart', this, this.startXY);
	},

	clearStart : function() {
		if (this.timer) {
			clearTimeout(this.timer);
			delete this.timer;
		}
	},

	stopSelect : function(e) {
		e.stopEvent();
		return false;
	},

	onBeforeStart : function(e) {

	},

	onStart : function(xy) {

	},

	onDrag : function(e) {

	},

	onEnd : function(e) {

	},

	getDragTarget : function() {
		return this.dragTarget;
	},

	getDragCt : function() {
		return this.el;
	},

	getXY : function(constrain) {
		return constrain ? this.constrainModes[constrain].call(this, this.lastXY) : this.lastXY;
	},

	getOffset : function(constrain) {
		var xy = this.getXY(constrain);
		var s = this.startXY;
		return [ s[0] - xy[0], s[1] - xy[1] ];
	},

	constrainModes : {
		'point' : function(xy) {

			if (!this.elRegion) {
				this.elRegion = this.getDragCt().getRegion();
			}

			var dr = this.dragRegion;

			dr.left = xy[0];
			dr.top = xy[1];
			dr.right = xy[0];
			dr.bottom = xy[1];

			dr.constrainTo(this.elRegion);

			return [ dr.left, dr.top ];
		}
	}
});

Tera.dd.ScrollManager = function() {
	var ddm = Tera.dd.DragDropMgr;
	var els = {};
	var dragEl = null;
	var proc = {};

	var onStop = function(e) {
		dragEl = null;
		clearProc();
	};

	var triggerRefresh = function() {
		if (ddm.dragCurrent) {
			ddm.refreshCache(ddm.dragCurrent.groups);
		}
	};

	var doScroll = function() {
		if (ddm.dragCurrent) {
			var dds = Tera.dd.ScrollManager;
			var inc = proc.el.ddScrollConfig ? proc.el.ddScrollConfig.increment : dds.increment;
			if (!dds.animate) {
				if (proc.el.scroll(proc.dir, inc)) {
					triggerRefresh();
				}
			} else {
				proc.el.scroll(proc.dir, inc, true, dds.animDuration, triggerRefresh);
			}
		}
	};

	var clearProc = function() {
		if (proc.id) {
			clearInterval(proc.id);
		}
		proc.id = 0;
		proc.el = null;
		proc.dir = "";
	};

	var startProc = function(el, dir) {
		clearProc();
		proc.el = el;
		proc.dir = dir;
		proc.id = setInterval(doScroll, Tera.dd.ScrollManager.frequency);
	};

	var onFire = function(e, isDrop) {
		if (isDrop || !ddm.dragCurrent) {
			return;
		}
		var dds = Tera.dd.ScrollManager;
		if (!dragEl || dragEl != ddm.dragCurrent) {
			dragEl = ddm.dragCurrent;

			dds.refreshCache();
		}

		var xy = Tera.lib.Event.getXY(e);
		var pt = new Tera.lib.Point(xy[0], xy[1]);
		for ( var id in els) {
			var el = els[id], r = el._region;
			var c = el.ddScrollConfig ? el.ddScrollConfig : dds;
			if (r && r.contains(pt) && el.isScrollable()) {
				if (r.bottom - pt.y <= c.vthresh) {
					if (proc.el != el) {
						startProc(el, "down");
					}
					return;
				} else if (r.right - pt.x <= c.hthresh) {
					if (proc.el != el) {
						startProc(el, "left");
					}
					return;
				} else if (pt.y - r.top <= c.vthresh) {
					if (proc.el != el) {
						startProc(el, "up");
					}
					return;
				} else if (pt.x - r.left <= c.hthresh) {
					if (proc.el != el) {
						startProc(el, "right");
					}
					return;
				}
			}
		}
		clearProc();
	};

	ddm.fireEvents = ddm.fireEvents.createSequence(onFire, ddm);
	ddm.stopDrag = ddm.stopDrag.createSequence(onStop, ddm);

	return {

		register : function(el) {
			if (Tera.isArray(el)) {
				for ( var i = 0, len = el.length; i < len; i++) {
					this.register(el[i]);
				}
			} else {
				el = Tera.get(el);
				els[el.id] = el;
			}
		},

		unregister : function(el) {
			if (Tera.isArray(el)) {
				for ( var i = 0, len = el.length; i < len; i++) {
					this.unregister(el[i]);
				}
			} else {
				el = Tera.get(el);
				delete els[el.id];
			}
		},

		vthresh :25,

		hthresh :25,

		increment :100,

		frequency :500,

		animate :true,

		animDuration :.4,

		refreshCache : function() {
			for ( var id in els) {
				if (typeof els[id] == 'object') {
					els[id]._region = els[id].getRegion();
				}
			}
		}
	};
}();

Tera.dd.Registry = function() {
	var elements = {};
	var handles = {};
	var autoIdSeed = 0;

	var getId = function(el, autogen) {
		if (typeof el == "string") {
			return el;
		}
		var id = el.id;
		if (!id && autogen !== false) {
			id = "extdd-" + (++autoIdSeed);
			el.id = id;
		}
		return id;
	};

	return {

		register : function(el, data) {
			data = data || {};
			if (typeof el == "string") {
				el = document.getElementById(el);
			}
			data.ddel = el;
			elements[getId(el)] = data;
			if (data.isHandle !== false) {
				handles[data.ddel.id] = data;
			}
			if (data.handles) {
				var hs = data.handles;
				for ( var i = 0, len = hs.length; i < len; i++) {
					handles[getId(hs[i])] = data;
				}
			}
		},

		unregister : function(el) {
			var id = getId(el, false);
			var data = elements[id];
			if (data) {
				delete elements[id];
				if (data.handles) {
					var hs = data.handles;
					for ( var i = 0, len = hs.length; i < len; i++) {
						delete handles[getId(hs[i], false)];
					}
				}
			}
		},

		getHandle : function(id) {
			if (typeof id != "string") {
				id = id.id;
			}
			return handles[id];
		},

		getHandleFromEvent : function(e) {
			var t = Tera.lib.Event.getTarget(e);
			return t ? handles[t.id] : null;
		},

		getTarget : function(id) {
			if (typeof id != "string") {
				id = id.id;
			}
			return elements[id];
		},

		getTargetFromEvent : function(e) {
			var t = Tera.lib.Event.getTarget(e);
			return t ? elements[t.id] || handles[t.id] : null;
		}
	};
}();

Tera.dd.StatusProxy = function(config) {
	Tera.apply(this, config);
	this.id = this.id || Tera.id();
	this.el = new Tera.Layer( {
		dh : {
			id :this.id,
			tag :"div",
			cls :"x-dd-drag-proxy " + this.dropNotAllowed,
			children : [ {
				tag :"div",
				cls :"x-dd-drop-icon"
			}, {
				tag :"div",
				cls :"x-dd-drag-ghost"
			} ]
		},
		shadow :!config || config.shadow !== false
	});
	this.ghost = Tera.get(this.el.dom.childNodes[1]);
	this.dropStatus = this.dropNotAllowed;
};

Tera.dd.StatusProxy.prototype = {

	dropAllowed :"x-dd-drop-ok",

	dropNotAllowed :"x-dd-drop-nodrop",

	setStatus : function(cssClass) {
		cssClass = cssClass || this.dropNotAllowed;
		if (this.dropStatus != cssClass) {
			this.el.replaceClass(this.dropStatus, cssClass);
			this.dropStatus = cssClass;
		}
	},

	reset : function(clearGhost) {
		this.el.dom.className = "x-dd-drag-proxy " + this.dropNotAllowed;
		this.dropStatus = this.dropNotAllowed;
		if (clearGhost) {
			this.ghost.update("");
		}
	},

	update : function(html) {
		if (typeof html == "string") {
			this.ghost.update(html);
		} else {
			this.ghost.update("");
			html.style.margin = "0";
			this.ghost.dom.appendChild(html);
		}
	},

	getEl : function() {
		return this.el;
	},

	getGhost : function() {
		return this.ghost;
	},

	hide : function(clear) {
		this.el.hide();
		if (clear) {
			this.reset(true);
		}
	},

	stop : function() {
		if (this.anim && this.anim.isAnimated && this.anim.isAnimated()) {
			this.anim.stop();
		}
	},

	show : function() {
		this.el.show();
	},

	sync : function() {
		this.el.sync();
	},

	repair : function(xy, callback, scope) {
		this.callback = callback;
		this.scope = scope;
		if (xy && this.animRepair !== false) {
			this.el.addClass("x-dd-drag-repair");
			this.el.hideUnders(true);
			this.anim = this.el.shift( {
				duration :this.repairDuration || .5,
				easing :'easeOut',
				xy :xy,
				stopFx :true,
				callback :this.afterRepair,
				scope :this
			});
		} else {
			this.afterRepair();
		}
	},

	afterRepair : function() {
		this.hide(true);
		if (typeof this.callback == "function") {
			this.callback.call(this.scope || this);
		}
		this.callback = null;
		this.scope = null;
	}
};

Tera.dd.DragSource = function(el, config) {
	this.el = Tera.get(el);
	if (!this.dragData) {
		this.dragData = {};
	}

	Tera.apply(this, config);

	if (!this.proxy) {
		this.proxy = new Tera.dd.StatusProxy();
	}
	Tera.dd.DragSource.superclass.constructor.call(this, this.el.dom, this.ddGroup || this.group, {
		dragElId :this.proxy.id,
		resizeFrame :false,
		isTarget :false,
		scroll :this.scroll === true
	});

	this.dragging = false;
};

Tera.extend(Tera.dd.DragSource, Tera.dd.DDProxy, {

	dropAllowed :"x-dd-drop-ok",

	dropNotAllowed :"x-dd-drop-nodrop",

	getDragData : function(e) {
		return this.dragData;
	},

	onDragEnter : function(e, id) {
		var target = Tera.dd.DragDropMgr.getDDById(id);
		this.cachedTarget = target;
		if (this.beforeDragEnter(target, e, id) !== false) {
			if (target.isNotifyTarget) {
				var status = target.notifyEnter(this, e, this.dragData);
				this.proxy.setStatus(status);
			} else {
				this.proxy.setStatus(this.dropAllowed);
			}

			if (this.afterDragEnter) {

				this.afterDragEnter(target, e, id);
			}
		}
	},

	beforeDragEnter : function(target, e, id) {
		return true;
	},

	alignElWithMouse : function() {
		Tera.dd.DragSource.superclass.alignElWithMouse.apply(this, arguments);
		this.proxy.sync();
	},

	onDragOver : function(e, id) {
		var target = this.cachedTarget || Tera.dd.DragDropMgr.getDDById(id);
		if (this.beforeDragOver(target, e, id) !== false) {
			if (target.isNotifyTarget) {
				var status = target.notifyOver(this, e, this.dragData);
				this.proxy.setStatus(status);
			}

			if (this.afterDragOver) {

				this.afterDragOver(target, e, id);
			}
		}
	},

	beforeDragOver : function(target, e, id) {
		return true;
	},

	onDragOut : function(e, id) {
		var target = this.cachedTarget || Tera.dd.DragDropMgr.getDDById(id);
		if (this.beforeDragOut(target, e, id) !== false) {
			if (target.isNotifyTarget) {
				target.notifyOut(this, e, this.dragData);
			}
			this.proxy.reset();
			if (this.afterDragOut) {

				this.afterDragOut(target, e, id);
			}
		}
		this.cachedTarget = null;
	},

	beforeDragOut : function(target, e, id) {
		return true;
	},

	onDragDrop : function(e, id) {
		var target = this.cachedTarget || Tera.dd.DragDropMgr.getDDById(id);
		if (this.beforeDragDrop(target, e, id) !== false) {
			if (target.isNotifyTarget) {
				if (target.notifyDrop(this, e, this.dragData)) {
					this.onValidDrop(target, e, id);
				} else {
					this.onInvalidDrop(target, e, id);
				}
			} else {
				this.onValidDrop(target, e, id);
			}

			if (this.afterDragDrop) {

				this.afterDragDrop(target, e, id);
			}
		}
		delete this.cachedTarget;
	},

	beforeDragDrop : function(target, e, id) {
		return true;
	},

	onValidDrop : function(target, e, id) {
		this.hideProxy();
		if (this.afterValidDrop) {

			this.afterValidDrop(target, e, id);
		}
	},

	getRepairXY : function(e, data) {
		return this.el.getXY();
	},

	onInvalidDrop : function(target, e, id) {
		this.beforeInvalidDrop(target, e, id);
		if (this.cachedTarget) {
			if (this.cachedTarget.isNotifyTarget) {
				this.cachedTarget.notifyOut(this, e, this.dragData);
			}
			this.cacheTarget = null;
		}
		this.proxy.repair(this.getRepairXY(e, this.dragData), this.afterRepair, this);

		if (this.afterInvalidDrop) {

			this.afterInvalidDrop(e, id);
		}
	},

	afterRepair : function() {
		if (Tera.enableFx) {
			this.el.highlight(this.hlColor || "c3daf9");
		}
		this.dragging = false;
	},

	beforeInvalidDrop : function(target, e, id) {
		return true;
	},

	handleMouseDown : function(e) {
		if (this.dragging) {
			return;
		}
		var data = this.getDragData(e);
		if (data && this.onBeforeDrag(data, e) !== false) {
			this.dragData = data;
			this.proxy.stop();
			Tera.dd.DragSource.superclass.handleMouseDown.apply(this, arguments);
		}
	},

	onBeforeDrag : function(data, e) {
		return true;
	},

	onStartDrag :Tera.emptyFn,

	startDrag : function(x, y) {
		this.proxy.reset();
		this.dragging = true;
		this.proxy.update("");
		this.onInitDrag(x, y);
		this.proxy.show();
	},

	onInitDrag : function(x, y) {
		var clone = this.el.dom.cloneNode(true);
		clone.id = Tera.id();
		this.proxy.update(clone);
		this.onStartDrag(x, y);
		return true;
	},

	getProxy : function() {
		return this.proxy;
	},

	hideProxy : function() {
		this.proxy.hide();
		this.proxy.reset(true);
		this.dragging = false;
	},

	triggerCacheRefresh : function() {
		Tera.dd.DDM.refreshCache(this.groups);
	},

	b4EndDrag : function(e) {
	},

	endDrag : function(e) {
		this.onEndDrag(this.dragData, e);
	},

	onEndDrag : function(data, e) {
	},

	autoOffset : function(x, y) {
		this.setDelta(-12, -20);
	}
});

Tera.dd.DropTarget = function(el, config) {
	this.el = Tera.get(el);

	Tera.apply(this, config);

	if (this.containerScroll) {
		Tera.dd.ScrollManager.register(this.el);
	}

	Tera.dd.DropTarget.superclass.constructor.call(this, this.el.dom, this.ddGroup || this.group, {
		isTarget :true
	});

};

Tera.extend(Tera.dd.DropTarget, Tera.dd.DDTarget, {

	dropAllowed :"x-dd-drop-ok",

	dropNotAllowed :"x-dd-drop-nodrop",

	isTarget :true,

	isNotifyTarget :true,

	notifyEnter : function(dd, e, data) {
		if (this.overClass) {
			this.el.addClass(this.overClass);
		}
		return this.dropAllowed;
	},

	notifyOver : function(dd, e, data) {
		return this.dropAllowed;
	},

	notifyOut : function(dd, e, data) {
		if (this.overClass) {
			this.el.removeClass(this.overClass);
		}
	},

	notifyDrop : function(dd, e, data) {
		return false;
	}
});

Tera.dd.DragZone = function(el, config) {
	Tera.dd.DragZone.superclass.constructor.call(this, el, config);
	if (this.containerScroll) {
		Tera.dd.ScrollManager.register(this.el);
	}
};

Tera.extend(Tera.dd.DragZone, Tera.dd.DragSource, {

	getDragData : function(e) {
		return Tera.dd.Registry.getHandleFromEvent(e);
	},

	onInitDrag : function(x, y) {
		this.proxy.update(this.dragData.ddel.cloneNode(true));
		this.onStartDrag(x, y);
		return true;
	},

	afterRepair : function() {
		if (Tera.enableFx) {
			Tera.Element.fly(this.dragData.ddel).highlight(this.hlColor || "c3daf9");
		}
		this.dragging = false;
	},

	getRepairXY : function(e) {
		return Tera.Element.fly(this.dragData.ddel).getXY();
	}
});

Tera.dd.DropZone = function(el, config) {
	Tera.dd.DropZone.superclass.constructor.call(this, el, config);
};

Tera.extend(Tera.dd.DropZone, Tera.dd.DropTarget, {

	getTargetFromEvent : function(e) {
		return Tera.dd.Registry.getTargetFromEvent(e);
	},

	onNodeEnter : function(n, dd, e, data) {

	},

	onNodeOver : function(n, dd, e, data) {
		return this.dropAllowed;
	},

	onNodeOut : function(n, dd, e, data) {

	},

	onNodeDrop : function(n, dd, e, data) {
		return false;
	},

	onContainerOver : function(dd, e, data) {
		return this.dropNotAllowed;
	},

	onContainerDrop : function(dd, e, data) {
		return false;
	},

	notifyEnter : function(dd, e, data) {
		return this.dropNotAllowed;
	},

	notifyOver : function(dd, e, data) {
		var n = this.getTargetFromEvent(e);
		if (!n) {
			if (this.lastOverNode) {
				this.onNodeOut(this.lastOverNode, dd, e, data);
				this.lastOverNode = null;
			}
			return this.onContainerOver(dd, e, data);
		}
		if (this.lastOverNode != n) {
			if (this.lastOverNode) {
				this.onNodeOut(this.lastOverNode, dd, e, data);
			}
			this.onNodeEnter(n, dd, e, data);
			this.lastOverNode = n;
		}
		return this.onNodeOver(n, dd, e, data);
	},

	notifyOut : function(dd, e, data) {
		if (this.lastOverNode) {
			this.onNodeOut(this.lastOverNode, dd, e, data);
			this.lastOverNode = null;
		}
	},

	notifyDrop : function(dd, e, data) {
		if (this.lastOverNode) {
			this.onNodeOut(this.lastOverNode, dd, e, data);
			this.lastOverNode = null;
		}
		var n = this.getTargetFromEvent(e);
		return n ? this.onNodeDrop(n, dd, e, data) : this.onContainerDrop(dd, e, data);
	},

	triggerCacheRefresh : function() {
		Tera.dd.DDM.refreshCache(this.groups);
	}
});

Tera.data.SortTypes = {

	none : function(s) {
		return s;
	},

	stripTagsRE :/<\/?[^>]+>/gi,

	asText : function(s) {
		return String(s).replace(this.stripTagsRE, "");
	},

	asUCText : function(s) {
		return String(s).toUpperCase().replace(this.stripTagsRE, "");
	},

	asUCString : function(s) {
		return String(s).toUpperCase();
	},

	asDate : function(s) {
		if (!s) {
			return 0;
		}
		if (Tera.isDate(s)) {
			return s.getTime();
		}
		return Date.parse(String(s));
	},

	asFloat : function(s) {
		var val = parseFloat(String(s).replace(/,/g, ""));
		if (isNaN(val))
			val = 0;
		return val;
	},

	asInt : function(s) {
		var val = parseInt(String(s).replace(/,/g, ""));
		if (isNaN(val))
			val = 0;
		return val;
	}
};

Tera.data.Record = function(data, id) {
	this.id = (id || id === 0) ? id : ++Tera.data.Record.AUTO_ID;
	this.data = data;
};

Tera.data.Record.create = function(o) {
	var f = Tera.extend(Tera.data.Record, {});
	var p = f.prototype;
	p.fields = new Tera.util.MixedCollection(false, function(field) {
		return field.name;
	});
	for ( var i = 0, len = o.length; i < len; i++) {
		p.fields.add(new Tera.data.Field(o[i]));
	}
	f.getField = function(name) {
		return p.fields.get(name);
	};
	return f;
};

Tera.data.Record.AUTO_ID = 1000;
Tera.data.Record.EDIT = 'edit';
Tera.data.Record.REJECT = 'reject';
Tera.data.Record.COMMIT = 'commit';

Tera.data.Record.prototype = {

	dirty :false,
	editing :false,
	error :null,

	modified :null,

	join : function(store) {
		this.store = store;
	},

	set : function(name, value) {
		if (String(this.data[name]) == String(value)) {
			return;
		}
		this.dirty = true;
		if (!this.modified) {
			this.modified = {};
		}
		if (typeof this.modified[name] == 'undefined') {
			this.modified[name] = this.data[name];
		}
		this.data[name] = value;
		if (!this.editing && this.store) {
			this.store.afterEdit(this);
		}
	},

	get : function(name) {
		return this.data[name];
	},

	beginEdit : function() {
		this.editing = true;
		this.modified = {};
	},

	cancelEdit : function() {
		this.editing = false;
		delete this.modified;
	},

	endEdit : function() {
		this.editing = false;
		if (this.dirty && this.store) {
			this.store.afterEdit(this);
		}
	},

	reject : function(silent) {
		var m = this.modified;
		for ( var n in m) {
			if (typeof m[n] != "function") {
				this.data[n] = m[n];
			}
		}
		this.dirty = false;
		delete this.modified;
		this.editing = false;
		if (this.store && silent !== true) {
			this.store.afterReject(this);
		}
	},

	commit : function(silent) {
		this.dirty = false;
		delete this.modified;
		this.editing = false;
		if (this.store && silent !== true) {
			this.store.afterCommit(this);
		}
	},

	getChanges : function() {
		var m = this.modified, cs = {};
		for ( var n in m) {
			if (m.hasOwnProperty(n)) {
				cs[n] = this.data[n];
			}
		}
		return cs;
	},

	hasError : function() {
		return this.error != null;
	},

	clearError : function() {
		this.error = null;
	},

	copy : function(newId) {
		return new this.constructor(Tera.apply( {}, this.data), newId || this.id);
	},

	isModified : function(fieldName) {
		return this.modified && this.modified.hasOwnProperty(fieldName);
	}
};

Tera.StoreMgr = Tera.apply(new Tera.util.MixedCollection(), {

	register : function() {
		for ( var i = 0, s; s = arguments[i]; i++) {
			this.add(s);
		}
	},

	unregister : function() {
		for ( var i = 0, s; s = arguments[i]; i++) {
			this.remove(this.lookup(s));
		}
	},

	lookup : function(id) {
		return typeof id == "object" ? id : this.get(id);
	},

	getKey : function(o) {
		return o.storeId || o.id;
	}
});

Tera.data.Store = function(config) {
	this.data = new Tera.util.MixedCollection(false);
	this.data.getKey = function(o) {
		return o.id;
	};

	this.baseParams = {};
	this.paramNames = {
		"start" :"start",
		"limit" :"limit",
		"sort" :"sort",
		"dir" :"dir"
	};

	if (config && config.data) {
		this.inlineData = config.data;
		delete config.data;
	}

	Tera.apply(this, config);

	if (this.url && !this.proxy) {
		this.proxy = new Tera.data.HttpProxy( {
			url :this.url
		});
	}

	if (this.reader) {
		if (!this.recordType) {
			this.recordType = this.reader.recordType;
		}
		if (this.reader.onMetaChange) {
			this.reader.onMetaChange = this.onMetaChange.createDelegate(this);
		}
	}

	if (this.recordType) {
		this.fields = this.recordType.prototype.fields;
	}
	this.modified = [];

	this.addEvents(

	'datachanged',

	'metachange',

	'add',

	'remove',

	'update',

	'clear',

	'beforeload',

	'load',

	'loadexception');

	if (this.proxy) {
		this.relayEvents(this.proxy, [ "loadexception" ]);
	}

	this.sortToggle = {};
	if (this.sortInfo) {
		this.setDefaultSort(this.sortInfo.field, this.sortInfo.direction);
	}

	Tera.data.Store.superclass.constructor.call(this);

	if (this.storeId || this.id) {
		Tera.StoreMgr.register(this);
	}
	if (this.inlineData) {
		this.loadData(this.inlineData);
		delete this.inlineData;
	} else if (this.autoLoad) {
		this.load.defer(10, this, [ typeof this.autoLoad == 'object' ? this.autoLoad : undefined ]);
	}
};
Tera.extend(Tera.data.Store, Tera.util.Observable, {

	remoteSort: false,

	pruneModifiedRecords :false,

	lastOptions :null,

	destroy : function() {
		if (this.id) {
			Tera.StoreMgr.unregister(this);
		}
		this.data = null;
		this.purgeListeners();
	},

	add : function(records) {
		records = [].concat(records);
		if (records.length < 1) {
			return;
		}
		for ( var i = 0, len = records.length; i < len; i++) {
			records[i].join(this);
		}
		var index = this.data.length;
		this.data.addAll(records);
		if (this.snapshot) {
			this.snapshot.addAll(records);
		}
		this.fireEvent("add", this, records, index);
	},

	addSorted : function(record) {
		var index = this.findInsertIndex(record);
		this.insert(index, record);
	},

	remove : function(record) {
		var index = this.data.indexOf(record);
		this.data.removeAt(index);
		if (this.pruneModifiedRecords) {
			this.modified.remove(record);
		}
		if (this.snapshot) {
			this.snapshot.remove(record);
		}
		this.fireEvent("remove", this, record, index);
	},

	removeAll : function() {
		this.data.clear();
		if (this.snapshot) {
			this.snapshot.clear();
		}
		if (this.pruneModifiedRecords) {
			this.modified = [];
		}
		this.fireEvent("clear", this);
	},
	
	



	insert : function(index, records) {
		records = [].concat(records);
		for ( var i = 0, len = records.length; i < len; i++) {
			this.data.insert(index, records[i]);
			records[i].join(this);
		}
		this.fireEvent("add", this, records, index);
	},

	indexOf : function(record) {
		return this.data.indexOf(record);
	},

	indexOfId : function(id) {
		return this.data.indexOfKey(id);
	},

	getById : function(id) {
		return this.data.key(id);
	},

	getAt : function(index) {
		return this.data.itemAt(index);
	},

	getRange : function(start, end) {
		return this.data.getRange(start, end);
	},

	storeOptions : function(o) {
		o = Tera.apply( {}, o);
		delete o.callback;
		delete o.scope;
		this.lastOptions = o;
	},

	load : function(options) {
		options = options || {};
		if (this.fireEvent("beforeload", this, options) !== false) {
			this.storeOptions(options);
			var p = Tera.apply(options.params || {}, this.baseParams);
			if (this.sortInfo && this.remoteSort) {
				var pn = this.paramNames;
				p[pn["sort"]] = this.sortInfo.field;
				p[pn["dir"]] = this.sortInfo.direction;
			}
			this.proxy.load(p, this.reader, this.loadRecords, this, options);
			return true;
		} else {
			return false;
		}
	},

	reload : function(options) {
		this.load(Tera.applyIf(options || {}, this.lastOptions));
	},

	loadRecords : function(o, options, success) {
		if (!o || success === false) {
			if (success !== false) {
				this.fireEvent("load", this, [], options);
			}
			if (options.callback) {
				options.callback.call(options.scope || this, [], options, false);
			}
			return;
		}
		var r = o.records, t = o.totalRecords || r.length;
		if (!options || options.add !== true) {
			if (this.pruneModifiedRecords) {
				this.modified = [];
			}
			for ( var i = 0, len = r.length; i < len; i++) {
				r[i].join(this);
			}
			if (this.snapshot) {
				this.data = this.snapshot;
				delete this.snapshot;
			}
			this.data.clear();
			this.data.addAll(r);
			this.totalLength = t;
			this.applySort();
			this.fireEvent("datachanged", this);
		} else {
			this.totalLength = Math.max(t, this.data.length + r.length);
			this.add(r);
		}
		this.fireEvent("load", this, r, options);
		if (options.callback) {
			options.callback.call(options.scope || this, r, options, true);
		}
	},

	loadData : function(o, append) {
		var r = this.reader.readRecords(o);
		this.loadRecords(r, {
			add :append
		}, true);
	},

	getCount : function() {
		return this.data.length || 0;
	},

	getTotalCount : function() {
		return this.totalLength || 0;
	},

	getSortState : function() {
		return this.sortInfo;
	},

	applySort : function() {
		if (this.sortInfo && !this.remoteSort) {
			var s = this.sortInfo, f = s.field;
			this.sortData(f, s.direction);
		}
	},

	sortData : function(f, direction) {
		direction = direction || 'ASC';
		var st = this.fields.get(f).sortType;
		var fn = function(r1, r2) {
			var v1 = st(r1.data[f]), v2 = st(r2.data[f]);
			return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
		};
		this.data.sort(direction, fn);
		if (this.snapshot && this.snapshot != this.data) {
			this.snapshot.sort(direction, fn);
		}
	},

	setDefaultSort : function(field, dir) {
		dir = dir ? dir.toUpperCase() : "ASC";
		this.sortInfo = {
			field :field,
			direction :dir
		};
		this.sortToggle[field] = dir;
	},

	sort : function(fieldName, dir) {
		var f = this.fields.get(fieldName);
		if (!f) {
			return false;
		}
		if (!dir) {
			if (this.sortInfo && this.sortInfo.field == f.name) {
				dir = (this.sortToggle[f.name] || "ASC").toggle("ASC", "DESC");
			} else {
				dir = f.sortDir;
			}
		}
		var st = (this.sortToggle) ? this.sortToggle[f.name] : null;
		var si = (this.sortInfo) ? this.sortInfo : null;

		this.sortToggle[f.name] = dir;
		this.sortInfo = {
			field :f.name,
			direction :dir
		};
		if (!this.remoteSort) {
			this.applySort();
			this.fireEvent("datachanged", this);
		} else {
			if (this.getCount()==0) {
				return false;
			}
			if (!this.load(this.lastOptions)) {
				if (st) {
					this.sortToggle[f.name] = st;
				}
				if (si) {
					this.sortInfo = si;
				}
			}
		}
	},

	each : function(fn, scope) {
		this.data.each(fn, scope);
	},

	getModifiedRecords : function() {
		return this.modified;
	},

	createFilterFn : function(property, value, anyMatch, caseSensitive) {
		if (Tera.isEmpty(value, false)) {
			return false;
		}
		value = this.data.createValueMatcher(value, anyMatch, caseSensitive);
		return function(r) {
			return value.test(r.data[property]);
		};
	},

	sum : function(property, start, end) {
		var rs = this.data.items, v = 0;
		start = start || 0;
		end = (end || end === 0) ? end : rs.length - 1;

		for ( var i = start; i <= end; i++) {
			v += (rs[i].data[property] || 0);
		}
		return v;
	},

	filter : function(property, value, anyMatch, caseSensitive) {
		var fn = this.createFilterFn(property, value, anyMatch, caseSensitive);
		return fn ? this.filterBy(fn) : this.clearFilter();
	},

	filterBy : function(fn, scope) {
		this.snapshot = this.snapshot || this.data;
		this.data = this.queryBy(fn, scope || this);
		this.fireEvent("datachanged", this);
	},

	query : function(property, value, anyMatch, caseSensitive) {
		var fn = this.createFilterFn(property, value, anyMatch, caseSensitive);
		return fn ? this.queryBy(fn) : this.data.clone();
	},

	queryBy : function(fn, scope) {
		var data = this.snapshot || this.data;
		return data.filterBy(fn, scope || this);
	},

	find : function(property, value, start, anyMatch, caseSensitive) {
		var fn = this.createFilterFn(property, value, anyMatch, caseSensitive);
		return fn ? this.data.findIndexBy(fn, null, start) : -1;
	},

	findBy : function(fn, scope, start) {
		return this.data.findIndexBy(fn, scope, start);
	},

	collect : function(dataIndex, allowNull, bypassFilter) {
		var d = (bypassFilter === true && this.snapshot) ? this.snapshot.items : this.data.items;
		var v, sv, r = [], l = {};
		for ( var i = 0, len = d.length; i < len; i++) {
			v = d[i].data[dataIndex];
			sv = String(v);
			if ((allowNull || !Tera.isEmpty(v)) && !l[sv]) {
				l[sv] = true;
				r[r.length] = v;
			}
		}
		return r;
	},

	clearFilter : function(suppressEvent) {
		if (this.isFiltered()) {
			this.data = this.snapshot;
			delete this.snapshot;
			if (suppressEvent !== true) {
				this.fireEvent("datachanged", this);
			}
		}
	},

	isFiltered : function() {
		return this.snapshot && this.snapshot != this.data;
	},

	afterEdit : function(record) {
		if (this.modified.indexOf(record) == -1) {
			this.modified.push(record);
		}
		this.fireEvent("update", this, record, Tera.data.Record.EDIT);
	},

	afterReject : function(record) {
		this.modified.remove(record);
		this.fireEvent("update", this, record, Tera.data.Record.REJECT);
	},

	afterCommit : function(record) {
		this.modified.remove(record);
		this.fireEvent("update", this, record, Tera.data.Record.COMMIT);
	},

	commitChanges : function() {
		var m = this.modified.slice(0);
		this.modified = [];
		for ( var i = 0, len = m.length; i < len; i++) {
			m[i].commit();
		}
	},

	rejectChanges : function() {
		var m = this.modified.slice(0);
		this.modified = [];
		for ( var i = 0, len = m.length; i < len; i++) {
			m[i].reject();
		}
	},

	onMetaChange : function(meta, rtype, o) {
		this.recordType = rtype;
		this.fields = rtype.prototype.fields;
		delete this.snapshot;
		this.sortInfo = meta.sortInfo;
		this.modified = [];
		this.fireEvent('metachange', this, this.reader.meta);
	},

	findInsertIndex : function(record) {
		this.suspendEvents();
		var data = this.data.clone();
		this.data.add(record);
		this.applySort();
		var index = this.data.indexOf(record);
		this.data = data;
		this.resumeEvents();
		return index;
	}
});

Tera.data.SimpleStore = function(config) {
	Tera.data.SimpleStore.superclass.constructor.call(this, Tera.apply(config, {
		reader :new Tera.data.ArrayReader( {
			id :config.id
		}, Tera.data.Record.create(config.fields))
	}));
};
Tera.extend(Tera.data.SimpleStore, Tera.data.Store, {
	loadData : function(data, append) {
		if (this.expandData === true) {
			var r = [];
			for ( var i = 0, len = data.length; i < len; i++) {
				r[r.length] = [ data[i] ];
			}
			data = r;
		}
		Tera.data.SimpleStore.superclass.loadData.call(this, data, append);
	}
});

Tera.data.JsonStore = function(c) {
	Tera.data.JsonStore.superclass.constructor.call(this, Tera.apply(c, {
		proxy :!c.data ? new Tera.data.HttpProxy( {
			url :c.url
		}) : undefined,
		reader :new Tera.data.JsonReader(c, c.fields)
	}));
};
Tera.extend(Tera.data.JsonStore, Tera.data.Store);

Tera.data.Field = function(config) {
	if (typeof config == "string") {
		config = {
			name :config
		};
	}
	Tera.apply(this, config);

	if (!this.type) {
		this.type = "auto";
	}

	var st = Tera.data.SortTypes;

	if (typeof this.sortType == "string") {
		this.sortType = st[this.sortType];
	}

	if (!this.sortType) {
		switch (this.type) {
			case "string":
				this.sortType = st.asUCString;
				break;
			case "date":
				this.sortType = st.asDate;
				break;
			default:
				this.sortType = st.none;
		}
	}

	var stripRe = /[\$,%]/g;

	if (!this.convert) {
		var cv, dateFormat = this.dateFormat;
		switch (this.type) {
			case "":
			case "auto":
			case undefined:
				cv = function(v) {
					return v;
				};
				break;
			case "string":
				cv = function(v) {
					return (v === undefined || v === null) ? '' : String(v);
				};
				break;
			case "int":
				cv = function(v) {
					return v !== undefined && v !== null && v !== '' ? parseInt(String(v).replace(stripRe, ""), 10) : '';
				};
				break;
			case "float":
				cv = function(v) {
					return v !== undefined && v !== null && v !== '' ? parseFloat(String(v).replace(stripRe, ""), 10) : '';
				};
				break;
			case "bool":
			case "boolean":
				cv = function(v) {
					return v === true || v === "true" || v == 1;
				};
				break;
			case "date":
				cv = function(v) {
					if (!v) {
						return '';
					}
					if (Tera.isDate(v)) {
						return v;
					}
					if (dateFormat) {
						if (dateFormat == "timestamp") {
							return new Date(v * 1000);
						}
						if (dateFormat == "time") {
							return new Date(parseInt(v, 10));
						}
						return Date.parseDate(v, dateFormat);
					}
					var parsed = Date.parse(v);
					return parsed ? new Date(parsed) : null;
				};
				break;

		}
		this.convert = cv;
	}
};

Tera.data.Field.prototype = {
	dateFormat :null,
	defaultValue :"",
	mapping :null,
	sortType :null,
	sortDir :"ASC"
};

Tera.data.DataReader = function(meta, recordType) {

	this.meta = meta;
	this.recordType = Tera.isArray(recordType) ? Tera.data.Record.create(recordType) : recordType;
};

Tera.data.DataReader.prototype = {

};

Tera.data.DataProxy = function() {
	this.addEvents(

	'beforeload',

	'load');
	Tera.data.DataProxy.superclass.constructor.call(this);
};

Tera.extend(Tera.data.DataProxy, Tera.util.Observable);

Tera.data.MemoryProxy = function(data) {
	Tera.data.MemoryProxy.superclass.constructor.call(this);
	this.data = data;
};

Tera.extend(Tera.data.MemoryProxy, Tera.data.DataProxy, {

	load : function(params, reader, callback, scope, arg) {
		params = params || {};
		var result;
		try {
			result = reader.readRecords(this.data);
		} catch (e) {
			this.fireEvent("loadexception", this, arg, null, e);
			callback.call(scope, null, arg, false);
			return;
		}
		callback.call(scope, result, arg, true);
	},

	update : function(params, records) {

	}
});

Tera.data.HttpProxy = function(conn) {
	Tera.data.HttpProxy.superclass.constructor.call(this);

	this.conn = conn;
	this.useAjax = !conn || !conn.events;

};

Tera.extend(Tera.data.HttpProxy, Tera.data.DataProxy, {

	getConnection : function() {
		return this.useAjax ? Tera.Ajax : this.conn;
	},

	load : function(params, reader, callback, scope, arg) {
		if (this.fireEvent("beforeload", this, params) !== false) {
			var o = {
				params :params || {},
				request : {
					callback :callback,
					scope :scope,
					arg :arg
				},
				reader :reader,
				callback :this.loadResponse,
				scope :this
			};
			if (this.useAjax) {
				Tera.applyIf(o, this.conn);
				if (this.activeRequest) {
					Tera.Ajax.abort(this.activeRequest);
				}
				this.activeRequest = Tera.Ajax.request(o);
			} else {
				this.conn.request(o);
			}
		} else {
			callback.call(scope || this, null, arg, false);
		}
	},

	loadResponse : function(o, success, response) {
		delete this.activeRequest;
		if (!success) {
			this.fireEvent("loadexception", this, o, response);
			o.request.callback.call(o.request.scope, null, o.request.arg, false);
			return;
		}
		var result;
		try {
			result = o.reader.read(response);
		} catch (e) {
			this.fireEvent("loadexception", this, o, response, e);
			o.request.callback.call(o.request.scope, null, o.request.arg, false);
			return;
		}
		this.fireEvent("load", this, o, o.request.arg);
		o.request.callback.call(o.request.scope, result, o.request.arg, true);
	},

	update : function(dataSet) {

	},

	updateResponse : function(dataSet) {

	}
});

Tera.data.ScriptTagProxy = function(config) {
	Tera.data.ScriptTagProxy.superclass.constructor.call(this);
	Tera.apply(this, config);
	this.head = document.getElementsByTagName("head")[0];

};

Tera.data.ScriptTagProxy.TRANS_ID = 1000;

Tera.extend(Tera.data.ScriptTagProxy, Tera.data.DataProxy, {

	timeout : 300000,

	callbackParam :"callback",

	nocache :true,

	load : function(params, reader, callback, scope, arg) {
		if (this.fireEvent("beforeload", this, params) !== false) {

			var p = Tera.urlEncode(Tera.apply(params, this.extraParams));

			var url = this.url;
			url += (url.indexOf("?") != -1 ? "&" : "?") + p;
			if (this.nocache) {
				url += "&_dc=" + (new Date().getTime());
			}
			var transId = ++Tera.data.ScriptTagProxy.TRANS_ID;
			var trans = {
				id :transId,
				cb :"stcCallback" + transId,
				scriptId :"stcScript" + transId,
				params :params,
				arg :arg,
				url :url,
				callback :callback,
				scope :scope,
				reader :reader
			};
			var conn = this;

			window[trans.cb] = function(o) {
				conn.handleResponse(o, trans);
			};

			url += String.format("&{0}={1}", this.callbackParam, trans.cb);

			if (this.autoAbort !== false) {
				this.abort();
			}

			trans.timeoutId = this.handleFailure.defer(this.timeout, this, [ trans ]);

			var script = document.createElement("script");
			script.setAttribute("src", url);
			script.setAttribute("type", "text/javascript");
			script.setAttribute("id", trans.scriptId);
			this.head.appendChild(script);

			this.trans = trans;
		} else {
			callback.call(scope || this, null, arg, false);
		}
	},

	isLoading : function() {
		return this.trans ? true : false;
	},

	abort : function() {
		if (this.isLoading()) {
			this.destroyTrans(this.trans);
		}
	},

	destroyTrans : function(trans, isLoaded) {
		this.head.removeChild(document.getElementById(trans.scriptId));
		clearTimeout(trans.timeoutId);
		if (isLoaded) {
			window[trans.cb] = undefined;
			try {
				delete window[trans.cb];
			} catch (e) {
			}
		} else {

			window[trans.cb] = function() {
				window[trans.cb] = undefined;
				try {
					delete window[trans.cb];
				} catch (e) {
				}
			};
		}
	},

	handleResponse : function(o, trans) {
		this.trans = false;
		this.destroyTrans(trans, true);
		var result;
		try {
			result = trans.reader.readRecords(o);
		} catch (e) {
			this.fireEvent("loadexception", this, o, trans.arg, e);
			trans.callback.call(trans.scope || window, null, trans.arg, false);
			return;
		}
		this.fireEvent("load", this, o, trans.arg);
		trans.callback.call(trans.scope || window, result, trans.arg, true);
	},

	handleFailure : function(trans) {
		this.trans = false;
		this.destroyTrans(trans, false);
		this.fireEvent("loadexception", this, null, trans.arg);
		trans.callback.call(trans.scope || window, null, trans.arg, false);
	}
});

Tera.data.JsonReader = function(meta, recordType) {
	meta = meta || {};
	Tera.data.JsonReader.superclass.constructor.call(this, meta, recordType || meta.fields);
};
Tera.extend(Tera.data.JsonReader, Tera.data.DataReader, {

	read : function(response) {
		var json = response.responseText;
		var o = eval("(" + json + ")");
		if (!o) {
			throw {
				message :"JsonReader.read: Json object not found"
			};
		}
		return this.readRecords(o);
	},

	onMetaChange : function(meta, recordType, o) {

	},

	simpleAccess : function(obj, subsc) {
		return obj[subsc];
	},

	getJsonAccessor : function() {
		var re = /[\[\.]/;
		return function(expr) {
			try {
				return (re.test(expr)) ? new Function("obj", "return obj." + expr) : function(obj) {
					return obj[expr];
				};
			} catch (e) {
			}
			return Tera.emptyFn;
		};
	}(),

	readRecords : function(o) {

		this.jsonData = o;
		if (o.metaData) {
			delete this.ef;
			this.meta = o.metaData;
			this.recordType = Tera.data.Record.create(o.metaData.fields);
			this.onMetaChange(this.meta, this.recordType, o);
		}
		var s = this.meta, Record = this.recordType, f = Record.prototype.fields, fi = f.items, fl = f.length;

		if (!this.ef) {
			if (s.totalProperty) {
				this.getTotal = this.getJsonAccessor(s.totalProperty);
			}
			if (s.successProperty) {
				this.getSuccess = this.getJsonAccessor(s.successProperty);
			}
			this.getRoot = s.root ? this.getJsonAccessor(s.root) : function(p) {
				return p;
			};
			if (s.id) {
				var g = this.getJsonAccessor(s.id);
				this.getId = function(rec) {
					var r = g(rec);
					return (r === undefined || r === "") ? null : r;
				};
			} else {
				this.getId = function() {
					return null;
				};
			}
			this.ef = [];
			for ( var i = 0; i < fl; i++) {
				f = fi[i];
				var map = (f.mapping !== undefined && f.mapping !== null) ? f.mapping : f.name;
				this.ef[i] = this.getJsonAccessor(map);
			}
		}

		var root = this.getRoot(o), c = root.length, totalRecords = c, success = true;
		if (s.totalProperty) {
			var v = parseInt(this.getTotal(o), 10);
			if (!isNaN(v)) {
				totalRecords = v;
			}
		}
		if (s.successProperty) {
			var v = this.getSuccess(o);
			if (v === false || v === 'false') {
				success = false;
			}
		}
		var records = [];
		for ( var i = 0; i < c; i++) {
			var n = root[i];
			var values = {};
			var id = this.getId(n);
			for ( var j = 0; j < fl; j++) {
				f = fi[j];
				var v = this.ef[j](n);
				values[f.name] = f.convert((v !== undefined) ? v : f.defaultValue, n);
			}
			var record = new Record(values, id);
			record.json = n;
			records[i] = record;
		}
		return {
			success :success,
			records :records,
			totalRecords :totalRecords
		};
	}
});

Tera.data.XmlReader = function(meta, recordType) {
	meta = meta || {};
	Tera.data.XmlReader.superclass.constructor.call(this, meta, recordType || meta.fields);
};
Tera.extend(Tera.data.XmlReader, Tera.data.DataReader, {

	read : function(response) {
		var doc = response.responseXML;
		if (!doc) {
			throw {
				message :"XmlReader.read: XML Document not available"
			};
		}
		return this.readRecords(doc);
	},

	readRecords : function(doc) {

		this.xmlData = doc;
		var root = doc.documentElement || doc;
		var q = Tera.DomQuery;
		var recordType = this.recordType, fields = recordType.prototype.fields;
		var sid = this.meta.id;
		var totalRecords = 0, success = true;
		if (this.meta.totalRecords) {
			totalRecords = q.selectNumber(this.meta.totalRecords, root, 0);
		}

		if (this.meta.success) {
			var sv = q.selectValue(this.meta.success, root, true);
			success = sv !== false && sv !== 'false';
		}
		var records = [];
		var ns = q.select(this.meta.record, root);
		for ( var i = 0, len = ns.length; i < len; i++) {
			var n = ns[i];
			var values = {};
			var id = sid ? q.selectValue(sid, n) : undefined;
			for ( var j = 0, jlen = fields.length; j < jlen; j++) {
				var f = fields.items[j];
				var v = q.selectValue(f.mapping || f.name, n, f.defaultValue);
				v = f.convert(v, n);
				values[f.name] = v;
			}
			var record = new recordType(values, id);
			record.node = n;
			records[records.length] = record;
		}

		return {
			success :success,
			records :records,
			totalRecords :totalRecords || records.length
		};
	}
});

Tera.data.ArrayReader = Tera.extend(Tera.data.JsonReader, {

	readRecords : function(o) {
		var sid = this.meta ? this.meta.id : null;
		var recordType = this.recordType, fields = recordType.prototype.fields;
		var records = [];
		var root = o;
		for ( var i = 0; i < root.length; i++) {
			var n = root[i];
			var values = {};
			var id = ((sid || sid === 0) && n[sid] !== undefined && n[sid] !== "" ? n[sid] : null);
			for ( var j = 0, jlen = fields.length; j < jlen; j++) {
				var f = fields.items[j];
				var k = f.mapping !== undefined && f.mapping !== null ? f.mapping : j;
				var v = n[k] !== undefined ? n[k] : f.defaultValue;
				v = f.convert(v, n);
				values[f.name] = v;
			}
			var record = new recordType(values, id);
			record.json = n;
			records[records.length] = record;
		}
		return {
			records :records,
			totalRecords :records.length
		};
	}
});

Tera.data.Tree = function(root) {
	this.nodeHash = {};

	this.root = null;
	if (root) {
		this.setRootNode(root);
	}
	this.addEvents(

	"append",

	"remove",

	"move",

	"insert",

	"beforeappend",

	"beforeremove",

	"beforemove",

	"beforeinsert");

	Tera.data.Tree.superclass.constructor.call(this);
};

Tera.extend(Tera.data.Tree, Tera.util.Observable, {

	pathSeparator :"/",

	proxyNodeEvent : function() {
		return this.fireEvent.apply(this, arguments);
	},

	getRootNode : function() {
		return this.root;
	},

	setRootNode : function(node) {
		this.root = node;
		node.ownerTree = this;
		node.isRoot = true;
		this.registerNode(node);
		return node;
	},

	getNodeById : function(id) {
		return this.nodeHash[id];
	},

	registerNode : function(node) {
		this.nodeHash[node.id] = node;
	},

	unregisterNode : function(node) {
		delete this.nodeHash[node.id];
	},

	toString : function() {
		return "[Tree" + (this.id ? " " + this.id : "") + "]";
	}
});

Tera.data.Node = function(attributes) {

	this.attributes = attributes || {};
	this.leaf = this.attributes.leaf;

	this.id = this.attributes.id;
	if (!this.id) {
		this.id = Tera.id(null, "ynode-");
		this.attributes.id = this.id;
	}

	this.childNodes = [];
	if (!this.childNodes.indexOf) {
		this.childNodes.indexOf = function(o) {
			for ( var i = 0, len = this.length; i < len; i++) {
				if (this[i] == o)
					return i;
			}
			return -1;
		};
	}

	this.parentNode = null;

	this.firstChild = null;

	this.lastChild = null;

	this.previousSibling = null;

	this.nextSibling = null;

	this.addEvents( {

		"append" :true,

		"remove" :true,

		"move" :true,

		"insert" :true,

		"beforeappend" :true,

		"beforeremove" :true,

		"beforemove" :true,

		"beforeinsert" :true
	});
	this.listeners = this.attributes.listeners;
	Tera.data.Node.superclass.constructor.call(this);
};

Tera.extend(Tera.data.Node, Tera.util.Observable, {

	fireEvent : function(evtName) {

		if (Tera.data.Node.superclass.fireEvent.apply(this, arguments) === false) {
			return false;
		}

		var ot = this.getOwnerTree();
		if (ot) {
			if (ot.proxyNodeEvent.apply(ot, arguments) === false) {
				return false;
			}
		}
		return true;
	},

	isLeaf : function() {
		return this.leaf === true;
	},

	setFirstChild : function(node) {
		this.firstChild = node;
	},

	setLastChild : function(node) {
		this.lastChild = node;
	},

	isLast : function() {
		return (!this.parentNode ? true : this.parentNode.lastChild == this);
	},

	isFirst : function() {
		return (!this.parentNode ? true : this.parentNode.firstChild == this);
	},

	hasChildNodes : function() {
		return !this.isLeaf() && this.childNodes.length > 0;
	},

	appendChild : function(node) {
		var multi = false;
		if (Tera.isArray(node)) {
			multi = node;
		} else if (arguments.length > 1) {
			multi = arguments;
		}

		if (multi) {
			for ( var i = 0, len = multi.length; i < len; i++) {
				this.appendChild(multi[i]);
			}
		} else {
			if (this.fireEvent("beforeappend", this.ownerTree, this, node) === false) {
				return false;
			}
			var index = this.childNodes.length;
			var oldParent = node.parentNode;

			if (oldParent) {
				if (node.fireEvent("beforemove", node.getOwnerTree(), node, oldParent, this, index) === false) {
					return false;
				}
				oldParent.removeChild(node);
			}
			index = this.childNodes.length;
			if (index == 0) {
				this.setFirstChild(node);
			}
			this.childNodes.push(node);
			node.parentNode = this;
			var ps = this.childNodes[index - 1];
			if (ps) {
				node.previousSibling = ps;
				ps.nextSibling = node;
			} else {
				node.previousSibling = null;
			}
			node.nextSibling = null;
			this.setLastChild(node);
			node.setOwnerTree(this.getOwnerTree());
			this.fireEvent("append", this.ownerTree, this, node, index);
			if (oldParent) {
				node.fireEvent("move", this.ownerTree, node, oldParent, this, index);
			}
			return node;
		}
	},

	removeChild : function(node) {
		var index = this.childNodes.indexOf(node);
		if (index == -1) {
			return false;
		}
		if (this.fireEvent("beforeremove", this.ownerTree, this, node) === false) {
			return false;
		}

		this.childNodes.splice(index, 1);

		if (node.previousSibling) {
			node.previousSibling.nextSibling = node.nextSibling;
		}
		if (node.nextSibling) {
			node.nextSibling.previousSibling = node.previousSibling;
		}

		if (this.firstChild == node) {
			this.setFirstChild(node.nextSibling);
		}
		if (this.lastChild == node) {
			this.setLastChild(node.previousSibling);
		}

		node.setOwnerTree(null);

		node.parentNode = null;
		node.previousSibling = null;
		node.nextSibling = null;
		this.fireEvent("remove", this.ownerTree, this, node);
		return node;
	},

	insertBefore : function(node, refNode) {
		if (!refNode) {
			return this.appendChild(node);
		}

		if (node == refNode) {
			return false;
		}

		if (this.fireEvent("beforeinsert", this.ownerTree, this, node, refNode) === false) {
			return false;
		}
		var index = this.childNodes.indexOf(refNode);
		var oldParent = node.parentNode;
		var refIndex = index;

		if (oldParent == this && this.childNodes.indexOf(node) < index) {
			refIndex--;
		}

		if (oldParent) {
			if (node.fireEvent("beforemove", node.getOwnerTree(), node, oldParent, this, index, refNode) === false) {
				return false;
			}
			oldParent.removeChild(node);
		}
		if (refIndex == 0) {
			this.setFirstChild(node);
		}
		this.childNodes.splice(refIndex, 0, node);
		node.parentNode = this;
		var ps = this.childNodes[refIndex - 1];
		if (ps) {
			node.previousSibling = ps;
			ps.nextSibling = node;
		} else {
			node.previousSibling = null;
		}
		node.nextSibling = refNode;
		refNode.previousSibling = node;
		node.setOwnerTree(this.getOwnerTree());
		this.fireEvent("insert", this.ownerTree, this, node, refNode);
		if (oldParent) {
			node.fireEvent("move", this.ownerTree, node, oldParent, this, refIndex, refNode);
		}
		return node;
	},

	remove : function() {
		this.parentNode.removeChild(this);
		return this;
	},

	item : function(index) {
		return this.childNodes[index];
	},

	replaceChild : function(newChild, oldChild) {
		this.insertBefore(newChild, oldChild);
		this.removeChild(oldChild);
		return oldChild;
	},

	indexOf : function(child) {
		return this.childNodes.indexOf(child);
	},

	getOwnerTree : function() {

		if (!this.ownerTree) {
			var p = this;
			while (p) {
				if (p.ownerTree) {
					this.ownerTree = p.ownerTree;
					break;
				}
				p = p.parentNode;
			}
		}
		return this.ownerTree;
	},

	getDepth : function() {
		var depth = 0;
		var p = this;
		while (p.parentNode) {
			++depth;
			p = p.parentNode;
		}
		return depth;
	},

	setOwnerTree : function(tree) {

		if (tree != this.ownerTree) {
			if (this.ownerTree) {
				this.ownerTree.unregisterNode(this);
			}
			this.ownerTree = tree;
			var cs = this.childNodes;
			for ( var i = 0, len = cs.length; i < len; i++) {
				cs[i].setOwnerTree(tree);
			}
			if (tree) {
				tree.registerNode(this);
			}
		}
	},

	getPath : function(attr) {
		attr = attr || "id";
		var p = this.parentNode;
		var b = [ this.attributes[attr] ];
		while (p) {
			b.unshift(p.attributes[attr]);
			p = p.parentNode;
		}
		var sep = this.getOwnerTree().pathSeparator;
		return sep + b.join(sep);
	},

	bubble : function(fn, scope, args) {
		var p = this;
		while (p) {
			if (fn.apply(scope || p, args || [ p ]) === false) {
				break;
			}
			p = p.parentNode;
		}
	},

	cascade : function(fn, scope, args) {
		if (fn.apply(scope || this, args || [ this ]) !== false) {
			var cs = this.childNodes;
			for ( var i = 0, len = cs.length; i < len; i++) {
				cs[i].cascade(fn, scope, args);
			}
		}
	},

	eachChild : function(fn, scope, args) {
		var cs = this.childNodes;
		for ( var i = 0, len = cs.length; i < len; i++) {
			if (fn.apply(scope || this, args || [ cs[i] ]) === false) {
				break;
			}
		}
	},

	findChild : function(attribute, value) {
		var cs = this.childNodes;
		for ( var i = 0, len = cs.length; i < len; i++) {
			if (cs[i].attributes[attribute] == value) {
				return cs[i];
			}
		}
		return null;
	},

	findChildBy : function(fn, scope) {
		var cs = this.childNodes;
		for ( var i = 0, len = cs.length; i < len; i++) {
			if (fn.call(scope || cs[i], cs[i]) === true) {
				return cs[i];
			}
		}
		return null;
	},

	sort : function(fn, scope) {
		var cs = this.childNodes;
		var len = cs.length;
		if (len > 0) {
			var sortFn = scope ? function() {
				fn.apply(scope, arguments);
			} : fn;
			cs.sort(sortFn);
			for ( var i = 0; i < len; i++) {
				var n = cs[i];
				n.previousSibling = cs[i - 1];
				n.nextSibling = cs[i + 1];
				if (i == 0) {
					this.setFirstChild(n);
				}
				if (i == len - 1) {
					this.setLastChild(n);
				}
			}
		}
	},

	contains : function(node) {
		return node.isAncestor(this);
	},

	isAncestor : function(node) {
		var p = this.parentNode;
		while (p) {
			if (p == node) {
				return true;
			}
			p = p.parentNode;
		}
		return false;
	},

	toString : function() {
		return "[Node" + (this.id ? " " + this.id : "") + "]";
	}
});

Tera.data.GroupingStore = Tera.extend(Tera.data.Store, {

	remoteGroup :false,

	groupOnSort :false,

	clearGrouping : function() {
		this.groupField = false;
		if (this.remoteGroup) {
			if (this.baseParams) {
				delete this.baseParams.groupBy;
			}
			this.reload();
		} else {
			this.applySort();
			this.fireEvent('datachanged', this);
		}
	},

	groupBy : function(field, forceRegroup) {
		if (this.groupField == field && !forceRegroup) {
			return;
		}
		this.groupField = field;
		if (this.remoteGroup) {
			if (!this.baseParams) {
				this.baseParams = {};
			}
			this.baseParams['groupBy'] = field;
		}
		if (this.groupOnSort) {
			this.sort(field);
			return;
		}
		if (this.remoteGroup) {
			this.reload();
		} else {
			var si = this.sortInfo || {};
			if (si.field != field) {
				this.applySort();
			} else {
				this.sortData(field);
			}
			this.fireEvent('datachanged', this);
		}
	},

	applySort : function() {
		Tera.data.GroupingStore.superclass.applySort.call(this);
		if (!this.groupOnSort && !this.remoteGroup) {
			var gs = this.getGroupState();
			if (gs && gs != this.sortInfo.field) {
				this.sortData(this.groupField);
			}
		}
	},

	applyGrouping : function(alwaysFireChange) {
		if (this.groupField !== false) {
			this.groupBy(this.groupField, true);
			return true;
		} else {
			if (alwaysFireChange === true) {
				this.fireEvent('datachanged', this);
			}
			return false;
		}
	},

	getGroupState : function() {
		return this.groupOnSort && this.groupField !== false ? (this.sortInfo ? this.sortInfo.field : undefined) : this.groupField;
	}
});

Tera.ComponentMgr = function() {
	var all = new Tera.util.MixedCollection();
	var types = {};

	return {

		register : function(c) {
			all.add(c);
		},

		unregister : function(c) {
			all.remove(c);
		},

		get : function(id) {
			return all.get(id);
		},

		onAvailable : function(id, fn, scope) {
			all.on("add", function(index, o) {
				if (o.id == id) {
					fn.call(scope || o, o);
					all.un("add", fn, scope);
				}
			});
		},

		all :all,

		registerType : function(xtype, cls) {
			types[xtype] = cls;
			cls.xtype = xtype;
		},

		create : function(config, defaultType) {
			return new types[config.xtype || defaultType](config);
		}
	};
}();

Tera.reg = Tera.ComponentMgr.registerType; // this will be called a lot
											// internally, shorthand to keep the
											// bytes down

Tera.Component = function(config) {
	config = config || {};
	if (config.initialConfig) {
		if (config.isAction) {
			this.baseAction = config;
		}
		config = config.initialConfig;
	} else if (config.tagName || config.dom || typeof config == "string") {
		config = {
			applyTo :config,
			id :config.id || config
		};
	}

	this.initialConfig = config;

	Tera.apply(this, config);
	this.addEvents(

	'disable',

	'enable',

	'beforeshow',

	'show',

	'beforehide',

	'hide',

	'beforerender',

	'render',

	'beforedestroy',

	'destroy',

	'beforestaterestore',

	'staterestore',

	'beforestatesave',

	'statesave');
	this.getId();
	Tera.ComponentMgr.register(this);
	Tera.Component.superclass.constructor.call(this);

	if (this.baseAction) {
		this.baseAction.addComponent(this);
	}

	this.initComponent();

	if (this.plugins) {
		if (Tera.isArray(this.plugins)) {
			for ( var i = 0, len = this.plugins.length; i < len; i++) {
				this.plugins[i].init(this);
			}
		} else {
			this.plugins.init(this);
		}
	}

	if (this.stateful !== false) {
		this.initState(config);
	}

	if (this.applyTo) {
		this.applyToMarkup(this.applyTo);
		delete this.applyTo;
	} else if (this.renderTo) {
		this.render(this.renderTo);
		delete this.renderTo;
	}
};

Tera.Component.AUTO_ID = 1000;

Tera.extend(Tera.Component, Tera.util.Observable, {

	disabledClass :"x-item-disabled",

	allowDomMove :true,

	autoShow :false,

	hideMode :'display',

	hideParent :false,

	hidden :false,

	disabled :false,

	rendered :false,

	ctype :"Tera.Component",

	actionMode :"el",

	getActionEl : function() {
		return this[this.actionMode];
	},

	initComponent :Tera.emptyFn,

	render : function(container, position) {
		if (!this.rendered && this.fireEvent("beforerender", this) !== false) {
			if (!container && this.el) {
				this.el = Tera.get(this.el);
				container = this.el.dom.parentNode;
				this.allowDomMove = false;
			}
			this.container = Tera.get(container);
			if (this.ctCls) {
				this.container.addClass(this.ctCls);
			}
			this.rendered = true;
			if (position !== undefined) {
				if (typeof position == 'number') {
					position = this.container.dom.childNodes[position];
				} else {
					position = Tera.getDom(position);
				}
			}
			this.onRender(this.container, position || null);
			if (this.autoShow) {
				this.el.removeClass( [ 'x-hidden', 'x-hide-' + this.hideMode ]);
			}
			if (this.cls) {
				this.el.addClass(this.cls);
				delete this.cls;
			}
			if (this.style) {
				this.el.applyStyles(this.style);
				delete this.style;
			}
			this.fireEvent("render", this);
			this.afterRender(this.container);
			if (this.hidden) {
				this.hide();
			}
			if (this.disabled) {
				this.disable();
			}

			this.initStateEvents();
		}
		return this;
	},

	initState : function(config) {
		if (Tera.state.Manager) {
			var state = Tera.state.Manager.get(this.stateId || this.id);
			if (state) {
				if (this.fireEvent('beforestaterestore', this, state) !== false) {
					this.applyState(state);
					this.fireEvent('staterestore', this, state);
				}
			}
		}
	},

	initStateEvents : function() {
		if (this.stateEvents) {
			for ( var i = 0, e; e = this.stateEvents[i]; i++) {
				this.on(e, this.saveState, this, {
					delay :100
				});
			}
		}
	},

	applyState : function(state, config) {
		if (state) {
			Tera.apply(this, state);
		}
	},

	getState : function() {
		return null;
	},

	saveState : function() {
		if (Tera.state.Manager) {
			var state = this.getState();
			if (this.fireEvent('beforestatesave', this, state) !== false) {
				Tera.state.Manager.set(this.stateId || this.id, state);
				this.fireEvent('statesave', this, state);
			}
		}
	},

	applyToMarkup : function(el) {
		this.allowDomMove = false;
		this.el = Tera.get(el);
		this.render(this.el.dom.parentNode);
	},

	addClass : function(cls) {
		if (this.el) {
			this.el.addClass(cls);
		} else {
			this.cls = this.cls ? this.cls + ' ' + cls : cls;
		}
	},

	removeClass : function(cls) {
		if (this.el) {
			this.el.removeClass(cls);
		} else if (this.cls) {
			this.cls = this.cls.split(' ').remove(cls).join(' ');
		}
	},

	onRender : function(ct, position) {
		if (this.autoEl) {
			if (typeof this.autoEl == 'string') {
				this.el = document.createElement(this.autoEl);
			} else {
				var div = document.createElement('div');
				Tera.DomHelper.overwrite(div, this.autoEl);
				this.el = div.firstChild;
			}
			if (!this.el.id) {
				this.el.id = this.getId();
			}
		}
		if (this.el) {
			this.el = Tera.get(this.el);
			if (this.allowDomMove !== false) {
				ct.dom.insertBefore(this.el.dom, position);
			}
			if (this.overCls) {
				this.el.addClassOnOver(this.overCls);
			}
		}
	},

	getAutoCreate : function() {
		var cfg = typeof this.autoCreate == "object" ? this.autoCreate : Tera.apply( {}, this.defaultAutoCreate);
		if (this.id && !cfg.id) {
			cfg.id = this.id;
		}
		return cfg;
	},

	afterRender :Tera.emptyFn,

	destroy : function() {
		if (this.fireEvent("beforedestroy", this) !== false) {
			this.beforeDestroy();
			if (this.rendered) {
				this.el.removeAllListeners();
				this.el.remove();
				if (this.actionMode == "container") {
					this.container.remove();
				}
			}
			this.onDestroy();
			Tera.ComponentMgr.unregister(this);
			this.fireEvent("destroy", this);
			this.purgeListeners();
		}
	},

	beforeDestroy :Tera.emptyFn,

	onDestroy :Tera.emptyFn,

	getEl : function() {
		return this.el;
	},

	getId : function() {
		return this.id || (this.id = "ext-comp-" + (++Tera.Component.AUTO_ID));
	},

	getItemId : function() {
		return this.itemId || this.getId();
	},

	focus : function(selectText, delay) {
		if (delay) {
			this.focus.defer(typeof delay == 'number' ? delay : 10, this, [ selectText, false ]);
			return;
		}
		if (this.rendered) {
			this.el.focus();
			if (selectText === true) {
				this.el.dom.select();
			}
		}
		return this;
	},

	blur : function() {
		if (this.rendered) {
			this.el.blur();
		}
		return this;
	},

	disable : function() {
		if (this.rendered) {
			this.onDisable();
		}
		this.disabled = true;
		this.fireEvent("disable", this);
		return this;
	},

	onDisable : function() {
		this.getActionEl().addClass(this.disabledClass);
		this.el.dom.disabled = true;
	},

	enable : function() {
		if (this.rendered) {
			this.onEnable();
		}
		this.disabled = false;
		this.fireEvent("enable", this);
		return this;
	},

	onEnable : function() {
		this.getActionEl().removeClass(this.disabledClass);
		this.el.dom.disabled = false;
	},

	setDisabled : function(disabled) {
		this[disabled ? "disable" : "enable"]();
	},

	show : function() {
		if (this.fireEvent("beforeshow", this) !== false) {
			this.hidden = false;
			if (this.autoRender) {
				this.render(typeof this.autoRender == 'boolean' ? Tera.getBody() : this.autoRender);
			}
			if (this.rendered) {
				this.onShow();
			}
			this.fireEvent("show", this);
		}
		return this;
	},

	onShow : function() {
		if (this.hideParent) {
			this.container.removeClass('x-hide-' + this.hideMode);
		} else {
			this.getActionEl().removeClass('x-hide-' + this.hideMode);
		}

	},

	hide : function() {
		if (this.fireEvent("beforehide", this) !== false) {
			this.hidden = true;
			if (this.rendered) {
				this.onHide();
			}
			this.fireEvent("hide", this);
		}
		return this;
	},

	onHide : function() {
		if (this.hideParent) {
			this.container.addClass('x-hide-' + this.hideMode);
		} else {
			this.getActionEl().addClass('x-hide-' + this.hideMode);
		}
	},

	setVisible : function(visible) {
		if (visible) {
			this.show();
		} else {
			this.hide();
		}
		return this;
	},

	isVisible : function() {
		return this.rendered && this.getActionEl().isVisible();
	},

	cloneConfig : function(overrides) {
		overrides = overrides || {};
		var id = overrides.id || Tera.id();
		var cfg = Tera.applyIf(overrides, this.initialConfig);
		cfg.id = id;
		return new this.constructor(cfg);
	},

	getXType : function() {
		return this.constructor.xtype;
	},

	isXType : function(xtype, shallow) {
		return !shallow ? ('/' + this.getXTypes() + '/').indexOf('/' + xtype + '/') != -1 : this.constructor.xtype == xtype;
	},

	getXTypes : function() {
		var tc = this.constructor;
		if (!tc.xtypes) {
			var c = [], sc = this;
			while (sc && sc.constructor.xtype) {
				c.unshift(sc.constructor.xtype);
				sc = sc.constructor.superclass;
			}
			tc.xtypeChain = c;
			tc.xtypes = c.join('/');
		}
		return tc.xtypes;
	},

	findParentBy : function(fn) {
		for ( var p = this.ownerCt; (p != null) && !fn(p, this); p = p.ownerCt)
			;
		return p || null;
	},

	findParentByType : function(xtype) {
		return typeof xtype == 'function' ? this.findParentBy( function(p) {
			return p.constructor === xtype;
		}) : this.findParentBy( function(p) {
			return p.constructor.xtype === xtype;
		});
	},

	mon : function(item, ename, fn, scope, opt) {
		if (!this.mons) {
			this.mons = [];
			this.on('beforedestroy', function() {
				for ( var i = 0, len = this.mons.length; i < len; i++) {
					var m = this.mons[i];
					m.item.un(m.ename, m.fn, m.scope);
				}
			}, this);
		}
		this.mons.push( {
			item :item,
			ename :ename,
			fn :fn,
			scope :scope
		});
		item.on(ename, fn, scope, opt);
	}
});

Tera.reg('component', Tera.Component);

Tera.Action = function(config) {
	this.initialConfig = config;
	this.items = [];
}

Tera.Action.prototype = {

	isAction :true,

	setText : function(text) {
		this.initialConfig.text = text;
		this.callEach('setText', [ text ]);
	},

	getText : function() {
		return this.initialConfig.text;
	},

	setIconClass : function(cls) {
		this.initialConfig.iconCls = cls;
		this.callEach('setIconClass', [ cls ]);
	},

	getIconClass : function() {
		return this.initialConfig.iconCls;
	},

	setDisabled : function(v) {
		this.initialConfig.disabled = v;
		this.callEach('setDisabled', [ v ]);
	},

	enable : function() {
		this.setDisabled(false);
	},

	disable : function() {
		this.setDisabled(true);
	},

	isDisabled : function() {
		return this.initialConfig.disabled;
	},

	setHidden : function(v) {
		this.initialConfig.hidden = v;
		this.callEach('setVisible', [ !v ]);
	},

	show : function() {
		this.setHidden(false);
	},

	hide : function() {
		this.setHidden(true);
	},

	isHidden : function() {
		return this.initialConfig.hidden;
	},

	setHandler : function(fn, scope) {
		this.initialConfig.handler = fn;
		this.initialConfig.scope = scope;
		this.callEach('setHandler', [ fn, scope ]);
	},

	each : function(fn, scope) {
		Tera.each(this.items, fn, scope);
	},

	callEach : function(fnName, args) {
		var cs = this.items;
		for ( var i = 0, len = cs.length; i < len; i++) {
			cs[i][fnName].apply(cs[i], args);
		}
	},

	addComponent : function(comp) {
		this.items.push(comp);
		comp.on('destroy', this.removeComponent, this);
	},

	removeComponent : function(comp) {
		this.items.remove(comp);
	},

	execute : function() {
		this.initialConfig.handler.apply(this.initialConfig.scope || window, arguments);
	}
};

( function() {
	Tera.Layer = function(config, existingEl) {
		config = config || {};
		var dh = Tera.DomHelper;
		var cp = config.parentEl, pel = cp ? Tera.getDom(cp) : document.body;
		if (existingEl) {
			this.dom = Tera.getDom(existingEl);
		}
		if (!this.dom) {
			var o = config.dh || {
				tag :"div",
				cls :"x-layer"
			};
			this.dom = dh.append(pel, o);
		}
		if (config.cls) {
			this.addClass(config.cls);
		}
		this.constrain = config.constrain !== false;
		this.visibilityMode = Tera.Element.VISIBILITY;
		if (config.id) {
			this.id = this.dom.id = config.id;
		} else {
			this.id = Tera.id(this.dom);
		}
		this.zindex = config.zindex || this.getZIndex();
		this.position("absolute", this.zindex);
		if (config.shadow) {
			this.shadowOffset = config.shadowOffset || 4;
			this.shadow = new Tera.Shadow( {
				offset :this.shadowOffset,
				mode :config.shadow
			});
		} else {
			this.shadowOffset = 0;
		}
		this.useShim = config.shim !== false && Tera.useShims;
		this.useDisplay = config.useDisplay;
		this.hide();
	};

	var supr = Tera.Element.prototype;

	var shims = [];

	Tera.extend(Tera.Layer, Tera.Element, {

		getZIndex : function() {
			return this.zindex || parseInt(this.getStyle("z-index"), 10) || 11000;
		},

		getShim : function() {
			if (!this.useShim) {
				return null;
			}
			if (this.shim) {
				return this.shim;
			}
			var shim = shims.shift();
			if (!shim) {
				shim = this.createShim();
				shim.enableDisplayMode('block');
				shim.dom.style.display = 'none';
				shim.dom.style.visibility = 'visible';
			}
			var pn = this.dom.parentNode;
			if (shim.dom.parentNode != pn) {
				pn.insertBefore(shim.dom, this.dom);
			}
			shim.setStyle('z-index', this.getZIndex() - 2);
			this.shim = shim;
			return shim;
		},

		hideShim : function() {
			if (this.shim) {
				this.shim.setDisplayed(false);
				shims.push(this.shim);
				delete this.shim;
			}
		},

		disableShadow : function() {
			if (this.shadow) {
				this.shadowDisabled = true;
				this.shadow.hide();
				this.lastShadowOffset = this.shadowOffset;
				this.shadowOffset = 0;
			}
		},

		enableShadow : function(show) {
			if (this.shadow) {
				this.shadowDisabled = false;
				this.shadowOffset = this.lastShadowOffset;
				delete this.lastShadowOffset;
				if (show) {
					this.sync(true);
				}
			}
		},

		sync : function(doShow) {
			var sw = this.shadow;
			if (!this.updating && this.isVisible() && (sw || this.useShim)) {
				var sh = this.getShim();

				var w = this.getWidth(), h = this.getHeight();

				var l = this.getLeft(true), t = this.getTop(true);

				if (sw && !this.shadowDisabled) {
					if (doShow && !sw.isVisible()) {
						sw.show(this);
					} else {
						sw.realign(l, t, w, h);
					}
					if (sh) {
						if (doShow) {
							sh.show();
						}

						var a = sw.adjusts, s = sh.dom.style;
						s.left = (Math.min(l, l + a.l)) + "px";
						s.top = (Math.min(t, t + a.t)) + "px";
						s.width = (w + a.w) + "px";
						s.height = (h + a.h) + "px";
					}
				} else if (sh) {
					if (doShow) {
						sh.show();
					}
					sh.setSize(w, h);
					sh.setLeftTop(l, t);
				}

			}
		},

		destroy : function() {
			this.hideShim();
			if (this.shadow) {
				this.shadow.hide();
			}
			this.removeAllListeners();
			Tera.removeNode(this.dom);
			Tera.Element.uncache(this.id);
		},

		remove : function() {
			this.destroy();
		},

		beginUpdate : function() {
			this.updating = true;
		},

		endUpdate : function() {
			this.updating = false;
			this.sync(true);
		},

		hideUnders : function(negOffset) {
			if (this.shadow) {
				this.shadow.hide();
			}
			this.hideShim();
		},

		constrainXY : function() {
			if (this.constrain) {
				var vw = Tera.lib.Dom.getViewWidth(), vh = Tera.lib.Dom.getViewHeight();
				var s = Tera.getDoc().getScroll();

				var xy = this.getXY();
				var x = xy[0], y = xy[1];
				var w = this.dom.offsetWidth + this.shadowOffset, h = this.dom.offsetHeight + this.shadowOffset;

				var moved = false;

				if ((x + w) > vw + s.left) {
					x = vw - w - this.shadowOffset;
					moved = true;
				}
				if ((y + h) > vh + s.top) {
					y = vh - h - this.shadowOffset;
					moved = true;
				}

				if (x < s.left) {
					x = s.left;
					moved = true;
				}
				if (y < s.top) {
					y = s.top;
					moved = true;
				}
				if (moved) {
					if (this.avoidY) {
						var ay = this.avoidY;
						if (y <= ay && (y + h) >= ay) {
							y = ay - h - 5;
						}
					}
					xy = [ x, y ];
					this.storeXY(xy);
					supr.setXY.call(this, xy);
					this.sync();
				}
			}
		},

		isVisible : function() {
			return this.visible;
		},

		showAction : function() {
			this.visible = true;
			if (this.useDisplay === true) {
				this.setDisplayed("");
			} else if (this.lastXY) {
				supr.setXY.call(this, this.lastXY);
			} else if (this.lastLT) {
				supr.setLeftTop.call(this, this.lastLT[0], this.lastLT[1]);
			}
		},

		hideAction : function() {
			this.visible = false;
			if (this.useDisplay === true) {
				this.setDisplayed(false);
			} else {
				this.setLeftTop(-10000, -10000);
			}
		},

		setVisible : function(v, a, d, c, e) {
			if (v) {
				this.showAction();
			}
			if (a && v) {
				var cb = function() {
					this.sync(true);
					if (c) {
						c();
					}
				}.createDelegate(this);
				supr.setVisible.call(this, true, true, d, cb, e);
			} else {
				if (!v) {
					this.hideUnders(true);
				}
				var cb = c;
				if (a) {
					cb = function() {
						this.hideAction();
						if (c) {
							c();
						}
					}.createDelegate(this);
				}
				supr.setVisible.call(this, v, a, d, cb, e);
				if (v) {
					this.sync(true);
				} else if (!a) {
					this.hideAction();
				}
			}
		},

		storeXY : function(xy) {
			delete this.lastLT;
			this.lastXY = xy;
		},

		storeLeftTop : function(left, top) {
			delete this.lastXY;
			this.lastLT = [ left, top ];
		},

		beforeFx : function() {
			this.beforeAction();
			return Tera.Layer.superclass.beforeFx.apply(this, arguments);
		},

		afterFx : function() {
			Tera.Layer.superclass.afterFx.apply(this, arguments);
			this.sync(this.isVisible());
		},

		beforeAction : function() {
			if (!this.updating && this.shadow) {
				this.shadow.hide();
			}
		},

		setLeft : function(left) {
			this.storeLeftTop(left, this.getTop(true));
			supr.setLeft.apply(this, arguments);
			this.sync();
		},

		setTop : function(top) {
			this.storeLeftTop(this.getLeft(true), top);
			supr.setTop.apply(this, arguments);
			this.sync();
		},

		setLeftTop : function(left, top) {
			this.storeLeftTop(left, top);
			supr.setLeftTop.apply(this, arguments);
			this.sync();
		},

		setXY : function(xy, a, d, c, e) {
			this.fixDisplay();
			this.beforeAction();
			this.storeXY(xy);
			var cb = this.createCB(c);
			supr.setXY.call(this, xy, a, d, cb, e);
			if (!a) {
				cb();
			}
		},

		createCB : function(c) {
			var el = this;
			return function() {
				el.constrainXY();
				el.sync(true);
				if (c) {
					c();
				}
			};
		},

		setX : function(x, a, d, c, e) {
			this.setXY( [ x, this.getY() ], a, d, c, e);
		},

		setY : function(y, a, d, c, e) {
			this.setXY( [ this.getX(), y ], a, d, c, e);
		},

		setSize : function(w, h, a, d, c, e) {
			this.beforeAction();
			var cb = this.createCB(c);
			supr.setSize.call(this, w, h, a, d, cb, e);
			if (!a) {
				cb();
			}
		},

		setWidth : function(w, a, d, c, e) {
			this.beforeAction();
			var cb = this.createCB(c);
			supr.setWidth.call(this, w, a, d, cb, e);
			if (!a) {
				cb();
			}
		},

		setHeight : function(h, a, d, c, e) {
			this.beforeAction();
			var cb = this.createCB(c);
			supr.setHeight.call(this, h, a, d, cb, e);
			if (!a) {
				cb();
			}
		},

		setBounds : function(x, y, w, h, a, d, c, e) {
			this.beforeAction();
			var cb = this.createCB(c);
			if (!a) {
				this.storeXY( [ x, y ]);
				supr.setXY.call(this, [ x, y ]);
				supr.setSize.call(this, w, h, a, d, cb, e);
				cb();
			} else {
				supr.setBounds.call(this, x, y, w, h, a, d, cb, e);
			}
			return this;
		},

		setZIndex : function(zindex) {
			this.zindex = zindex;
			this.setStyle("z-index", zindex + 2);
			if (this.shadow) {
				this.shadow.setZIndex(zindex + 1);
			}
			if (this.shim) {
				this.shim.setStyle("z-index", zindex);
			}
		}
	});
})();

Tera.Shadow = function(config) {
	Tera.apply(this, config);
	if (typeof this.mode != "string") {
		this.mode = this.defaultMode;
	}
	var o = this.offset, a = {
		h :0
	};
	var rad = Math.floor(this.offset / 2);
	switch (this.mode.toLowerCase()) {
		case "drop":
			a.w = 0;
			a.l = a.t = o;
			a.t -= 1;
			if (Tera.isIE) {
				a.l -= this.offset + rad;
				a.t -= this.offset + rad;
				a.w -= rad;
				a.h -= rad;
				a.t += 1;
			}
			break;
		case "sides":
			a.w = (o * 2);
			a.l = -o;
			a.t = o - 1;
			if (Tera.isIE) {
				a.l -= (this.offset - rad);
				a.t -= this.offset + rad;
				a.l += 1;
				a.w -= (this.offset - rad) * 2;
				a.w -= rad + 1;
				a.h -= 1;
			}
			break;
		case "frame":
			a.w = a.h = (o * 2);
			a.l = a.t = -o;
			a.t += 1;
			a.h -= 2;
			if (Tera.isIE) {
				a.l -= (this.offset - rad);
				a.t -= (this.offset - rad);
				a.l += 1;
				a.w -= (this.offset + rad + 1);
				a.h -= (this.offset + rad);
				a.h += 1;
			}
			break;
	}
	;

	this.adjusts = a;
};

Tera.Shadow.prototype = {

	offset :4,

	defaultMode :"drop",

	show : function(target) {
		target = Tera.get(target);
		if (!this.el) {
			this.el = Tera.Shadow.Pool.pull();
			if (this.el.dom.nextSibling != target.dom) {
				this.el.insertBefore(target);
			}
		}
		this.el.setStyle("z-index", this.zIndex || parseInt(target.getStyle("z-index"), 10) - 1);
		if (Tera.isIE) {
			this.el.dom.style.filter = "progid:DXImageTransform.Microsoft.alpha(opacity=50) progid:DXImageTransform.Microsoft.Blur(pixelradius="
					+ (this.offset) + ")";
		}
		this.realign(target.getLeft(true), target.getTop(true), target.getWidth(), target.getHeight());
		this.el.dom.style.display = "block";
	},

	isVisible : function() {
		return this.el ? true : false;
	},

	realign : function(l, t, w, h) {
		if (!this.el) {
			return;
		}
		var a = this.adjusts, d = this.el.dom, s = d.style;
		var iea = 0;
		s.left = (l + a.l) + "px";
		s.top = (t + a.t) + "px";
		var sw = (w + a.w), sh = (h + a.h), sws = sw + "px", shs = sh + "px";
		if (s.width != sws || s.height != shs) {
			s.width = sws;
			s.height = shs;
			if (!Tera.isIE) {
				var cn = d.childNodes;
				var sww = Math.max(0, (sw - 12)) + "px";
				cn[0].childNodes[1].style.width = sww;
				cn[1].childNodes[1].style.width = sww;
				cn[2].childNodes[1].style.width = sww;
				cn[1].style.height = Math.max(0, (sh - 12)) + "px";
			}
		}
	},

	hide : function() {
		if (this.el) {
			this.el.dom.style.display = "none";
			Tera.Shadow.Pool.push(this.el);
			delete this.el;
		}
	},

	setZIndex : function(z) {
		this.zIndex = z;
		if (this.el) {
			this.el.setStyle("z-index", z);
		}
	}
};

Tera.Shadow.Pool = function() {
	var p = [];
	var markup = Tera.isIE ? '<div class="x-ie-shadow"></div>'
			: '<div class="x-shadow"><div class="xst"><div class="xstl"></div><div class="xstc"></div><div class="xstr"></div></div><div class="xsc"><div class="xsml"></div><div class="xsmc"></div><div class="xsmr"></div></div><div class="xsb"><div class="xsbl"></div><div class="xsbc"></div><div class="xsbr"></div></div></div>';
	return {
		pull : function() {
			var sh = p.shift();
			if (!sh) {
				sh = Tera.get(Tera.DomHelper.insertHtml("beforeBegin", document.body.firstChild, markup));
				sh.autoBoxAdjust = false;
			}
			return sh;
		},

		push : function(sh) {
			p.push(sh);
		}
	};
}();

Tera.BoxComponent = Tera.extend(Tera.Component, {

	initComponent : function() {
		Tera.BoxComponent.superclass.initComponent.call(this);
		this.addEvents(

		'resize',

		'move');
	},

	boxReady :false,
	deferHeight :false,

	setSize : function(w, h) {
		if (typeof w == 'object') {
			h = w.height;
			w = w.width;
		}
		if (!this.boxReady) {
			this.width = w;
			this.height = h;
			return this;
		}

		if (this.lastSize && this.lastSize.width == w && this.lastSize.height == h) {
			return this;
		}
		this.lastSize = {
			width :w,
			height :h
		};
		var adj = this.adjustSize(w, h);
		var aw = adj.width, ah = adj.height;
		if (aw !== undefined || ah !== undefined) {
			var rz = this.getResizeEl();
			if (!this.deferHeight && aw !== undefined && ah !== undefined) {
				rz.setSize(aw, ah);
			} else if (!this.deferHeight && ah !== undefined) {
				rz.setHeight(ah);
			} else if (aw !== undefined) {
				rz.setWidth(aw);
			}
			this.onResize(aw, ah, w, h);
			this.fireEvent('resize', this, aw, ah, w, h);
		}
		return this;
	},

	setWidth : function(width) {
		return this.setSize(width);
	},

	setHeight : function(height) {
		return this.setSize(undefined, height);
	},

	getSize : function() {
		return this.el.getSize();
	},

	getPosition : function(local) {
		if (local === true) {
			return [ this.el.getLeft(true), this.el.getTop(true) ];
		}
		return this.xy || this.el.getXY();
	},

	getBox : function(local) {
		var s = this.el.getSize();
		if (local === true) {
			s.x = this.el.getLeft(true);
			s.y = this.el.getTop(true);
		} else {
			var xy = this.xy || this.el.getXY();
			s.x = xy[0];
			s.y = xy[1];
		}
		return s;
	},

	updateBox : function(box) {
		this.setSize(box.width, box.height);
		this.setPagePosition(box.x, box.y);
		return this;
	},

	getResizeEl : function() {
		return this.resizeEl || this.el;
	},

	getPositionEl : function() {
		return this.positionEl || this.el;
	},

	setPosition : function(x, y) {
		if (x && typeof x[1] == 'number') {
			y = x[1];
			x = x[0];
		}
		this.x = x;
		this.y = y;
		if (!this.boxReady) {
			return this;
		}
		var adj = this.adjustPosition(x, y);
		var ax = adj.x, ay = adj.y;

		var el = this.getPositionEl();
		if (ax !== undefined || ay !== undefined) {
			if (ax !== undefined && ay !== undefined) {
				el.setLeftTop(ax, ay);
			} else if (ax !== undefined) {
				el.setLeft(ax);
			} else if (ay !== undefined) {
				el.setTop(ay);
			}
			this.onPosition(ax, ay);
			this.fireEvent('move', this, ax, ay);
		}
		return this;
	},

	setPagePosition : function(x, y) {
		if (x && typeof x[1] == 'number') {
			y = x[1];
			x = x[0];
		}
		this.pageX = x;
		this.pageY = y;
		if (!this.boxReady) {
			return;
		}
		if (x === undefined || y === undefined) {
			return;
		}
		var p = this.el.translatePoints(x, y);
		this.setPosition(p.left, p.top);
		return this;
	},

	onRender : function(ct, position) {
		Tera.BoxComponent.superclass.onRender.call(this, ct, position);
		if (this.resizeEl) {
			this.resizeEl = Tera.get(this.resizeEl);
		}
		if (this.positionEl) {
			this.positionEl = Tera.get(this.positionEl);
		}
	},

	afterRender : function() {
		Tera.BoxComponent.superclass.afterRender.call(this);
		this.boxReady = true;
		this.setSize(this.width, this.height);
		if (this.x || this.y) {
			this.setPosition(this.x, this.y);
		} else if (this.pageX || this.pageY) {
			this.setPagePosition(this.pageX, this.pageY);
		}
	},

	syncSize : function() {
		delete this.lastSize;
		this.setSize(this.autoWidth ? undefined : this.el.getWidth(), this.autoHeight ? undefined : this.el.getHeight());
		return this;
	},

	onResize : function(adjWidth, adjHeight, rawWidth, rawHeight) {

	},

	onPosition : function(x, y) {

	},

	adjustSize : function(w, h) {
		if (this.autoWidth) {
			w = 'auto';
		}
		if (this.autoHeight) {
			h = 'auto';
		}
		return {
			width :w,
			height :h
		};
	},

	adjustPosition : function(x, y) {
		return {
			x :x,
			y :y
		};
	}
});
Tera.reg('box', Tera.BoxComponent);

Tera.SplitBar = function(dragElement, resizingElement, orientation, placement, existingProxy) {

	this.el = Tera.get(dragElement, true);
	this.el.dom.unselectable = "on";

	this.resizingEl = Tera.get(resizingElement, true);

	this.orientation = orientation || Tera.SplitBar.HORIZONTAL;

	this.minSize = 0;

	this.maxSize = 2000;

	this.animate = false;

	this.useShim = false;

	this.shim = null;

	if (!existingProxy) {

		this.proxy = Tera.SplitBar.createProxy(this.orientation);
	} else {
		this.proxy = Tera.get(existingProxy).dom;
	}

	this.dd = new Tera.dd.DDProxy(this.el.dom.id, "XSplitBars", {
		dragElId :this.proxy.id
	});

	this.dd.b4StartDrag = this.onStartProxyDrag.createDelegate(this);

	this.dd.endDrag = this.onEndProxyDrag.createDelegate(this);

	this.dragSpecs = {};

	this.adapter = new Tera.SplitBar.BasicLayoutAdapter();
	this.adapter.init(this);

	if (this.orientation == Tera.SplitBar.HORIZONTAL) {

		this.placement = placement || (this.el.getX() > this.resizingEl.getX() ? Tera.SplitBar.LEFT : Tera.SplitBar.RIGHT);
		this.el.addClass("x-splitbar-h");
	} else {

		this.placement = placement || (this.el.getY() > this.resizingEl.getY() ? Tera.SplitBar.TOP : Tera.SplitBar.BOTTOM);
		this.el.addClass("x-splitbar-v");
	}

	this.addEvents(

	"resize",

	"moved",

	"beforeresize",

	"beforeapply");

	Tera.SplitBar.superclass.constructor.call(this);
};

Tera.extend(Tera.SplitBar, Tera.util.Observable,
		{
			onStartProxyDrag : function(x, y) {
				this.fireEvent("beforeresize", this);
				this.overlay = Tera.DomHelper.append(document.body, {
					cls :"x-drag-overlay",
					html :"&#160;"
				}, true);
				this.overlay.unselectable();
				this.overlay.setSize(Tera.lib.Dom.getViewWidth(true), Tera.lib.Dom.getViewHeight(true));
				this.overlay.show();
				Tera.get(this.proxy).setDisplayed("block");
				var size = this.adapter.getElementSize(this);
				this.activeMinSize = this.getMinimumSize();
				;
				this.activeMaxSize = this.getMaximumSize();
				;
				var c1 = size - this.activeMinSize;
				var c2 = Math.max(this.activeMaxSize - size, 0);
				if (this.orientation == Tera.SplitBar.HORIZONTAL) {
					this.dd.resetConstraints();
					this.dd.setXConstraint(this.placement == Tera.SplitBar.LEFT ? c1 : c2, this.placement == Tera.SplitBar.LEFT ? c2 : c1);
					this.dd.setYConstraint(0, 0);
				} else {
					this.dd.resetConstraints();
					this.dd.setXConstraint(0, 0);
					this.dd.setYConstraint(this.placement == Tera.SplitBar.TOP ? c1 : c2, this.placement == Tera.SplitBar.TOP ? c2 : c1);
				}
				this.dragSpecs.startSize = size;
				this.dragSpecs.startPoint = [ x, y ];
				Tera.dd.DDProxy.prototype.b4StartDrag.call(this.dd, x, y);
			},

			onEndProxyDrag : function(e) {
				Tera.get(this.proxy).setDisplayed(false);
				var endPoint = Tera.lib.Event.getXY(e);
				if (this.overlay) {
					this.overlay.remove();
					delete this.overlay;
				}
				var newSize;
				if (this.orientation == Tera.SplitBar.HORIZONTAL) {
					newSize = this.dragSpecs.startSize
							+ (this.placement == Tera.SplitBar.LEFT ? endPoint[0] - this.dragSpecs.startPoint[0] : this.dragSpecs.startPoint[0]
									- endPoint[0]);
				} else {
					newSize = this.dragSpecs.startSize
							+ (this.placement == Tera.SplitBar.TOP ? endPoint[1] - this.dragSpecs.startPoint[1] : this.dragSpecs.startPoint[1]
									- endPoint[1]);
				}
				newSize = Math.min(Math.max(newSize, this.activeMinSize), this.activeMaxSize);
				if (newSize != this.dragSpecs.startSize) {
					if (this.fireEvent('beforeapply', this, newSize) !== false) {
						this.adapter.setElementSize(this, newSize);
						this.fireEvent("moved", this, newSize);
						this.fireEvent("resize", this, newSize);
					}
				}
			},

			getAdapter : function() {
				return this.adapter;
			},

			setAdapter : function(adapter) {
				this.adapter = adapter;
				this.adapter.init(this);
			},

			getMinimumSize : function() {
				return this.minSize;
			},

			setMinimumSize : function(minSize) {
				this.minSize = minSize;
			},

			getMaximumSize : function() {
				return this.maxSize;
			},

			setMaximumSize : function(maxSize) {
				this.maxSize = maxSize;
			},

			setCurrentSize : function(size) {
				var oldAnimate = this.animate;
				this.animate = false;
				this.adapter.setElementSize(this, size);
				this.animate = oldAnimate;
			},

			destroy : function(removeEl) {
				if (this.shim) {
					this.shim.remove();
				}
				this.dd.unreg();
				Tera.removeNode(this.proxy);
				if (removeEl) {
					this.el.remove();
				}
			}
		});

Tera.SplitBar.createProxy = function(dir) {
	var proxy = new Tera.Element(document.createElement("div"));
	proxy.unselectable();
	var cls = 'x-splitbar-proxy';
	proxy.addClass(cls + ' ' + (dir == Tera.SplitBar.HORIZONTAL ? cls + '-h' : cls + '-v'));
	document.body.appendChild(proxy.dom);
	return proxy.dom;
};

Tera.SplitBar.BasicLayoutAdapter = function() {
};

Tera.SplitBar.BasicLayoutAdapter.prototype = {

	init : function(s) {

	},

	getElementSize : function(s) {
		if (s.orientation == Tera.SplitBar.HORIZONTAL) {
			return s.resizingEl.getWidth();
		} else {
			return s.resizingEl.getHeight();
		}
	},

	setElementSize : function(s, newSize, onComplete) {
		if (s.orientation == Tera.SplitBar.HORIZONTAL) {
			if (!s.animate) {
				s.resizingEl.setWidth(newSize);
				if (onComplete) {
					onComplete(s, newSize);
				}
			} else {
				s.resizingEl.setWidth(newSize, true, .1, onComplete, 'easeOut');
			}
		} else {

			if (!s.animate) {
				s.resizingEl.setHeight(newSize);
				if (onComplete) {
					onComplete(s, newSize);
				}
			} else {
				s.resizingEl.setHeight(newSize, true, .1, onComplete, 'easeOut');
			}
		}
	}
};

Tera.SplitBar.AbsoluteLayoutAdapter = function(container) {
	this.basic = new Tera.SplitBar.BasicLayoutAdapter();
	this.container = Tera.get(container);
};

Tera.SplitBar.AbsoluteLayoutAdapter.prototype = {
	init : function(s) {
		this.basic.init(s);
	},

	getElementSize : function(s) {
		return this.basic.getElementSize(s);
	},

	setElementSize : function(s, newSize, onComplete) {
		this.basic.setElementSize(s, newSize, this.moveSplitter.createDelegate(this, [ s ]));
	},

	moveSplitter : function(s) {
		var yes = Tera.SplitBar;
		switch (s.placement) {
			case yes.LEFT:
				s.el.setX(s.resizingEl.getRight());
				break;
			case yes.RIGHT:
				s.el.setStyle("right", (this.container.getWidth() - s.resizingEl.getLeft()) + "px");
				break;
			case yes.TOP:
				s.el.setY(s.resizingEl.getBottom());
				break;
			case yes.BOTTOM:
				s.el.setY(s.resizingEl.getTop() - s.el.getHeight());
				break;
		}
	}
};

Tera.SplitBar.VERTICAL = 1;

Tera.SplitBar.HORIZONTAL = 2;

Tera.SplitBar.LEFT = 1;

Tera.SplitBar.RIGHT = 2;

Tera.SplitBar.TOP = 3;

Tera.SplitBar.BOTTOM = 4;

Tera.Container = Tera.extend(Tera.BoxComponent, {

	autoDestroy :true,

	defaultType :'panel',

	initComponent : function() {
		Tera.Container.superclass.initComponent.call(this);

		this.addEvents(

		'afterlayout',

		'beforeadd',

		'beforeremove',

		'add',

		'remove');

		var items = this.items;
		if (items) {
			delete this.items;
			if (Tera.isArray(items)) {
				this.add.apply(this, items);
			} else {
				this.add(items);
			}
		}
	},

	initItems : function() {
		if (!this.items) {
			this.items = new Tera.util.MixedCollection(false, this.getComponentId);
			this.getLayout();
		}
	},

	setLayout : function(layout) {
		if (this.layout && this.layout != layout) {
			this.layout.setContainer(null);
		}
		this.initItems();
		this.layout = layout;
		layout.setContainer(this);
	},

	render : function() {
		Tera.Container.superclass.render.apply(this, arguments);
		if (this.layout) {
			if (typeof this.layout == 'string') {
				this.layout = new Tera.Container.LAYOUTS[this.layout.toLowerCase()](this.layoutConfig);
			}
			this.setLayout(this.layout);

			if (this.activeItem !== undefined) {
				var item = this.activeItem;
				delete this.activeItem;
				this.layout.setActiveItem(item);
				return;
			}
		}
		if (!this.ownerCt) {
			this.doLayout();
		}
		if (this.monitorResize === true) {
			Tera.EventManager.onWindowResize(this.doLayout, this, [ false ]);
		}
	},

	getLayoutTarget : function() {
		return this.el;
	},

	getComponentId : function(comp) {
		return comp.itemId || comp.id;
	},

	add : function(comp) {
		if (!this.items) {
			this.initItems();
		}
		var a = arguments, len = a.length;
		if (len > 1) {
			for ( var i = 0; i < len; i++) {
				this.add(a[i]);
			}
			return;
		}
		var c = this.lookupComponent(this.applyDefaults(comp));
		var pos = this.items.length;
		if (this.fireEvent('beforeadd', this, c, pos) !== false && this.onBeforeAdd(c) !== false) {
			this.items.add(c);
			c.ownerCt = this;
			this.fireEvent('add', this, c, pos);
		}
		return c;
	},

	insert : function(index, comp) {
		if (!this.items) {
			this.initItems();
		}
		var a = arguments, len = a.length;
		if (len > 2) {
			for ( var i = len - 1; i >= 1; --i) {
				this.insert(index, a[i]);
			}
			return;
		}
		var c = this.lookupComponent(this.applyDefaults(comp));

		if (c.ownerCt == this && this.items.indexOf(c) < index) {
			--index;
		}

		if (this.fireEvent('beforeadd', this, c, index) !== false && this.onBeforeAdd(c) !== false) {
			this.items.insert(index, c);
			c.ownerCt = this;
			this.fireEvent('add', this, c, index);
		}
		return c;
	},

	applyDefaults : function(c) {
		if (this.defaults) {
			if (typeof c == 'string') {
				c = Tera.ComponentMgr.get(c);
				Tera.apply(c, this.defaults);
			} else if (!c.events) {
				Tera.applyIf(c, this.defaults);
			} else {
				Tera.apply(c, this.defaults);
			}
		}
		return c;
	},

	onBeforeAdd : function(item) {
		if (item.ownerCt) {
			item.ownerCt.remove(item, false);
		}
		if (this.hideBorders === true) {
			item.border = (item.border === true);
		}
	},

	remove : function(comp, autoDestroy) {
		var c = this.getComponent(comp);
		if (c && this.fireEvent('beforeremove', this, c) !== false) {
			this.items.remove(c);
			delete c.ownerCt;
			if (autoDestroy === true || (autoDestroy !== false && this.autoDestroy)) {
				c.destroy();
			}
			if (this.layout && this.layout.activeItem == c) {
				delete this.layout.activeItem;
			}
			this.fireEvent('remove', this, c);
		}
		return c;
	},

	getComponent : function(comp) {
		if (typeof comp == 'object') {
			return comp;
		}
		return this.items.get(comp);
	},

	lookupComponent : function(comp) {
		if (typeof comp == 'string') {
			return Tera.ComponentMgr.get(comp);
		} else if (!comp.events) {
			return this.createComponent(comp);
		}
		return comp;
	},

	createComponent : function(config) {
		return Tera.ComponentMgr.create(config, this.defaultType);
	},

	doLayout : function(shallow) {
		if (this.rendered && this.layout) {
			this.layout.layout();
		}
		if (shallow !== false && this.items) {
			var cs = this.items.items;
			for ( var i = 0, len = cs.length; i < len; i++) {
				var c = cs[i];
				if (c.doLayout) {
					c.doLayout();
				}
			}
		}
	},

	getLayout : function() {
		if (!this.layout) {
			var layout = new Tera.layout.ContainerLayout(this.layoutConfig);
			this.setLayout(layout);
		}
		return this.layout;
	},

	onDestroy : function() {
		if (this.items) {
			var cs = this.items.items;
			for ( var i = 0, len = cs.length; i < len; i++) {
				Tera.destroy(cs[i]);
			}
		}
		if (this.monitorResize) {
			Tera.EventManager.removeResizeListener(this.doLayout, this);
		}
		Tera.Container.superclass.onDestroy.call(this);
	},

	bubble : function(fn, scope, args) {
		var p = this;
		while (p) {
			if (fn.apply(scope || p, args || [ p ]) === false) {
				break;
			}
			p = p.ownerCt;
		}
	},

	cascade : function(fn, scope, args) {
		if (fn.apply(scope || this, args || [ this ]) !== false) {
			if (this.items) {
				var cs = this.items.items;
				for ( var i = 0, len = cs.length; i < len; i++) {
					if (cs[i].cascade) {
						cs[i].cascade(fn, scope, args);
					} else {
						fn.apply(scope || this, args || [ cs[i] ]);
					}
				}
			}
		}
	},

	findById : function(id) {
		var m, ct = this;
		this.cascade( function(c) {
			if (ct != c && c.id === id) {
				m = c;
				return false;
			}
		});
		return m || null;
	},

	findByType : function(xtype) {
		return typeof xtype == 'function' ? this.findBy( function(c) {
			return c.constructor === xtype;
		}) : this.findBy( function(c) {
			return c.constructor.xtype === xtype;
		});
	},

	find : function(prop, value) {
		return this.findBy( function(c) {
			return c[prop] === value;
		});
	},

	findBy : function(fn, scope) {
		var m = [], ct = this;
		this.cascade( function(c) {
			if (ct != c && fn.call(scope || c, c, ct) === true) {
				m.push(c);
			}
		});
		return m;
	}
});

Tera.Container.LAYOUTS = {};
Tera.reg('container', Tera.Container);

Tera.layout.ContainerLayout = function(config) {
	Tera.apply(this, config);
};

Tera.layout.ContainerLayout.prototype = {

	monitorResize :false,
	activeItem :null,

	layout : function() {
		var target = this.container.getLayoutTarget();
		this.onLayout(this.container, target);
		this.container.fireEvent('afterlayout', this.container, this);
	},

	onLayout : function(ct, target) {
		this.renderAll(ct, target);
	},

	isValidParent : function(c, target) {
		var el = c.getPositionEl ? c.getPositionEl() : c.getEl();
		return el.dom.parentNode == target.dom;
	},

	renderAll : function(ct, target) {
		var items = ct.items.items;
		for ( var i = 0, len = items.length; i < len; i++) {
			var c = items[i];
			if (c && (!c.rendered || !this.isValidParent(c, target))) {
				this.renderItem(c, i, target);
			}
		}
	},

	renderItem : function(c, position, target) {
		if (c && !c.rendered) {
			c.render(target, position);
			if (this.extraCls) {
				var t = c.getPositionEl ? c.getPositionEl() : c;
				t.addClass(this.extraCls);
			}
			if (this.renderHidden && c != this.activeItem) {
				c.hide();
			}
		} else if (c && !this.isValidParent(c, target)) {
			if (this.extraCls) {
				c.addClass(this.extraCls);
			}
			if (typeof position == 'number') {
				position = target.dom.childNodes[position];
			}
			target.dom.insertBefore(c.getEl().dom, position || null);
			if (this.renderHidden && c != this.activeItem) {
				c.hide();
			}
		}
	},

	onResize : function() {
		if (this.container.collapsed) {
			return;
		}
		var b = this.container.bufferResize;
		if (b) {
			if (!this.resizeTask) {
				this.resizeTask = new Tera.util.DelayedTask(this.layout, this);
				this.resizeBuffer = typeof b == 'number' ? b : 100;
			}
			this.resizeTask.delay(this.resizeBuffer);
		} else {
			this.layout();
		}
	},

	setContainer : function(ct) {
		if (this.monitorResize && ct != this.container) {
			if (this.container) {
				this.container.un('resize', this.onResize, this);
			}
			if (ct) {
				ct.on('resize', this.onResize, this);
			}
		}
		this.container = ct;
	},

	parseMargins : function(v) {
		var ms = v.split(' ');
		var len = ms.length;
		if (len == 1) {
			ms[1] = ms[0];
			ms[2] = ms[0];
			ms[3] = ms[0];
		}
		if (len == 2) {
			ms[2] = ms[0];
			ms[3] = ms[1];
		}
		return {
			top :parseInt(ms[0], 10) || 0,
			right :parseInt(ms[1], 10) || 0,
			bottom :parseInt(ms[2], 10) || 0,
			left :parseInt(ms[3], 10) || 0
		};
	}
};
Tera.Container.LAYOUTS['auto'] = Tera.layout.ContainerLayout;

Tera.layout.FitLayout = Tera.extend(Tera.layout.ContainerLayout, {

	monitorResize :true,

	onLayout : function(ct, target) {
		Tera.layout.FitLayout.superclass.onLayout.call(this, ct, target);
		if (!this.container.collapsed) {
			this.setItemSize(this.activeItem || ct.items.itemAt(0), target.getStyleSize());
		}
	},

	setItemSize : function(item, size) {
		if (item && size.height > 0) {
			item.setSize(size);
		}
	}
});
Tera.Container.LAYOUTS['fit'] = Tera.layout.FitLayout;

Tera.layout.CardLayout = Tera.extend(Tera.layout.FitLayout, {

	deferredRender :false,

	renderHidden :true,

	setActiveItem : function(item) {
		item = this.container.getComponent(item);
		if (this.activeItem != item) {
			if (this.activeItem) {
				this.activeItem.hide();
			}
			this.activeItem = item;
			item.show();
			this.layout();
		}
	},

	renderAll : function(ct, target) {
		if (this.deferredRender) {
			this.renderItem(this.activeItem, undefined, target);
		} else {
			Tera.layout.CardLayout.superclass.renderAll.call(this, ct, target);
		}
	}
});
Tera.Container.LAYOUTS['card'] = Tera.layout.CardLayout;

Tera.layout.AnchorLayout = Tera.extend(Tera.layout.ContainerLayout, {

	monitorResize :true,

	getAnchorViewSize : function(ct, target) {
		return target.dom == document.body ? target.getViewSize() : target.getStyleSize();
	},

	onLayout : function(ct, target) {
		Tera.layout.AnchorLayout.superclass.onLayout.call(this, ct, target);

		var size = this.getAnchorViewSize(ct, target);

		var w = size.width, h = size.height;

		if (w < 20 || h < 20) {
			return;
		}

		var aw, ah;
		if (ct.anchorSize) {
			if (typeof ct.anchorSize == 'number') {
				aw = ct.anchorSize;
			} else {
				aw = ct.anchorSize.width;
				ah = ct.anchorSize.height;
			}
		} else {
			aw = ct.initialConfig.width;
			ah = ct.initialConfig.height;
		}

		var cs = ct.items.items, len = cs.length, i, c, a, cw, ch;
		for (i = 0; i < len; i++) {
			c = cs[i];
			if (c.anchor) {
				a = c.anchorSpec;
				if (!a) {
					var vs = c.anchor.split(' ');
					c.anchorSpec = a = {
						right :this.parseAnchor(vs[0], c.initialConfig.width, aw),
						bottom :this.parseAnchor(vs[1], c.initialConfig.height, ah)
					};
				}
				cw = a.right ? this.adjustWidthAnchor(a.right(w), c) : undefined;
				ch = a.bottom ? this.adjustHeightAnchor(a.bottom(h), c) : undefined;

				if (cw || ch) {
					c.setSize(cw || undefined, ch || undefined);
				}
			}
		}
	},

	parseAnchor : function(a, start, cstart) {
		if (a && a != 'none') {
			var last;
			if (/^(r|right|b|bottom)$/i.test(a)) {
				var diff = cstart - start;
				return function(v) {
					if (v !== last) {
						last = v;
						return v - diff;
					}
				}
			} else if (a.indexOf('%') != -1) {
				var ratio = parseFloat(a.replace('%', '')) * .01;
				return function(v) {
					if (v !== last) {
						last = v;
						return Math.floor(v * ratio);
					}
				}
			} else {
				a = parseInt(a, 10);
				if (!isNaN(a)) {
					return function(v) {
						if (v !== last) {
							last = v;
							return v + a;
						}
					}
				}
			}
		}
		return false;
	},

	adjustWidthAnchor : function(value, comp) {
		return value;
	},

	adjustHeightAnchor : function(value, comp) {
		return value;
	}

});
Tera.Container.LAYOUTS['anchor'] = Tera.layout.AnchorLayout;

Tera.layout.ColumnLayout = Tera.extend(Tera.layout.ContainerLayout, {

	monitorResize :true,

	extraCls :'x-column',

	scrollOffset :0,

	isValidParent : function(c, target) {
		return c.getEl().dom.parentNode == this.innerCt.dom;
	},

	onLayout : function(ct, target) {
		var cs = ct.items.items, len = cs.length, c, i;

		if (!this.innerCt) {
			target.addClass('x-column-layout-ct');

			this.innerCt = target.createChild( {
				cls :'x-column-inner'
			});
			this.innerCt.createChild( {
				cls :'x-clear'
			});
		}
		this.renderAll(ct, this.innerCt);

		var size = target.getViewSize();

		if (size.width < 1 && size.height < 1) {
			return;
		}

		var w = size.width - target.getPadding('lr') - this.scrollOffset, h = size.height - target.getPadding('tb'), pw = w;

		this.innerCt.setWidth(w);

		for (i = 0; i < len; i++) {
			c = cs[i];
			if (!c.columnWidth) {
				pw -= (c.getSize().width + c.getEl().getMargins('lr'));
			}
		}

		pw = pw < 0 ? 0 : pw;

		for (i = 0; i < len; i++) {
			c = cs[i];
			if (c.columnWidth) {
				c.setSize(Math.floor(c.columnWidth * pw) - c.getEl().getMargins('lr'));
			}
		}
	}

});

Tera.Container.LAYOUTS['column'] = Tera.layout.ColumnLayout;

Tera.layout.BorderLayout = Tera.extend(Tera.layout.ContainerLayout, {
	monitorResize :true,
	rendered :false,

	onLayout : function(ct, target) {
		var collapsed;
		if (!this.rendered) {
			target.position();
			target.addClass('x-border-layout-ct');
			var items = ct.items.items;
			collapsed = [];
			for ( var i = 0, len = items.length; i < len; i++) {
				var c = items[i];
				var pos = c.region;
				if (c.collapsed) {
					collapsed.push(c);
				}
				c.collapsed = false;
				if (!c.rendered) {
					c.cls = c.cls ? c.cls + ' x-border-panel' : 'x-border-panel';
					c.render(target, i);
				}
				this[pos] = pos != 'center' && c.split ? new Tera.layout.BorderLayout.SplitRegion(this, c.initialConfig, pos)
						: new Tera.layout.BorderLayout.Region(this, c.initialConfig, pos);
				this[pos].render(target, c);
			}
			this.rendered = true;
		}

		var size = target.getViewSize();
		if (size.width < 20 || size.height < 20) {
			if (collapsed) {
				this.restoreCollapsed = collapsed;
			}
			return;
		} else if (this.restoreCollapsed) {
			collapsed = this.restoreCollapsed;
			delete this.restoreCollapsed;
		}

		var w = size.width, h = size.height;
		var centerW = w, centerH = h, centerY = 0, centerX = 0;

		var n = this.north, s = this.south, west = this.west, e = this.east, c = this.center;
		if (!c) {
			throw 'No center region defined in BorderLayout ' + ct.id;
		}

		if (n && n.isVisible()) {
			var b = n.getSize();
			var m = n.getMargins();
			b.width = w - (m.left + m.right);
			b.x = m.left;
			b.y = m.top;
			centerY = b.height + b.y + m.bottom;
			centerH -= centerY;
			n.applyLayout(b);
		}
		if (s && s.isVisible()) {
			var b = s.getSize();
			var m = s.getMargins();
			b.width = w - (m.left + m.right);
			b.x = m.left;
			var totalHeight = (b.height + m.top + m.bottom);
			b.y = h - totalHeight + m.top;
			centerH -= totalHeight;
			s.applyLayout(b);
		}
		if (west && west.isVisible()) {
			var b = west.getSize();
			var m = west.getMargins();
			b.height = centerH - (m.top + m.bottom);
			b.x = m.left;
			b.y = centerY + m.top;
			var totalWidth = (b.width + m.left + m.right);
			centerX += totalWidth;
			centerW -= totalWidth;
			west.applyLayout(b);
		}
		if (e && e.isVisible()) {
			var b = e.getSize();
			var m = e.getMargins();
			b.height = centerH - (m.top + m.bottom);
			var totalWidth = (b.width + m.left + m.right);
			b.x = w - totalWidth + m.left;
			b.y = centerY + m.top;
			centerW -= totalWidth;
			e.applyLayout(b);
		}

		var m = c.getMargins();
		var centerBox = {
			x :centerX + m.left,
			y :centerY + m.top,
			width :centerW - (m.left + m.right),
			height :centerH - (m.top + m.bottom)
		};
		c.applyLayout(centerBox);

		if (collapsed) {
			for ( var i = 0, len = collapsed.length; i < len; i++) {
				collapsed[i].collapse(false);
			}
		}

		if (Tera.isIE && Tera.isStrict) {
			target.repaint();
		}
	}

});

Tera.layout.BorderLayout.Region = function(layout, config, pos) {
	Tera.apply(this, config);
	this.layout = layout;
	this.position = pos;
	this.state = {};
	if (typeof this.margins == 'string') {
		this.margins = this.layout.parseMargins(this.margins);
	}
	this.margins = Tera.applyIf(this.margins || {}, this.defaultMargins);
	if (this.collapsible) {
		if (typeof this.cmargins == 'string') {
			this.cmargins = this.layout.parseMargins(this.cmargins);
		}
		if (this.collapseMode == 'mini' && !this.cmargins) {
			this.cmargins = {
				left :0,
				top :0,
				right :0,
				bottom :0
			};
		} else {
			this.cmargins = Tera.applyIf(this.cmargins || {}, pos == 'north' || pos == 'south' ? this.defaultNSCMargins : this.defaultEWCMargins);
		}
	}
};

Tera.layout.BorderLayout.Region.prototype = {

	collapsible :false,

	split :false,

	floatable :true,

	minWidth :50,

	minHeight :50,

	defaultMargins : {
		left :0,
		top :0,
		right :0,
		bottom :0
	},
	defaultNSCMargins : {
		left :5,
		top :5,
		right :5,
		bottom :5
	},
	defaultEWCMargins : {
		left :5,
		top :0,
		right :5,
		bottom :0
	},

	isCollapsed :false,

	render : function(ct, p) {
		this.panel = p;
		p.el.enableDisplayMode();
		this.targetEl = ct;
		this.el = p.el;

		var gs = p.getState, ps = this.position;
		p.getState = function() {
			return Tera.apply(gs.call(p) || {}, this.state);
		}.createDelegate(this);

		if (ps != 'center') {
			p.allowQueuedExpand = false;
			p.on( {
				beforecollapse :this.beforeCollapse,
				collapse :this.onCollapse,
				beforeexpand :this.beforeExpand,
				expand :this.onExpand,
				hide :this.onHide,
				show :this.onShow,
				scope :this
			});
			if (this.collapsible) {
				p.collapseEl = 'el';
				p.slideAnchor = this.getSlideAnchor();
			}
			if (p.tools && p.tools.toggle) {
				p.tools.toggle.addClass('x-tool-collapse-' + ps);
				p.tools.toggle.addClassOnOver('x-tool-collapse-' + ps + '-over');
			}
		}
	},

	getCollapsedEl : function() {
		if (!this.collapsedEl) {
			if (!this.toolTemplate) {
				var tt = new Tera.Template('<div class="x-tool x-tool-{id}">&#160;</div>');
				tt.disableFormats = true;
				tt.compile();
				Tera.layout.BorderLayout.Region.prototype.toolTemplate = tt;
			}
			this.collapsedEl = this.targetEl.createChild( {
				cls :"x-layout-collapsed x-layout-collapsed-" + this.position,
				id :this.panel.id + '-xcollapsed'
			});
			this.collapsedEl.enableDisplayMode('block');

			if (this.collapseMode == 'mini') {
				this.collapsedEl.addClass('x-layout-cmini-' + this.position);
				this.miniCollapsedEl = this.collapsedEl.createChild( {
					cls :"x-layout-mini x-layout-mini-" + this.position,
					html :"&#160;"
				});
				this.miniCollapsedEl.addClassOnOver('x-layout-mini-over');
				this.collapsedEl.addClassOnOver("x-layout-collapsed-over");
				this.collapsedEl.on('click', this.onExpandClick, this, {
					stopEvent :true
				});
			} else {
				var t = this.toolTemplate.append(this.collapsedEl.dom, {
					id :'expand-' + this.position
				}, true);
				t.addClassOnOver('x-tool-expand-' + this.position + '-over');
				t.on('click', this.onExpandClick, this, {
					stopEvent :true
				});

				if (this.floatable !== false) {
					this.collapsedEl.addClassOnOver("x-layout-collapsed-over");
					this.collapsedEl.on("click", this.collapseClick, this);
				}
			}
		}
		return this.collapsedEl;
	},

	onExpandClick : function(e) {
		if (this.isSlid) {
			this.afterSlideIn();
			this.panel.expand(false);
		} else {
			this.panel.expand();
		}
	},

	onCollapseClick : function(e) {
		this.panel.collapse();
	},

	beforeCollapse : function(p, animate) {
		this.lastAnim = animate;
		if (this.splitEl) {
			this.splitEl.hide();
		}
		this.getCollapsedEl().show();
		this.panel.el.setStyle('z-index', 100);
		this.isCollapsed = true;
		this.layout.layout();
	},

	onCollapse : function(animate) {
		this.panel.el.setStyle('z-index', 1);
		if (this.lastAnim === false || this.panel.animCollapse === false) {
			this.getCollapsedEl().dom.style.visibility = 'visible';
		} else {
			this.getCollapsedEl().slideIn(this.panel.slideAnchor, {
				duration :.2
			});
		}
		this.state.collapsed = true;
		this.panel.saveState();
	},

	beforeExpand : function(animate) {
		var c = this.getCollapsedEl();
		this.el.show();
		if (this.position == 'east' || this.position == 'west') {
			this.panel.setSize(undefined, c.getHeight());
		} else {
			this.panel.setSize(c.getWidth(), undefined);
		}
		c.hide();
		c.dom.style.visibility = 'hidden';
		this.panel.el.setStyle('z-index', 100);
	},

	onExpand : function() {
		this.isCollapsed = false;
		if (this.splitEl) {
			this.splitEl.show();
		}
		this.layout.layout();
		this.panel.el.setStyle('z-index', 1);
		this.state.collapsed = false;
		this.panel.saveState();
	},

	collapseClick : function(e) {
		if (this.isSlid) {
			e.stopPropagation();
			this.slideIn();
		} else {
			e.stopPropagation();
			this.slideOut();
		}
	},

	onHide : function() {
		if (this.isCollapsed) {
			this.getCollapsedEl().hide();
		} else if (this.splitEl) {
			this.splitEl.hide();
		}
	},

	onShow : function() {
		if (this.isCollapsed) {
			this.getCollapsedEl().show();
		} else if (this.splitEl) {
			this.splitEl.show();
		}
	},

	isVisible : function() {
		return !this.panel.hidden;
	},

	getMargins : function() {
		return this.isCollapsed && this.cmargins ? this.cmargins : this.margins;
	},

	getSize : function() {
		return this.isCollapsed ? this.getCollapsedEl().getSize() : this.panel.getSize();
	},

	setPanel : function(panel) {
		this.panel = panel;
	},

	getMinWidth : function() {
		return this.minWidth;
	},

	getMinHeight : function() {
		return this.minHeight;
	},

	applyLayoutCollapsed : function(box) {
		var ce = this.getCollapsedEl();
		ce.setLeftTop(box.x, box.y);
		ce.setSize(box.width, box.height);
	},

	applyLayout : function(box) {
		if (this.isCollapsed) {
			this.applyLayoutCollapsed(box);
		} else {
			this.panel.setPosition(box.x, box.y);
			this.panel.setSize(box.width, box.height);
		}
	},

	beforeSlide : function() {
		this.panel.beforeEffect();
	},

	afterSlide : function() {
		this.panel.afterEffect();
	},

	initAutoHide : function() {
		if (this.autoHide !== false) {
			if (!this.autoHideHd) {
				var st = new Tera.util.DelayedTask(this.slideIn, this);
				this.autoHideHd = {
					"mouseout" : function(e) {
						if (!e.within(this.el, true)) {
							st.delay(500);
						}
					},
					"mouseover" : function(e) {
						st.cancel();
					},
					scope :this
				};
			}
			this.el.on(this.autoHideHd);
		}
	},

	clearAutoHide : function() {
		if (this.autoHide !== false) {
			this.el.un("mouseout", this.autoHideHd.mouseout);
			this.el.un("mouseover", this.autoHideHd.mouseover);
		}
	},

	clearMonitor : function() {
		Tera.getDoc().un("click", this.slideInIf, this);
	},

	slideOut : function() {
		if (this.isSlid || this.el.hasActiveFx()) {
			return;
		}
		this.isSlid = true;
		var ts = this.panel.tools;
		if (ts && ts.toggle) {
			ts.toggle.hide();
		}
		this.el.show();
		if (this.position == 'east' || this.position == 'west') {
			this.panel.setSize(undefined, this.collapsedEl.getHeight());
		} else {
			this.panel.setSize(this.collapsedEl.getWidth(), undefined);
		}
		this.restoreLT = [ this.el.dom.style.left, this.el.dom.style.top ];
		this.el.alignTo(this.collapsedEl, this.getCollapseAnchor());
		this.el.setStyle("z-index", 102);
		if (this.animFloat !== false) {
			this.beforeSlide();
			this.el.slideIn(this.getSlideAnchor(), {
				callback : function() {
					this.afterSlide();
					this.initAutoHide();
					Tera.getDoc().on("click", this.slideInIf, this);
				},
				scope :this,
				block :true
			});
		} else {
			this.initAutoHide();
			Tera.getDoc().on("click", this.slideInIf, this);
		}
	},

	afterSlideIn : function() {
		this.clearAutoHide();
		this.isSlid = false;
		this.clearMonitor();
		this.el.setStyle("z-index", "");
		this.el.dom.style.left = this.restoreLT[0];
		this.el.dom.style.top = this.restoreLT[1];

		var ts = this.panel.tools;
		if (ts && ts.toggle) {
			ts.toggle.show();
		}
	},

	slideIn : function(cb) {
		if (!this.isSlid || this.el.hasActiveFx()) {
			Tera.callback(cb);
			return;
		}
		this.isSlid = false;
		if (this.animFloat !== false) {
			this.beforeSlide();
			this.el.slideOut(this.getSlideAnchor(), {
				callback : function() {
					this.el.hide();
					this.afterSlide();
					this.afterSlideIn();
					Tera.callback(cb);
				},
				scope :this,
				block :true
			});
		} else {
			this.el.hide();
			this.afterSlideIn();
		}
	},

	slideInIf : function(e) {
		if (!e.within(this.el)) {
			this.slideIn();
		}
	},

	anchors : {
		"west" :"left",
		"east" :"right",
		"north" :"top",
		"south" :"bottom"
	},

	sanchors : {
		"west" :"l",
		"east" :"r",
		"north" :"t",
		"south" :"b"
	},

	canchors : {
		"west" :"tl-tr",
		"east" :"tr-tl",
		"north" :"tl-bl",
		"south" :"bl-tl"
	},

	getAnchor : function() {
		return this.anchors[this.position];
	},

	getCollapseAnchor : function() {
		return this.canchors[this.position];
	},

	getSlideAnchor : function() {
		return this.sanchors[this.position];
	},

	getAlignAdj : function() {
		var cm = this.cmargins;
		switch (this.position) {
			case "west":
				return [ 0, 0 ];
				break;
			case "east":
				return [ 0, 0 ];
				break;
			case "north":
				return [ 0, 0 ];
				break;
			case "south":
				return [ 0, 0 ];
				break;
		}
	},

	getExpandAdj : function() {
		var c = this.collapsedEl, cm = this.cmargins;
		switch (this.position) {
			case "west":
				return [ -(cm.right + c.getWidth() + cm.left), 0 ];
				break;
			case "east":
				return [ cm.right + c.getWidth() + cm.left, 0 ];
				break;
			case "north":
				return [ 0, -(cm.top + cm.bottom + c.getHeight()) ];
				break;
			case "south":
				return [ 0, cm.top + cm.bottom + c.getHeight() ];
				break;
		}
	}
};

Tera.layout.BorderLayout.SplitRegion = function(layout, config, pos) {
	Tera.layout.BorderLayout.SplitRegion.superclass.constructor.call(this, layout, config, pos);
	this.applyLayout = this.applyFns[pos];
};

Tera.extend(Tera.layout.BorderLayout.SplitRegion, Tera.layout.BorderLayout.Region, {

	splitTip :"Drag to resize.",

	collapsibleSplitTip :"Drag to resize. Double click to hide.",

	useSplitTips :false,

	splitSettings : {
		north : {
			orientation :Tera.SplitBar.VERTICAL,
			placement :Tera.SplitBar.TOP,
			maxFn :'getVMaxSize',
			minProp :'minHeight',
			maxProp :'maxHeight'
		},
		south : {
			orientation :Tera.SplitBar.VERTICAL,
			placement :Tera.SplitBar.BOTTOM,
			maxFn :'getVMaxSize',
			minProp :'minHeight',
			maxProp :'maxHeight'
		},
		east : {
			orientation :Tera.SplitBar.HORIZONTAL,
			placement :Tera.SplitBar.RIGHT,
			maxFn :'getHMaxSize',
			minProp :'minWidth',
			maxProp :'maxWidth'
		},
		west : {
			orientation :Tera.SplitBar.HORIZONTAL,
			placement :Tera.SplitBar.LEFT,
			maxFn :'getHMaxSize',
			minProp :'minWidth',
			maxProp :'maxWidth'
		}
	},

	applyFns : {
		west : function(box) {
			if (this.isCollapsed) {
				return this.applyLayoutCollapsed(box);
			}
			var sd = this.splitEl.dom, s = sd.style;
			this.panel.setPosition(box.x, box.y);
			var sw = sd.offsetWidth;
			s.left = (box.x + box.width - sw) + 'px';
			s.top = (box.y) + 'px';
			s.height = Math.max(0, box.height) + 'px';
			this.panel.setSize(box.width - sw, box.height);
		},
		east : function(box) {
			if (this.isCollapsed) {
				return this.applyLayoutCollapsed(box);
			}
			var sd = this.splitEl.dom, s = sd.style;
			var sw = sd.offsetWidth;
			this.panel.setPosition(box.x + sw, box.y);
			s.left = (box.x) + 'px';
			s.top = (box.y) + 'px';
			s.height = Math.max(0, box.height) + 'px';
			this.panel.setSize(box.width - sw, box.height);
		},
		north : function(box) {
			if (this.isCollapsed) {
				return this.applyLayoutCollapsed(box);
			}
			var sd = this.splitEl.dom, s = sd.style;
			var sh = sd.offsetHeight;
			this.panel.setPosition(box.x, box.y);
			s.left = (box.x) + 'px';
			s.top = (box.y + box.height - sh) + 'px';
			s.width = Math.max(0, box.width) + 'px';
			this.panel.setSize(box.width, box.height - sh);
		},
		south : function(box) {
			if (this.isCollapsed) {
				return this.applyLayoutCollapsed(box);
			}
			var sd = this.splitEl.dom, s = sd.style;
			var sh = sd.offsetHeight;
			this.panel.setPosition(box.x, box.y + sh);
			s.left = (box.x) + 'px';
			s.top = (box.y) + 'px';
			s.width = Math.max(0, box.width) + 'px';
			this.panel.setSize(box.width, box.height - sh);
		}
	},

	render : function(ct, p) {
		Tera.layout.BorderLayout.SplitRegion.superclass.render.call(this, ct, p);

		var ps = this.position;

		this.splitEl = ct.createChild( {
			cls :"x-layout-split x-layout-split-" + ps,
			html :"&#160;",
			id :this.panel.id + '-xsplit'
		});

		if (this.collapseMode == 'mini') {
			this.miniSplitEl = this.splitEl.createChild( {
				cls :"x-layout-mini x-layout-mini-" + ps,
				html :"&#160;"
			});
			this.miniSplitEl.addClassOnOver('x-layout-mini-over');
			this.miniSplitEl.on('click', this.onCollapseClick, this, {
				stopEvent :true
			});
		}

		var s = this.splitSettings[ps];

		this.split = new Tera.SplitBar(this.splitEl.dom, p.el, s.orientation);
		this.split.placement = s.placement;
		this.split.getMaximumSize = this[s.maxFn].createDelegate(this);
		this.split.minSize = this.minSize || this[s.minProp];
		this.split.on("beforeapply", this.onSplitMove, this);
		this.split.useShim = this.useShim === true;
		this.maxSize = this.maxSize || this[s.maxProp];

		if (p.hidden) {
			this.splitEl.hide();
		}

		if (this.useSplitTips) {
			this.splitEl.dom.title = this.collapsible ? this.collapsibleSplitTip : this.splitTip;
		}
		if (this.collapsible) {
			this.splitEl.on("dblclick", this.onCollapseClick, this);
		}
	},

	getSize : function() {
		if (this.isCollapsed) {
			return this.collapsedEl.getSize();
		}
		var s = this.panel.getSize();
		if (this.position == 'north' || this.position == 'south') {
			s.height += this.splitEl.dom.offsetHeight;
		} else {
			s.width += this.splitEl.dom.offsetWidth;
		}
		return s;
	},

	getHMaxSize : function() {
		var cmax = this.maxSize || 10000;
		var center = this.layout.center;
		return Math.min(cmax, (this.el.getWidth() + center.el.getWidth()) - center.getMinWidth());
	},

	getVMaxSize : function() {
		var cmax = this.maxSize || 10000;
		var center = this.layout.center;
		return Math.min(cmax, (this.el.getHeight() + center.el.getHeight()) - center.getMinHeight());
	},

	onSplitMove : function(split, newSize) {
		var s = this.panel.getSize();
		this.lastSplitSize = newSize;
		if (this.position == 'north' || this.position == 'south') {
			this.panel.setSize(s.width, newSize);
			this.state.height = newSize;
		} else {
			this.panel.setSize(newSize, s.height);
			this.state.width = newSize;
		}
		this.layout.layout();
		this.panel.saveState();
		return false;
	},

	getSplitBar : function() {
		return this.split;
	}
});

Tera.Container.LAYOUTS['border'] = Tera.layout.BorderLayout;

Tera.layout.FormLayout = Tera.extend(Tera.layout.AnchorLayout, {

	labelSeparator :':',

	getAnchorViewSize : function(ct, target) {
		return ct.body.getStyleSize();
	},

	setContainer : function(ct) {
		Tera.layout.FormLayout.superclass.setContainer.call(this, ct);

		if (ct.labelAlign) {
			ct.addClass('x-form-label-' + ct.labelAlign);
		}

		if (ct.hideLabels) {
			this.labelStyle = "display:none";
			this.elementStyle = "padding-left:0;";
			this.labelAdjust = 0;
		} else {
			this.labelSeparator = ct.labelSeparator || this.labelSeparator;
			ct.labelWidth = ct.labelWidth || 100;
			if (typeof ct.labelWidth == 'number') {
				var pad = (typeof ct.labelPad == 'number' ? ct.labelPad : 5);
				this.labelAdjust = ct.labelWidth + pad;
				this.labelStyle = "width:" + ct.labelWidth + "px;";
				this.elementStyle = "padding-left:" + (ct.labelWidth + pad) + 'px';
			}
			if (ct.labelAlign == 'top') {
				this.labelStyle = "width:auto;";
				this.labelAdjust = 0;
				this.elementStyle = "padding-left:0;";
			}
		}

		if (!this.fieldTpl) {
			var t = new Tera.Template('<div class="x-form-item {5}" tabIndex="-1">',
					'<label for="{0}" style="{2}" class="x-form-item-label">{1}{4}</label>',
					'<div class="x-form-element" id="x-form-el-{0}" style="{3}">', '</div><div class="{6}"></div>', '</div>');
			t.disableFormats = true;
			t.compile();
			Tera.layout.FormLayout.prototype.fieldTpl = t;
		}
	},

	renderItem : function(c, position, target) {
		if (c && !c.rendered && c.isFormField && c.inputType != 'hidden') {
			var args = [ c.id, c.fieldLabel, c.labelStyle || this.labelStyle || '', this.elementStyle || '',
					typeof c.labelSeparator == 'undefined' ? this.labelSeparator : c.labelSeparator,
					(c.itemCls || this.container.itemCls || '') + (c.hideLabel ? ' x-hide-label' : ''), c.clearCls || 'x-form-clear-left' ];
			if (typeof position == 'number') {
				position = target.dom.childNodes[position] || null;
			}
			if (position) {
				this.fieldTpl.insertBefore(position, args);
			} else {
				this.fieldTpl.append(target, args);
			}
			c.render('x-form-el-' + c.id);
		} else {
			Tera.layout.FormLayout.superclass.renderItem.apply(this, arguments);
		}
	},

	adjustWidthAnchor : function(value, comp) {
		return value - (comp.isFormField ? (comp.hideLabel ? 0 : this.labelAdjust) : 0);
	},

	isValidParent : function(c, target) {
		return true;
	}

});

Tera.Container.LAYOUTS['form'] = Tera.layout.FormLayout;

Tera.layout.Accordion = Tera.extend(Tera.layout.FitLayout, {

	fill :true,

	autoWidth :true,

	titleCollapse :true,

	hideCollapseTool :false,

	collapseFirst :false,

	animate :false,

	sequence :false,

	activeOnTop :false,

	renderItem : function(c) {
		if (this.animate === false) {
			c.animCollapse = false;
		}
		c.collapsible = true;
		if (this.autoWidth) {
			c.autoWidth = true;
		}
		if (this.titleCollapse) {
			c.titleCollapse = true;
		}
		if (this.hideCollapseTool) {
			c.hideCollapseTool = true;
		}
		if (this.collapseFirst !== undefined) {
			c.collapseFirst = this.collapseFirst;
		}
		if (!this.activeItem && !c.collapsed) {
			this.activeItem = c;
		} else if (this.activeItem) {
			c.collapsed = true;
		}
		Tera.layout.Accordion.superclass.renderItem.apply(this, arguments);
		c.header.addClass('x-accordion-hd');
		c.on('beforeexpand', this.beforeExpand, this);
	},

	beforeExpand : function(p, anim) {
		var ai = this.activeItem;
		if (ai) {
			if (this.sequence) {
				delete this.activeItem;
				if (!ai.collapsed) {
					ai.collapse( {
						callback : function() {
							p.expand(anim || true);
						},
						scope :this
					});
					return false;
				}
			} else {
				ai.collapse(this.animate);
			}
		}
		this.activeItem = p;
		if (this.activeOnTop) {
			p.el.dom.parentNode.insertBefore(p.el.dom, p.el.dom.parentNode.firstChild);
		}
		this.layout();
	},

	setItemSize : function(item, size) {
		if (this.fill && item) {
			var items = this.container.items.items;
			var hh = 0;
			for ( var i = 0, len = items.length; i < len; i++) {
				var p = items[i];
				if (p != item) {
					hh += (p.getSize().height - p.bwrap.getHeight());
				}
			}
			size.height -= hh;
			item.setSize(size);
		}
	}
});
Tera.Container.LAYOUTS['accordion'] = Tera.layout.Accordion;

Tera.layout.TableLayout = Tera.extend(Tera.layout.ContainerLayout, {

	monitorResize :false,

	setContainer : function(ct) {
		Tera.layout.TableLayout.superclass.setContainer.call(this, ct);

		this.currentRow = 0;
		this.currentColumn = 0;
		this.cells = [];
	},

	onLayout : function(ct, target) {
		var cs = ct.items.items, len = cs.length, c, i;

		if (!this.table) {
			target.addClass('x-table-layout-ct');

			this.table = target.createChild( {
				tag :'table',
				cls :'x-table-layout',
				cellspacing :0,
				cn : {
					tag :'tbody'
				}
			}, null, true);

			this.renderAll(ct, target);
		}
	},

	getRow : function(index) {
		var row = this.table.tBodies[0].childNodes[index];
		if (!row) {
			row = document.createElement('tr');
			this.table.tBodies[0].appendChild(row);
		}
		return row;
	},

	getNextCell : function(c) {
		var cell = this.getNextNonSpan(this.currentColumn, this.currentRow);
		var curCol = this.currentColumn = cell[0], curRow = this.currentRow = cell[1];
		for ( var rowIndex = curRow; rowIndex < curRow + (c.rowspan || 1); rowIndex++) {
			if (!this.cells[rowIndex]) {
				this.cells[rowIndex] = [];
			}
			for ( var colIndex = curCol; colIndex < curCol + (c.colspan || 1); colIndex++) {
				this.cells[rowIndex][colIndex] = true;
			}
		}
		var td = document.createElement('td');
		if (c.cellId) {
			td.id = c.cellId;
		}
		var cls = 'x-table-layout-cell';
		if (c.cellCls) {
			cls += ' ' + c.cellCls;
		}
		td.className = cls;
		if (c.colspan) {
			td.colSpan = c.colspan;
		}
		if (c.rowspan) {
			td.rowSpan = c.rowspan;
		}
		this.getRow(curRow).appendChild(td);
		return td;
	},

	getNextNonSpan : function(colIndex, rowIndex) {
		var cols = this.columns;
		while ((cols && colIndex >= cols) || (this.cells[rowIndex] && this.cells[rowIndex][colIndex])) {
			if (cols && colIndex >= cols) {
				rowIndex++;
				colIndex = 0;
			} else {
				colIndex++;
			}
		}
		return [ colIndex, rowIndex ];
	},

	renderItem : function(c, position, target) {
		if (c && !c.rendered) {
			c.render(this.getNextCell(c));
		}
	},

	isValidParent : function(c, target) {
		return true;
	}

});

Tera.Container.LAYOUTS['table'] = Tera.layout.TableLayout;

Tera.layout.AbsoluteLayout = Tera.extend(Tera.layout.AnchorLayout, {
	extraCls :'x-abs-layout-item',
	isForm :false,

	setContainer : function(ct) {
		Tera.layout.AbsoluteLayout.superclass.setContainer.call(this, ct);
		if (ct.isXType('form')) {
			this.isForm = true;
		}
	},

	onLayout : function(ct, target) {
		if (this.isForm) {
			ct.body.position();
		} else {
			target.position();
		}
		Tera.layout.AbsoluteLayout.superclass.onLayout.call(this, ct, target);
	},

	getAnchorViewSize : function(ct, target) {
		return this.isForm ? ct.body.getStyleSize() : Tera.layout.AbsoluteLayout.superclass.getAnchorViewSize.call(this, ct, target);
	},

	isValidParent : function(c, target) {
		return this.isForm ? true : Tera.layout.AbsoluteLayout.superclass.isValidParent.call(this, c, target);
	},

	adjustWidthAnchor : function(value, comp) {
		return value ? value - comp.getPosition(true)[0] : value;
	},

	adjustHeightAnchor : function(value, comp) {
		return value ? value - comp.getPosition(true)[1] : value;
	}

});
Tera.Container.LAYOUTS['absolute'] = Tera.layout.AbsoluteLayout;

Tera.Viewport = Tera.extend(Tera.Container, {

	initComponent : function() {
		Tera.Viewport.superclass.initComponent.call(this);
		document.getElementsByTagName('html')[0].className += ' x-viewport';
		this.el = Tera.getBody();
		this.el.setHeight = Tera.emptyFn;
		this.el.setWidth = Tera.emptyFn;
		this.el.setSize = Tera.emptyFn;
		this.el.dom.scroll = 'no';
		this.allowDomMove = false;
		this.autoWidth = true;
		this.autoHeight = true;
		Tera.EventManager.onWindowResize(this.fireResize, this);
		this.renderTo = this.el;
	},

	fireResize : function(w, h) {
		this.fireEvent('resize', this, w, h, w, h);
	}
});
Tera.reg('viewport', Tera.Viewport);

Tera.Panel = Tera.extend(Tera.Container, {

	baseCls :'x-panel',

	collapsedCls :'x-panel-collapsed',

	maskDisabled :true,

	animCollapse :Tera.enableFx,

	headerAsText :true,

	buttonAlign :'right',

	collapsed :false,

	collapseFirst :true,

	minButtonWidth :75,

	elements :'body',

	toolTarget :'header',
	collapseEl :'bwrap',
	slideAnchor :'t',

	deferHeight :true,
	expandDefaults : {
		duration :.25
	},
	collapseDefaults : {
		duration :.25
	},

	initComponent : function() {
		Tera.Panel.superclass.initComponent.call(this);

		this.addEvents(

		'bodyresize',

		'titlechange',

		'collapse',

		'expand',

		'beforecollapse',

		'beforeexpand',

		'beforeclose',

		'close',

		'activate',

		'deactivate');

		if (this.tbar) {
			this.elements += ',tbar';
			if (typeof this.tbar == 'object') {
				this.topToolbar = this.tbar;
			}
			delete this.tbar;
		}
		if (this.bbar) {
			this.elements += ',bbar';
			if (typeof this.bbar == 'object') {
				this.bottomToolbar = this.bbar;
			}
			delete this.bbar;
		}

		if (this.header === true) {
			this.elements += ',header';
			delete this.header;
		} else if (this.title && this.header !== false) {
			this.elements += ',header';
		}

		if (this.footer === true) {
			this.elements += ',footer';
			delete this.footer;
		}

		if (this.buttons) {
			var btns = this.buttons;

			this.buttons = [];
			for ( var i = 0, len = btns.length; i < len; i++) {
				if (btns[i].render) {
					this.buttons.push(btns[i]);
				} else {
					this.addButton(btns[i]);
				}
			}
		}
		if (this.autoLoad) {
			this.on('render', this.doAutoLoad, this, {
				delay :10
			});
		}
	},

	createElement : function(name, pnode) {
		if (this[name]) {
			pnode.appendChild(this[name].dom);
			return;
		}

		if (name === 'bwrap' || this.elements.indexOf(name) != -1) {
			if (this[name + 'Cfg']) {
				this[name] = Tera.fly(pnode).createChild(this[name + 'Cfg']);
			} else {
				var el = document.createElement('div');
				el.className = this[name + 'Cls'];
				this[name] = Tera.get(pnode.appendChild(el));
			}
		}
	},

	onRender : function(ct, position) {
		Tera.Panel.superclass.onRender.call(this, ct, position);

		this.createClasses();

		if (this.el) {
			this.el.addClass(this.baseCls);
			this.header = this.el.down('.' + this.headerCls);
			this.bwrap = this.el.down('.' + this.bwrapCls);
			var cp = this.bwrap ? this.bwrap : this.el;
			this.tbar = cp.down('.' + this.tbarCls);
			this.body = cp.down('.' + this.bodyCls);
			this.bbar = cp.down('.' + this.bbarCls);
			this.footer = cp.down('.' + this.footerCls);
			this.fromMarkup = true;
		} else {
			this.el = ct.createChild( {
				id :this.id,
				cls :this.baseCls
			}, position);
		}
		var el = this.el, d = el.dom;

		if (this.cls) {
			this.el.addClass(this.cls);
		}

		if (this.buttons) {
			this.elements += ',footer';
		}

		if (this.frame) {
			el.insertHtml('afterBegin', String.format(Tera.Element.boxMarkup, this.baseCls));

			this.createElement('header', d.firstChild.firstChild.firstChild);
			this.createElement('bwrap', d);

			var bw = this.bwrap.dom;
			var ml = d.childNodes[1], bl = d.childNodes[2];
			bw.appendChild(ml);
			bw.appendChild(bl);

			var mc = bw.firstChild.firstChild.firstChild;
			this.createElement('tbar', mc);
			this.createElement('body', mc);
			this.createElement('bbar', mc);
			this.createElement('footer', bw.lastChild.firstChild.firstChild);

			if (!this.footer) {
				this.bwrap.dom.lastChild.className += ' x-panel-nofooter';
			}
		} else {
			this.createElement('header', d);
			this.createElement('bwrap', d);

			var bw = this.bwrap.dom;
			this.createElement('tbar', bw);
			this.createElement('body', bw);
			this.createElement('bbar', bw);
			this.createElement('footer', bw);

			if (!this.header) {
				this.body.addClass(this.bodyCls + '-noheader');
				if (this.tbar) {
					this.tbar.addClass(this.tbarCls + '-noheader');
				}
			}
		}

		if (this.border === false) {
			this.el.addClass(this.baseCls + '-noborder');
			this.body.addClass(this.bodyCls + '-noborder');
			if (this.header) {
				this.header.addClass(this.headerCls + '-noborder');
			}
			if (this.footer) {
				this.footer.addClass(this.footerCls + '-noborder');
			}
			if (this.tbar) {
				this.tbar.addClass(this.tbarCls + '-noborder');
			}
			if (this.bbar) {
				this.bbar.addClass(this.bbarCls + '-noborder');
			}
		}

		if (this.bodyBorder === false) {
			this.body.addClass(this.bodyCls + '-noborder');
		}

		if (this.bodyStyle) {
			this.body.applyStyles(this.bodyStyle);
		}

		this.bwrap.enableDisplayMode('block');

		if (this.header) {
			this.header.unselectable();

			if (this.headerAsText) {
				this.header.dom.innerHTML = '<span class="' + this.headerTextCls + '">' + this.header.dom.innerHTML + '</span>';

				if (this.iconCls) {
					this.setIconClass(this.iconCls);
				}
			}
		}

		if (this.floating) {
			this.makeFloating(this.floating);
		}

		if (this.collapsible) {
			this.tools = this.tools ? this.tools.slice(0) : [];
			if (!this.hideCollapseTool) {
				this.tools[this.collapseFirst ? 'unshift' : 'push']( {
					id :'toggle',
					handler :this.toggleCollapse,
					scope :this
				});
			}
			if (this.titleCollapse && this.header) {
				this.header.on('click', this.toggleCollapse, this);
				this.header.setStyle('cursor', 'pointer');
			}
		}
		if (this.tools) {
			var ts = this.tools;
			this.tools = {};
			this.addTool.apply(this, ts);
		} else {
			this.tools = {};
		}

		if (this.buttons && this.buttons.length > 0) {
			var tb = this.footer.createChild( {
				cls :'x-panel-btns-ct',
				cn : {
					cls :"x-panel-btns x-panel-btns-" + this.buttonAlign,
					html :'<table cellspacing="0"><tbody><tr></tr></tbody></table><div class="x-clear"></div>'
				}
			}, null, true);
			var tr = tb.getElementsByTagName('tr')[0];
			for ( var i = 0, len = this.buttons.length; i < len; i++) {
				var b = this.buttons[i];
				var td = document.createElement('td');
				td.className = 'x-panel-btn-td';
				b.render(tr.appendChild(td));
			}
		}

		if (this.tbar && this.topToolbar) {
			if (Tera.isArray(this.topToolbar)) {
				this.topToolbar = new Tera.Toolbar(this.topToolbar);
			}
			this.topToolbar.render(this.tbar);
			this.topToolbar.ownerCt = this;
		}
		if (this.bbar && this.bottomToolbar) {
			if (Tera.isArray(this.bottomToolbar)) {
				this.bottomToolbar = new Tera.Toolbar(this.bottomToolbar);
			}
			this.bottomToolbar.render(this.bbar);
			this.bottomToolbar.ownerCt = this;
		}
	},

	setIconClass : function(cls) {
		var old = this.iconCls;
		this.iconCls = cls;
		if (this.rendered && this.header) {
			if (this.frame) {
				this.header.addClass('x-panel-icon');
				this.header.replaceClass(old, this.iconCls);
			} else {
				var hd = this.header.dom;
				var img = hd.firstChild && String(hd.firstChild.tagName).toLowerCase() == 'img' ? hd.firstChild : null;
				if (img) {
					Tera.fly(img).replaceClass(old, this.iconCls);
				} else {
					Tera.DomHelper.insertBefore(hd.firstChild, {
						tag :'img',
						src :Tera.BLANK_IMAGE_URL,
						cls :'x-panel-inline-icon ' + this.iconCls
					});
				}
			}
		}
	},

	makeFloating : function(cfg) {
		this.floating = true;
		this.el = new Tera.Layer(typeof cfg == 'object' ? cfg : {
			shadow :this.shadow !== undefined ? this.shadow : 'sides',
			shadowOffset :this.shadowOffset,
			constrain :false,
			shim :this.shim === false ? false : undefined
		}, this.el);
	},

	getTopToolbar : function() {
		return this.topToolbar;
	},

	getBottomToolbar : function() {
		return this.bottomToolbar;
	},

	addButton : function(config, handler, scope) {
		var bc = {
			handler :handler,
			scope :scope,
			minWidth :this.minButtonWidth,
			hideParent :true
		};
		if (typeof config == "string") {
			bc.text = config;
		} else {
			Tera.apply(bc, config);
		}
		var btn = new Tera.Button(bc);
		btn.ownerCt = this;
		if (!this.buttons) {
			this.buttons = [];
		}
		this.buttons.push(btn);
		return btn;
	},

	addTool : function() {
		if (!this[this.toolTarget]) {
			return;
		}
		if (!this.toolTemplate) {
			var tt = new Tera.Template('<div class="x-tool x-tool-{id}">&#160;</div>');
			tt.disableFormats = true;
			tt.compile();
			Tera.Panel.prototype.toolTemplate = tt;
		}
		for ( var i = 0, a = arguments, len = a.length; i < len; i++) {
			var tc = a[i], overCls = 'x-tool-' + tc.id + '-over';
			var t = this.toolTemplate.insertFirst((tc.align !== 'left') ? this[this.toolTarget] : this[this.toolTarget].child('span'), tc, true);
			this.tools[tc.id] = t;
			t.enableDisplayMode('block');
			t.on('click', this.createToolHandler(t, tc, overCls, this));
			if (tc.on) {
				t.on(tc.on);
			}
			if (tc.hidden) {
				t.hide();
			}
			if (tc.qtip) {
				if (typeof tc.qtip == 'object') {
					Tera.QuickTips.register(Tera.apply( {
						target :t.id
					}, tc.qtip));
				} else {
					t.dom.qtip = tc.qtip;
				}
			}
			t.addClassOnOver(overCls);
		}
	},

	onShow : function() {
		if (this.floating) {
			return this.el.show();
		}
		Tera.Panel.superclass.onShow.call(this);
	},

	onHide : function() {
		if (this.floating) {
			return this.el.hide();
		}
		Tera.Panel.superclass.onHide.call(this);
	},

	createToolHandler : function(t, tc, overCls, panel) {
		return function(e) {
			t.removeClass(overCls);
			e.stopEvent();
			if (tc.handler) {
				tc.handler.call(tc.scope || t, e, t, panel);
			}
		};
	},

	afterRender : function() {
		if (this.fromMarkup && this.height === undefined && !this.autoHeight) {
			this.height = this.el.getHeight();
		}
		if (this.floating && !this.hidden && !this.initHidden) {
			this.el.show();
		}
		if (this.title) {
			this.setTitle(this.title);
		}
		this.setAutoScroll();
		if (this.html) {
			this.body.update(typeof this.html == 'object' ? Tera.DomHelper.markup(this.html) : this.html);
			delete this.html;
		}
		if (this.contentEl) {
			var ce = Tera.getDom(this.contentEl);
			Tera.fly(ce).removeClass( [ 'x-hidden', 'x-hide-display' ]);
			this.body.dom.appendChild(ce);
		}
		if (this.collapsed) {
			this.collapsed = false;
			this.collapse(false);
		}
		Tera.Panel.superclass.afterRender.call(this);
		this.initEvents();
	},

	setAutoScroll : function() {
		if (this.rendered && this.autoScroll) {
			this.body.setOverflow('auto');
		}
	},

	getKeyMap : function() {
		if (!this.keyMap) {
			this.keyMap = new Tera.KeyMap(this.el, this.keys);
		}
		return this.keyMap;
	},

	initEvents : function() {
		if (this.keys) {
			this.getKeyMap();
		}
		if (this.draggable) {
			this.initDraggable();
		}
	},

	initDraggable : function() {

		this.dd = new Tera.Panel.DD(this, typeof this.draggable == 'boolean' ? null : this.draggable);
	},

	beforeEffect : function() {
		if (this.floating) {
			this.el.beforeAction();
		}
		this.el.addClass('x-panel-animated');
	},

	afterEffect : function() {
		this.syncShadow();
		this.el.removeClass('x-panel-animated');
	},

	createEffect : function(a, cb, scope) {
		var o = {
			scope :scope,
			block :true
		};
		if (a === true) {
			o.callback = cb;
			return o;
		} else if (!a.callback) {
			o.callback = cb;
		} else {
			o.callback = function() {
				cb.call(scope);
				Tera.callback(a.callback, a.scope);
			};
		}
		return Tera.applyIf(o, a);
	},

	collapse : function(animate) {
		if (this.collapsed || this.el.hasFxBlock() || this.fireEvent('beforecollapse', this, animate) === false) {
			return;
		}
		var doAnim = animate === true || (animate !== false && this.animCollapse);
		this.beforeEffect();
		this.onCollapse(doAnim, animate);
		return this;
	},

	onCollapse : function(doAnim, animArg) {
		if (doAnim) {
			this[this.collapseEl].slideOut(this.slideAnchor, Tera.apply(this.createEffect(animArg || true, this.afterCollapse, this),
					this.collapseDefaults));
		} else {
			this[this.collapseEl].hide();
			this.afterCollapse();
		}
	},

	afterCollapse : function() {
		this.collapsed = true;
		this.el.addClass(this.collapsedCls);
		this.afterEffect();
		this.fireEvent('collapse', this);
	},

	expand : function(animate) {
		if (!this.collapsed || this.el.hasFxBlock() || this.fireEvent('beforeexpand', this, animate) === false) {
			return;
		}
		var doAnim = animate === true || (animate !== false && this.animCollapse);
		this.el.removeClass(this.collapsedCls);
		this.beforeEffect();
		this.onExpand(doAnim, animate);
		return this;
	},

	onExpand : function(doAnim, animArg) {
		if (doAnim) {
			this[this.collapseEl].slideIn(this.slideAnchor, Tera.apply(this.createEffect(animArg || true, this.afterExpand, this),
					this.expandDefaults));
		} else {
			this[this.collapseEl].show();
			this.afterExpand();
		}
	},

	afterExpand : function() {
		this.collapsed = false;
		this.afterEffect();
		this.fireEvent('expand', this);
	},

	toggleCollapse : function(animate) {
		this[this.collapsed ? 'expand' : 'collapse'](animate);
		return this;
	},

	onDisable : function() {
		if (this.rendered && this.maskDisabled) {
			this.el.mask();
		}
		Tera.Panel.superclass.onDisable.call(this);
	},

	onEnable : function() {
		if (this.rendered && this.maskDisabled) {
			this.el.unmask();
		}
		Tera.Panel.superclass.onEnable.call(this);
	},

	onResize : function(w, h) {
		if (w !== undefined || h !== undefined) {
			if (!this.collapsed) {
				if (typeof w == 'number') {
					this.body.setWidth(this.adjustBodyWidth(w - this.getFrameWidth()));
				} else if (w == 'auto') {
					this.body.setWidth(w);
				}

				if (typeof h == 'number') {
					this.body.setHeight(this.adjustBodyHeight(h - this.getFrameHeight()));
				} else if (h == 'auto') {
					this.body.setHeight(h);
				}
			} else {
				this.queuedBodySize = {
					width :w,
					height :h
				};
				if (!this.queuedExpand && this.allowQueuedExpand !== false) {
					this.queuedExpand = true;
					this.on('expand', function() {
						delete this.queuedExpand;
						this.onResize(this.queuedBodySize.width, this.queuedBodySize.height);
						this.doLayout();
					}, this, {
						single :true
					});
				}
			}
			this.fireEvent('bodyresize', this, w, h);
		}
		this.syncShadow();
	},

	adjustBodyHeight : function(h) {
		return h;
	},

	adjustBodyWidth : function(w) {
		return w;
	},

	onPosition : function() {
		this.syncShadow();
	},

	onDestroy : function() {
		if (this.tools) {
			for ( var k in this.tools) {
				Tera.destroy(this.tools[k]);
			}
		}
		if (this.buttons) {
			for ( var b in this.buttons) {
				Tera.destroy(this.buttons[b]);
			}
		}
		Tera.destroy(this.topToolbar, this.bottomToolbar);
		Tera.Panel.superclass.onDestroy.call(this);
	},

	getFrameWidth : function() {
		var w = this.el.getFrameWidth('lr');

		if (this.frame) {
			var l = this.bwrap.dom.firstChild;
			w += (Tera.fly(l).getFrameWidth('l') + Tera.fly(l.firstChild).getFrameWidth('r'));
			var mc = this.bwrap.dom.firstChild.firstChild.firstChild;
			w += Tera.fly(mc).getFrameWidth('lr');
		}
		return w;
	},

	getFrameHeight : function() {
		var h = this.el.getFrameWidth('tb');
		h += (this.tbar ? this.tbar.getHeight() : 0) + (this.bbar ? this.bbar.getHeight() : 0);

		if (this.frame) {
			var hd = this.el.dom.firstChild;
			var ft = this.bwrap.dom.lastChild;
			h += (hd.offsetHeight + ft.offsetHeight);
			var mc = this.bwrap.dom.firstChild.firstChild.firstChild;
			h += Tera.fly(mc).getFrameWidth('tb');
		} else {
			h += (this.header ? this.header.getHeight() : 0) + (this.footer ? this.footer.getHeight() : 0);
		}
		return h;
	},

	getInnerWidth : function() {
		return this.getSize().width - this.getFrameWidth();
	},

	getInnerHeight : function() {
		return this.getSize().height - this.getFrameHeight();
	},

	syncShadow : function() {
		if (this.floating) {
			this.el.sync(true);
		}
	},

	getLayoutTarget : function() {
		return this.body;
	},

	setTitle : function(title, iconCls) {
		this.title = title;
		if (this.header && this.headerAsText) {
			this.header.child('span').update(title);
		}
		if (iconCls) {
			this.setIconClass(iconCls);
		}
		this.fireEvent('titlechange', this, title);
		return this;
	},

	getUpdater : function() {
		return this.body.getUpdater();
	},

	load : function() {
		var um = this.body.getUpdater();
		um.update.apply(um, arguments);
		return this;
	},

	beforeDestroy : function() {
		Tera.Element.uncache(this.header, this.tbar, this.bbar, this.footer, this.body);
	},

	createClasses : function() {
		this.headerCls = this.baseCls + '-header';
		this.headerTextCls = this.baseCls + '-header-text';
		this.bwrapCls = this.baseCls + '-bwrap';
		this.tbarCls = this.baseCls + '-tbar';
		this.bodyCls = this.baseCls + '-body';
		this.bbarCls = this.baseCls + '-bbar';
		this.footerCls = this.baseCls + '-footer';
	},

	createGhost : function(cls, useShim, appendTo) {
		var el = document.createElement('div');
		el.className = 'x-panel-ghost ' + (cls ? cls : '');
		if (this.header) {
			el.appendChild(this.el.dom.firstChild.cloneNode(true));
		}
		Tera.fly(el.appendChild(document.createElement('ul'))).setHeight(this.bwrap.getHeight());
		el.style.width = this.el.dom.offsetWidth + 'px';
		;
		if (!appendTo) {
			this.container.dom.appendChild(el);
		} else {
			Tera.getDom(appendTo).appendChild(el);
		}
		if (useShim !== false && this.el.useShim !== false) {
			var layer = new Tera.Layer( {
				shadow :false,
				useDisplay :true,
				constrain :false
			}, el);
			layer.show();
			return layer;
		} else {
			return new Tera.Element(el);
		}
	},

	doAutoLoad : function() {
		this.body.load(typeof this.autoLoad == 'object' ? this.autoLoad : {
			url :this.autoLoad
		});
	}

});
Tera.reg('panel', Tera.Panel);

Tera.Window = Tera.extend(Tera.Panel, {

	baseCls :'x-window',

	resizable :true,

	draggable :true,

	closable :true,

	constrain :false,

	constrainHeader :false,

	plain :false,

	minimizable :false,

	maximizable :false,

	minHeight :100,

	minWidth :200,

	expandOnShow :true,

	closeAction :'close',

	elements :'header,body',

	collapsible :false,

	initHidden :true,

	monitorResize :true,

	frame :true,

	floating :true,

	initComponent : function() {
		Tera.Window.superclass.initComponent.call(this);
		this.addEvents(

		'resize',

		'maximize',

		'minimize',

		'restore');
	},

	getState : function() {
		return Tera.apply(Tera.Window.superclass.getState.call(this) || {}, this.getBox());
	},

	onRender : function(ct, position) {
		Tera.Window.superclass.onRender.call(this, ct, position);

		if (this.plain) {
			this.el.addClass('x-window-plain');
		}

		this.focusEl = this.el.createChild( {
			tag :"a",
			href :"#",
			cls :"x-dlg-focus",
			tabIndex :"-1",
			html :"&#160;"
		});
		this.focusEl.swallowEvent('click', true);

		this.proxy = this.el.createProxy("x-window-proxy");
		this.proxy.enableDisplayMode('block');

		if (this.modal) {
			this.mask = this.container.createChild( {
				cls :"ext-el-mask"
			}, this.el.dom);
			this.mask.enableDisplayMode("block");
			this.mask.hide();
		}
	},

	initEvents : function() {
		Tera.Window.superclass.initEvents.call(this);
		if (this.animateTarget) {
			this.setAnimateTarget(this.animateTarget);
		}

		if (this.resizable) {
			this.resizer = new Tera.Resizable(this.el, {
				minWidth :this.minWidth,
				minHeight :this.minHeight,
				handles :this.resizeHandles || "all",
				pinned :true,
				resizeElement :this.resizerAction
			});
			this.resizer.window = this;
			this.resizer.on("beforeresize", this.beforeResize, this);
		}

		if (this.draggable) {
			this.header.addClass("x-window-draggable");
		}
		this.initTools();

		this.el.on("mousedown", this.toFront, this);
		this.manager = this.manager || Tera.WindowMgr;
		this.manager.register(this);
		this.hidden = true;
		if (this.maximized) {
			this.maximized = false;
			this.maximize();
		}
		if (this.closable) {
			var km = this.getKeyMap();
			km.on(27, this.onEsc, this);
			km.disable();
		}
	},

	initDraggable : function() {

		this.dd = new Tera.Window.DD(this);
	},

	onEsc : function() {
		this[this.closeAction]();
	},

	beforeDestroy : function() {
		Tera.destroy(this.resizer, this.dd, this.proxy, this.mask);
		Tera.Window.superclass.beforeDestroy.call(this);
	},

	onDestroy : function() {
		if (this.manager) {
			this.manager.unregister(this);
		}
		Tera.Window.superclass.onDestroy.call(this);
	},

	initTools : function() {
		if (this.minimizable) {
			this.addTool( {
				id :'minimize',
				handler :this.minimize.createDelegate(this, [])
			});
		}
		if (this.maximizable) {
			this.addTool( {
				id :'maximize',
				handler :this.maximize.createDelegate(this, [])
			});
			this.addTool( {
				id :'restore',
				handler :this.restore.createDelegate(this, []),
				hidden :true
			});
			this.header.on('dblclick', this.toggleMaximize, this);
		}
		if (this.closable) {
			this.addTool( {
				id :'close',
				handler :this[this.closeAction].createDelegate(this, [])
			});
		}
	},

	resizerAction : function() {
		var box = this.proxy.getBox();
		this.proxy.hide();
		this.window.handleResize(box);
		return box;
	},

	beforeResize : function() {
		this.resizer.minHeight = Math.max(this.minHeight, this.getFrameHeight() + 40);
		this.resizer.minWidth = Math.max(this.minWidth, this.getFrameWidth() + 40);
		this.resizeBox = this.el.getBox();
	},

	updateHandles : function() {
		if (Tera.isIE && this.resizer) {
			this.resizer.syncHandleHeight();
			this.el.repaint();
		}
	},

	handleResize : function(box) {
		var rz = this.resizeBox;
		if (rz.x != box.x || rz.y != box.y) {
			this.updateBox(box);
		} else {
			this.setSize(box);
		}
		this.focus();
		this.updateHandles();
		this.saveState();
		this.fireEvent("resize", this, box.width, box.height);
	},

	focus : function() {
		var f = this.focusEl, db = this.defaultButton, t = typeof db;
		if (t != 'undefined') {
			if (t == 'number') {
				f = this.buttons[db];
			} else if (t == 'string') {
				f = Tera.getCmp(db);
			} else {
				f = db;
			}
		}
		f.focus.defer(10, f);
	},

	setAnimateTarget : function(el) {
		el = Tera.get(el);
		this.animateTarget = el;
	},

	beforeShow : function() {
		delete this.el.lastXY;
		delete this.el.lastLT;
		if (this.x === undefined || this.y === undefined) {
			var xy = this.el.getAlignToXY(this.container, 'c-c');
			var pos = this.el.translatePoints(xy[0], xy[1]);
			this.x = this.x === undefined ? pos.left : this.x;
			this.y = this.y === undefined ? pos.top : this.y;
		}
		this.el.setLeftTop(this.x, this.y);

		if (this.expandOnShow) {
			this.expand(false);
		}

		if (this.modal) {
			Tera.getBody().addClass("x-body-masked");
			this.mask.setSize(Tera.lib.Dom.getViewWidth(true), Tera.lib.Dom.getViewHeight(true));
			this.mask.show();
		}
	},

	show : function(animateTarget, cb, scope) {
		if (!this.rendered) {
			this.render(Tera.getBody());
		}
		if (this.hidden === false) {
			this.toFront();
			return;
		}
		if (this.fireEvent("beforeshow", this) === false) {
			return;
		}
		if (cb) {
			this.on('show', cb, scope, {
				single :true
			});
		}
		this.hidden = false;
		if (animateTarget !== undefined) {
			this.setAnimateTarget(animateTarget);
		}
		this.beforeShow();
		if (this.animateTarget) {
			this.animShow();
		} else {
			this.afterShow();
		}
	},

	afterShow : function() {
		this.proxy.hide();
		this.el.setStyle('display', 'block');
		this.el.show();
		if (this.maximized) {
			this.fitContainer();
		}
		if (Tera.isMac && Tera.isGecko) {
			this.cascade(this.setAutoScroll);
		}

		if (this.monitorResize || this.modal || this.constrain || this.constrainHeader) {
			Tera.EventManager.onWindowResize(this.onWindowResize, this);
		}
		this.doConstrain();
		if (this.layout) {
			this.doLayout();
		}
		if (this.keyMap) {
			this.keyMap.enable();
		}
		this.toFront();
		this.updateHandles();
		this.fireEvent("show", this);
	},

	animShow : function() {
		this.proxy.show();
		this.proxy.setBox(this.animateTarget.getBox());
		this.proxy.setOpacity(0);
		var b = this.getBox(false);
		b.callback = this.afterShow;
		b.scope = this;
		b.duration = .25;
		b.easing = 'easeNone';
		b.opacity = .5;
		b.block = true;
		this.el.setStyle('display', 'none');
		this.proxy.shift(b);
	},

	hide : function(animateTarget, cb, scope) {
		if (this.hidden || this.fireEvent("beforehide", this) === false) {
			return;
		}
		if (cb) {
			this.on('hide', cb, scope, {
				single :true
			});
		}
		this.hidden = true;
		if (animateTarget !== undefined) {
			this.setAnimateTarget(animateTarget);
		}
		if (this.animateTarget) {
			this.animHide();
		} else {
			this.el.hide();
			this.afterHide();
		}
	},

	afterHide : function() {
		this.proxy.hide();
		if (this.monitorResize || this.modal || this.constrain || this.constrainHeader) {
			Tera.EventManager.removeResizeListener(this.onWindowResize, this);
		}
		if (this.modal) {
			this.mask.hide();
			Tera.getBody().removeClass("x-body-masked");
		}
		if (this.keyMap) {
			this.keyMap.disable();
		}
		this.fireEvent("hide", this);
	},

	animHide : function() {
		this.proxy.setOpacity(.5);
		this.proxy.show();
		var tb = this.getBox(false);
		this.proxy.setBox(tb);
		this.el.hide();
		var b = this.animateTarget.getBox();
		b.callback = this.afterHide;
		b.scope = this;
		b.duration = .25;
		b.easing = 'easeNone';
		b.block = true;
		b.opacity = 0;
		this.proxy.shift(b);
	},

	onWindowResize : function() {
		if (this.maximized) {
			this.fitContainer();
		}
		if (this.modal) {
			this.mask.setSize('100%', '100%');
			var force = this.mask.dom.offsetHeight;
			this.mask.setSize(Tera.lib.Dom.getViewWidth(true), Tera.lib.Dom.getViewHeight(true));
		}
		this.doConstrain();
	},

	doConstrain : function() {
		if (this.constrain || this.constrainHeader) {
			var offsets;
			if (this.constrain) {
				offsets = {
					right :this.el.shadowOffset,
					left :this.el.shadowOffset,
					bottom :this.el.shadowOffset
				};
			} else {
				var s = this.getSize();
				offsets = {
					right :-(s.width - 100),
					bottom :-(s.height - 25)
				};
			}

			var xy = this.el.getConstrainToXY(this.container, true, offsets);
			if (xy) {
				this.setPosition(xy[0], xy[1]);
			}
		}
	},

	ghost : function(cls) {
		var ghost = this.createGhost(cls);
		var box = this.getBox(true);
		ghost.setLeftTop(box.x, box.y);
		ghost.setWidth(box.width);
		this.el.hide();
		this.activeGhost = ghost;
		return ghost;
	},

	unghost : function(show, matchPosition) {
		if (show !== false) {
			this.el.show();
			this.focus();
			if (Tera.isMac && Tera.isGecko) {
				this.cascade(this.setAutoScroll);
			}
		}
		if (matchPosition !== false) {
			this.setPosition(this.activeGhost.getLeft(true), this.activeGhost.getTop(true));
		}
		this.activeGhost.hide();
		this.activeGhost.remove();
		delete this.activeGhost;
	},

	minimize : function() {
		this.fireEvent('minimize', this);
	},

	close : function() {
		if (this.fireEvent("beforeclose", this) !== false) {
			this.hide(null, function() {
				this.fireEvent('close', this);
				this.destroy();
			}, this);
		}
	},

	maximize : function() {
		if (!this.maximized) {
			this.expand(false);
			this.restoreSize = this.getSize();
			this.restorePos = this.getPosition(true);
			if (this.maximizable) {
				this.tools.maximize.hide();
				this.tools.restore.show();
			}
			this.maximized = true;
			this.el.disableShadow();

			if (this.dd) {
				this.dd.lock();
			}
			if (this.collapsible) {
				this.tools.toggle.hide();
			}
			this.el.addClass('x-window-maximized');
			this.container.addClass('x-window-maximized-ct');

			this.setPosition(0, 0);
			this.fitContainer();
			this.fireEvent('maximize', this);
		}
	},

	restore : function() {
		if (this.maximized) {
			this.el.removeClass('x-window-maximized');
			this.tools.restore.hide();
			this.tools.maximize.show();
			this.setPosition(this.restorePos[0], this.restorePos[1]);
			this.setSize(this.restoreSize.width, this.restoreSize.height);
			delete this.restorePos;
			delete this.restoreSize;
			this.maximized = false;
			this.el.enableShadow(true);

			if (this.dd) {
				this.dd.unlock();
			}
			if (this.collapsible) {
				this.tools.toggle.show();
			}
			this.container.removeClass('x-window-maximized-ct');

			this.doConstrain();
			this.fireEvent('restore', this);
		}
	},

	toggleMaximize : function() {
		this[this.maximized ? 'restore' : 'maximize']();
	},

	fitContainer : function() {
		var vs = this.container.getViewSize();
		this.setSize(vs.width, vs.height);
	},

	setZIndex : function(index) {
		if (this.modal) {
			this.mask.setStyle("z-index", index);
		}
		this.el.setZIndex(++index);
		index += 5;

		if (this.resizer) {
			this.resizer.proxy.setStyle("z-index", ++index);
		}

		this.lastZIndex = index;
	},

	alignTo : function(element, position, offsets) {
		var xy = this.el.getAlignToXY(element, position, offsets);
		this.setPagePosition(xy[0], xy[1]);
		return this;
	},

	anchorTo : function(el, alignment, offsets, monitorScroll, _pname) {
		var action = function() {
			this.alignTo(el, alignment, offsets);
		};
		Tera.EventManager.onWindowResize(action, this);
		var tm = typeof monitorScroll;
		if (tm != 'undefined') {
			Tera.EventManager.on(window, 'scroll', action, this, {
				buffer :tm == 'number' ? monitorScroll : 50
			});
		}
		action.call(this);
		this[_pname] = action;
		return this;
	},

	toFront : function() {
		if (this.manager.bringToFront(this)) {
			this.focus();
		}
		return this;
	},

	setActive : function(active) {
		if (active) {
			if (!this.maximized) {
				this.el.enableShadow(true);
			}
			this.fireEvent('activate', this);
		} else {
			this.el.disableShadow();
			this.fireEvent('deactivate', this);
		}
	},

	toBack : function() {
		this.manager.sendToBack(this);
		return this;
	},

	center : function() {
		var xy = this.el.getAlignToXY(this.container, 'c-c');
		this.setPagePosition(xy[0], xy[1]);
		return this;
	}
});
Tera.reg('window', Tera.Window);

Tera.Window.DD = function(win) {
	this.win = win;
	Tera.Window.DD.superclass.constructor.call(this, win.el.id, 'WindowDD-' + win.id);
	this.setHandleElId(win.header.id);
	this.scroll = false;
};

Tera.extend(Tera.Window.DD, Tera.dd.DD, {
	moveOnly :true,
	headerOffsets : [ 100, 25 ],
	startDrag : function() {
		var w = this.win;
		this.proxy = w.ghost();
		if (w.constrain !== false) {
			var so = w.el.shadowOffset;
			this.constrainTo(w.container, {
				right :so,
				left :so,
				bottom :so
			});
		} else if (w.constrainHeader !== false) {
			var s = this.proxy.getSize();
			this.constrainTo(w.container, {
				right :-(s.width - this.headerOffsets[0]),
				bottom :-(s.height - this.headerOffsets[1])
			});
		}
	},
	b4Drag :Tera.emptyFn,

	onDrag : function(e) {
		this.alignElWithMouse(this.proxy, e.getPageX(), e.getPageY());
	},

	endDrag : function(e) {
		this.win.unghost();
		this.win.saveState();
	}
});

Tera.WindowGroup = function() {
	var list = {};
	var accessList = [];
	var front = null;

	var sortWindows = function(d1, d2) {
		return (!d1._lastAccess || d1._lastAccess < d2._lastAccess) ? -1 : 1;
	};

	var orderWindows = function() {
		var a = accessList, len = a.length;
		if (len > 0) {
			a.sort(sortWindows);
			var seed = a[0].manager.zseed;
			for ( var i = 0; i < len; i++) {
				var win = a[i];
				if (win && !win.hidden) {
					win.setZIndex(seed + (i * 10));
				}
			}
		}
		activateLast();
	};

	var setActiveWin = function(win) {
		if (win != front) {
			if (front) {
				front.setActive(false);
			}
			front = win;
			if (win) {
				win.setActive(true);
			}
		}
	};

	var activateLast = function() {
		for ( var i = accessList.length - 1; i >= 0; --i) {
			if (!accessList[i].hidden) {
				setActiveWin(accessList[i]);
				return;
			}
		}
		setActiveWin(null);
	};

	return {

		zseed :9000,

		register : function(win) {
			list[win.id] = win;
			accessList.push(win);
			win.on('hide', activateLast);
		},

		unregister : function(win) {
			delete list[win.id];
			win.un('hide', activateLast);
			accessList.remove(win);
		},

		get : function(id) {
			return typeof id == "object" ? id : list[id];
		},

		bringToFront : function(win) {
			win = this.get(win);
			if (win != front) {
				win._lastAccess = new Date().getTime();
				orderWindows();
				return true;
			}
			return false;
		},

		sendToBack : function(win) {
			win = this.get(win);
			win._lastAccess = -(new Date().getTime());
			orderWindows();
			return win;
		},

		hideAll : function() {
			for ( var id in list) {
				if (list[id] && typeof list[id] != "function" && list[id].isVisible()) {
					list[id].hide();
				}
			}
		},

		getActive : function() {
			return front;
		},

		getBy : function(fn, scope) {
			var r = [];
			for ( var i = accessList.length - 1; i >= 0; --i) {
				var win = accessList[i];
				if (fn.call(scope || win, win) !== false) {
					r.push(win);
				}
			}
			return r;
		},

		each : function(fn, scope) {
			for ( var id in list) {
				if (list[id] && typeof list[id] != "function") {
					if (fn.call(scope || list[id], list[id]) === false) {
						return;
					}
				}
			}
		}
	};
};

Tera.WindowMgr = new Tera.WindowGroup();

Tera.dd.PanelProxy = function(panel, config) {
	this.panel = panel;
	this.id = this.panel.id + '-ddproxy';
	Tera.apply(this, config);
};

Tera.dd.PanelProxy.prototype = {

	insertProxy :true,

	setStatus :Tera.emptyFn,
	reset :Tera.emptyFn,
	update :Tera.emptyFn,
	stop :Tera.emptyFn,
	sync :Tera.emptyFn,

	getEl : function() {
		return this.ghost;
	},

	getGhost : function() {
		return this.ghost;
	},

	getProxy : function() {
		return this.proxy;
	},

	hide : function() {
		if (this.ghost) {
			if (this.proxy) {
				this.proxy.remove();
				delete this.proxy;
			}
			this.panel.el.dom.style.display = '';
			this.ghost.remove();
			delete this.ghost;
		}
	},

	show : function() {
		if (!this.ghost) {
			this.ghost = this.panel.createGhost(undefined, undefined, Tera.getBody());
			this.ghost.setXY(this.panel.el.getXY())
			if (this.insertProxy) {
				this.proxy = this.panel.el.insertSibling( {
					cls :'x-panel-dd-spacer'
				});
				this.proxy.setSize(this.panel.getSize());
			}
			this.panel.el.dom.style.display = 'none';
		}
	},

	repair : function(xy, callback, scope) {
		this.hide();
		if (typeof callback == "function") {
			callback.call(scope || this);
		}
	},

	moveProxy : function(parentNode, before) {
		if (this.proxy) {
			parentNode.insertBefore(this.proxy.dom, before);
		}
	}
};

Tera.Panel.DD = function(panel, cfg) {
	this.panel = panel;
	this.dragData = {
		panel :panel
	};
	this.proxy = new Tera.dd.PanelProxy(panel, cfg);
	Tera.Panel.DD.superclass.constructor.call(this, panel.el, cfg);
	var h = panel.header;
	if (h) {
		this.setHandleElId(h.id);
	}
	(h ? h : this.panel.body).setStyle('cursor', 'move');
	this.scroll = false;
};

Tera.extend(Tera.Panel.DD, Tera.dd.DragSource, {
	showFrame :Tera.emptyFn,
	startDrag :Tera.emptyFn,
	b4StartDrag : function(x, y) {
		this.proxy.show();
	},
	b4MouseDown : function(e) {
		var x = e.getPageX();
		var y = e.getPageY();
		this.autoOffset(x, y);
	},
	onInitDrag : function(x, y) {
		this.onStartDrag(x, y);
		return true;
	},
	createFrame :Tera.emptyFn,
	getDragEl : function(e) {
		return this.proxy.ghost.dom;
	},
	endDrag : function(e) {
		this.proxy.hide();
		this.panel.saveState();
	},

	autoOffset : function(x, y) {
		x -= this.startPageX;
		y -= this.startPageY;
		this.setDelta(x, y);
	}
});

Tera.state.Provider = function() {

	this.addEvents("statechange");
	this.state = {};
	Tera.state.Provider.superclass.constructor.call(this);
};
Tera.extend(Tera.state.Provider, Tera.util.Observable, {

	get : function(name, defaultValue) {
		return typeof this.state[name] == "undefined" ? defaultValue : this.state[name];
	},

	clear : function(name) {
		delete this.state[name];
		this.fireEvent("statechange", this, name, null);
	},

	set : function(name, value) {
		this.state[name] = value;
		this.fireEvent("statechange", this, name, value);
	},

	decodeValue : function(cookie) {
		var re = /^(a|n|d|b|s|o)\:(.*)$/;
		var matches = re.exec(unescape(cookie));
		if (!matches || !matches[1])
			return;
		var type = matches[1];
		var v = matches[2];
		switch (type) {
			case "n":
				return parseFloat(v);
			case "d":
				return new Date(Date.parse(v));
			case "b":
				return (v == "1");
			case "a":
				var all = [];
				var values = v.split("^");
				for ( var i = 0, len = values.length; i < len; i++) {
					all.push(this.decodeValue(values[i]));
				}
				return all;
			case "o":
				var all = {};
				var values = v.split("^");
				for ( var i = 0, len = values.length; i < len; i++) {
					var kv = values[i].split("=");
					all[kv[0]] = this.decodeValue(kv[1]);
				}
				return all;
			default:
				return v;
		}
	},

	encodeValue : function(v) {
		var enc;
		if (typeof v == "number") {
			enc = "n:" + v;
		} else if (typeof v == "boolean") {
			enc = "b:" + (v ? "1" : "0");
		} else if (Tera.isDate(v)) {
			enc = "d:" + v.toGMTString();
		} else if (Tera.isArray(v)) {
			var flat = "";
			for ( var i = 0, len = v.length; i < len; i++) {
				flat += this.encodeValue(v[i]);
				if (i != len - 1)
					flat += "^";
			}
			enc = "a:" + flat;
		} else if (typeof v == "object") {
			var flat = "";
			for ( var key in v) {
				if (typeof v[key] != "function" && v[key] !== undefined) {
					flat += key + "=" + this.encodeValue(v[key]) + "^";
				}
			}
			enc = "o:" + flat.substring(0, flat.length - 1);
		} else {
			enc = "s:" + v;
		}
		return escape(enc);
	}
});

Tera.state.Manager = function() {
	var provider = new Tera.state.Provider();

	return {

		setProvider : function(stateProvider) {
			provider = stateProvider;
		},

		get : function(key, defaultValue) {
			return provider.get(key, defaultValue);
		},

		set : function(key, value) {
			provider.set(key, value);
		},

		clear : function(key) {
			provider.clear(key);
		},

		getProvider : function() {
			return provider;
		}
	};
}();

Tera.state.CookieProvider = function(config) {
	Tera.state.CookieProvider.superclass.constructor.call(this);
	this.path = "/";
	this.expires = new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 7));
	this.domain = null;
	this.secure = false;
	Tera.apply(this, config);
	this.state = this.readCookies();
};

Tera.extend(Tera.state.CookieProvider, Tera.state.Provider, {

	set : function(name, value) {
		if (typeof value == "undefined" || value === null) {
			this.clear(name);
			return;
		}
		this.setCookie(name, value);
		Tera.state.CookieProvider.superclass.set.call(this, name, value);
	},

	clear : function(name) {
		this.clearCookie(name);
		Tera.state.CookieProvider.superclass.clear.call(this, name);
	},

	readCookies : function() {
		var cookies = {};
		var c = document.cookie + ";";
		var re = /\s?(.*?)=(.*?);/g;
		var matches;
		while ((matches = re.exec(c)) != null) {
			var name = matches[1];
			var value = matches[2];
			if (name && name.substring(0, 3) == "ys-") {
				cookies[name.substr(3)] = this.decodeValue(value);
			}
		}
		return cookies;
	},

	setCookie : function(name, value) {
		document.cookie = "ys-" + name + "=" + this.encodeValue(value) + ((this.expires == null) ? "" : ("; expires=" + this.expires.toGMTString()))
				+ ((this.path == null) ? "" : ("; path=" + this.path)) + ((this.domain == null) ? "" : ("; domain=" + this.domain))
				+ ((this.secure == true) ? "; secure" : "");
	},

	clearCookie : function(name) {
		document.cookie = "ys-" + name + "=null; expires=Thu, 01-Jan-70 00:00:01 GMT" + ((this.path == null) ? "" : ("; path=" + this.path))
				+ ((this.domain == null) ? "" : ("; domain=" + this.domain)) + ((this.secure == true) ? "; secure" : "");
	}
});

Tera.DataView = Tera.extend(Tera.BoxComponent, {

	selectedClass :"x-view-selected",

	emptyText :"",

	deferEmptyText :true,

	last :false,

	initComponent : function() {
		Tera.DataView.superclass.initComponent.call(this);
		if (typeof this.tpl == "string") {
			this.tpl = new Tera.XTemplate(this.tpl);
		}

		this.addEvents(

		"beforeclick",

		"click",

		"containerclick",

		"dblclick",

		"contextmenu",

		"selectionchange",

		"beforeselect");

		this.all = new Tera.CompositeElementLite();
		this.selected = new Tera.CompositeElementLite();
	},

	onRender : function() {
		if (!this.el) {
			this.el = document.createElement('div');
			this.el.id = this.id;
		}
		Tera.DataView.superclass.onRender.apply(this, arguments);
	},

	afterRender : function() {
		Tera.DataView.superclass.afterRender.call(this);

		this.el.on( {
			"click" :this.onClick,
			"dblclick" :this.onDblClick,
			"contextmenu" :this.onContextMenu,
			scope :this
		});

		if (this.overClass) {
			this.el.on( {
				"mouseover" :this.onMouseOver,
				"mouseout" :this.onMouseOut,
				scope :this
			});
		}

		if (this.store) {
			this.setStore(this.store, true);
		}
	},

	refresh : function() {
		this.clearSelections(false, true);
		this.el.update("");
		var html = [];
		var records = this.store.getRange();
		if (records.length < 1) {
			if (!this.deferEmptyText || this.hasSkippedEmptyText) {
				this.el.update(this.emptyText);
			}
			this.hasSkippedEmptyText = true;
			this.all.clear();
			return;
		}
		this.tpl.overwrite(this.el, this.collectData(records, 0));
		this.all.fill(Tera.query(this.itemSelector, this.el.dom));
		this.updateIndexes(0);
	},

	prepareData : function(data) {
		return data;
	},

	collectData : function(records, startIndex) {
		var r = [];
		for ( var i = 0, len = records.length; i < len; i++) {
			r[r.length] = this.prepareData(records[i].data, startIndex + i, records[i]);
		}
		return r;
	},

	bufferRender : function(records) {
		var div = document.createElement('div');
		this.tpl.overwrite(div, this.collectData(records));
		return Tera.query(this.itemSelector, div);
	},

	onUpdate : function(ds, record) {
		var index = this.store.indexOf(record);
		var sel = this.isSelected(index);
		var original = this.all.elements[index];
		var node = this.bufferRender( [ record ], index)[0];

		this.all.replaceElement(index, node, true);
		if (sel) {
			this.selected.replaceElement(original, node);
			this.all.item(index).addClass(this.selectedClass);
		}
		this.updateIndexes(index, index);
	},

	onAdd : function(ds, records, index) {
		if (this.all.getCount() == 0) {
			this.refresh();
			return;
		}
		var nodes = this.bufferRender(records, index), n, a = this.all.elements;
		if (index < this.all.getCount()) {
			n = this.all.item(index).insertSibling(nodes, 'before', true);
			a.splice.apply(a, [ index, 0 ].concat(nodes));
		} else {
			n = this.all.last().insertSibling(nodes, 'after', true);
			a.push.apply(a, nodes);
		}
		this.updateIndexes(index);
	},

	onRemove : function(ds, record, index) {
		this.deselect(index);
		this.all.removeElement(index, true);
		this.updateIndexes(index);
	},

	refreshNode : function(index) {
		this.onUpdate(this.store, this.store.getAt(index));
	},

	updateIndexes : function(startIndex, endIndex) {
		var ns = this.all.elements;
		startIndex = startIndex || 0;
		endIndex = endIndex || ((endIndex === 0) ? 0 : (ns.length - 1));
		for ( var i = startIndex; i <= endIndex; i++) {
			ns[i].viewIndex = i;
		}
	},

	setStore : function(store, initial) {
		if (!initial && this.store) {
			this.store.un("beforeload", this.onBeforeLoad, this);
			this.store.un("datachanged", this.refresh, this);
			this.store.un("add", this.onAdd, this);
			this.store.un("remove", this.onRemove, this);
			this.store.un("update", this.onUpdate, this);
			this.store.un("clear", this.refresh, this);
		}
		if (store) {
			store = Tera.StoreMgr.lookup(store);
			store.on("beforeload", this.onBeforeLoad, this);
			store.on("datachanged", this.refresh, this);
			store.on("add", this.onAdd, this);
			store.on("remove", this.onRemove, this);
			store.on("update", this.onUpdate, this);
			store.on("clear", this.refresh, this);
		}
		this.store = store;
		if (store) {
			this.refresh();
		}
	},

	findItemFromChild : function(node) {
		return Tera.fly(node).findParent(this.itemSelector, this.el);
	},

	onClick : function(e) {
		var item = e.getTarget(this.itemSelector, this.el);
		if (item) {
			var index = this.indexOf(item);
			if (this.onItemClick(item, index, e) !== false) {
				this.fireEvent("click", this, index, item, e);
			}
		} else {
			if (this.fireEvent("containerclick", this, e) !== false) {
				this.clearSelections();
			}
		}
	},

	onContextMenu : function(e) {
		var item = e.getTarget(this.itemSelector, this.el);
		if (item) {
			this.fireEvent("contextmenu", this, this.indexOf(item), item, e);
		}
	},

	onDblClick : function(e) {
		var item = e.getTarget(this.itemSelector, this.el);
		if (item) {
			this.fireEvent("dblclick", this, this.indexOf(item), item, e);
		}
	},

	onMouseOver : function(e) {
		var item = e.getTarget(this.itemSelector, this.el);
		if (item && item !== this.lastItem) {
			this.lastItem = item;
			Tera.fly(item).addClass(this.overClass);
		}
	},

	onMouseOut : function(e) {
		if (this.lastItem) {
			if (!e.within(this.lastItem, true)) {
				Tera.fly(this.lastItem).removeClass(this.overClass);
				delete this.lastItem;
			}
		}
	},

	onItemClick : function(item, index, e) {
		if (this.fireEvent("beforeclick", this, index, item, e) === false) {
			return false;
		}
		if (this.multiSelect) {
			this.doMultiSelection(item, index, e);
			e.preventDefault();
		} else if (this.singleSelect) {
			this.doSingleSelection(item, index, e);
			e.preventDefault();
		}
		return true;
	},

	doSingleSelection : function(item, index, e) {
		if (e.ctrlKey && this.isSelected(index)) {
			this.deselect(index);
		} else {
			this.select(index, false);
		}
	},

	doMultiSelection : function(item, index, e) {
		if (e.shiftKey && this.last !== false) {
			var last = this.last;
			this.selectRange(last, index, e.ctrlKey);
			this.last = last;
		} else {
			if ((e.ctrlKey || this.simpleSelect) && this.isSelected(index)) {
				this.deselect(index);
			} else {
				this.select(index, e.ctrlKey || e.shiftKey || this.simpleSelect);
			}
		}
	},

	getSelectionCount : function() {
		return this.selected.getCount()
	},

	getSelectedNodes : function() {
		return this.selected.elements;
	},

	getSelectedIndexes : function() {
		var indexes = [], s = this.selected.elements;
		for ( var i = 0, len = s.length; i < len; i++) {
			indexes.push(s[i].viewIndex);
		}
		return indexes;
	},

	getSelectedRecords : function() {
		var r = [], s = this.selected.elements;
		for ( var i = 0, len = s.length; i < len; i++) {
			r[r.length] = this.store.getAt(s[i].viewIndex);
		}
		return r;
	},

	getRecords : function(nodes) {
		var r = [], s = nodes;
		for ( var i = 0, len = s.length; i < len; i++) {
			r[r.length] = this.store.getAt(s[i].viewIndex);
		}
		return r;
	},

	getRecord : function(node) {
		return this.store.getAt(node.viewIndex);
	},

	clearSelections : function(suppressEvent, skipUpdate) {
		if ((this.multiSelect || this.singleSelect) && this.selected.getCount() > 0) {
			if (!skipUpdate) {
				this.selected.removeClass(this.selectedClass);
			}
			this.selected.clear();
			this.last = false;
			if (!suppressEvent) {
				this.fireEvent("selectionchange", this, this.selected.elements);
			}
		}
	},

	isSelected : function(node) {
		return this.selected.contains(this.getNode(node));
	},

	deselect : function(node) {
		if (this.isSelected(node)) {
			var node = this.getNode(node);
			this.selected.removeElement(node);
			if (this.last == node.viewIndex) {
				this.last = false;
			}
			Tera.fly(node).removeClass(this.selectedClass);
			this.fireEvent("selectionchange", this, this.selected.elements);
		}
	},

	select : function(nodeInfo, keepExisting, suppressEvent) {
		if (Tera.isArray(nodeInfo)) {
			if (!keepExisting) {
				this.clearSelections(true);
			}
			for ( var i = 0, len = nodeInfo.length; i < len; i++) {
				this.select(nodeInfo[i], true, true);
			}
			if (!suppressEvent) {
				this.fireEvent("selectionchange", this, this.selected.elements);
			}
		} else {
			var node = this.getNode(nodeInfo);
			if (!keepExisting) {
				this.clearSelections(true);
			}
			if (node && !this.isSelected(node)) {
				if (this.fireEvent("beforeselect", this, node, this.selected.elements) !== false) {
					Tera.fly(node).addClass(this.selectedClass);
					this.selected.add(node);
					this.last = node.viewIndex;
					if (!suppressEvent) {
						this.fireEvent("selectionchange", this, this.selected.elements);
					}
				}
			}
		}
	},

	selectRange : function(start, end, keepExisting) {
		if (!keepExisting) {
			this.clearSelections(true);
		}
		this.select(this.getNodes(start, end), true);
	},

	getNode : function(nodeInfo) {
		if (typeof nodeInfo == "string") {
			return document.getElementById(nodeInfo);
		} else if (typeof nodeInfo == "number") {
			return this.all.elements[nodeInfo];
		}
		return nodeInfo;
	},

	getNodes : function(start, end) {
		var ns = this.all.elements;
		start = start || 0;
		end = typeof end == "undefined" ? ns.length - 1 : end;
		var nodes = [], i;
		if (start <= end) {
			for (i = start; i <= end; i++) {
				nodes.push(ns[i]);
			}
		} else {
			for (i = start; i >= end; i--) {
				nodes.push(ns[i]);
			}
		}
		return nodes;
	},

	indexOf : function(node) {
		node = this.getNode(node);
		if (typeof node.viewIndex == "number") {
			return node.viewIndex;
		}
		return this.all.indexOf(node);
	},

	onBeforeLoad : function() {
		if (this.loadingText) {
			this.clearSelections(false, true);
			this.el.update('<div class="loading-indicator">' + this.loadingText + '</div>');
			this.all.clear();
		}
	},

	onDestroy : function() {
		Tera.DataView.superclass.onDestroy.call(this);
		this.setStore(null);
	}
});

Tera.reg('dataview', Tera.DataView);

Tera.ColorPalette = function(config) {
	Tera.ColorPalette.superclass.constructor.call(this, config);
	this.addEvents(

	'select');

	if (this.handler) {
		this.on("select", this.handler, this.scope, true);
	}
};
Tera
		.extend(
				Tera.ColorPalette,
				Tera.Component,
				{

					itemCls :"x-color-palette",

					value :null,
					clickEvent :'click',
					ctype :"Tera.ColorPalette",

					allowReselect :false,

					colors : [ "000000", "993300", "333300", "003300", "003366", "000080", "333399", "333333", "800000", "FF6600", "808000",
							"008000", "008080", "0000FF", "666699", "808080", "FF0000", "FF9900", "99CC00", "339966", "33CCCC", "3366FF", "800080",
							"969696", "FF00FF", "FFCC00", "FFFF00", "00FF00", "00FFFF", "00CCFF", "993366", "C0C0C0", "FF99CC", "FFCC99", "FFFF99",
							"CCFFCC", "CCFFFF", "99CCFF", "CC99FF", "FFFFFF" ],

					onRender : function(container, position) {
						var t = this.tpl
								|| new Tera.XTemplate(
										'<tpl for="."><a href="#" class="color-{.}" hidefocus="on"><em><span style="background:#{.}" unselectable="on">&#160;</span></em></a></tpl>');
						var el = document.createElement("div");
						el.className = this.itemCls;
						t.overwrite(el, this.colors);
						container.dom.insertBefore(el, position);
						this.el = Tera.get(el);
						this.el.on(this.clickEvent, this.handleClick, this, {
							delegate :"a"
						});
						if (this.clickEvent != 'click') {
							this.el.on('click', Tera.emptyFn, this, {
								delegate :"a",
								preventDefault :true
							});
						}
					},

					afterRender : function() {
						Tera.ColorPalette.superclass.afterRender.call(this);
						if (this.value) {
							var s = this.value;
							this.value = null;
							this.select(s);
						}
					},

					handleClick : function(e, t) {
						e.preventDefault();
						if (!this.disabled) {
							var c = t.className.match(/(?:^|\s)color-(.{6})(?:\s|$)/)[1];
							this.select(c.toUpperCase());
						}
					},

					select : function(color) {
						color = color.replace("#", "");
						if (color != this.value || this.allowReselect) {
							var el = this.el;
							if (this.value) {
								el.child("a.color-" + this.value).removeClass("x-color-palette-sel");
							}
							el.child("a.color-" + color).addClass("x-color-palette-sel");
							this.value = color;
							this.fireEvent("select", this, color);
						}
					}

				});
Tera.reg('colorpalette', Tera.ColorPalette);

Tera.DatePicker = Tera
		.extend(
				Tera.Component,
				{

					todayText :"Today",

					okText :"&#160;OK&#160;",

					cancelText :"Cancel",

					todayTip :"{0} (Spacebar)",

					minDate :null,

					maxDate :null,

					minText :"This date is before the minimum date",

					maxText :"This date is after the maximum date",

					format :"m/d/y",

					disabledDays :null,

					disabledDaysText :"",

					disabledDatesRE :null,

					disabledDatesText :"",

					constrainToViewport :true,

					monthNames :Date.monthNames,

					dayNames :Date.dayNames,

					nextText :'Next Month (Control+Right)',

					prevText :'Previous Month (Control+Left)',

					monthYearText :'Choose a month (Control+Up/Down to move years)',

					startDay :0,

					initComponent : function() {
						Tera.DatePicker.superclass.initComponent.call(this);

						this.value = this.value ? this.value.clearTime() : new Date().clearTime();

						this.addEvents(

						'select');

						if (this.handler) {
							this.on("select", this.handler, this.scope || this);
						}

						this.initDisabledDays();
					},

					initDisabledDays : function() {
						if (!this.disabledDatesRE && this.disabledDates) {
							var dd = this.disabledDates;
							var re = "(?:";
							for ( var i = 0; i < dd.length; i++) {
								re += dd[i];
								if (i != dd.length - 1)
									re += "|";
							}
							this.disabledDatesRE = new RegExp(re + ")");
						}
					},

					setValue : function(value) {
						var old = this.value;
						this.value = value.clearTime(true);
						if (this.el) {
							this.update(this.value);
						}
					},

					getValue : function() {
						return this.value;
					},

					focus : function() {
						if (this.el) {
							this.update(this.activeDate);
						}
					},

					onRender : function(container, position) {
						var m = [ '<table cellspacing="0">', '<tr><td class="x-date-left"><a href="#" title="', this.prevText,
								'">&#160;</a></td><td class="x-date-middle" align="center"></td><td class="x-date-right"><a href="#" title="',
								this.nextText, '">&#160;</a></td></tr>',
								'<tr><td colspan="3"><table class="x-date-inner" cellspacing="0"><thead><tr>' ];
						var dn = this.dayNames;
						for ( var i = 0; i < 7; i++) {
							var d = this.startDay + i;
							if (d > 6) {
								d = d - 7;
							}
							m.push("<th><span>", dn[d].substr(0, 1), "</span></th>");
						}
						m[m.length] = "</tr></thead><tbody><tr>";
						for ( var i = 0; i < 42; i++) {
							if (i % 7 == 0 && i != 0) {
								m[m.length] = "</tr><tr>";
							}
							m[m.length] = '<td><a href="#" hidefocus="on" class="x-date-date" tabIndex="1"><em><span></span></em></a></td>';
						}
						m[m.length] = '</tr></tbody></table></td></tr><tr><td colspan="3" class="x-date-bottom" align="center"></td></tr></table><div class="x-date-mp"></div>';

						var el = document.createElement("div");
						el.className = "x-date-picker";
						el.innerHTML = m.join("");

						container.dom.insertBefore(el, position);

						this.el = Tera.get(el);
						this.eventEl = Tera.get(el.firstChild);

						new Tera.util.ClickRepeater(this.el.child("td.x-date-left a"), {
							handler :this.showPrevMonth,
							scope :this,
							preventDefault :true,
							stopDefault :true
						});

						new Tera.util.ClickRepeater(this.el.child("td.x-date-right a"), {
							handler :this.showNextMonth,
							scope :this,
							preventDefault :true,
							stopDefault :true
						});

						this.eventEl.on("mousewheel", this.handleMouseWheel, this);

						this.monthPicker = this.el.down('div.x-date-mp');
						this.monthPicker.enableDisplayMode('block');

						var kn = new Tera.KeyNav(this.eventEl, {
							"left" : function(e) {
								e.ctrlKey ? this.showPrevMonth() : this.update(this.activeDate.add("d", -1));
							},

							"right" : function(e) {
								e.ctrlKey ? this.showNextMonth() : this.update(this.activeDate.add("d", 1));
							},

							"up" : function(e) {
								e.ctrlKey ? this.showNextYear() : this.update(this.activeDate.add("d", -7));
							},

							"down" : function(e) {
								e.ctrlKey ? this.showPrevYear() : this.update(this.activeDate.add("d", 7));
							},

							"pageUp" : function(e) {
								this.showNextMonth();
							},

							"pageDown" : function(e) {
								this.showPrevMonth();
							},

							"enter" : function(e) {
								e.stopPropagation();
								return true;
							},

							scope :this
						});

						this.eventEl.on("click", this.handleDateClick, this, {
							delegate :"a.x-date-date"
						});

						this.eventEl.addKeyListener(Tera.EventObject.SPACE, this.selectToday, this);

						this.el.unselectable();

						this.cells = this.el.select("table.x-date-inner tbody td");
						this.textNodes = this.el.query("table.x-date-inner tbody span");

						this.mbtn = new Tera.Button( {
							text :"&#160;",
							tooltip :this.monthYearText,
							renderTo :this.el.child("td.x-date-middle", true)
						});

						this.mbtn.on('click', this.showMonthPicker, this);
						this.mbtn.el.child(this.mbtn.menuClassTarget).addClass("x-btn-with-menu");

						var today = (new Date()).dateFormat(this.format);
						this.todayBtn = new Tera.Button( {
							renderTo :this.el.child("td.x-date-bottom", true),
							text :String.format(this.todayText, today),
							tooltip :String.format(this.todayTip, today),
							handler :this.selectToday,
							scope :this
						});

						if (Tera.isIE) {
							this.el.repaint();
						}
						this.update(this.value);
					},

					createMonthPicker : function() {
						if (!this.monthPicker.dom.firstChild) {
							var buf = [ '<table border="0" cellspacing="0">' ];
							for ( var i = 0; i < 6; i++) {
								buf
										.push(
												'<tr><td class="x-date-mp-month"><a href="#">',
												this.monthNames[i].substr(0, 3),
												'</a></td>',
												'<td class="x-date-mp-month x-date-mp-sep"><a href="#">',
												this.monthNames[i + 6].substr(0, 3),
												'</a></td>',
												i == 0 ? '<td class="x-date-mp-ybtn" align="center"><a class="x-date-mp-prev"></a></td><td class="x-date-mp-ybtn" align="center"><a class="x-date-mp-next"></a></td></tr>'
														: '<td class="x-date-mp-year"><a href="#"></a></td><td class="x-date-mp-year"><a href="#"></a></td></tr>');
							}
							buf.push('<tr class="x-date-mp-btns"><td colspan="4"><button type="button" class="x-date-mp-ok">', this.okText,
									'</button><button type="button" class="x-date-mp-cancel">', this.cancelText, '</button></td></tr>', '</table>');
							this.monthPicker.update(buf.join(''));
							this.monthPicker.on('click', this.onMonthClick, this);
							this.monthPicker.on('dblclick', this.onMonthDblClick, this);

							this.mpMonths = this.monthPicker.select('td.x-date-mp-month');
							this.mpYears = this.monthPicker.select('td.x-date-mp-year');

							this.mpMonths.each( function(m, a, i) {
								i += 1;
								if ((i % 2) == 0) {
									m.dom.xmonth = 5 + Math.round(i * .5);
								} else {
									m.dom.xmonth = Math.round((i - 1) * .5);
								}
							});
						}
					},

					showMonthPicker : function() {
						this.createMonthPicker();
						var size = this.el.getSize();
						this.monthPicker.setSize(size);
						this.monthPicker.child('table').setSize(size);

						this.mpSelMonth = (this.activeDate || this.value).getMonth();
						this.updateMPMonth(this.mpSelMonth);
						this.mpSelYear = (this.activeDate || this.value).getFullYear();
						this.updateMPYear(this.mpSelYear);

						this.monthPicker.slideIn('t', {
							duration :.2
						});
					},

					updateMPYear : function(y) {
						this.mpyear = y;
						var ys = this.mpYears.elements;
						for ( var i = 1; i <= 10; i++) {
							var td = ys[i - 1], y2;
							if ((i % 2) == 0) {
								y2 = y + Math.round(i * .5);
								td.firstChild.innerHTML = y2;
								td.xyear = y2;
							} else {
								y2 = y - (5 - Math.round(i * .5));
								td.firstChild.innerHTML = y2;
								td.xyear = y2;
							}
							this.mpYears.item(i - 1)[y2 == this.mpSelYear ? 'addClass' : 'removeClass']('x-date-mp-sel');
						}
					},

					updateMPMonth : function(sm) {
						this.mpMonths.each( function(m, a, i) {
							m[m.dom.xmonth == sm ? 'addClass' : 'removeClass']('x-date-mp-sel');
						});
					},

					selectMPMonth : function(m) {

					},

					onMonthClick : function(e, t) {
						e.stopEvent();
						var el = new Tera.Element(t), pn;
						if (el.is('button.x-date-mp-cancel')) {
							this.hideMonthPicker();
						} else if (el.is('button.x-date-mp-ok')) {
							var d = new Date(this.mpSelYear, this.mpSelMonth, (this.activeDate || this.value).getDate());
							if (d.getMonth() != this.mpSelMonth) {

								d = new Date(this.mpSelYear, this.mpSelMonth, 1).getLastDateOfMonth();
							}
							this.update(d);
							this.hideMonthPicker();
						} else if (pn = el.up('td.x-date-mp-month', 2)) {
							this.mpMonths.removeClass('x-date-mp-sel');
							pn.addClass('x-date-mp-sel');
							this.mpSelMonth = pn.dom.xmonth;
						} else if (pn = el.up('td.x-date-mp-year', 2)) {
							this.mpYears.removeClass('x-date-mp-sel');
							pn.addClass('x-date-mp-sel');
							this.mpSelYear = pn.dom.xyear;
						} else if (el.is('a.x-date-mp-prev')) {
							this.updateMPYear(this.mpyear - 10);
						} else if (el.is('a.x-date-mp-next')) {
							this.updateMPYear(this.mpyear + 10);
						}
					},

					onMonthDblClick : function(e, t) {
						e.stopEvent();
						var el = new Tera.Element(t), pn;
						if (pn = el.up('td.x-date-mp-month', 2)) {
							this.update(new Date(this.mpSelYear, pn.dom.xmonth, (this.activeDate || this.value).getDate()));
							this.hideMonthPicker();
						} else if (pn = el.up('td.x-date-mp-year', 2)) {
							this.update(new Date(pn.dom.xyear, this.mpSelMonth, (this.activeDate || this.value).getDate()));
							this.hideMonthPicker();
						}
					},

					hideMonthPicker : function(disableAnim) {
						if (this.monthPicker) {
							if (disableAnim === true) {
								this.monthPicker.hide();
							} else {
								this.monthPicker.slideOut('t', {
									duration :.2
								});
							}
						}
					},

					showPrevMonth : function(e) {
						this.update(this.activeDate.add("mo", -1));
					},

					showNextMonth : function(e) {
						this.update(this.activeDate.add("mo", 1));
					},

					showPrevYear : function() {
						this.update(this.activeDate.add("y", -1));
					},

					showNextYear : function() {
						this.update(this.activeDate.add("y", 1));
					},

					handleMouseWheel : function(e) {
						var delta = e.getWheelDelta();
						if (delta > 0) {
							this.showPrevMonth();
							e.stopEvent();
						} else if (delta < 0) {
							this.showNextMonth();
							e.stopEvent();
						}
					},

					handleDateClick : function(e, t) {
						e.stopEvent();
						if (t.dateValue && !Tera.fly(t.parentNode).hasClass("x-date-disabled")) {
							this.setValue(new Date(t.dateValue));
							this.fireEvent("select", this, this.value);
						}
					},

					selectToday : function() {
						this.setValue(new Date().clearTime());
						this.fireEvent("select", this, this.value);
					},

					update : function(date) {
						var vd = this.activeDate;
						this.activeDate = date;
						if (vd && this.el) {
							var t = date.getTime();
							if (vd.getMonth() == date.getMonth() && vd.getFullYear() == date.getFullYear()) {
								this.cells.removeClass("x-date-selected");
								this.cells.each( function(c) {
									if (c.dom.firstChild.dateValue == t) {
										c.addClass("x-date-selected");
										setTimeout( function() {
											try {
												c.dom.firstChild.focus();
											} catch (e) {
											}
										}, 50);
										return false;
									}
								});
								return;
							}
						}
						var days = date.getDaysInMonth();
						var firstOfMonth = date.getFirstDateOfMonth();
						var startingPos = firstOfMonth.getDay() - this.startDay;

						if (startingPos <= this.startDay) {
							startingPos += 7;
						}

						var pm = date.add("mo", -1);
						var prevStart = pm.getDaysInMonth() - startingPos;

						var cells = this.cells.elements;
						var textEls = this.textNodes;
						days += startingPos;

						var day = 86400000;
						var d = (new Date(pm.getFullYear(), pm.getMonth(), prevStart)).clearTime();
						var today = new Date().clearTime().getTime();
						var sel = date.clearTime().getTime();
						var min = this.minDate ? this.minDate.clearTime() : Number.NEGATIVE_INFINITY;
						var max = this.maxDate ? this.maxDate.clearTime() : Number.POSITIVE_INFINITY;
						var ddMatch = this.disabledDatesRE;
						var ddText = this.disabledDatesText;
						var ddays = this.disabledDays ? this.disabledDays.join("") : false;
						var ddaysText = this.disabledDaysText;
						var format = this.format;

						var setCellClass = function(cal, cell) {
							cell.title = "";
							var t = d.getTime();
							cell.firstChild.dateValue = t;
							if (t == today) {
								cell.className += " x-date-today";
								cell.title = cal.todayText;
							}
							if (t == sel) {
								cell.className += " x-date-selected";
								setTimeout( function() {
									try {
										cell.firstChild.focus();
									} catch (e) {
									}
								}, 50);
							}

							if (t < min) {
								cell.className = " x-date-disabled";
								cell.title = cal.minText;
								return;
							}
							if (t > max) {
								cell.className = " x-date-disabled";
								cell.title = cal.maxText;
								return;
							}
							if (ddays) {
								if (ddays.indexOf(d.getDay()) != -1) {
									cell.title = ddaysText;
									cell.className = " x-date-disabled";
								}
							}
							if (ddMatch && format) {
								var fvalue = d.dateFormat(format);
								if (ddMatch.test(fvalue)) {
									cell.title = ddText.replace("%0", fvalue);
									cell.className = " x-date-disabled";
								}
							}
						};

						var i = 0;
						for (; i < startingPos; i++) {
							textEls[i].innerHTML = (++prevStart);
							d.setDate(d.getDate() + 1);
							cells[i].className = "x-date-prevday";
							setCellClass(this, cells[i]);
						}
						for (; i < days; i++) {
							intDay = i - startingPos + 1;
							textEls[i].innerHTML = (intDay);
							d.setDate(d.getDate() + 1);
							cells[i].className = "x-date-active";
							setCellClass(this, cells[i]);
						}
						var extraDays = 0;
						for (; i < 42; i++) {
							textEls[i].innerHTML = (++extraDays);
							d.setDate(d.getDate() + 1);
							cells[i].className = "x-date-nextday";
							setCellClass(this, cells[i]);
						}

						this.mbtn.setText(this.monthNames[date.getMonth()] + " " + date.getFullYear());

						if (!this.internalRender) {
							var main = this.el.dom.firstChild;
							var w = main.offsetWidth;
							this.el.setWidth(w + this.el.getBorderWidth("lr"));
							Tera.fly(main).setWidth(w);
							this.internalRender = true;

							if (Tera.isOpera && !this.secondPass) {
								main.rows[0].cells[1].style.width = (w - (main.rows[0].cells[0].offsetWidth + main.rows[0].cells[2].offsetWidth))
										+ "px";
								this.secondPass = true;
								this.update.defer(10, this, [ date ]);
							}
						}
					},

					beforeDestroy : function() {
						if (this.rendered) {
							this.mbtn.destroy();
							this.todayBtn.destroy();
						}
					}

				});
Tera.reg('datepicker', Tera.DatePicker);

Tera.TabPanel = Tera.extend(Tera.Panel, {

	monitorResize :true,

	deferredRender :true,

	tabWidth :120,

	minTabWidth :30,

	resizeTabs :false,

	enableTabScroll :false,

	scrollIncrement :0,

	scrollRepeatInterval :400,

	scrollDuration :.35,

	animScroll :true,

	tabPosition :'top',

	baseCls :'x-tab-panel',

	autoTabs :false,

	autoTabSelector :'div.x-tab',

	activeTab :null,

	tabMargin :2,

	plain :false,

	wheelIncrement :20,

	idDelimiter :'__',

	itemCls :'x-tab-item',

	elements :'body',
	headerAsText :false,
	frame :false,
	hideBorders :true,

	initComponent : function() {
		this.frame = false;
		Tera.TabPanel.superclass.initComponent.call(this);
		this.addEvents(

		'beforetabchange',

		'tabchange',

		'contextmenu');
		this.setLayout(new Tera.layout.CardLayout( {
			deferredRender :this.deferredRender
		}));
		if (this.tabPosition == 'top') {
			this.elements += ',header';
			this.stripTarget = 'header';
		} else {
			this.elements += ',footer';
			this.stripTarget = 'footer';
		}
		if (!this.stack) {
			this.stack = Tera.TabPanel.AccessStack();
		}
		this.initItems();
	},

	render : function() {
		Tera.TabPanel.superclass.render.apply(this, arguments);
		if (this.activeTab !== undefined) {
			var item = this.activeTab;
			delete this.activeTab;
			this.setActiveTab(item);
		}
	},

	onRender : function(ct, position) {
		Tera.TabPanel.superclass.onRender.call(this, ct, position);

		if (this.plain) {
			var pos = this.tabPosition == 'top' ? 'header' : 'footer';
			this[pos].addClass('x-tab-panel-' + pos + '-plain');
		}

		var st = this[this.stripTarget];

		this.stripWrap = st.createChild( {
			cls :'x-tab-strip-wrap',
			cn : {
				tag :'ul',
				cls :'x-tab-strip x-tab-strip-' + this.tabPosition
			}
		});
		this.stripSpacer = st.createChild( {
			cls :'x-tab-strip-spacer'
		});
		this.strip = new Tera.Element(this.stripWrap.dom.firstChild);

		this.edge = this.strip.createChild( {
			tag :'li',
			cls :'x-tab-edge'
		});
		this.strip.createChild( {
			cls :'x-clear'
		});

		this.body.addClass('x-tab-panel-body-' + this.tabPosition);

		if (!this.itemTpl) {
			var tt = new Tera.Template('<li class="{cls}" id="{id}"><a class="x-tab-strip-close" onclick="return false;"></a>',
					'<a class="x-tab-right" href="#" onclick="return false;"><em class="x-tab-left">',
					'<span class="x-tab-strip-inner"><span class="x-tab-strip-text {iconCls}">{text}</span></span>', '</em></a></li>');
			tt.disableFormats = true;
			tt.compile();
			Tera.TabPanel.prototype.itemTpl = tt;
		}

		this.items.each(this.initTab, this);
	},

	afterRender : function() {
		Tera.TabPanel.superclass.afterRender.call(this);
		if (this.autoTabs) {
			this.readTabs(false);
		}
	},

	initEvents : function() {
		Tera.TabPanel.superclass.initEvents.call(this);
		this.on('add', this.onAdd, this);
		this.on('remove', this.onRemove, this);

		this.strip.on('mousedown', this.onStripMouseDown, this);
		this.strip.on('click', this.onStripClick, this);
		this.strip.on('contextmenu', this.onStripContextMenu, this);
		if (this.enableTabScroll) {
			this.strip.on('mousewheel', this.onWheel, this);
		}
	},

	findTargets : function(e) {
		var item = null;
		var itemEl = e.getTarget('li', this.strip);
		if (itemEl) {
			item = this.getComponent(itemEl.id.split(this.idDelimiter)[1]);
			if (item.disabled) {
				return {
					close :null,
					item :null,
					el :null
				};
			}
		}
		return {
			close :e.getTarget('.x-tab-strip-close', this.strip),
			item :item,
			el :itemEl
		};
	},

	onStripMouseDown : function(e) {
		e.preventDefault();
		if (e.button != 0) {
			return;
		}
		var t = this.findTargets(e);
		if (t.close) {
			this.remove(t.item);
			return;
		}
		if (t.item && t.item != this.activeTab) {
			this.setActiveTab(t.item);
		}
	},

	onStripClick : function(e) {
		var t = this.findTargets(e);
		if (!t.close && t.item && t.item != this.activeTab) {
			this.setActiveTab(t.item);
		}
	},

	onStripContextMenu : function(e) {
		e.preventDefault();
		var t = this.findTargets(e);
		if (t.item) {
			this.fireEvent('contextmenu', this, t.item, e);
		}
	},

	readTabs : function(removeExisting) {
		if (removeExisting === true) {
			this.items.each( function(item) {
				this.remove(item);
			}, this);
		}
		var tabs = this.el.query(this.autoTabSelector);
		for ( var i = 0, len = tabs.length; i < len; i++) {
			var tab = tabs[i];
			var title = tab.getAttribute('title');
			tab.removeAttribute('title');
			this.add( {
				title :title,
				el :tab
			});
		}
	},

	initTab : function(item, index) {
		var before = this.strip.dom.childNodes[index];
		var cls = item.closable ? 'x-tab-strip-closable' : '';
		if (item.disabled) {
			cls += ' x-item-disabled';
		}
		if (item.iconCls) {
			cls += ' x-tab-with-icon';
		}
		if (item.tabCls) {
			cls += ' ' + item.tabCls;
		}

		var p = {
			id :this.id + this.idDelimiter + item.getItemId(),
			text :item.title,
			cls :cls,
			iconCls :item.iconCls || ''
		};
		var el = before ? this.itemTpl.insertBefore(before, p) : this.itemTpl.append(this.strip, p);

		Tera.fly(el).addClassOnOver('x-tab-strip-over');

		if (item.tabTip) {
			Tera.fly(el).child('span.x-tab-strip-text', true).qtip = item.tabTip;
		}
		item.on('disable', this.onItemDisabled, this);
		item.on('enable', this.onItemEnabled, this);
		item.on('titlechange', this.onItemTitleChanged, this);
		item.on('beforeshow', this.onBeforeShowItem, this);
	},

	onAdd : function(tp, item, index) {
		this.initTab(item, index);
		if (this.items.getCount() == 1) {
			this.syncSize();
		}
		this.delegateUpdates();
	},

	onBeforeAdd : function(item) {
		var existing = item.events ? (this.items.containsKey(item.getItemId()) ? item : null) : this.items.get(item);
		if (existing) {
			this.setActiveTab(item);
			return false;
		}
		Tera.TabPanel.superclass.onBeforeAdd.apply(this, arguments);
		var es = item.elements;
		item.elements = es ? es.replace(',header', '') : es;
		item.border = (item.border === true);
	},

	onRemove : function(tp, item) {
		Tera.removeNode(this.getTabEl(item));
		this.stack.remove(item);
		item.un('disable', this.onItemDisabled, this);
		item.un('enable', this.onItemEnabled, this);
		item.un('titlechange', this.onItemTitleChanged, this);
		item.un('beforeshow', this.onBeforeShowItem, this);
		if (item == this.activeTab) {
			var next = this.stack.next();
			if (next) {
				this.setActiveTab(next);
			} else {
				this.setActiveTab(0);
			}
		}
		this.delegateUpdates();
	},

	onBeforeShowItem : function(item) {
		if (item != this.activeTab) {
			this.setActiveTab(item);
			return false;
		}
	},

	onItemDisabled : function(item) {
		var el = this.getTabEl(item);
		if (el) {
			Tera.fly(el).addClass('x-item-disabled');
		}
		this.stack.remove(item);
	},

	onItemEnabled : function(item) {
		var el = this.getTabEl(item);
		if (el) {
			Tera.fly(el).removeClass('x-item-disabled');
		}
	},

	onItemTitleChanged : function(item) {
		var el = this.getTabEl(item);
		if (el) {
			Tera.fly(el).child('span.x-tab-strip-text', true).innerHTML = item.title;
		}
	},

	getTabEl : function(item) {
		var itemId = (typeof item === 'number') ? this.items.items[item].getItemId() : item.getItemId();
		return document.getElementById(this.id + this.idDelimiter + itemId);
	},

	onResize : function() {
		Tera.TabPanel.superclass.onResize.apply(this, arguments);
		this.delegateUpdates();
	},

	beginUpdate : function() {
		this.suspendUpdates = true;
	},

	endUpdate : function() {
		this.suspendUpdates = false;
		this.delegateUpdates();
	},

	hideTabStripItem : function(item) {
		item = this.getComponent(item);
		var el = this.getTabEl(item);
		if (el) {
			el.style.display = 'none';
			this.delegateUpdates();
		}
		this.stack.remove(item);
	},

	unhideTabStripItem : function(item) {
		item = this.getComponent(item);
		var el = this.getTabEl(item);
		if (el) {
			el.style.display = '';
			this.delegateUpdates();
		}
	},

	delegateUpdates : function() {
		if (this.suspendUpdates) {
			return;
		}
		if (this.resizeTabs && this.rendered) {
			this.autoSizeTabs();
		}
		if (this.enableTabScroll && this.rendered) {
			this.autoScrollTabs();
		}
	},

	autoSizeTabs : function() {
		var count = this.items.length;
		var ce = this.tabPosition != 'bottom' ? 'header' : 'footer';
		var ow = this[ce].dom.offsetWidth;
		var aw = this[ce].dom.clientWidth;

		if (!this.resizeTabs || count < 1 || !aw) {
			return;
		}

		var each = Math.max(Math.min(Math.floor((aw - 4) / count) - this.tabMargin, this.tabWidth), this.minTabWidth);
		this.lastTabWidth = each;
		var lis = this.stripWrap.dom.getElementsByTagName('li');
		for ( var i = 0, len = lis.length - 1; i < len; i++) {
			var li = lis[i];
			var inner = li.childNodes[1].firstChild.firstChild;
			var tw = li.offsetWidth;
			var iw = inner.offsetWidth;
			inner.style.width = (each - (tw - iw)) + 'px';
		}
	},

	adjustBodyWidth : function(w) {
		if (this.header) {
			this.header.setWidth(w);
		}
		if (this.footer) {
			this.footer.setWidth(w);
		}
		return w;
	},

	setActiveTab : function(item) {
		item = this.getComponent(item);
		if (!item || this.fireEvent('beforetabchange', this, item, this.activeTab) === false) {
			return;
		}
		if (!this.rendered) {
			this.activeTab = item;
			return;
		}
		if (this.activeTab != item) {
			if (this.activeTab) {
				var oldEl = this.getTabEl(this.activeTab);
				if (oldEl) {
					Tera.fly(oldEl).removeClass('x-tab-strip-active');
				}
				this.activeTab.fireEvent('deactivate', this.activeTab);
			}
			var el = this.getTabEl(item);
			Tera.fly(el).addClass('x-tab-strip-active');
			this.activeTab = item;
			this.stack.add(item);

			this.layout.setActiveItem(item);
			if (this.layoutOnTabChange && item.doLayout) {
				item.doLayout();
			}
			if (this.scrolling) {
				this.scrollToTab(item, this.animScroll);
			}

			item.fireEvent('activate', item);
			this.fireEvent('tabchange', this, item);
		}
	},

	getActiveTab : function() {
		return this.activeTab || null;
	},

	getItem : function(item) {
		return this.getComponent(item);
	},

	autoScrollTabs : function() {
		var count = this.items.length;
		var ow = this.header.dom.offsetWidth;
		var tw = this.header.dom.clientWidth;

		var wrap = this.stripWrap;
		var wd = wrap.dom;
		var cw = wd.offsetWidth;
		var pos = this.getScrollPos();
		var l = this.edge.getOffsetsTo(this.stripWrap)[0] + pos;

		if (!this.enableTabScroll || count < 1 || cw < 20) {
			return;
		}
		if (l <= tw) {
			wd.scrollLeft = 0;
			wrap.setWidth(tw);
			if (this.scrolling) {
				this.scrolling = false;
				this.header.removeClass('x-tab-scrolling');
				this.scrollLeft.hide();
				this.scrollRight.hide();
				if (Tera.isAir) {
					wd.style.marginLeft = '';
					wd.style.marginRight = '';
				}
			}
		} else {
			if (!this.scrolling) {
				this.header.addClass('x-tab-scrolling');
				if (Tera.isAir) {
					wd.style.marginLeft = '18px';
					wd.style.marginRight = '18px';
				}
			}
			tw -= wrap.getMargins('lr');
			wrap.setWidth(tw > 20 ? tw : 20);
			if (!this.scrolling) {
				if (!this.scrollLeft) {
					this.createScrollers();
				} else {
					this.scrollLeft.show();
					this.scrollRight.show();
				}
			}
			this.scrolling = true;
			if (pos > (l - tw)) {
				wd.scrollLeft = l - tw;
			} else {
				this.scrollToTab(this.activeTab, false);
			}
			this.updateScrollButtons();
		}
	},

	createScrollers : function() {
		var h = this.stripWrap.dom.offsetHeight;

		var sl = this.header.insertFirst( {
			cls :'x-tab-scroller-left'
		});
		sl.setHeight(h);
		sl.addClassOnOver('x-tab-scroller-left-over');
		this.leftRepeater = new Tera.util.ClickRepeater(sl, {
			interval :this.scrollRepeatInterval,
			handler :this.onScrollLeft,
			scope :this
		});
		this.scrollLeft = sl;

		var sr = this.header.insertFirst( {
			cls :'x-tab-scroller-right'
		});
		sr.setHeight(h);
		sr.addClassOnOver('x-tab-scroller-right-over');
		this.rightRepeater = new Tera.util.ClickRepeater(sr, {
			interval :this.scrollRepeatInterval,
			handler :this.onScrollRight,
			scope :this
		});
		this.scrollRight = sr;
	},

	getScrollWidth : function() {
		return this.edge.getOffsetsTo(this.stripWrap)[0] + this.getScrollPos();
	},

	getScrollPos : function() {
		return parseInt(this.stripWrap.dom.scrollLeft, 10) || 0;
	},

	getScrollArea : function() {
		return parseInt(this.stripWrap.dom.clientWidth, 10) || 0;
	},

	getScrollAnim : function() {
		return {
			duration :this.scrollDuration,
			callback :this.updateScrollButtons,
			scope :this
		};
	},

	getScrollIncrement : function() {
		return this.scrollIncrement || (this.resizeTabs ? this.lastTabWidth + 2 : 100);
	},

	scrollToTab : function(item, animate) {
		if (!item) {
			return;
		}
		var el = this.getTabEl(item);
		var pos = this.getScrollPos(), area = this.getScrollArea();
		var left = Tera.fly(el).getOffsetsTo(this.stripWrap)[0] + pos;
		var right = left + el.offsetWidth;
		if (left < pos) {
			this.scrollTo(left, animate);
		} else if (right > (pos + area)) {
			this.scrollTo(right - area, animate);
		}
	},

	scrollTo : function(pos, animate) {
		this.stripWrap.scrollTo('left', pos, animate ? this.getScrollAnim() : false);
		if (!animate) {
			this.updateScrollButtons();
		}
	},

	onWheel : function(e) {
		var d = e.getWheelDelta() * this.wheelIncrement * -1;
		e.stopEvent();

		var pos = this.getScrollPos();
		var newpos = pos + d;
		var sw = this.getScrollWidth() - this.getScrollArea();

		var s = Math.max(0, Math.min(sw, newpos));
		if (s != pos) {
			this.scrollTo(s, false);
		}
	},

	onScrollRight : function() {
		var sw = this.getScrollWidth() - this.getScrollArea();
		var pos = this.getScrollPos();
		var s = Math.min(sw, pos + this.getScrollIncrement());
		if (s != pos) {
			this.scrollTo(s, this.animScroll);
		}
	},

	onScrollLeft : function() {
		var pos = this.getScrollPos();
		var s = Math.max(0, pos - this.getScrollIncrement());
		if (s != pos) {
			this.scrollTo(s, this.animScroll);
		}
	},

	updateScrollButtons : function() {
		var pos = this.getScrollPos();
		this.scrollLeft[pos == 0 ? 'addClass' : 'removeClass']('x-tab-scroller-left-disabled');
		this.scrollRight[pos >= (this.getScrollWidth() - this.getScrollArea()) ? 'addClass' : 'removeClass']('x-tab-scroller-right-disabled');
	}

});
Tera.reg('tabpanel', Tera.TabPanel);

Tera.TabPanel.prototype.activate = Tera.TabPanel.prototype.setActiveTab;

Tera.TabPanel.AccessStack = function() {
	var items = [];
	return {
		add : function(item) {
			items.push(item);
			if (items.length > 10) {
				items.shift();
			}
		},

		remove : function(item) {
			var s = [];
			for ( var i = 0, len = items.length; i < len; i++) {
				if (items[i] != item) {
					s.push(items[i]);
				}
			}
			items = s;
		},

		next : function() {
			return items.pop();
		}
	};
};

Tera.Button = Tera
		.extend(
				Tera.Component,
				{

					hidden :false,

					disabled :false,

					pressed :false,

					enableToggle :false,

					menuAlign :"tl-bl?",

					type :'button',

					menuClassTarget :'tr',

					clickEvent :'click',

					handleMouseEvents :true,

					tooltipType :'qtip',

					buttonSelector :"button:first",

					initComponent : function() {
						Tera.Button.superclass.initComponent.call(this);

						this.addEvents(

						"click",

						"toggle",

						'mouseover',

						'mouseout',

						'menushow',

						'menuhide',

						'menutriggerover',

						'menutriggerout');
						if (this.menu) {
							this.menu = Tera.menu.MenuMgr.get(this.menu);
						}
						if (typeof this.toggleGroup === 'string') {
							this.enableToggle = true;
						}
					},

					onRender : function(ct, position) {
						if (!this.template) {
							if (!Tera.Button.buttonTemplate) {
								Tera.Button.buttonTemplate = new Tera.Template(
										'<table border="0" cellpadding="0" cellspacing="0" class="x-btn-wrap"><tbody><tr>',
										'<td class="x-btn-left"><i>&#160;</i></td><td class="x-btn-center"><em unselectable="on"><button class="x-btn-text" type="{1}">{0}</button></em></td><td class="x-btn-right"><i>&#160;</i></td>',
										"</tr></tbody></table>");
							}
							this.template = Tera.Button.buttonTemplate;
						}
						var btn, targs = [ this.text || '&#160;', this.type ];

						if (position) {
							btn = this.template.insertBefore(position, targs, true);
						} else {
							btn = this.template.append(ct, targs, true);
						}
						var btnEl = btn.child(this.buttonSelector);
						btnEl.on('focus', this.onFocus, this);
						btnEl.on('blur', this.onBlur, this);

						this.initButtonEl(btn, btnEl);

						if (this.menu) {
							this.el.child(this.menuClassTarget).addClass("x-btn-with-menu");
						}
						Tera.ButtonToggleMgr.register(this);
					},

					initButtonEl : function(btn, btnEl) {

						this.el = btn;
						btn.addClass("x-btn");

						if (this.icon) {
							btnEl.setStyle('background-image', 'url(' + this.icon + ')');
						}
						if (this.iconCls) {
							btnEl.addClass(this.iconCls);
							if (!this.cls) {
								btn.addClass(this.text ? 'x-btn-text-icon' : 'x-btn-icon');
							}
						}
						if (this.tabIndex !== undefined) {
							btnEl.dom.tabIndex = this.tabIndex;
						}
						if (this.tooltip) {
							if (typeof this.tooltip == 'object') {
								Tera.QuickTips.register(Tera.apply( {
									target :btnEl.id
								}, this.tooltip));
							} else {
								btnEl.dom[this.tooltipType] = this.tooltip;
							}
						}

						if (this.pressed) {
							this.el.addClass("x-btn-pressed");
						}

						if (this.handleMouseEvents) {
							btn.on("mouseover", this.onMouseOver, this);
							btn.on("mousedown", this.onMouseDown, this);
						}

						if (this.menu) {
							this.menu.on("show", this.onMenuShow, this);
							this.menu.on("hide", this.onMenuHide, this);
						}

						if (this.id) {
							this.el.dom.id = this.el.id = this.id;
						}

						if (this.repeat) {
							var repeater = new Tera.util.ClickRepeater(btn, typeof this.repeat == "object" ? this.repeat : {});
							repeater.on("click", this.onClick, this);
						}

						btn.on(this.clickEvent, this.onClick, this);
					},

					afterRender : function() {
						Tera.Button.superclass.afterRender.call(this);
						if (Tera.isIE6) {
							this.autoWidth.defer(1, this);
						} else {
							this.autoWidth();
						}
					},

					setIconClass : function(cls) {
						if (this.el) {
							this.el.child(this.buttonSelector).replaceClass(this.iconCls, cls);
						}
						this.iconCls = cls;
					},

					beforeDestroy : function() {
						if (this.rendered) {
							var btn = this.el.child(this.buttonSelector);
							if (btn) {
								btn.removeAllListeners();
							}
						}
						if (this.menu) {
							Tera.destroy(this.menu);
						}
					},

					onDestroy : function() {
						if (this.rendered) {
							Tera.ButtonToggleMgr.unregister(this);
						}
					},

					autoWidth : function() {
						if (this.el) {
							this.el.setWidth("auto");
							if (Tera.isIE7 && Tera.isStrict) {
								var ib = this.el.child(this.buttonSelector);
								if (ib && ib.getWidth() > 20) {
									ib.clip();
									ib.setWidth(Tera.util.TextMetrics.measure(ib, this.text).width + ib.getFrameWidth('lr'));
								}
							}
							if (this.minWidth) {
								if (this.el.getWidth() < this.minWidth) {
									this.el.setWidth(this.minWidth);
								}
							}
						}
					},

					setHandler : function(handler, scope) {
						this.handler = handler;
						this.scope = scope;
					},

					setText : function(text) {
						this.text = text;
						if (this.el) {
							this.el.child("td.x-btn-center " + this.buttonSelector).update(text);
						}
						this.autoWidth();
					},

					getText : function() {
						return this.text;
					},

					toggle : function(state) {
						state = state === undefined ? !this.pressed : state;
						if (state != this.pressed) {
							if (state) {
								this.el.addClass("x-btn-pressed");
								this.pressed = true;
								this.fireEvent("toggle", this, true);
							} else {
								this.el.removeClass("x-btn-pressed");
								this.pressed = false;
								this.fireEvent("toggle", this, false);
							}
							if (this.toggleHandler) {
								this.toggleHandler.call(this.scope || this, this, state);
							}
						}
					},

					focus : function() {
						this.el.child(this.buttonSelector).focus();
					},

					onDisable : function() {
						if (this.el) {
							if (!Tera.isIE6 || !this.text) {
								this.el.addClass(this.disabledClass);
							}
							this.el.dom.disabled = true;
						}
						this.disabled = true;
					},

					onEnable : function() {
						if (this.el) {
							if (!Tera.isIE6 || !this.text) {
								this.el.removeClass(this.disabledClass);
							}
							this.el.dom.disabled = false;
						}
						this.disabled = false;
					},

					showMenu : function() {
						if (this.menu) {
							this.menu.show(this.el, this.menuAlign);
						}
						return this;
					},

					hideMenu : function() {
						if (this.menu) {
							this.menu.hide();
						}
						return this;
					},

					hasVisibleMenu : function() {
						return this.menu && this.menu.isVisible();
					},

					onClick : function(e) {
						if (e) {
							e.preventDefault();
						}
						if (e.button != 0) {
							return;
						}
						if (!this.disabled) {
							if (this.enableToggle && (this.allowDepress !== false || !this.pressed)) {
								this.toggle();
							}
							if (this.menu && !this.menu.isVisible() && !this.ignoreNextClick) {
								this.showMenu();
							}
							this.fireEvent("click", this, e);
							if (this.handler) {
								this.handler.call(this.scope || this, this, e);
							}
						}
					},

					isMenuTriggerOver : function(e, internal) {
						return this.menu && !internal;
					},

					isMenuTriggerOut : function(e, internal) {
						return this.menu && !internal;
					},

					onMouseOver : function(e) {
						if (!this.disabled) {
							var internal = e.within(this.el, true);
							if (!internal) {
								this.el.addClass("x-btn-over");
								Tera.getDoc().on('mouseover', this.monitorMouseOver, this);
								this.fireEvent('mouseover', this, e);
							}
							if (this.isMenuTriggerOver(e, internal)) {
								this.fireEvent('menutriggerover', this, this.menu, e);
							}
						}
					},

					monitorMouseOver : function(e) {
						if (e.target != this.el.dom && !e.within(this.el)) {
							Tera.getDoc().un('mouseover', this.monitorMouseOver, this);
							this.onMouseOut(e);
						}
					},

					onMouseOut : function(e) {
						var internal = e.within(this.el) && e.target != this.el.dom;
						this.el.removeClass("x-btn-over");
						this.fireEvent('mouseout', this, e);
						if (this.isMenuTriggerOut(e, internal)) {
							this.fireEvent('menutriggerout', this, this.menu, e);
						}
					},
					onFocus : function(e) {
						if (!this.disabled) {
							this.el.addClass("x-btn-focus");
						}
					},
					onBlur : function(e) {
						this.el.removeClass("x-btn-focus");
					},

					getClickEl : function(e, isUp) {
						return this.el;
					},

					onMouseDown : function(e) {
						if (!this.disabled && e.button == 0) {
							this.getClickEl(e).addClass("x-btn-click");
							Tera.getDoc().on('mouseup', this.onMouseUp, this);
						}
					},
					onMouseUp : function(e) {
						if (e.button == 0) {
							this.getClickEl(e, true).removeClass("x-btn-click");
							Tera.getDoc().un('mouseup', this.onMouseUp, this);
						}
					},
					onMenuShow : function(e) {
						this.ignoreNextClick = 0;
						this.el.addClass("x-btn-menu-active");
						this.fireEvent('menushow', this, this.menu);
					},
					onMenuHide : function(e) {
						this.el.removeClass("x-btn-menu-active");
						this.ignoreNextClick = this.restoreClick.defer(250, this);
						this.fireEvent('menuhide', this, this.menu);
					},

					restoreClick : function() {
						this.ignoreNextClick = 0;
					}

				});
Tera.reg('button', Tera.Button);

Tera.ButtonToggleMgr = function() {
	var groups = {};

	function toggleGroup(btn, state) {
		if (state) {
			var g = groups[btn.toggleGroup];
			for ( var i = 0, l = g.length; i < l; i++) {
				if (g[i] != btn) {
					g[i].toggle(false);
				}
			}
		}
	}

	return {
		register : function(btn) {
			if (!btn.toggleGroup) {
				return;
			}
			var g = groups[btn.toggleGroup];
			if (!g) {
				g = groups[btn.toggleGroup] = [];
			}
			g.push(btn);
			btn.on("toggle", toggleGroup);
		},

		unregister : function(btn) {
			if (!btn.toggleGroup) {
				return;
			}
			var g = groups[btn.toggleGroup];
			if (g) {
				g.remove(btn);
				btn.un("toggle", toggleGroup);
			}
		}
	};
}();

Tera.SplitButton = Tera
		.extend(
				Tera.Button,
				{

					arrowSelector :'button:last',

					initComponent : function() {
						Tera.SplitButton.superclass.initComponent.call(this);

						this.addEvents("arrowclick");
					},

					onRender : function(ct, position) {

						var tpl = new Tera.Template(
								'<table cellspacing="0" class="x-btn-menu-wrap x-btn"><tr><td>',
								'<table cellspacing="0" class="x-btn-wrap x-btn-menu-text-wrap"><tbody>',
								'<tr><td class="x-btn-left"><i>&#160;</i></td><td class="x-btn-center"><button class="x-btn-text" type="{1}">{0}</button></td></tr>',
								"</tbody></table></td><td>",
								'<table cellspacing="0" class="x-btn-wrap x-btn-menu-arrow-wrap"><tbody>',
								'<tr><td class="x-btn-center"><button class="x-btn-menu-arrow-el" type="button">&#160;</button></td><td class="x-btn-right"><i>&#160;</i></td></tr>',
								"</tbody></table></td></tr></table>");
						var btn, targs = [ this.text || '&#160;', this.type ];
						if (position) {
							btn = tpl.insertBefore(position, targs, true);
						} else {
							btn = tpl.append(ct, targs, true);
						}
						var btnEl = btn.child(this.buttonSelector);

						this.initButtonEl(btn, btnEl);
						this.arrowBtnTable = btn.child("table:last");
						if (this.arrowTooltip) {
							btn.child(this.arrowSelector).dom[this.tooltipType] = this.arrowTooltip;
						}
					},

					autoWidth : function() {
						if (this.el) {
							var tbl = this.el.child("table:first");
							var tbl2 = this.el.child("table:last");
							this.el.setWidth("auto");
							tbl.setWidth("auto");
							if (Tera.isIE7 && Tera.isStrict) {
								var ib = this.el.child(this.buttonSelector);
								if (ib && ib.getWidth() > 20) {
									ib.clip();
									ib.setWidth(Tera.util.TextMetrics.measure(ib, this.text).width + ib.getFrameWidth('lr'));
								}
							}
							if (this.minWidth) {
								if ((tbl.getWidth() + tbl2.getWidth()) < this.minWidth) {
									tbl.setWidth(this.minWidth - tbl2.getWidth());
								}
							}
							this.el.setWidth(tbl.getWidth() + tbl2.getWidth());
						}
					},

					setArrowHandler : function(handler, scope) {
						this.arrowHandler = handler;
						this.scope = scope;
					},

					onClick : function(e) {
						e.preventDefault();
						if (!this.disabled) {
							if (e.getTarget(".x-btn-menu-arrow-wrap")) {
								if (this.menu && !this.menu.isVisible() && !this.ignoreNextClick) {
									this.showMenu();
								}
								this.fireEvent("arrowclick", this, e);
								if (this.arrowHandler) {
									this.arrowHandler.call(this.scope || this, this, e);
								}
							} else {
								if (this.enableToggle) {
									this.toggle();
								}
								this.fireEvent("click", this, e);
								if (this.handler) {
									this.handler.call(this.scope || this, this, e);
								}
							}
						}
					},

					getClickEl : function(e, isUp) {
						if (!isUp) {
							return (this.lastClickEl = e.getTarget("table", 10, true));
						}
						return this.lastClickEl;
					},

					onDisable : function() {
						if (this.el) {
							if (!Tera.isIE6) {
								this.el.addClass("x-item-disabled");
							}
							this.el.child(this.buttonSelector).dom.disabled = true;
							this.el.child(this.arrowSelector).dom.disabled = true;
						}
						this.disabled = true;
					},

					onEnable : function() {
						if (this.el) {
							if (!Tera.isIE6) {
								this.el.removeClass("x-item-disabled");
							}
							this.el.child(this.buttonSelector).dom.disabled = false;
							this.el.child(this.arrowSelector).dom.disabled = false;
						}
						this.disabled = false;
					},

					isMenuTriggerOver : function(e) {
						return this.menu && e.within(this.arrowBtnTable) && !e.within(this.arrowBtnTable, true);
					},

					isMenuTriggerOut : function(e, internal) {
						return this.menu && !e.within(this.arrowBtnTable);
					},

					onDestroy : function() {
						Tera.destroy(this.arrowBtnTable);
						Tera.SplitButton.superclass.onDestroy.call(this);
					}
				});

Tera.MenuButton = Tera.SplitButton;

Tera.reg('splitbutton', Tera.SplitButton);

Tera.CycleButton = Tera.extend(Tera.SplitButton, {

	getItemText : function(item) {
		if (item && this.showText === true) {
			var text = '';
			if (this.prependText) {
				text += this.prependText;
			}
			text += item.text;
			return text;
		}
		return undefined;
	},

	setActiveItem : function(item, suppressEvent) {
		if (typeof item != 'object') {
			item = this.menu.items.get(item);
		}
		if (item) {
			if (!this.rendered) {
				this.text = this.getItemText(item);
				this.iconCls = item.iconCls;
			} else {
				var t = this.getItemText(item);
				if (t) {
					this.setText(t);
				}
				this.setIconClass(item.iconCls);
			}
			this.activeItem = item;
			if (!item.checked) {
				item.setChecked(true, true);
			}
			if (this.forceIcon) {
				this.setIconClass(this.forceIcon);
			}
			if (!suppressEvent) {
				this.fireEvent('change', this, item);
			}
		}
	},

	getActiveItem : function() {
		return this.activeItem;
	},

	initComponent : function() {
		this.addEvents(

		"change");

		if (this.changeHandler) {
			this.on('change', this.changeHandler, this.scope || this);
			delete this.changeHandler;
		}

		this.itemCount = this.items.length;

		this.menu = {
			cls :'x-cycle-menu',
			items : []
		};
		var checked;
		for ( var i = 0, len = this.itemCount; i < len; i++) {
			var item = this.items[i];
			item.group = item.group || this.id;
			item.itemIndex = i;
			item.checkHandler = this.checkHandler;
			item.scope = this;
			item.checked = item.checked || false;
			this.menu.items.push(item);
			if (item.checked) {
				checked = item;
			}
		}
		this.setActiveItem(checked, true);
		Tera.CycleButton.superclass.initComponent.call(this);

		this.on('click', this.toggleSelected, this);
	},

	checkHandler : function(item, pressed) {
		if (pressed) {
			this.setActiveItem(item);
		}
	},

	toggleSelected : function() {
		this.menu.render();

		var nextIdx, checkItem;
		for ( var i = 1; i < this.itemCount; i++) {
			nextIdx = (this.activeItem.itemIndex + i) % this.itemCount;

			checkItem = this.menu.items.itemAt(nextIdx);

			if (!checkItem.disabled) {
				checkItem.setChecked(true);
				break;
			}
		}
	}
});
Tera.reg('cycle', Tera.CycleButton);

Tera.Toolbar = function(config) {
	if (Tera.isArray(config)) {
		config = {
			buttons :config
		};
	}
	Tera.Toolbar.superclass.constructor.call(this, config);
};

( function() {

	var T = Tera.Toolbar;

	Tera.extend(T, Tera.BoxComponent, {

		trackMenus :true,

		initComponent : function() {
			T.superclass.initComponent.call(this);

			if (this.items) {
				this.buttons = this.items;
			}

			this.items = new Tera.util.MixedCollection(false, function(o) {
				return o.itemId || o.id || Tera.id();
			});
		},

		autoCreate : {
			cls :'x-toolbar x-small-editor',
			html :'<table cellspacing="0"><tr></tr></table>'
		},

		onRender : function(ct, position) {
			this.el = ct.createChild(Tera.apply( {
				id :this.id
			}, this.autoCreate), position);
			this.tr = this.el.child("tr", true);
		},

		afterRender : function() {
			T.superclass.afterRender.call(this);
			if (this.buttons) {
				this.add.apply(this, this.buttons);
				delete this.buttons;
			}
		},

		add : function() {
			var a = arguments, l = a.length;
			for ( var i = 0; i < l; i++) {
				var el = a[i];
				if (el.isFormField) {
					this.addField(el);
				} else if (el.render) {
					this.addItem(el);
				} else if (typeof el == "string") {
					if (el == "separator" || el == "-") {
						this.addSeparator();
					} else if (el == " ") {
						this.addSpacer();
					} else if (el == "->") {
						this.addFill();
					} else {
						this.addText(el);
					}
				} else if (el.tagName) {
					this.addElement(el);
				} else if (typeof el == "object") {
					if (el.xtype) {
						this.addField(Tera.ComponentMgr.create(el, 'button'));
					} else {
						this.addButton(el);
					}
				}
			}
		},

		addSeparator : function() {
			return this.addItem(new T.Separator());
		},

		addSpacer : function() {
			return this.addItem(new T.Spacer());
		},

		addFill : function() {
			return this.addItem(new T.Fill());
		},

		addElement : function(el) {
			return this.addItem(new T.Item(el));
		},

		addItem : function(item) {
			var td = this.nextBlock();
			this.initMenuTracking(item);
			item.render(td);
			this.items.add(item);
			return item;
		},

		addButton : function(config) {
			if (Tera.isArray(config)) {
				var buttons = [];
				for ( var i = 0, len = config.length; i < len; i++) {
					buttons.push(this.addButton(config[i]));
				}
				return buttons;
			}
			var b = config;
			if (!(config instanceof T.Button)) {
				b = config.split ? new T.SplitButton(config) : new T.Button(config);
			}
			var td = this.nextBlock();
			this.initMenuTracking(b);
			b.render(td);
			this.items.add(b);
			return b;
		},

		initMenuTracking : function(item) {
			if (this.trackMenus && item.menu) {
				item.on( {
					'menutriggerover' :this.onButtonTriggerOver,
					'menushow' :this.onButtonMenuShow,
					'menuhide' :this.onButtonMenuHide,
					scope :this
				})
			}
		},

		addText : function(text) {
			return this.addItem(new T.TextItem(text));
		},

		insertButton : function(index, item) {
			if (Tera.isArray(item)) {
				var buttons = [];
				for ( var i = 0, len = item.length; i < len; i++) {
					buttons.push(this.insertButton(index + i, item[i]));
				}
				return buttons;
			}
			if (!(item instanceof T.Button)) {
				item = new T.Button(item);
			}
			var td = document.createElement("td");
			this.tr.insertBefore(td, this.tr.childNodes[index]);
			this.initMenuTracking(item);
			item.render(td);
			this.items.insert(index, item);
			return item;
		},

		addDom : function(config, returnEl) {
			var td = this.nextBlock();
			Tera.DomHelper.overwrite(td, config);
			var ti = new T.Item(td.firstChild);
			ti.render(td);
			this.items.add(ti);
			return ti;
		},

		addField : function(field) {
			var td = this.nextBlock();
			field.render(td);
			var ti = new T.Item(td.firstChild);
			ti.render(td);
			this.items.add(ti);
			return ti;
		},

		nextBlock : function() {
			var td = document.createElement("td");
			this.tr.appendChild(td);
			return td;
		},

		onDestroy : function() {
			Tera.Toolbar.superclass.onDestroy.call(this);
			if (this.rendered) {
				if (this.items) {
					Tera.destroy.apply(Tera, this.items.items);
				}
				Tera.Element.uncache(this.tr);
			}
		},

		onDisable : function() {
			this.items.each( function(item) {
				if (item.disable) {
					item.disable();
				}
			});
		},

		onEnable : function() {
			this.items.each( function(item) {
				if (item.enable) {
					item.enable();
				}
			});
		},

		onButtonTriggerOver : function(btn) {
			if (this.activeMenuBtn && this.activeMenuBtn != btn) {
				this.activeMenuBtn.hideMenu();
				btn.showMenu();
				this.activeMenuBtn = btn;
			}
		},

		onButtonMenuShow : function(btn) {
			this.activeMenuBtn = btn;
		},

		onButtonMenuHide : function(btn) {
			delete this.activeMenuBtn;
		}

	});
	Tera.reg('toolbar', Tera.Toolbar);

	T.Item = function(el) {
		this.el = Tera.getDom(el);
		this.id = Tera.id(this.el);
		this.hidden = false;
	};

	T.Item.prototype = {

		getEl : function() {
			return this.el;
		},

		render : function(td) {
			this.td = td;
			td.appendChild(this.el);
		},

		destroy : function() {
			if (this.td && this.td.parentNode) {
				this.td.parentNode.removeChild(this.td);
			}
		},

		show : function() {
			this.hidden = false;
			this.td.style.display = "";
		},

		hide : function() {
			this.hidden = true;
			this.td.style.display = "none";
		},

		setVisible : function(visible) {
			if (visible) {
				this.show();
			} else {
				this.hide();
			}
		},

		focus : function() {
			Tera.fly(this.el).focus();
		},

		disable : function() {
			Tera.fly(this.td).addClass("x-item-disabled");
			this.disabled = true;
			this.el.disabled = true;
		},

		enable : function() {
			Tera.fly(this.td).removeClass("x-item-disabled");
			this.disabled = false;
			this.el.disabled = false;
		}
	};
	Tera.reg('tbitem', T.Item);

	T.Separator = function() {
		var s = document.createElement("span");
		s.className = "ytb-sep";
		T.Separator.superclass.constructor.call(this, s);
	};
	Tera.extend(T.Separator, T.Item, {
		enable :Tera.emptyFn,
		disable :Tera.emptyFn,
		focus :Tera.emptyFn
	});
	Tera.reg('tbseparator', T.Separator);

	T.Spacer = function() {
		var s = document.createElement("div");
		s.className = "ytb-spacer";
		T.Spacer.superclass.constructor.call(this, s);
	};
	Tera.extend(T.Spacer, T.Item, {
		enable :Tera.emptyFn,
		disable :Tera.emptyFn,
		focus :Tera.emptyFn
	});

	Tera.reg('tbspacer', T.Spacer);

	T.Fill = Tera.extend(T.Spacer, {

		render : function(td) {
			td.style.width = '100%';
			T.Fill.superclass.render.call(this, td);
		}
	});
	Tera.reg('tbfill', T.Fill);

	T.TextItem = function(t) {
		var s = document.createElement("span");
		s.className = "ytb-text";
		s.innerHTML = t.text ? t.text : t;
		T.TextItem.superclass.constructor.call(this, s);
	};
	Tera.extend(T.TextItem, T.Item, {
		enable :Tera.emptyFn,
		disable :Tera.emptyFn,
		focus :Tera.emptyFn
	});
	Tera.reg('tbtext', T.TextItem);

	T.Button = Tera.extend(Tera.Button, {
		hideParent :true,

		onDestroy : function() {
			T.Button.superclass.onDestroy.call(this);
			if (this.container) {
				this.container.remove();
			}
		}
	});
	Tera.reg('tbbutton', T.Button);

	T.SplitButton = Tera.extend(Tera.SplitButton, {
		hideParent :true,

		onDestroy : function() {
			T.SplitButton.superclass.onDestroy.call(this);
			if (this.container) {
				this.container.remove();
			}
		}
	});

	Tera.reg('tbsplit', T.SplitButton);

	T.MenuButton = T.SplitButton;

})();

Tera.PagingToolbar = Tera.extend(Tera.Toolbar, {

	pageSize :20,

	displayMsg :'Displaying {0} - {1} of {2}',

	emptyMsg :'No data to display',

	beforePageText :"Page",

	afterPageText :"of {0}",

	firstText :"First Page",

	prevText :"Previous Page",

	nextText :"Next Page",

	lastText :"Last Page",

	refreshText :"Refresh",

	exportFileText :"Exportar a Excel",

	exportFile :false,
	
	clipBoardText :"Copiar al  PortaPapeles",

	exportGridName :null,
	
	exportGridColum: null,
	
	//gsm: null,

	paramNames : {
		start :'start',
		limit :'limit'
	},

	initComponent : function() {
		Tera.PagingToolbar.superclass.initComponent.call(this);
		this.cursor = 0;
		this.bind(this.store);
	},

	setExport : function(bExport) {
		this.exportFile = bExport;
	},
	setExportGridName : function(pGridName) {
		this.exportGridName = pGridName;
	},
	getExportGridName : function() {
		return this.exportGridName;
	},
	getExportGridColum: function () {
		return this.exportGridColum;
	},
	//setGridSelectionModel : function(pGsm) {
	//	this.gsm = pGsm;
	//},
	onRender : function(ct, position) {
		Tera.PagingToolbar.superclass.onRender.call(this, ct, position);
		this.first = this.addButton( {
			tooltip :this.firstText,
			iconCls :"x-tbar-page-first",
			disabled :true,
			handler :this.onClick.createDelegate(this, [ "first" ])
		});
		this.prev = this.addButton( {
			tooltip :this.prevText,
			iconCls :"x-tbar-page-prev",
			disabled :true,
			handler :this.onClick.createDelegate(this, [ "prev" ])
		});
		this.addSeparator();
		this.add(this.beforePageText);
		this.field = Tera.get(this.addDom( {
			tag :"input",
			type :"text",
			size :"3",
			value :"1",
			cls :"x-tbar-page-number"
		}).el);
		this.field.on("keydown", this.onPagingKeydown, this);
		this.field.on("focus", function() {
			this.dom.select();
		});
		this.afterTextEl = this.addText(String.format(this.afterPageText, 1));
		this.field.setHeight(18);
		this.addSeparator();
		this.next = this.addButton( {
			tooltip :this.nextText,
			iconCls :"x-tbar-page-next",
			disabled :true,
			handler :this.onClick.createDelegate(this, [ "next" ])
		});
		this.last = this.addButton( {
			tooltip :this.lastText,
			iconCls :"x-tbar-page-last",
			disabled :true,
			handler :this.onClick.createDelegate(this, [ "last" ])
		});
		this.addSeparator();
		
		
	 
//		// manfredi saco para que no  muestre el boton de refresh
 		this.loading = this.addButton( {
 			tooltip :this.refreshText,
 			iconCls :"x-tbar-loading",
 			hidden :true,
 			handler :this.onClick.createDelegate(this, [ "refresh" ])
 		});
 		
		if (this.exportFile && !Tera.isEmpty(this.getExportGridName())) {
			this.addSeparator();
			this.exportBtn = this.addButton( {
				tooltip :this.exportFileText,
				iconCls :"x-tbar-page-export",
				disabled :false,
				text: "<span style='color: #000;'>Excel</span>",
				handler :this.onClick.createDelegate(this, [ "exportFile" ])
			});
		}
		
		if (!Tera.isEmpty(this.getExportGridName())) {
			this.addSeparator();
			this.clipBoardBtn = this.addButton( {
				tooltip :this.clipBoardText,
				iconCls :"x-tbar-page-clipboard",
				disabled :false,
				text: "<span style='color: #000;'>Portapapeles</span>",
				handler :this.onClick.createDelegate(this, [ "copyClipboard" ])
			});
		}
		this.addSeparator();
		if (this.displayInfo) {
			this.displayEl = Tera.fly(this.el.dom).createChild( {
				cls :'x-paging-info'
			});
		}
		var fp = this.filterPanel;
		if (!Tera.isEmpty(fp)) {
			fp.setPageSize(this.pageSize);
			
			if (!Tera.isEmpty(this.store)) {
				fp.setStore(this.store);
			}
		}
		if (this.dsLoaded) {
			this.onLoad.apply(this, this.dsLoaded);
		}
	},
	getExportBtn : function() {
		return this.exportBtn;
	},
	getClipBoardBtn : function() {
		return this.clipBoardBtn;
	},


	onLoad : function(store, r, o) {
		if (!this.rendered) {
			this.dsLoaded = [ store, r, o ];
			return;
		}
		this.cursor = o.params ? o.params[this.paramNames.start] : 0;
		var d = this.getPageData(), ap = d.activePage, ps = d.pages;

		this.afterTextEl.el.innerHTML = String.format(this.afterPageText, d.pages);
		this.field.dom.value = ap;
		this.first.setDisabled(ap == 1);
		this.prev.setDisabled(ap == 1);
		this.next.setDisabled(ap == ps);
		this.last.setDisabled(ap == ps);
		//saco paraque no aparezca el boton manfredi
		this.loading.enable();
		this.updateInfo();
	},

	getPageData : function() {
		var total = this.store.getTotalCount();
		return {
			total :total,
			activePage :Math.ceil((this.cursor + this.pageSize) / this.pageSize),
			pages :total < this.pageSize ? 1 : Math.ceil(total / this.pageSize)
		};
	},

	onLoadError : function() {
		if (!this.rendered) {
			return;
		}
		//manfredi saco
	 	this.loading.enable();
	},

	readPage : function(d) {
		var v = this.field.dom.value, pageNum;
		if (!v || isNaN(pageNum = parseInt(v, 10))) {
			this.field.dom.value = d.activePage;
			return false;
		}
		return pageNum;
	},

	onPagingKeydown : function(e) {
		var k = e.getKey(), d = this.getPageData(), pageNum;
		if (k == e.RETURN) {
			e.stopEvent();
			if (pageNum = this.readPage(d)) {
				pageNum = Math.min(Math.max(1, pageNum), d.pages) - 1;
				this.doLoad(pageNum * this.pageSize);
			}
		} else if (k == e.HOME || k == e.END) {
			e.stopEvent();
			pageNum = k == e.HOME ? 1 : d.pages;
			this.field.dom.value = pageNum;
		} else if (k == e.UP || k == e.PAGEUP || k == e.DOWN || k == e.PAGEDOWN) {
			e.stopEvent();
			if (pageNum = this.readPage(d)) {
				var increment = e.shiftKey ? 10 : 1;
				if (k == e.DOWN || k == e.PAGEDOWN) {
					increment *= -1;
				}
				pageNum += increment;
				if (pageNum >= 1 & pageNum <= d.pages) {
					this.field.dom.value = pageNum;
				}
			}
		}
	},

	beforeLoad : function() {
		
		//saco 
		if (this.rendered && this.loading) {
			  this.loading.disable();
		}
	},

	doLoad : function(start) {
		var o = {}, pn = this.paramNames;
		o[pn.start] = start;
		o[pn.limit] = this.pageSize;

		this.store.load( {
			params :o
		});
	},

	onClick : function(which) {
		var store = this.store;
		switch (which) {
			case "first":
				this.doLoad(0);
				break;
			case "prev":
				this.doLoad(Math.max(0, this.cursor - this.pageSize));
				break;
			case "next":
				this.doLoad(this.cursor + this.pageSize);
				break;
			case "last":
				var total = store.getTotalCount();
				var extra = total % this.pageSize;
				var lastStart = extra ? (total - extra) : total - this.pageSize;
				this.doLoad(lastStart);
				break;
			case "refresh":
				this.doLoad(this.cursor);
				break;
		}
	},

	unbind : function(store) {
		store = Tera.StoreMgr.lookup(store);
		store.un("beforeload", this.beforeLoad, this);
		store.un("load", this.onLoad, this);
		store.un("loadexception", this.onLoadError, this);
		this.store = undefined;
	},

	bind : function(store) {
		store = Tera.StoreMgr.lookup(store);
		store.on("beforeload", this.beforeLoad, this);
		store.on("load", this.onLoad, this);
		store.on("loadexception", this.onLoadError, this);
		this.store = store;
	},
	
    // private Manfredi
    updateInfo : function(){
        if(this.displayEl){
            var count = this.store.getCount();
            var msg = count == 0 ?
                this.emptyMsg :
                String.format(
                    this.displayMsg,
                    this.cursor+1, this.cursor+count, this.store.getTotalCount()
                );
            this.displayEl.update(msg);
        }
    }
	

	
});
Tera.reg('paging', Tera.PagingToolbar);

Tera.Resizable = function(el, config) {
	this.el = Tera.get(el);

	if (config && config.wrap) {
		config.resizeChild = this.el;
		this.el = this.el.wrap(typeof config.wrap == "object" ? config.wrap : {
			cls :"xresizable-wrap"
		});
		this.el.id = this.el.dom.id = config.resizeChild.id + "-rzwrap";
		this.el.setStyle("overflow", "hidden");
		this.el.setPositioning(config.resizeChild.getPositioning());
		config.resizeChild.clearPositioning();
		if (!config.width || !config.height) {
			var csize = config.resizeChild.getSize();
			this.el.setSize(csize.width, csize.height);
		}
		if (config.pinned && !config.adjustments) {
			config.adjustments = "auto";
		}
	}

	this.proxy = this.el.createProxy( {
		tag :"div",
		cls :"x-resizable-proxy",
		id :this.el.id + "-rzproxy"
	});
	this.proxy.unselectable();
	this.proxy.enableDisplayMode('block');

	Tera.apply(this, config);

	if (this.pinned) {
		this.disableTrackOver = true;
		this.el.addClass("x-resizable-pinned");
	}

	var position = this.el.getStyle("position");
	if (position != "absolute" && position != "fixed") {
		this.el.setStyle("position", "relative");
	}
	if (!this.handles) {
		this.handles = 's,e,se';
		if (this.multiDirectional) {
			this.handles += ',n,w';
		}
	}
	if (this.handles == "all") {
		this.handles = "n s e w ne nw se sw";
	}
	var hs = this.handles.split(/\s*?[,;]\s*?| /);
	var ps = Tera.Resizable.positions;
	for ( var i = 0, len = hs.length; i < len; i++) {
		if (hs[i] && ps[hs[i]]) {
			var pos = ps[hs[i]];
			this[pos] = new Tera.Resizable.Handle(this, pos, this.disableTrackOver, this.transparent);
		}
	}

	this.corner = this.southeast;

	if (this.handles.indexOf("n") != -1 || this.handles.indexOf("w") != -1) {
		this.updateBox = true;
	}

	this.activeHandle = null;

	if (this.resizeChild) {
		if (typeof this.resizeChild == "boolean") {
			this.resizeChild = Tera.get(this.el.dom.firstChild, true);
		} else {
			this.resizeChild = Tera.get(this.resizeChild, true);
		}
	}

	if (this.adjustments == "auto") {
		var rc = this.resizeChild;
		var hw = this.west, he = this.east, hn = this.north, hs = this.south;
		if (rc && (hw || hn)) {
			rc.position("relative");
			rc.setLeft(hw ? hw.el.getWidth() : 0);
			rc.setTop(hn ? hn.el.getHeight() : 0);
		}
		this.adjustments = [ (he ? -he.el.getWidth() : 0) + (hw ? -hw.el.getWidth() : 0),
				(hn ? -hn.el.getHeight() : 0) + (hs ? -hs.el.getHeight() : 0) - 1 ];
	}

	if (this.draggable) {
		this.dd = this.dynamic ? this.el.initDD(null) : this.el.initDDProxy(null, {
			dragElId :this.proxy.id
		});
		this.dd.setHandleElId(this.resizeChild ? this.resizeChild.id : this.el.id);
	}

	this.addEvents("beforeresize", "resize");

	if (this.width !== null && this.height !== null) {
		this.resizeTo(this.width, this.height);
	} else {
		this.updateChildSize();
	}
	if (Tera.isIE) {
		this.el.dom.style.zoom = 1;
	}
	Tera.Resizable.superclass.constructor.call(this);
};

Tera.extend(Tera.Resizable, Tera.util.Observable, {
	resizeChild :false,
	adjustments : [ 0, 0 ],
	minWidth :5,
	minHeight :5,
	maxWidth :10000,
	maxHeight :10000,
	enabled :true,
	animate :false,
	duration :.35,
	dynamic :false,
	handles :false,
	multiDirectional :false,
	disableTrackOver :false,
	easing :'easeOutStrong',
	widthIncrement :0,
	heightIncrement :0,
	pinned :false,
	width :null,
	height :null,
	preserveRatio :false,
	transparent :false,
	minX :0,
	minY :0,
	draggable :false,

	resizeTo : function(width, height) {
		this.el.setSize(width, height);
		this.updateChildSize();
		this.fireEvent("resize", this, width, height, null);
	},

	startSizing : function(e, handle) {
		this.fireEvent("beforeresize", this, e);
		if (this.enabled) {

			if (!this.overlay) {
				this.overlay = this.el.createProxy( {
					tag :"div",
					cls :"x-resizable-overlay",
					html :"&#160;"
				}, Tera.getBody());
				this.overlay.unselectable();
				this.overlay.enableDisplayMode("block");
				this.overlay.on("mousemove", this.onMouseMove, this);
				this.overlay.on("mouseup", this.onMouseUp, this);
			}
			this.overlay.setStyle("cursor", handle.el.getStyle("cursor"));

			this.resizing = true;
			this.startBox = this.el.getBox();
			this.startPoint = e.getXY();
			this.offsets = [ (this.startBox.x + this.startBox.width) - this.startPoint[0],
					(this.startBox.y + this.startBox.height) - this.startPoint[1] ];

			this.overlay.setSize(Tera.lib.Dom.getViewWidth(true), Tera.lib.Dom.getViewHeight(true));
			this.overlay.show();

			if (this.constrainTo) {
				var ct = Tera.get(this.constrainTo);
				this.resizeRegion = ct.getRegion().adjust(ct.getFrameWidth('t'), ct.getFrameWidth('l'), -ct.getFrameWidth('b'),
						-ct.getFrameWidth('r'));
			}

			this.proxy.setStyle('visibility', 'hidden');
			this.proxy.show();
			this.proxy.setBox(this.startBox);
			if (!this.dynamic) {
				this.proxy.setStyle('visibility', 'visible');
			}
		}
	},

	onMouseDown : function(handle, e) {
		if (this.enabled) {
			e.stopEvent();
			this.activeHandle = handle;
			this.startSizing(e, handle);
		}
	},

	onMouseUp : function(e) {
		var size = this.resizeElement();
		this.resizing = false;
		this.handleOut();
		this.overlay.hide();
		this.proxy.hide();
		this.fireEvent("resize", this, size.width, size.height, e);
	},

	updateChildSize : function() {
		if (this.resizeChild) {
			var el = this.el;
			var child = this.resizeChild;
			var adj = this.adjustments;
			if (el.dom.offsetWidth) {
				var b = el.getSize(true);
				child.setSize(b.width + adj[0], b.height + adj[1]);
			}

			if (Tera.isIE) {
				setTimeout( function() {
					if (el.dom.offsetWidth) {
						var b = el.getSize(true);
						child.setSize(b.width + adj[0], b.height + adj[1]);
					}
				}, 10);
			}
		}
	},

	snap : function(value, inc, min) {
		if (!inc || !value)
			return value;
		var newValue = value;
		var m = value % inc;
		if (m > 0) {
			if (m > (inc / 2)) {
				newValue = value + (inc - m);
			} else {
				newValue = value - m;
			}
		}
		return Math.max(min, newValue);
	},

	resizeElement : function() {
		var box = this.proxy.getBox();
		if (this.updateBox) {
			this.el.setBox(box, false, this.animate, this.duration, null, this.easing);
		} else {
			this.el.setSize(box.width, box.height, this.animate, this.duration, null, this.easing);
		}
		this.updateChildSize();
		if (!this.dynamic) {
			this.proxy.hide();
		}
		return box;
	},

	constrain : function(v, diff, m, mx) {
		if (v - diff < m) {
			diff = v - m;
		} else if (v - diff > mx) {
			diff = mx - v;
		}
		return diff;
	},

	onMouseMove : function(e) {
		if (this.enabled) {
			try {

				if (this.resizeRegion && !this.resizeRegion.contains(e.getPoint())) {
					return;
				}

				var curSize = this.curSize || this.startBox;
				var x = this.startBox.x, y = this.startBox.y;
				var ox = x, oy = y;
				var w = curSize.width, h = curSize.height;
				var ow = w, oh = h;
				var mw = this.minWidth, mh = this.minHeight;
				var mxw = this.maxWidth, mxh = this.maxHeight;
				var wi = this.widthIncrement;
				var hi = this.heightIncrement;

				var eventXY = e.getXY();
				var diffX = -(this.startPoint[0] - Math.max(this.minX, eventXY[0]));
				var diffY = -(this.startPoint[1] - Math.max(this.minY, eventXY[1]));

				var pos = this.activeHandle.position;

				switch (pos) {
					case "east":
						w += diffX;
						w = Math.min(Math.max(mw, w), mxw);
						break;
					case "south":
						h += diffY;
						h = Math.min(Math.max(mh, h), mxh);
						break;
					case "southeast":
						w += diffX;
						h += diffY;
						w = Math.min(Math.max(mw, w), mxw);
						h = Math.min(Math.max(mh, h), mxh);
						break;
					case "north":
						diffY = this.constrain(h, diffY, mh, mxh);
						y += diffY;
						h -= diffY;
						break;
					case "west":
						diffX = this.constrain(w, diffX, mw, mxw);
						x += diffX;
						w -= diffX;
						break;
					case "northeast":
						w += diffX;
						w = Math.min(Math.max(mw, w), mxw);
						diffY = this.constrain(h, diffY, mh, mxh);
						y += diffY;
						h -= diffY;
						break;
					case "northwest":
						diffX = this.constrain(w, diffX, mw, mxw);
						diffY = this.constrain(h, diffY, mh, mxh);
						y += diffY;
						h -= diffY;
						x += diffX;
						w -= diffX;
						break;
					case "southwest":
						diffX = this.constrain(w, diffX, mw, mxw);
						h += diffY;
						h = Math.min(Math.max(mh, h), mxh);
						x += diffX;
						w -= diffX;
						break;
				}

				var sw = this.snap(w, wi, mw);
				var sh = this.snap(h, hi, mh);
				if (sw != w || sh != h) {
					switch (pos) {
						case "northeast":
							y -= sh - h;
							break;
						case "north":
							y -= sh - h;
							break;
						case "southwest":
							x -= sw - w;
							break;
						case "west":
							x -= sw - w;
							break;
						case "northwest":
							x -= sw - w;
							y -= sh - h;
							break;
					}
					w = sw;
					h = sh;
				}

				if (this.preserveRatio) {
					switch (pos) {
						case "southeast":
						case "east":
							h = oh * (w / ow);
							h = Math.min(Math.max(mh, h), mxh);
							w = ow * (h / oh);
							break;
						case "south":
							w = ow * (h / oh);
							w = Math.min(Math.max(mw, w), mxw);
							h = oh * (w / ow);
							break;
						case "northeast":
							w = ow * (h / oh);
							w = Math.min(Math.max(mw, w), mxw);
							h = oh * (w / ow);
							break;
						case "north":
							var tw = w;
							w = ow * (h / oh);
							w = Math.min(Math.max(mw, w), mxw);
							h = oh * (w / ow);
							x += (tw - w) / 2;
							break;
						case "southwest":
							h = oh * (w / ow);
							h = Math.min(Math.max(mh, h), mxh);
							var tw = w;
							w = ow * (h / oh);
							x += tw - w;
							break;
						case "west":
							var th = h;
							h = oh * (w / ow);
							h = Math.min(Math.max(mh, h), mxh);
							y += (th - h) / 2;
							var tw = w;
							w = ow * (h / oh);
							x += tw - w;
							break;
						case "northwest":
							var tw = w;
							var th = h;
							h = oh * (w / ow);
							h = Math.min(Math.max(mh, h), mxh);
							w = ow * (h / oh);
							y += th - h;
							x += tw - w;
							break;

					}
				}
				this.proxy.setBounds(x, y, w, h);
				if (this.dynamic) {
					this.resizeElement();
				}
			} catch (e) {
			}
		}
	},

	handleOver : function() {
		if (this.enabled) {
			this.el.addClass("x-resizable-over");
		}
	},

	handleOut : function() {
		if (!this.resizing) {
			this.el.removeClass("x-resizable-over");
		}
	},

	getEl : function() {
		return this.el;
	},

	getResizeChild : function() {
		return this.resizeChild;
	},

	destroy : function(removeEl) {
		this.proxy.remove();
		if (this.overlay) {
			this.overlay.removeAllListeners();
			this.overlay.remove();
		}
		var ps = Tera.Resizable.positions;
		for ( var k in ps) {
			if (typeof ps[k] != "function" && this[ps[k]]) {
				var h = this[ps[k]];
				h.el.removeAllListeners();
				h.el.remove();
			}
		}
		if (removeEl) {
			this.el.update("");
			this.el.remove();
		}
	},

	syncHandleHeight : function() {
		var h = this.el.getHeight(true);
		if (this.west) {
			this.west.el.setHeight(h);
		}
		if (this.east) {
			this.east.el.setHeight(h);
		}
	}
});

Tera.Resizable.positions = {
	n :"north",
	s :"south",
	e :"east",
	w :"west",
	se :"southeast",
	sw :"southwest",
	nw :"northwest",
	ne :"northeast"
};

Tera.Resizable.Handle = function(rz, pos, disableTrackOver, transparent) {
	if (!this.tpl) {

		var tpl = Tera.DomHelper.createTemplate( {
			tag :"div",
			cls :"x-resizable-handle x-resizable-handle-{0}"
		});
		tpl.compile();
		Tera.Resizable.Handle.prototype.tpl = tpl;
	}
	this.position = pos;
	this.rz = rz;
	this.el = this.tpl.append(rz.el.dom, [ this.position ], true);
	this.el.unselectable();
	if (transparent) {
		this.el.setOpacity(0);
	}
	this.el.on("mousedown", this.onMouseDown, this);
	if (!disableTrackOver) {
		this.el.on("mouseover", this.onMouseOver, this);
		this.el.on("mouseout", this.onMouseOut, this);
	}
};

Tera.Resizable.Handle.prototype = {
	afterResize : function(rz) {

	},

	onMouseDown : function(e) {
		this.rz.onMouseDown(this, e);
	},

	onMouseOver : function(e) {
		this.rz.handleOver(this, e);
	},

	onMouseOut : function(e) {
		this.rz.handleOut(this, e);
	}
};

Tera.Editor = function(field, config) {
	this.field = field;
	Tera.Editor.superclass.constructor.call(this, config);
};

Tera.extend(Tera.Editor, Tera.Component, {

	value :"",

	alignment :"c-c?",

	shadow :"frame",

	constrain :false,

	swallowKeys :true,

	completeOnEnter :false,

	cancelOnEsc :false,

	updateEl :false,

	initComponent : function() {
		Tera.Editor.superclass.initComponent.call(this);
		this.addEvents(

		"beforestartedit",

		"startedit",

		"beforecomplete",

		"complete",

		"specialkey");
	},

	onRender : function(ct, position) {
		this.el = new Tera.Layer( {
			shadow :this.shadow,
			cls :"x-editor",
			parentEl :ct,
			shim :this.shim,
			shadowOffset :4,
			id :this.id,
			constrain :this.constrain
		});
		this.el.setStyle("overflow", Tera.isGecko ? "auto" : "hidden");
		if (this.field.msgTarget != 'title') {
			this.field.msgTarget = 'qtip';
		}
		this.field.inEditor = true;
		this.field.render(this.el);
		if (Tera.isGecko) {
			this.field.el.dom.setAttribute('autocomplete', 'off');
		}
		this.field.on("specialkey", this.onSpecialKey, this);
		if (this.swallowKeys) {
			this.field.el.swallowEvent( [ 'keydown', 'keypress' ]);
		}
		this.field.show();
		this.field.on("blur", this.onBlur, this);
		if (this.field.grow) {
			this.field.on("autosize", this.el.sync, this.el, {
				delay :1
			});
		}
	},

	onSpecialKey : function(field, e) {
		if (this.completeOnEnter && e.getKey() == e.ENTER) {
			e.stopEvent();
			this.completeEdit();
		} else if (this.cancelOnEsc && e.getKey() == e.ESC) {
			this.cancelEdit();
		} else {
			this.fireEvent('specialkey', field, e);
		}
	},

	startEdit : function(el, value) {
		if (this.editing) {
			this.completeEdit();
		}
		this.boundEl = Tera.get(el);
		var v = value !== undefined ? value : this.boundEl.dom.innerHTML;
		if (!this.rendered) {
			this.render(this.parentEl || document.body);
		}
		if (this.fireEvent("beforestartedit", this, this.boundEl, v) === false) {
			return;
		}
		this.startValue = v;
		this.field.setValue(v);
		this.doAutoSize();
		this.el.alignTo(this.boundEl, this.alignment);
		this.editing = true;
		this.show();
	},

	doAutoSize : function() {
		if (this.autoSize) {
			var sz = this.boundEl.getSize();
			switch (this.autoSize) {
				case "width":
					this.setSize(sz.width, "");
					break;
				case "height":
					this.setSize("", sz.height);
					break;
				default:
					this.setSize(sz.width, sz.height);
			}
		}
	},

	setSize : function(w, h) {
		delete this.field.lastSize;
		this.field.setSize(w, h);
		if (this.el) {
			this.el.sync();
		}
	},

	realign : function() {
		this.el.alignTo(this.boundEl, this.alignment);
	},

	completeEdit : function(remainVisible) {
		if (!this.editing) {
			return;
		}
		var v = this.getValue();
		if (this.revertInvalid !== false && !this.field.isValid()) {
			v = this.startValue;
			this.cancelEdit(true);
		}
		if (String(v) === String(this.startValue) && this.ignoreNoChange) {
			this.editing = false;
			this.hide();
			return;
		}
		if (this.fireEvent("beforecomplete", this, v, this.startValue) !== false) {
			this.editing = false;
			if (this.updateEl && this.boundEl) {
				this.boundEl.update(v);
			}
			if (remainVisible !== true) {
				this.hide();
			}
			this.fireEvent("complete", this, v, this.startValue);
		}
	},

	onShow : function() {
		this.el.show();
		if (this.hideEl !== false) {
			this.boundEl.hide();
		}
		this.field.show();
		if (Tera.isIE && !this.fixIEFocus) {
			this.fixIEFocus = true;
			this.deferredFocus.defer(50, this);
		} else {
			this.field.focus();
		}
		this.fireEvent("startedit", this.boundEl, this.startValue);
	},

	deferredFocus : function() {
		if (this.editing) {
			this.field.focus();
		}
	},

	cancelEdit : function(remainVisible) {
		if (this.editing) {
			this.setValue(this.startValue);
			if (remainVisible !== true) {
				this.hide();
			}
		}
	},

	onBlur : function() {
		if (this.allowBlur !== true && this.editing) {
			this.completeEdit();
		}
	},

	onHide : function() {
		if (this.editing) {
			this.completeEdit();
			return;
		}
		this.field.blur();
		if (this.field.collapse) {
			this.field.collapse();
		}
		this.el.hide();
		if (this.hideEl !== false) {
			this.boundEl.show();
		}
	},

	setValue : function(v) {
		this.field.setValue(v);
	},

	getValue : function() {
		return this.field.getValue();
	},

	beforeDestroy : function() {
		this.field.destroy();
		this.field = null;
	}
});
Tera.reg('editor', Tera.Editor);

Tera.MessageBox = function() {
	var dlg, opt, mask, waitTimer;
	var bodyEl, msgEl, textboxEl, textareaEl, progressBar, pp, iconEl, spacerEl;
	var buttons, activeTextEl, bwidth, iconCls = '';

	var handleButton = function(button) {
		if (dlg.isVisible()) {
			dlg.hide();
			Tera.callback(opt.fn, opt.scope || window, [ button, activeTextEl.dom.value ], 1);
		}
	};

	var handleHide = function() {
		if (opt && opt.cls) {
			dlg.el.removeClass(opt.cls);
		}
		progressBar.reset();
	};

	var handleEsc = function(d, k, e) {
		if (opt && opt.closable !== false) {
			dlg.hide();
		}
		if (e) {
			e.stopEvent();
		}
	};

	var updateButtons = function(b) {
		var width = 0;
		if (!b) {
			buttons["ok"].hide();
			buttons["cancel"].hide();
			buttons["yes"].hide();
			buttons["no"].hide();
			return width;
		}
		dlg.footer.dom.style.display = '';
		for ( var k in buttons) {
			if (typeof buttons[k] != "function") {
				if (b[k]) {
					buttons[k].show();
					buttons[k].setText(typeof b[k] == "string" ? b[k] : Tera.MessageBox.buttonText[k]);
					width += buttons[k].el.getWidth() + 15;
				} else {
					buttons[k].hide();
				}
			}
		}
		return width;
	};

	return {

		getDialog : function(titleText) {
			if (!dlg) {
				dlg = new Tera.Window( {
					autoCreate :true,
					title :titleText,
					resizable :false,
					constrain :true,
					constrainHeader :true,
					minimizable :false,
					maximizable :false,
					stateful :false,
					modal :true,
					shim :true,
					buttonAlign :"center",
					width :400,
					height :100,
					minHeight :80,
					plain :true,
					footer :true,
					closable :true,
					close : function() {
						if (opt && opt.buttons && opt.buttons.no && !opt.buttons.cancel) {
							handleButton("no");
						} else {
							handleButton("cancel");
						}
					}
				});
				buttons = {};
				var bt = this.buttonText;

				buttons["ok"] = dlg.addButton(bt["ok"], handleButton.createCallback("ok"));
				buttons["yes"] = dlg.addButton(bt["yes"], handleButton.createCallback("yes"));
				buttons["no"] = dlg.addButton(bt["no"], handleButton.createCallback("no"));
				buttons["cancel"] = dlg.addButton(bt["cancel"], handleButton.createCallback("cancel"));
				buttons["ok"].hideMode = buttons["yes"].hideMode = buttons["no"].hideMode = buttons["cancel"].hideMode = 'offsets';
				dlg.render(document.body);
				dlg.getEl().addClass('x-window-dlg');
				mask = dlg.mask;
				bodyEl = dlg.body
						.createChild( {
							html :'<div class="ext-mb-icon"></div><div class="ext-mb-content"><span class="ext-mb-text"></span><br /><div class="ext-mb-fix-cursor"><input type="text" class="ext-mb-input" /><textarea class="ext-mb-textarea"></textarea></div></div>'
						});
				iconEl = Tera.get(bodyEl.dom.firstChild);
				var contentEl = bodyEl.dom.childNodes[1];
				msgEl = Tera.get(contentEl.firstChild);
				textboxEl = Tera.get(contentEl.childNodes[2].firstChild);
				textboxEl.enableDisplayMode();
				textboxEl.addKeyListener( [ 10, 13 ], function() {
					if (dlg.isVisible() && opt && opt.buttons) {
						if (opt.buttons.ok) {
							handleButton("ok");
						} else if (opt.buttons.yes) {
							handleButton("yes");
						}
					}
				});
				textareaEl = Tera.get(contentEl.childNodes[2].childNodes[1]);
				textareaEl.enableDisplayMode();
				progressBar = new Tera.ProgressBar( {
					renderTo :bodyEl
				});
				bodyEl.createChild( {
					cls :'x-clear'
				});
			}
			return dlg;
		},

		updateText : function(text) {
			if (!dlg.isVisible() && !opt.width) {
				dlg.setSize(this.maxWidth, 100);
			}
			msgEl.update(text || '&#160;');

			var iw = iconCls != '' ? (iconEl.getWidth() + iconEl.getMargins('lr')) : 0;
			var mw = msgEl.getWidth() + msgEl.getMargins('lr');
			var fw = dlg.getFrameWidth('lr');
			var bw = dlg.body.getFrameWidth('lr');
			if (Tera.isIE && iw > 0) {

				iw += 3;
			}
			var w = Math.max(Math.min(opt.width || iw + mw + fw + bw, this.maxWidth), Math.max(opt.minWidth || this.minWidth, bwidth || 0));

			if (opt.prompt === true) {
				activeTextEl.setWidth(w - iw - fw - bw);
			}
			if (opt.progress === true || opt.wait === true) {
				progressBar.setSize(w - iw - fw - bw);
			}
			dlg.setSize(w, 'auto').center();
			return this;
		},

		updateProgress : function(value, progressText, msg) {
			progressBar.updateProgress(value, progressText);
			if (msg) {
				this.updateText(msg);
			}
			return this;
		},

		isVisible : function() {
			return dlg && dlg.isVisible();
		},

		hide : function() {
			if (this.isVisible()) {
				dlg.hide();
				handleHide();
			}
			return this;
		},

		show : function(options) {
			if (this.isVisible()) {
				this.hide();
			}
			opt = options;
			var d = this.getDialog(opt.title || "&#160;");

			d.setTitle(opt.title || "&#160;");
			var allowClose = (opt.closable !== false && opt.progress !== true && opt.wait !== true);
			d.tools.close.setDisplayed(allowClose);
			activeTextEl = textboxEl;
			opt.prompt = opt.prompt || (opt.multiline ? true : false);
			if (opt.prompt) {
				if (opt.multiline) {
					textboxEl.hide();
					textareaEl.show();
					textareaEl.setHeight(typeof opt.multiline == "number" ? opt.multiline : this.defaultTextHeight);
					activeTextEl = textareaEl;
				} else {
					textboxEl.show();
					textareaEl.hide();
				}
			} else {
				textboxEl.hide();
				textareaEl.hide();
			}
			activeTextEl.dom.value = opt.value || "";
			if (opt.prompt) {
				d.focusEl = activeTextEl;
			} else {
				var bs = opt.buttons;
				var db = null;
				if (bs && bs.ok) {
					db = buttons["ok"];
				} else if (bs && bs.yes) {
					db = buttons["yes"];
				}
				if (db) {
					d.focusEl = db;
				}
			}
			if (opt.iconCls) {
				d.setIconClass(opt.iconCls);
			}
			this.setIcon(opt.icon);
			bwidth = updateButtons(opt.buttons);
			progressBar.setVisible(opt.progress === true || opt.wait === true);
			this.updateProgress(0, opt.progressText);
			this.updateText(opt.msg);
			if (opt.cls) {
				d.el.addClass(opt.cls);
			}
			d.proxyDrag = opt.proxyDrag === true;
			d.modal = opt.modal !== false;
			d.mask = opt.modal !== false ? mask : false;
			if (!d.isVisible()) {

				document.body.appendChild(dlg.el.dom);
				d.setAnimateTarget(opt.animEl);
				d.show(opt.animEl);
			}

			d.on('show', function() {
				if (allowClose === true) {
					d.keyMap.enable();
				} else {
					d.keyMap.disable();
				}
			}, this, {
				single :true
			});

			if (opt.wait === true) {
				progressBar.wait(opt.waitConfig);
			}
			return this;
		},

		setIcon : function(icon) {
			if (icon && icon != '') {
				iconEl.removeClass('x-hidden');
				iconEl.replaceClass(iconCls, icon);
				iconCls = icon;
			} else {
				iconEl.replaceClass(iconCls, 'x-hidden');
				iconCls = '';
			}
			return this;
		},

		progress : function(title, msg, progressText) {
			this.show( {
				title :title,
				msg :msg,
				buttons :false,
				progress :true,
				closable :false,
				minWidth :this.minProgressWidth,
				progressText :progressText
			});
			return this;
		},

		wait : function(msg, title, config) {
			this.show( {
				title :title,
				msg :msg,
				buttons :false,
				closable :false,
				wait :true,
				modal :true,
				minWidth :this.minProgressWidth,
				waitConfig :config
			});
			return this;
		},

		alert : function(title, msg, fn, scope) {
			this.show( {
				title :title,
				msg :msg,
				buttons :this.OK,
				fn :fn,
				scope :scope
			});
			return this;
		},

		confirm : function(title, msg, fn, scope) {
			this.show( {
				title :title,
				msg :msg,
				buttons :this.YESNO,
				fn :fn,
				scope :scope,
				icon :this.QUESTION
			});
			return this;
		},

		prompt : function(title, msg, fn, scope, multiline, value) {
			this.show( {
				title :title,
				msg :msg,
				buttons :this.OKCANCEL,
				fn :fn,
				minWidth :250,
				scope :scope,
				prompt :true,
				multiline :multiline,
				value :value
			});
			return this;
		},

		OK : {
			ok :true
		},

		CANCEL : {
			cancel :true
		},

		OKCANCEL : {
			ok :true,
			cancel :true
		},

		YESNO : {
			yes :true,
			no :true
		},

		YESNOCANCEL : {
			yes :true,
			no :true,
			cancel :true
		},

		INFO :'ext-mb-info',

		WARNING :'ext-mb-warning',

		QUESTION :'ext-mb-question',

		ERROR :'ext-mb-error',

		defaultTextHeight :75,

		maxWidth :600,

		minWidth :100,

		minProgressWidth :250,

		buttonText : {
			ok :"OK",
			cancel :"Cancel",
			yes :"Yes",
			no :"No"
		}
	};
}();

Tera.Msg = Tera.MessageBox;

Tera.Tip = Tera.extend(Tera.Panel, {

	minWidth :40,

	maxWidth :300,

	shadow :"sides",

	defaultAlign :"tl-bl?",
	autoRender :true,
	quickShowInterval :250,

	frame :true,
	hidden :true,
	baseCls :'x-tip',
	floating : {
		shadow :true,
		shim :true,
		useDisplay :true,
		constrain :false
	},
	autoHeight :true,

	initComponent : function() {
		Tera.Tip.superclass.initComponent.call(this);
		if (this.closable && !this.title) {
			this.elements += ',header';
		}
	},

	afterRender : function() {
		Tera.Tip.superclass.afterRender.call(this);
		if (this.closable) {
			this.addTool( {
				id :'close',
				handler :this.hide,
				scope :this
			});
		}
	},

	showAt : function(xy) {
		Tera.Tip.superclass.show.call(this);
		if (this.measureWidth !== false && (!this.initialConfig || typeof this.initialConfig.width != 'number')) {
			this.doAutoWidth();
		}
		if (this.constrainPosition) {
			xy = this.el.adjustForConstraints(xy);
		}
		this.setPagePosition(xy[0], xy[1]);
	},

	doAutoWidth : function() {
		var bw = this.body.getTextWidth();
		if (this.title) {
			bw = Math.max(bw, this.header.child('span').getTextWidth(this.title));
		}
		bw += this.getFrameWidth() + (this.closable ? 20 : 0) + this.body.getPadding("lr");
		this.setWidth(bw.constrain(this.minWidth, this.maxWidth));
	},

	showBy : function(el, pos) {
		if (!this.rendered) {
			this.render(Tera.getBody());
		}
		this.showAt(this.el.getAlignToXY(el, pos || this.defaultAlign));
	},

	initDraggable : function() {
		this.dd = new Tera.Tip.DD(this, typeof this.draggable == 'boolean' ? null : this.draggable);
		this.header.addClass('x-tip-draggable');
	}
});

Tera.Tip.DD = function(tip, config) {
	Tera.apply(this, config);
	this.tip = tip;
	Tera.Tip.DD.superclass.constructor.call(this, tip.el.id, 'WindowDD-' + tip.id);
	this.setHandleElId(tip.header.id);
	this.scroll = false;
};

Tera.extend(Tera.Tip.DD, Tera.dd.DD, {
	moveOnly :true,
	scroll :false,
	headerOffsets : [ 100, 25 ],
	startDrag : function() {
		this.tip.el.disableShadow();
	},
	endDrag : function(e) {
		this.tip.el.enableShadow(true);
	}
});

Tera.ToolTip = Tera.extend(Tera.Tip, {

	showDelay :500,

	hideDelay :200,

	dismissDelay :5000,

	mouseOffset : [ 15, 18 ],

	trackMouse :false,
	constrainPosition :true,

	initComponent : function() {
		Tera.ToolTip.superclass.initComponent.call(this);
		this.lastActive = new Date();
		this.initTarget();
	},

	initTarget : function() {
		if (this.target) {
			this.target = Tera.get(this.target);
			this.target.on('mouseover', this.onTargetOver, this);
			this.target.on('mouseout', this.onTargetOut, this);
			this.target.on('mousemove', this.onMouseMove, this);
		}
	},

	onMouseMove : function(e) {
		this.targetXY = e.getXY();
		if (!this.hidden && this.trackMouse) {
			this.setPagePosition(this.getTargetXY());
		}
	},

	getTargetXY : function() {
		return [ this.targetXY[0] + this.mouseOffset[0], this.targetXY[1] + this.mouseOffset[1] ];
	},

	onTargetOver : function(e) {
		if (this.disabled || e.within(this.target.dom, true)) {
			return;
		}
		this.clearTimer('hide');
		this.targetXY = e.getXY();
		this.delayShow();
	},

	delayShow : function() {
		if (this.hidden && !this.showTimer) {
			if (this.lastActive.getElapsed() < this.quickShowInterval) {
				this.show();
			} else {
				this.showTimer = this.show.defer(this.showDelay, this);
			}
		} else if (!this.hidden && this.autoHide !== false) {
			this.show();
		}
	},

	onTargetOut : function(e) {
		if (this.disabled || e.within(this.target.dom, true)) {
			return;
		}
		this.clearTimer('show');
		if (this.autoHide !== false) {
			this.delayHide();
		}
	},

	delayHide : function() {
		if (!this.hidden && !this.hideTimer) {
			this.hideTimer = this.hide.defer(this.hideDelay, this);
		}
	},

	hide : function() {
		this.clearTimer('dismiss');
		this.lastActive = new Date();
		Tera.ToolTip.superclass.hide.call(this);
	},

	show : function() {
		this.showAt(this.getTargetXY());
	},

	showAt : function(xy) {
		this.lastActive = new Date();
		this.clearTimers();
		Tera.ToolTip.superclass.showAt.call(this, xy);
		if (this.dismissDelay && this.autoHide !== false) {
			this.dismissTimer = this.hide.defer(this.dismissDelay, this);
		}
	},

	clearTimer : function(name) {
		name = name + 'Timer';
		clearTimeout(this[name]);
		delete this[name];
	},

	clearTimers : function() {
		this.clearTimer('show');
		this.clearTimer('dismiss');
		this.clearTimer('hide');
	},

	onShow : function() {
		Tera.ToolTip.superclass.onShow.call(this);
		Tera.getDoc().on('mousedown', this.onDocMouseDown, this);
	},

	onHide : function() {
		Tera.ToolTip.superclass.onHide.call(this);
		Tera.getDoc().un('mousedown', this.onDocMouseDown, this);
	},

	onDocMouseDown : function(e) {
		if (this.autoHide !== false && !e.within(this.el.dom)) {
			this.disable();
			this.enable.defer(100, this);
		}
	},

	onDisable : function() {
		this.clearTimers();
		this.hide();
	},

	adjustPosition : function(x, y) {

		var ay = this.targetXY[1], h = this.getSize().height;
		if (this.constrainPosition && y <= ay && (y + h) >= ay) {
			y = ay - h - 5;
		}
		return {
			x :x,
			y :y
		};
	},

	onDestroy : function() {
		Tera.ToolTip.superclass.onDestroy.call(this);
		if (this.target) {
			this.target.un('mouseover', this.onTargetOver, this);
			this.target.un('mouseout', this.onTargetOut, this);
			this.target.un('mousemove', this.onMouseMove, this);
		}
	}
});

Tera.QuickTip = Tera.extend(Tera.ToolTip, {

	interceptTitles :false,

	tagConfig : {
		namespace :"ext",
		attribute :"qtip",
		width :"qwidth",
		target :"target",
		title :"qtitle",
		hide :"hide",
		cls :"qclass",
		align :"qalign"
	},

	initComponent : function() {
		this.target = this.target || Tera.getDoc();
		this.targets = this.targets || {};
		Tera.QuickTip.superclass.initComponent.call(this);
	},

	register : function(config) {
		var cs = Tera.isArray(config) ? config : arguments;
		for ( var i = 0, len = cs.length; i < len; i++) {
			var c = cs[i];
			var target = c.target;
			if (target) {
				if (Tera.isArray(target)) {
					for ( var j = 0, jlen = target.length; j < jlen; j++) {
						this.targets[Tera.id(target[j])] = c;
					}
				} else {
					this.targets[Tera.id(target)] = c;
				}
			}
		}
	},

	unregister : function(el) {
		delete this.targets[Tera.id(el)];
	},

	onTargetOver : function(e) {
		if (this.disabled) {
			return;
		}
		this.targetXY = e.getXY();
		var t = e.getTarget();
		if (!t || t.nodeType !== 1 || t == document || t == document.body) {
			return;
		}
		if (this.activeTarget && t == this.activeTarget.el) {
			this.clearTimer('hide');
			this.show();
			return;
		}
		if (t && this.targets[t.id]) {
			this.activeTarget = this.targets[t.id];
			this.activeTarget.el = t;
			this.delayShow();
			return;
		}
		var ttp, et = Tera.fly(t), cfg = this.tagConfig;
		var ns = cfg.namespace;
		if (this.interceptTitles && t.title) {
			ttp = t.title;
			t.qtip = ttp;
			t.removeAttribute("title");
			e.preventDefault();
		} else {
			ttp = t.qtip || et.getAttributeNS(ns, cfg.attribute);
		}
		if (ttp) {
			var autoHide = et.getAttributeNS(ns, cfg.hide);
			this.activeTarget = {
				el :t,
				text :ttp,
				width :et.getAttributeNS(ns, cfg.width),
				autoHide :autoHide != "user" && autoHide !== 'false',
				title :et.getAttributeNS(ns, cfg.title),
				cls :et.getAttributeNS(ns, cfg.cls),
				align :et.getAttributeNS(ns, cfg.align)
			};
			this.delayShow();
		}
	},

	onTargetOut : function(e) {
		this.clearTimer('show');
		if (this.autoHide !== false) {
			this.delayHide();
		}
	},

	showAt : function(xy) {
		var t = this.activeTarget;
		if (t) {
			if (!this.rendered) {
				this.render(Tera.getBody());
				this.activeTarget = t;
			}
			if (t.width) {
				this.setWidth(t.width);
				this.body.setWidth(this.adjustBodyWidth(t.width - this.getFrameWidth()));
				this.measureWidth = false;
			} else {
				this.measureWidth = true;
			}
			this.setTitle(t.title || '');
			this.body.update(t.text);
			this.autoHide = t.autoHide;
			this.dismissDelay = t.dismissDelay || this.dismissDelay;
			if (this.lastCls) {
				this.el.removeClass(this.lastCls);
				delete this.lastCls;
			}
			if (t.cls) {
				this.el.addClass(t.cls);
				this.lastCls = t.cls;
			}
			if (t.align) {
				xy = this.el.getAlignToXY(t.el, t.align);
				this.constrainPosition = false;
			} else {
				this.constrainPosition = true;
			}
		}
		Tera.QuickTip.superclass.showAt.call(this, xy);
	},

	hide : function() {
		delete this.activeTarget;
		Tera.QuickTip.superclass.hide.call(this);
	}
});

Tera.QuickTips = function() {
	var tip, locks = [];
	return {

		init : function() {
			if (!tip) {
				tip = new Tera.QuickTip( {
					elements :'header,body'
				});
			}
		},

		enable : function() {
			if (tip) {
				locks.pop();
				if (locks.length < 1) {
					tip.enable();
				}
			}
		},

		disable : function() {
			if (tip) {
				tip.disable();
			}
			locks.push(1);
		},

		isEnabled : function() {
			return tip !== undefined && !tip.disabled;
		},

		getQuickTip : function() {
			return tip;
		},

		register : function() {
			tip.register.apply(tip, arguments);
		},

		unregister : function() {
			tip.unregister.apply(tip, arguments);
		},

		tips : function() {
			tip.register.apply(tip, arguments);
		}
	}
}();

Tera.tree.TreePanel = Tera.extend(Tera.Panel, {
	rootVisible :true,
	animate :Tera.enableFx,
	lines :true,
	enableDD :false,
	hlDrop :Tera.enableFx,
	pathSeparator :"/",

	initComponent : function() {
		Tera.tree.TreePanel.superclass.initComponent.call(this);

		if (!this.eventModel) {
			this.eventModel = new Tera.tree.TreeEventModel(this);
		}

		this.nodeHash = {};

		if (this.root) {
			this.setRootNode(this.root);
		}

		this.addEvents(

		"append",

		"remove",

		"movenode",

		"insert",

		"beforeappend",

		"beforeremove",

		"beforemovenode",

		"beforeinsert",

		"beforeload",

		"load",

		"textchange",

		"beforeexpandnode",

		"beforecollapsenode",

		"expandnode",

		"disabledchange",

		"collapsenode",

		"beforeclick",

		"click",

		"checkchange",

		"dblclick",

		"contextmenu",

		"beforechildrenrendered",

		"startdrag",

		"enddrag",

		"dragdrop",

		"beforenodedrop",

		"nodedrop",

		"nodedragover");
		if (this.singleExpand) {
			this.on("beforeexpandnode", this.restrictExpand, this);
		}
	},

	proxyNodeEvent : function(ename, a1, a2, a3, a4, a5, a6) {
		if (ename == 'collapse' || ename == 'expand' || ename == 'beforecollapse' || ename == 'beforeexpand' || ename == 'move'
				|| ename == 'beforemove') {
			ename = ename + 'node';
		}

		return this.fireEvent(ename, a1, a2, a3, a4, a5, a6);
	},

	getRootNode : function() {
		return this.root;
	},

	setRootNode : function(node) {
		this.root = node;
		node.ownerTree = this;
		node.isRoot = true;
		this.registerNode(node);
		if (!this.rootVisible) {
			var uiP = node.attributes.uiProvider;
			node.ui = uiP ? new uiP(node) : new Tera.tree.RootTreeNodeUI(node);
		}
		return node;
	},

	getNodeById : function(id) {
		return this.nodeHash[id];
	},

	registerNode : function(node) {
		this.nodeHash[node.id] = node;
	},

	unregisterNode : function(node) {
		delete this.nodeHash[node.id];
	},

	toString : function() {
		return "[Tree" + (this.id ? " " + this.id : "") + "]";
	},

	restrictExpand : function(node) {
		var p = node.parentNode;
		if (p) {
			if (p.expandedChild && p.expandedChild.parentNode == p) {
				p.expandedChild.collapse();
			}
			p.expandedChild = node;
		}
	},

	getChecked : function(a, startNode) {
		startNode = startNode || this.root;
		var r = [];
		var f = function() {
			if (this.attributes.checked) {
				r.push(!a ? this : (a == 'id' ? this.id : this.attributes[a]));
			}
		}
		startNode.cascade(f);
		return r;
	},

	getEl : function() {
		return this.el;
	},

	getLoader : function() {
		return this.loader;
	},

	expandAll : function() {
		this.root.expand(true);
	},

	collapseAll : function() {
		this.root.collapse(true);
	},

	getSelectionModel : function() {
		if (!this.selModel) {
			this.selModel = new Tera.tree.DefaultSelectionModel();
		}
		return this.selModel;
	},

	expandPath : function(path, attr, callback) {
		attr = attr || "id";
		var keys = path.split(this.pathSeparator);
		var curNode = this.root;
		if (curNode.attributes[attr] != keys[1]) {
			if (callback) {
				callback(false, null);
			}
			return;
		}
		var index = 1;
		var f = function() {
			if (++index == keys.length) {
				if (callback) {
					callback(true, curNode);
				}
				return;
			}
			var c = curNode.findChild(attr, keys[index]);
			if (!c) {
				if (callback) {
					callback(false, curNode);
				}
				return;
			}
			curNode = c;
			c.expand(false, false, f);
		};
		curNode.expand(false, false, f);
	},

	selectPath : function(path, attr, callback) {
		attr = attr || "id";
		var keys = path.split(this.pathSeparator);
		var v = keys.pop();
		if (keys.length > 0) {
			var f = function(success, node) {
				if (success && node) {
					var n = node.findChild(attr, v);
					if (n) {
						n.select();
						if (callback) {
							callback(true, n);
						}
					} else if (callback) {
						callback(false, n);
					}
				} else {
					if (callback) {
						callback(false, n);
					}
				}
			};
			this.expandPath(keys.join(this.pathSeparator), attr, f);
		} else {
			this.root.select();
			if (callback) {
				callback(true, this.root);
			}
		}
	},

	getTreeEl : function() {
		return this.body;
	},

	onRender : function(ct, position) {
		Tera.tree.TreePanel.superclass.onRender.call(this, ct, position);
		this.el.addClass('x-tree');
		this.innerCt = this.body.createChild( {
			tag :"ul",
			cls :"x-tree-root-ct " + (this.useArrows ? 'x-tree-arrows' : this.lines ? "x-tree-lines" : "x-tree-no-lines")
		});
	},

	initEvents : function() {
		Tera.tree.TreePanel.superclass.initEvents.call(this);

		if (this.containerScroll) {
			Tera.dd.ScrollManager.register(this.body);
		}
		if ((this.enableDD || this.enableDrop) && !this.dropZone) {

			this.dropZone = new Tera.tree.TreeDropZone(this, this.dropConfig || {
				ddGroup :this.ddGroup || "TreeDD",
				appendOnly :this.ddAppendOnly === true
			});
		}
		if ((this.enableDD || this.enableDrag) && !this.dragZone) {

			this.dragZone = new Tera.tree.TreeDragZone(this, this.dragConfig || {
				ddGroup :this.ddGroup || "TreeDD",
				scroll :this.ddScroll
			});
		}
		this.getSelectionModel().init(this);
	},

	afterRender : function() {
		Tera.tree.TreePanel.superclass.afterRender.call(this);
		this.root.render();
		if (!this.rootVisible) {
			this.root.renderChildren();
		}
	},

	onDestroy : function() {
		if (this.rendered) {
			this.body.removeAllListeners();
			Tera.dd.ScrollManager.unregister(this.body);
			if (this.dropZone) {
				this.dropZone.unreg();
			}
			if (this.dragZone) {
				this.dragZone.unreg();
			}
		}
		this.root.destroy();
		this.nodeHash = null;
		Tera.tree.TreePanel.superclass.onDestroy.call(this);
	}

});
Tera.reg('treepanel', Tera.tree.TreePanel);
Tera.tree.TreeEventModel = function(tree) {
	this.tree = tree;
	this.tree.on('render', this.initEvents, this);
}

Tera.tree.TreeEventModel.prototype = {
	initEvents : function() {
		var el = this.tree.getTreeEl();
		el.on('click', this.delegateClick, this);
		if (this.tree.trackMouseOver !== false) {
			el.on('mouseover', this.delegateOver, this);
			el.on('mouseout', this.delegateOut, this);
		}
		el.on('dblclick', this.delegateDblClick, this);
		el.on('contextmenu', this.delegateContextMenu, this);
	},

	getNode : function(e) {
		var t;
		if (t = e.getTarget('.x-tree-node-el', 10)) {
			var id = Tera.fly(t, '_treeEvents').getAttributeNS('ext', 'tree-node-id');
			if (id) {
				return this.tree.getNodeById(id);
			}
		}
		return null;
	},

	getNodeTarget : function(e) {
		var t = e.getTarget('.x-tree-node-icon', 1);
		if (!t) {
			t = e.getTarget('.x-tree-node-el', 6);
		}
		return t;
	},

	delegateOut : function(e, t) {
		if (!this.beforeEvent(e)) {
			return;
		}
		if (e.getTarget('.x-tree-ec-icon', 1)) {
			var n = this.getNode(e);
			this.onIconOut(e, n);
			if (n == this.lastEcOver) {
				delete this.lastEcOver;
			}
		}
		if ((t = this.getNodeTarget(e)) && !e.within(t, true)) {
			this.onNodeOut(e, this.getNode(e));
		}
	},

	delegateOver : function(e, t) {
		if (!this.beforeEvent(e)) {
			return;
		}
		if (this.lastEcOver) {
			this.onIconOut(e, this.lastEcOver);
			delete this.lastEcOver;
		}
		if (e.getTarget('.x-tree-ec-icon', 1)) {
			this.lastEcOver = this.getNode(e);
			this.onIconOver(e, this.lastEcOver);
		}
		if (t = this.getNodeTarget(e)) {
			this.onNodeOver(e, this.getNode(e));
		}
	},

	delegateClick : function(e, t) {
		if (!this.beforeEvent(e)) {
			return;
		}

		if (e.getTarget('input[type=checkbox]', 1)) {
			this.onCheckboxClick(e, this.getNode(e));
		} else if (e.getTarget('.x-tree-ec-icon', 1)) {
			this.onIconClick(e, this.getNode(e));
		} else if (this.getNodeTarget(e)) {
			this.onNodeClick(e, this.getNode(e));
		}
	},

	delegateDblClick : function(e, t) {
		if (this.beforeEvent(e) && this.getNodeTarget(e)) {
			this.onNodeDblClick(e, this.getNode(e));
		}
	},

	delegateContextMenu : function(e, t) {
		if (this.beforeEvent(e) && this.getNodeTarget(e)) {
			this.onNodeContextMenu(e, this.getNode(e));
		}
	},

	onNodeClick : function(e, node) {
		node.ui.onClick(e);
	},

	onNodeOver : function(e, node) {
		node.ui.onOver(e);
	},

	onNodeOut : function(e, node) {
		node.ui.onOut(e);
	},

	onIconOver : function(e, node) {
		node.ui.addClass('x-tree-ec-over');
	},

	onIconOut : function(e, node) {
		node.ui.removeClass('x-tree-ec-over');
	},

	onIconClick : function(e, node) {
		node.ui.ecClick(e);
	},

	onCheckboxClick : function(e, node) {
		node.ui.onCheckChange(e);
	},

	onNodeDblClick : function(e, node) {
		node.ui.onDblClick(e);
	},

	onNodeContextMenu : function(e, node) {
		node.ui.onContextMenu(e);
	},

	beforeEvent : function(e) {
		if (this.disabled) {
			e.stopEvent();
			return false;
		}
		return true;
	},

	disable : function() {
		this.disabled = true;
	},

	enable : function() {
		this.disabled = false;
	}
};

Tera.tree.DefaultSelectionModel = function(config) {
	this.selNode = null;

	this.addEvents(

	"selectionchange",

	"beforeselect");

	Tera.apply(this, config);
	Tera.tree.DefaultSelectionModel.superclass.constructor.call(this);
};

Tera.extend(Tera.tree.DefaultSelectionModel, Tera.util.Observable, {
	init : function(tree) {
		this.tree = tree;
		tree.getTreeEl().on("keydown", this.onKeyDown, this);
		tree.on("click", this.onNodeClick, this);
	},

	onNodeClick : function(node, e) {
		this.select(node);
	},

	select : function(node) {
		var last = this.selNode;
		if (last != node && this.fireEvent('beforeselect', this, node, last) !== false) {
			if (last) {
				last.ui.onSelectedChange(false);
			}
			this.selNode = node;
			node.ui.onSelectedChange(true);
			this.fireEvent("selectionchange", this, node, last);
		}
		return node;
	},

	unselect : function(node) {
		if (this.selNode == node) {
			this.clearSelections();
		}
	},

	clearSelections : function() {
		var n = this.selNode;
		if (n) {
			n.ui.onSelectedChange(false);
			this.selNode = null;
			this.fireEvent("selectionchange", this, null);
		}
		return n;
	},

	getSelectedNode : function() {
		return this.selNode;
	},

	isSelected : function(node) {
		return this.selNode == node;
	},

	selectPrevious : function() {
		var s = this.selNode || this.lastSelNode;
		if (!s) {
			return null;
		}
		var ps = s.previousSibling;
		if (ps) {
			if (!ps.isExpanded() || ps.childNodes.length < 1) {
				return this.select(ps);
			} else {
				var lc = ps.lastChild;
				while (lc && lc.isExpanded() && lc.childNodes.length > 0) {
					lc = lc.lastChild;
				}
				return this.select(lc);
			}
		} else if (s.parentNode && (this.tree.rootVisible || !s.parentNode.isRoot)) {
			return this.select(s.parentNode);
		}
		return null;
	},

	selectNext : function() {
		var s = this.selNode || this.lastSelNode;
		if (!s) {
			return null;
		}
		if (s.firstChild && s.isExpanded()) {
			return this.select(s.firstChild);
		} else if (s.nextSibling) {
			return this.select(s.nextSibling);
		} else if (s.parentNode) {
			var newS = null;
			s.parentNode.bubble( function() {
				if (this.nextSibling) {
					newS = this.getOwnerTree().selModel.select(this.nextSibling);
					return false;
				}
			});
			return newS;
		}
		return null;
	},

	onKeyDown : function(e) {
		var s = this.selNode || this.lastSelNode;

		var sm = this;
		if (!s) {
			return;
		}
		var k = e.getKey();
		switch (k) {
			case e.DOWN:
				e.stopEvent();
				this.selectNext();
				break;
			case e.UP:
				e.stopEvent();
				this.selectPrevious();
				break;
			case e.RIGHT:
				e.preventDefault();
				if (s.hasChildNodes()) {
					if (!s.isExpanded()) {
						s.expand();
					} else if (s.firstChild) {
						this.select(s.firstChild, e);
					}
				}
				break;
			case e.LEFT:
				e.preventDefault();
				if (s.hasChildNodes() && s.isExpanded()) {
					s.collapse();
				} else if (s.parentNode && (this.tree.rootVisible || s.parentNode != this.tree.getRootNode())) {
					this.select(s.parentNode, e);
				}
				break;
		}
		;
	}
});

Tera.tree.MultiSelectionModel = function(config) {
	this.selNodes = [];
	this.selMap = {};
	this.addEvents(

	"selectionchange");
	Tera.apply(this, config);
	Tera.tree.MultiSelectionModel.superclass.constructor.call(this);
};

Tera.extend(Tera.tree.MultiSelectionModel, Tera.util.Observable, {
	init : function(tree) {
		this.tree = tree;
		tree.getTreeEl().on("keydown", this.onKeyDown, this);
		tree.on("click", this.onNodeClick, this);
	},

	onNodeClick : function(node, e) {
		this.select(node, e, e.ctrlKey);
	},

	select : function(node, e, keepExisting) {
		if (keepExisting !== true) {
			this.clearSelections(true);
		}
		if (this.isSelected(node)) {
			this.lastSelNode = node;
			return node;
		}
		this.selNodes.push(node);
		this.selMap[node.id] = node;
		this.lastSelNode = node;
		node.ui.onSelectedChange(true);
		this.fireEvent("selectionchange", this, this.selNodes);
		return node;
	},

	unselect : function(node) {
		if (this.selMap[node.id]) {
			node.ui.onSelectedChange(false);
			var sn = this.selNodes;
			var index = sn.indexOf(node);
			if (index != -1) {
				this.selNodes.splice(index, 1);
			}
			delete this.selMap[node.id];
			this.fireEvent("selectionchange", this, this.selNodes);
		}
	},

	clearSelections : function(suppressEvent) {
		var sn = this.selNodes;
		if (sn.length > 0) {
			for ( var i = 0, len = sn.length; i < len; i++) {
				sn[i].ui.onSelectedChange(false);
			}
			this.selNodes = [];
			this.selMap = {};
			if (suppressEvent !== true) {
				this.fireEvent("selectionchange", this, this.selNodes);
			}
		}
	},

	isSelected : function(node) {
		return this.selMap[node.id] ? true : false;
	},

	getSelectedNodes : function() {
		return this.selNodes;
	},

	onKeyDown :Tera.tree.DefaultSelectionModel.prototype.onKeyDown,

	selectNext :Tera.tree.DefaultSelectionModel.prototype.selectNext,

	selectPrevious :Tera.tree.DefaultSelectionModel.prototype.selectPrevious
});

Tera.tree.TreeNode = function(attributes) {
	attributes = attributes || {};
	if (typeof attributes == "string") {
		attributes = {
			text :attributes
		};
	}
	this.childrenRendered = false;
	this.rendered = false;
	Tera.tree.TreeNode.superclass.constructor.call(this, attributes);
	this.expanded = attributes.expanded === true;
	this.isTarget = attributes.isTarget !== false;
	this.draggable = attributes.draggable !== false && attributes.allowDrag !== false;
	this.allowChildren = attributes.allowChildren !== false && attributes.allowDrop !== false;

	this.text = attributes.text;

	this.disabled = attributes.disabled === true;

	this.addEvents(

	"textchange",

	"beforeexpand",

	"beforecollapse",

	"expand",

	"disabledchange",

	"collapse",

	"beforeclick",

	"click",

	"checkchange",

	"dblclick",

	"contextmenu",

	"beforechildrenrendered");

	var uiClass = this.attributes.uiProvider || this.defaultUI || Tera.tree.TreeNodeUI;

	this.ui = new uiClass(this);
};
Tera.extend(Tera.tree.TreeNode, Tera.data.Node, {
	preventHScroll :true,

	isExpanded : function() {
		return this.expanded;
	},

	getUI : function() {
		return this.ui;
	},

	setFirstChild : function(node) {
		var of = this.firstChild;
		Tera.tree.TreeNode.superclass.setFirstChild.call(this, node);
		if (this.childrenRendered && of && node != of) {
			of.renderIndent(true, true);
		}
		if (this.rendered) {
			this.renderIndent(true, true);
		}
	},

	setLastChild : function(node) {
		var ol = this.lastChild;
		Tera.tree.TreeNode.superclass.setLastChild.call(this, node);
		if (this.childrenRendered && ol && node != ol) {
			ol.renderIndent(true, true);
		}
		if (this.rendered) {
			this.renderIndent(true, true);
		}
	},

	appendChild : function() {
		var node = Tera.tree.TreeNode.superclass.appendChild.apply(this, arguments);
		if (node && this.childrenRendered) {
			node.render();
		}
		this.ui.updateExpandIcon();
		return node;
	},

	removeChild : function(node) {
		this.ownerTree.getSelectionModel().unselect(node);
		Tera.tree.TreeNode.superclass.removeChild.apply(this, arguments);

		if (this.childrenRendered) {
			node.ui.remove();
		}
		if (this.childNodes.length < 1) {
			this.collapse(false, false);
		} else {
			this.ui.updateExpandIcon();
		}
		if (!this.firstChild && !this.isHiddenRoot()) {
			this.childrenRendered = false;
		}
		return node;
	},

	insertBefore : function(node, refNode) {
		var newNode = Tera.tree.TreeNode.superclass.insertBefore.apply(this, arguments);
		if (newNode && refNode && this.childrenRendered) {
			node.render();
		}
		this.ui.updateExpandIcon();
		return newNode;
	},

	setText : function(text) {
		var oldText = this.text;
		this.text = text;
		this.attributes.text = text;
		if (this.rendered) {
			this.ui.onTextChange(this, text, oldText);
		}
		this.fireEvent("textchange", this, text, oldText);
	},

	select : function() {
		this.getOwnerTree().getSelectionModel().select(this);
	},

	unselect : function() {
		this.getOwnerTree().getSelectionModel().unselect(this);
	},

	isSelected : function() {
		return this.getOwnerTree().getSelectionModel().isSelected(this);
	},

	expand : function(deep, anim, callback) {
		if (!this.expanded) {
			if (this.fireEvent("beforeexpand", this, deep, anim) === false) {
				return;
			}
			if (!this.childrenRendered) {
				this.renderChildren();
			}
			this.expanded = true;
			if (!this.isHiddenRoot() && (this.getOwnerTree().animate && anim !== false) || anim) {
				this.ui.animExpand( function() {
					this.fireEvent("expand", this);
					if (typeof callback == "function") {
						callback(this);
					}
					if (deep === true) {
						this.expandChildNodes(true);
					}
				}.createDelegate(this));
				return;
			} else {
				this.ui.expand();
				this.fireEvent("expand", this);
				if (typeof callback == "function") {
					callback(this);
				}
			}
		} else {
			if (typeof callback == "function") {
				callback(this);
			}
		}
		if (deep === true) {
			this.expandChildNodes(true);
		}
	},

	isHiddenRoot : function() {
		return this.isRoot && !this.getOwnerTree().rootVisible;
	},

	collapse : function(deep, anim) {
		if (this.expanded && !this.isHiddenRoot()) {
			if (this.fireEvent("beforecollapse", this, deep, anim) === false) {
				return;
			}
			this.expanded = false;
			if ((this.getOwnerTree().animate && anim !== false) || anim) {
				this.ui.animCollapse( function() {
					this.fireEvent("collapse", this);
					if (deep === true) {
						this.collapseChildNodes(true);
					}
				}.createDelegate(this));
				return;
			} else {
				this.ui.collapse();
				this.fireEvent("collapse", this);
			}
		}
		if (deep === true) {
			var cs = this.childNodes;
			for ( var i = 0, len = cs.length; i < len; i++) {
				cs[i].collapse(true, false);
			}
		}
	},

	delayedExpand : function(delay) {
		if (!this.expandProcId) {
			this.expandProcId = this.expand.defer(delay, this);
		}
	},

	cancelExpand : function() {
		if (this.expandProcId) {
			clearTimeout(this.expandProcId);
		}
		this.expandProcId = false;
	},

	toggle : function() {
		if (this.expanded) {
			this.collapse();
		} else {
			this.expand();
		}
	},

	ensureVisible : function(callback) {
		var tree = this.getOwnerTree();
		tree.expandPath(this.parentNode.getPath(), false, function() {
			var node = tree.getNodeById(this.id);
			tree.getTreeEl().scrollChildIntoView(node.ui.anchor);
			Tera.callback(callback);
		}.createDelegate(this));
	},

	expandChildNodes : function(deep) {
		var cs = this.childNodes;
		for ( var i = 0, len = cs.length; i < len; i++) {
			cs[i].expand(deep);
		}
	},

	collapseChildNodes : function(deep) {
		var cs = this.childNodes;
		for ( var i = 0, len = cs.length; i < len; i++) {
			cs[i].collapse(deep);
		}
	},

	disable : function() {
		this.disabled = true;
		this.unselect();
		if (this.rendered && this.ui.onDisableChange) {
			this.ui.onDisableChange(this, true);
		}
		this.fireEvent("disabledchange", this, true);
	},

	enable : function() {
		this.disabled = false;
		if (this.rendered && this.ui.onDisableChange) {
			this.ui.onDisableChange(this, false);
		}
		this.fireEvent("disabledchange", this, false);
	},

	renderChildren : function(suppressEvent) {
		if (suppressEvent !== false) {
			this.fireEvent("beforechildrenrendered", this);
		}
		var cs = this.childNodes;
		for ( var i = 0, len = cs.length; i < len; i++) {
			cs[i].render(true);
		}
		this.childrenRendered = true;
	},

	sort : function(fn, scope) {
		Tera.tree.TreeNode.superclass.sort.apply(this, arguments);
		if (this.childrenRendered) {
			var cs = this.childNodes;
			for ( var i = 0, len = cs.length; i < len; i++) {
				cs[i].render(true);
			}
		}
	},

	render : function(bulkRender) {
		this.ui.render(bulkRender);
		if (!this.rendered) {

			this.getOwnerTree().registerNode(this);
			this.rendered = true;
			if (this.expanded) {
				this.expanded = false;
				this.expand(false, false);
			}
		}
	},

	renderIndent : function(deep, refresh) {
		if (refresh) {
			this.ui.childIndent = null;
		}
		this.ui.renderIndent();
		if (deep === true && this.childrenRendered) {
			var cs = this.childNodes;
			for ( var i = 0, len = cs.length; i < len; i++) {
				cs[i].renderIndent(true, refresh);
			}
		}
	},

	beginUpdate : function() {
		this.childrenRendered = false;
	},

	endUpdate : function() {
		if (this.expanded && this.rendered) {
			this.renderChildren();
		}
	},

	destroy : function() {
		for ( var i = 0, l = this.childNodes.length; i < l; i++) {
			this.childNodes[i].destroy();
		}
		this.childNodes = null;
		if (this.ui.destroy) {
			this.ui.destroy();
		}
	}
});

Tera.tree.AsyncTreeNode = function(config) {
	this.loaded = false;
	this.loading = false;
	Tera.tree.AsyncTreeNode.superclass.constructor.apply(this, arguments);

	this.addEvents('beforeload', 'load');

};
Tera.extend(Tera.tree.AsyncTreeNode, Tera.tree.TreeNode, {
	expand : function(deep, anim, callback) {
		if (this.loading) {
			var timer;
			var f = function() {
				if (!this.loading) {
					clearInterval(timer);
					this.expand(deep, anim, callback);
				}
			}.createDelegate(this);
			timer = setInterval(f, 200);
			return;
		}
		if (!this.loaded) {
			if (this.fireEvent("beforeload", this) === false) {
				return;
			}
			this.loading = true;
			this.ui.beforeLoad(this);
			var loader = this.loader || this.attributes.loader || this.getOwnerTree().getLoader();
			if (loader) {
				loader.load(this, this.loadComplete.createDelegate(this, [ deep, anim, callback ]));
				return;
			}
		}
		Tera.tree.AsyncTreeNode.superclass.expand.call(this, deep, anim, callback);
	},

	isLoading : function() {
		return this.loading;
	},

	loadComplete : function(deep, anim, callback) {
		this.loading = false;
		this.loaded = true;
		this.ui.afterLoad(this);
		this.fireEvent("load", this);
		this.expand(deep, anim, callback);
	},

	isLoaded : function() {
		return this.loaded;
	},

	hasChildNodes : function() {
		if (!this.isLeaf() && !this.loaded) {
			return true;
		} else {
			return Tera.tree.AsyncTreeNode.superclass.hasChildNodes.call(this);
		}
	},

	reload : function(callback) {
		this.collapse(false, false);
		while (this.firstChild) {
			this.removeChild(this.firstChild);
		}
		this.childrenRendered = false;
		this.loaded = false;
		if (this.isHiddenRoot()) {
			this.expanded = false;
		}
		this.expand(false, false, callback);
	}
});

Tera.tree.TreeNodeUI = function(node) {
	this.node = node;
	this.rendered = false;
	this.animating = false;
	this.wasLeaf = true;
	this.ecc = 'x-tree-ec-icon x-tree-elbow';
	this.emptyIcon = Tera.BLANK_IMAGE_URL;
};

Tera.tree.TreeNodeUI.prototype = {

	removeChild : function(node) {
		if (this.rendered) {
			this.ctNode.removeChild(node.ui.getEl());
		}
	},

	beforeLoad : function() {
		this.addClass("x-tree-node-loading");
	},

	afterLoad : function() {
		this.removeClass("x-tree-node-loading");
	},

	onTextChange : function(node, text, oldText) {
		if (this.rendered) {
			this.textNode.innerHTML = text;
		}
	},

	onDisableChange : function(node, state) {
		this.disabled = state;
		if (this.checkbox) {
			this.checkbox.disabled = state;
		}
		if (state) {
			this.addClass("x-tree-node-disabled");
		} else {
			this.removeClass("x-tree-node-disabled");
		}
	},

	onSelectedChange : function(state) {
		if (state) {
			this.focus();
			this.addClass("x-tree-selected");
		} else {

			this.removeClass("x-tree-selected");
		}
	},

	onMove : function(tree, node, oldParent, newParent, index, refNode) {
		this.childIndent = null;
		if (this.rendered) {
			var targetNode = newParent.ui.getContainer();
			if (!targetNode) {
				this.holder = document.createElement("div");
				this.holder.appendChild(this.wrap);
				return;
			}
			var insertBefore = refNode ? refNode.ui.getEl() : null;
			if (insertBefore) {
				targetNode.insertBefore(this.wrap, insertBefore);
			} else {
				targetNode.appendChild(this.wrap);
			}
			this.node.renderIndent(true);
		}
	},

	addClass : function(cls) {
		if (this.elNode) {
			Tera.fly(this.elNode).addClass(cls);
		}
	},

	removeClass : function(cls) {
		if (this.elNode) {
			Tera.fly(this.elNode).removeClass(cls);
		}
	},

	remove : function() {
		if (this.rendered) {
			this.holder = document.createElement("div");
			this.holder.appendChild(this.wrap);
		}
	},

	fireEvent : function() {
		return this.node.fireEvent.apply(this.node, arguments);
	},

	initEvents : function() {
		this.node.on("move", this.onMove, this);

		if (this.node.disabled) {
			this.addClass("x-tree-node-disabled");
			if (this.checkbox) {
				this.checkbox.disabled = true;
			}
		}
		if (this.node.hidden) {
			this.hide();
		}
		var ot = this.node.getOwnerTree();
		var dd = ot.enableDD || ot.enableDrag || ot.enableDrop;
		if (dd && (!this.node.isRoot || ot.rootVisible)) {
			Tera.dd.Registry.register(this.elNode, {
				node :this.node,
				handles :this.getDDHandles(),
				isHandle :false
			});
		}
	},

	getDDHandles : function() {
		return [ this.iconNode, this.textNode, this.elNode ];
	},

	hide : function() {
		this.node.hidden = true;
		if (this.wrap) {
			this.wrap.style.display = "none";
		}
	},

	show : function() {
		this.node.hidden = false;
		if (this.wrap) {
			this.wrap.style.display = "";
		}
	},

	onContextMenu : function(e) {
		if (this.node.hasListener("contextmenu") || this.node.getOwnerTree().hasListener("contextmenu")) {
			e.preventDefault();
			this.focus();
			this.fireEvent("contextmenu", this.node, e);
		}
	},

	onClick : function(e) {
		if (this.dropping) {
			e.stopEvent();
			return;
		}
		if (this.fireEvent("beforeclick", this.node, e) !== false) {
			var a = e.getTarget('a');
			if (!this.disabled && this.node.attributes.href && a) {
				this.fireEvent("click", this.node, e);
				return;
			} else if (a && e.ctrlKey) {
				e.stopEvent();
			}
			e.preventDefault();
			if (this.disabled) {
				return;
			}

			if (this.node.attributes.singleClickExpand && !this.animating && this.node.hasChildNodes()) {
				this.node.toggle();
			}

			this.fireEvent("click", this.node, e);
		} else {
			e.stopEvent();
		}
	},

	onDblClick : function(e) {
		e.preventDefault();
		if (this.disabled) {
			return;
		}
		if (this.checkbox) {
			this.toggleCheck();
		}
		if (!this.animating && this.node.hasChildNodes()) {
			this.node.toggle();
		}
		this.fireEvent("dblclick", this.node, e);
	},

	onOver : function(e) {
		this.addClass('x-tree-node-over');
	},

	onOut : function(e) {
		this.removeClass('x-tree-node-over');
	},

	onCheckChange : function() {
		var checked = this.checkbox.checked;

		this.checkbox.defaultChecked = checked;
		this.node.attributes.checked = checked;
		this.fireEvent('checkchange', this.node, checked);
	},

	ecClick : function(e) {
		if (!this.animating && (this.node.hasChildNodes() || this.node.attributes.expandable)) {
			this.node.toggle();
		}
	},

	startDrop : function() {
		this.dropping = true;
	},

	endDrop : function() {
		setTimeout( function() {
			this.dropping = false;
		}.createDelegate(this), 50);
	},

	expand : function() {
		this.updateExpandIcon();
		this.ctNode.style.display = "";
	},

	focus : function() {
		if (!this.node.preventHScroll) {
			try {
				this.anchor.focus();
			} catch (e) {
			}
		} else if (!Tera.isIE) {
			try {
				var noscroll = this.node.getOwnerTree().getTreeEl().dom;
				var l = noscroll.scrollLeft;
				this.anchor.focus();
				noscroll.scrollLeft = l;
			} catch (e) {
			}
		}
	},

	toggleCheck : function(value) {
		var cb = this.checkbox;
		if (cb) {
			cb.checked = (value === undefined ? !cb.checked : value);
			this.onCheckChange();
		}
	},

	blur : function() {
		try {
			this.anchor.blur();
		} catch (e) {
		}
	},

	animExpand : function(callback) {
		var ct = Tera.get(this.ctNode);
		ct.stopFx();
		if (!this.node.hasChildNodes()) {
			this.updateExpandIcon();
			this.ctNode.style.display = "";
			Tera.callback(callback);
			return;
		}
		this.animating = true;
		this.updateExpandIcon();

		ct.slideIn('t', {
			callback : function() {
				this.animating = false;
				Tera.callback(callback);
			},
			scope :this,
			duration :this.node.ownerTree.duration || .25
		});
	},

	highlight : function() {
		var tree = this.node.getOwnerTree();
		Tera.fly(this.wrap).highlight(tree.hlColor || "C3DAF9", {
			endColor :tree.hlBaseColor
		});
	},

	collapse : function() {
		this.updateExpandIcon();
		this.ctNode.style.display = "none";
	},

	animCollapse : function(callback) {
		var ct = Tera.get(this.ctNode);
		ct.enableDisplayMode('block');
		ct.stopFx();

		this.animating = true;
		this.updateExpandIcon();

		ct.slideOut('t', {
			callback : function() {
				this.animating = false;
				Tera.callback(callback);
			},
			scope :this,
			duration :this.node.ownerTree.duration || .25
		});
	},

	getContainer : function() {
		return this.ctNode;
	},

	getEl : function() {
		return this.wrap;
	},

	appendDDGhost : function(ghostNode) {
		ghostNode.appendChild(this.elNode.cloneNode(true));
	},

	getDDRepairXY : function() {
		return Tera.lib.Dom.getXY(this.iconNode);
	},

	onRender : function() {
		this.render();
	},

	render : function(bulkRender) {
		var n = this.node, a = n.attributes;
		var targetNode = n.parentNode ? n.parentNode.ui.getContainer() : n.ownerTree.innerCt.dom;

		if (!this.rendered) {
			this.rendered = true;

			this.renderElements(n, a, targetNode, bulkRender);

			if (a.qtip) {
				if (this.textNode.setAttributeNS) {
					this.textNode.setAttributeNS("ext", "qtip", a.qtip);
					if (a.qtipTitle) {
						this.textNode.setAttributeNS("ext", "qtitle", a.qtipTitle);
					}
				} else {
					this.textNode.setAttribute("ext:qtip", a.qtip);
					if (a.qtipTitle) {
						this.textNode.setAttribute("ext:qtitle", a.qtipTitle);
					}
				}
			} else if (a.qtipCfg) {
				a.qtipCfg.target = Tera.id(this.textNode);
				Tera.QuickTips.register(a.qtipCfg);
			}
			this.initEvents();
			if (!this.node.expanded) {
				this.updateExpandIcon(true);
			}
		} else {
			if (bulkRender === true) {
				targetNode.appendChild(this.wrap);
			}
		}
	},

	renderElements : function(n, a, targetNode, bulkRender) {

		this.indentMarkup = n.parentNode ? n.parentNode.ui.getChildIndent() : '';

		var cb = typeof a.checked == 'boolean';

		var href = a.href ? a.href : Tera.isGecko ? "" : "#";
		var buf = [ '<li class="x-tree-node"><div ext:tree-node-id="', n.id, '" class="x-tree-node-el x-tree-node-leaf x-unselectable ', a.cls,
				'" unselectable="on">', '<span class="x-tree-node-indent">', this.indentMarkup, "</span>", '<img src="', this.emptyIcon,
				'" class="x-tree-ec-icon x-tree-elbow" />', '<img src="', a.icon || this.emptyIcon, '" class="x-tree-node-icon',
				(a.icon ? " x-tree-node-inline-icon" : ""), (a.iconCls ? " " + a.iconCls : ""), '" unselectable="on" />',
				cb ? ('<input class="x-tree-node-cb" type="checkbox" ' + (a.checked ? 'checked="checked" />' : '/>')) : '',
				'<a hidefocus="on" class="x-tree-node-anchor" href="', href, '" tabIndex="1" ', a.hrefTarget ? ' target="' + a.hrefTarget + '"' : "",
				'><span unselectable="on">', n.text, "</span></a></div>", '<ul class="x-tree-node-ct" style="display:none;"></ul>', "</li>" ]
				.join('');

		var nel;
		if (bulkRender !== true && n.nextSibling && (nel = n.nextSibling.ui.getEl())) {
			this.wrap = Tera.DomHelper.insertHtml("beforeBegin", nel, buf);
		} else {
			this.wrap = Tera.DomHelper.insertHtml("beforeEnd", targetNode, buf);
		}

		this.elNode = this.wrap.childNodes[0];
		this.ctNode = this.wrap.childNodes[1];
		var cs = this.elNode.childNodes;
		this.indentNode = cs[0];
		this.ecNode = cs[1];
		this.iconNode = cs[2];
		var index = 3;
		if (cb) {
			this.checkbox = cs[3];

			this.checkbox.defaultChecked = this.checkbox.checked;
			index++;
		}
		this.anchor = cs[index];
		this.textNode = cs[index].firstChild;
	},

	getAnchor : function() {
		return this.anchor;
	},

	getTextEl : function() {
		return this.textNode;
	},

	getIconEl : function() {
		return this.iconNode;
	},

	isChecked : function() {
		return this.checkbox ? this.checkbox.checked : false;
	},

	updateExpandIcon : function() {
		if (this.rendered) {
			var n = this.node, c1, c2;
			var cls = n.isLast() ? "x-tree-elbow-end" : "x-tree-elbow";
			var hasChild = n.hasChildNodes();
			if (hasChild || n.attributes.expandable) {
				if (n.expanded) {
					cls += "-minus";
					c1 = "x-tree-node-collapsed";
					c2 = "x-tree-node-expanded";
				} else {
					cls += "-plus";
					c1 = "x-tree-node-expanded";
					c2 = "x-tree-node-collapsed";
				}
				if (this.wasLeaf) {
					this.removeClass("x-tree-node-leaf");
					this.wasLeaf = false;
				}
				if (this.c1 != c1 || this.c2 != c2) {
					Tera.fly(this.elNode).replaceClass(c1, c2);
					this.c1 = c1;
					this.c2 = c2;
				}
			} else {
				if (!this.wasLeaf) {
					Tera.fly(this.elNode).replaceClass("x-tree-node-expanded", "x-tree-node-leaf");
					delete this.c1;
					delete this.c2;
					this.wasLeaf = true;
				}
			}
			var ecc = "x-tree-ec-icon " + cls;
			if (this.ecc != ecc) {
				this.ecNode.className = ecc;
				this.ecc = ecc;
			}
		}
	},

	getChildIndent : function() {
		if (!this.childIndent) {
			var buf = [];
			var p = this.node;
			while (p) {
				if (!p.isRoot || (p.isRoot && p.ownerTree.rootVisible)) {
					if (!p.isLast()) {
						buf.unshift('<img src="' + this.emptyIcon + '" class="x-tree-elbow-line" />');
					} else {
						buf.unshift('<img src="' + this.emptyIcon + '" class="x-tree-icon" />');
					}
				}
				p = p.parentNode;
			}
			this.childIndent = buf.join("");
		}
		return this.childIndent;
	},

	renderIndent : function() {
		if (this.rendered) {
			var indent = "";
			var p = this.node.parentNode;
			if (p) {
				indent = p.ui.getChildIndent();
			}
			if (this.indentMarkup != indent) {
				this.indentNode.innerHTML = indent;
				this.indentMarkup = indent;
			}
			this.updateExpandIcon();
		}
	},

	destroy : function() {
		if (this.elNode) {
			Tera.dd.Registry.unregister(this.elNode.id);
		}
		delete this.elNode;
		delete this.ctNode;
		delete this.indentNode;
		delete this.ecNode;
		delete this.iconNode;
		delete this.checkbox;
		delete this.anchor;
		delete this.textNode;
		Tera.removeNode(this.ctNode);
	}
};

Tera.tree.RootTreeNodeUI = Tera.extend(Tera.tree.TreeNodeUI, {

	render : function() {
		if (!this.rendered) {
			var targetNode = this.node.ownerTree.innerCt.dom;
			this.node.expanded = true;
			targetNode.innerHTML = '<div class="x-tree-root-node"></div>';
			this.wrap = this.ctNode = targetNode.firstChild;
		}
	},
	collapse :Tera.emptyFn,
	expand :Tera.emptyFn
});

Tera.tree.TreeLoader = function(config) {
	this.baseParams = {};
	Tera.apply(this, config);

	this.addEvents(

	"beforeload",

	"load",

	"loadexception");

	Tera.tree.TreeLoader.superclass.constructor.call(this);
};

Tera.extend(Tera.tree.TreeLoader, Tera.util.Observable, {

	uiProviders : {},

	clearOnLoad :true,

	load : function(node, callback) {
		if (this.clearOnLoad) {
			while (node.firstChild) {
				node.removeChild(node.firstChild);
			}
		}
		if (this.doPreload(node)) {
			if (typeof callback == "function") {
				callback();
			}
		} else if (this.dataUrl || this.url) {
			this.requestData(node, callback);
		}
	},

	doPreload : function(node) {
		if (node.attributes.children) {
			if (node.childNodes.length < 1) {
				var cs = node.attributes.children;
				node.beginUpdate();
				for ( var i = 0, len = cs.length; i < len; i++) {
					var cn = node.appendChild(this.createNode(cs[i]));
					if (this.preloadChildren) {
						this.doPreload(cn);
					}
				}
				node.endUpdate();
			}
			return true;
		} else {
			return false;
		}
	},

	getParams : function(node) {
		var buf = [], bp = this.baseParams;
		for ( var key in bp) {
			if (typeof bp[key] != "function") {
				buf.push(encodeURIComponent(key), "=", encodeURIComponent(bp[key]), "&");
			}
		}
		buf.push("node=", encodeURIComponent(node.id));
		return buf.join("");
	},

	requestData : function(node, callback) {
		if (this.fireEvent("beforeload", this, node, callback) !== false) {
			this.transId = Tera.Ajax.request( {
				method :this.requestMethod,
				url :this.dataUrl || this.url,
				success :this.handleResponse,
				failure :this.handleFailure,
				scope :this,
				argument : {
					callback :callback,
					node :node
				},
				params :this.getParams(node)
			});
		} else {

			if (typeof callback == "function") {
				callback();
			}
		}
	},

	isLoading : function() {
		return this.transId ? true : false;
	},

	abort : function() {
		if (this.isLoading()) {
			Tera.Ajax.abort(this.transId);
		}
	},

	createNode : function(attr) {

		if (this.baseAttrs) {
			Tera.applyIf(attr, this.baseAttrs);
		}
		if (this.applyLoader !== false) {
			attr.loader = this;
		}
		if (typeof attr.uiProvider == 'string') {
			attr.uiProvider = this.uiProviders[attr.uiProvider] || eval(attr.uiProvider);
		}
		return (attr.leaf ? new Tera.tree.TreeNode(attr) : new Tera.tree.AsyncTreeNode(attr));
	},

	processResponse : function(response, node, callback) {
		var json = response.responseText;
		try {
			var o = eval("(" + json + ")");
			node.beginUpdate();
			for ( var i = 0, len = o.length; i < len; i++) {
				var n = this.createNode(o[i]);
				if (n) {
					node.appendChild(n);
				}
			}
			node.endUpdate();
			if (typeof callback == "function") {
				callback(this, node);
			}
		} catch (e) {
			this.handleFailure(response);
		}
	},

	handleResponse : function(response) {
		this.transId = false;
		var a = response.argument;
		this.processResponse(response, a.node, a.callback);
		this.fireEvent("load", this, a.node, response);
	},

	handleFailure : function(response) {
		this.transId = false;
		var a = response.argument;
		this.fireEvent("loadexception", this, a.node, response);
		if (typeof a.callback == "function") {
			a.callback(this, a.node);
		}
	}
});

Tera.tree.TreeFilter = function(tree, config) {
	this.tree = tree;
	this.filtered = {};
	Tera.apply(this, config);
};

Tera.tree.TreeFilter.prototype = {
	clearBlank :false,
	reverse :false,
	autoClear :false,
	remove :false,

	filter : function(value, attr, startNode) {
		attr = attr || "text";
		var f;
		if (typeof value == "string") {
			var vlen = value.length;

			if (vlen == 0 && this.clearBlank) {
				this.clear();
				return;
			}
			value = value.toLowerCase();
			f = function(n) {
				return n.attributes[attr].substr(0, vlen).toLowerCase() == value;
			};
		} else if (value.exec) {
			f = function(n) {
				return value.test(n.attributes[attr]);
			};
		} else {
			throw 'Illegal filter type, must be string or regex';
		}
		this.filterBy(f, null, startNode);
	},

	filterBy : function(fn, scope, startNode) {
		startNode = startNode || this.tree.root;
		if (this.autoClear) {
			this.clear();
		}
		var af = this.filtered, rv = this.reverse;
		var f = function(n) {
			if (n == startNode) {
				return true;
			}
			if (af[n.id]) {
				return false;
			}
			var m = fn.call(scope || n, n);
			if (!m || rv) {
				af[n.id] = n;
				n.ui.hide();
				return false;
			}
			return true;
		};
		startNode.cascade(f);
		if (this.remove) {
			for ( var id in af) {
				if (typeof id != "function") {
					var n = af[id];
					if (n && n.parentNode) {
						n.parentNode.removeChild(n);
					}
				}
			}
		}
	},

	clear : function() {
		var t = this.tree;
		var af = this.filtered;
		for ( var id in af) {
			if (typeof id != "function") {
				var n = af[id];
				if (n) {
					n.ui.show();
				}
			}
		}
		this.filtered = {};
	}
};

Tera.tree.TreeSorter = function(tree, config) {

	Tera.apply(this, config);
	tree.on("beforechildrenrendered", this.doSort, this);
	tree.on("append", this.updateSort, this);
	tree.on("insert", this.updateSort, this);
	tree.on("textchange", this.updateSortParent, this);

	var dsc = this.dir && this.dir.toLowerCase() == "desc";
	var p = this.property || "text";
	var sortType = this.sortType;
	var fs = this.folderSort;
	var cs = this.caseSensitive === true;
	var leafAttr = this.leafAttr || 'leaf';

	this.sortFn = function(n1, n2) {
		if (fs) {
			if (n1.attributes[leafAttr] && !n2.attributes[leafAttr]) {
				return 1;
			}
			if (!n1.attributes[leafAttr] && n2.attributes[leafAttr]) {
				return -1;
			}
		}
		var v1 = sortType ? sortType(n1) : (cs ? n1.attributes[p] : n1.attributes[p].toUpperCase());
		var v2 = sortType ? sortType(n2) : (cs ? n2.attributes[p] : n2.attributes[p].toUpperCase());
		if (v1 < v2) {
			return dsc ? +1 : -1;
		} else if (v1 > v2) {
			return dsc ? -1 : +1;
		} else {
			return 0;
		}
	};
};

Tera.tree.TreeSorter.prototype = {
	doSort : function(node) {
		node.sort(this.sortFn);
	},

	compareNodes : function(n1, n2) {
		return (n1.text.toUpperCase() > n2.text.toUpperCase() ? 1 : -1);
	},

	updateSort : function(tree, node) {
		if (node.childrenRendered) {
			this.doSort.defer(1, this, [ node ]);
		}
	},

	updateSortParent : function(node) {
		var p = node.parentNode;
		if (p && p.childrenRendered) {
			this.doSort.defer(1, this, [ p ]);
		}
	}
};

if (Tera.dd.DropZone) {

	Tera.tree.TreeDropZone = function(tree, config) {

		this.allowParentInsert = false;

		this.allowContainerDrop = false;

		this.appendOnly = false;
		Tera.tree.TreeDropZone.superclass.constructor.call(this, tree.innerCt, config);

		this.tree = tree;

		this.dragOverData = {};

		this.lastInsertClass = "x-tree-no-status";
	};

	Tera.extend(Tera.tree.TreeDropZone, Tera.dd.DropZone, {

		ddGroup :"TreeDD",

		expandDelay :1000,

		expandNode : function(node) {
			if (node.hasChildNodes() && !node.isExpanded()) {
				node.expand(false, null, this.triggerCacheRefresh.createDelegate(this));
			}
		},

		queueExpand : function(node) {
			this.expandProcId = this.expandNode.defer(this.expandDelay, this, [ node ]);
		},

		cancelExpand : function() {
			if (this.expandProcId) {
				clearTimeout(this.expandProcId);
				this.expandProcId = false;
			}
		},

		isValidDropPoint : function(n, pt, dd, e, data) {
			if (!n || !data) {
				return false;
			}
			var targetNode = n.node;
			var dropNode = data.node;

			if (!(targetNode && targetNode.isTarget && pt)) {
				return false;
			}
			if (pt == "append" && targetNode.allowChildren === false) {
				return false;
			}
			if ((pt == "above" || pt == "below") && (targetNode.parentNode && targetNode.parentNode.allowChildren === false)) {
				return false;
			}
			if (dropNode && (targetNode == dropNode || dropNode.contains(targetNode))) {
				return false;
			}

			var overEvent = this.dragOverData;
			overEvent.tree = this.tree;
			overEvent.target = targetNode;
			overEvent.data = data;
			overEvent.point = pt;
			overEvent.source = dd;
			overEvent.rawEvent = e;
			overEvent.dropNode = dropNode;
			overEvent.cancel = false;
			var result = this.tree.fireEvent("nodedragover", overEvent);
			return overEvent.cancel === false && result !== false;
		},

		getDropPoint : function(e, n, dd) {
			var tn = n.node;
			if (tn.isRoot) {
				return tn.allowChildren !== false ? "append" : false;
			}
			var dragEl = n.ddel;
			var t = Tera.lib.Dom.getY(dragEl), b = t + dragEl.offsetHeight;
			var y = Tera.lib.Event.getPageY(e);
			var noAppend = tn.allowChildren === false || tn.isLeaf();
			if (this.appendOnly || tn.parentNode.allowChildren === false) {
				return noAppend ? false : "append";
			}
			var noBelow = false;
			if (!this.allowParentInsert) {
				noBelow = tn.hasChildNodes() && tn.isExpanded();
			}
			var q = (b - t) / (noAppend ? 2 : 3);
			if (y >= t && y < (t + q)) {
				return "above";
			} else if (!noBelow && (noAppend || y >= b - q && y <= b)) {
				return "below";
			} else {
				return "append";
			}
		},

		onNodeEnter : function(n, dd, e, data) {
			this.cancelExpand();
		},

		onNodeOver : function(n, dd, e, data) {
			var pt = this.getDropPoint(e, n, dd);
			var node = n.node;

			if (!this.expandProcId && pt == "append" && node.hasChildNodes() && !n.node.isExpanded()) {
				this.queueExpand(node);
			} else if (pt != "append") {
				this.cancelExpand();
			}

			var returnCls = this.dropNotAllowed;
			if (this.isValidDropPoint(n, pt, dd, e, data)) {
				if (pt) {
					var el = n.ddel;
					var cls;
					if (pt == "above") {
						returnCls = n.node.isFirst() ? "x-tree-drop-ok-above" : "x-tree-drop-ok-between";
						cls = "x-tree-drag-insert-above";
					} else if (pt == "below") {
						returnCls = n.node.isLast() ? "x-tree-drop-ok-below" : "x-tree-drop-ok-between";
						cls = "x-tree-drag-insert-below";
					} else {
						returnCls = "x-tree-drop-ok-append";
						cls = "x-tree-drag-append";
					}
					if (this.lastInsertClass != cls) {
						Tera.fly(el).replaceClass(this.lastInsertClass, cls);
						this.lastInsertClass = cls;
					}
				}
			}
			return returnCls;
		},

		onNodeOut : function(n, dd, e, data) {
			this.cancelExpand();
			this.removeDropIndicators(n);
		},

		onNodeDrop : function(n, dd, e, data) {
			var point = this.getDropPoint(e, n, dd);
			var targetNode = n.node;
			targetNode.ui.startDrop();
			if (!this.isValidDropPoint(n, point, dd, e, data)) {
				targetNode.ui.endDrop();
				return false;
			}

			var dropNode = data.node || (dd.getTreeNode ? dd.getTreeNode(data, targetNode, point, e) : null);
			var dropEvent = {
				tree :this.tree,
				target :targetNode,
				data :data,
				point :point,
				source :dd,
				rawEvent :e,
				dropNode :dropNode,
				cancel :!dropNode,
				dropStatus :false
			};
			var retval = this.tree.fireEvent("beforenodedrop", dropEvent);
			if (retval === false || dropEvent.cancel === true || !dropEvent.dropNode) {
				targetNode.ui.endDrop();
				return dropEvent.dropStatus;
			}

			targetNode = dropEvent.target;
			if (point == "append" && !targetNode.isExpanded()) {
				targetNode.expand(false, null, function() {
					this.completeDrop(dropEvent);
				}.createDelegate(this));
			} else {
				this.completeDrop(dropEvent);
			}
			return true;
		},

		completeDrop : function(de) {
			var ns = de.dropNode, p = de.point, t = de.target;
			if (!Tera.isArray(ns)) {
				ns = [ ns ];
			}
			var n;
			for ( var i = 0, len = ns.length; i < len; i++) {
				n = ns[i];
				if (p == "above") {
					t.parentNode.insertBefore(n, t);
				} else if (p == "below") {
					t.parentNode.insertBefore(n, t.nextSibling);
				} else {
					t.appendChild(n);
				}
			}
			n.ui.focus();
			if (this.tree.hlDrop) {
				n.ui.highlight();
			}
			t.ui.endDrop();
			this.tree.fireEvent("nodedrop", de);
		},

		afterNodeMoved : function(dd, data, e, targetNode, dropNode) {
			if (this.tree.hlDrop) {
				dropNode.ui.focus();
				dropNode.ui.highlight();
			}
			this.tree.fireEvent("nodedrop", this.tree, targetNode, data, dd, e);
		},

		getTree : function() {
			return this.tree;
		},

		removeDropIndicators : function(n) {
			if (n && n.ddel) {
				var el = n.ddel;
				Tera.fly(el).removeClass( [ "x-tree-drag-insert-above", "x-tree-drag-insert-below", "x-tree-drag-append" ]);
				this.lastInsertClass = "_noclass";
			}
		},

		beforeDragDrop : function(target, e, id) {
			this.cancelExpand();
			return true;
		},

		afterRepair : function(data) {
			if (data && Tera.enableFx) {
				data.node.ui.highlight();
			}
			this.hideProxy();
		}
	});

}

if (Tera.dd.DragZone) {
	Tera.tree.TreeDragZone = function(tree, config) {
		Tera.tree.TreeDragZone.superclass.constructor.call(this, tree.getTreeEl(), config);

		this.tree = tree;
	};

	Tera.extend(Tera.tree.TreeDragZone, Tera.dd.DragZone, {

		ddGroup :"TreeDD",

		onBeforeDrag : function(data, e) {
			var n = data.node;
			return n && n.draggable && !n.disabled;
		},

		onInitDrag : function(e) {
			var data = this.dragData;
			this.tree.getSelectionModel().select(data.node);
			this.tree.eventModel.disable();
			this.proxy.update("");
			data.node.ui.appendDDGhost(this.proxy.ghost.dom);
			this.tree.fireEvent("startdrag", this.tree, data.node, e);
		},

		getRepairXY : function(e, data) {
			return data.node.ui.getDDRepairXY();
		},

		onEndDrag : function(data, e) {
			this.tree.eventModel.enable.defer(100, this.tree.eventModel);
			this.tree.fireEvent("enddrag", this.tree, data.node, e);
		},

		onValidDrop : function(dd, e, id) {
			this.tree.fireEvent("dragdrop", this.tree, this.dragData.node, dd, e);
			this.hideProxy();
		},

		beforeInvalidDrop : function(e, id) {

			var sm = this.tree.getSelectionModel();
			sm.clearSelections();
			sm.select(this.dragData.node);
		}
	});
}

Tera.tree.TreeEditor = function(tree, fc, config) {
	fc = fc || {};
	var field = fc.events ? fc : new Tera.form.TextField(fc);
	Tera.tree.TreeEditor.superclass.constructor.call(this, field, config);

	this.tree = tree;

	if (!tree.rendered) {
		tree.on('render', this.initEditor, this);
	} else {
		this.initEditor(tree);
	}
};

Tera.extend(Tera.tree.TreeEditor, Tera.Editor, {

	alignment :"l-l",
	autoSize :false,

	hideEl :false,

	cls :"x-small-editor x-tree-editor",

	shim :false,
	shadow :"frame",

	maxWidth :250,

	editDelay :350,

	initEditor : function(tree) {
		tree.on('beforeclick', this.beforeNodeClick, this);
		tree.on('dblclick', this.onNodeDblClick, this);
		this.on('complete', this.updateNode, this);
		this.on('beforestartedit', this.fitToTree, this);
		this.on('startedit', this.bindScroll, this, {
			delay :10
		});
		this.on('specialkey', this.onSpecialKey, this);
	},

	fitToTree : function(ed, el) {
		var td = this.tree.getTreeEl().dom, nd = el.dom;
		if (td.scrollLeft > nd.offsetLeft) {
			td.scrollLeft = nd.offsetLeft;
		}
		var w = Math.min(this.maxWidth, (td.clientWidth > 20 ? td.clientWidth : td.offsetWidth) - Math.max(0, nd.offsetLeft - td.scrollLeft) - 5);
		this.setSize(w, '');
	},

	triggerEdit : function(node, defer) {
		this.completeEdit();
		if (node.attributes.editable !== false) {
			this.editNode = node;
			this.autoEditTimer = this.startEdit.defer(this.editDelay, this, [ node.ui.textNode, node.text ]);
			return false;
		}
	},

	bindScroll : function() {
		this.tree.getTreeEl().on('scroll', this.cancelEdit, this);
	},

	beforeNodeClick : function(node, e) {
		clearTimeout(this.autoEditTimer);
		if (this.tree.getSelectionModel().isSelected(node)) {
			e.stopEvent();
			return this.triggerEdit(node);
		}
	},

	onNodeDblClick : function(node, e) {
		clearTimeout(this.autoEditTimer);
	},

	updateNode : function(ed, value) {
		this.tree.getTreeEl().un('scroll', this.cancelEdit, this);
		this.editNode.setText(value);
	},

	onHide : function() {
		Tera.tree.TreeEditor.superclass.onHide.call(this);
		if (this.editNode) {
			this.editNode.ui.focus.defer(50, this.editNode.ui);
		}
	},

	onSpecialKey : function(field, e) {
		var k = e.getKey();
		if (k == e.ESC) {
			e.stopEvent();
			this.cancelEdit();
		} else if (k == e.ENTER && !e.hasModifier()) {
			e.stopEvent();
			this.completeEdit();
		}
	}
});

Tera.menu.Menu = function(config) {
	if (Tera.isArray(config)) {
		config = {
			items :config
		};
	}
	Tera.apply(this, config);
	this.id = this.id || Tera.id();
	this.addEvents(

	'beforeshow',

	'beforehide',

	'show',

	'hide',

	'click',

	'mouseover',

	'mouseout',

	'itemclick');
	Tera.menu.MenuMgr.register(this);
	Tera.menu.Menu.superclass.constructor.call(this);
	var mis = this.items;

	this.items = new Tera.util.MixedCollection();
	if (mis) {
		this.add.apply(this, mis);
	}
};

Tera.extend(Tera.menu.Menu, Tera.util.Observable, {

	minWidth :120,

	shadow :"sides",

	subMenuAlign :"tl-tr?",

	defaultAlign :"tl-bl?",

	allowOtherMenus :false,

	hidden :true,

	createEl : function() {
		return new Tera.Layer( {
			cls :"x-menu",
			shadow :this.shadow,
			constrain :false,
			parentEl :this.parentEl || document.body,
			zindex :15000
		});
	},

	render : function() {
		if (this.el) {
			return;
		}
		var el = this.el = this.createEl();

		if (!this.keyNav) {
			this.keyNav = new Tera.menu.MenuNav(this);
		}
		if (this.plain) {
			el.addClass("x-menu-plain");
		}
		if (this.cls) {
			el.addClass(this.cls);
		}
		this.focusEl = el.createChild( {
			tag :"a",
			cls :"x-menu-focus",
			href :"#",
			onclick :"return false;",
			tabIndex :"-1"
		});
		var ul = el.createChild( {
			tag :"ul",
			cls :"x-menu-list"
		});
		ul.on("click", this.onClick, this);
		ul.on("mouseover", this.onMouseOver, this);
		ul.on("mouseout", this.onMouseOut, this);
		this.items.each( function(item) {
			var li = document.createElement("li");
			li.className = "x-menu-list-item";
			ul.dom.appendChild(li);
			item.render(li, this);
		}, this);
		this.ul = ul;
		this.autoWidth();
	},

	autoWidth : function() {
		var el = this.el, ul = this.ul;
		if (!el) {
			return;
		}
		var w = this.width;
		if (w) {
			el.setWidth(w);
		} else if (Tera.isIE) {
			el.setWidth(this.minWidth);
			var t = el.dom.offsetWidth;
			el.setWidth(ul.getWidth() + el.getFrameWidth("lr"));
		}
	},

	delayAutoWidth : function() {
		if (this.el) {
			if (!this.awTask) {
				this.awTask = new Tera.util.DelayedTask(this.autoWidth, this);
			}
			this.awTask.delay(20);
		}
	},

	findTargetItem : function(e) {
		var t = e.getTarget(".x-menu-list-item", this.ul, true);
		if (t && t.menuItemId) {
			return this.items.get(t.menuItemId);
		}
	},

	onClick : function(e) {
		var t;
		if (t = this.findTargetItem(e)) {
			t.onClick(e);
			this.fireEvent("click", this, t, e);
		}
	},

	setActiveItem : function(item, autoExpand) {
		if (item != this.activeItem) {
			if (this.activeItem) {
				this.activeItem.deactivate();
			}
			this.activeItem = item;
			item.activate(autoExpand);
		} else if (autoExpand) {
			item.expandMenu();
		}
	},

	tryActivate : function(start, step) {
		var items = this.items;
		for ( var i = start, len = items.length; i >= 0 && i < len; i += step) {
			var item = items.get(i);
			if (!item.disabled && item.canActivate) {
				this.setActiveItem(item, false);
				return item;
			}
		}
		return false;
	},

	onMouseOver : function(e) {
		var t;
		if (t = this.findTargetItem(e)) {
			if (t.canActivate && !t.disabled) {
				this.setActiveItem(t, true);
			}
		}
		this.fireEvent("mouseover", this, e, t);
	},

	onMouseOut : function(e) {
		var t;
		if (t = this.findTargetItem(e)) {
			if (t == this.activeItem && t.shouldDeactivate(e)) {
				this.activeItem.deactivate();
				delete this.activeItem;
			}
		}
		this.fireEvent("mouseout", this, e, t);
	},

	isVisible : function() {
		return this.el && !this.hidden;
	},

	show : function(el, pos, parentMenu) {
		this.parentMenu = parentMenu;
		if (!this.el) {
			this.render();
		}
		this.fireEvent("beforeshow", this);
		this.showAt(this.el.getAlignToXY(el, pos || this.defaultAlign), parentMenu, false);
	},

	showAt : function(xy, parentMenu, _e) {
		this.parentMenu = parentMenu;
		if (!this.el) {
			this.render();
		}
		if (_e !== false) {
			this.fireEvent("beforeshow", this);
			xy = this.el.adjustForConstraints(xy);
		}
		this.el.setXY(xy);
		this.el.show();
		this.hidden = false;
		this.focus();
		this.fireEvent("show", this);
	},

	focus : function() {
		if (!this.hidden) {
			this.doFocus.defer(50, this);
		}
	},

	doFocus : function() {
		if (!this.hidden) {
			this.focusEl.focus();
		}
	},

	hide : function(deep) {
		if (this.el && this.isVisible()) {
			this.fireEvent("beforehide", this);
			if (this.activeItem) {
				this.activeItem.deactivate();
				this.activeItem = null;
			}
			this.el.hide();
			this.hidden = true;
			this.fireEvent("hide", this);
		}
		if (deep === true && this.parentMenu) {
			this.parentMenu.hide(true);
		}
	},

	add : function() {
		var a = arguments, l = a.length, item;
		for ( var i = 0; i < l; i++) {
			var el = a[i];
			if (el.render) {
				item = this.addItem(el);
			} else if (typeof el == "string") {
				if (el == "separator" || el == "-") {
					item = this.addSeparator();
				} else {
					item = this.addText(el);
				}
			} else if (el.tagName || el.el) {
				item = this.addElement(el);
			} else if (typeof el == "object") {
				Tera.applyIf(el, this.defaults);
				item = this.addMenuItem(el);
			}
		}
		return item;
	},

	getEl : function() {
		if (!this.el) {
			this.render();
		}
		return this.el;
	},

	addSeparator : function() {
		return this.addItem(new Tera.menu.Separator());
	},

	addElement : function(el) {
		return this.addItem(new Tera.menu.BaseItem(el));
	},

	addItem : function(item) {
		this.items.add(item);
		if (this.ul) {
			var li = document.createElement("li");
			li.className = "x-menu-list-item";
			this.ul.dom.appendChild(li);
			item.render(li, this);
			this.delayAutoWidth();
		}
		return item;
	},

	addMenuItem : function(config) {
		if (!(config instanceof Tera.menu.Item)) {
			if (typeof config.checked == "boolean") {
				config = new Tera.menu.CheckItem(config);
			} else {
				config = new Tera.menu.Item(config);
			}
		}
		return this.addItem(config);
	},

	addText : function(text) {
		return this.addItem(new Tera.menu.TextItem(text));
	},

	insert : function(index, item) {
		this.items.insert(index, item);
		if (this.ul) {
			var li = document.createElement("li");
			li.className = "x-menu-list-item";
			this.ul.dom.insertBefore(li, this.ul.dom.childNodes[index]);
			item.render(li, this);
			this.delayAutoWidth();
		}
		return item;
	},

	remove : function(item) {
		this.items.removeKey(item.id);
		item.destroy();
	},

	removeAll : function() {
		if (this.items) {
			var f;
			while (f = this.items.first()) {
				this.remove(f);
			}
		}
	},

	destroy : function() {
		this.beforeDestroy();
		Tera.menu.MenuMgr.unregister(this);
		if (this.keyNav) {
			this.keyNav.disable();
		}
		this.removeAll();
		if (this.ul) {
			this.ul.removeAllListeners();
		}
		if (this.el) {
			this.el.destroy();
		}
	},

	beforeDestroy :Tera.emptyFn

});

Tera.menu.MenuNav = function(menu) {
	Tera.menu.MenuNav.superclass.constructor.call(this, menu.el);
	this.scope = this.menu = menu;
};

Tera.extend(Tera.menu.MenuNav, Tera.KeyNav, {
	doRelay : function(e, h) {
		var k = e.getKey();
		if (!this.menu.activeItem && e.isNavKeyPress() && k != e.SPACE && k != e.RETURN) {
			this.menu.tryActivate(0, 1);
			return false;
		}
		return h.call(this.scope || this, e, this.menu);
	},

	up : function(e, m) {
		if (!m.tryActivate(m.items.indexOf(m.activeItem) - 1, -1)) {
			m.tryActivate(m.items.length - 1, -1);
		}
	},

	down : function(e, m) {
		if (!m.tryActivate(m.items.indexOf(m.activeItem) + 1, 1)) {
			m.tryActivate(0, 1);
		}
	},

	right : function(e, m) {
		if (m.activeItem) {
			m.activeItem.expandMenu(true);
		}
	},

	left : function(e, m) {
		m.hide();
		if (m.parentMenu && m.parentMenu.activeItem) {
			m.parentMenu.activeItem.activate();
		}
	},

	enter : function(e, m) {
		if (m.activeItem) {
			e.stopPropagation();
			m.activeItem.onClick(e);
			m.fireEvent("click", this, m.activeItem);
			return true;
		}
	}
});

Tera.menu.MenuMgr = function() {
	var menus, active, groups = {}, attached = false, lastShow = new Date();

	function init() {
		menus = {};
		active = new Tera.util.MixedCollection();
		Tera.getDoc().addKeyListener(27, function() {
			if (active.length > 0) {
				hideAll();
			}
		});
	}

	function hideAll() {
		if (active && active.length > 0) {
			var c = active.clone();
			c.each( function(m) {
				m.hide();
			});
		}
	}

	function onHide(m) {
		active.remove(m);
		if (active.length < 1) {
			Tera.getDoc().un("mousedown", onMouseDown);
			attached = false;
		}
	}

	function onShow(m) {
		var last = active.last();
		lastShow = new Date();
		active.add(m);
		if (!attached) {
			Tera.getDoc().on("mousedown", onMouseDown);
			attached = true;
		}
		if (m.parentMenu) {
			m.getEl().setZIndex(parseInt(m.parentMenu.getEl().getStyle("z-index"), 10) + 3);
			m.parentMenu.activeChild = m;
		} else if (last && last.isVisible()) {
			m.getEl().setZIndex(parseInt(last.getEl().getStyle("z-index"), 10) + 3);
		}
	}

	function onBeforeHide(m) {
		if (m.activeChild) {
			m.activeChild.hide();
		}
		if (m.autoHideTimer) {
			clearTimeout(m.autoHideTimer);
			delete m.autoHideTimer;
		}
	}

	function onBeforeShow(m) {
		var pm = m.parentMenu;
		if (!pm && !m.allowOtherMenus) {
			hideAll();
		} else if (pm && pm.activeChild) {
			pm.activeChild.hide();
		}
	}

	function onMouseDown(e) {
		if (lastShow.getElapsed() > 50 && active.length > 0 && !e.getTarget(".x-menu")) {
			hideAll();
		}
	}

	function onBeforeCheck(mi, state) {
		if (state) {
			var g = groups[mi.group];
			for ( var i = 0, l = g.length; i < l; i++) {
				if (g[i] != mi) {
					g[i].setChecked(false);
				}
			}
		}
	}

	return {

		hideAll : function() {
			hideAll();
		},

		register : function(menu) {
			if (!menus) {
				init();
			}
			menus[menu.id] = menu;
			menu.on("beforehide", onBeforeHide);
			menu.on("hide", onHide);
			menu.on("beforeshow", onBeforeShow);
			menu.on("show", onShow);
			var g = menu.group;
			if (g && menu.events["checkchange"]) {
				if (!groups[g]) {
					groups[g] = [];
				}
				groups[g].push(menu);
				menu.on("checkchange", onCheck);
			}
		},

		get : function(menu) {
			if (typeof menu == "string") {
				if (!menus) {
					return null;
				}
				return menus[menu];
			} else if (menu.events) {
				return menu;
			} else if (typeof menu.length == 'number') {
				return new Tera.menu.Menu( {
					items :menu
				});
			} else {
				return new Tera.menu.Menu(menu);
			}
		},

		unregister : function(menu) {
			delete menus[menu.id];
			menu.un("beforehide", onBeforeHide);
			menu.un("hide", onHide);
			menu.un("beforeshow", onBeforeShow);
			menu.un("show", onShow);
			var g = menu.group;
			if (g && menu.events["checkchange"]) {
				groups[g].remove(menu);
				menu.un("checkchange", onCheck);
			}
		},

		registerCheckable : function(menuItem) {
			var g = menuItem.group;
			if (g) {
				if (!groups[g]) {
					groups[g] = [];
				}
				groups[g].push(menuItem);
				menuItem.on("beforecheckchange", onBeforeCheck);
			}
		},

		unregisterCheckable : function(menuItem) {
			var g = menuItem.group;
			if (g) {
				groups[g].remove(menuItem);
				menuItem.un("beforecheckchange", onBeforeCheck);
			}
		},

		getCheckedItem : function(groupId) {
			var g = groups[groupId];
			if (g) {
				for ( var i = 0, l = g.length; i < l; i++) {
					if (g[i].checked) {
						return g[i];
					}
				}
			}
			return null;
		},

		setCheckedItem : function(groupId, itemId) {
			var g = groups[groupId];
			if (g) {
				for ( var i = 0, l = g.length; i < l; i++) {
					if (g[i].id == itemId) {
						g[i].setChecked(true);
					}
				}
			}
			return null;
		}
	};
}();

Tera.menu.BaseItem = function(config) {
	Tera.menu.BaseItem.superclass.constructor.call(this, config);

	this.addEvents(

	'click',

	'activate',

	'deactivate');

	if (this.handler) {
		this.on("click", this.handler, this.scope);
	}
};

Tera.extend(Tera.menu.BaseItem, Tera.Component, {

	canActivate :false,

	activeClass :"x-menu-item-active",

	hideOnClick :true,

	hideDelay :100,

	ctype :"Tera.menu.BaseItem",

	actionMode :"container",

	render : function(container, parentMenu) {
		this.parentMenu = parentMenu;
		Tera.menu.BaseItem.superclass.render.call(this, container);
		this.container.menuItemId = this.id;
	},

	onRender : function(container, position) {
		this.el = Tera.get(this.el);
		container.dom.appendChild(this.el.dom);
	},

	setHandler : function(handler, scope) {
		if (this.handler) {
			this.un("click", this.handler, this.scope);
		}
		this.on("click", this.handler = handler, this.scope = scope);
	},

	onClick : function(e) {
		if (!this.disabled && this.fireEvent("click", this, e) !== false && this.parentMenu.fireEvent("itemclick", this, e) !== false) {
			this.handleClick(e);
		} else {
			e.stopEvent();
		}
	},

	activate : function() {
		if (this.disabled) {
			return false;
		}
		var li = this.container;
		li.addClass(this.activeClass);
		this.region = li.getRegion().adjust(2, 2, -2, -2);
		this.fireEvent("activate", this);
		return true;
	},

	deactivate : function() {
		this.container.removeClass(this.activeClass);
		this.fireEvent("deactivate", this);
	},

	shouldDeactivate : function(e) {
		return !this.region || !this.region.contains(e.getPoint());
	},

	handleClick : function(e) {
		if (this.hideOnClick) {
			this.parentMenu.hide.defer(this.hideDelay, this.parentMenu, [ true ]);
		}
	},

	expandMenu : function(autoActivate) {
	},

	hideMenu : function() {
	}
});

Tera.menu.TextItem = function(text) {
	this.text = text;
	Tera.menu.TextItem.superclass.constructor.call(this);
};

Tera.extend(Tera.menu.TextItem, Tera.menu.BaseItem, {

	hideOnClick :false,

	itemCls :"x-menu-text",

	onRender : function() {
		var s = document.createElement("span");
		s.className = this.itemCls;
		s.innerHTML = this.text;
		this.el = s;
		Tera.menu.TextItem.superclass.onRender.apply(this, arguments);
	}
});

Tera.menu.Separator = function(config) {
	Tera.menu.Separator.superclass.constructor.call(this, config);
};

Tera.extend(Tera.menu.Separator, Tera.menu.BaseItem, {

	itemCls :"x-menu-sep",

	hideOnClick :false,

	onRender : function(li) {
		var s = document.createElement("span");
		s.className = this.itemCls;
		s.innerHTML = "&#160;";
		this.el = s;
		li.addClass("x-menu-sep-li");
		Tera.menu.Separator.superclass.onRender.apply(this, arguments);
	}
});

Tera.menu.Item = function(config) {
	Tera.menu.Item.superclass.constructor.call(this, config);
	if (this.menu) {
		this.menu = Tera.menu.MenuMgr.get(this.menu);
	}
};
Tera.extend(Tera.menu.Item, Tera.menu.BaseItem, {

	itemCls :"x-menu-item",

	canActivate :true,

	showDelay :200,
	hideDelay :200,

	ctype :"Tera.menu.Item",

	onRender : function(container, position) {
		var el = document.createElement("a");
		el.hideFocus = true;
		el.unselectable = "on";
		el.href = this.href || "#";
		if (this.hrefTarget) {
			el.target = this.hrefTarget;
		}
		el.className = this.itemCls + (this.menu ? " x-menu-item-arrow" : "") + (this.cls ? " " + this.cls : "");
		el.innerHTML = String.format('<img src="{0}" class="x-menu-item-icon {2}" />{1}', this.icon || Tera.BLANK_IMAGE_URL, this.itemText
				|| this.text, this.iconCls || '');
		this.el = el;
		Tera.menu.Item.superclass.onRender.call(this, container, position);
	},

	setText : function(text) {
		this.text = text;
		if (this.rendered) {
			this.el.update(String.format('<img src="{0}" class="x-menu-item-icon {2}">{1}', this.icon || Tera.BLANK_IMAGE_URL, this.text,
					this.iconCls || ''));
			this.parentMenu.autoWidth();
		}
	},

	setIconClass : function(cls) {
		var oldCls = this.iconCls;
		this.iconCls = cls;
		if (this.rendered) {
			this.el.child('img.x-menu-item-icon').replaceClass(oldCls, this.iconCls);
		}
	},

	handleClick : function(e) {
		if (!this.href) {
			e.stopEvent();
		}
		Tera.menu.Item.superclass.handleClick.apply(this, arguments);
	},

	activate : function(autoExpand) {
		if (Tera.menu.Item.superclass.activate.apply(this, arguments)) {
			this.focus();
			if (autoExpand) {
				this.expandMenu();
			}
		}
		return true;
	},

	shouldDeactivate : function(e) {
		if (Tera.menu.Item.superclass.shouldDeactivate.call(this, e)) {
			if (this.menu && this.menu.isVisible()) {
				return !this.menu.getEl().getRegion().contains(e.getPoint());
			}
			return true;
		}
		return false;
	},

	deactivate : function() {
		Tera.menu.Item.superclass.deactivate.apply(this, arguments);
		this.hideMenu();
	},

	expandMenu : function(autoActivate) {
		if (!this.disabled && this.menu) {
			clearTimeout(this.hideTimer);
			delete this.hideTimer;
			if (!this.menu.isVisible() && !this.showTimer) {
				this.showTimer = this.deferExpand.defer(this.showDelay, this, [ autoActivate ]);
			} else if (this.menu.isVisible() && autoActivate) {
				this.menu.tryActivate(0, 1);
			}
		}
	},

	deferExpand : function(autoActivate) {
		delete this.showTimer;
		this.menu.show(this.container, this.parentMenu.subMenuAlign || "tl-tr?", this.parentMenu);
		if (autoActivate) {
			this.menu.tryActivate(0, 1);
		}
	},

	hideMenu : function() {
		clearTimeout(this.showTimer);
		delete this.showTimer;
		if (!this.hideTimer && this.menu && this.menu.isVisible()) {
			this.hideTimer = this.deferHide.defer(this.hideDelay, this);
		}
	},

	deferHide : function() {
		delete this.hideTimer;
		this.menu.hide();
	}
});

Tera.menu.CheckItem = function(config) {
	Tera.menu.CheckItem.superclass.constructor.call(this, config);
	this.addEvents(

	"beforecheckchange",

	"checkchange");

	if (this.checkHandler) {
		this.on('checkchange', this.checkHandler, this.scope);
	}
	Tera.menu.MenuMgr.registerCheckable(this);
};
Tera.extend(Tera.menu.CheckItem, Tera.menu.Item, {

	itemCls :"x-menu-item x-menu-check-item",

	groupClass :"x-menu-group-item",

	checked :false,

	ctype :"Tera.menu.CheckItem",

	onRender : function(c) {
		Tera.menu.CheckItem.superclass.onRender.apply(this, arguments);
		if (this.group) {
			this.el.addClass(this.groupClass);
		}
		if (this.checked) {
			this.checked = false;
			this.setChecked(true, true);
		}
	},

	destroy : function() {
		Tera.menu.MenuMgr.unregisterCheckable(this);
		Tera.menu.CheckItem.superclass.destroy.apply(this, arguments);
	},

	setChecked : function(state, suppressEvent) {
		if (this.checked != state && this.fireEvent("beforecheckchange", this, state) !== false) {
			if (this.container) {
				this.container[state ? "addClass" : "removeClass"]("x-menu-item-checked");
			}
			this.checked = state;
			if (suppressEvent !== true) {
				this.fireEvent("checkchange", this, state);
			}
		}
	},

	handleClick : function(e) {
		if (!this.disabled && !(this.checked && this.group)) {
			this.setChecked(!this.checked);
		}
		Tera.menu.CheckItem.superclass.handleClick.apply(this, arguments);
	}
});

Tera.menu.Adapter = function(component, config) {
	Tera.menu.Adapter.superclass.constructor.call(this, config);
	this.component = component;
};
Tera.extend(Tera.menu.Adapter, Tera.menu.BaseItem, {
	canActivate :true,

	onRender : function(container, position) {
		this.component.render(container);
		this.el = this.component.getEl();
	},

	activate : function() {
		if (this.disabled) {
			return false;
		}
		this.component.focus();
		this.fireEvent("activate", this);
		return true;
	},

	deactivate : function() {
		this.fireEvent("deactivate", this);
	},

	disable : function() {
		this.component.disable();
		Tera.menu.Adapter.superclass.disable.call(this);
	},

	enable : function() {
		this.component.enable();
		Tera.menu.Adapter.superclass.enable.call(this);
	}
});

Tera.menu.DateItem = function(config) {
	Tera.menu.DateItem.superclass.constructor.call(this, new Tera.DatePicker(config), config);

	this.picker = this.component;
	this.addEvents('select');

	this.picker.on("render", function(picker) {
		picker.getEl().swallowEvent("click");
		picker.container.addClass("x-menu-date-item");
	});

	this.picker.on("select", this.onSelect, this);
};

Tera.extend(Tera.menu.DateItem, Tera.menu.Adapter, {
	onSelect : function(picker, date) {
		this.fireEvent("select", this, date, picker);
		Tera.menu.DateItem.superclass.handleClick.call(this);
	}
});

Tera.menu.ColorItem = function(config) {
	Tera.menu.ColorItem.superclass.constructor.call(this, new Tera.ColorPalette(config), config);

	this.palette = this.component;
	this.relayEvents(this.palette, [ "select" ]);
	if (this.selectHandler) {
		this.on('select', this.selectHandler, this.scope);
	}
};
Tera.extend(Tera.menu.ColorItem, Tera.menu.Adapter);

Tera.menu.DateMenu = function(config) {
	Tera.menu.DateMenu.superclass.constructor.call(this, config);
	this.plain = true;
	var di = new Tera.menu.DateItem(config);
	this.add(di);

	this.picker = di.picker;

	this.relayEvents(di, [ "select" ]);

	this.on('beforeshow', function() {
		if (this.picker) {
			this.picker.hideMonthPicker(true);
		}
	}, this);
};
Tera.extend(Tera.menu.DateMenu, Tera.menu.Menu, {
	cls :'x-date-menu',

	beforeDestroy : function() {
		this.picker.destroy();
	}
});

Tera.menu.ColorMenu = function(config) {
	Tera.menu.ColorMenu.superclass.constructor.call(this, config);
	this.plain = true;
	var ci = new Tera.menu.ColorItem(config);
	this.add(ci);

	this.palette = ci.palette;

	this.relayEvents(ci, [ "select" ]);
};
Tera.extend(Tera.menu.ColorMenu, Tera.menu.Menu);

Tera.form.Field = Tera.extend(Tera.BoxComponent, {

	invalidClass :"x-form-invalid",

	invalidText :"The value in this field is invalid",

	focusClass :"x-form-focus",

	validationEvent :"keyup",

	validateOnBlur :true,

	validationDelay :250,
	
	forceToUpper: false,

	defaultAutoCreate : {
		tag :"input",
		type :"text",
		size :"20",
		autocomplete :"off"
	},

	fieldClass :"x-form-field",
	
	fieldClass2 :"x-form-field2",

	msgTarget :'qtip',

	msgFx :'normal',

	readOnly :false,

	disabled :false,

	isFormField :true,

	hasFocus :false,

	optional: false,
	
	isUpperCase: null,
	
	isExceptionForceToUpper: false,  //usado para mantener lower case en un form con upperCase 
	
	initComponent : function() {
		Tera.form.Field.superclass.initComponent.call(this);
		this.addEvents(

		'focus',

		'blur',

		'specialkey',

		'change',

		'invalid',

		'valid');
	},

	getName : function() {
		return this.rendered && this.el.dom.name ? this.el.dom.name : (this.hiddenName || '');
	},

	isOptional: function() {
		return this.optional;
	},

	onRender : function(ct, position) {
		Tera.form.Field.superclass.onRender.call(this, ct, position);
		if (!this.el) {
			var cfg = this.getAutoCreate();
			if (!cfg.name) {
				cfg.name = this.name || this.id;
			}
			if (this.inputType) {
				cfg.type = this.inputType;
			}
			this.el = ct.createChild(cfg, position);
		}
		var type = this.el.dom.type;
		if (type) {
			if (type == 'password') {
				type = 'text';
			}
			this.el.addClass('x-form-' + type);
		}
		if (this.readOnly) {
			this.el.dom.readOnly = true;
		}
		if (this.tabIndex !== undefined) {
			this.el.dom.setAttribute('tabIndex', this.tabIndex);
		}
		if (ct.getX() >=0) {
			this.el.addClass( [ this.fieldClass, this.cls ]);
		}else {
			this.el.addClass( [ this.fieldClass2, this.cls ]);
		}
		this.initValue();
		
		if (this.findForceToUpperParentBy(this)) {
			if (!this.isExceptionForceToUpper){ //para evitar cambio del field a upperCase
				this.applyUpper = true;
				//Tera.apply(this, { vtype: 'uppercase', validationDelay: 5 });
				Tera.apply(this, { style: "textTransform: uppercase" });
				this.mon(this.el, 'keyup', function(evt,tfield) {
					var kc = this.getKeyCode(evt);
					if (kc.isShiftPressed || kc.isDelete || kc.isTab || kc.isBackspace || kc.isLeftOrRightArrow) {
						return;
					} 
					var value = this.getValue();
					if (this.getXType() != 'datefield' && (!Tera.num(value, -1) === -1)) {
						var cp = this.el.getCaretPosition();
						this.setValue(value.toUpperCase());
						this.el.setCaretPosition(cp);
					}
				}, this);
			}			
		}
		
		if (!Tera.isEmpty(this.isUpperCase) && this.isUpperCase){
			this.applyUpper = true;
			Tera.apply(this, { style: "textTransform: uppercase" });
			this.mon(this.el, 'keyup', function(evt,tfield) {
				var kc = this.getKeyCode(evt);
				if (kc.isShiftPressed || kc.isDelete || kc.isTab || kc.isBackspace || kc.isLeftOrRightArrow) {
					return;
				} 
				var value = this.getValue();
				if (this.getXType() != 'datefield' && (!Tera.num(value, -1) === -1)) {
					var cp = this.el.getCaretPosition();
					this.setValue(value.toUpperCase());
					this.el.setCaretPosition(cp);
				}
			}, this);
		}
	},
	
	getKeyCode : function(onKeyDownEvent) {
        var keycode = {};
        keycode.unicode =onKeyDownEvent.getKey();
        keycode.isShiftPressed = onKeyDownEvent.shiftKey;
        keycode.isDelete=(onKeyDownEvent.getKey() == Tera.EventObject.DELETE)? true: false;
        keycode.isTab = (onKeyDownEvent.getCharCode() == Tera.EventObject.TAB)? true: false;
        keycode.isBackspace = (onKeyDownEvent.getCharCode() == Tera.EventObject.BACKSPACE)? true: false;
        keycode.isLeftOrRightArrow = (onKeyDownEvent.getCharCode() == Tera.EventObject.LEFT || onKeyDownEvent.getCharCode() == Tera.EventObject.RIGHT)? true: false;
        keycode.pressedKey = String.fromCharCode(keycode.unicode);
        return(keycode);
    },
	
	initValue : function() {
		if (this.value !== undefined) {
			this.setValue(this.value);
		} else if (this.el.dom.value.length > 0) {
			this.setValue(this.el.dom.value);
		}
	},

	isDirty : function() {
		if (this.disabled) {
			return false;
		}
		return String(this.getValue()) !== String(this.originalValue);
	},

	afterRender : function() {
		Tera.form.Field.superclass.afterRender.call(this);
		this.initEvents();
		this.validate();
	},

	fireKey : function(e) {
		if (e.isSpecialKey()) {
			this.fireEvent("specialkey", this, e);
		}
	},

	reset : function() {
		this.setValue(this.originalValue);
		this.clearInvalid();
	},

	initEvents : function() {
		this.el.on(Tera.isIE || Tera.isSafari3 ? "keydown" : "keypress", this.fireKey, this);
		this.el.on("focus", this.onFocus, this);
		this.el.on("blur", this.onBlur, this);

		this.originalValue = this.getValue();
	},

	onFocus : function() {
		if (!Tera.isOpera && this.focusClass) {
			this.el.addClass(this.focusClass);
		}
		if (!this.hasFocus) {
			this.hasFocus = true;
			this.startValue = this.getValue();
			this.fireEvent("focus", this);
		}
	},

	beforeBlur :Tera.emptyFn,

	onBlur : function() {
		this.beforeBlur();
		if (!Tera.isOpera && this.focusClass) {
			this.el.removeClass(this.focusClass);
		}
		this.hasFocus = false;
		if (this.validationEvent !== false && this.validateOnBlur && this.validationEvent != "blur") {
			this.validate();
		}
		var v = this.getValue();
		if (String(v) !== String(this.startValue)) {
			this.fireEvent('change', this, v, this.startValue);
		}
		this.fireEvent("blur", this);
	},

	isValid : function(preventMark) {
		if (this.disabled) {
			return true;
		}
		var restore = this.preventMark;
		this.preventMark = preventMark === true;
		var v = this.validateValue(this.processValue(this.getRawValue()));
		this.preventMark = restore;
		return v;
	},

	validate : function() {
		if (this.disabled || this.validateValue(this.processValue(this.getRawValue()))) {
			this.clearInvalid();
			return true;
		}
		return false;
	},

	processValue : function(value) {
		return value;
	},

	validateValue : function(value) {
		return true;
	},

	markInvalid : function(msg) {
		if (!this.rendered || this.preventMark) {
			return;
		}
		this.el.addClass(this.invalidClass);
		msg = msg || this.invalidText;
		switch (this.msgTarget) {
			case 'qtip':
				this.el.dom.qtip = msg;
				this.el.dom.qclass = 'x-form-invalid-tip';
				if (Tera.QuickTips) {
					Tera.QuickTips.enable();
				}
				break;
			case 'title':
				this.el.dom.title = msg;
				break;
			case 'under':
				if (!this.errorEl) {
					var elp = this.getErrorCt();
					this.errorEl = elp.createChild( {
						cls :'x-form-invalid-msg'
					});
					this.errorEl.setWidth(elp.getWidth(true) - 20);
				}
				this.errorEl.update(msg);
				Tera.form.Field.msgFx[this.msgFx].show(this.errorEl, this);
				break;
			case 'side':
				if (!this.errorIcon) {
					var elp = this.getErrorCt();
					if (elp == null) break;
					this.errorIcon = elp.createChild( {
						cls :'x-form-invalid-icon'
					});
				}
				this.alignErrorIcon();
				this.errorIcon.dom.qtip = msg;
				this.errorIcon.dom.qclass = 'x-form-invalid-tip';
				this.errorIcon.show();
				this.on('resize', this.alignErrorIcon, this);
				break;
			default:
				var t = Tera.getDom(this.msgTarget);
				t.innerHTML = msg;
				t.style.display = this.msgDisplay;
				break;
		}
		this.fireEvent('invalid', this, msg);
	},

	getErrorCt : function() {
		return this.el.findParent('.x-form-element', 5, true) || this.el.findParent('.x-form-field-wrap', 5, true);
	},

	alignErrorIcon : function() {
		this.errorIcon.alignTo(this.el, 'tl-tr', [ 2, 0 ]);
	},

	clearInvalid : function() {
		if (!this.rendered || this.preventMark) {
			return;
		}
		this.el.removeClass(this.invalidClass);
		switch (this.msgTarget) {
			case 'qtip':
				this.el.dom.qtip = '';
				break;
			case 'title':
				this.el.dom.title = '';
				break;
			case 'under':
				if (this.errorEl) {
					Tera.form.Field.msgFx[this.msgFx].hide(this.errorEl, this);
				}
				break;
			case 'side':
				if (this.errorIcon) {
					this.errorIcon.dom.qtip = '';
					this.errorIcon.hide();
					this.un('resize', this.alignErrorIcon, this);
				}
				break;
			default:
				var t = Tera.getDom(this.msgTarget);
				t.innerHTML = '';
				t.style.display = 'none';
				break;
		}
		this.fireEvent('valid', this);
	},

	getRawValue : function() {
		var v = this.rendered ? this.el.getValue() : Tera.value(this.value, '');
		if (v === this.emptyText) {
			v = '';
		}
		
		if (this.applyUpper) {
			v = v.toUpperCase();
		}
		return v;
	},

	getValue : function() {
		if (!this.rendered) {
			if (this.applyUpper) {
				return this.value.toUpperCase();
			}
			return this.value;
		}
		var v = this.el.getValue();
		if (v === this.emptyText || v === undefined) {
			v = '';
		}

		if (this.applyUpper) {
			v = v.toUpperCase();
		}
			
		return v;
	},

	findForceToUpperParentBy : function(component, shallow){
		var container = component.findParentBy(function(component, shallow){
    		if (!Tera.isEmpty(component.forceToUpper) && component.forceToUpper) {
       			return true;
    		}
    		return false;
    	},this);
    	
    	if (Tera.isEmpty(container)) {
    		return false;
    	} else {
    		component.applyToUpper = true;
    		return container.forceToUpper;
    	}	
    },
	setRawValue : function(v) {
		if (this.applyUpper) {
    		v = v.toUpperCase();
    	}
		return this.el.dom.value = (v === null || v === undefined ? '' : v);
	},

	setValue : function(v) {
		this.value = v;
		if (this.applyUpper) {
    		v = v.toUpperCase();
    	}
		if (this.rendered) {
			this.el.dom.value = (v === null || v === undefined ? '' : v);
			this.validate();
		}
	},

	adjustSize : function(w, h) {
		var s = Tera.form.Field.superclass.adjustSize.call(this, w, h);
		s.width = this.adjustWidth(this.el.dom.tagName, s.width);
		return s;
	},

	adjustWidth : function(tag, w) {
		tag = tag.toLowerCase();
		if (typeof w == 'number' && !Tera.isSafari) {
			if (Tera.isIE && (tag == 'input' || tag == 'textarea')) {
				if (tag == 'input' && !Tera.isStrict) {
					return this.inEditor ? w : w - 3;
				}
				if (tag == 'input' && Tera.isStrict) {
					return w - (Tera.isIE6 ? 4 : 1);
				}
				if (tag == 'textarea' && Tera.isStrict) {
					return w - 2;
				}
			} else if (Tera.isOpera && Tera.isStrict) {
				if (tag == 'input') {
					return w + 2;
				}
				if (tag == 'textarea') {
					return w - 2;
				}
			}
		}
		return w;
	}

});

Tera.form.Field.msgFx = {
	normal : {
		show : function(msgEl, f) {
			msgEl.setDisplayed('block');
		},

		hide : function(msgEl, f) {
			msgEl.setDisplayed(false).update('');
		}
	},

	slide : {
		show : function(msgEl, f) {
			msgEl.slideIn('t', {
				stopFx :true
			});
		},

		hide : function(msgEl, f) {
			msgEl.slideOut('t', {
				stopFx :true,
				useDisplay :true
			});
		}
	},

	slideRight : {
		show : function(msgEl, f) {
			msgEl.fixDisplay();
			msgEl.alignTo(f.el, 'tl-tr');
			msgEl.slideIn('l', {
				stopFx :true
			});
		},

		hide : function(msgEl, f) {
			msgEl.slideOut('l', {
				stopFx :true,
				useDisplay :true
			});
		}
	}
};
Tera.reg('field', Tera.form.Field);

Tera.form.TextField = Tera.extend(Tera.form.Field, {

	grow :false,

	growMin :30,

	growMax :800,

	vtype :null,

	maskRe :null,

	disableKeyFilter :false,

	allowBlank :true,

	minLength :0,

	maxLength :Number.MAX_VALUE,

	minLengthText :"El campo tiene un minimo de {0} car&aacute;cteres",

	maxLengthText :"El campo tiene un m&aacute;ximo de {0} car&aacute;cteres",

	selectOnFocus :false,

	blankText :"El Campo es Requerido",

	validator :null,

	regex :null,

	regexText :"",

	emptyText :null,

	emptyClass :'x-form-empty-field',

	initComponent : function() {
		Tera.form.TextField.superclass.initComponent.call(this);
		this.addEvents(

		'autosize',

		'keydown',

		'keyup',

		'keypress');
	},

	initEvents : function() {
		Tera.form.TextField.superclass.initEvents.call(this);
		if (this.validationEvent == 'keyup') {
			this.validationTask = new Tera.util.DelayedTask(this.validate, this);
			this.el.on('keyup', this.filterValidation, this);
		} else if (this.validationEvent !== false) {
			this.el.on(this.validationEvent, this.validate, this, {
				buffer :this.validationDelay
			});
		}
		if (this.selectOnFocus || this.emptyText) {
			this.on("focus", this.preFocus, this);
			if (this.emptyText) {
				this.on('blur', this.postBlur, this);
				this.applyEmptyText();
			}
		}
		if (this.maskRe || (this.vtype && this.disableKeyFilter !== true && (this.maskRe = Tera.form.VTypes[this.vtype + 'Mask']))) {
			this.el.on("keypress", this.filterKeys, this);
		}
		if (this.grow) {
			this.el.on("keyup", this.onKeyUpBuffered, this, {
				buffer :50
			});
			this.el.on("click", this.autoSize, this);
		}

		if (this.enableKeyEvents) {
			this.el.on("keyup", this.onKeyUp, this);
			this.el.on("keydown", this.onKeyDown, this);
			this.el.on("keypress", this.onKeyPress, this);
		}
		/*
		this.mon(this.el, 'keyup', function () {
				if (this.applyUpper) {
	    			this.setValue(this.getValue().toUpperCase());
	    		}
		}, this);
		*/
	},

	processValue : function(value) {
		if (this.stripCharsRe) {
			var newValue = value.replace(this.stripCharsRe, '');
			if (newValue !== value) {
				this.setRawValue(newValue);
				return newValue;
			}
		}
		return value;
	},

	filterValidation : function(e) {
		if (!e.isNavKeyPress()) {
			this.validationTask.delay(this.validationDelay);
		}
	},

	onKeyUpBuffered : function(e) {
		if (!e.isNavKeyPress()) {
			this.autoSize();
		}
	},

	onKeyUp : function(e) {
		this.fireEvent('keyup', this, e);
	},

	onKeyDown : function(e) {
		this.fireEvent('keydown', this, e);
	},

	onKeyPress : function(e) {
		this.fireEvent('keypress', this, e);
	},

	reset : function() {
		Tera.form.TextField.superclass.reset.call(this);

		this.applyEmptyText();
	},

	applyEmptyText : function() {
		if (this.rendered && this.emptyText && this.getRawValue().length < 1) {
			this.setRawValue(this.emptyText);
			this.el.addClass(this.emptyClass);
		}
	},

	preFocus : function() {
		if (this.emptyText) {
			if (this.el.dom.value == this.emptyText) {
				this.setRawValue('');
			}
			this.el.removeClass(this.emptyClass);
		}
		if (this.selectOnFocus) {
			this.el.dom.select();
		}
	},

	postBlur : function() {
		this.applyEmptyText();
	},

	filterKeys : function(e) {
		var k = e.getKey();
		if (!Tera.isIE && (e.isNavKeyPress() || k == e.BACKSPACE || (k == e.DELETE && e.button == -1))) {
			return;
		}
		var c = e.getCharCode(), cc = String.fromCharCode(c);
		if (Tera.isIE && (e.isSpecialKey() || !cc)) {
			return;
		}
		if (!this.maskRe.test(cc)) {
			e.stopEvent();
		}
	},

	setValue : function(v) {
		if (this.emptyText && this.el && v !== undefined && v !== null && v !== '') {
			this.el.removeClass(this.emptyClass);
		}
		Tera.form.TextField.superclass.setValue.apply(this, arguments);
		this.applyEmptyText();
		this.autoSize();
	},

	validateValue : function(value) {
		if (value.length < 1 || value === this.emptyText) {
			if (this.allowBlank) {
				this.clearInvalid();
				return true;
			} else {
				this.markInvalid(this.blankText);
				return false;
			}
		}
		if (value.length < this.minLength) {
			this.markInvalid(String.format(this.minLengthText, this.minLength));
			return false;
		}
		if (value.length > this.maxLength) {
			this.markInvalid(String.format(this.maxLengthText, this.maxLength));
			return false;
		}
		if (this.vtype) {
			var vt = Tera.form.VTypes;
			if (!vt[this.vtype](value, this)) {
				this.markInvalid(this.vtypeText || vt[this.vtype + 'Text']);
				return false;
			}
		}
		if (typeof this.validator == "function") {
			var msg = this.validator(value);
			if (msg !== true) {
				this.markInvalid(msg);
				return false;
			}
		}
		if (this.regex && !this.regex.test(value)) {
			this.markInvalid(this.regexText);
			return false;
		}
		return true;
	},

	selectText : function(start, end) {
		var v = this.getRawValue();
		if (v.length > 0) {
			start = start === undefined ? 0 : start;
			end = end === undefined ? v.length : end;
			var d = this.el.dom;
			if (d.setSelectionRange) {
				d.setSelectionRange(start, end);
			} else if (d.createTextRange) {
				var range = d.createTextRange();
				range.moveStart("character", start);
				range.moveEnd("character", end - v.length);
				range.select();
			}
		}
	},

	autoSize : function() {
		if (!this.grow || !this.rendered) {
			return;
		}
		if (!this.metrics) {
			this.metrics = Tera.util.TextMetrics.createInstance(this.el);
		}
		var el = this.el;
		var v = el.dom.value;
		var d = document.createElement('div');
		d.appendChild(document.createTextNode(v));
		v = d.innerHTML;
		d = null;
		v += "&#160;";
		var w = Math.min(this.growMax, Math.max(this.metrics.getWidth(v) + 10, this.growMin));
		this.el.setWidth(w);
		this.fireEvent("autosize", this, w);
	}
});




Tera.reg('textfield', Tera.form.TextField);

Tera.form.TriggerField = Tera.extend(Tera.form.TextField, {

	defaultAutoCreate : {
		tag :"input",
		type :"text",
		size :"16",
		autocomplete :"off"
	},

	hideTrigger :false,

	autoSize :Tera.emptyFn,
	monitorTab :true,
	deferHeight :true,
	mimicing :false,

	onResize : function(w, h) {
		Tera.form.TriggerField.superclass.onResize.call(this, w, h);
		if (typeof w == 'number') {
			this.el.setWidth(this.adjustWidth('input', w - this.trigger.getWidth()));
		}
		this.wrap.setWidth(this.el.getWidth() + this.trigger.getWidth());
	},

	adjustSize :Tera.BoxComponent.prototype.adjustSize,

	getResizeEl : function() {
		return this.wrap;
	},

	getPositionEl : function() {
		return this.wrap;
	},

	alignErrorIcon : function() {
		this.errorIcon.alignTo(this.wrap, 'tl-tr', [ 2, 0 ]);
	},

	onRender : function(ct, position) {
		Tera.form.TriggerField.superclass.onRender.call(this, ct, position);
		this.wrap = this.el.wrap( {
			cls :"x-form-field-wrap"
		});
		this.trigger = this.wrap.createChild(this.triggerConfig || {
			tag :"img",
			src :Tera.BLANK_IMAGE_URL,
			cls :"x-form-trigger " + this.triggerClass
		});
		if (this.hideTrigger) {
			this.trigger.setDisplayed(false);
		}
		this.initTrigger();
		if (!this.width) {
			this.wrap.setWidth(this.el.getWidth() + this.trigger.getWidth());
		}
	},

	initTrigger : function() {
		this.trigger.on("click", this.onTriggerClick, this, {
			preventDefault :true
		});
		this.trigger.addClassOnOver('x-form-trigger-over');
		this.trigger.addClassOnClick('x-form-trigger-click');
	},

	onDestroy : function() {
		if (this.trigger) {
			this.trigger.removeAllListeners();
			this.trigger.remove();
		}
		if (this.wrap) {
			this.wrap.remove();
		}
		Tera.form.TriggerField.superclass.onDestroy.call(this);
	},

	onFocus : function() {
		Tera.form.TriggerField.superclass.onFocus.call(this);
		if (!this.mimicing) {
			this.wrap.addClass('x-trigger-wrap-focus');
			this.mimicing = true;
			Tera.get(Tera.isIE ? document.body : document).on("mousedown", this.mimicBlur, this, {
				delay :10
			});
			if (this.monitorTab) {
				this.el.on("keydown", this.checkTab, this);
			}
		}
	},

	checkTab : function(e) {
		if (e.getKey() == e.TAB) {
			this.triggerBlur();
		}
	},

	onBlur : function() {
	},

	mimicBlur : function(e) {
		if (!this.wrap.contains(e.target) && this.validateBlur(e)) {
			this.triggerBlur();
		}
	},

	triggerBlur : function() {
		this.mimicing = false;
		Tera.get(Tera.isIE ? document.body : document).un("mousedown", this.mimicBlur);
		if (this.monitorTab) {
			this.el.un("keydown", this.checkTab, this);
		}
		this.beforeBlur();
		this.wrap.removeClass('x-trigger-wrap-focus');
		Tera.form.TriggerField.superclass.onBlur.call(this);
	},

	beforeBlur :Tera.emptyFn,

	validateBlur : function(e) {
		return true;
	},

	onDisable : function() {
		Tera.form.TriggerField.superclass.onDisable.call(this);
		if (this.wrap) {
			this.wrap.addClass('x-item-disabled');
		}
	},

	onEnable : function() {
		Tera.form.TriggerField.superclass.onEnable.call(this);
		if (this.wrap) {
			this.wrap.removeClass('x-item-disabled');
		}
	},

	onShow : function() {
		if (this.wrap) {
			this.wrap.dom.style.display = '';
			this.wrap.dom.style.visibility = 'visible';
		}
	},

	onHide : function() {
		this.wrap.dom.style.display = 'none';
	},

	onTriggerClick :Tera.emptyFn

});

Tera.form.TwinTriggerField = Tera.extend(Tera.form.TriggerField, {
	initComponent : function() {
		Tera.form.TwinTriggerField.superclass.initComponent.call(this);

		this.triggerConfig = {
			tag :'span',
			cls :'x-form-twin-triggers',
			cn : [ {
				tag :"img",
				src :Tera.BLANK_IMAGE_URL,
				cls :"x-form-trigger " + this.trigger1Class
			}, {
				tag :"img",
				src :Tera.BLANK_IMAGE_URL,
				cls :"x-form-trigger " + this.trigger2Class
			} ]
		};
	},

	getTrigger : function(index) {
		return this.triggers[index];
	},

	initTrigger : function() {
		var ts = this.trigger.select('.x-form-trigger', true);
		this.wrap.setStyle('overflow', 'hidden');
		var triggerField = this;
		ts.each( function(t, all, index) {
			t.hide = function() {
				var w = triggerField.wrap.getWidth();
				this.dom.style.display = 'none';
				triggerField.el.setWidth(w - triggerField.trigger.getWidth());
			};
			t.show = function() {
				var w = triggerField.wrap.getWidth();
				this.dom.style.display = '';
				triggerField.el.setWidth(w - triggerField.trigger.getWidth());
			};
			var triggerIndex = 'Trigger' + (index + 1);

			if (this['hide' + triggerIndex]) {
				t.dom.style.display = 'none';
			}
			t.on("click", this['on' + triggerIndex + 'Click'], this, {
				preventDefault :true
			});
			t.addClassOnOver('x-form-trigger-over');
			t.addClassOnClick('x-form-trigger-click');
		}, this);
		this.triggers = ts.elements;
	},

	onTrigger1Click :Tera.emptyFn,
	onTrigger2Click :Tera.emptyFn
});
Tera.reg('trigger', Tera.form.TriggerField);

Tera.form.TextArea = Tera.extend(Tera.form.TextField, {

	growMin :60,

	growMax :1000,
	growAppend :'&#160;\n&#160;',
	growPad :0,

	enterIsSpecial :false,

	preventScrollbars :false,

	onRender : function(ct, position) {
		if (!this.el) {
			this.defaultAutoCreate = {
				tag :"textarea",
				style :"width:100px;height:60px;",
				autocomplete :"off"
			};
		}
		Tera.form.TextArea.superclass.onRender.call(this, ct, position);
		if (this.grow) {
			this.textSizeEl = Tera.DomHelper.append(document.body, {
				tag :"pre",
				cls :"x-form-grow-sizer"
			});
			if (this.preventScrollbars) {
				this.el.setStyle("overflow", "hidden");
			}
			this.el.setHeight(this.growMin);
		}
	},

	onDestroy : function() {
		if (this.textSizeEl) {
			Tera.removeNode(this.textSizeEl);
		}
		Tera.form.TextArea.superclass.onDestroy.call(this);
	},

	fireKey : function(e) {
		if (e.isSpecialKey() && (this.enterIsSpecial || (e.getKey() != e.ENTER || e.hasModifier()))) {
			this.fireEvent("specialkey", this, e);
		}
	},

	onKeyUp : function(e) {
		if (!e.isNavKeyPress() || e.getKey() == e.ENTER) {
			this.autoSize();
		}
	},

	autoSize : function() {
		if (!this.grow || !this.textSizeEl) {
			return;
		}
		var el = this.el;
		var v = el.dom.value;
		var ts = this.textSizeEl;
		ts.innerHTML = '';
		ts.appendChild(document.createTextNode(v));
		v = ts.innerHTML;

		Tera.fly(ts).setWidth(this.el.getWidth());
		if (v.length < 1) {
			v = "&#160;&#160;";
		} else {
			if (Tera.isIE) {
				v = v.replace(/\n/g, '<p>&#160;</p>');
			}
			v += this.growAppend;
		}
		ts.innerHTML = v;
		var h = Math.min(this.growMax, Math.max(ts.offsetHeight, this.growMin) + this.growPad);
		if (h != this.lastHeight) {
			this.lastHeight = h;
			this.el.setHeight(h);
			this.fireEvent("autosize", this, h);
		}
	}
});
Tera.reg('textarea', Tera.form.TextArea);

Tera.form.NumberField = Tera.extend(Tera.form.TextField, {

	fieldClass :"x-form-field x-form-num-field",

	allowDecimals :true,

	//decimalSeparator :".",
	
	decimalSeparator :",",

	decimalPrecision :2,

	allowNegative :true,

	minValue :Number.NEGATIVE_INFINITY,

	maxValue :Number.MAX_VALUE,

	minText :"The minimum value for this field is {0}",

	maxText :"The maximum value for this field is {0}",

	nanText :"{0} is not a valid number",

	baseChars :"0123456789",

	initEvents : function() {
		Tera.form.NumberField.superclass.initEvents.call(this);
		var allowed = this.baseChars + '';
		if (this.allowDecimals) {
			allowed += this.decimalSeparator;
		}
		if (this.allowNegative) {
			allowed += "-";
		}
		this.stripCharsRe = new RegExp('[^' + allowed + ']', 'gi');
		var keyPress = function(e) {
			var k = e.getKey();
			if (!Tera.isIE && (e.isSpecialKey() || k == e.BACKSPACE || k == e.DELETE)) {
				return;
			}
			var c = e.getCharCode();
			if (allowed.indexOf(String.fromCharCode(c)) === -1) {
				e.stopEvent();
			}
		};
		this.el.on("keypress", keyPress, this);
	},

	validateValue : function(value) {
		if (!Tera.form.NumberField.superclass.validateValue.call(this, value)) {
			return false;
		}
		if (value.length < 1) {
			return true;
		}
		value = String(value).replace(this.decimalSeparator, ".");
		if (isNaN(value)) {
			this.markInvalid(String.format(this.nanText, value));
			return false;
		}
		var num = this.parseValue(value);
		if (num < this.minValue) {
			this.markInvalid(String.format(this.minText, this.minValue));
			return false;
		}
		if (num > this.maxValue) {
			this.markInvalid(String.format(this.maxText, this.maxValue));
			return false;
		}
		return true;
	},

	getValue : function() {
		return this.fixPrecision(this.parseValue(Tera.form.NumberField.superclass.getValue.call(this)));
	},

	setValue : function(v) {
		v = typeof v == 'number' ? v : parseFloat(String(v).replace(this.decimalSeparator, "."));
		v = isNaN(v) ? '' : String(v).replace(".", this.decimalSeparator);
		Tera.form.NumberField.superclass.setValue.call(this, v);
	},

	parseValue : function(value) {
		value = parseFloat(String(value).replace(this.decimalSeparator, "."));
		return isNaN(value) ? '' : value;
	},

	fixPrecision : function(value) {
		var nan = isNaN(value);
		if (!this.allowDecimals || this.decimalPrecision == -1 || nan || !value) {
			return nan ? '' : value;
		}
		return parseFloat(parseFloat(value).toFixed(this.decimalPrecision));
	},

	beforeBlur : function() {
		var v = this.parseValue(this.getRawValue());
		if (v) {
			this.setValue(this.fixPrecision(v));
		}
	}
});
Tera.reg('numberfield', Tera.form.NumberField);

Tera.form.DateField = Tera.extend(Tera.form.TriggerField, {

	format :"m/d/Y",

	altFormats :"m/d/Y|n/j/Y|n/j/y|m/j/y|n/d/y|m/j/Y|n/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d",

	disabledDays :null,

	disabledDaysText :"Disabled",

	disabledDates :null,

	enabledDay :null,
	
	disabledDatesText :"Disabled",

	disableCalendarButton : false,
	
	minValue :null,

	maxValue :null,

	minText :"The date in this field must be equal to or after {0}",

	maxText :"The date in this field must be equal to or before {0}",

	invalidText :"{0} is not a valid date - it must be in the format {1}",

	triggerClass :'x-form-date-trigger',

	defaultAutoCreate : {
		tag :"input",
		type :"text",
		size :"10",
		autocomplete :"off"
	},

	initComponent : function() {
		Tera.form.DateField.superclass.initComponent.call(this);
		if (typeof this.minValue == "string") {
			this.minValue = this.parseDate(this.minValue);
		}
		if (typeof this.maxValue == "string") {
			this.maxValue = this.parseDate(this.maxValue);
		}
		
		this.ddMatch = null;

		if (this.enabledDay && !this.disabledDates) {
			var disabledDates2 = "";
			for(i=1;i<=31;i++){
				if (i!=this.enabledDay) {
					if (i<=9)disabledDates2 += "0";
					disabledDates2 += i+"/../..";
					if (i!=31)disabledDates2 +="|";
				}
			}
			this.disabledDates = [disabledDates2];
		}
		if (this.disabledDates) {
			var dd = this.disabledDates;
			var re = "(?:";
			for ( var i = 0; i < dd.length; i++) {
				re += dd[i];
				if (i != dd.length - 1)
					re += "|";
			}
			this.ddMatch = new RegExp(re + ")");
		}
	},
	setEnabledDay : function(value) {
		this.ddMatch = null
		this.disabledDates = null;
		this.enabledDay = value;
		if (this.enabledDay && !this.disabledDates) {
			var disabledDates2 = "";
			for(i=1;i<=31;i++){
				if (i!=this.enabledDay) {
					if (i<=9)disabledDates2 += "0";
					disabledDates2 += i+"/../..";
					if (i!=31)disabledDates2 +="|";
				}
			}
			this.disabledDates = [disabledDates2];
		}
		if (this.disabledDates) {
			var dd = this.disabledDates;
			var re = "(?:";
			for ( var i = 0; i < dd.length; i++) {
				re += dd[i];
				if (i != dd.length - 1)
					re += "|";
			}
			this.ddMatch = new RegExp(re + ")");
		}
	},
	validateValue : function(value) {
		value = this.formatDate(value);
		if (!Tera.form.DateField.superclass.validateValue.call(this, value)) {
			return false;
		}
		if (value.length < 1) {
			return true;
		}
		var svalue = value;
		value = this.parseDate(value);
		if (!value) {
			this.markInvalid(String.format(this.invalidText, svalue, this.format));
			return false;
		}
		var time = value.getTime();
		if (this.minValue && time < this.minValue.getTime()) {
			this.markInvalid(String.format(this.minText, this.formatDate(this.minValue)));
			return false;
		}
		if (this.maxValue && time > this.maxValue.getTime()) {
			this.markInvalid(String.format(this.maxText, this.formatDate(this.maxValue)));
			return false;
		}
		if (this.disabledDays) {
			var day = value.getDay();
			for ( var i = 0; i < this.disabledDays.length; i++) {
				if (day === this.disabledDays[i]) {
					this.markInvalid(this.disabledDaysText);
					return false;
				}
			}
		}
		var fvalue = this.formatDate(value);
		if (this.ddMatch && this.ddMatch.test(fvalue)) {
			this.markInvalid(String.format(this.disabledDatesText, fvalue));
			return false;
		}
		return true;
	},

	validateBlur : function() {
		return !this.menu || !this.menu.isVisible();
	},

	getValue : function() {
		return this.parseDate(Tera.form.DateField.superclass.getValue.call(this)) || "";
	},

	setValue : function(date) {
		Tera.form.DateField.superclass.setValue.call(this, this.formatDate(this.parseDate(date)));
	},

	parseDate : function(value) {
		if (!value || Tera.isDate(value)) {
			return value;
		}
		var v = Date.parseDate(value, this.format);
		if (!v && this.altFormats) {
			if (!this.altFormatsArray) {
				this.altFormatsArray = this.altFormats.split("|");
			}
			for ( var i = 0, len = this.altFormatsArray.length; i < len && !v; i++) {
				v = Date.parseDate(value, this.altFormatsArray[i]);
			}
		}
		return v;
	},

	onDestroy : function() {
		if (this.menu) {
			this.menu.destroy();
		}
		if (this.wrap) {
			this.wrap.remove();
		}
		Tera.form.DateField.superclass.onDestroy.call(this);
	},

	formatDate : function(date) {
		return Tera.isDate(date) ? date.dateFormat(this.format) : date;
	},

	menuListeners : {
		select : function(m, d) {
			this.setValue(d);
		},
		show : function() {
			this.onFocus();
		},
		hide : function() {
			this.focus.defer(10, this);
			var ml = this.menuListeners;
			this.menu.un("select", ml.select, this);
			this.menu.un("show", ml.show, this);
			this.menu.un("hide", ml.hide, this);
		}
	},

	onTriggerClick : function() {
		if (this.disableCalendarButton) {
			return false;
		}
		if (this.disabled) {
			return;
		}
		if (this.menu == null) {
			this.menu = new Tera.menu.DateMenu();
		}
		Tera.apply(this.menu.picker, {
			minDate :this.minValue,
			maxDate :this.maxValue,
			disabledDatesRE :this.ddMatch,
			disabledDatesText :this.disabledDatesText,
			disabledDays :this.disabledDays,
			disabledDaysText :this.disabledDaysText,
			format :this.format,
			minText :String.format(this.minText, this.formatDate(this.minValue)),
			maxText :String.format(this.maxText, this.formatDate(this.maxValue))
		});
		this.menu.on(Tera.apply( {}, this.menuListeners, {
			scope :this
		}));
		this.menu.picker.setValue(this.getValue() || new Date());
		this.menu.show(this.el, "tl-bl?");
	},

	beforeBlur : function() {
		var v = this.parseDate(this.getRawValue());
		if (v) {
			this.setValue(v);
		}
	}

});
Tera.reg('datefield', Tera.form.DateField);

Tera.form.ComboBox = Tera.extend(Tera.form.TriggerField, {

	defaultAutoCreate : {
		tag :"input",
		type :"text",
		size :"24",
		autocomplete :"off"
	},

	listClass :'',

	selectedClass :'x-combo-selected',

	triggerClass :'x-form-arrow-trigger',

	shadow :'sides',

	listAlign :'tl-bl?',

	maxHeight :300,

	minHeight :90,

	triggerAction :'query',

	minChars :4,

	typeAhead :false,

	queryDelay :500,

	pageSize :0,

	selectOnFocus :false,

	queryParam :'query',

	loadingText :'Loading...',

	resizable :false,

	handleHeight :8,

	editable :true,

	allQuery :'',

	mode :'remote',

	minListWidth :70,

	forceSelection :false,

	typeAheadDelay :250,

	lazyInit :true,

	initComponent : function() {
		Tera.form.ComboBox.superclass.initComponent.call(this);
		this.addEvents(

		'expand',

		'collapse',

		'beforeselect',

		'select',

		'beforequery');
		if (this.transform) {
			this.allowDomMove = false;
			var s = Tera.getDom(this.transform);
			if (!this.hiddenName) {
				this.hiddenName = s.name;
			}
			if (!this.store) {
				this.mode = 'local';
				var d = [], opts = s.options;
				for ( var i = 0, len = opts.length; i < len; i++) {
					var o = opts[i];
					var value = (Tera.isIE ? o.getAttributeNode('value').specified : o.hasAttribute('value')) ? o.value : o.text;
					if (o.selected) {
						this.value = value;
					}
					d.push( [ value, o.text ]);
				}
				this.store = new Tera.data.SimpleStore( {
					'id' :0,
					fields : [ 'value', 'text' ],
					data :d
				});
				this.valueField = 'value';
				this.displayField = 'text';
			}
			s.name = Tera.id();
			if (!this.lazyRender) {
				this.target = true;
				this.el = Tera.DomHelper.insertBefore(s, this.autoCreate || this.defaultAutoCreate);
				Tera.removeNode(s);
				this.render(this.el.parentNode);
			} else {
				Tera.removeNode(s);
			}
		} else if (Tera.isArray(this.store)) {
			if (Tera.isArray(this.store[0])) {
				this.store = new Tera.data.SimpleStore( {
					fields : [ 'value', 'text' ],
					data :this.store
				});
				this.valueField = 'value';
			} else {
				this.store = new Tera.data.SimpleStore( {
					fields : [ 'text' ],
					data :this.store,
					expandData :true
				});
				this.valueField = 'text';
			}
			this.displayField = 'text';
			this.mode = 'local';
		}

		this.selectedIndex = -1;
		if (this.mode == 'local') {
			if (this.initialConfig.queryDelay === undefined) {
				this.queryDelay = 10;
			}
			if (this.initialConfig.minChars === undefined) {
				this.minChars = 0;
			}
		}
	},

	onRender : function(ct, position) {
		Tera.form.ComboBox.superclass.onRender.call(this, ct, position);
		if (this.hiddenName) {
			this.hiddenField = this.el.insertSibling( {
				tag :'input',
				type :'hidden',
				name :this.hiddenName,
				id :(this.hiddenId || this.hiddenName)
			}, 'before', true);
			this.hiddenField.value = this.hiddenValue !== undefined ? this.hiddenValue : this.value !== undefined ? this.value : '';

			this.el.dom.removeAttribute('name');
		}
		if (Tera.isGecko) {
			this.el.dom.setAttribute('autocomplete', 'off');
		}

		if (!this.lazyInit) {
			this.initList();
		} else {
			this.on('focus', this.initList, this, {
				single :true
			});
		}

		if (!this.editable) {
			this.editable = true;
			this.setEditable(false);
		}
	},

	initList : function() {
		if (!this.list) {
			var cls = 'x-combo-list';

			this.list = new Tera.Layer( {
				shadow :this.shadow,
				cls : [ cls, this.listClass ].join(' '),
				constrain :false
			});

			var lw = this.listWidth || Math.max(this.wrap.getWidth(), this.minListWidth);
			this.list.setWidth(lw);
			this.list.swallowEvent('mousewheel');
			this.assetHeight = 0;

			if (this.title) {
				this.header = this.list.createChild( {
					cls :cls + '-hd',
					html :this.title
				});
				this.assetHeight += this.header.getHeight();
			}

			this.innerList = this.list.createChild( {
				cls :cls + '-inner'
			});
			this.innerList.on('mouseover', this.onViewOver, this);
			this.innerList.on('mousemove', this.onViewMove, this);
			this.innerList.setWidth(lw - this.list.getFrameWidth('lr'));

			if (this.pageSize) {
				this.footer = this.list.createChild( {
					cls :cls + '-ft'
				});
				this.pageTb = new Tera.PagingToolbar( {
					store :this.store,
					pageSize :this.pageSize,
					renderTo :this.footer
				});
				this.assetHeight += this.footer.getHeight();
			}

			if (!this.tpl) {

				this.tpl = '<tpl for="."><div class="' + cls + '-item">{' + this.displayField + '}</div></tpl>';

			}

			this.view = new Tera.DataView( {
				applyTo :this.innerList,
				tpl :this.tpl,
				singleSelect :true,
				selectedClass :this.selectedClass,
				itemSelector :this.itemSelector || '.' + cls + '-item'
			});

			this.view.on('click', this.onViewClick, this);

			this.bindStore(this.store, true);

			if (this.resizable) {
				this.resizer = new Tera.Resizable(this.list, {
					pinned :true,
					handles :'se'
				});
				this.resizer.on('resize', function(r, w, h) {
					this.maxHeight = h - this.handleHeight - this.list.getFrameWidth('tb') - this.assetHeight;
					this.listWidth = w;
					this.innerList.setWidth(w - this.list.getFrameWidth('lr'));
					this.restrictHeight();
				}, this);
				this[this.pageSize ? 'footer' : 'innerList'].setStyle('margin-bottom', this.handleHeight + 'px');
			}
		}
	},

	bindStore : function(store, initial) {
		if (this.store && !initial) {
			this.store.un('beforeload', this.onBeforeLoad, this);
			this.store.un('load', this.onLoad, this);
			this.store.un('loadexception', this.collapse, this);
			if (!store) {
				this.store = null;
				if (this.view) {
					this.view.setStore(null);
				}
			}
		}
		if (store) {
			this.store = Tera.StoreMgr.lookup(store);

			this.store.on('beforeload', this.onBeforeLoad, this);
			this.store.on('load', this.onLoad, this);
			this.store.on('loadexception', this.collapse, this);

			if (this.view) {
				this.view.setStore(store);
			}
		}
	},

	initEvents : function() {
		Tera.form.ComboBox.superclass.initEvents.call(this);

		this.keyNav = new Tera.KeyNav(this.el, {
			"up" : function(e) {
				this.inKeyMode = true;
				this.selectPrev();
			},

			"down" : function(e) {
				if (!this.isExpanded()) {
					this.onTriggerClick();
				} else {
					this.inKeyMode = true;
					this.selectNext();
				}
			},

			"enter" : function(e) {
				this.onViewClick();
				this.delayedCheck = true;
				this.unsetDelayCheck.defer(10, this);
			},

			"esc" : function(e) {
				this.collapse();
			},

			"tab" : function(e) {
				this.onViewClick(false);
				return true;
			},

			scope :this,

			doRelay : function(foo, bar, hname) {
				if (hname == 'down' || this.scope.isExpanded()) {
					return Tera.KeyNav.prototype.doRelay.apply(this, arguments);
				}
				return true;
			},

			forceKeyDown :true
		});
		this.queryDelay = Math.max(this.queryDelay || 10, this.mode == 'local' ? 10 : 250);
		this.dqTask = new Tera.util.DelayedTask(this.initQuery, this);
		if (this.typeAhead) {
			this.taTask = new Tera.util.DelayedTask(this.onTypeAhead, this);
		}
		if (this.editable !== false) {
			this.el.on("keyup", this.onKeyUp, this);
		}
		if (this.forceSelection) {
			this.on('blur', this.doForce, this);
		}
	},

	onDestroy : function() {
		if (this.view) {
			this.view.el.removeAllListeners();
			this.view.el.remove();
			this.view.purgeListeners();
		}
		if (this.list) {
			this.list.destroy();
		}
		this.bindStore(null);
		Tera.form.ComboBox.superclass.onDestroy.call(this);
	},

	unsetDelayCheck : function() {
		delete this.delayedCheck;
	},
	fireKey : function(e) {
		if (e.isNavKeyPress() && !this.isExpanded() && !this.delayedCheck) {
			this.fireEvent("specialkey", this, e);
		}
	},

	onResize : function(w, h) {
		Tera.form.ComboBox.superclass.onResize.apply(this, arguments);
		if (this.list && this.listWidth === undefined) {
			var lw = Math.max(w, this.minListWidth);
			this.list.setWidth(lw);
			this.innerList.setWidth(lw - this.list.getFrameWidth('lr'));
		}
	},

	onEnable : function() {
		Tera.form.ComboBox.superclass.onEnable.apply(this, arguments);
		if (this.hiddenField) {
			this.hiddenField.disabled = false;
		}
	},

	onDisable : function() {
		Tera.form.ComboBox.superclass.onDisable.apply(this, arguments);
		if (this.hiddenField) {
			this.hiddenField.disabled = true;
		}
	},

	setEditable : function(value) {
		if (value == this.editable) {
			return;
		}
		this.editable = value;
		if (!value) {
			this.el.dom.setAttribute('readOnly', true);
			this.el.on('mousedown', this.onTriggerClick, this);
			this.el.addClass('x-combo-noedit');
		} else {
			this.el.dom.setAttribute('readOnly', false);
			this.el.un('mousedown', this.onTriggerClick, this);
			this.el.removeClass('x-combo-noedit');
		}
	},

	onBeforeLoad : function() {
		if (!this.hasFocus) {
			return;
		}
		this.innerList.update(this.loadingText ? '<div class="loading-indicator">' + this.loadingText + '</div>' : '');
		this.restrictHeight();
		this.selectedIndex = -1;
	},

	onLoad : function() {
		if (!this.hasFocus) {
			return;
		}
		if (this.store.getCount() > 0) {
			this.expand();
			this.restrictHeight();
			if (this.lastQuery == this.allQuery) {
				if (this.editable) {
					this.el.dom.select();
				}
				if (!this.selectByValue(this.value, true)) {
					this.select(0, true);
				}
			} else {
				this.selectNext();
				if (this.typeAhead && this.lastKey != Tera.EventObject.BACKSPACE && this.lastKey != Tera.EventObject.DELETE) {
					this.taTask.delay(this.typeAheadDelay);
				}
			}
		} else {
			this.onEmptyResults();
		}
	},

	onTypeAhead : function() {
		if (this.store.getCount() > 0) {
			var r = this.store.getAt(0);
			var newValue = r.data[this.displayField];
			var len = newValue.length;
			var selStart = this.getRawValue().length;
			if (selStart != len) {
				this.setRawValue(newValue);
				this.selectText(selStart, newValue.length);
			}
		}
	},

	onSelect : function(record, index) {
		if (this.fireEvent('beforeselect', this, record, index) !== false) {
			this.setValue(record.data[this.valueField || this.displayField]);
			this.collapse();
			this.fireEvent('select', this, record, index);
		}
	},

	getValue : function() {
		if (this.valueField) {
			return typeof this.value != 'undefined' ? this.value : '';
		} else {
			return Tera.form.ComboBox.superclass.getValue.call(this);
		}
	},

	clearValue : function() {
		if (this.hiddenField) {
			this.hiddenField.value = '';
		}
		this.setRawValue('');
		this.lastSelectionText = '';
		this.applyEmptyText();
		this.value = '';
	},

	setValue : function(v) {
		var text = v;
		if (this.valueField) {
			var r = this.findRecord(this.valueField, v);
			if (r) {
				text = r.data[this.displayField];
			} else if (this.valueNotFoundText !== undefined) {
				text = this.valueNotFoundText;
			}
		}
		this.lastSelectionText = text;
		if (this.hiddenField) {
			this.hiddenField.value = v;
		}
		Tera.form.ComboBox.superclass.setValue.call(this, text);
		this.value = v;
	},

	findRecord : function(prop, value) {
		var record;
		if (this.store.getCount() > 0) {
			this.store.each( function(r) {
				if (r.data[prop] == value) {
					record = r;
					return false;
				}
			});
		}
		return record;
	},

	onViewMove : function(e, t) {
		this.inKeyMode = false;
	},

	onViewOver : function(e, t) {
		if (this.inKeyMode) {
			return;
		}
		var item = this.view.findItemFromChild(t);
		if (item) {
			var index = this.view.indexOf(item);
			this.select(index, false);
		}
	},

	onViewClick : function(doFocus) {
		var index = this.view.getSelectedIndexes()[0];
		var r = this.store.getAt(index);
		if (r) {
			this.onSelect(r, index);
		}
		if (doFocus !== false) {
			this.el.focus();
		}
	},

	restrictHeight : function() {
		this.innerList.dom.style.height = '';
		var inner = this.innerList.dom;
		var pad = this.list.getFrameWidth('tb') + (this.resizable ? this.handleHeight : 0) + this.assetHeight;
		var h = Math.max(inner.clientHeight, inner.offsetHeight, inner.scrollHeight);
		var ha = this.getPosition()[1] - Tera.getBody().getScroll().top;
		var hb = Tera.lib.Dom.getViewHeight() - ha - this.getSize().height;
		var space = Math.max(ha, hb, this.minHeight || 0) - this.list.shadowOffset - pad - 5;
		h = Math.min(h, space, this.maxHeight);

		this.innerList.setHeight(h);
		this.list.beginUpdate();
		this.list.setHeight(h + pad);
		this.list.alignTo(this.wrap, this.listAlign);
		this.list.endUpdate();
	},

	onEmptyResults : function() {
		this.collapse();
	},

	isExpanded : function() {
		return this.list && this.list.isVisible();
	},

	selectByValue : function(v, scrollIntoView) {
		if (v !== undefined && v !== null) {
			var r = this.findRecord(this.valueField || this.displayField, v);
			if (r) {
				this.select(this.store.indexOf(r), scrollIntoView);
				return true;
			}
		}
		return false;
	},

	select : function(index, scrollIntoView) {
		this.selectedIndex = index;
		this.view.select(index);
		if (scrollIntoView !== false) {
			var el = this.view.getNode(index);
			if (el) {
				this.innerList.scrollChildIntoView(el, false);
			}
		}
	},

	selectNext : function() {
		var ct = this.store.getCount();
		if (ct > 0) {
			if (this.selectedIndex == -1) {
				this.select(0);
			} else if (this.selectedIndex < ct - 1) {
				this.select(this.selectedIndex + 1);
			}
		}
	},

	selectPrev : function() {
		var ct = this.store.getCount();
		if (ct > 0) {
			if (this.selectedIndex == -1) {
				this.select(0);
			} else if (this.selectedIndex != 0) {
				this.select(this.selectedIndex - 1);
			}
		}
	},

	onKeyUp : function(e) {
		if (this.editable !== false && !e.isSpecialKey()) {
			this.lastKey = e.getKey();
			this.dqTask.delay(this.queryDelay);
		}
	},

	validateBlur : function() {
		return !this.list || !this.list.isVisible();
	},

	initQuery : function() {
		this.doQuery(this.getRawValue());
	},

	doForce : function() {
		if (this.el.dom.value.length > 0) {
			this.el.dom.value = this.lastSelectionText === undefined ? '' : this.lastSelectionText;
			this.applyEmptyText();
		}
	},

	doQuery : function(q, forceAll) {
		if (q === undefined || q === null) {
			q = '';
		}
		var qe = {
			query :q,
			forceAll :forceAll,
			combo :this,
			cancel :false
		};
		if (this.fireEvent('beforequery', qe) === false || qe.cancel) {
			return false;
		}
		q = qe.query;
		forceAll = qe.forceAll;
		if (forceAll === true || (q.length >= this.minChars)) {
			if (this.lastQuery !== q) {
				this.lastQuery = q;
				if (this.mode == 'local') {
					this.selectedIndex = -1;
					if (forceAll) {
						this.store.clearFilter();
					} else {
						this.store.filter(this.displayField, q);
					}
					this.onLoad();
				} else {
					this.store.baseParams[this.queryParam] = q;
					this.store.load( {
						params :this.getParams(q)
					});
					this.expand();
				}
			} else {
				this.selectedIndex = -1;
				this.onLoad();
			}
		}
	},

	getParams : function(q) {
		var p = {};
		if (this.pageSize) {
			p.start = 0;
			p.limit = this.pageSize;
		}
		return p;
	},

	collapse : function() {
		if (!this.isExpanded()) {
			return;
		}
		this.list.hide();
		Tera.getDoc().un('mousewheel', this.collapseIf, this);
		Tera.getDoc().un('mousedown', this.collapseIf, this);
		this.fireEvent('collapse', this);
	},

	collapseIf : function(e) {
		if (!e.within(this.wrap) && !e.within(this.list)) {
			this.collapse();
		}
	},

	expand : function() {
		if (this.isExpanded() || !this.hasFocus) {
			return;
		}
		this.list.alignTo(this.wrap, this.listAlign);
		this.list.show();
		this.innerList.setOverflow('auto');
		Tera.getDoc().on('mousewheel', this.collapseIf, this);
		Tera.getDoc().on('mousedown', this.collapseIf, this);
		this.fireEvent('expand', this);
	},

	onTriggerClick : function() {
		if (this.disabled) {
			return;
		}
		if (this.isExpanded()) {
			this.collapse();
			this.el.focus();
		} else {
			this.onFocus( {});
			if (this.triggerAction == 'all') {
				this.doQuery(this.allQuery, true);
			} else {
				this.doQuery(this.getRawValue());
			}
			this.el.focus();
		}
	}

});
Tera.reg('combo', Tera.form.ComboBox);

Tera.form.Checkbox = Tera.extend(Tera.form.Field, {

	focusClass :undefined,

	fieldClass :"x-form-field",

	checked :false,

	defaultAutoCreate : {
		tag :"input",
		type :'checkbox',
		autocomplete :"off"
	},

	initComponent : function() {
		Tera.form.Checkbox.superclass.initComponent.call(this);
		this.addEvents(

		'check');
	},

	onResize : function() {
		Tera.form.Checkbox.superclass.onResize.apply(this, arguments);
		if (!this.boxLabel) {
			this.el.alignTo(this.wrap, 'c-c');
		}
	},

	initEvents : function() {
		Tera.form.Checkbox.superclass.initEvents.call(this);
		this.el.on("click", this.onClick, this);
		this.el.on("change", this.onClick, this);
	},

	getResizeEl : function() {
		return this.wrap;
	},

	getPositionEl : function() {
		return this.wrap;
	},

	markInvalid :Tera.emptyFn,

	clearInvalid :Tera.emptyFn,

	onRender : function(ct, position) {
		Tera.form.Checkbox.superclass.onRender.call(this, ct, position);
		if (this.inputValue !== undefined) {
			this.el.dom.value = this.inputValue;
		}
		this.wrap = this.el.wrap( {
			cls :"x-form-check-wrap"
		});
		if (this.boxLabel) {
			this.wrap.createChild( {
				tag :'label',
				htmlFor :this.el.id,
				cls :'x-form-cb-label',
				html :this.boxLabel
			});
		}
		if (this.checked) {
			this.setValue(true);
		} else {
			this.checked = this.el.dom.checked;
		}
	},

	onDestroy : function() {
		if (this.wrap) {
			this.wrap.remove();
		}
		Tera.form.Checkbox.superclass.onDestroy.call(this);
	},

	initValue :Tera.emptyFn,

	getValue : function() {
		if (this.rendered) {
			return this.el.dom.checked;
		}
		return false;
	},

	onClick : function() {
		if (this.el.dom.checked != this.checked) {
			this.setValue(this.el.dom.checked);
		}
	},

	setValue : function(v) {
		this.checked = (v === true || v === 'true' || v == '1' || String(v).toLowerCase() == 'on');
		if (this.el && this.el.dom) {
			this.el.dom.checked = this.checked;
			this.el.dom.defaultChecked = this.checked;
		}
		this.fireEvent("check", this, this.checked);
	}
});
Tera.reg('checkbox', Tera.form.Checkbox);

Tera.form.Radio = Tera.extend(Tera.form.Checkbox, {
	inputType :'radio',

	markInvalid :Tera.emptyFn,

	clearInvalid :Tera.emptyFn,

	getGroupValue : function() {
		var p = this.el.up('form') || Tera.getBody();
		var c = p.child('input[name=' + this.el.dom.name + ']:checked', true);
		return c ? c.value : null;
	},

	onClick : function() {
		if (this.el.dom.checked != this.checked) {
			var p = this.el.up('form') || Tera.getBody();
			var els = p.select('input[name=' + this.el.dom.name + ']');
			els.each( function(el) {
				if (el.dom.id == this.id) {
					this.setValue(true);
				} else {
					Tera.getCmp(el.dom.id).setValue(false);
				}
			}, this);
		}
	},

	setValue : function(v) {
		if (typeof v == 'boolean') {
			Tera.form.Radio.superclass.setValue.call(this, v);
		} else {
			var r = this.el.up('form').child('input[name=' + this.el.dom.name + '][value=' + v + ']', true);
			if (r) {
				r.checked = true;
			}
			;
		}
	}
});
Tera.reg('radio', Tera.form.Radio);

Tera.form.Hidden = Tera.extend(Tera.form.Field, {

	inputType :'hidden',

	onRender : function() {
		Tera.form.Hidden.superclass.onRender.apply(this, arguments);
	},

	initEvents : function() {
		this.originalValue = this.getValue();
	},

	setSize :Tera.emptyFn,
	setWidth :Tera.emptyFn,
	setHeight :Tera.emptyFn,
	setPosition :Tera.emptyFn,
	setPagePosition :Tera.emptyFn,
	markInvalid :Tera.emptyFn,
	clearInvalid :Tera.emptyFn
});
Tera.reg('hidden', Tera.form.Hidden);

Tera.form.BasicForm = function(el, config) {
	Tera.apply(this, config);

	this.items = new Tera.util.MixedCollection(false, function(o) {
		return o.id || (o.id = Tera.id());
	});
	this.addEvents(

	'beforeaction',

	'actionfailed',

	'actioncomplete');

	if (el) {
		this.initEl(el);
	}
	Tera.form.BasicForm.superclass.constructor.call(this);
};

Tera.extend(Tera.form.BasicForm, Tera.util.Observable, {

	//timeout :700,
	timeout :1800,

	activeAction :null,

	trackResetOnLoad :false,

	initEl : function(el) {
		this.el = Tera.get(el);
		this.id = this.el.id || Tera.id();
		if (!this.standardSubmit) {
			this.el.on('submit', this.onSubmit, this);
		}
		this.el.addClass('x-form');
	},

	getEl : function() {
		return this.el;
	},

	onSubmit : function(e) {
		e.stopEvent();
	},

	destroy : function() {
		this.items.each( function(f) {
			Tera.destroy(f);
		});
		if (this.el) {
			this.el.removeAllListeners();
			this.el.remove();
		}
		this.purgeListeners();
	},

	isValid : function() {
		var valid = true;
		this.items.each( function(f) {
			if (!f.validate()) {
				valid = false;
			}
		});
		return valid;
	},

	isDirty : function() {
		var dirty = false;
		this.items.each( function(f) {
			if (f.isDirty()) {
				dirty = true;
				return false;
			}
		});
		return dirty;
	},

	doAction : function(action, options) {
		if (typeof action == 'string') {
			action = new Tera.form.Action.ACTION_TYPES[action](this, options);
		}
		if (this.fireEvent('beforeaction', this, action) !== false) {
			this.beforeAction(action);
			action.run.defer(100, action);
		}
		return this;
	},

	submit : function(options) {
		if (this.standardSubmit) {
			var v = this.isValid();
			if (v) {
				this.el.dom.submit();
			}
			return v;
		}
		this.doAction('submit', options);
		return this;
	},

	load : function(options) {
		this.doAction('load', options);
		return this;
	},

	updateRecord : function(record) {
		record.beginEdit();
		var fs = record.fields;
		fs.each( function(f) {
			var field = this.findField(f.name);
			if (field) {
				record.set(f.name, field.getValue());
			}
		}, this);
		record.endEdit();
		return this;
	},

	loadRecord : function(record) {
		this.setValues(record.data);
		return this;
	},

	beforeAction : function(action) {
		var o = action.options;
		if (o.waitMsg) {
			if (this.waitMsgTarget === true) {
				this.el.mask(o.waitMsg, 'x-mask-loading');
			} else if (this.waitMsgTarget) {
				this.waitMsgTarget = Tera.get(this.waitMsgTarget);
				this.waitMsgTarget.mask(o.waitMsg, 'x-mask-loading');
			} else {
				Tera.MessageBox.wait(o.waitMsg, o.waitTitle || this.waitTitle || 'Espere Por Favor');
			}
		}
	},

	afterAction : function(action, success) {
		this.activeAction = null;
		var o = action.options;
		if (o.waitMsg) {
			if (this.waitMsgTarget === true) {
				this.el.unmask();
			} else if (this.waitMsgTarget) {
				this.waitMsgTarget.unmask();
			} else {
				Tera.MessageBox.updateProgress(1);
				Tera.MessageBox.hide();
			}
		}
		if (success) {
			if (o.reset) {
				this.reset();
			}
			Tera.callback(o.success, o.scope, [ this, action ]);
			this.fireEvent('actioncomplete', this, action);
		} else {
			Tera.callback(o.failure, o.scope, [ this, action ]);
			this.fireEvent('actionfailed', this, action);
		}
	},

	findField : function(id) {
		var field = this.items.get(id);
		if (!field) {
			this.items.each( function(f) {
				if (f.isFormField && (f.dataIndex == id || f.id == id || f.getName() == id)) {
					field = f;
					return false;
				}
			});
		}
		return field || null;
	},

	markInvalid : function(errors) {
		if (Tera.isArray(errors)) {
			for ( var i = 0, len = errors.length; i < len; i++) {
				var fieldError = errors[i];
				var f = this.findField(fieldError.id);
				if (f) {
					f.markInvalid(fieldError.msg);
				}
			}
		} else {
			var field, id;
			for (id in errors) {
				if (typeof errors[id] != 'function' && (field = this.findField(id))) {
					field.markInvalid(errors[id]);
				}
			}
		}
		return this;
	},

	setValues : function(values) {
		if (Tera.isArray(values)) {
			for ( var i = 0, len = values.length; i < len; i++) {
				var v = values[i];
				var f = this.findField(v.id);
				if (f) {
					f.setValue(v.value);
					if (this.trackResetOnLoad) {
						f.originalValue = f.getValue();
					}
				}
			}
		} else {
			var field, id;
			for (id in values) {
				if (typeof values[id] != 'function' && (field = this.findField(id))) {
					field.setValue(values[id]);
					if (this.trackResetOnLoad) {
						field.originalValue = field.getValue();
					}
				}
			}
		}
		return this;
	},

	getValues : function(asString) {
		var fs = Tera.lib.Ajax.serializeForm(this.el.dom);
		if (asString === true) {
			return fs;
		}
		return Tera.urlDecode(fs);
	},

	clearInvalid : function() {
		this.items.each( function(f) {
			f.clearInvalid();
		});
		return this;
	},

	reset : function() {
		this.items.each( function(f) {
			f.reset();
		});
		return this;
	},

	add : function() {
		this.items.addAll(Array.prototype.slice.call(arguments, 0));
		return this;
	},

	remove : function(field) {
		this.items.remove(field);
		return this;
	},

	render : function() {
		this.items.each( function(f) {
			if (f.isFormField && !f.rendered && document.getElementById(f.id)) {
				f.applyToMarkup(f.id);
			}
		});
		return this;
	},

	applyToFields : function(o) {
		this.items.each( function(f) {
			Tera.apply(f, o);
		});
		return this;
	},

	applyIfToFields : function(o) {
		this.items.each( function(f) {
			Tera.applyIf(f, o);
		});
		return this;
	}
});

Tera.BasicForm = Tera.form.BasicForm;

Tera.FormPanel = Tera.extend(Tera.Panel, {

	buttonAlign :'center',

	minButtonWidth :75,

	labelAlign :'left',

	monitorValid :false,

	monitorPoll :200,

	layout :'form',
	
	forceToUpper: false,

	initComponent : function() {
		this.form = this.createForm();

		Tera.FormPanel.superclass.initComponent.call(this);

		this.addEvents(

		'clientvalidation');

		this.relayEvents(this.form, [ 'beforeaction', 'actionfailed', 'actioncomplete' ]);
	},

	createForm : function() {
		delete this.initialConfig.listeners;
		return new Tera.form.BasicForm(null, this.initialConfig);
	},

	initFields : function() {
		var f = this.form;
		var formPanel = this;
		var fn = function(c) {
			if (c.doLayout && c != formPanel) {
				Tera.applyIf(c, {
					labelAlign :c.ownerCt.labelAlign,
					labelWidth :c.ownerCt.labelWidth,
					itemCls :c.ownerCt.itemCls
				});
				if (c.items) {
					c.items.each(fn);
				}
			} else if (c.isFormField) {
				f.add(c);
			}
		}
		this.items.each(fn);
	},

	getLayoutTarget : function() {
		return this.form.el;
	},

	getForm : function() {
		return this.form;
	},

	onRender : function(ct, position) {
		this.initFields();

		Tera.FormPanel.superclass.onRender.call(this, ct, position);
		var o = {
			tag :'form',
			method :this.method || 'POST',
			id :this.formId || Tera.id()
		};
		if (this.fileUpload) {
			o.enctype = 'multipart/form-data';
		}
		this.form.initEl(this.body.createChild(o));
	},

	beforeDestroy : function() {
		Tera.FormPanel.superclass.beforeDestroy.call(this);
		Tera.destroy(this.form);
	},

	initEvents : function() {
		Tera.FormPanel.superclass.initEvents.call(this);
		this.items.on('remove', this.onRemove, this);
		this.items.on('add', this.onAdd, this);
		if (this.monitorValid) {
			this.startMonitoring();
		}
	},

	onAdd : function(ct, c) {
		if (c.isFormField) {
			this.form.add(c);
		}
	},

	onRemove : function(c) {
		if (c.isFormField) {
			Tera.destroy(c.container.up('.x-form-item'));
			this.form.remove(c);
		}
	},

	startMonitoring : function() {
		if (!this.bound) {
			this.bound = true;
			Tera.TaskMgr.start( {
				run :this.bindHandler,
				interval :this.monitorPoll || 200,
				scope :this
			});
		}
	},

	stopMonitoring : function() {
		this.bound = false;
	},

	load : function() {
		this.form.load.apply(this.form, arguments);
	},

	onDisable : function() {
		Tera.FormPanel.superclass.onDisable.call(this);
		if (this.form) {
			this.form.items.each( function() {
				this.disable();
			});
		}
	},

	onEnable : function() {
		Tera.FormPanel.superclass.onEnable.call(this);
		if (this.form) {
			this.form.items.each( function() {
				this.enable();
			});
		}
	},

	bindHandler : function() {
		if (!this.bound) {
			return false;
		}
		var valid = true;
		this.form.items.each( function(f) {
			if (!f.isValid(true)) {
				valid = false;
				return false;
			}
		});
		if (this.buttons) {
			for ( var i = 0, len = this.buttons.length; i < len; i++) {
				var btn = this.buttons[i];
				if (btn.formBind === true && btn.disabled === valid) {
					btn.setDisabled(!valid);
				}
			}
		}
		this.fireEvent('clientvalidation', this, valid);
	}
});
Tera.reg('form', Tera.FormPanel);

Tera.form.FormPanel = Tera.FormPanel;

Tera.form.FieldSet = Tera.extend(Tera.Panel, {

	baseCls :'x-fieldset',

	layout :'form',

	onRender : function(ct, position) {
		if (!this.el) {
			this.el = document.createElement('fieldset');
			this.el.id = this.id;
			if (this.title || this.header || this.checkboxToggle) {
				this.el.appendChild(document.createElement('legend')).className = 'x-fieldset-header';
			}
		}

		Tera.form.FieldSet.superclass.onRender.call(this, ct, position);

		if (this.checkboxToggle) {
			var o = typeof this.checkboxToggle == 'object' ? this.checkboxToggle : {
				tag :'input',
				type :'checkbox',
				name :this.checkboxName || this.id + '-checkbox'
			};
			this.checkbox = this.header.insertFirst(o);
			this.checkbox.dom.checked = !this.collapsed;
			this.checkbox.on('click', this.onCheckClick, this);
		}
	},

	onCollapse : function(doAnim, animArg) {
		if (this.checkbox) {
			this.checkbox.dom.checked = false;
		}
		this.afterCollapse();

	},

	onExpand : function(doAnim, animArg) {
		if (this.checkbox) {
			this.checkbox.dom.checked = true;
		}
		this.afterExpand();
	},

	onCheckClick : function() {
		this[this.checkbox.dom.checked ? 'expand' : 'collapse']();
	}

});
Tera.reg('fieldset', Tera.form.FieldSet);

Tera.form.HtmlEditor = Tera
		.extend(
				Tera.form.Field,
				{

					enableFormat :true,

					enableFontSize :true,

					enableColors :true,

					enableAlignments :true,

					enableLists :true,

					enableSourceEdit :true,

					enableLinks :true,

					enableFont :true,

					createLinkText :'Please enter the URL for the link:',

					defaultLinkValue :'http:/' + '/',

					fontFamilies : [ 'Arial', 'Courier New', 'Tahoma', 'Times New Roman', 'Verdana' ],
					defaultFont :'tahoma',

					validationEvent :false,
					deferHeight :true,
					initialized :false,
					activated :false,
					sourceEditMode :false,
					onFocus :Tera.emptyFn,
					iframePad :3,
					hideMode :'offsets',
					defaultAutoCreate : {
						tag :"textarea",
						style :"width:500px;height:300px;",
						autocomplete :"off"
					},

					initComponent : function() {
						this.addEvents(

						'initialize',

						'activate',

						'beforesync',

						'beforepush',

						'sync',

						'push',

						'editmodechange')
					},

					createFontOptions : function() {
						var buf = [], fs = this.fontFamilies, ff, lc;
						for ( var i = 0, len = fs.length; i < len; i++) {
							ff = fs[i];
							lc = ff.toLowerCase();
							buf.push('<option value="', lc, '" style="font-family:', ff, ';"', (this.defaultFont == lc ? ' selected="true">' : '>'),
									ff, '</option>');
						}
						return buf.join('');
					},

					createToolbar : function(editor) {

						var tipsEnabled = Tera.QuickTips && Tera.QuickTips.isEnabled();

						function btn(id, toggle, handler) {
							return {
								itemId :id,
								cls :'x-btn-icon x-edit-' + id,
								enableToggle :toggle !== false,
								scope :editor,
								handler :handler || editor.relayBtnCmd,
								clickEvent :'mousedown',
								tooltip :tipsEnabled ? editor.buttonTips[id] || undefined : undefined,
								tabIndex :-1
							};
						}

						var tb = new Tera.Toolbar( {
							renderTo :this.wrap.dom.firstChild
						});

						tb.el.on('click', function(e) {
							e.preventDefault();
						});

						if (this.enableFont && !Tera.isSafari) {
							this.fontSelect = tb.el.createChild( {
								tag :'select',
								cls :'x-font-select',
								html :this.createFontOptions()
							});
							this.fontSelect.on('change', function() {
								var font = this.fontSelect.dom.value;
								this.relayCmd('fontname', font);
								this.deferFocus();
							}, this);
							tb.add(this.fontSelect.dom, '-');
						}
						;

						if (this.enableFormat) {
							tb.add(btn('bold'), btn('italic'), btn('underline'));
						}
						;

						if (this.enableFontSize) {
							tb.add('-', btn('increasefontsize', false, this.adjustFont), btn('decreasefontsize', false, this.adjustFont));
						}
						;

						if (this.enableColors) {
							tb.add('-', {
								itemId :'forecolor',
								cls :'x-btn-icon x-edit-forecolor',
								clickEvent :'mousedown',
								tooltip :tipsEnabled ? editor.buttonTips['forecolor'] || undefined : undefined,
								tabIndex :-1,
								menu :new Tera.menu.ColorMenu( {
									allowReselect :true,
									focus :Tera.emptyFn,
									value :'000000',
									plain :true,
									selectHandler : function(cp, color) {
										this.execCmd('forecolor', Tera.isSafari || Tera.isIE ? '#' + color : color);
										this.deferFocus();
									},
									scope :this,
									clickEvent :'mousedown'
								})
							}, {
								itemId :'backcolor',
								cls :'x-btn-icon x-edit-backcolor',
								clickEvent :'mousedown',
								tooltip :tipsEnabled ? editor.buttonTips['backcolor'] || undefined : undefined,
								tabIndex :-1,
								menu :new Tera.menu.ColorMenu( {
									focus :Tera.emptyFn,
									value :'FFFFFF',
									plain :true,
									allowReselect :true,
									selectHandler : function(cp, color) {
										if (Tera.isGecko) {
											this.execCmd('useCSS', false);
											this.execCmd('hilitecolor', color);
											this.execCmd('useCSS', true);
											this.deferFocus();
										} else {
											this
													.execCmd(Tera.isOpera ? 'hilitecolor' : 'backcolor', Tera.isSafari || Tera.isIE ? '#' + color
															: color);
											this.deferFocus();
										}
									},
									scope :this,
									clickEvent :'mousedown'
								})
							});
						}
						;

						if (this.enableAlignments) {
							tb.add('-', btn('justifyleft'), btn('justifycenter'), btn('justifyright'));
						}
						;

						if (!Tera.isSafari) {
							if (this.enableLinks) {
								tb.add('-', btn('createlink', false, this.createLink));
							}
							;

							if (this.enableLists) {
								tb.add('-', btn('insertorderedlist'), btn('insertunorderedlist'));
							}
							if (this.enableSourceEdit) {
								tb.add('-', btn('sourceedit', true, function(btn) {
									this.toggleSourceEdit(btn.pressed);
								}));
							}
						}

						this.tb = tb;
					},

					getDocMarkup : function() {
						return '<html><head><style type="text/css">body{border:0;margin:0;padding:3px;height:98%;cursor:text;}</style></head><body></body></html>';
					},

					getEditorBody : function() {
						return this.doc.body || this.doc.documentElement;
					},

					onRender : function(ct, position) {
						Tera.form.HtmlEditor.superclass.onRender.call(this, ct, position);
						this.el.dom.style.border = '0 none';
						this.el.dom.setAttribute('tabIndex', -1);
						this.el.addClass('x-hidden');
						if (Tera.isIE) {
							this.el.applyStyles('margin-top:-1px;margin-bottom:-1px;')
						}
						this.wrap = this.el.wrap( {
							cls :'x-html-editor-wrap',
							cn : {
								cls :'x-html-editor-tb'
							}
						});

						this.createToolbar(this);

						this.tb.items.each( function(item) {
							if (item.itemId != 'sourceedit') {
								item.disable();
							}
						});

						var iframe = document.createElement('iframe');
						iframe.name = Tera.id();
						iframe.frameBorder = 'no';

						iframe.src = (Tera.SSL_SECURE_URL || "javascript:false");

						this.wrap.dom.appendChild(iframe);

						this.iframe = iframe;

						if (Tera.isIE) {
							iframe.contentWindow.document.designMode = 'on';
							this.doc = iframe.contentWindow.document;
							this.win = iframe.contentWindow;
						} else {
							this.doc = (iframe.contentDocument || window.frames[iframe.name].document);
							this.win = window.frames[iframe.name];
							this.doc.designMode = 'on';
						}
						this.doc.open();
						this.doc.write(this.getDocMarkup())
						this.doc.close();

						var task = {
							run : function() {
								if (this.doc.body || this.doc.readyState == 'complete') {
									Tera.TaskMgr.stop(task);
									this.doc.designMode = "on";
									this.initEditor.defer(10, this);
								}
							},
							interval :10,
							duration :10000,
							scope :this
						};
						Tera.TaskMgr.start(task);

						if (!this.width) {
							this.setSize(this.el.getSize());
						}
					},

					onResize : function(w, h) {
						Tera.form.HtmlEditor.superclass.onResize.apply(this, arguments);
						if (this.el && this.iframe) {
							if (typeof w == 'number') {
								var aw = w - this.wrap.getFrameWidth('lr');
								this.el.setWidth(this.adjustWidth('textarea', aw));
								this.iframe.style.width = aw + 'px';
							}
							if (typeof h == 'number') {
								var ah = h - this.wrap.getFrameWidth('tb') - this.tb.el.getHeight();
								this.el.setHeight(this.adjustWidth('textarea', ah));
								this.iframe.style.height = ah + 'px';
								if (this.doc) {
									this.getEditorBody().style.height = (ah - (this.iframePad * 2)) + 'px';
								}
							}
						}
					},

					toggleSourceEdit : function(sourceEditMode) {
						if (sourceEditMode === undefined) {
							sourceEditMode = !this.sourceEditMode;
						}
						this.sourceEditMode = sourceEditMode === true;
						var btn = this.tb.items.get('sourceedit');
						if (btn.pressed !== this.sourceEditMode) {
							btn.toggle(this.sourceEditMode);
							return;
						}
						if (this.sourceEditMode) {
							this.tb.items.each( function(item) {
								if (item.itemId != 'sourceedit') {
									item.disable();
								}
							});
							this.syncValue();
							this.iframe.className = 'x-hidden';
							this.el.removeClass('x-hidden');
							this.el.dom.removeAttribute('tabIndex');
							this.el.focus();
						} else {
							if (this.initialized) {
								this.tb.items.each( function(item) {
									item.enable();
								});
							}
							this.pushValue();
							this.iframe.className = '';
							this.el.addClass('x-hidden');
							this.el.dom.setAttribute('tabIndex', -1);
							this.deferFocus();
						}
						var lastSize = this.lastSize;
						if (lastSize) {
							delete this.lastSize;
							this.setSize(lastSize);
						}
						this.fireEvent('editmodechange', this, this.sourceEditMode);
					},

					createLink : function() {
						var url = prompt(this.createLinkText, this.defaultLinkValue);
						if (url && url != 'http:/' + '/') {
							this.relayCmd('createlink', url);
						}
					},

					adjustSize :Tera.BoxComponent.prototype.adjustSize,

					getResizeEl : function() {
						return this.wrap;
					},

					getPositionEl : function() {
						return this.wrap;
					},

					initEvents : function() {
						this.originalValue = this.getValue();
					},

					markInvalid :Tera.emptyFn,

					clearInvalid :Tera.emptyFn,

					setValue : function(v) {
						Tera.form.HtmlEditor.superclass.setValue.call(this, v);
						this.pushValue();
					},

					cleanHtml : function(html) {
						html = String(html);
						if (html.length > 5) {
							if (Tera.isSafari) {
								html = html.replace(/\sclass="(?:Apple-style-span|khtml-block-placeholder)"/gi, '');
							}
						}
						if (html == '&nbsp;') {
							html = '';
						}
						return html;
					},

					syncValue : function() {
						if (this.initialized) {
							var bd = this.getEditorBody();
							var html = bd.innerHTML;
							if (Tera.isSafari) {
								var bs = bd.getAttribute('style');
								var m = bs.match(/text-align:(.*?);/i);
								if (m && m[1]) {
									html = '<div style="' + m[0] + '">' + html + '</div>';
								}
							}
							html = this.cleanHtml(html);
							if (this.fireEvent('beforesync', this, html) !== false) {
								this.el.dom.value = html;
								this.fireEvent('sync', this, html);
							}
						}
					},

					pushValue : function() {
						if (this.initialized) {
							var v = this.el.dom.value;
							if (!this.activated && v.length < 1) {
								v = '&nbsp;';
							}
							if (this.fireEvent('beforepush', this, v) !== false) {
								this.getEditorBody().innerHTML = v;
								this.fireEvent('push', this, v);
							}
						}
					},

					deferFocus : function() {
						this.focus.defer(10, this);
					},

					focus : function() {
						if (this.win && !this.sourceEditMode) {
							this.win.focus();
						} else {
							this.el.focus();
						}
					},

					initEditor : function() {
						var dbody = this.getEditorBody();
						var ss = this.el.getStyles('font-size', 'font-family', 'background-image', 'background-repeat');
						ss['background-attachment'] = 'fixed';
						dbody.bgProperties = 'fixed';
						Tera.DomHelper.applyStyles(dbody, ss);
						Tera.EventManager.on(this.doc, {
							'mousedown' :this.onEditorEvent,
							'dblclick' :this.onEditorEvent,
							'click' :this.onEditorEvent,
							'keyup' :this.onEditorEvent,
							buffer :100,
							scope :this
						});
						if (Tera.isGecko) {
							Tera.EventManager.on(this.doc, 'keypress', this.applyCommand, this);
						}
						if (Tera.isIE || Tera.isSafari || Tera.isOpera) {
							Tera.EventManager.on(this.doc, 'keydown', this.fixKeys, this);
						}
						this.initialized = true;

						this.fireEvent('initialize', this);
						this.pushValue();
					},

					onDestroy : function() {
						if (this.rendered) {
							this.tb.items.each( function(item) {
								if (item.menu) {
									item.menu.removeAll();
									if (item.menu.el) {
										item.menu.el.destroy();
									}
								}
								item.destroy();
							});
							this.wrap.dom.innerHTML = '';
							this.wrap.remove();
						}
					},

					onFirstFocus : function() {
						this.activated = true;
						this.tb.items.each( function(item) {
							item.enable();
						});
						if (Tera.isGecko) {
							this.win.focus();
							var s = this.win.getSelection();
							if (!s.focusNode || s.focusNode.nodeType != 3) {
								var r = s.getRangeAt(0);
								r.selectNodeContents(this.getEditorBody());
								r.collapse(true);
								this.deferFocus();
							}
							try {
								this.execCmd('useCSS', true);
								this.execCmd('styleWithCSS', false);
							} catch (e) {
							}
						}
						this.fireEvent('activate', this);
					},

					adjustFont : function(btn) {
						var adjust = btn.itemId == 'increasefontsize' ? 1 : -1;

						var v = parseInt(this.doc.queryCommandValue('FontSize') || 2, 10);
						if (Tera.isSafari3 || Tera.isAir) {

							if (v <= 10) {
								v = 1 + adjust;
							} else if (v <= 13) {
								v = 2 + adjust;
							} else if (v <= 16) {
								v = 3 + adjust;
							} else if (v <= 18) {
								v = 4 + adjust;
							} else if (v <= 24) {
								v = 5 + adjust;
							} else {
								v = 6 + adjust;
							}
							v = v.constrain(1, 6);
						} else {
							if (Tera.isSafari) {
								adjust *= 2;
							}
							v = Math.max(1, v + adjust) + (Tera.isSafari ? 'px' : 0);
						}
						this.execCmd('FontSize', v);
					},

					onEditorEvent : function(e) {
						this.updateToolbar();
					},

					updateToolbar : function() {

						if (!this.activated) {
							this.onFirstFocus();
							return;
						}

						var btns = this.tb.items.map, doc = this.doc;

						if (this.enableFont && !Tera.isSafari) {
							var name = (this.doc.queryCommandValue('FontName') || this.defaultFont).toLowerCase();
							if (name != this.fontSelect.dom.value) {
								this.fontSelect.dom.value = name;
							}
						}
						if (this.enableFormat) {
							btns.bold.toggle(doc.queryCommandState('bold'));
							btns.italic.toggle(doc.queryCommandState('italic'));
							btns.underline.toggle(doc.queryCommandState('underline'));
						}
						if (this.enableAlignments) {
							btns.justifyleft.toggle(doc.queryCommandState('justifyleft'));
							btns.justifycenter.toggle(doc.queryCommandState('justifycenter'));
							btns.justifyright.toggle(doc.queryCommandState('justifyright'));
						}
						if (!Tera.isSafari && this.enableLists) {
							btns.insertorderedlist.toggle(doc.queryCommandState('insertorderedlist'));
							btns.insertunorderedlist.toggle(doc.queryCommandState('insertunorderedlist'));
						}

						Tera.menu.MenuMgr.hideAll();

						this.syncValue();
					},

					relayBtnCmd : function(btn) {
						this.relayCmd(btn.itemId);
					},

					relayCmd : function(cmd, value) {
						this.win.focus();
						this.execCmd(cmd, value);
						this.updateToolbar();
						this.deferFocus();
					},

					execCmd : function(cmd, value) {
						this.doc.execCommand(cmd, false, value === undefined ? null : value);
						this.syncValue();
					},

					applyCommand : function(e) {
						if (e.ctrlKey) {
							var c = e.getCharCode(), cmd;
							if (c > 0) {
								c = String.fromCharCode(c);
								switch (c) {
									case 'b':
										cmd = 'bold';
										break;
									case 'i':
										cmd = 'italic';
										break;
									case 'u':
										cmd = 'underline';
										break;
								}
								if (cmd) {
									this.win.focus();
									this.execCmd(cmd);
									this.deferFocus();
									e.preventDefault();
								}
							}
						}
					},

					insertAtCursor : function(text) {
						if (!this.activated) {
							return;
						}
						if (Tera.isIE) {
							this.win.focus();
							var r = this.doc.selection.createRange();
							if (r) {
								r.collapse(true);
								r.pasteHTML(text);
								this.syncValue();
								this.deferFocus();
							}
						} else if (Tera.isGecko || Tera.isOpera) {
							this.win.focus();
							this.execCmd('InsertHTML', text);
							this.deferFocus();
						} else if (Tera.isSafari) {
							this.execCmd('InsertText', text);
							this.deferFocus();
						}
					},

					fixKeys : function() {
						if (Tera.isIE) {
							return function(e) {
								var k = e.getKey(), r;
								if (k == e.TAB) {
									e.stopEvent();
									r = this.doc.selection.createRange();
									if (r) {
										r.collapse(true);
										r.pasteHTML('&nbsp;&nbsp;&nbsp;&nbsp;');
										this.deferFocus();
									}
								} else if (k == e.ENTER) {
									r = this.doc.selection.createRange();
									if (r) {
										var target = r.parentElement();
										if (!target || target.tagName.toLowerCase() != 'li') {
											e.stopEvent();
											r.pasteHTML('<br />');
											r.collapse(false);
											r.select();
										}
									}
								}
							};
						} else if (Tera.isOpera) {
							return function(e) {
								var k = e.getKey();
								if (k == e.TAB) {
									e.stopEvent();
									this.win.focus();
									this.execCmd('InsertHTML', '&nbsp;&nbsp;&nbsp;&nbsp;');
									this.deferFocus();
								}
							};
						} else if (Tera.isSafari) {
							return function(e) {
								var k = e.getKey();
								if (k == e.TAB) {
									e.stopEvent();
									this.execCmd('InsertText', '\t');
									this.deferFocus();
								}
							};
						}
					}(),

					getToolbar : function() {
						return this.tb;
					},

					buttonTips : {
						bold : {
							title :'Bold (Ctrl+B)',
							text :'Make the selected text bold.',
							cls :'x-html-editor-tip'
						},
						italic : {
							title :'Italic (Ctrl+I)',
							text :'Make the selected text italic.',
							cls :'x-html-editor-tip'
						},
						underline : {
							title :'Underline (Ctrl+U)',
							text :'Underline the selected text.',
							cls :'x-html-editor-tip'
						},
						increasefontsize : {
							title :'Grow Text',
							text :'Increase the font size.',
							cls :'x-html-editor-tip'
						},
						decreasefontsize : {
							title :'Shrink Text',
							text :'Decrease the font size.',
							cls :'x-html-editor-tip'
						},
						backcolor : {
							title :'Text Highlight Color',
							text :'Change the background color of the selected text.',
							cls :'x-html-editor-tip'
						},
						forecolor : {
							title :'Font Color',
							text :'Change the color of the selected text.',
							cls :'x-html-editor-tip'
						},
						justifyleft : {
							title :'Align Text Left',
							text :'Align text to the left.',
							cls :'x-html-editor-tip'
						},
						justifycenter : {
							title :'Center Text',
							text :'Center text in the editor.',
							cls :'x-html-editor-tip'
						},
						justifyright : {
							title :'Align Text Right',
							text :'Align text to the right.',
							cls :'x-html-editor-tip'
						},
						insertunorderedlist : {
							title :'Bullet List',
							text :'Start a bulleted list.',
							cls :'x-html-editor-tip'
						},
						insertorderedlist : {
							title :'Numbered List',
							text :'Start a numbered list.',
							cls :'x-html-editor-tip'
						},
						createlink : {
							title :'Hyperlink',
							text :'Make the selected text a hyperlink.',
							cls :'x-html-editor-tip'
						},
						sourceedit : {
							title :'Source Edit',
							text :'Switch to source editing mode.',
							cls :'x-html-editor-tip'
						}
					}

				});
Tera.reg('htmleditor', Tera.form.HtmlEditor);

Tera.form.TimeField = Tera.extend(Tera.form.ComboBox, {

	minValue :null,

	maxValue :null,

	minText :"The time in this field must be equal to or after {0}",

	maxText :"The time in this field must be equal to or before {0}",

	invalidText :"{0} is not a valid time",

	format :"g:i A",

	altFormats :"g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H",

	increment :15,

	mode :'local',

	triggerAction :'all',

	typeAhead :false,

	initComponent : function() {
		Tera.form.TimeField.superclass.initComponent.call(this);

		if (typeof this.minValue == "string") {
			this.minValue = this.parseDate(this.minValue);
		}
		if (typeof this.maxValue == "string") {
			this.maxValue = this.parseDate(this.maxValue);
		}

		if (!this.store) {
			var min = this.parseDate(this.minValue);
			if (!min) {
				min = new Date().clearTime();
			}
			var max = this.parseDate(this.maxValue);
			if (!max) {
				max = new Date().clearTime().add('mi', (24 * 60) - 1);
			}
			var times = [];
			while (min <= max) {
				times.push( [ min.dateFormat(this.format) ]);
				min = min.add('mi', this.increment);
			}
			this.store = new Tera.data.SimpleStore( {
				fields : [ 'text' ],
				data :times
			});
			this.displayField = 'text';
		}
	},

	getValue : function() {
		var v = Tera.form.TimeField.superclass.getValue.call(this);
		return this.formatDate(this.parseDate(v)) || '';
	},

	setValue : function(value) {
		Tera.form.TimeField.superclass.setValue.call(this, this.formatDate(this.parseDate(value)));
	},

	validateValue :Tera.form.DateField.prototype.validateValue,
	parseDate :Tera.form.DateField.prototype.parseDate,
	formatDate :Tera.form.DateField.prototype.formatDate,

	beforeBlur : function() {
		var v = this.parseDate(this.getRawValue());
		if (v) {
			this.setValue(v.dateFormat(this.format));
		}
	}

});
Tera.reg('timefield', Tera.form.TimeField);

Tera.form.Label = Tera.extend(Tera.BoxComponent, {

	onRender : function(ct, position) {
		if (!this.el) {
			this.el = document.createElement('label');
			this.el.id = this.getId();
			this.el.innerHTML = this.text ? Tera.util.Format.htmlEncode(this.text) : (this.html || '');
			if (this.forId) {
				this.el.setAttribute('htmlFor', this.forId);
			}
		}
		Tera.form.Label.superclass.onRender.call(this, ct, position);
	},
//agregado manfredi frame 2.3
	
setText: function(t, encode){
    var e = encode === false;
    this[!e ? 'text' : 'html'] = t;
    delete this[e ? 'text' : 'html'];
    if(this.rendered){
        this.el.dom.innerHTML = encode !== false ? Tera.util.Format.htmlEncode(t) : t;
    }
    return this;
}


});




Tera.reg('label', Tera.form.Label);

Tera.form.Action = function(form, options) {
	this.form = form;
	
	if (!Tera.isEmpty(this.form)) {
		if (!Tera.isEmpty(this.form.items)) {
			if (!Tera.isEmpty(this.form.items.items)) {
				var iter = this.form.items.items; 
				
				iter.each(function(item, idx){
					if ((item.getXType() == 'textfield' || item.getXType() == 'lupafield') && item.applyToUpper) {
						item.setValue(item.getValue());
					}
				});		
			}
		}
	}
		
	this.options = options || {};
};

Tera.form.Action.CLIENT_INVALID = 'client';

Tera.form.Action.SERVER_INVALID = 'server';

Tera.form.Action.CONNECT_FAILURE = 'connect';

Tera.form.Action.LOAD_FAILURE = 'load';

Tera.form.Action.prototype = {

	type :'default',

	run : function(options) {

	},

	success : function(response) {

	},

	handleResponse : function(response) {

	},

	failure : function(response) {
		this.response = response;
		this.failureType = Tera.form.Action.CONNECT_FAILURE;
		this.form.afterAction(this, false);
	},

	processResponse : function(response) {
		this.response = response;
		if (!response.responseText) {
			return true;
		}
		this.result = this.handleResponse(response);
		return this.result;
	},

	getUrl : function(appendParams) {
		var url = this.options.url || this.form.url || this.form.el.dom.action;
		if (appendParams) {
			var p = this.getParams();
			if (p) {
				url += (url.indexOf('?') != -1 ? '&' : '?') + p;
			}
		}
		return url;
	},

	getMethod : function() {
		return (this.options.method || this.form.method || this.form.el.dom.method || 'POST').toUpperCase();
	},

	getParams : function() {
		var bp = this.form.baseParams;
		var p = this.options.params;
		if (p) {
			if (typeof p == "object") {
				p = Tera.urlEncode(Tera.applyIf(p, bp));
			} else if (typeof p == 'string' && bp) {
				p += '&' + Tera.urlEncode(bp);
			}
		} else if (bp) {
			p = Tera.urlEncode(bp);
		}
		return p;
	},

	createCallback : function(opts) {
		var opts = opts || {};
		return {
			success :this.success,
			failure :this.failure,
			scope :this,
			timeout :(opts.timeout * 1000) || (this.form.timeout * 1000),
			upload :this.form.fileUpload ? this.success : undefined
		};
	}
};

Tera.form.Action.Submit = function(form, options) {
	Tera.form.Action.Submit.superclass.constructor.call(this, form, options);
};

Tera.extend(Tera.form.Action.Submit, Tera.form.Action, {

	type :'submit',

	run : function() {
		var o = this.options;
		var method = this.getMethod();
		var isGet = method == 'GET';
		if (o.clientValidation === false || this.form.isValid()) {
			Tera.Ajax.request(Tera.apply(this.createCallback(o), {
				form :this.form.el.dom,
				url :this.getUrl(isGet),
				method :method,
				headers :o.headers,
				params :!isGet ? this.getParams() : null,
				isUpload :this.form.fileUpload
			}));
		} else if (o.clientValidation !== false) {
			this.failureType = Tera.form.Action.CLIENT_INVALID;
			this.form.afterAction(this, false);
		}
	},

	success : function(response) {
		var result = this.processResponse(response);
		if (result === true || result.success) {
			this.form.afterAction(this, true);
			return;
		}
		if (result.errors) {
			this.form.markInvalid(result.errors);
			this.failureType = Tera.form.Action.SERVER_INVALID;
		}
		this.form.afterAction(this, false);
	},

	handleResponse : function(response) {
		if (this.form.errorReader) {
			var rs = this.form.errorReader.read(response);
			var errors = [];
			if (rs.records) {
				for ( var i = 0, len = rs.records.length; i < len; i++) {
					var r = rs.records[i];
					errors[i] = r.data;
				}
			}
			if (errors.length < 1) {
				errors = null;
			}
			return {
				success :rs.success,
				errors :errors
			};
		}
		return Tera.decode(response.responseText);
	}
});

Tera.form.Action.Load = function(form, options) {
	Tera.form.Action.Load.superclass.constructor.call(this, form, options);
	this.reader = this.form.reader;
};

Tera.extend(Tera.form.Action.Load, Tera.form.Action, {
	type :'load',

	run : function() {
		Tera.Ajax.request(Tera.apply(this.createCallback(this.options), {
			method :this.getMethod(),
			url :this.getUrl(false),
			headers :this.options.headers,
			params :this.getParams()
		}));
	},

	success : function(response) {
		var result = this.processResponse(response);
		if (result === true || !result.success || !result.data) {
			this.failureType = Tera.form.Action.LOAD_FAILURE;
			this.form.afterAction(this, false);
			return;
		}
		this.form.clearInvalid();
		this.form.setValues(result.data);
		this.form.afterAction(this, true);
	},

	handleResponse : function(response) {
		if (this.form.reader) {
			var rs = this.form.reader.read(response);
			var data = rs.records && rs.records[0] ? rs.records[0].data : null;
			return {
				success :rs.success,
				data :data
			};
		}
		return Tera.decode(response.responseText);
	}
});

Tera.form.Action.ACTION_TYPES = {
	'load' :Tera.form.Action.Load,
	'submit' :Tera.form.Action.Submit
};

Tera.form.VTypes = function() {
	var alpha = /^[a-zA-Z_]+$/;
	var alphanum = /^[a-zA-Z0-9_]+$/;
	var email = /^([\w]+)(.[\w]+)*@([\w-]+\.){1,5}([A-Za-z]){2,4}$/;
	var url = /(((https?)|(ftp)):\/\/([\-\w]+\.)+\w{2,3}(\/[%\-\w]+(\.\w{2,})?)*(([\w\-\.\?\\\/+@&#;`~=%!]*)(\.\w{2,})?)*\/?)/i;

	return {

		'email' : function(v) {
			return email.test(v);
		},

		'emailText' :'This field should be an e-mail address in the format "user@domain.com"',

		'emailMask' :/[a-z0-9_\.\-@]/i,

		'url' : function(v) {
			return url.test(v);
		},

		'urlText' :'This field should be a URL in the format "http:/' + '/www.domain.com"',

		'alpha' : function(v) {
			return alpha.test(v);
		},

		'alphaText' :'This field should only contain letters and _',

		'alphaMask' :/[a-z_]/i,

		'alphanum' : function(v) {
			return alphanum.test(v);
		},

		'alphanumText' :'This field should only contain letters, numbers and _',

		'alphanumMask' :/[a-z0-9_]/i
	};
}();

Tera.namespace('Tera', 'Tera.plugins');

Tera.ComponentFocusMgr = (function(){
	var focusChain = new Tera.util.MixedCollection(false, function(item){return item.id});
	var currentTarget = null;
	
	return {
		addToFocusList: function(eventTarget, cmp){
			if(currentTarget != eventTarget) {
				focusChain.clear();
				currentTarget = eventTarget;
			} 
			
			if(!focusChain.contains(cmp)){
				focusChain.add(cmp.id, cmp);
			}
		},
		
		getTopFocus: function(){		
			return focusChain.itemAt(0);
		},
		
		getFocusChain: function(){
			return focusChain;
		}
		
	}
})();


Tera.plugins.FocusDetector = function(config) {
    Tera.apply(this, config);
};
 
Tera.extend(Tera.plugins.FocusDetector, Tera.util.Observable, {
    init:function(client) {
        this.client = client;
        if(this.initFocus){       	
       		Tera.ComponentFocusMgr.addToFocusList('',this.client);
        }
        client.on({
	        'render': this.render,
			scope: this
		});
        client.on({
            'destroy': this.destroy,
            scope: this
        });
    },
    render: function() {    	
    	this.client.el.on('click', this.clickHandler, this);
    	this.client.on('contextmenu',this.clickHandler , this);
    },
    destroy: function(){
		this.client.el.un('click', this.clickHandler);
		this.client.un('contextmenu', this.clickHandler);
    },
    clickHandler:  function(ev){
    		Tera.ComponentFocusMgr.addToFocusList( ev.target, this.client);    		
    }
});
Tera.override(Tera.Element, {	
	_iCaretNative : undefined,
	_isSupported : undefined,
	/**
	 * @private
	 */
	isCaretNative: function(){
		if(typeof this._isCaretNative == 'undefined') {
			var d = document, o = d.createElement("input");
			this._isCaretNative = "selectionStart" in o; 
			this._isCaretSupported = this._isCaretNative || (o = d.selection) && !!o.createRange;	
			
		}
		return this._isCaretNative
	},
	
	/**
	 * @private
	 */
	isSupported: function(){
		if(typeof this._isCaretSupported == 'undefined') {
			this.isCaretNative();
		}
		return this._isCaretSupported
	},
	
	/**
	 * selects the text of the Element
	 * @param {Mixed} start Start position of the selection 
	 * @param {Number} end End position of the selection
	 * @return {}
	 */
	setCaret: function(start, end){
		
		if(!(this.dom.nodeName.toLowerCase() == "textarea" || this.dom.nodeName.toLowerCase() == "input")){
			return;
		}
		if(typeof start.start != 'undefined') {
				end = start.end;
				start = start.start;				
		}
		if(this.isCaretNative()) {
			return this.dom.setSelectionRange(start, end) 
		} else {
			var o = this.dom;
			var t = o.createTextRange();
	        end -= start + o.value.slice(start + 1, end).split("\n").length - 1;
	        start -= o.value.slice(0, start).split("\n").length - 1;
	        t.move("character", start), t.moveEnd("character", end), t.select();
		}		
	},
	
	/**
	 * returns the current Selection of the Element
	 * @return {Object} {start: startposition, end: endposition} of the selection
	 */
	getCaret: function(){
		
		var o = this.dom;
		if(this.isCaretNative()) {
			return { start: o.selectionStart, end: o.selectionEnd }
		} else {
			var s = (this.dom.focus(), document.selection.createRange()), r, start, end, value;
			// the current selection range is not part of the controll asking for the caret...
	        if(s.parentElement() != o)
	        	return {start: 0, end: 0};
	                
			var isTA = o.nodeName.toLowerCase() == "textarea";
	        if(isTA ? (r = s.duplicate()).moveToElementText(o) : r = o.createTextRange(), !isTA)
	        	return r.setEndPoint("EndToStart", s), {start: r.text.length, end: r.text.length + s.text.length};
            for(var $ = "[###]"; (value = o.value).indexOf($) + 1; $ += $);
            r.setEndPoint("StartToEnd", s), r.text = $ + r.text, end = o.value.indexOf($);
            s.text = $, start = o.value.indexOf($);
            if(document.execCommand && document.queryCommandSupported("Undo"))
                for(r in {0:0, 0:0})
                    document.execCommand("Undo");
            return o.value = value, this.setCaret(start, end), {start: start, end: end};
		}
	},
	/**
	 * Returns the caret Plus the Line, Column nummer and the position Number
	 * @return {}
	 */
	getCaretPosition : function(){
		
		if(!(this.dom.nodeName.toLowerCase() == "textarea" || this.dom.nodeName.toLowerCase() == "input")){
			return;
		}
        var c = this.getCaret(), o=this.dom;
        if(c){
              var lines;// = (typeof o.value != 'undefined') ? o.value.split("\n") : o.innerText.split("\n");
              if(typeof o.value != 'undefined') {
              	lines = o.value.split("\n");
              } else {
              	lines = o.innerText.split("\n");
              }
	              c.lines = lines.length;
              var p=c.start,ll=c.lines,len=0,line=0;
              for(;line<ll;line++) {
                len = lines[line].length + 1;
                if(p<len)break;
                p -= len;
         }
         line++;

         c.line=line;
         c.column = p;
         c.position = c.start;

        }
        return c;
    },
    /**
     * sets the CaretPosition 
     * @param {Number} start the beginnig of the selection
     * @param {Number} end the end of the selection
     */
    setCaretPosition : function(start, end) {
    	// FIX start.start ? 
    	if(typeof start.start != 'undefined') {
				end = start.end
				start = start.start;	
		} 
		if(typeof start != 'undefined' ) {
		  	this.setCaret(start,end || start);	
		}
    },
    /**
     * gets the current selected text
     * @return {}
     */
    getCaretText : function(){
        var o = this.getCaret();
        return (typeof this.dom.value != 'undefined') ? this.dom.value.slice(o.start, o.end):'';
    },
    setCaretText : function(text){
        var o = this.getCaret(), i = this.dom, s = i.value;
        if(typeof s == 'undefined') {
        	return;
        }
        i.value = s.slice(0, o.start) + text + s.slice(o.end);
        this.setCaret(o.start += text.length, o.start);
    }
	
});


Tera.ClipBoard = (function(){
	var lastInTeraCopiedString = false;
	var dataObject = null;
	
	return {
    	setData: function(dataObj, identifierString){
    		if(typeof identifieSring != 'undefined') {
    			lastInTeraCopiedString = identifieString;
    		}
    		dataObject = dataObj;
    	},
    	getData: function(stringFromClipboard){
    		
    		if(typeof identifieSring == 'undefined' || lastInTeraCopiedString == identifieSring) {
    			return dataObject;
    		}
    		return stringFromClipboard;
    	}
    }
})();

Tera.plugins.CopyPasteable = function(config) {
    Tera.apply(this, config);
};

Tera.extend(Tera.plugins.CopyPasteable, Tera.util.Observable, {
    init:function(client) {
        this.client = client;
        this.client.copyToClipboard = this.copyToClipboard;
        this.client.pasteFromClipboard = this.pasteFromClipboard;
        this.client.cutToClipboard = this.cutToClipboard;
        this.client.addEvents(
			'beforeCopy',
			'copy',
			'beforePaste',
			'paste'
        );
        client.on({
	        'render': this.render,
			scope: this
		});
        
        client.on({
            'destroy': this.destroy,
            scope: this
        });
    },
    copyToClipboard: function(identifierString) {
        Tera.ClipBoard.setData(identifierString, identifierString); 
    },
    cutToClipboard: function(identifierString){    	
        Tera.ClipBoard.setData(identifierString, identifierString); 
    },
    pasteFromClipboard: function(orginalString){
    	return orginalString;
    },
    buildCopyIdentifierString: function(orginalValue){
    	if(typeof orginalValue == 'undefined' || orginalValue =='') {
    		return Tera.id();
    	}
    	return orginalValue;
    },
    buildCutIdentifierString: function(orginalValue){
    	if(typeof orginalValue == 'undefined' || orginalValue =='') {
    		return Tera.id();
    	}
    	return orginalValue;
    },
    elementSuportsPaste: function(el){
    	return el.nodeName && (el.nodeName.toLowerCase() == 'input' || el.nodeName.toLowerCase() == 'textarea');
    },
    cutHandler: function (key, event){
    	var el = event.target; 
	    var caretState = Tera.fly(el).getCaret();    	
	    var orginalCuttedText = Tera.fly(el).getCaretText();
    	if(this.client.fireEvent("beforeCopy", this.client, event, key) !== false){
    		var value = (this.buildCutIdentifierString.createDelegate(this.client))(orginalCuttedText);
    		if(value !== false) {
	            this.bypassToClipboard(value);
				(function(el){
		        	el.focus();
		        	Tera.fly(el).setCaretPosition(caretState);
		        }).defer(10, this, [el]);
	        } else {
	        	value = Tera.fly(el).getCaretText();
	        }
	        (this.cutToClipboard.createDelegate(this.client))(value);
    	}
        return true;
    },
    copyHandler: function (key, event){
    	var el = event.target; 
    	var caretState = Tera.fly(el).getCaret();    	
	    var orginalCopyText = Tera.fly(el).getCaretText();
    	if(this.client.fireEvent("beforeCopy", this.client, event, key) !== false){
    		var value = (this.buildCopyIdentifierString.createDelegate(this.client))(orginalCopyText);
    		if(value !== false) {
	            this.bypassToClipboard(value);
				(function(el){
		        	el.focus();
	        		var elfly = Tera.fly(el); 
	        	 	elfly.setCaretPosition(caretState);
	        	 	elfly.setCaretText();
		        }).defer(10, this, [el]);
	        } else {
	        	value = Tera.fly(el).getCaretText();
	        }
	        (this.copyToClipboard.createDelegate(this.client))(value);
    	}
        return true;
    },
    bypassToClipboard: function(dataToByPass){
        this.bypassTextarea.value = dataToByPass;
        this.bypassTextarea.focus();
        this.bypassTextarea.select();
    },
    pasteHandler: function(key, event){
    	el = event.target;
    	var caretState = Tera.fly(el).getCaretPosition();
    	var bp = this.bypassIframe;
    	if (bp.contentWindow) {//ns6 syntax
    		this.getIframeBody().innerHTML = '';
			bp.contentWindow.focus();
		}
		else if (bp.Document) {//ie5+ syntax
			this.getIframeBody().innerHTML = '';
			bp.contentDocument.focus();
		}
        (function(el){
        	var pastVal = (this.pasteFromClipboard.createDelegate(this.client))(this.getIframeBody().innerHTML);
        	if(typeof pastVailue != 'undefined'){
	        	el.focus();
	        	var elfly = Tera.fly(el); 
	        	 elfly.setCaretPosition(caretState);
	        	 elfly.setCaretText(pastVal);
	        	 elfly.setCaretPosition(caretState.start+pastVal.length, caretState.start+pastVal.length);
        	}
        }).defer(10, this, [el]);
        
    },    
    
    render: function(){    	
        this.wrapper = Tera.DomHelper.append(this.client.el, {
               tag: 'div',
               children: [{
                    tag: 'textarea'
               }, {
               		tag: 'iframe',
               		name: Tera.id(),
               		frameBorder: '1',
               		src: Tera.isIE ? Tera.SSL_SECURE_URL : "javascript:;"
               }
               ],
               style: 'height:0; width:0; overflow:hidden; position: absolute;'
       });
       this.bypassIframe = this.wrapper.childNodes[1];
       this.initFrame();
       this.bypassTextarea = this.wrapper.firstChild;
       /*
       new Tera.KeyMap(this.client.el, [{
            key: 'c',
            ctrl: true,
            fn: this.copyHandler,
            scope: this
       },{
            key: 'v',
            ctrl: true,
            fn: this.pasteHandler,
            scope: this
       },{
            key: 'x',
            ctrl: true,
            fn: this.cutHandler,
            scope: this
       }]);  
       */ 
    },
    
    /**
     * returns the body element of the bypass Iframe
     * @return {Element}
     * @private
     */
    getIframeBody: function() {
    	this.doc = this.getDoc();
    	return this.doc.body || this.doc.documentElement;
    
    },
    
     // private
    getDoc : function(){
        return Tera.isIE ? this.getWin().document : (this.bypassIframe.contentDocument || this.getWin().document);
    },

    // private
    getWin : function(){
        return Tera.isIE ? this.bypassIframe.contentWindow : window.frames[this.bypassIframe.name];
    },
    
    /**
     * initializes the iframe (sets designMode=on and so..)
     */
    initFrame : function(){
        this.doc = this.getDoc();
        this.win = this.getWin();

        this.doc.open();
        this.doc.write('');
        this.doc.close();

        var task = { // must defer to wait for browser to be ready
            run : function(){
                if(this.doc.body || this.doc.readyState == 'complete'){
                    Tera.TaskMgr.stop(task);
                    this.doc.designMode="on";
                    //this.initEditor.defer(10, this);
                }
            },
            interval : 10,
            duration:10000,
            scope: this
        };
        Tera.TaskMgr.start(task);
    },
    
    destroy: function(){
    	try {
	        if(this.wrapper) {
	            this.wrapper.remove();
	        }
    	} catch (e) {
			// TODO: handle exception
		}
    
    }
}); // end of extend
 
// end of file

Tera.plugins.CopyPasteableGrid = function(config) {
    Tera.apply(this, config);
};
 
/**
 * Example Subclass of the CopyPasteable Plugin. Created for the Grid.
 * 
 * it overrides the buildCopyIdentifier to create a string that gets interpreted well by Excel
 * @class Tera.plugins.CopyPasteableGrid
 * @extends Tera.plugins.CopyPasteable
 */
Tera.extend(Tera.plugins.CopyPasteableGrid, Tera.plugins.CopyPasteable, {
	
	
	/**
	 * Overritten to create a string that gets well interpreted by Excel
	 * @param {string} orgVal the Value that would go its way into the Clipboard if you wouldnt bypass
	 * @return {string} the Value that should get bypassed into the Clipboard
	 */
  	buildCopyIdentifierString: function (orgVal) {
		
		// just format the output for excel
		var records = this.getSelectionModel().getSelections();
		
        returnstring = ""; 
        for(var i = 0; i < records.length; i++){
            var currRec = records[i];
            
            for(var prop in currRec.data) {
                returnstring = returnstring + currRec.data[prop] + '\t';                
            }
            
            
            returnstring = returnstring + '\n';
        }        
   		//alert(returnstring);
        return returnstring;
	},
	
	/**
	 * Overritten to create a string that gets well interpreted by Excel
	 * @param {string} orgVal the Value that would go its way into the Clipboard if you wouldnt bypass
	 * @return {string} the Value that should get bypassed into the Clipboard
	 */
	buildCutIdentifierString: function (orgVal) {
		
		// just format the output for excel
		var records = this.getSelectionModel().getSelections();
		
        returnstring = ""; 
        for(var i = 0; i < records.length; i++){
            var currRec = records[i];
            
            for(var prop in currRec.data) {
                returnstring = returnstring + currRec.data[prop] + '\t';                
            }
            
            
            returnstring = returnstring + '\n';
        }        
   		//alert(returnstring);
        return returnstring;
	},
	
		
	cutToClipboard: function(identifierString){},
	copyToClipboard: function(identifierString){
		var cl = this;
		var records = cl.getSelectionModel().getSelections();
		var clipBoardObject = [];
        for(var i = 0; i < records.length; i++){
            var currRec = records[i];
            clipBoardObject[clipBoardObject.length] = currRec.copy();
        }        
		Tera.ClipBoard.setData(clipBoardObject, identifierString);
		
	},
	pasteFromClipboard: function(identifierString){}
});



Tera.grid.GridPanel = Tera.extend(Tera.Panel, {

	ddText :"{0} selected row{1}",

	minColumnWidth :25,

	trackMouseOver :true,

	enableDragDrop :false,

	enableColumnMove :true,

	enableColumnHide :true,

	enableHdMenu :true,

	stripeRows :false,

	autoExpandColumn :false,

	autoExpandMin :50,

	autoExpandMax :1000,

	view :null,

	loadMask :false,

	rendered :false,

	plugins: [new Tera.plugins.CopyPasteableGrid(), new Tera.plugins.FocusDetector()],
	
	viewReady :false,

	stateEvents : [ "columnmove", "columnresize", "sortchange" ],

	initComponent : function() {
		Tera.grid.GridPanel.superclass.initComponent.call(this);

		this.autoScroll = false;
		this.autoWidth = false;

		if (Tera.isArray(this.columns)) {
			this.colModel = new Tera.grid.ColumnModel(this.columns);
			delete this.columns;
		}

		if (this.ds) {
			this.store = this.ds;
			delete this.ds;
		}
		if (this.cm) {
			this.colModel = this.cm;
			delete this.cm;
		}
		if (this.sm) {
			this.selModel = this.sm;
			delete this.sm;
		}
		this.store = Tera.StoreMgr.lookup(this.store);

		this.addEvents(

		"click",

		"dblclick",

		"contextmenu",

		"mousedown",

		"mouseup",

		"mouseover",

		"mouseout",

		"keypress",

		"keydown",

		"cellmousedown",

		"rowmousedown",

		"headermousedown",

		"cellclick",

		"celldblclick",

		"rowclick",

		"rowdblclick",

		"headerclick",

		"headerdblclick",

		"rowcontextmenu",

		"cellcontextmenu",

		"headercontextmenu",

		"bodyscroll",

		"columnresize",

		"columnmove",

		"sortchange");
	},

	onRender : function(ct, position) {
		Tera.grid.GridPanel.superclass.onRender.apply(this, arguments);

		var c = this.body;

		this.el.addClass('x-grid-panel');

		var view = this.getView();
		view.init(this);

		c.on("mousedown", this.onMouseDown, this);
		c.on("click", this.onClick, this);
		c.on("dblclick", this.onDblClick, this);
		c.on("contextmenu", this.onContextMenu, this);
		c.on("keydown", this.onKeyDown, this);

		this.relayEvents(c, [ "mousedown", "mouseup", "mouseover", "mouseout", "keypress" ]);

		this.getSelectionModel().init(this);
		
		try {
			var paging = this.getBottomToolbar();
			if (paging != undefined && paging.getXType()=='paging') {
				//alert('sm');
				paging.setGSM(this.getSelectionModel());
			}
		} catch(error) { }

		this.view.render();
	},

	initEvents : function() {
		Tera.grid.GridPanel.superclass.initEvents.call(this);

		if (this.loadMask) {
			this.loadMask = new Tera.LoadMask(this.bwrap, Tera.apply( {
				store :this.store
			}, this.loadMask));
		}
	},

	initStateEvents : function() {
		Tera.grid.GridPanel.superclass.initStateEvents.call(this);
		this.colModel.on('hiddenchange', this.saveState, this, {
			delay :100
		});
	},

	applyState : function(state) {
		var cm = this.colModel;
		var cs = state.columns;
		if (cs) {
			for ( var i = 0, len = cs.length; i < len; i++) {
				var s = cs[i];
				var c = cm.getColumnById(s.id);
				if (c) {
					c.hidden = s.hidden;
					c.width = s.width;
					var oldIndex = cm.getIndexById(s.id);
					if (oldIndex != i) {
						cm.moveColumn(oldIndex, i);
					}
				}
			}
		}
		if (state.sort) {
			this.store[this.store.remoteSort ? 'setDefaultSort' : 'sort'](state.sort.field, state.sort.direction);
		}
	},

	getState : function() {
		var o = {
			columns : []
		};
		for ( var i = 0, c; c = this.colModel.config[i]; i++) {
			o.columns[i] = {
				id :c.id,
				width :c.width
			};
			if (c.hidden) {
				o.columns[i].hidden = true;
			}
		}
		var ss = this.store.getSortState();
		if (ss) {
			o.sort = ss;
		}
		return o;
	},

	afterRender : function() {
		Tera.grid.GridPanel.superclass.afterRender.call(this);
		this.view.layout();
		this.viewReady = true;
	},

	reconfigure : function(store, colModel) {
		if (this.loadMask) {
			this.loadMask.destroy();
			this.loadMask = new Tera.LoadMask(this.bwrap, Tera.apply( {
				store :store
			}, this.initialConfig.loadMask));
		}
		this.view.bind(store, colModel);
		this.store = store;
		this.colModel = colModel;
		if (this.rendered) {
			this.view.refresh(true);
		}
	},

	onKeyDown : function(e) {
		this.fireEvent("keydown", e);
	},

	onDestroy : function() {
		if (this.rendered) {
			if (this.loadMask) {
				this.loadMask.destroy();
			}
			var c = this.body;
			c.removeAllListeners();
			this.view.destroy();
			c.update("");
		}
		this.colModel.purgeListeners();
		Tera.grid.GridPanel.superclass.onDestroy.call(this);
	},

	processEvent : function(name, e) {
		this.fireEvent(name, e);
		var t = e.getTarget();
		var v = this.view;
		var header = v.findHeaderIndex(t);
		if (header !== false) {
			this.fireEvent("header" + name, this, header, e);
		} else {
			var row = v.findRowIndex(t);
			var cell = v.findCellIndex(t);
			if (row !== false) {
				this.fireEvent("row" + name, this, row, e);
				if (cell !== false) {
					this.fireEvent("cell" + name, this, row, cell, e);
				}
			}
		}
	},

	onClick : function(e) {
		this.processEvent("click", e);
	},

	onMouseDown : function(e) {
		this.processEvent("mousedown", e);
	},

	onContextMenu : function(e, t) {
		this.processEvent("contextmenu", e);
	},

	onDblClick : function(e) {
		this.processEvent("dblclick", e);
	},

	walkCells : function(row, col, step, fn, scope) {
		var cm = this.colModel, clen = cm.getColumnCount();
		var ds = this.store, rlen = ds.getCount(), first = true;
		if (step < 0) {
			if (col < 0) {
				row--;
				first = false;
			}
			while (row >= 0) {
				if (!first) {
					col = clen - 1;
				}
				first = false;
				while (col >= 0) {
					if (fn.call(scope || this, row, col, cm) === true) {
						return [ row, col ];
					}
					col--;
				}
				row--;
			}
		} else {
			if (col >= clen) {
				row++;
				first = false;
			}
			while (row < rlen) {
				if (!first) {
					col = 0;
				}
				first = false;
				while (col < clen) {
					if (fn.call(scope || this, row, col, cm) === true) {
						return [ row, col ];
					}
					col++;
				}
				row++;
			}
		}
		return null;
	},

	getSelections : function() {
		return this.selModel.getSelections();
	},

	onResize : function() {
		Tera.grid.GridPanel.superclass.onResize.apply(this, arguments);
		if (this.viewReady) {
			this.view.layout();
		}
	},

	getGridEl : function() {
		return this.body;
	},

	stopEditing : function() {
	},

	getSelectionModel : function() {
		if (!this.selModel) {
			this.selModel = new Tera.grid.RowSelectionModel(this.disableSelection ? {
				selectRow :Tera.emptyFn
			} : null);
		}
		return this.selModel;
	},

	getStore : function() {
		return this.store;
	},

	getColumnModel : function() {
		return this.colModel;
	},

	getView : function() {
		if (!this.view) {
			this.view = new Tera.grid.GridView(this.viewConfig);
		}
		return this.view;
	},

	getDragDropText : function() {
		var count = this.selModel.getCount();
		return String.format(this.ddText, count, count == 1 ? '' : 's');
	}

});
Tera.reg('grid', Tera.grid.GridPanel);

Tera.grid.GridView = function(config) {
	Tera.apply(this, config);
	this.addEvents(

	"beforerowremoved",

	"beforerowsinserted",

	"beforerefresh",

	"rowremoved",

	"rowsinserted",

	"rowupdated",

	"refresh");
	Tera.grid.GridView.superclass.constructor.call(this);
};

Tera
		.extend(
				Tera.grid.GridView,
				Tera.util.Observable,
				{

					deferEmptyText :true,

					scrollOffset :19,

					autoFill :false,

					forceFit :false,

					sortClasses : [ "sort-asc", "sort-desc" ],

					sortAscText :"Sort Ascending",

					sortDescText :"Sort Descending",

					columnsText :"Columns",

					borderWidth :2,

					initTemplates : function() {
						var ts = this.templates || {};
						if (!ts.master) {
							ts.master = new Tera.Template(
									'<div class="x-grid3" hidefocus="true">',
									'<div class="x-grid3-viewport">',
									'<div class="x-grid3-header"><div class="x-grid3-header-inner"><div class="x-grid3-header-offset">{header}</div></div><div class="x-clear"></div></div>',
									'<div class="x-grid3-scroller"><div class="x-grid3-body">{body}</div><a href="#" class="x-grid3-focus" tabIndex="-1"></a></div>',
									"</div>", '<div class="x-grid3-resize-marker">&#160;</div>', '<div class="x-grid3-resize-proxy">&#160;</div>',
									"</div>");
						}

						if (!ts.header) {
							ts.header = new Tera.Template('<table border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
									'<thead><tr class="x-grid3-hd-row">{cells}</tr></thead>', "</table>");
						}

						if (!ts.hcell) {
							ts.hcell = new Tera.Template(
									'<td class="x-grid3-hd x-grid3-cell x-grid3-td-{id}" style="{style}"><div {tooltip} {attr} class="x-grid3-hd-inner x-grid3-hd-{id}" unselectable="on" style="{istyle}">',
									this.grid.enableHdMenu ? '<a class="x-grid3-hd-btn" href="#"></a>' : '',
									'{value}<img class="x-grid3-sort-icon" src="', Tera.BLANK_IMAGE_URL, '" />', "</div></td>");
						}

						if (!ts.body) {
							ts.body = new Tera.Template('{rows}');
						}

						if (!ts.row) {
							ts.row = new Tera.Template(
									'<div class="x-grid3-row {alt}" style="{tstyle}"><table class="x-grid3-row-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
									'<tbody><tr>{cells}</tr>',
									(this.enableRowBody ? '<tr class="x-grid3-row-body-tr" style="{bodyStyle}"><td colspan="{cols}" class="x-grid3-body-cell" tabIndex="0" hidefocus="on"><div class="x-grid3-row-body">{body}</div></td></tr>'
											: ''), '</tbody></table></div>');
						}

						if (!ts.cell) {
							ts.cell = new Tera.Template(
									'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}" tabIndex="0" {cellAttr}>',
									'<div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on" {attr}>{value}</div>', "</td>");
						}

						for ( var k in ts) {
							var t = ts[k];
							if (t && typeof t.compile == 'function' && !t.compiled) {
								t.disableFormats = true;
								t.compile();
							}
						}

						this.templates = ts;

						this.tdClass = 'x-grid3-cell';
						this.cellSelector = 'td.x-grid3-cell';
						this.hdCls = 'x-grid3-hd';
						this.rowSelector = 'div.x-grid3-row';
						this.colRe = new RegExp("x-grid3-td-([^\\s]+)", "");
					},

					fly : function(el) {
						if (!this._flyweight) {
							this._flyweight = new Tera.Element.Flyweight(document.body);
						}
						this._flyweight.dom = el;
						return this._flyweight;
					},

					getEditorParent : function(ed) {
						return this.scroller.dom;
					},

					initElements : function() {
						var E = Tera.Element;

						var el = this.grid.getGridEl().dom.firstChild;
						var cs = el.childNodes;

						this.el = new E(el);

						this.mainWrap = new E(cs[0]);
						this.mainHd = new E(this.mainWrap.dom.firstChild);

						if (this.grid.hideHeaders) {
							this.mainHd.setDisplayed(false);
						}

						this.innerHd = this.mainHd.dom.firstChild;
						this.scroller = new E(this.mainWrap.dom.childNodes[1]);
						if (this.forceFit) {
							this.scroller.setStyle('overflow-x', 'hidden');
						}
						this.mainBody = new E(this.scroller.dom.firstChild);

						this.focusEl = new E(this.scroller.dom.childNodes[1]);
						this.focusEl.swallowEvent("click", true);

						this.resizeMarker = new E(cs[1]);
						this.resizeProxy = new E(cs[2]);
					},

					getRows : function() {
						return this.hasRows() ? this.mainBody.dom.childNodes : [];
					},

					findCell : function(el) {
						if (!el) {
							return false;
						}
						return this.fly(el).findParent(this.cellSelector, 3);
					},

					findCellIndex : function(el, requiredCls) {
						var cell = this.findCell(el);
						if (cell && (!requiredCls || this.fly(cell).hasClass(requiredCls))) {
							return this.getCellIndex(cell);
						}
						return false;
					},

					getCellIndex : function(el) {
						if (el) {
							var m = el.className.match(this.colRe);
							if (m && m[1]) {
								return this.cm.getIndexById(m[1]);
							}
						}
						return false;
					},

					findHeaderCell : function(el) {
						var cell = this.findCell(el);
						return cell && this.fly(cell).hasClass(this.hdCls) ? cell : null;
					},

					findHeaderIndex : function(el) {
						return this.findCellIndex(el, this.hdCls);
					},

					findRow : function(el) {
						if (!el) {
							return false;
						}
						return this.fly(el).findParent(this.rowSelector, 10);
					},

					findRowIndex : function(el) {
						var r = this.findRow(el);
						return r ? r.rowIndex : false;
					},

					getRow : function(row) {
						return this.getRows()[row];
					},

					getCell : function(row, col) {
						return this.getRow(row).getElementsByTagName('td')[col];
					},

					getHeaderCell : function(index) {
						return this.mainHd.dom.getElementsByTagName('td')[index];
					},

					addRowClass : function(row, cls) {
						var r = this.getRow(row);
						if (r) {
							this.fly(r).addClass(cls);
						}
					},

					removeRowClass : function(row, cls) {
						var r = this.getRow(row);
						if (r) {
							this.fly(r).removeClass(cls);
						}
					},

					removeRow : function(row) {
						Tera.removeNode(this.getRow(row));
					},

					removeRows : function(firstRow, lastRow) {
						var bd = this.mainBody.dom;
						for ( var rowIndex = firstRow; rowIndex <= lastRow; rowIndex++) {
							Tera.removeNode(bd.childNodes[firstRow]);
						}
					},

					getScrollState : function() {
						var sb = this.scroller.dom;
						return {
							left :sb.scrollLeft,
							top :sb.scrollTop
						};
					},

					restoreScroll : function(state) {
						var sb = this.scroller.dom;
						sb.scrollLeft = state.left;
						sb.scrollTop = state.top;
					},

					scrollToTop : function() {
						this.scroller.dom.scrollTop = 0;
						this.scroller.dom.scrollLeft = 0;
					},

					syncScroll : function() {
						this.syncHeaderScroll();
						var mb = this.scroller.dom;
						this.grid.fireEvent("bodyscroll", mb.scrollLeft, mb.scrollTop);
					},

					syncHeaderScroll : function() {
						var mb = this.scroller.dom;
						this.innerHd.scrollLeft = mb.scrollLeft;
						this.innerHd.scrollLeft = mb.scrollLeft;
					},

					updateSortIcon : function(col, dir) {
						var sc = this.sortClasses;
						var hds = this.mainHd.select('td').removeClass(sc);
						hds.item(col).addClass(sc[dir == "DESC" ? 1 : 0]);
					},

					updateAllColumnWidths : function() {
						var tw = this.getTotalWidth();
						var clen = this.cm.getColumnCount();
						var ws = [];
						for ( var i = 0; i < clen; i++) {
							ws[i] = this.getColumnWidth(i);
						}

						this.innerHd.firstChild.firstChild.style.width = tw;

						for ( var i = 0; i < clen; i++) {
							var hd = this.getHeaderCell(i);
							hd.style.width = ws[i];
						}

						var ns = this.getRows();
						for ( var i = 0, len = ns.length; i < len; i++) {
							ns[i].style.width = tw;
							ns[i].firstChild.style.width = tw;
							var row = ns[i].firstChild.rows[0];
							for ( var j = 0; j < clen; j++) {
								row.childNodes[j].style.width = ws[j];
							}
						}

						this.onAllColumnWidthsUpdated(ws, tw);
					},

					updateColumnWidth : function(col, width) {
						var w = this.getColumnWidth(col);
						var tw = this.getTotalWidth();

						this.innerHd.firstChild.firstChild.style.width = tw;
						var hd = this.getHeaderCell(col);
						hd.style.width = w;

						var ns = this.getRows();
						for ( var i = 0, len = ns.length; i < len; i++) {
							ns[i].style.width = tw;
							ns[i].firstChild.style.width = tw;
							ns[i].firstChild.rows[0].childNodes[col].style.width = w;
						}

						this.onColumnWidthUpdated(col, w, tw);
					},

					updateColumnHidden : function(col, hidden) {
						var tw = this.getTotalWidth();

						this.innerHd.firstChild.firstChild.style.width = tw;

						var display = hidden ? 'none' : '';

						var hd = this.getHeaderCell(col);
						hd.style.display = display;

						var ns = this.getRows();
						for ( var i = 0, len = ns.length; i < len; i++) {
							ns[i].style.width = tw;
							ns[i].firstChild.style.width = tw;
							ns[i].firstChild.rows[0].childNodes[col].style.display = display;
						}

						this.onColumnHiddenUpdated(col, hidden, tw);

						delete this.lastViewWidth;
						this.layout();
					},

					doRender : function(cs, rs, ds, startRow, colCount, stripe) {
						var ts = this.templates, ct = ts.cell, rt = ts.row, last = colCount - 1;
						var tstyle = 'width:' + this.getTotalWidth() + ';';
						var buf = [], cb, c, p = {}, rp = {
							tstyle :tstyle
						}, r;
						for ( var j = 0, len = rs.length; j < len; j++) {
							r = rs[j];
							cb = [];
							var rowIndex = (j + startRow);
							for ( var i = 0; i < colCount; i++) {
								c = cs[i];
								p.id = c.id;
								p.css = i == 0 ? 'x-grid3-cell-first ' : (i == last ? 'x-grid3-cell-last ' : '');
								p.attr = p.cellAttr = "";
								p.value = c.renderer(r.data[c.name], p, r, rowIndex, i, ds);
								p.style = c.style;
								if (p.value == undefined || p.value === "")
									p.value = "&#160;";
								if (r.dirty && typeof r.modified[c.name] !== 'undefined') {
									p.css += ' x-grid3-dirty-cell';
								}
								cb[cb.length] = ct.apply(p);
							}
							var alt = [];
							if (stripe && ((rowIndex + 1) % 2 == 0)) {
								alt[0] = "x-grid3-row-alt";
							}
							if (r.dirty) {
								alt[1] = " x-grid3-dirty-row";
							}
							rp.cols = colCount;
							if (this.getRowClass) {
								alt[2] = this.getRowClass(r, rowIndex, rp, ds);
							}
							rp.alt = alt.join(" ");
							rp.cells = cb.join("");
							buf[buf.length] = rt.apply(rp);
						}
						return buf.join("");
					},

					processRows : function(startRow, skipStripe) {
						if (this.ds.getCount() < 1) {
							return;
						}
						skipStripe = skipStripe || !this.grid.stripeRows;
						startRow = startRow || 0;
						var rows = this.getRows();
						var cls = ' x-grid3-row-alt ';
						for ( var i = startRow, len = rows.length; i < len; i++) {
							var row = rows[i];
							row.rowIndex = i;
							if (!skipStripe) {
								var isAlt = ((i + 1) % 2 == 0);
								var hasAlt = (' ' + row.className + ' ').indexOf(cls) != -1;
								if (isAlt == hasAlt) {
									continue;
								}
								if (isAlt) {
									row.className += " x-grid3-row-alt";
								} else {
									row.className = row.className.replace("x-grid3-row-alt", "");
								}
							}
						}
					},

					renderUI : function() {

						var header = this.renderHeaders();
						var body = this.templates.body.apply( {
							rows :''
						});

						var html = this.templates.master.apply( {
							body :body,
							header :header
						});

						var g = this.grid;

						g.getGridEl().dom.innerHTML = html;

						this.initElements();

						this.mainBody.dom.innerHTML = this.renderRows();
						this.processRows(0, true);

						if (this.deferEmptyText !== true) {
							this.applyEmptyText();
						}

						Tera.fly(this.innerHd).on("click", this.handleHdDown, this);
						this.mainHd.on("mouseover", this.handleHdOver, this);
						this.mainHd.on("mouseout", this.handleHdOut, this);
						this.mainHd.on("mousemove", this.handleHdMove, this);

						this.scroller.on('scroll', this.syncScroll, this);
						if (g.enableColumnResize !== false) {
							this.splitone = new Tera.grid.GridView.SplitDragZone(g, this.mainHd.dom);
						}

						if (g.enableColumnMove) {
							this.columnDrag = new Tera.grid.GridView.ColumnDragZone(g, this.innerHd);
							this.columnDrop = new Tera.grid.HeaderDropZone(g, this.mainHd.dom);
						}

						if (g.enableHdMenu !== false) {
							if (g.enableColumnHide !== false) {
								this.colMenu = new Tera.menu.Menu( {
									id :g.id + "-hcols-menu"
								});
								this.colMenu.on("beforeshow", this.beforeColMenuShow, this);
								this.colMenu.on("itemclick", this.handleHdMenuClick, this);
							}
							this.hmenu = new Tera.menu.Menu( {
								id :g.id + "-hctx"
							});
							this.hmenu.add( {
								id :"asc",
								text :this.sortAscText,
								cls :"xg-hmenu-sort-asc"
							}, {
								id :"desc",
								text :this.sortDescText,
								cls :"xg-hmenu-sort-desc"
							});
							if (g.enableColumnHide !== false) {
								this.hmenu.add('-', {
									id :"columns",
									text :this.columnsText,
									menu :this.colMenu,
									iconCls :'x-cols-icon'
								});
							}
							this.hmenu.on("itemclick", this.handleHdMenuClick, this);

						}

						if (g.enableDragDrop || g.enableDrag) {
							this.dragZone = new Tera.grid.GridDragZone(g, {
								ddGroup :g.ddGroup || 'GridDD'
							});
						}

						this.updateHeaderSortState();

					},

					layout : function() {
						if (!this.mainBody) {
							return;
						}
						var g = this.grid;
						var c = g.getGridEl();
						var csize = c.getSize(true);
						var vw = csize.width;

						if (vw < 20 || csize.height < 20) {
							return;
						}

						if (g.autoHeight) {
							this.scroller.dom.style.overflow = 'visible';
						} else {
							this.el.setSize(csize.width, csize.height);

							var hdHeight = this.mainHd.getHeight();
							var vh = csize.height - (hdHeight);

							this.scroller.setSize(vw, vh);
							if (this.innerHd) {
								this.innerHd.style.width = (vw) + 'px';
							}
						}
						if (this.forceFit) {
							if (this.lastViewWidth != vw) {
								this.fitColumns(false, false);
								this.lastViewWidth = vw;
							}
						} else {
							this.autoExpand();
							this.syncHeaderScroll();
						}
						this.onLayout(vw, vh);
					},

					onLayout : function(vw, vh) {
					},

					onColumnWidthUpdated : function(col, w, tw) {
					},

					onAllColumnWidthsUpdated : function(ws, tw) {
					},

					onColumnHiddenUpdated : function(col, hidden, tw) {
					},

					updateColumnText : function(col, text) {
					},

					afterMove : function(colIndex) {
					},

					init : function(grid) {
						this.grid = grid;

						this.initTemplates();
						this.initData(grid.store, grid.colModel);
						this.initUI(grid);
					},

					getColumnId : function(index) {
						return this.cm.getColumnId(index);
					},

					renderHeaders : function() {
						var cm = this.cm, ts = this.templates;
						var ct = ts.hcell;

						var cb = [], sb = [], p = {};

						for ( var i = 0, len = cm.getColumnCount(); i < len; i++) {
							p.id = cm.getColumnId(i);
							p.value = cm.getColumnHeader(i) || "";
							p.style = this.getColumnStyle(i, true);
							p.tooltip = this.getColumnTooltip(i);
							if (cm.config[i].align == 'right') {
								p.istyle = 'padding-right:16px';
							} else {
								delete p.istyle;
							}
							cb[cb.length] = ct.apply(p);
						}
						return ts.header.apply( {
							cells :cb.join(""),
							tstyle :'width:' + this.getTotalWidth() + ';'
						});
					},

					getColumnTooltip : function(i) {
						var tt = this.cm.getColumnTooltip(i);
						if (tt) {
							if (Tera.QuickTips.isEnabled()) {
								return 'ext:qtip="' + tt + '"';
							} else {
								return 'title="' + tt + '"';
							}
						}
						return "";
					},

					beforeUpdate : function() {
						this.grid.stopEditing(true);
					},

					updateHeaders : function() {
						this.innerHd.firstChild.innerHTML = this.renderHeaders();
					},

					focusRow : function(row) {
						this.focusCell(row, 0, false);
					},

					focusCell : function(row, col, hscroll) {
						var xy = this.ensureVisible(row, col, hscroll);
						this.focusEl.setXY(xy);
						if (Tera.isGecko) {
							this.focusEl.focus();
						} else {
							this.focusEl.focus.defer(1, this.focusEl);
						}
					},

					ensureVisible : function(row, col, hscroll) {
						if (typeof row != "number") {
							row = row.rowIndex;
						}
						if (!this.ds) {
							return;
						}
						if (row < 0 || row >= this.ds.getCount()) {
							return;
						}
						col = (col !== undefined ? col : 0);

						var rowEl = this.getRow(row), cellEl;
						if (!(hscroll === false && col === 0)) {
							while (this.cm.isHidden(col)) {
								col++;
							}
							cellEl = this.getCell(row, col);
						}
						if (!rowEl) {
							return;
						}

						var c = this.scroller.dom;

						var ctop = 0;
						var p = rowEl, stop = this.el.dom;
						while (p && p != stop) {
							ctop += p.offsetTop;
							p = p.offsetParent;
						}
						ctop -= this.mainHd.dom.offsetHeight;

						var cbot = ctop + rowEl.offsetHeight;

						var ch = c.clientHeight;
						var stop = parseInt(c.scrollTop, 10);
						var sbot = stop + ch;

						if (ctop < stop) {
							c.scrollTop = ctop;
						} else if (cbot > sbot) {
							c.scrollTop = cbot - ch;
						}

						if (hscroll !== false) {
							var cleft = parseInt(cellEl.offsetLeft, 10);
							var cright = cleft + cellEl.offsetWidth;

							var sleft = parseInt(c.scrollLeft, 10);
							var sright = sleft + c.clientWidth;
							if (cleft < sleft) {
								c.scrollLeft = cleft;
							} else if (cright > sright) {
								c.scrollLeft = cright - c.clientWidth;
							}
						}
						return cellEl ? Tera.fly(cellEl).getXY() : [ c.scrollLeft, Tera.fly(rowEl).getY() ];
					},

					insertRows : function(dm, firstRow, lastRow, isUpdate) {
						if (!isUpdate && firstRow === 0 && lastRow == dm.getCount() - 1) {
							this.refresh();
						} else {
							if (!isUpdate) {
								this.fireEvent("beforerowsinserted", this, firstRow, lastRow);
							}
							var html = this.renderRows(firstRow, lastRow);
							var before = this.getRow(firstRow);
							if (before) {
								Tera.DomHelper.insertHtml('beforeBegin', before, html);
							} else {
								Tera.DomHelper.insertHtml('beforeEnd', this.mainBody.dom, html);
							}
							if (!isUpdate) {
								this.fireEvent("rowsinserted", this, firstRow, lastRow);
								this.processRows(firstRow);
							}
						}
					},

					deleteRows : function(dm, firstRow, lastRow) {
						if (dm.getRowCount() < 1) {
							this.refresh();
						} else {
							this.fireEvent("beforerowsdeleted", this, firstRow, lastRow);

							this.removeRows(firstRow, lastRow);

							this.processRows(firstRow);
							this.fireEvent("rowsdeleted", this, firstRow, lastRow);
						}
					},

					getColumnStyle : function(col, isHeader) {
						var style = !isHeader ? (this.cm.config[col].css || '') : '';
						style += 'width:' + this.getColumnWidth(col) + ';';
						if (this.cm.isHidden(col)) {
							style += 'display:none;';
						}
						var align = this.cm.config[col].align;
						if (align) {
							style += 'text-align:' + align + ';';
						}
						return style;
					},

					getColumnWidth : function(col) {
						var w = this.cm.getColumnWidth(col);
						if (typeof w == 'number') {
							return (Tera.isBorderBox ? w : (w - this.borderWidth > 0 ? w - this.borderWidth : 0)) + 'px';
						}
						return w;
					},

					getTotalWidth : function() {
						return this.cm.getTotalWidth() + 'px';
					},

					fitColumns : function(preventRefresh, onlyExpand, omitColumn) {
						var cm = this.cm, leftOver, dist, i;
						var tw = cm.getTotalWidth(false);
						var aw = this.grid.getGridEl().getWidth(true) - this.scrollOffset;

						if (aw < 20) {
							return;
						}
						var extra = aw - tw;

						if (extra === 0) {
							return false;
						}

						var vc = cm.getColumnCount(true);
						var ac = vc - (typeof omitColumn == 'number' ? 1 : 0);
						if (ac === 0) {
							ac = 1;
							omitColumn = undefined;
						}
						var colCount = cm.getColumnCount();
						var cols = [];
						var extraCol = 0;
						var width = 0;
						var w;
						for (i = 0; i < colCount; i++) {
							if (!cm.isHidden(i) && !cm.isFixed(i) && i !== omitColumn) {
								w = cm.getColumnWidth(i);
								cols.push(i);
								extraCol = i;
								cols.push(w);
								width += w;
							}
						}
						var frac = (aw - cm.getTotalWidth()) / width;
						while (cols.length) {
							w = cols.pop();
							i = cols.pop();
							cm.setColumnWidth(i, Math.max(this.grid.minColumnWidth, Math.floor(w + w * frac)), true);
						}

						if ((tw = cm.getTotalWidth(false)) > aw) {
							var adjustCol = ac != vc ? omitColumn : extraCol;
							cm.setColumnWidth(adjustCol, Math.max(1, cm.getColumnWidth(adjustCol) - (tw - aw)), true);
						}

						if (preventRefresh !== true) {
							this.updateAllColumnWidths();
						}

						return true;
					},

					autoExpand : function(preventUpdate) {
						var g = this.grid, cm = this.cm;
						if (!this.userResized && g.autoExpandColumn) {
							var tw = cm.getTotalWidth(false);
							var aw = this.grid.getGridEl().getWidth(true) - this.scrollOffset;
							if (tw != aw) {
								var ci = cm.getIndexById(g.autoExpandColumn);
								var currentWidth = cm.getColumnWidth(ci);
								var cw = Math.min(Math.max(((aw - tw) + currentWidth), g.autoExpandMin), g.autoExpandMax);
								if (cw != currentWidth) {
									cm.setColumnWidth(ci, cw, true);
									if (preventUpdate !== true) {
										this.updateColumnWidth(ci, cw);
									}
								}
							}
						}
					},

					getColumnData : function() {
						var cs = [], cm = this.cm, colCount = cm.getColumnCount();
						for ( var i = 0; i < colCount; i++) {
							var name = cm.getDataIndex(i);
							cs[i] = {
								name :(typeof name == 'undefined' ? this.ds.fields.get(i).name : name),
								renderer :cm.getRenderer(i),
								id :cm.getColumnId(i),
								style :this.getColumnStyle(i)
							};
						}
						return cs;
					},

					renderRows : function(startRow, endRow) {
						var g = this.grid, cm = g.colModel, ds = g.store, stripe = g.stripeRows;
						var colCount = cm.getColumnCount();

						if (ds.getCount() < 1) {
							return "";
						}

						var cs = this.getColumnData();

						startRow = startRow || 0;
						endRow = typeof endRow == "undefined" ? ds.getCount() - 1 : endRow;

						var rs = ds.getRange(startRow, endRow);

						return this.doRender(cs, rs, ds, startRow, colCount, stripe);
					},

					renderBody : function() {
						var markup = this.renderRows();
						return this.templates.body.apply( {
							rows :markup
						});
					},

					refreshRow : function(record) {
						var ds = this.ds, index;
						if (typeof record == 'number') {
							index = record;
							record = ds.getAt(index);
						} else {
							index = ds.indexOf(record);
						}
						var cls = [];
						this.insertRows(ds, index, index, true);
						this.getRow(index).rowIndex = index;
						this.onRemove(ds, record, index + 1, true);
						this.fireEvent("rowupdated", this, index, record);
					},

					refresh : function(headersToo) {
						this.fireEvent("beforerefresh", this);
						this.grid.stopEditing(true);

						var result = this.renderBody();
						this.mainBody.update(result);

						if (headersToo === true) {
							this.updateHeaders();
							this.updateHeaderSortState();
						}
						this.processRows(0, true);
						this.layout();
						this.applyEmptyText();
						this.fireEvent("refresh", this);
					},

					applyEmptyText : function() {
						if (this.emptyText && !this.hasRows()) {
							this.mainBody.update('<div class="x-grid-empty">' + this.emptyText + '</div>');
						}
					},

					updateHeaderSortState : function() {
						var state = this.ds.getSortState();
						if (!state) {
							return;
						}
						if (!this.sortState || (this.sortState.field != state.field || this.sortState.direction != state.direction)) {
							this.grid.fireEvent('sortchange', this.grid, state);
						}
						this.sortState = state;
						var sortColumn = this.cm.findColumnIndex(state.field);
						if (sortColumn != -1) {
							var sortDir = state.direction;
							this.updateSortIcon(sortColumn, sortDir);
						}
					},

					destroy : function() {
						if (this.colMenu) {
							this.colMenu.removeAll();
							Tera.menu.MenuMgr.unregister(this.colMenu);
							this.colMenu.getEl().remove();
							delete this.colMenu;
						}
						if (this.hmenu) {
							this.hmenu.removeAll();
							Tera.menu.MenuMgr.unregister(this.hmenu);
							this.hmenu.getEl().remove();
							delete this.hmenu;
						}
						if (this.grid.enableColumnMove) {
							var dds = Tera.dd.DDM.ids['gridHeader' + this.grid.getGridEl().id];
							if (dds) {
								for ( var dd in dds) {
									if (!dds[dd].config.isTarget && dds[dd].dragElId) {
										var elid = dds[dd].dragElId;
										dds[dd].unreg();
										Tera.get(elid).remove();
									} else if (dds[dd].config.isTarget) {
										dds[dd].proxyTop.remove();
										dds[dd].proxyBottom.remove();
										dds[dd].unreg();
									}
									if (Tera.dd.DDM.locationCache[dd]) {
										delete Tera.dd.DDM.locationCache[dd];
									}
								}
								delete Tera.dd.DDM.ids['gridHeader' + this.grid.getGridEl().id];
							}
						}

						Tera.destroy(this.resizeMarker, this.resizeProxy);

						if (this.dragZone) {
							this.dragZone.unreg();
						}

						this.initData(null, null);
						Tera.EventManager.removeResizeListener(this.onWindowResize, this);
					},

					onDenyColumnHide : function() {

					},

					render : function() {

						var cm = this.cm;
						var colCount = cm.getColumnCount();

						if (this.autoFill) {
							this.fitColumns(true, true);
						} else if (this.forceFit) {
							this.fitColumns(true, false);
						} else if (this.grid.autoExpandColumn) {
							this.autoExpand(true);
						}

						this.renderUI();
					},

					initData : function(ds, cm) {
						if (this.ds) {
							this.ds.un("load", this.onLoad, this);
							this.ds.un("datachanged", this.onDataChange, this);
							this.ds.un("add", this.onAdd, this);
							this.ds.un("remove", this.onRemove, this);
							this.ds.un("update", this.onUpdate, this);
							this.ds.un("clear", this.onClear, this);
						}
						if (ds) {
							ds.on("load", this.onLoad, this);
							ds.on("datachanged", this.onDataChange, this);
							ds.on("add", this.onAdd, this);
							ds.on("remove", this.onRemove, this);
							ds.on("update", this.onUpdate, this);
							ds.on("clear", this.onClear, this);
						}
						this.ds = ds;

						if (this.cm) {
							this.cm.un("configchange", this.onColConfigChange, this);
							this.cm.un("widthchange", this.onColWidthChange, this);
							this.cm.un("headerchange", this.onHeaderChange, this);
							this.cm.un("hiddenchange", this.onHiddenChange, this);
							this.cm.un("columnmoved", this.onColumnMove, this);
							this.cm.un("columnlockchange", this.onColumnLock, this);
						}
						if (cm) {
							cm.on("configchange", this.onColConfigChange, this);
							cm.on("widthchange", this.onColWidthChange, this);
							cm.on("headerchange", this.onHeaderChange, this);
							cm.on("hiddenchange", this.onHiddenChange, this);
							cm.on("columnmoved", this.onColumnMove, this);
							cm.on("columnlockchange", this.onColumnLock, this);
						}
						this.cm = cm;
					},

					onDataChange : function() {
						this.refresh();
						this.updateHeaderSortState();
					},

					onClear : function() {
						this.refresh();
					},

					onUpdate : function(ds, record) {
						this.refreshRow(record);
					},

					onAdd : function(ds, records, index) {
						this.insertRows(ds, index, index + (records.length - 1));
					},

					onRemove : function(ds, record, index, isUpdate) {
						if (isUpdate !== true) {
							this.fireEvent("beforerowremoved", this, index, record);
						}
						this.removeRow(index);
						if (isUpdate !== true) {
							this.processRows(index);
							this.applyEmptyText();
							this.fireEvent("rowremoved", this, index, record);
						}
					},

					onLoad : function() {
						this.scrollToTop();
					},

					onColWidthChange : function(cm, col, width) {
						this.updateColumnWidth(col, width);
					},

					onHeaderChange : function(cm, col, text) {
						this.updateHeaders();
					},

					onHiddenChange : function(cm, col, hidden) {
						this.updateColumnHidden(col, hidden);
					},

					onColumnMove : function(cm, oldIndex, newIndex) {
						this.indexMap = null;
						var s = this.getScrollState();
						this.refresh(true);
						this.restoreScroll(s);
						this.afterMove(newIndex);
					},

					onColConfigChange : function() {
						delete this.lastViewWidth;
						this.indexMap = null;
						this.refresh(true);
					},

					initUI : function(grid) {
						grid.on("headerclick", this.onHeaderClick, this);

						if (grid.trackMouseOver) {
							grid.on("mouseover", this.onRowOver, this);
							grid.on("mouseout", this.onRowOut, this);
						}
					},

					initEvents : function() {

					},

					onHeaderClick : function(g, index) {
						if (this.headersDisabled || !this.cm.isSortable(index)) {
							return;
						}
						g.stopEditing(true);
						g.store.sort(this.cm.getDataIndex(index));
					},

					onRowOver : function(e, t) {
						var row;
						if ((row = this.findRowIndex(t)) !== false) {
							this.addRowClass(row, "x-grid3-row-over");
						}
					},

					onRowOut : function(e, t) {
						var row;
						if ((row = this.findRowIndex(t)) !== false && row !== this.findRowIndex(e.getRelatedTarget())) {
							this.removeRowClass(row, "x-grid3-row-over");
						}
					},

					handleWheel : function(e) {
						e.stopPropagation();
					},

					onRowSelect : function(row) {
						this.addRowClass(row, "x-grid3-row-selected");
					},

					onRowDeselect : function(row) {
						this.removeRowClass(row, "x-grid3-row-selected");
					},

					onCellSelect : function(row, col) {
						var cell = this.getCell(row, col);
						if (cell) {
							this.fly(cell).addClass("x-grid3-cell-selected");
						}
					},

					onCellDeselect : function(row, col) {
						var cell = this.getCell(row, col);
						if (cell) {
							this.fly(cell).removeClass("x-grid3-cell-selected");
						}
					},

					onColumnSplitterMoved : function(i, w) {
						this.userResized = true;
						var cm = this.grid.colModel;
						cm.setColumnWidth(i, w, true);

						if (this.forceFit) {
							this.fitColumns(true, false, i);
							this.updateAllColumnWidths();
						} else {
							this.updateColumnWidth(i, w);
						}

						this.grid.fireEvent("columnresize", i, w);
					},

					handleHdMenuClick : function(item) {
						var index = this.hdCtxIndex;
						var cm = this.cm, ds = this.ds;
						switch (item.id) {
							case "asc":
								ds.sort(cm.getDataIndex(index), "ASC");
								break;
							case "desc":
								ds.sort(cm.getDataIndex(index), "DESC");
								break;
							default:
								index = cm.getIndexById(item.id.substr(4));
								if (index != -1) {
									if (item.checked && cm.getColumnsBy(this.isHideableColumn, this).length <= 1) {
										this.onDenyColumnHide();
										return false;
									}
									cm.setHidden(index, item.checked);
								}
						}
						return true;
					},

					isHideableColumn : function(c) {
						return !c.hidden && !c.fixed;
					},

					beforeColMenuShow : function() {
						var cm = this.cm, colCount = cm.getColumnCount();
						this.colMenu.removeAll();
						for ( var i = 0; i < colCount; i++) {
							if (cm.config[i].fixed !== true && cm.config[i].hideable !== false) {
								this.colMenu.add(new Tera.menu.CheckItem( {
									id :"col-" + cm.getColumnId(i),
									text :cm.getColumnHeader(i),
									checked :!cm.isHidden(i),
									hideOnClick :false,
									disabled :cm.config[i].hideable === false
								}));
							}
						}
					},

					handleHdDown : function(e, t) {
						if (Tera.fly(t).hasClass('x-grid3-hd-btn')) {
							e.stopEvent();
							var hd = this.findHeaderCell(t);
							Tera.fly(hd).addClass('x-grid3-hd-menu-open');
							var index = this.getCellIndex(hd);
							this.hdCtxIndex = index;
							var ms = this.hmenu.items, cm = this.cm;
							ms.get("asc").setDisabled(!cm.isSortable(index));
							ms.get("desc").setDisabled(!cm.isSortable(index));
							this.hmenu.on("hide", function() {
								Tera.fly(hd).removeClass('x-grid3-hd-menu-open');
							}, this, {
								single :true
							});
							this.hmenu.show(t, "tl-bl?");
						}
					},

					handleHdOver : function(e, t) {
						var hd = this.findHeaderCell(t);
						if (hd && !this.headersDisabled) {
							this.activeHd = hd;
							this.activeHdIndex = this.getCellIndex(hd);
							var fly = this.fly(hd);
							this.activeHdRegion = fly.getRegion();
							if (!this.cm.isMenuDisabled(this.activeHdIndex)) {
								fly.addClass("x-grid3-hd-over");
								this.activeHdBtn = fly.child('.x-grid3-hd-btn');
								if (this.activeHdBtn) {
									this.activeHdBtn.dom.style.height = (hd.firstChild.offsetHeight - 1) + 'px';
								}
							}
						}
					},

					handleHdMove : function(e, t) {
						if (this.activeHd && !this.headersDisabled) {
							var hw = this.splitHandleWidth || 5;
							var r = this.activeHdRegion;
							var x = e.getPageX();
							var ss = this.activeHd.style;
							if (x - r.left <= hw && this.cm.isResizable(this.activeHdIndex - 1)) {
								ss.cursor = Tera.isAir ? 'move' : Tera.isSafari ? 'e-resize' : 'col-resize';
							} else if (r.right - x <= (!this.activeHdBtn ? hw : 2) && this.cm.isResizable(this.activeHdIndex)) {
								ss.cursor = Tera.isAir ? 'move' : Tera.isSafari ? 'w-resize' : 'col-resize';
							} else {
								ss.cursor = '';
							}
						}
					},

					handleHdOut : function(e, t) {
						var hd = this.findHeaderCell(t);
						if (hd && (!Tera.isIE || !e.within(hd, true))) {
							this.activeHd = null;
							this.fly(hd).removeClass("x-grid3-hd-over");
							hd.style.cursor = '';
						}
					},

					hasRows : function() {
						var fc = this.mainBody.dom.firstChild;
						return fc && fc.className != 'x-grid-empty';
					},

					bind : function(d, c) {
						this.initData(d, c);
					}
				});

Tera.grid.GridView.SplitDragZone = function(grid, hd) {
	this.grid = grid;
	this.view = grid.getView();
	this.marker = this.view.resizeMarker;
	this.proxy = this.view.resizeProxy;
	Tera.grid.GridView.SplitDragZone.superclass.constructor.call(this, hd, "gridSplitters" + this.grid.getGridEl().id, {
		dragElId :Tera.id(this.proxy.dom),
		resizeFrame :false
	});
	this.scroll = false;
	this.hw = this.view.splitHandleWidth || 5;
};
Tera.extend(Tera.grid.GridView.SplitDragZone, Tera.dd.DDProxy, {

	b4StartDrag : function(x, y) {
		this.view.headersDisabled = true;
		var h = this.view.mainWrap.getHeight();
		this.marker.setHeight(h);
		this.marker.show();
		this.marker.alignTo(this.view.getHeaderCell(this.cellIndex), 'tl-tl', [ -2, 0 ]);
		this.proxy.setHeight(h);
		var w = this.cm.getColumnWidth(this.cellIndex);
		var minw = Math.max(w - this.grid.minColumnWidth, 0);
		this.resetConstraints();
		this.setXConstraint(minw, 1000);
		this.setYConstraint(0, 0);
		this.minX = x - minw;
		this.maxX = x + 1000;
		this.startPos = x;
		Tera.dd.DDProxy.prototype.b4StartDrag.call(this, x, y);
	},

	handleMouseDown : function(e) {
		var t = this.view.findHeaderCell(e.getTarget());
		if (t) {
			var xy = this.view.fly(t).getXY(), x = xy[0], y = xy[1];
			var exy = e.getXY(), ex = exy[0], ey = exy[1];
			var w = t.offsetWidth, adjust = false;
			if ((ex - x) <= this.hw) {
				adjust = -1;
			} else if ((x + w) - ex <= this.hw) {
				adjust = 0;
			}
			if (adjust !== false) {
				this.cm = this.grid.colModel;
				var ci = this.view.getCellIndex(t);
				if (adjust == -1) {
					if (ci + adjust < 0) {
						return;
					}
					while (this.cm.isHidden(ci + adjust)) {
						--adjust;
						if (ci + adjust < 0) {
							return;
						}
					}
				}
				this.cellIndex = ci + adjust;
				this.split = t.dom;
				if (this.cm.isResizable(this.cellIndex) && !this.cm.isFixed(this.cellIndex)) {
					Tera.grid.GridView.SplitDragZone.superclass.handleMouseDown.apply(this, arguments);
				}
			} else if (this.view.columnDrag) {
				this.view.columnDrag.callHandleMouseDown(e);
			}
		}
	},

	endDrag : function(e) {
		this.marker.hide();
		var v = this.view;
		var endX = Math.max(this.minX, e.getPageX());
		var diff = endX - this.startPos;
		v.onColumnSplitterMoved(this.cellIndex, this.cm.getColumnWidth(this.cellIndex) + diff);
		setTimeout( function() {
			v.headersDisabled = false;
		}, 50);
	},

	autoOffset : function() {
		this.setDelta(0, 0);
	}
});

Tera.grid.GroupingView = Tera.extend(Tera.grid.GridView, {

	hideGroupedColumn :false,

	showGroupName :true,

	startCollapsed :false,

	enableGrouping :true,

	enableGroupingMenu :true,

	enableNoGroups :true,

	emptyGroupText :'(None)',

	ignoreAdd :false,

	groupTextTpl :'{text}',

	gidSeed :1000,

	initTemplates : function() {
		Tera.grid.GroupingView.superclass.initTemplates.call(this);
		this.state = {};

		var sm = this.grid.getSelectionModel();
		sm.on(sm.selectRow ? 'beforerowselect' : 'beforecellselect', this.onBeforeRowSelect, this);

		if (!this.startGroup) {
			this.startGroup = new Tera.XTemplate('<div id="{groupId}" class="x-grid-group {cls}">',
					'<div id="{groupId}-hd" class="x-grid-group-hd" style="{style}"><div>', this.groupTextTpl, '</div></div>',
					'<div id="{groupId}-bd" class="x-grid-group-body">');
		}
		this.startGroup.compile();
		this.endGroup = '</div></div>';
	},

	findGroup : function(el) {
		return Tera.fly(el).up('.x-grid-group', this.mainBody.dom);
	},

	getGroups : function() {
		return this.hasRows() ? this.mainBody.dom.childNodes : [];
	},

	onAdd : function() {
		if (this.enableGrouping && !this.ignoreAdd) {
			var ss = this.getScrollState();
			this.refresh();
			this.restoreScroll(ss);
		} else if (!this.enableGrouping) {
			Tera.grid.GroupingView.superclass.onAdd.apply(this, arguments);
		}
	},

	onRemove : function(ds, record, index, isUpdate) {
		Tera.grid.GroupingView.superclass.onRemove.apply(this, arguments);
		var g = document.getElementById(record._groupId);
		if (g && g.childNodes[1].childNodes.length < 1) {
			Tera.removeNode(g);
		}
		this.applyEmptyText();
	},

	refreshRow : function(record) {
		if (this.ds.getCount() == 1) {
			this.refresh();
		} else {
			this.isUpdating = true;
			Tera.grid.GroupingView.superclass.refreshRow.apply(this, arguments);
			this.isUpdating = false;
		}
	},

	beforeMenuShow : function() {
		var field = this.getGroupField();
		var g = this.hmenu.items.get('groupBy');
		if (g) {
			g.setDisabled(this.cm.config[this.hdCtxIndex].groupable === false);
		}
		var s = this.hmenu.items.get('showGroups');
		if (s) {
			s.setDisabled(!field && this.cm.config[this.hdCtxIndex].groupable === false);
			s.setChecked(!!field, true);
		}
	},

	renderUI : function() {
		Tera.grid.GroupingView.superclass.renderUI.call(this);
		this.mainBody.on('mousedown', this.interceptMouse, this);

		if (this.enableGroupingMenu && this.hmenu) {
			this.hmenu.add('-', {
				id :'groupBy',
				text :this.groupByText,
				handler :this.onGroupByClick,
				scope :this,
				iconCls :'x-group-by-icon'
			});
			if (this.enableNoGroups) {
				this.hmenu.add( {
					id :'showGroups',
					text :this.showGroupsText,
					checked :true,
					checkHandler :this.onShowGroupsClick,
					scope :this
				});
			}
			this.hmenu.on('beforeshow', this.beforeMenuShow, this);
		}
	},

	onGroupByClick : function() {
		this.grid.store.groupBy(this.cm.getDataIndex(this.hdCtxIndex));
		this.beforeMenuShow();
	},

	onShowGroupsClick : function(mi, checked) {
		if (checked) {
			this.onGroupByClick();
		} else {
			this.grid.store.clearGrouping();
		}
	},

	toggleGroup : function(group, expanded) {
		this.grid.stopEditing(true);
		group = Tera.getDom(group);
		var gel = Tera.fly(group);
		expanded = expanded !== undefined ? expanded : gel.hasClass('x-grid-group-collapsed');

		this.state[gel.dom.id] = expanded;
		gel[expanded ? 'removeClass' : 'addClass']('x-grid-group-collapsed');
	},

	toggleAllGroups : function(expanded) {
		var groups = this.getGroups();
		for ( var i = 0, len = groups.length; i < len; i++) {
			this.toggleGroup(groups[i], expanded);
		}
	},

	expandAllGroups : function() {
		this.toggleAllGroups(true);
	},

	collapseAllGroups : function() {
		this.toggleAllGroups(false);
	},

	interceptMouse : function(e) {
		var hd = e.getTarget('.x-grid-group-hd', this.mainBody);
		if (hd) {
			e.stopEvent();
			this.toggleGroup(hd.parentNode);
		}
	},

	getGroup : function(v, r, groupRenderer, rowIndex, colIndex, ds) {
		var g = groupRenderer ? groupRenderer(v, {}, r, rowIndex, colIndex, ds) : String(v);
		if (g === '') {
			g = this.cm.config[colIndex].emptyGroupText || this.emptyGroupText;
		}
		return g;
	},

	getGroupField : function() {
		return this.grid.store.getGroupState();
	},

	renderRows : function() {
		var groupField = this.getGroupField();
		var eg = !!groupField;

		if (this.hideGroupedColumn) {
			var colIndex = this.cm.findColumnIndex(groupField);
			if (!eg && this.lastGroupField !== undefined) {
				this.mainBody.update('');
				this.cm.setHidden(this.cm.findColumnIndex(this.lastGroupField), false);
				delete this.lastGroupField;
			} else if (eg && this.lastGroupField === undefined) {
				this.lastGroupField = groupField;
				this.cm.setHidden(colIndex, true);
			} else if (eg && this.lastGroupField !== undefined && groupField !== this.lastGroupField) {
				this.mainBody.update('');
				var oldIndex = this.cm.findColumnIndex(this.lastGroupField);
				this.cm.setHidden(oldIndex, false);
				this.lastGroupField = groupField;
				this.cm.setHidden(colIndex, true);
			}
		}
		return Tera.grid.GroupingView.superclass.renderRows.apply(this, arguments);
	},

	doRender : function(cs, rs, ds, startRow, colCount, stripe) {
		if (rs.length < 1) {
			return '';
		}
		var groupField = this.getGroupField();
		var colIndex = this.cm.findColumnIndex(groupField);

		this.enableGrouping = !!groupField;

		if (!this.enableGrouping || this.isUpdating) {
			return Tera.grid.GroupingView.superclass.doRender.apply(this, arguments);
		}
		var gstyle = 'width:' + this.getTotalWidth() + ';';

		var gidPrefix = this.grid.getGridEl().id;
		var cfg = this.cm.config[colIndex];
		var groupRenderer = cfg.groupRenderer || cfg.renderer;
		var prefix = this.showGroupName ? (cfg.groupName || cfg.header) + ': ' : '';

		var groups = [], curGroup, i, len, gid;
		for (i = 0, len = rs.length; i < len; i++) {
			var rowIndex = startRow + i;
			var r = rs[i], gvalue = r.data[groupField], g = this.getGroup(gvalue, r, groupRenderer, rowIndex, colIndex, ds);
			if (!curGroup || curGroup.group != g) {
				gid = gidPrefix + '-gp-' + groupField + '-' + Tera.util.Format.htmlEncode(g);

				var isCollapsed = typeof this.state[gid] !== 'undefined' ? !this.state[gid] : this.startCollapsed;
				var gcls = isCollapsed ? 'x-grid-group-collapsed' : '';
				curGroup = {
					group :g,
					gvalue :gvalue,
					text :prefix + g,
					groupId :gid,
					startRow :rowIndex,
					rs : [ r ],
					cls :gcls,
					style :gstyle
				};
				groups.push(curGroup);
			} else {
				curGroup.rs.push(r);
			}
			r._groupId = gid;
		}

		var buf = [];
		for (i = 0, len = groups.length; i < len; i++) {
			var g = groups[i];
			this.doGroupStart(buf, g, cs, ds, colCount);
			buf[buf.length] = Tera.grid.GroupingView.superclass.doRender.call(this, cs, g.rs, ds, g.startRow, colCount, stripe);

			this.doGroupEnd(buf, g, cs, ds, colCount);
		}
		return buf.join('');
	},

	getGroupId : function(value) {
		var gidPrefix = this.grid.getGridEl().id;
		var groupField = this.getGroupField();
		var colIndex = this.cm.findColumnIndex(groupField);
		var cfg = this.cm.config[colIndex];
		var groupRenderer = cfg.groupRenderer || cfg.renderer;
		var gtext = this.getGroup(value, {
			data : {}
		}, groupRenderer, 0, colIndex, this.ds);
		return gidPrefix + '-gp-' + groupField + '-' + Tera.util.Format.htmlEncode(value);
	},

	doGroupStart : function(buf, g, cs, ds, colCount) {
		buf[buf.length] = this.startGroup.apply(g);
	},

	doGroupEnd : function(buf, g, cs, ds, colCount) {
		buf[buf.length] = this.endGroup;
	},

	getRows : function() {
		if (!this.enableGrouping) {
			return Tera.grid.GroupingView.superclass.getRows.call(this);
		}
		var r = [];
		var g, gs = this.getGroups();
		for ( var i = 0, len = gs.length; i < len; i++) {
			g = gs[i].childNodes[1].childNodes;
			for ( var j = 0, jlen = g.length; j < jlen; j++) {
				r[r.length] = g[j];
			}
		}
		return r;
	},

	updateGroupWidths : function() {
		if (!this.enableGrouping || !this.hasRows()) {
			return;
		}
		var tw = Math.max(this.cm.getTotalWidth(), this.el.dom.offsetWidth - this.scrollOffset) + 'px';
		var gs = this.getGroups();
		for ( var i = 0, len = gs.length; i < len; i++) {
			gs[i].firstChild.style.width = tw;
		}
	},

	onColumnWidthUpdated : function(col, w, tw) {
		this.updateGroupWidths();
	},

	onAllColumnWidthsUpdated : function(ws, tw) {
		this.updateGroupWidths();
	},

	onColumnHiddenUpdated : function(col, hidden, tw) {
		this.updateGroupWidths();
	},

	onLayout : function() {
		this.updateGroupWidths();
	},

	onBeforeRowSelect : function(sm, rowIndex) {
		if (!this.enableGrouping) {
			return;
		}
		var row = this.getRow(rowIndex);
		if (row && !row.offsetParent) {
			var g = this.findGroup(row);
			this.toggleGroup(g, true);
		}
	},

	groupByText :'Group By This Field',

	showGroupsText :'Show in Groups'
});

Tera.grid.GroupingView.GROUP_ID = 1000;

Tera.grid.HeaderDragZone = function(grid, hd, hd2) {
	this.grid = grid;
	this.view = grid.getView();
	this.ddGroup = "gridHeader" + this.grid.getGridEl().id;
	Tera.grid.HeaderDragZone.superclass.constructor.call(this, hd);
	if (hd2) {
		this.setHandleElId(Tera.id(hd));
		this.setOuterHandleElId(Tera.id(hd2));
	}
	this.scroll = false;
};
Tera.extend(Tera.grid.HeaderDragZone, Tera.dd.DragZone, {
	maxDragWidth :120,
	getDragData : function(e) {
		var t = Tera.lib.Event.getTarget(e);
		var h = this.view.findHeaderCell(t);
		if (h) {
			return {
				ddel :h.firstChild,
				header :h
			};
		}
		return false;
	},

	onInitDrag : function(e) {
		this.view.headersDisabled = true;
		var clone = this.dragData.ddel.cloneNode(true);
		clone.id = Tera.id();
		clone.style.width = Math.min(this.dragData.header.offsetWidth, this.maxDragWidth) + "px";
		this.proxy.update(clone);
		return true;
	},

	afterValidDrop : function() {
		var v = this.view;
		setTimeout( function() {
			v.headersDisabled = false;
		}, 50);
	},

	afterInvalidDrop : function() {
		var v = this.view;
		setTimeout( function() {
			v.headersDisabled = false;
		}, 50);
	}
});

Tera.grid.HeaderDropZone = function(grid, hd, hd2) {
	this.grid = grid;
	this.view = grid.getView();

	this.proxyTop = Tera.DomHelper.append(document.body, {
		cls :"col-move-top",
		html :"&#160;"
	}, true);
	this.proxyBottom = Tera.DomHelper.append(document.body, {
		cls :"col-move-bottom",
		html :"&#160;"
	}, true);
	this.proxyTop.hide = this.proxyBottom.hide = function() {
		this.setLeftTop(-100, -100);
		this.setStyle("visibility", "hidden");
	};
	this.ddGroup = "gridHeader" + this.grid.getGridEl().id;

	Tera.grid.HeaderDropZone.superclass.constructor.call(this, grid.getGridEl().dom);
};
Tera.extend(Tera.grid.HeaderDropZone, Tera.dd.DropZone, {
	proxyOffsets : [ -4, -9 ],
	fly :Tera.Element.fly,

	getTargetFromEvent : function(e) {
		var t = Tera.lib.Event.getTarget(e);
		var cindex = this.view.findCellIndex(t);
		if (cindex !== false) {
			return this.view.getHeaderCell(cindex);
		}
	},

	nextVisible : function(h) {
		var v = this.view, cm = this.grid.colModel;
		h = h.nextSibling;
		while (h) {
			if (!cm.isHidden(v.getCellIndex(h))) {
				return h;
			}
			h = h.nextSibling;
		}
		return null;
	},

	prevVisible : function(h) {
		var v = this.view, cm = this.grid.colModel;
		h = h.prevSibling;
		while (h) {
			if (!cm.isHidden(v.getCellIndex(h))) {
				return h;
			}
			h = h.prevSibling;
		}
		return null;
	},

	positionIndicator : function(h, n, e) {
		var x = Tera.lib.Event.getPageX(e);
		var r = Tera.lib.Dom.getRegion(n.firstChild);
		var px, pt, py = r.top + this.proxyOffsets[1];
		if ((r.right - x) <= (r.right - r.left) / 2) {
			px = r.right + this.view.borderWidth;
			pt = "after";
		} else {
			px = r.left;
			pt = "before";
		}
		var oldIndex = this.view.getCellIndex(h);
		var newIndex = this.view.getCellIndex(n);

		if (this.grid.colModel.isFixed(newIndex)) {
			return false;
		}

		var locked = this.grid.colModel.isLocked(newIndex);

		if (pt == "after") {
			newIndex++;
		}
		if (oldIndex < newIndex) {
			newIndex--;
		}
		if (oldIndex == newIndex && (locked == this.grid.colModel.isLocked(oldIndex))) {
			return false;
		}
		px += this.proxyOffsets[0];
		this.proxyTop.setLeftTop(px, py);
		this.proxyTop.show();
		if (!this.bottomOffset) {
			this.bottomOffset = this.view.mainHd.getHeight();
		}
		this.proxyBottom.setLeftTop(px, py + this.proxyTop.dom.offsetHeight + this.bottomOffset);
		this.proxyBottom.show();
		return pt;
	},

	onNodeEnter : function(n, dd, e, data) {
		if (data.header != n) {
			this.positionIndicator(data.header, n, e);
		}
	},

	onNodeOver : function(n, dd, e, data) {
		var result = false;
		if (data.header != n) {
			result = this.positionIndicator(data.header, n, e);
		}
		if (!result) {
			this.proxyTop.hide();
			this.proxyBottom.hide();
		}
		return result ? this.dropAllowed : this.dropNotAllowed;
	},

	onNodeOut : function(n, dd, e, data) {
		this.proxyTop.hide();
		this.proxyBottom.hide();
	},

	onNodeDrop : function(n, dd, e, data) {
		var h = data.header;
		if (h != n) {
			var cm = this.grid.colModel;
			var x = Tera.lib.Event.getPageX(e);
			var r = Tera.lib.Dom.getRegion(n.firstChild);
			var pt = (r.right - x) <= ((r.right - r.left) / 2) ? "after" : "before";
			var oldIndex = this.view.getCellIndex(h);
			var newIndex = this.view.getCellIndex(n);
			var locked = cm.isLocked(newIndex);
			if (pt == "after") {
				newIndex++;
			}
			if (oldIndex < newIndex) {
				newIndex--;
			}
			if (oldIndex == newIndex && (locked == cm.isLocked(oldIndex))) {
				return false;
			}
			cm.setLocked(oldIndex, locked, true);
			cm.moveColumn(oldIndex, newIndex);
			this.grid.fireEvent("columnmove", oldIndex, newIndex);
			return true;
		}
		return false;
	}
});

Tera.grid.GridView.ColumnDragZone = function(grid, hd) {
	Tera.grid.GridView.ColumnDragZone.superclass.constructor.call(this, grid, hd, null);
	this.proxy.el.addClass('x-grid3-col-dd');
};

Tera.extend(Tera.grid.GridView.ColumnDragZone, Tera.grid.HeaderDragZone, {
	handleMouseDown : function(e) {

	},

	callHandleMouseDown : function(e) {
		Tera.grid.GridView.ColumnDragZone.superclass.handleMouseDown.call(this, e);
	}
});
Tera.grid.SplitDragZone = function(grid, hd, hd2) {
	this.grid = grid;
	this.view = grid.getView();
	this.proxy = this.view.resizeProxy;
	Tera.grid.SplitDragZone.superclass.constructor.call(this, hd, "gridSplitters" + this.grid.getGridEl().id, {
		dragElId :Tera.id(this.proxy.dom),
		resizeFrame :false
	});
	this.setHandleElId(Tera.id(hd));
	this.setOuterHandleElId(Tera.id(hd2));
	this.scroll = false;
};
Tera.extend(Tera.grid.SplitDragZone, Tera.dd.DDProxy, {
	fly :Tera.Element.fly,

	b4StartDrag : function(x, y) {
		this.view.headersDisabled = true;
		this.proxy.setHeight(this.view.mainWrap.getHeight());
		var w = this.cm.getColumnWidth(this.cellIndex);
		var minw = Math.max(w - this.grid.minColumnWidth, 0);
		this.resetConstraints();
		this.setXConstraint(minw, 1000);
		this.setYConstraint(0, 0);
		this.minX = x - minw;
		this.maxX = x + 1000;
		this.startPos = x;
		Tera.dd.DDProxy.prototype.b4StartDrag.call(this, x, y);
	},

	handleMouseDown : function(e) {
		ev = Tera.EventObject.setEvent(e);
		var t = this.fly(ev.getTarget());
		if (t.hasClass("x-grid-split")) {
			this.cellIndex = this.view.getCellIndex(t.dom);
			this.split = t.dom;
			this.cm = this.grid.colModel;
			if (this.cm.isResizable(this.cellIndex) && !this.cm.isFixed(this.cellIndex)) {
				Tera.grid.SplitDragZone.superclass.handleMouseDown.apply(this, arguments);
			}
		}
	},

	endDrag : function(e) {
		this.view.headersDisabled = false;
		var endX = Math.max(this.minX, Tera.lib.Event.getPageX(e));
		var diff = endX - this.startPos;
		this.view.onColumnSplitterMoved(this.cellIndex, this.cm.getColumnWidth(this.cellIndex) + diff);
	},

	autoOffset : function() {
		this.setDelta(0, 0);
	}
});
Tera.grid.GridDragZone = function(grid, config) {
	this.view = grid.getView();
	Tera.grid.GridDragZone.superclass.constructor.call(this, this.view.mainBody.dom, config);
	if (this.view.lockedBody) {
		this.setHandleElId(Tera.id(this.view.mainBody.dom));
		this.setOuterHandleElId(Tera.id(this.view.lockedBody.dom));
	}
	this.scroll = false;
	this.grid = grid;
	this.ddel = document.createElement('div');
	this.ddel.className = 'x-grid-dd-wrap';
};

Tera.extend(Tera.grid.GridDragZone, Tera.dd.DragZone, {
	ddGroup :"GridDD",

	getDragData : function(e) {
		var t = Tera.lib.Event.getTarget(e);
		var rowIndex = this.view.findRowIndex(t);
		if (rowIndex !== false) {
			var sm = this.grid.selModel;
			if (!sm.isSelected(rowIndex) || e.hasModifier()) {
				sm.handleMouseDown(this.grid, rowIndex, e);
			}
			return {
				grid :this.grid,
				ddel :this.ddel,
				rowIndex :rowIndex,
				selections :sm.getSelections()
			};
		}
		return false;
	},

	onInitDrag : function(e) {
		var data = this.dragData;
		this.ddel.innerHTML = this.grid.getDragDropText();
		this.proxy.update(this.ddel);
	},

	afterRepair : function() {
		this.dragging = false;
	},

	getRepairXY : function(e, data) {
		return false;
	},

	onEndDrag : function(data, e) {
	},

	onValidDrop : function(dd, e, id) {
		this.hideProxy();
	},

	beforeInvalidDrop : function(e, id) {

	}
});

Tera.grid.ColumnModel = function(config) {

	this.defaultWidth = 100;

	this.defaultSortable = false;

	if (config.columns) {
		Tera.apply(this, config);
		this.setConfig(config.columns, true);
	} else {
		this.setConfig(config, true);
	}
	this.addEvents(

	"widthchange",

	"headerchange",

	"hiddenchange",

	"columnmoved", "columnlockchange",

	"configchange");
	Tera.grid.ColumnModel.superclass.constructor.call(this);
};
Tera.extend(Tera.grid.ColumnModel, Tera.util.Observable, {

	getColumnId : function(index) {
		return this.config[index].id;
	},

	setConfig : function(config, initial) {
		if (!initial) {
			delete this.totalWidth;
			for ( var i = 0, len = this.config.length; i < len; i++) {
				var c = this.config[i];
				if (c.editor) {
					c.editor.destroy();
				}
			}
		}
		this.config = config;
		this.lookup = {};
		for ( var i = 0, len = config.length; i < len; i++) {
			var c = config[i];
			if (typeof c.renderer == "string") {
				c.renderer = Tera.util.Format[c.renderer];
			}
			if (typeof c.id == "undefined") {
				c.id = i;
			}
			if (c.editor && c.editor.isFormField) {
				c.editor = new Tera.grid.GridEditor(c.editor);
			}
			this.lookup[c.id] = c;
		}
		if (!initial) {
			this.fireEvent('configchange', this);
		}
	},

	getColumnById : function(id) {
		return this.lookup[id];
	},

	getIndexById : function(id) {
		for ( var i = 0, len = this.config.length; i < len; i++) {
			if (this.config[i].id == id) {
				return i;
			}
		}
		return -1;
	},

	moveColumn : function(oldIndex, newIndex) {
		var c = this.config[oldIndex];
		this.config.splice(oldIndex, 1);
		this.config.splice(newIndex, 0, c);
		this.dataMap = null;
		this.fireEvent("columnmoved", this, oldIndex, newIndex);
	},

	isLocked : function(colIndex) {
		return this.config[colIndex].locked === true;
	},

	setLocked : function(colIndex, value, suppressEvent) {
		if (this.isLocked(colIndex) == value) {
			return;
		}
		this.config[colIndex].locked = value;
		if (!suppressEvent) {
			this.fireEvent("columnlockchange", this, colIndex, value);
		}
	},

	getTotalLockedWidth : function() {
		var totalWidth = 0;
		for ( var i = 0; i < this.config.length; i++) {
			if (this.isLocked(i) && !this.isHidden(i)) {
				this.totalWidth += this.getColumnWidth(i);
			}
		}
		return totalWidth;
	},

	getLockedCount : function() {
		for ( var i = 0, len = this.config.length; i < len; i++) {
			if (!this.isLocked(i)) {
				return i;
			}
		}
	},

	getColumnCount : function(visibleOnly) {
		if (visibleOnly === true) {
			var c = 0;
			for ( var i = 0, len = this.config.length; i < len; i++) {
				if (!this.isHidden(i)) {
					c++;
				}
			}
			return c;
		}
		return this.config.length;
	},

	getColumnsBy : function(fn, scope) {
		var r = [];
		for ( var i = 0, len = this.config.length; i < len; i++) {
			var c = this.config[i];
			if (fn.call(scope || this, c, i) === true) {
				r[r.length] = c;
			}
		}
		return r;
	},

	isSortable : function(col) {
		if (typeof this.config[col].sortable == "undefined") {
			return this.defaultSortable;
		}
		return this.config[col].sortable;
	},

	isMenuDisabled : function(col) {
		return !!this.config[col].menuDisabled;
	},

	getRenderer : function(col) {
		if (!this.config[col].renderer) {
			return Tera.grid.ColumnModel.defaultRenderer;
		}
		return this.config[col].renderer;
	},

	setRenderer : function(col, fn) {
		this.config[col].renderer = fn;
	},

	getColumnWidth : function(col) {
		return this.config[col].width || this.defaultWidth;
	},

	setColumnWidth : function(col, width, suppressEvent) {
		this.config[col].width = width;
		this.totalWidth = null;
		if (!suppressEvent) {
			this.fireEvent("widthchange", this, col, width);
		}
	},

	getTotalWidth : function(includeHidden) {
		if (!this.totalWidth) {
			this.totalWidth = 0;
			for ( var i = 0, len = this.config.length; i < len; i++) {
				if (includeHidden || !this.isHidden(i)) {
					this.totalWidth += this.getColumnWidth(i);
				}
			}
		}
		return this.totalWidth;
	},

	getColumnHeader : function(col) {
		return this.config[col].header;
	},

	setColumnHeader : function(col, header) {
		this.config[col].header = header;
		this.fireEvent("headerchange", this, col, header);
	},

	getColumnTooltip : function(col) {
		return this.config[col].tooltip;
	},

	setColumnTooltip : function(col, tooltip) {
		this.config[col].tooltip = tooltip;
	},

	getDataIndex : function(col) {
		return this.config[col].dataIndex;
	},

	setDataIndex : function(col, dataIndex) {
		this.config[col].dataIndex = dataIndex;
	},

	findColumnIndex : function(dataIndex) {
		var c = this.config;
		for ( var i = 0, len = c.length; i < len; i++) {
			if (c[i].dataIndex == dataIndex) {
				return i;
			}
		}
		return -1;
	},

	isCellEditable : function(colIndex, rowIndex) {
		return (this.config[colIndex].editable || (typeof this.config[colIndex].editable == "undefined" && this.config[colIndex].editor)) ? true
				: false;
	},

	getCellEditor : function(colIndex, rowIndex) {
		return this.config[colIndex].editor;
	},

	setEditable : function(col, editable) {
		this.config[col].editable = editable;
	},

	isHidden : function(colIndex) {
		return this.config[colIndex].hidden;
	},

	isFixed : function(colIndex) {
		return this.config[colIndex].fixed;
	},

	isResizable : function(colIndex) {
		return colIndex >= 0 && this.config[colIndex].resizable !== false && this.config[colIndex].fixed !== true;
	},

	setHidden : function(colIndex, hidden) {
		var c = this.config[colIndex];
		if (c.hidden !== hidden) {
			c.hidden = hidden;
			this.totalWidth = null;
			this.fireEvent("hiddenchange", this, colIndex, hidden);
		}
	},

	setEditor : function(col, editor) {
		this.config[col].editor = editor;
	}
});

Tera.grid.ColumnModel.defaultRenderer = function(value) {
	if (typeof value == "string" && value.length < 1) {
		return "&#160;";
	}
	return value;
};

Tera.grid.DefaultColumnModel = Tera.grid.ColumnModel;

Tera.grid.AbstractSelectionModel = function() {
	this.locked = false;
	Tera.grid.AbstractSelectionModel.superclass.constructor.call(this);
};

Tera.extend(Tera.grid.AbstractSelectionModel, Tera.util.Observable, {

	init : function(grid) {
		this.grid = grid;
		this.initEvents();
	},

	lock : function() {
		this.locked = true;
	},

	unlock : function() {
		this.locked = false;
	},

	isLocked : function() {
		return this.locked;
	}
});

Tera.grid.RowSelectionModel = function(config) {
	Tera.apply(this, config);
	this.selections = new Tera.util.MixedCollection(false, function(o) {
		return o.id;
	});

	this.last = false;
	this.lastActive = false;

	this.addEvents(

	"selectionchange",

	"beforerowselect",

	"rowselect",

	"rowdeselect");

	Tera.grid.RowSelectionModel.superclass.constructor.call(this);
};

Tera.extend(Tera.grid.RowSelectionModel, Tera.grid.AbstractSelectionModel, {

	singleSelect :false,

	initEvents : function() {

		if (!this.grid.enableDragDrop && !this.grid.enableDrag) {
			this.grid.on("rowmousedown", this.handleMouseDown, this);
		} else {
			this.grid.on("rowclick", function(grid, rowIndex, e) {
				if (e.button === 0 && !e.shiftKey && !e.ctrlKey) {
					this.selectRow(rowIndex, false);
					grid.view.focusRow(rowIndex);
				}
			}, this);
		}

		this.rowNav = new Tera.KeyNav(this.grid.getGridEl(), {
			"up" : function(e) {
				if (!e.shiftKey) {
					this.selectPrevious(e.shiftKey);
				} else if (this.last !== false && this.lastActive !== false) {
					var last = this.last;
					this.selectRange(this.last, this.lastActive - 1);
					this.grid.getView().focusRow(this.lastActive);
					if (last !== false) {
						this.last = last;
					}
				} else {
					this.selectFirstRow();
				}
			},
			"down" : function(e) {
				if (!e.shiftKey) {
					this.selectNext(e.shiftKey);
				} else if (this.last !== false && this.lastActive !== false) {
					var last = this.last;
					this.selectRange(this.last, this.lastActive + 1);
					this.grid.getView().focusRow(this.lastActive);
					if (last !== false) {
						this.last = last;
					}
				} else {
					this.selectFirstRow();
				}
			},
			scope :this
		});

		var view = this.grid.view;
		view.on("refresh", this.onRefresh, this);
		view.on("rowupdated", this.onRowUpdated, this);
		view.on("rowremoved", this.onRemove, this);
	},

	onRefresh : function() {
		var ds = this.grid.store, index;
		var s = this.getSelections();
		this.clearSelections(true);
		for ( var i = 0, len = s.length; i < len; i++) {
			var r = s[i];
			if ((index = ds.indexOfId(r.id)) != -1) {
				this.selectRow(index, true);
			}
		}
		if (s.length != this.selections.getCount()) {
			this.fireEvent("selectionchange", this);
		}
	},

	onRemove : function(v, index, r) {
		if (this.selections.remove(r) !== false) {
			this.fireEvent('selectionchange', this);
		}
	},

	onRowUpdated : function(v, index, r) {
		if (this.isSelected(r)) {
			v.onRowSelect(index);
		}
	},

	selectRecords : function(records, keepExisting) {
		if (!keepExisting) {
			this.clearSelections();
		}
		var ds = this.grid.store;
		for ( var i = 0, len = records.length; i < len; i++) {
			this.selectRow(ds.indexOf(records[i]), true);
		}
	},

	getCount : function() {
		return this.selections.length;
	},

	selectFirstRow : function() {
		this.selectRow(0);
	},

	selectLastRow : function(keepExisting) {
		this.selectRow(this.grid.store.getCount() - 1, keepExisting);
	},

	selectNext : function(keepExisting) {
		if (this.hasNext()) {
			this.selectRow(this.last + 1, keepExisting);
			this.grid.getView().focusRow(this.last);
			return true;
		}
		return false;
	},

	selectPrevious : function(keepExisting) {
		if (this.hasPrevious()) {
			this.selectRow(this.last - 1, keepExisting);
			this.grid.getView().focusRow(this.last);
			return true;
		}
		return false;
	},

	hasNext : function() {
		return this.last !== false && (this.last + 1) < this.grid.store.getCount();
	},

	hasPrevious : function() {
		return !!this.last;
	},

	getSelections : function() {
		return [].concat(this.selections.items);
	},

	getSelected : function() {
		return this.selections.itemAt(0);
	},

	each : function(fn, scope) {
		var s = this.getSelections();
		for ( var i = 0, len = s.length; i < len; i++) {
			if (fn.call(scope || this, s[i], i) === false) {
				return false;
			}
		}
		return true;
	},

	clearSelections : function(fast) {
		if (this.locked)
			return;
		if (fast !== true) {
			var ds = this.grid.store;
			var s = this.selections;
			s.each( function(r) {
				this.deselectRow(ds.indexOfId(r.id));
			}, this);
			s.clear();
		} else {
			this.selections.clear();
		}
		this.last = false;
	},

	selectAll : function() {
		if (this.locked)
			return;
		this.selections.clear();
		for ( var i = 0, len = this.grid.store.getCount(); i < len; i++) {
			this.selectRow(i, true);
		}
	},

	hasSelection : function() {
		return this.selections.length > 0;
	},

	isSelected : function(index) {
		var r = typeof index == "number" ? this.grid.store.getAt(index) : index;
		return (r && this.selections.key(r.id) ? true : false);
	},

	isIdSelected : function(id) {
		return (this.selections.key(id) ? true : false);
	},

	handleMouseDown : function(g, rowIndex, e) {
		if (e.button !== 0 || this.isLocked()) {
			return;
		}
		;
		var view = this.grid.getView();
		if (e.shiftKey && this.last !== false) {
			var last = this.last;
			this.selectRange(last, rowIndex, e.ctrlKey);
			this.last = last;
			view.focusRow(rowIndex);
		} else {
			var isSelected = this.isSelected(rowIndex);
			if (e.ctrlKey && isSelected) {
				this.deselectRow(rowIndex);
			} else if (!isSelected || this.getCount() > 1) {
				this.selectRow(rowIndex, e.ctrlKey || e.shiftKey);
				view.focusRow(rowIndex);
			}
		}
	},

	selectRows : function(rows, keepExisting) {
		if (!keepExisting) {
			this.clearSelections();
		}
		for ( var i = 0, len = rows.length; i < len; i++) {
			this.selectRow(rows[i], true);
		}
	},

	selectRange : function(startRow, endRow, keepExisting) {
		if (this.locked)
			return;
		if (!keepExisting) {
			this.clearSelections();
		}
		if (startRow <= endRow) {
			for ( var i = startRow; i <= endRow; i++) {
				this.selectRow(i, true);
			}
		} else {
			for ( var i = startRow; i >= endRow; i--) {
				this.selectRow(i, true);
			}
		}
	},

	deselectRange : function(startRow, endRow, preventViewNotify) {
		if (this.locked)
			return;
		for ( var i = startRow; i <= endRow; i++) {
			this.deselectRow(i, preventViewNotify);
		}
	},

	selectRow : function(index, keepExisting, preventViewNotify) {
		if (this.locked || (index < 0 || index >= this.grid.store.getCount()))
			return;
		var r = this.grid.store.getAt(index);
		if (r && this.fireEvent("beforerowselect", this, index, keepExisting, r) !== false) {
			if (!keepExisting || this.singleSelect) {
				this.clearSelections();
			}
			this.selections.add(r);
			this.last = this.lastActive = index;
			if (!preventViewNotify) {
				this.grid.getView().onRowSelect(index);
			}
			this.fireEvent("rowselect", this, index, r);
			this.fireEvent("selectionchange", this);
		}
	},

	deselectRow : function(index, preventViewNotify) {
		if (this.locked)
			return;
		if (this.last == index) {
			this.last = false;
		}
		if (this.lastActive == index) {
			this.lastActive = false;
		}
		var r = this.grid.store.getAt(index);
		if (r) {
			this.selections.remove(r);
			if (!preventViewNotify) {
				this.grid.getView().onRowDeselect(index);
			}
			this.fireEvent("rowdeselect", this, index, r);
			this.fireEvent("selectionchange", this);
		}
	},

	restoreLast : function() {
		if (this._last) {
			this.last = this._last;
		}
	},

	acceptsNav : function(row, col, cm) {
		return !cm.isHidden(col) && cm.isCellEditable(col, row);
	},

	onEditorKey : function(field, e) {
		var k = e.getKey(), newCell, g = this.grid, ed = g.activeEditor;
		var shift = e.shiftKey;
		if (k == e.TAB) {
			e.stopEvent();
			ed.completeEdit();
			if (shift) {
				newCell = g.walkCells(ed.row, ed.col - 1, -1, this.acceptsNav, this);
			} else {
				newCell = g.walkCells(ed.row, ed.col + 1, 1, this.acceptsNav, this);
			}
		} else if (k == e.ENTER) {
			e.stopEvent();
			ed.completeEdit();
			if (this.moveEditorOnEnter !== false) {
				if (shift) {
					newCell = g.walkCells(ed.row - 1, ed.col, -1, this.acceptsNav, this);
				} else {
					newCell = g.walkCells(ed.row + 1, ed.col, 1, this.acceptsNav, this);
				}
			}
		} else if (k == e.ESC) {
			ed.cancelEdit();
		}
		if (newCell) {
			g.startEditing(newCell[0], newCell[1]);
		}
	}
});

Tera.grid.CellSelectionModel = function(config) {
	Tera.apply(this, config);

	this.selection = null;

	this.addEvents(

	"beforecellselect",

	"cellselect",

	"selectionchange");

	Tera.grid.CellSelectionModel.superclass.constructor.call(this);
};

Tera.extend(Tera.grid.CellSelectionModel, Tera.grid.AbstractSelectionModel, {

	initEvents : function() {
		this.grid.on("cellmousedown", this.handleMouseDown, this);
		this.grid.getGridEl().on(Tera.isIE || Tera.isSafari3 ? "keydown" : "keypress", this.handleKeyDown, this);
		var view = this.grid.view;
		view.on("refresh", this.onViewChange, this);
		view.on("rowupdated", this.onRowUpdated, this);
		view.on("beforerowremoved", this.clearSelections, this);
		view.on("beforerowsinserted", this.clearSelections, this);
		if (this.grid.isEditor) {
			this.grid.on("beforeedit", this.beforeEdit, this);
		}
	},

	beforeEdit : function(e) {
		this.select(e.row, e.column, false, true, e.record);
	},

	onRowUpdated : function(v, index, r) {
		if (this.selection && this.selection.record == r) {
			v.onCellSelect(index, this.selection.cell[1]);
		}
	},

	onViewChange : function() {
		this.clearSelections(true);
	},

	getSelectedCell : function() {
		return this.selection ? this.selection.cell : null;
	},

	clearSelections : function(preventNotify) {
		var s = this.selection;
		if (s) {
			if (preventNotify !== true) {
				this.grid.view.onCellDeselect(s.cell[0], s.cell[1]);
			}
			this.selection = null;
			this.fireEvent("selectionchange", this, null);
		}
	},

	hasSelection : function() {
		return this.selection ? true : false;
	},

	handleMouseDown : function(g, row, cell, e) {
		if (e.button !== 0 || this.isLocked()) {
			return;
		}
		;
		this.select(row, cell);
	},

	select : function(rowIndex, colIndex, preventViewNotify, preventFocus, r) {
		if (this.fireEvent("beforecellselect", this, rowIndex, colIndex) !== false) {
			this.clearSelections();
			r = r || this.grid.store.getAt(rowIndex);
			this.selection = {
				record :r,
				cell : [ rowIndex, colIndex ]
			};
			if (!preventViewNotify) {
				var v = this.grid.getView();
				v.onCellSelect(rowIndex, colIndex);
				if (preventFocus !== true) {
					v.focusCell(rowIndex, colIndex);
				}
			}
			this.fireEvent("cellselect", this, rowIndex, colIndex);
			this.fireEvent("selectionchange", this, this.selection);
		}
	},

	isSelectable : function(rowIndex, colIndex, cm) {
		return !cm.isHidden(colIndex);
	},

	handleKeyDown : function(e) {
		if (!e.isNavKeyPress()) {
			return;
		}
		var g = this.grid, s = this.selection;
		if (!s) {
			e.stopEvent();
			var cell = g.walkCells(0, 0, 1, this.isSelectable, this);
			if (cell) {
				this.select(cell[0], cell[1]);
			}
			return;
		}
		var sm = this;
		var walk = function(row, col, step) {
			return g.walkCells(row, col, step, sm.isSelectable, sm);
		};
		var k = e.getKey(), r = s.cell[0], c = s.cell[1];
		var newCell;

		switch (k) {
			case e.TAB:
				if (e.shiftKey) {
					newCell = walk(r, c - 1, -1);
				} else {
					newCell = walk(r, c + 1, 1);
				}
				break;
			case e.DOWN:
				newCell = walk(r + 1, c, 1);
				break;
			case e.UP:
				newCell = walk(r - 1, c, -1);
				break;
			case e.RIGHT:
				newCell = walk(r, c + 1, 1);
				break;
			case e.LEFT:
				newCell = walk(r, c - 1, -1);
				break;
			case e.ENTER:
				if (g.isEditor && !g.editing) {
					g.startEditing(r, c);
					e.stopEvent();
					return;
				}
				break;
		}
		;
		if (newCell) {
			this.select(newCell[0], newCell[1]);
			e.stopEvent();
		}
	},

	acceptsNav : function(row, col, cm) {
		return !cm.isHidden(col) && cm.isCellEditable(col, row);
	},

	onEditorKey : function(field, e) {
		var k = e.getKey(), newCell, g = this.grid, ed = g.activeEditor;
		if (k == e.TAB) {
			if (e.shiftKey) {
				newCell = g.walkCells(ed.row, ed.col - 1, -1, this.acceptsNav, this);
			} else {
				newCell = g.walkCells(ed.row, ed.col + 1, 1, this.acceptsNav, this);
			}
			e.stopEvent();
		} else if (k == e.ENTER) {
			ed.completeEdit();
			e.stopEvent();
		} else if (k == e.ESC) {
			e.stopEvent();
			ed.cancelEdit();
		}
		if (newCell) {
			g.startEditing(newCell[0], newCell[1]);
		}
	}
});

Tera.grid.EditorGridPanel = Tera.extend(Tera.grid.GridPanel, {

	clicksToEdit :2,

	isEditor :true,
	detectEdit :false,

	autoEncode :false,

	trackMouseOver :false,
	
	viewConfig : {
		getEditorParent: function() {
			return this.mainWrap.dom;
		}
	},
	
	initComponent : function() {
		Tera.grid.EditorGridPanel.superclass.initComponent.call(this);

		if (!this.selModel) {

			this.selModel = new Tera.grid.CellSelectionModel();
		}

		this.activeEditor = null;

		this.addEvents(

		"beforeedit",

		"afteredit",

		"validateedit");
	},

	initEvents : function() {
		Tera.grid.EditorGridPanel.superclass.initEvents.call(this);

		this.on("bodyscroll", this.stopEditing, this, [ true ]);

		if (this.clicksToEdit == 1) {
			this.on("cellclick", this.onCellDblClick, this);
		} else {
			if (this.clicksToEdit == 'auto' && this.view.mainBody) {
				this.view.mainBody.on("mousedown", this.onAutoEditClick, this);
			}
			this.on("celldblclick", this.onCellDblClick, this);
		}
		this.getGridEl().addClass("xedit-grid");
	},

	onCellDblClick : function(g, row, col) {
		this.startEditing(row, col);
	},

	onAutoEditClick : function(e, t) {
		if (e.button !== 0) {
			return;
		}
		var row = this.view.findRowIndex(t);
		var col = this.view.findCellIndex(t);
		if (row !== false && col !== false) {
			this.stopEditing();
			if (this.selModel.getSelectedCell) {
				var sc = this.selModel.getSelectedCell();
				if (sc && sc.cell[0] === row && sc.cell[1] === col) {
					this.startEditing(row, col);
				}
			} else {
				if (this.selModel.isSelected(row)) {
					this.startEditing(row, col);
				}
			}
		}
	},

	onEditComplete : function(ed, value, startValue) {
		this.editing = false;
		this.activeEditor = null;
		ed.un("specialkey", this.selModel.onEditorKey, this.selModel);
		var r = ed.record;
		var field = this.colModel.getDataIndex(ed.col);
		value = this.postEditValue(value, startValue, r, field);
		if (String(value) !== String(startValue)) {
			var e = {
				grid :this,
				record :r,
				field :field,
				originalValue :startValue,
				value :value,
				row :ed.row,
				column :ed.col,
				cancel :false
			};
			if (this.fireEvent("validateedit", e) !== false && !e.cancel) {
				r.set(field, e.value);
				delete e.cancel;
				this.fireEvent("afteredit", e);
			}
		}
		this.view.focusCell(ed.row, ed.col);
	},

	startEditing : function(row, col) {
		this.stopEditing();
		if (this.colModel.isCellEditable(col, row)) {
			this.view.ensureVisible(row, col, true);
			var r = this.store.getAt(row);
			var field = this.colModel.getDataIndex(col);
			var e = {
				grid :this,
				record :r,
				field :field,
				value :r.data[field],
				row :row,
				column :col,
				cancel :false
			};
			if (this.fireEvent("beforeedit", e) !== false && !e.cancel) {
				this.editing = true;
				var ed = this.colModel.getCellEditor(col, row);
				if (!ed.rendered) {
					ed.render(this.view.getEditorParent(ed));
				}
				( function() {
					ed.row = row;
					ed.col = col;
					ed.record = r;
					ed.on("complete", this.onEditComplete, this, {
						single :true
					});
					ed.on("specialkey", this.selModel.onEditorKey, this.selModel);
					this.activeEditor = ed;
					var v = this.preEditValue(r, field);
					ed.startEdit(this.view.getCell(row, col), v);
				}).defer(50, this);
			}
		}
	},

	preEditValue : function(r, field) {
		return this.autoEncode && typeof value == 'string' ? Tera.util.Format.htmlDecode(r.data[field]) : r.data[field];
	},

	postEditValue : function(value, originalValue, r, field) {
		return this.autoEncode && typeof value == 'string' ? Tera.util.Format.htmlEncode(value) : value;
	},

	stopEditing : function(cancel) {
		if (this.activeEditor) {
			this.activeEditor[cancel === true ? 'cancelEdit' : 'completeEdit']();
		}
		this.activeEditor = null;
	}
});
Tera.reg('editorgrid', Tera.grid.EditorGridPanel);
Tera.grid.GridEditor = function(field, config) {
	Tera.grid.GridEditor.superclass.constructor.call(this, field, config);
	field.monitorTab = false;
};

Tera.extend(Tera.grid.GridEditor, Tera.Editor, {
	alignment :"tl-tl",
	autoSize :"width",
	hideEl :false,
	cls :"x-small-editor x-grid-editor",
	shim :false,
	shadow :false
});

Tera.grid.PropertyRecord = Tera.data.Record.create( [ {
	name :'name',
	type :'string'
}, 'value' ]);

Tera.grid.PropertyStore = function(grid, source) {
	this.grid = grid;
	this.store = new Tera.data.Store( {
		recordType :Tera.grid.PropertyRecord
	});
	this.store.on('update', this.onUpdate, this);
	if (source) {
		this.setSource(source);
	}
	Tera.grid.PropertyStore.superclass.constructor.call(this);
};
Tera.extend(Tera.grid.PropertyStore, Tera.util.Observable, {
	setSource : function(o) {
		this.source = o;
		this.store.removeAll();
		var data = [];
		for ( var k in o) {
			if (this.isEditableValue(o[k])) {
				data.push(new Tera.grid.PropertyRecord( {
					name :k,
					value :o[k]
				}, k));
			}
		}
		this.store.loadRecords( {
			records :data
		}, {}, true);
	},

	onUpdate : function(ds, record, type) {
		if (type == Tera.data.Record.EDIT) {
			var v = record.data['value'];
			var oldValue = record.modified['value'];
			if (this.grid.fireEvent('beforepropertychange', this.source, record.id, v, oldValue) !== false) {
				this.source[record.id] = v;
				record.commit();
				this.grid.fireEvent('propertychange', this.source, record.id, v, oldValue);
			} else {
				record.reject();
			}
		}
	},

	getProperty : function(row) {
		return this.store.getAt(row);
	},

	isEditableValue : function(val) {
		if (Tera.isDate(val)) {
			return true;
		} else if (typeof val == 'object' || typeof val == 'function') {
			return false;
		}
		return true;
	},

	setValue : function(prop, value) {
		this.source[prop] = value;
		this.store.getById(prop).set('value', value);
	},

	getSource : function() {
		return this.source;
	}
});

Tera.grid.PropertyColumnModel = function(grid, store) {
	this.grid = grid;
	var g = Tera.grid;
	g.PropertyColumnModel.superclass.constructor.call(this, [ {
		header :this.nameText,
		width :50,
		sortable :true,
		dataIndex :'name',
		id :'name',
		menuDisabled :true
	}, {
		header :this.valueText,
		width :50,
		resizable :false,
		dataIndex :'value',
		id :'value',
		menuDisabled :true
	} ]);
	this.store = store;
	this.bselect = Tera.DomHelper.append(document.body, {
		tag :'select',
		cls :'x-grid-editor x-hide-display',
		children : [ {
			tag :'option',
			value :'true',
			html :'true'
		}, {
			tag :'option',
			value :'false',
			html :'false'
		} ]
	});
	var f = Tera.form;

	var bfield = new f.Field( {
		el :this.bselect,
		bselect :this.bselect,
		autoShow :true,
		getValue : function() {
			return this.bselect.value == 'true';
		}
	});
	this.editors = {
		'date' :new g.GridEditor(new f.DateField( {
			selectOnFocus :true
		})),
		'string' :new g.GridEditor(new f.TextField( {
			selectOnFocus :true
		})),
		'number' :new g.GridEditor(new f.NumberField( {
			selectOnFocus :true,
			style :'text-align:left;'
		})),
		'boolean' :new g.GridEditor(bfield)
	};
	this.renderCellDelegate = this.renderCell.createDelegate(this);
	this.renderPropDelegate = this.renderProp.createDelegate(this);
};

Tera.extend(Tera.grid.PropertyColumnModel, Tera.grid.ColumnModel, {
	nameText :'Name',
	valueText :'Value',
	dateFormat :'m/j/Y',

	renderDate : function(dateVal) {
		return dateVal.dateFormat(this.dateFormat);
	},

	renderBool : function(bVal) {
		return bVal ? 'true' : 'false';
	},

	isCellEditable : function(colIndex, rowIndex) {
		return colIndex == 1;
	},

	getRenderer : function(col) {
		return col == 1 ? this.renderCellDelegate : this.renderPropDelegate;
	},

	renderProp : function(v) {
		return this.getPropertyName(v);
	},

	renderCell : function(val) {
		var rv = val;
		if (Tera.isDate(val)) {
			rv = this.renderDate(val);
		} else if (typeof val == 'boolean') {
			rv = this.renderBool(val);
		}
		return Tera.util.Format.htmlEncode(rv);
	},

	getPropertyName : function(name) {
		var pn = this.grid.propertyNames;
		return pn && pn[name] ? pn[name] : name;
	},

	getCellEditor : function(colIndex, rowIndex) {
		var p = this.store.getProperty(rowIndex);
		var n = p.data['name'], val = p.data['value'];
		if (this.grid.customEditors[n]) {
			return this.grid.customEditors[n];
		}
		if (Tera.isDate(val)) {
			return this.editors['date'];
		} else if (typeof val == 'number') {
			return this.editors['number'];
		} else if (typeof val == 'boolean') {
			return this.editors['boolean'];
		} else {
			return this.editors['string'];
		}
	}
});

Tera.grid.PropertyGrid = Tera.extend(Tera.grid.EditorGridPanel, {

	enableColumnMove :false,
	stripeRows :false,
	trackMouseOver :false,
	clicksToEdit :1,
	enableHdMenu :false,
	viewConfig : {
		forceFit :true
	},

	initComponent : function() {
		this.customEditors = this.customEditors || {};
		this.lastEditRow = null;
		var store = new Tera.grid.PropertyStore(this);
		this.propStore = store;
		var cm = new Tera.grid.PropertyColumnModel(this, store);
		store.store.sort('name', 'ASC');
		this.addEvents(

		'beforepropertychange',

		'propertychange');
		this.cm = cm;
		this.ds = store.store;
		Tera.grid.PropertyGrid.superclass.initComponent.call(this);

		this.selModel.on('beforecellselect', function(sm, rowIndex, colIndex) {
			if (colIndex === 0) {
				this.startEditing.defer(200, this, [ rowIndex, 1 ]);
				return false;
			}
		}, this);
	},

	onRender : function() {
		Tera.grid.PropertyGrid.superclass.onRender.apply(this, arguments);

		this.getGridEl().addClass('x-props-grid');
	},

	afterRender : function() {
		Tera.grid.PropertyGrid.superclass.afterRender.apply(this, arguments);
		if (this.source) {
			this.setSource(this.source);
		}
	},

	setSource : function(source) {
		this.propStore.setSource(source);
	},

	getSource : function() {
		return this.propStore.getSource();
	}
});
Tera.reg("propertygrid", Tera.grid.PropertyGrid);

Tera.grid.RowNumberer = function(config) {
	Tera.apply(this, config);
	if (this.rowspan) {
		this.renderer = this.renderer.createDelegate(this);
	}
};

Tera.grid.RowNumberer.prototype = {

	header :"",

	width :23,

	sortable :false,

	fixed :true,
	menuDisabled :true,
	dataIndex :'',
	id :'numberer',
	rowspan :undefined,

	renderer : function(v, p, record, rowIndex) {
		if (this.rowspan) {
			p.cellAttr = 'rowspan="' + this.rowspan + '"';
		}
		return rowIndex + 1;
	}
};

Tera.grid.CheckboxSelectionModel = Tera.extend(Tera.grid.RowSelectionModel, {

	header :'<div class="x-grid3-hd-checker">&#160;</div>',

	width :20,

	sortable :false,

	menuDisabled :true,
	fixed :true,
	dataIndex :'',
	id :'checker',

	initEvents : function() {
		Tera.grid.CheckboxSelectionModel.superclass.initEvents.call(this);
		this.grid.on('render', function() {
			var view = this.grid.getView();
			view.mainBody.on('mousedown', this.onMouseDown, this);
			Tera.fly(view.innerHd).on('mousedown', this.onHdMouseDown, this);

		}, this);
	},

	onMouseDown : function(e, t) {
		if (e.button === 0 && t.className == 'x-grid3-row-checker') {
			e.stopEvent();
			var row = e.getTarget('.x-grid3-row');
			if (row) {
				var index = row.rowIndex;
				if (this.isSelected(index)) {
					this.deselectRow(index);
				} else {
					this.selectRow(index, true);
				}
			}
		}
	},

	onHdMouseDown : function(e, t) {
		if (t.className == 'x-grid3-hd-checker') {
			e.stopEvent();
			var hd = Tera.fly(t.parentNode);
			var isChecked = hd.hasClass('x-grid3-hd-checker-on');
			if (isChecked) {
				hd.removeClass('x-grid3-hd-checker-on');
				this.clearSelections();
			} else {
				hd.addClass('x-grid3-hd-checker-on');
				this.selectAll();
			}
		}
	},

	renderer : function(v, p, record) {
		return '<div class="x-grid3-row-checker">&#160;</div>';
	}
});

Tera.LoadMask = function(el, config) {
	this.el = Tera.get(el);
	Tera.apply(this, config);
	if (this.store) {
		this.store.on('beforeload', this.onBeforeLoad, this);
		this.store.on('load', this.onLoad, this);
		this.store.on('loadexception', this.onLoad, this);
		this.removeMask = Tera.value(this.removeMask, false);
	} else {
		var um = this.el.getUpdater();
		um.showLoadIndicator = false;
		um.on('beforeupdate', this.onBeforeLoad, this);
		um.on('update', this.onLoad, this);
		um.on('failure', this.onLoad, this);
		this.removeMask = Tera.value(this.removeMask, true);
	}
};

Tera.LoadMask.prototype = {

	msg :'Loading...',

	msgCls :'x-mask-loading',

	disabled :false,

	disable : function() {
		this.disabled = true;
	},

	enable : function() {
		this.disabled = false;
	},

	onLoad : function() {
		this.el.unmask(this.removeMask);
	},

	onBeforeLoad : function() {
		if (!this.disabled) {
			this.el.mask(this.msg, this.msgCls);
		}
	},

	show : function() {
		this.onBeforeLoad();
	},

	hide : function() {
		this.onLoad();
	},

	destroy : function() {
		if (this.store) {
			this.store.un('beforeload', this.onBeforeLoad, this);
			this.store.un('load', this.onLoad, this);
			this.store.un('loadexception', this.onLoad, this);
		} else {
			var um = this.el.getUpdater();
			um.un('beforeupdate', this.onBeforeLoad, this);
			um.un('update', this.onLoad, this);
			um.un('failure', this.onLoad, this);
		}
	}
};

Tera.ProgressBar = Tera.extend(Tera.BoxComponent, {

	baseCls :'x-progress',

	waitTimer :null,

	initComponent : function() {
		Tera.ProgressBar.superclass.initComponent.call(this);
		this.addEvents(

		"update");
	},

	onRender : function(ct, position) {
		Tera.ProgressBar.superclass.onRender.call(this, ct, position);

		var tpl = new Tera.Template('<div class="{cls}-wrap">', '<div class="{cls}-inner">', '<div class="{cls}-bar">', '<div class="{cls}-text">',
				'<div>&#160;</div>', '</div>', '</div>', '<div class="{cls}-text {cls}-text-back">', '<div>&#160;</div>', '</div>', '</div>',
				'</div>');

		if (position) {
			this.el = tpl.insertBefore(position, {
				cls :this.baseCls
			}, true);
		} else {
			this.el = tpl.append(ct, {
				cls :this.baseCls
			}, true);
		}
		if (this.id) {
			this.el.dom.id = this.id;
		}
		var inner = this.el.dom.firstChild;
		this.progressBar = Tera.get(inner.firstChild);

		if (this.textEl) {

			this.textEl = Tera.get(this.textEl);
			delete this.textTopEl;
		} else {

			this.textTopEl = Tera.get(this.progressBar.dom.firstChild);
			var textBackEl = Tera.get(inner.childNodes[1]);
			this.textTopEl.setStyle("z-index", 99).addClass('x-hidden');
			this.textEl = new Tera.CompositeElement( [ this.textTopEl.dom.firstChild, textBackEl.dom.firstChild ]);
			this.textEl.setWidth(inner.offsetWidth);
		}
		this.progressBar.setHeight(inner.offsetHeight);
	},

	afterRender : function() {
		Tera.ProgressBar.superclass.afterRender.call(this);
		if (this.value) {
			this.updateProgress(this.value, this.text);
		} else {
			this.updateText(this.text);
		}
	},

	updateProgress : function(value, text) {
		this.value = value || 0;
		if (text) {
			this.updateText(text);
		}
		var w = Math.floor(value * this.el.dom.firstChild.offsetWidth);
		this.progressBar.setWidth(w);
		if (this.textTopEl) {

			this.textTopEl.removeClass('x-hidden').setWidth(w);
		}
		this.fireEvent('update', this, value, text);
		return this;
	},

	wait : function(o) {
		if (!this.waitTimer) {
			var scope = this;
			o = o || {};
			this.waitTimer = Tera.TaskMgr.start( {
				run : function(i) {
					var inc = o.increment || 10;
					this.updateProgress(((((i + inc) % inc) + 1) * (100 / inc)) * .01);
				},
				interval :o.interval || 1000,
				duration :o.duration,
				onStop : function() {
					if (o.fn) {
						o.fn.apply(o.scope || this);
					}
					this.reset();
				},
				scope :scope
			});
		}
		return this;
	},

	isWaiting : function() {
		return this.waitTimer != null;
	},

	updateText : function(text) {
		this.text = text || '&#160;';
		this.textEl.update(this.text);
		return this;
	},

	setSize : function(w, h) {
		Tera.ProgressBar.superclass.setSize.call(this, w, h);
		if (this.textTopEl) {
			var inner = this.el.dom.firstChild;
			this.textEl.setSize(inner.offsetWidth, inner.offsetHeight);
		}
		return this;
	},

	reset : function(hide) {
		this.updateProgress(0);
		if (this.textTopEl) {
			this.textTopEl.addClass('x-hidden');
		}
		if (this.waitTimer) {
			this.waitTimer.onStop = null;
			Tera.TaskMgr.stop(this.waitTimer);
			this.waitTimer = null;
		}
		if (hide === true) {
			this.hide();
		}
		return this;
	}
});
Tera.reg('progress', Tera.ProgressBar);

Tera.Slider = Tera.extend(Tera.BoxComponent, {

	vertical :false,

	minValue :0,

	maxValue :100,

	keyIncrement :1,

	increment :0,

	clickRange : [ 5, 15 ],

	clickToChange :true,

	animate :true,

	initComponent : function() {
		if (this.value === undefined) {
			this.value = this.minValue;
		}
		Tera.Slider.superclass.initComponent.call(this);
		this.keyIncrement = Math.max(this.increment, this.keyIncrement);
		this.addEvents(

		'beforechange',

		'change',

		'dragstart',

		'drag',

		'dragend');

		if (this.vertical) {
			Tera.apply(this, Tera.Slider.Vertical);
		}
	},

	onRender : function() {
		this.autoEl = {
			cls :'x-slider ' + (this.vertical ? 'x-slider-vert' : 'x-slider-horz'),
			cn : {
				cls :'x-slider-end',
				cn : {
					cls :'x-slider-inner',
					cn : [ {
						cls :'x-slider-thumb'
					}, {
						tag :'a',
						cls :'x-slider-focus',
						href :"#",
						tabIndex :'-1',
						hidefocus :'on'
					} ]
				}
			}
		};
		Tera.Slider.superclass.onRender.apply(this, arguments);
		this.endEl = this.el.first();
		this.innerEl = this.endEl.first();
		this.thumb = this.innerEl.first();
		this.halfThumb = (this.vertical ? this.thumb.getHeight() : this.thumb.getWidth()) / 2;
		this.focusEl = this.thumb.next();
		this.initEvents();
	},

	initEvents : function() {
		this.thumb.addClassOnOver('x-slider-thumb-over');
		this.mon(this.el, 'mousedown', this.onMouseDown, this);
		this.mon(this.el, 'keydown', this.onKeyDown, this);

		this.tracker = new Tera.dd.DragTracker( {
			onBeforeStart :this.onBeforeDragStart.createDelegate(this),
			onStart :this.onDragStart.createDelegate(this),
			onDrag :this.onDrag.createDelegate(this),
			onEnd :this.onDragEnd.createDelegate(this),
			tolerance :3,
			autoStart :300
		});
		this.tracker.initEl(this.thumb);
		this.on('beforedestroy', this.tracker.destroy, this.tracker);
	},

	onMouseDown : function(e) {
		if (this.disabled) {
			return;
		}
		if (this.clickToChange && e.target != this.thumb.dom) {
			var local = this.innerEl.translatePoints(e.getXY());
			this.onClickChange(local);
		}
		this.focus();
	},

	onClickChange : function(local) {
		if (local.top > this.clickRange[0] && local.top < this.clickRange[1]) {
			this.setValue(Math.round(local.left / this.getRatio()));
		}
	},

	onKeyDown : function(e) {
		if (this.disabled) {
			e.preventDefault();
			return;
		}
		var k = e.getKey();
		switch (k) {
			case e.UP:
			case e.RIGHT:
				e.stopEvent();
				if (e.ctrlKey) {
					this.setValue(this.maxValue);
				} else {
					this.setValue(this.value + this.keyIncrement);
				}
				break;
			case e.DOWN:
			case e.LEFT:
				e.stopEvent();
				if (e.ctrlKey) {
					this.setValue(this.minValue);
				} else {
					this.setValue(this.value - this.keyIncrement);
				}
				break;
			default:
				e.preventDefault();
		}
	},

	doSnap : function(value) {
		if (!this.increment || this.increment == 1 || !value) {
			return value;
		}
		var newValue = value, inc = this.increment;
		var m = value % inc;
		if (m > 0) {
			if (m > (inc / 2)) {
				newValue = value + (inc - m);
			} else {
				newValue = value - m;
			}
		}
		return newValue.constrain(this.minValue, this.maxValue);
	},

	afterRender : function() {
		Tera.Slider.superclass.afterRender.apply(this, arguments);
		if (this.value !== undefined) {
			var v = this.normalizeValue(this.value);
			if (v !== this.value) {
				delete this.value;
				this.setValue(v, false);
			} else {
				this.moveThumb(this.translateValue(v), false);
			}
		}
	},

	getRatio : function() {
		var w = this.innerEl.getWidth();
		var v = this.maxValue - this.minValue;
		return w / v;
	},

	normalizeValue : function(v) {
		if (typeof v != 'number') {
			v = parseInt(v);
		}
		v = Math.round(v);
		v = this.doSnap(v);
		v = v.constrain(this.minValue, this.maxValue);
		return v;
	},

	setValue : function(v, animate) {
		v = this.normalizeValue(v);
		if (v !== this.value && this.fireEvent('beforechange', this, v, this.value) !== false) {
			this.value = v;
			this.moveThumb(this.translateValue(v), animate !== false);
			this.fireEvent('change', this, v);
		}
	},

	translateValue : function(v) {
		return (v * this.getRatio()) - this.halfThumb;
	},

	moveThumb : function(v, animate) {
		if (!animate || this.animate === false) {
			this.thumb.setLeft(v);
		} else {
			this.thumb.shift( {
				left :v,
				stopFx :true,
				duration :.35
			});
		}
	},

	focus : function() {
		this.focusEl.focus(10);
	},

	onBeforeDragStart : function(e) {
		return !this.disabled;
	},

	onDragStart : function(e) {
		this.thumb.addClass('x-slider-thumb-drag');
		this.fireEvent('dragstart', this, e);
	},

	onDrag : function(e) {
		var pos = this.innerEl.translatePoints(this.tracker.getXY());
		this.setValue(Math.round(pos.left / this.getRatio()), false);
		this.fireEvent('drag', this, e);
	},

	onDragEnd : function(e) {
		this.thumb.removeClass('x-slider-thumb-drag');
		this.fireEvent('dragend', this, e);
	},

	onResize : function(w, h) {
		this.innerEl.setWidth(w - (this.el.getPadding('l') + this.endEl.getPadding('r')));
	},

	getValue : function() {
		return this.value;
	}
});
Tera.reg('slider', Tera.Slider);

Tera.Slider.Vertical = {
	onResize : function(w, h) {
		this.innerEl.setHeight(h - (this.el.getPadding('t') + this.endEl.getPadding('b')));
	},

	getRatio : function() {
		var h = this.innerEl.getHeight();
		var v = this.maxValue - this.minValue;
		return h / v;
	},

	moveThumb : function(v, animate) {
		if (!animate || this.animate === false) {
			this.thumb.setBottom(v);
		} else {
			this.thumb.shift( {
				bottom :v,
				stopFx :true,
				duration :.35
			});
		}
	},

	onDrag : function(e) {
		var pos = this.innerEl.translatePoints(this.tracker.getXY());
		var bottom = this.innerEl.getHeight() - pos.top;
		this.setValue(Math.round(bottom / this.getRatio()), false);
		this.fireEvent('drag', this, e);
	},

	onClickChange : function(local) {
		if (local.left > this.clickRange[0] && local.left < this.clickRange[1]) {
			var bottom = this.innerEl.getHeight() - local.top;
			this.setValue(Math.round(bottom / this.getRatio()));
		}
	}
};

Tera.StatusBar = Tera.extend(Tera.Toolbar, {

	cls :'x-statusbar',

	busyIconCls :'x-status-busy',

	busyText :'Loading...',

	autoClear :5000,

	activeThreadId :0,

	initComponent : function() {
		if (this.statusAlign == 'right') {
			this.cls += ' x-status-right';
		}
		Tera.StatusBar.superclass.initComponent.call(this);
	},

	afterRender : function() {
		Tera.StatusBar.superclass.afterRender.call(this);

		var right = this.statusAlign == 'right', td = Tera.get(this.nextBlock());

		if (right) {
			this.tr.appendChild(td.dom);
		} else {
			td.insertBefore(this.tr.firstChild);
		}

		this.statusEl = td.createChild( {
			cls :'x-status-text ' + (this.iconCls || this.defaultIconCls || ''),
			html :this.text || this.defaultText || ''
		});
		this.statusEl.unselectable();

		this.spacerEl = td.insertSibling( {
			tag :'td',
			style :'width:100%',
			cn : [ {
				cls :'ytb-spacer'
			} ]
		}, right ? 'before' : 'after');
	},

	setStatus : function(o) {
		o = o || {};

		if (typeof o == 'string') {
			o = {
				text :o
			};
		}
		if (o.text !== undefined) {
			this.setText(o.text);
		}
		if (o.iconCls !== undefined) {
			this.setIcon(o.iconCls);
		}

		if (o.clear) {
			var c = o.clear, wait = this.autoClear, defaults = {
				useDefaults :true,
				anim :true
			};

			if (typeof c == 'object') {
				c = Tera.applyIf(c, defaults);
				if (c.wait) {
					wait = c.wait;
				}
			} else if (typeof c == 'number') {
				wait = c;
				c = defaults;
			} else if (typeof c == 'boolean') {
				c = defaults;
			}

			c.threadId = this.activeThreadId;
			this.clearStatus.defer(wait, this, [ c ]);
		}
		return this;
	},

	clearStatus : function(o) {
		o = o || {};

		if (o.threadId && o.threadId !== this.activeThreadId) {
			return this;
		}

		var text = o.useDefaults ? this.defaultText : '', iconCls = o.useDefaults ? this.defaultIconCls : '';

		if (o.anim) {
			this.statusEl.fadeOut( {
				remove :false,
				useDisplay :true,
				scope :this,
				callback : function() {
					this.setStatus( {
						text :text,
						iconCls :iconCls
					});
					this.statusEl.show();
				}
			});
		} else {
			this.statusEl.hide();
			this.setStatus( {
				text :text,
				iconCls :iconCls
			});
			this.statusEl.show();
		}
		return this;
	},

	setText : function(text) {
		this.activeThreadId++;
		this.text = text || '';
		if (this.rendered) {
			this.statusEl.update(this.text);
		}
		return this;
	},

	getText : function() {
		return this.text;
	},

	setIcon : function(cls) {
		this.activeThreadId++;
		cls = cls || '';

		if (this.rendered) {
			if (this.currIconCls) {
				this.statusEl.removeClass(this.currIconCls);
				this.currIconCls = null;
			}
			if (cls.length > 0) {
				this.statusEl.addClass(cls);
				this.currIconCls = cls;
			}
		} else {
			this.currIconCls = cls;
		}
		return this;
	},

	showBusy : function(o) {
		if (typeof o == 'string') {
			o = {
				text :o
			};
		}
		o = Tera.applyIf(o || {}, {
			text :this.busyText,
			iconCls :this.busyIconCls
		});
		return this.setStatus(o);
	}
});
Tera.reg('statusbar', Tera.StatusBar);

Tera.debug = {};

( function() {

	var cp;

	function createConsole() {

		var scriptPanel = new Tera.debug.ScriptsPanel();
		var logView = new Tera.debug.LogPanel();
		var tree = new Tera.debug.DomTree();

		var tabs = new Tera.TabPanel( {
			activeTab :0,
			border :false,
			tabPosition :'bottom',
			items : [ {
				title :'Debug Console',
				layout :'border',
				items : [ logView, scriptPanel ]
			}, {
				title :'DOM Inspector',
				layout :'border',
				items : [ tree ]
			} ]
		});

		cp = new Tera.Panel( {
			id :'x-debug-browser',
			title :'Console',
			collapsible :true,
			animCollapse :false,
			style :'position:absolute;left:0;bottom:0;',
			height :200,
			logView :logView,
			layout :'fit',

			tools : [ {
				id :'close',
				handler : function() {
					cp.destroy();
					cp = null;
					Tera.EventManager.removeResizeListener(handleResize);
				}
			} ],

			items :tabs
		});

		cp.render(document.body);

		cp.resizer = new Tera.Resizable(cp.el, {
			minHeight :50,
			handles :"n",
			pinned :true,
			transparent :true,
			resizeElement : function() {
				var box = this.proxy.getBox();
				this.proxy.hide();
				cp.setHeight(box.height);
				return box;
			}
		});

		function handleResize() {
			cp.setWidth(Tera.getBody().getViewSize().width);
		}
		Tera.EventManager.onWindowResize(handleResize);

		handleResize();
	}

	Tera.apply(Tera, {
		log : function() {
			if (!cp) {
				createConsole();
			}
			cp.logView.log.apply(cp.logView, arguments);
		},

		logf : function(format, arg1, arg2, etc) {
			Tera.log(String.format.apply(String, arguments));
		},

		dump : function(o) {
			if (typeof o == 'string' || typeof o == 'number' || typeof o == 'undefined' || Tera.isDate(o)) {
				Tera.log(o);
			} else if (!o) {
				Tera.log("null");
			} else if (typeof o != "object") {
				Tera.log('Unknown return type');
			} else if (Tera.isArray(o)) {
				Tera.log('[' + o.join(',') + ']');
			} else {
				var b = [ "{\n" ];
				for ( var key in o) {
					var to = typeof o[key];
					if (to != "function" && to != "object") {
						b.push(String.format("  {0}: {1},\n", key, o[key]));
					}
				}
				var s = b.join("");
				if (s.length > 3) {
					s = s.substr(0, s.length - 2);
				}
				Tera.log(s + "\n}");
			}
		},

		_timers : {},

		time : function(name) {
			name = name || "def";
			Tera._timers[name] = new Date().getTime();
		},

		timeEnd : function(name, printResults) {
			var t = new Date().getTime();
			name = name || "def";
			var v = String.format("{0} ms", t - Tera._timers[name]);
			Tera._timers[name] = new Date().getTime();
			if (printResults !== false) {
				Tera.log('Timer ' + (name == "def" ? v : name + ": " + v));
			}
			return v;
		}
	});

})();

Tera.debug.ScriptsPanel = Tera.extend(Tera.Panel, {
	id :'x-debug-scripts',
	region :'east',
	minWidth :200,
	split :true,
	width :350,
	border :false,
	layout :'anchor',
	style :'border-width:0 0 0 1px;',

	initComponent : function() {

		this.scriptField = new Tera.form.TextArea( {
			anchor :'100% -26',
			style :'border-width:0;'
		});

		this.trapBox = new Tera.form.Checkbox( {
			id :'console-trap',
			boxLabel :'Trap Errors',
			checked :true
		});

		this.toolbar = new Tera.Toolbar( [ {
			text :'Run',
			scope :this,
			handler :this.evalScript
		}, {
			text :'Clear',
			scope :this,
			handler :this.clear
		}, '->', this.trapBox, ' ', ' ' ]);

		this.items = [ this.toolbar, this.scriptField ];

		Tera.debug.ScriptsPanel.superclass.initComponent.call(this);
	},

	evalScript : function() {
		var s = this.scriptField.getValue();
		if (this.trapBox.getValue()) {
			try {
				var rt = eval(s);
				Tera.dump(rt === undefined ? '(no return)' : rt);
			} catch (e) {
				Tera.log(e.message || e.descript);
			}
		} else {
			var rt = eval(s);
			Tera.dump(rt === undefined ? '(no return)' : rt);
		}
	},

	clear : function() {
		this.scriptField.setValue('');
		this.scriptField.focus();
	}

});

Tera.debug.LogPanel = Tera.extend(Tera.Panel, {
	autoScroll :true,
	region :'center',
	border :false,
	style :'border-width:0 1px 0 0',

	log : function() {
		var markup = [ '<div style="padding:5px !important;border-bottom:1px solid #ccc;">',
				Tera.util.Format.htmlEncode(Array.prototype.join.call(arguments, ', ')).replace(/\n/g, '<br />').replace(/\s/g, '&#160;'), '</div>' ]
				.join('');

		this.body.insertHtml('beforeend', markup);
		this.body.scrollTo('top', 100000);
	},

	clear : function() {
		this.body.update('');
		this.body.dom.scrollTop = 0;
	}
});

Tera.debug.DomTree = Tera.extend(Tera.tree.TreePanel, {
	enableDD :false,
	lines :false,
	rootVisible :false,
	animate :false,
	hlColor :'ffff9c',
	autoScroll :true,
	region :'center',
	border :false,

	initComponent : function() {

		Tera.debug.DomTree.superclass.initComponent.call(this);

		var styles = false, hnode;
		var nonSpace = /^\s*$/;
		var html = Tera.util.Format.htmlEncode;
		var ellipsis = Tera.util.Format.ellipsis;
		var styleRe = /\s?([a-z\-]*)\:([^;]*)(?:[;\s\n\r]*)/gi;

		function findNode(n) {
			if (!n || n.nodeType != 1 || n == document.body || n == document) {
				return false;
			}
			var pn = [ n ], p = n;
			while ((p = p.parentNode) && p.nodeType == 1 && p.tagName.toUpperCase() != 'HTML') {
				pn.unshift(p);
			}
			var cn = hnode;
			for ( var i = 0, len = pn.length; i < len; i++) {
				cn.expand();
				cn = cn.findChild('htmlNode', pn[i]);
				if (!cn) {
					return false;
				}
			}
			cn.select();
			var a = cn.ui.anchor;
			treeEl.dom.scrollTop = Math.max(0, a.offsetTop - 10);
			cn.highlight();
			return true;
		}

		function nodeTitle(n) {
			var s = n.tagName;
			if (n.id) {
				s += '#' + n.id;
			} else if (n.className) {
				s += '.' + n.className;
			}
			return s;
		}

		function onNodeSelect(t, n, last) {
			return;
			if (last && last.unframe) {
				last.unframe();
			}
			var props = {};
			if (n && n.htmlNode) {
				if (frameEl.pressed) {
					n.frame();
				}
				if (inspecting) {
					return;
				}
				addStyle.enable();
				reload.setDisabled(n.leaf);
				var dom = n.htmlNode;
				stylePanel.setTitle(nodeTitle(dom));
				if (styles && !showAll.pressed) {
					var s = dom.style ? dom.style.cssText : '';
					if (s) {
						var m;
						while ((m = styleRe.exec(s)) != null) {
							props[m[1].toLowerCase()] = m[2];
						}
					}
				} else if (styles) {
					var cl = Tera.debug.cssList;
					var s = dom.style, fly = Tera.fly(dom);
					if (s) {
						for ( var i = 0, len = cl.length; i < len; i++) {
							var st = cl[i];
							var v = s[st] || fly.getStyle(st);
							if (v != undefined && v !== null && v !== '') {
								props[st] = v;
							}
						}
					}
				} else {
					for ( var a in dom) {
						var v = dom[a];
						if ((isNaN(a + 10)) && v != undefined && v !== null && v !== '' && !(Tera.isGecko && a[0] == a[0].toUpperCase())) {
							props[a] = v;
						}
					}
				}
			} else {
				if (inspecting) {
					return;
				}
				addStyle.disable();
				reload.disabled();
			}
			stylesGrid.setSource(props);
			stylesGrid.treeNode = n;
			stylesGrid.view.fitColumns();
		}

		this.loader = new Tera.tree.TreeLoader();
		this.loader.load = function(n, cb) {
			var isBody = n.htmlNode == document.body;
			var cn = n.htmlNode.childNodes;
			for ( var i = 0, c; c = cn[i]; i++) {
				if (isBody && c.id == 'x-debug-browser') {
					continue;
				}
				if (c.nodeType == 1) {
					n.appendChild(new Tera.debug.HtmlNode(c));
				} else if (c.nodeType == 3 && !nonSpace.test(c.nodeValue)) {
					n.appendChild(new Tera.tree.TreeNode( {
						text :'<em>' + ellipsis(html(String(c.nodeValue)), 35) + '</em>',
						cls :'x-tree-noicon'
					}));
				}
			}
			cb();
		};

		this.root = this.setRootNode(new Tera.tree.TreeNode('Tera'));

		hnode = this.root.appendChild(new Tera.debug.HtmlNode(document.getElementsByTagName('html')[0]));

	}
});

Tera.debug.HtmlNode = function() {
	var html = Tera.util.Format.htmlEncode;
	var ellipsis = Tera.util.Format.ellipsis;
	var nonSpace = /^\s*$/;

	var attrs = [ {
		n :'id',
		v :'id'
	}, {
		n :'className',
		v :'class'
	}, {
		n :'name',
		v :'name'
	}, {
		n :'type',
		v :'type'
	}, {
		n :'src',
		v :'src'
	}, {
		n :'href',
		v :'href'
	} ];

	function hasChild(n) {
		for ( var i = 0, c; c = n.childNodes[i]; i++) {
			if (c.nodeType == 1) {
				return true;
			}
		}
		return false;
	}

	function renderNode(n, leaf) {
		var tag = n.tagName.toLowerCase();
		var s = '&lt;' + tag;
		for ( var i = 0, len = attrs.length; i < len; i++) {
			var a = attrs[i];
			var v = n[a.n];
			if (v && !nonSpace.test(v)) {
				s += ' ' + a.v + '=&quot;<i>' + html(v) + '</i>&quot;';
			}
		}
		var style = n.style ? n.style.cssText : '';
		if (style) {
			s += ' style=&quot;<i>' + html(style.toLowerCase()) + '</i>&quot;';
		}
		if (leaf && n.childNodes.length > 0) {
			s += '&gt;<em>' + ellipsis(html(String(n.innerHTML)), 35) + '</em>&lt;/' + tag + '&gt;';
		} else if (leaf) {
			s += ' /&gt;';
		} else {
			s += '&gt;';
		}
		return s;
	}

	var HtmlNode = function(n) {
		var leaf = !hasChild(n);
		this.htmlNode = n;
		this.tagName = n.tagName.toLowerCase();
		var attr = {
			text :renderNode(n, leaf),
			leaf :leaf,
			cls :'x-tree-noicon'
		};
		HtmlNode.superclass.constructor.call(this, attr);
		this.attributes.htmlNode = n;
		if (!leaf) {
			this.on('expand', this.onExpand, this);
			this.on('collapse', this.onCollapse, this);
		}
	};

	Tera.extend(HtmlNode, Tera.tree.AsyncTreeNode, {
		cls :'x-tree-noicon',
		preventHScroll :true,
		refresh : function(highlight) {
			var leaf = !hasChild(this.htmlNode);
			this.setText(renderNode(this.htmlNode, leaf));
			if (highlight) {
				Tera.fly(this.ui.textNode).highlight();
			}
		},

		onExpand : function() {
			if (!this.closeNode && this.parentNode) {
				this.closeNode = this.parentNode.insertBefore(new Tera.tree.TreeNode( {
					text :'&lt;/' + this.tagName + '&gt;',
					cls :'x-tree-noicon'
				}), this.nextSibling);
			} else if (this.closeNode) {
				this.closeNode.ui.show();
			}
		},

		onCollapse : function() {
			if (this.closeNode) {
				this.closeNode.ui.hide();
			}
		},

		render : function(bulkRender) {
			HtmlNode.superclass.render.call(this, bulkRender);
		},

		highlightNode : function() {
		},

		highlight : function() {
		},

		frame : function() {
			this.htmlNode.style.border = '1px solid #0000ff';
		},

		unframe : function() {
			this.htmlNode.style.border = '';
		}
	});

	return HtmlNode;
}();

Tera.form.FilterPanel = Tera.extend(Tera.form.FormPanel, {

	filterConfig : null,
	isFilter : false,
	filtroActivo : null,
	pageSize : null,
	store : null,
	reloadOnClearForm: true,
	runValidation: false,
	validationFunction: null,
	
	initComponent : function(params) {
		Tera.form.FilterPanel.superclass.initComponent.call(this);
		
		if (this.runValidation) {
			var fp = this;

			var defaultValidation = function() {
				if (fp.isFilterValid()) {
					Tera.getCmp('btnFiltrar').enable();
				} else {
					Tera.getCmp('btnFiltrar').disable();
				}
			};
			
			if (this.validationFunction != null && Tera.isFunction(this.validationFunction)) {
				defaultValidation = this.validationFunction;
			}

			var task = {
				run: defaultValidation,
			    interval: 500
			};
			Tera.TaskMgr.start(task);
		}
	},

	setFiltering : function(bFiltering) {
		this.isFilter = bFiltering;
	},
	
	isFilterEmpty : function() {
		for (i = 0; i < this.mappedFields.length; i++) {
			var item = Tera.getCmp(this.mappedFields[i]);
			if (!Tera.isEmpty(item.getValue())) {
				 return false;	 
			}
		}
		return true;
	},
	
	isFilterValid : function() {
		var fieldsNoOpcionales = new Array();
		for (i = 0; i < this.mappedFields.length; i++) {
			var item = Tera.getCmp(this.mappedFields[i]);
			if (!item.isOptional()) {
				fieldsNoOpcionales.push(item);
			}
		}
		
		var boolArr = new Array();
		for (j = 0; j < fieldsNoOpcionales.length; j++) {
			boolArr.push(Tera.isEmpty(fieldsNoOpcionales[j].getValue()));
		}
		if (boolArr.indexOf(false) == -1) {
			return false;
		}
		
		for (x = 0; x < fieldsNoOpcionales.length; x++) {
			var item = fieldsNoOpcionales[x];
					
			if (item.el.hasClass(item.invalidClass)){
				return false;
			}
			
			if (!item.el.hasClass(item.invalidClass) && 
				!Tera.isEmpty(item.getValue()) && 
				(item.getXType()=='lupafield' && !item.isPrepared())) {
				return false;
			}
		}
		return true;
	},
	
	isFilterPrepared : function() {
		for (i = 0; i < this.mappedFields.length; i++) {
			var item = Tera.getCmp(this.mappedFields[i]);
			if (Tera.isEmpty(item.getValue())) {
				continue;				
			}
			
			if (item.getXType()=='lupafield' && !item.isPrepared()) {
				return false;
			}
		}
		return true;
	},
	 
	getFieldsAsParamObject : function() {
		var o = "{";
		var strItem = "";
		for (i = 0; i < this.mappedFields.length; i++) {
			var item = Tera.getCmp(this.mappedFields[i]);
			var value = item.getValue();

			if (item.getXType() === 'datefield' || Tera.isDate(item)) {
				value = Tera.util.Format.date(value, 'd/m/Y');
			}
			
			if (Tera.value(item.getValue(), false, false)) {
				strItem += "," + item.name + ": \'" + value + "\'";
			}
		}
		strItem = strItem.substring(1, strItem.length);
		o += strItem + "}";
		return Tera.decode(o);
	},
	
	getForm : function() {
		this.filtroActivo = this.getFieldsAsParamObject();
		return Tera.form.FilterPanel.superclass.getForm.call(this);
	},
	
	submit : function(o) {
		var ps = this.pageSize;
		
		if (!this.isFilterPrepared()) {
			return false;
		}
		
		if (this.isFilterEmpty()) {
			this.clearForm();
			return false;
		}
		
		if (!this.isFilterValid()) {
			return false;
		}
				
		var form = this.getForm();
		
		if (!Tera.isEmpty(form)) {
			var params = { limit: ps };
			var form = this.getForm() 
			form.baseParams = params;
			form.submit(o);
		}
		//return Tera.form.FilterPanel.superclass.submit.call(this, o);
	},

	clearForm : function() {
		var ps = this.pageSize;
		for (i = 0; i < this.mappedFields.length; i++) {
			this.clearField(this.mappedFields[i]);
		}
		if (!Tera.isEmpty(this.store)) {
			if (this.reloadOnClearForm) {
				this.store.removeAll();
			// Modificaicon Julian Manfredi	
			//	this.store.load({params: {start:0, limit:ps}});	
			}
		}
	},
	clearField : function(pId) {
		Tera.getCmp(pId).setValue('');
	},
	
	setPageSize : function(pPageSize) {
		this.pageSize = pPageSize;
	},
	
	setStore : function(pStore) {
		this.store = pStore;
	}
});
Tera.reg('filterpanel', Tera.form.FilterPanel);


Tera.override(Tera.PagingToolbar, {
	gsm : null,
	filterPanel: null,
	exportUrl: null,
	setGSM : function(pgsm) {
		this.gsm = pgsm;
	},
	
	doLoad : function(start) {
		var scope = this;
		var fp = this.filterPanel;
		var o = {}, pn = this.paramNames;
		
		o[pn.start] = start;
		o[pn.limit] = this.pageSize;

		if (!Tera.isEmpty(fp)) {
			if (!Tera.isEmpty(fp.filtroActivo)) {
				var serFA = Tera.encode(fp.filtroActivo);
				var serFP = Tera.encode(fp.getFieldsAsParamObject()); 
				
				if (serFA != serFP) {
					Tera.Msg.show({
						modal : true,
						title : 'Filtros sin aplicar',
						msg : 'Existen filtros que a�n no han sido aplicados.<br>Desea aplicarlos a la consulta?',
						buttons : { yes : 'Aplicar', no : 'No aplicar', cancel : 'Cancelar' },
						icon : Tera.Msg.QUESTION,
						draggable : false,
						resizable : false,
						fn : function(btn, text, opts) {
							if (btn == 'yes') {
								o[pn.start] = 0;
								Tera.apply(o, fp.getFieldsAsParamObject());
								scope.store.load( { params : o });
								return;
								
							} else if (btn == 'no') {	
								Tera.apply(o, fp.filtroActivo);
								scope.store.load( { params : o });
								return;
								
							} else if (btn == 'cancel') {
								return false;
							} 
						}
					}); 
				} else {
					Tera.apply(o, fp.filtroActivo);
					scope.store.load( { params : o });
					return;
				}
			} else {
				scope.store.load( { params : o });
				return;
			}
		}
		scope.store.load( { params : o });
	},
	
	onClick : function(which) {
		var store = this.store;
		var fp = this.filterPanel;

		if (!Tera.isEmpty(fp)) {
			if (!Tera.isEmpty(fp.filtroActivo)) {
				this.store.proxy.conn.url = fp.filterConfig.urls.pagingToolbarFilterUrl;
			} else {
				this.store.proxy.conn.url = fp.filterConfig.urls.pagingToolbarUrl;
			}
		}

		switch (which) {
			case "first":
				this.doLoad(0);
				break;
			case "prev":
				this.doLoad(Math.max(0, this.cursor - this.pageSize));
				break;
			case "next":
				this.doLoad(this.cursor + this.pageSize);
				break;
			case "last":
				var total = store.getTotalCount();
				var extra = total % this.pageSize;
				var lastStart = extra ? (total - extra) : total - this.pageSize;
				this.doLoad(lastStart);
				break;
			case "refresh":
				this.doLoad(this.cursor);
				break;
			case "exportFile":
				this.downloadFile();
				break;
			case "copyClipboard":
				this.copyClipboard();
				break;
		}
	},
	downloadFile : function() {
		var fp = this.filterPanel;
		var o = {}
		var pn = this.paramNames;
		var extraParams = null;
		var url = null;
		
		o[pn.start] = 0;
		o["gridName"] = this.getExportGridName();
		o["gridColum"] = this.getExportGridColum();

		extraParams = Tera.urlEncode(o);
		
		if (!Tera.isEmpty(fp)) {
			var fpParams = fp.getFieldsAsParamObject();
			Tera.apply(o, fpParams);
			
			extraParams = Tera.urlEncode(o);
			url = fp.filterConfig.urls.exportToolbarUrl + "&" + extraParams;
			
		} else {
			url = this.exportUrl + "&" + extraParams;;
		}

		var id = "downloadIframe";
		var frame = document.createElement('iframe');
		frame.id = id;
		frame.name = id;
		frame.className = 'x-hidden';

		// if(Tera.isIE) {
			// frame.src = Tera.SSL_SECURE_URL;
		// }
		document.body.appendChild(frame);
	
		if (Tera.isIE) {
			document.frames[id].name = id;
		}
		frame.src = url;
	
		var callback = function() {
			Tera.EventManager.removeListener(frame, 'load', callback, this);
			setTimeout( function() {
				document.body.removeChild(frame);
			}, 110);
		};
	
		Tera.EventManager.on(frame, 'load', callback, this);
	},
	copyClipboard : function() {
		if (!Tera.isIE) {
			return;
		}
		
		if (!this.gsm.hasSelection()) {
			Tera.MessageBox.alert('Alerta', 'Debe seleccionar un registro');
			return;
		}
		
		var records = this.gsm.getSelections();
		var clipBoardObject = [];
		var value = '';
	    for(var i = 0; i < records.length; i++){
	        var currRec = records[i];
	        clipBoardObject[clipBoardObject.length] = currRec.copy();
	        for(var x = 0; x < currRec.fields.keys.length; x++){
	        	var key = currRec.fields.keys[x];
	        	var cellValue = currRec.get(key);
	        	value += Tera.isEmpty(cellValue) ? ' ' : cellValue; 
	        	value += '\t';
	        }
	        value += '\n';
	    }        
	    window.clipboardData.setData('Text', value);
	}
});

Tera.override(Tera.Component, {
	findParentBy: function(fn) {
		for (var p = this.ownerCt; (p != null) && !fn(p); p = p.ownerCt);
		return p;
	},

	findParentByType: function(xtype) {
		return typeof xtype == 'function' ?
			this.findParentBy(function(p){
				return p.constructor === xtype;
			}) :
			this.findParentBy(function(p){
				return p.constructor.xtype === xtype;
			});
		}
	}
);

// Modificado - rosario.argentina (04.02.2009)
Tera.override(Tera.data.Record, {
	getJson : function() {
		var f = { date: 'd/m/Y H:i:s'};
		return Tera.util.JSON.encode(this.data, f);
	}
});

Tera.override(Tera.data.Store, {
	getModifiedDataAsJson : function() {
		var data = '';
		Tera.each(this.modified, function(record) {
			data += ",";
			data += record.getJson();
		});
		data = '[' + data.substring(1, data.length) + ']';
		return data;
	}
});

Tera.override(Tera.data.Store, {
	getGridDataAsJson : function() {
		var data = '';
		Tera.each(this.data.items, function(record) {
			data += ",";
			data += record.getJson();
		});
		data = '[' + data.substring(1, data.length) + ']';
		return data;
	}
});

Tera.override(Tera.data.Store, {
	listeners : {
		"beforeLoad": function(store, options) {
			if (store.url) {
				if (!store.url.match("lupas.do")) {
					Tera.Msg.wait("Cargando datos", "Espere por favor...");	
				}
			}
		}, 
		"load": function(store, records, options ) {
			Tera.Msg.hide();
		},
		"loadexception" : function() {
			Tera.MessageBox.alert('Error al obtener datos', 'Ocurrio un error durante la carga de datos. Intente nuevamente o contacte al administrador.');
		}
	}
});


/* Paging toolbar enhancements:
Tera.override(Tera.data.Store, {
   
    removeAll : function(silent){
        var items = [];
        this.each(function(rec){
            items.push(rec);
        });
       // this.clearData();
        this.totalLength = 0;
        if(this.snapshot){
            this.snapshot.clear();
        }
        if(this.pruneModifiedRecords){
            this.modified = [];
        }
        if (silent !== true) {  // <-- prevents write-actions when we just want to clear a store.
            this.fireEvent('clear', this, items);
        }
    }
});

Tera.override(Tera.PagingToolbar, {
    bindStore : function(store, initial){
        var doLoad;
        if(!initial && this.store){
            if(store !== this.store && this.store.autoDestroy){
                this.store.destroy();
            }else{
                this.store.un('beforeload', this.beforeLoad, this);
                this.store.un('load', this.onLoad, this);
                this.store.un('exception', this.onLoadError, this);
                this.store.un('datachanged', this.onChange, this);
                this.store.un('add', this.onChange, this);
                this.store.un('remove', this.onChange, this);
                this.store.un('clear', this.onClear, this);
            }
            if(!store){
                this.store = null;
            }
        }
        if(store){
            store = Tera.StoreMgr.lookup(store);
            store.on({
                scope: this,
                beforeload: this.beforeLoad,
                load: this.onLoad,
                exception: this.onLoadError,
                datachanged: this.onChange,
                add: this.onChange,
                remove: this.onChange,
                clear: this.onClear
            });
            doLoad = true;
        }
        this.store = store;
        if(doLoad){
            this.onLoad(store, null, {});
        }
    },

    onChange : function() {
        if(this.rendered){
            var d = this.getPageData(), ap = d.activePage, ps = d.pages;
            this.afterTextItem.setText(String.format(this.afterPageText, d.pages));
            this.inputItem.setValue(ap);
            this.first.setDisabled(ap == 1);
            this.prev.setDisabled(ap == 1);
            this.next.setDisabled(ap == ps);
            this.last.setDisabled(ap == ps);
            this.refresh.enable();
            this.updateInfo();
        }
        this.fireEvent('change', this, d);
    },
    onClear : function(){
        this.cursor = 0;
        this.onChange();
    }

});  */
// fin modicia










Date.compareDates = function(firstDate, secondDate, format, clearTime) {
	var d1, d2;
	var usedFormat;
	if (Tera.isEmpty(format)) {
		usedFormat = 'd/m/Y';
	} else {
		usedFormat = format;
	}
	if (Tera.type(firstDate)=='string') {
		d1 = Date.parseDate(firstDate, usedFormat);
	} else {
		d1 = firstDate;
	}
	if (Tera.type(secondDate)=='string') {
		d2 = Date.parseDate(secondDate, usedFormat);
	} else {
		d2 = secondDate;
	}
	
	if (!Tera.isEmpty(clearTime) && clearTime == true) {
		d1 = d1.clearTime();
		d2 = d2.clearTime();
	}
	if (d1>d2) {
		return 1;
	} else if(d1<d2) {
		return -1;
	} else {
		return 0;
	}
}

// lupa inicio

Tera.form.LupaField = Tera.extend(Tera.form.TwinTriggerField, {

    validationEvent : false,
    validateOnBlur : false,
    
    trigger1Class : 'x-form-clear-trigger',
    trigger2Class : 'x-form-search-trigger',
    triggerOnChange: true,
    triggerOnEnter: true,
    fireSearchAt: 2,
    disableOnRequest : false,
    hideTrigger1 : true,
    showClearButton : true,
    showSearchButton : true,
    showLabel : true,
    limitDescription : 0,
    onGrid: null,
    columnToEdit: null,
    columnToInput : null,
    columnToLabel : null,
    optional: false,
    executePostServer: true,  

	descriptionObject: null,
	descriptionValue: '',
	defaultDescriptionValue: '[ Introduzca C&#243;digo ]',
	defaultNotFoundValue: '[ C&#243;digo no encontrado ]',
	defaultSearchValue: '[ Buscando ... ]',
    searchWindowTitle: '[ Seleccione un C&#243;digo ]',
    windowWidth: null,
    windowHeight: null,
    url : '',
    descriptionRetrival : '',
	entityName: '',
	store: null,
	storeData: [],
	cm: null,
	params: null,
	
	prepared : false,
	visualize : false,
	
	initialValue: null,

	windowContainer: null,
	grid: null,
	sm: null,
	
	acceptButton: null,
	cancelButton: null,
		
    initComponent : function(){
        Tera.form.LupaField.superclass.initComponent.call(this);

        this.addEvents('assigned');
        this.addEvents('change');
        this.addEvents('clearClick');
        this.addEvents('searchClick');
        this.addEvents('descriptionSuccess');
        this.addEvents('descriptionFailure');
        
/*        this.acceptButton = new Tera.Button({ text: 'Aceptar', id: 'btnLupaAceptar',minWidth: 80, disabled: true });
        this.cancelButton = new Tera.Button({ text: 'Cancelar', id: 'btnLupaCancelar', minWidth: 80});*/
    },
    
    initEvents : function() {
    	Tera.form.LupaField.superclass.initEvents.call(this);
		if (this.triggerOnChange) {
			this.on('change', this.onChange, this);
		}
		
		
		
		// Manfredi se saca
		if (this.triggerOnEnter) {	
			this.on('specialkey', function(f, e){
				
				 
	            if(e.getKey() == e.ENTER){
	            	if (!Tera.isEmpty(this.onGrid)) {
	            		return true;
	            	}
	                this.onTriggerChange(this.getValue());
	            }
	        }, this);
        }
    },
    
    onRender : function(container, position){
    	Tera.form.LupaField.superclass.onRender.call(this, container, position);

		// Show/Hide the clear button.
		(this.showClearButton ? this.triggers[0].show() : this.triggers[0].hide());
		(this.showSearchButton ? this.triggers[1].show() : this.triggers[1].hide());
		if (this.wrap == undefined) {
			this.wrap = this.el.wrap( {
				cls :"x-form-field-wrap"
			});
		}
		var wrap = this.wrap.dom;
		wrap.style.cssText = '';

		if (this.showLabel) {
			//	this.defaultDescriptionValue = '';
			//	this.defaultSearchValue = '';
			//}
			var dv = this.defaultDescriptionValue;
			var dh = Tera.DomHelper;
		
			this.descriptionObject = { tag : 'span', id: this.id + '-descriptionValueId', style: 'padding-right: 5px; padding-left: 10px; color:#aaa;', html: dv };
			dh.append(this.wrap, this.descriptionObject);
		}
		
		var width = this.width;
		
		if (Tera.isEmpty(width)) {
    		this.width = 100;
		} else {
    		this.width = this.width + 20;
		}
		
		if (!Tera.isEmpty(this.value)) {
			this.initialValue = this.value;
			this.onTriggerChange.call(this, this.value, '');
		} else {
			if (!this.showClearButton && !this.showSearchButton) {
				this.setDescriptionValue('');
			}
		}
    },

    onTrigger1Click : function(){
    	if (this.disabled ) { 
            return;
        }
    	
    	if (this.visualize) { 
            return;
        }
        
    	var v = this.getRawValue();
        if(v.length < 1){
            return;
        } else {
            this.setValue('');
           	this.setDescriptionValue(this.defaultDescriptionValue);
        }
        
        this.reset();
        this.fireEvent('clearClick', this);
    },

    onTrigger2Click : function(){
        if (this.disabled) { 
            return;
        }
        
    	if (this.visualize) { 
            return;
        }
        
    	this.fireEvent('searchClick', this);
    	
    	var objWin = this.getSearchWindow(this);
    	this.showWindow(objWin.getWindow());
    	objWin.getContainer().load();
    },

    onTriggerChange : function(newValue, oldValue) {
    	var scope = this;
        var oParams = { entityName: this.entityName };
        
        if (!Tera.isEmpty(newValue)) {
        	newValue = newValue.toString().trim();
        } 
        if (!Tera.isEmpty(oldValue)) {
        	oldValue = oldValue.toString().trim();
        }
        
        if (!Tera.isEmpty(this.params)) {
        	if ("function"==Tera.type(this.params)) {
        		Tera.apply(oParams, this.params.call(this));	
        	} else if ("object"==Tera.type(this.params)) {
        		Tera.apply(oParams, this.params);
        	}
        } else {
        	Tera.apply(oParams, {codigoFilter: this.getValue()});
        }

    	this.setDescriptionValue(this.defaultSearchValue);
    	
		if (!Tera.isEmpty(this.storeData) && this.storeData.length > 0) {
			
			var o = {};
			var data = [o];
			var responseText = { 'responseText' : null };
			
			var store = this.getStore();
			var value = this.getValue().trim();
    		var idx = store.find('codigo', value);
    		
    		if (idx == -1) {
    			o['codigo'] = value;
    			o['descripcion'] = '\[ Registro No Encontrado \]';

    			responseText['responseText'] = Tera.encode({'data' : data});

    			if (Tera.isEmpty(this.initialValue)) {
    				scope.onDescriptionFailure.call(scope, value, responseText);
    				scope.assign('');
    			} else {
    				
    				responseText['responseText'] = Tera.encode({'data' : scope.defaultNotFoundValue});
    				
    				scope.onDescriptionFailure.call(scope, value, responseText);
    				scope.assign(this.initialValue);
    				
    				this.initialValue = null;
    			}

    		} else {
    			var record = store.getAt(idx);
    			var codigo = record.get("codigo");
        		var descripcion = record.get("descripcion");

    			o['codigo'] = codigo;
    			o['descripcion'] = descripcion;

    			responseText['responseText'] = Tera.encode({'data' : data});
    			scope.onDescriptionSuccess.call(scope, codigo, responseText);
    		}
    		if (!scope.disabled) {
    			scope.enable();
				scope.focus();
				scope.validate();
    		}
    		
    		return;
		}
    	
		if (this.executePostServer){
			if (!Tera.isEmpty(newValue)){
				Tera.Ajax.request({
					url: scope.descriptionRetrival,
					params: oParams,
					callback: function(options, success, response) {
	    				var data = Tera.decode(response.responseText);

	    				if (data.success) {
	    					scope.onDescriptionSuccess.call(scope, newValue, response);
	    	    			scope.prepared = true;

	    				} else {
	    					scope.onDescriptionFailure.call(scope, newValue, response);
	    					scope.prepared = false;
	    					if (!scope.showSearchButton) {
	    						scope.assign(newValue);
	    					}
	        				this.initialValue = null;
	    					scope.focus();
	    					scope.markInvalid();
	    				}
	    			}
				});

				if (!this.disabled) {
					this.enable();
				}
			} 			
		}
	},
    
    onEnable : function(component) {
    	Tera.form.LupaField.superclass.onEnable.call(this);
    },

    onDisable : function(component) {
    	Tera.form.LupaField.superclass.onDisable.call(this);
    },

    onDescriptionSuccess: function(triggerValue, response) {
		var data = Tera.decode(response.responseText);
		if (data.data[0] == undefined){
			this.setDescriptionValue(this.defaultNotFoundValue);
		} else {
		this.setDescriptionValue(data.data[0].descripcion);
		}
		
		this.fireEvent('descriptionSuccess', this, response);
	},
	
	onDescriptionFailure: function(triggerValue, response) {
		var data = Tera.decode(response.responseText);
		this.setDescriptionValue(data.failure);
		
		this.focus();
		this.validate();
		
		this.fireEvent('descriptionFailure', this, response);
	},
	
    assign : function(lv,dv){
		if (!Tera.isEmpty(this.onGrid)) {
			var grid = Tera.getCmp(this.onGrid);
			var sm = grid.getSelectionModel();
			
			var rs = sm.getSelected();
			rs.beginEdit();
			rs.set(this.columnToEdit, lv);
			rs.endEdit();
		}
		
        this.setValue(lv, false);

        if (!Tera.isEmpty(dv)) {
        	this.setDescriptionValue(dv);
        	this.prepared = true;
        } else {
        	this.prepared = false;
        }
        
        this.fireEvent('assigned', lv, dv);
    },

    setDescriptionValue : function(dv) {
    	if (!this.showLabel) {
    		return;
    	}
    	
    	var id = this.id + '-descriptionValueId';
    	var el = Tera.get(id);
    	if (!Tera.isEmpty(el)) {
    		
    		if (!Tera.isEmpty(this.limitDescription) && this.limitDescription!=0) {
    			if (dv.length > this.limitDescription) {
	    			dv = dv.substring(0, this.limitDescription);
	    			dv = dv + "...";
    			}
    		}
    		el.dom.innerHTML = dv;
    	}
    },

 	// -
    getStore : function() {
    	var oParams = { entityName: this.entityName };

    	if (!Tera.isEmpty(this.params)) {
    		Tera.apply(oParams, this.params);
    	}
    	
    	if (!Tera.isEmpty(this.store)) {
        	var tmpStore = this.store;
        	tmpStore.baseParams = oParams;
        	return tmpStore;
        }
    	if (Tera.isEmpty(this.store) && Tera.isEmpty(this.url) && !Tera.isEmpty(this.storeData)) {
        	var data = this.storeData;
    		var tmpStore = new Tera.data.SimpleStore({
    			remoteSort: true,
    			fields: [ 'codigo', 'descripcion']
    		});
    		tmpStore.loadData(data);
    		return tmpStore; 
        }
    	if (Tera.isEmpty(this.store) && !Tera.isEmpty(this.url)) {
    		return new Tera.data.JsonStore({
    			remoteSort : true,
        		url: this.url,
        		baseParams: oParams,
        		totalProperty: 'totalCount',
        		root: 'data',
        		id: 'codigo',
    			fields: ['codigo', 'descripcion']
    		});
        }
    },

    getColumnModel : function(pTriggerField) {
        if (!Tera.isEmpty(pTriggerField.cm)) {
            return pTriggerField.cm;
        }
        
        var cm = null;
        if (Tera.isEmpty(this.sm)) {
        	cm = new Tera.grid.ColumnModel([new Tera.grid.RowNumberer(),
        	   { header: "Codigo", id: "codigo", dataIndex: 'codigo', width: 35, align: 'center' },
        	   { header: "Descripcion", id: "descripcion", dataIndex: 'descripcion', align: 'left'}
        	]);	
        } else {
        	cm = new Tera.grid.ColumnModel([this.sm, new Tera.grid.RowNumberer(),
        	    { header: "codigo", id: "codigo", dataIndex: 'codigo', width: 35, align: 'center' },
        	    { header: "Descripcion", id: "descripcion", dataIndex: 'descripcion', align: 'left'}
        	]);
        }
        
        cm.defaultSortable = true;
		
        return cm;
	},

	getGrid : function(pTriggerField, cfgObject) {
		var grid = new Tera.grid.GridPanel({
    		id: 'gridLupa',
    		name: 'gridLupa',
        	borderBody: false,
        	height: 244,
        	width: 554,
    		store: cfgObject.store,
    	    cm: cfgObject.cm,
    	    sm: cfgObject.sm,
        	loadMask: true,
        	stripeRows: true,
        	viewConfig: {
        		forceFit: true,
        		autoFill: true
        	},
        	bbar: new Tera.PagingToolbar({
        		filterPanel: cfgObject.filterPanel,
            	pageSize: 10,
            	store: cfgObject.store,
            	displayInfo: true,
            	displayMsg: 'Mostrando  {0} - {1} de {2}',
            	emptyMsg: "No existen registros a mostrar."
    		})
    	});
		
		return grid;
	},

	getAceptarCommandHandler : function(container, triggerField, wnd) {
		if (Tera.isEmpty(triggerField.columnToInput)) {
			triggerField.columnToInput = 'codigo'; 
		}
		if (Tera.isEmpty(triggerField.columnToLabel)) {
			triggerField.columnToLabel = 'descripcion';
		}
		
		var code = container.getGrid().getSelectionModel().getSelected().get(triggerField.columnToInput);
		var desc = container.getGrid().getSelectionModel().getSelected().get(triggerField.columnToLabel);

    	triggerField.assign(code, desc);
    	
    	triggerField.fireEvent('assigned', code);

    	wnd.close();
    	triggerField.focus();
	},
	
	getCancelarCommandHandler : function(container, triggerField, wnd) {
		wnd.close();
	},
	
	getWindowContainer : function(pTriggerField) {
		var triggerField = pTriggerField;
		var store = this.getStore();
		
		if (store.getCount()>0) {
			store.removeAll();
		}
        var cm = this.getColumnModel(triggerField);
		
		var recordsArray = new Array();
		var ColumnRecordTemplate = Tera.data.Record.create([
			{name:'nombreColumna'},
			{name:'valorColumna'}
		]);

		for (var idx = 1; idx < cm.getColumnCount(); idx++) {
			var header = cm.getColumnHeader(idx);			
			var id = cm.getColumnId(idx);
			var record = new ColumnRecordTemplate({
				nombreColumna: id,
				valorColumna: header
			});
			recordsArray[idx-1] = record;
		}
		
		
		var columnsStore = new Tera.data.SimpleStore({
			fields: ['nombreColumna', 'valorColumna']
	    });
		columnsStore.add(recordsArray);
		
		var columnsCombo = new Tera.form.ComboBox({
		 	width: 175,
	    	name: 'column',
	     	id: 'columnCombo',
			mode: 'local',
			typeAhead: true,
			triggerAction: 'all',
			store: columnsStore,
			editable: false,
			forceSelection: true,
			displayField: 'valorColumna',
			valueField: 'nombreColumna',
			fieldLabel: "Buscar por"
		});

		 columnsCombo.setValue(columnsStore.data.items[(columnsStore.data.items.length)-1].data.nombreColumna);
		
		 //columnsCombo.setValue('Descripcion');
		
		var rsm = new Tera.grid.RowSelectionModel({
			singleSelect: true,
		    listeners: {
		    	"rowselect" : function() { Tera.getCmp('btnLupaAceptar').enable(); },
		    	"rowdeselect" : function() { Tera.getCmp('btnLupaAceptar').disable(); }
		    }}
		);
    	
		var grid = null;
		        	        	  
        var filterConfig = {
			urls: {
    		    pagingToolbarFilterUrl: triggerField.url,
    			pagingToolbarUrl: triggerField.url,
    			exportToolbarUrl: triggerField.url
    		}
        };
        
        
        var doQuery = function(el, pEventObject) {
        	var param = el.getValue();
			var searchByParam = columnsCombo.getValue();
			var filterKey = "{ \"" + searchByParam + "Filter\" : \"" + param + "\"}";
			var dynamicQuery = Tera.util.JSON.decode(filterKey);
			
			var parameters = {
		        entityName: triggerField.entityName,
				start: 0,
				limit: 10
			}
			
			Tera.apply(parameters, dynamicQuery);
			
			if (triggerField.storeData && triggerField.storeData.length>0) {
				grid.getStore().loadData(triggerField.storeData);
			}
			
			if ((!Tera.isEmpty(param) && (!Tera.isEmpty(param.trim()) && param.trim().length > triggerField.fireSearchAt)) ||
				(!Tera.isEmpty(param) && (!Tera.isEmpty(param.trim()) && pEventObject.getKey() == 13))) {
				
				// Seleccion del filtro activo.
				filterPanel.getForm();
				
				if (triggerField.storeData && triggerField.storeData.length>0) {
					grid.getStore().filter('descripcion', param, true, false);
				} else {
  					grid.getStore().load({ params : parameters });
				}
			} else if(Tera.isEmpty(param) || Tera.isEmpty(param.trim())){
				if (triggerField.storeData && triggerField.storeData.length==0) {
					grid.getStore().load({ params : { 
						entityName: triggerField.entityName,
						start: 0,
						limit: 10
					}});
				}
			}
        };
               
        var filterPanel = new Tera.form.FilterPanel({
    		mappedFields: [ 'descripcionFilter'],
    		filterConfig: filterConfig,
    		border: false,
    		items: [{
        		layout: 'fit',
        		border: false,
        		items: [{
        			xtype:"fieldset",
            	    title:"Filtro ",
            	    autoHeight:true,
            	    items:[columnsCombo, {
                  		xtype: 'textfield',
                  		fieldLabel: 'Valor',
                  		width: 350,
                  		fieldClass: 'fix-margin',
                  		id: 'descripcionFilter',
                  		name: 'descripcionFilter',
                  		listeners: {
                  			'specialKey' : function(pField, pEventObject) {
                   				if (pEventObject.getKey()== 13) {
                  					doQuery(pField, pEventObject);	
                  				}
                   			},
        	        		'keyup' : function(pField, pEventObject) {
        	      				doQuery(pField, pEventObject);
        				  	}
                  		}
            	    }]
        		}]
          	}]
        });

        var cfgObject = {
        	store : store,
        	cm : cm,
        	sm : (!Tera.isEmpty(triggerField.sm) ? triggerField.sm : rsm),
        	filterPanel : filterPanel
        };
        	
        var grid = this.getGrid(triggerField, cfgObject);
        triggerField.grid = grid;

        var panel = new Tera.Panel({
        	bodyStyle: 'padding: 0px 5px 5px 5px',
        	border: false,
        	items : [{
        		layout: 'fit',
        		border: false,
        		items: [filterPanel]
        	  },{
        		  layout:'fit',
        		  border: false,
        		  items: grid
        	  }] 
        }); 
    	
    	return {
    		getGrid : function() {
    			return grid;
    		},
    		getPanel : function() {
    			return panel; 
    		},
    		setPanel : function(pPanel) {
    			this.panel = pPanel;
    		},
    		load : function() {
    			if (Tera.isEmpty(store.url) && !Tera.isEmpty(triggerField.storeData)) {
    	        	store.loadData(triggerField.storeData);
    	        } else {
    	        	store.load({ params: {start: 0, limit: 10}});
    	        }
    		},
    		getAceptarButton : function() {
    			return triggerField.acceptButton;    			
    		},
    		getCancelarButton : function() {
    			return triggerField.cancelButton;
    		}
    	};
	},
	
	showWindow : function(pWin) {
		pWin.show();
	},

	getSearchWindow : function(trigger) {
		var triggerField = trigger;
						
		Tera.override(Tera.form.Field, {
            fireKey : function(e) {
                if(((Tera.isIE && e.type == 'keydown') || e.type == 'keypress') && e.isSpecialKey()) {
                    this.fireEvent('specialkey', this, e);
                }
                else {
                    this.fireEvent(e.type, this, e);
                }
            },
            initEvents : function() {
                this.el.on("focus", this.onFocus,  this);
                this.el.on("blur", this.onBlur,  this);
                this.el.on("keydown", this.fireKey, this);
                this.el.on("keypress", this.fireKey, this);
                this.el.on("keyup", this.fireKey, this);
                this.originalValue = this.getValue();
            }
        });
		
		Tera.override(Tera.data.Store, {
			showLoadingMessage : true
        });
       
    	var titleWin = this.searchWindowTitle;
    	if (this.showLabel != null && this.showLabel) {
    		if (this.fieldLabel != null && this.fieldLabel != ''){
    			titleWin = 'Selecci&#243;n de ' + this.fieldLabel;
    		}
    	}

    	var container = triggerField.getWindowContainer(triggerField);
    	triggerField.windowContainer = container;
    	
    	var btnAceptar = new Tera.Button({
    		text: 'Aceptar', id: 'btnLupaAceptar', minWidth: 65});
    	    	
    	var btnCancelar = new Tera.Button({
    		text: 'Cancelar', id: 'btnLupaCancelar', minWidth: 65});
    	
    	var winlupa = new Tera.Window({
        	title: titleWin,
    		width: Tera.isEmpty(triggerField.windowWidth) ? 650 : triggerField.windowWidth,
    		closable: true, draggable: false, resizable: false, border: false, modal: true,
    		bodyStyle: 'padding-top: 10px',
    		layout: 'form',
    		items: [container.getPanel()],
    		buttons: [btnAceptar, btnCancelar],
    		buttonAlign: 'center'
    	});

    	btnCancelar.setHandler(this.getCancelarCommandHandler.createDelegate(this, [container, triggerField, winlupa], false));
    	btnAceptar.setHandler(this.getAceptarCommandHandler.createDelegate(this, [container, triggerField, winlupa], false));
    	
    	return {
    		getWindow : function() {
    			return winlupa;
    		}, 
    		getContainer : function() {
    			return container;
    		}
    	}
	},
	
	onChange : function(field, nv, ov){
    	if (Tera.isEmpty(nv)) {
    		if (this.allowBlank) {
    			this.reset();
    		}
    		this.setValue('');
			this.setDescriptionValue(this.defaultDescriptionValue);
						
			return false;
		}
    	
    	this.validate();
		this.onTriggerChange.call(this, nv, ov);
	},
	    	
	setValue : function(v, searchDesc) {
		Tera.form.LupaField.superclass.setValue.call(this, v);
		Tera.form.LupaField.superclass.setRawValue.call(this, v);
		if (Tera.isEmpty(v)) {
			this.prepared = false;

			this.setDescriptionValue(this.defaultDescriptionValue);

		} else {
			if (searchDesc!=null && searchDesc) {
				this.onTriggerChange.call(this, this.value, '');
			}
		}
	},
	
	setDisableOnRequest : function(pBoolean) {
		this.setDisabled(true);
	},

	getLabel: function() {
		var id = this.id + '-descriptionValueId';
    	var el = Tera.get(id);
    	if (!Tera.isEmpty(el)) {
    		return el.dom.innerHTML;
    	}
    	return '';
	},
	
	isPrepared : function() {
		if (!this.triggerOnChange || !this.triggerOnEnter) {
			return true;
		}
		return (this.prepared)
	},
	
	isOptional : function() {
		return this.optional;
	},
	setParams: function(pParams) {
		this.params = pParams;
	},
	isValid : function() {
		if (this.allowBlank) {
			return true;
		} else {
			if (Tera.isEmpty(this.getValue())) {
				return false;
			} else {
				if (this.prepared) {
					return true;
				} else {
					return false;
				} 
			}
		}
	}/*,
	
	validateValue : function(value) {
		return Tera.form.LupaField.superclass.validateValue.call(this, value);
		
		if (Tera.isEmpty(value) && !this.allowBlank) {
			return false;
		} else {
			return true;
		}
		//return (!this.el.hasClass('x-form-invalid'));
	}*/
	
});
Tera.reg('lupafield', Tera.form.LupaField);

Tera.form.LupaFieldMultiSelect = Tera.extend(Tera.form.LupaField, {

	records: new Array(),
	sm : null,
	
	initComponent : function(){
		Tera.form.LupaFieldMultiSelect.superclass.initComponent.call(this);
		
		this.sm = new Tera.grid.CheckboxSelectionModel({checkOnly: true, header: '<div class="x-grid3-hd-checker">&#160;</div>'});
	},
	
	initEvents : function() {
    	Tera.form.LupaField.superclass.initEvents.call(this);

		this.sm.on("rowselect", function(pSm, pRowIndex, pRecord) {
			this.records.push(pRecord.get('codigo'));
			Tera.getCmp('btnLupaAceptar').enable();
		}, this);
		
		this.sm.on("rowdeselect", function(pSm, pRowIndex, pRecord) {
			var idx = this.records.indexOf(pRecord.get('codigo'));
			if (idx != -1) {
				this.records.splice(idx, 1);
			}
			if (this.sm.getCount() <= 0) {
				Tera.getCmp('btnLupaAceptar').disable();
			}
		}, this);
    },
    
	onRender : function(container, position){
		Tera.form.LupaFieldMultiSelect.superclass.onRender.call(this, container, position);
	},
	
	getWindowContainer : function() {
		return Tera.form.LupaFieldMultiSelect.superclass.getWindowContainer.call(this, this);		
	},

	updateButtonState : function(pRecords) {
		if (pRecords.length > 0) {
			this.acceptButton.enable();
		} else {
			this.acceptButton.disable();
		}
	},
	
	showWindow : function(pWin) {
		Tera.form.LupaFieldMultiSelect.superclass.showWindow.call(this, pWin);

		this.grid.getSelectionModel().clearSelections();
		this.records.length = 0;
		
		this.grid.getStore().on('load', function(pStore, pRecords, pOptions){
			if (!Tera.isEmpty(this.getValue().trim())) {
				this.records = this.getArrayFromString(this.getValue());
			}
			
			if (!Tera.isEmpty(this.records) && this.records.length>0) {
				if (!Tera.isEmpty(this.grid)) {
					this.records.each(function(item) {
						var keys = this.grid.getStore().collect('codigo');
						var value = item.id
						
						if (Tera.isEmpty(value)) {
							value = item;
						}
						var idx = keys.indexOf(value);
						
						this.grid.getSelectionModel().suspendEvents(false);
						this.grid.getSelectionModel().selectRow(idx, this);
						this.grid.getSelectionModel().resumeEvents();
						
					}, this);
				}
			}
		}, this);
	},
	
	getAceptarCommandHandler : function(container, triggerField, wnd) {
		if (Tera.isEmpty(this.columnToInput)) {
			this.columnToInput = 'codigo'; 
		}
		if (Tera.isEmpty(this.columnToLabel)) {
			this.columnToLabel = 'descripcion';
		}
	
		var arr = this.records;
		var str = this.getStringFromArray(arr);
		
		this.setValue("");
		
		this.assign(str, '');
		this.fireEvent('assigned', str);

    	wnd.close();
    	this.focus();
	},
	
    getStringFromArray : function(array) {
    	if (!array) {
    		return "";
    	}
    	return array.join(",");	
    },
    
    getArrayFromString : function() {
    	var str = this.getValue();
    	
    	var tempArray = new Array();
    	var tempRecords = str.split(",");
    	
    	tempRecords.each(function(item){
    		if (item.trim() != "") {
    			tempArray.push(item.trim());
    		}
    	});
    	
    	return tempArray;
    },
    
    onChange : function(field, nv, ov){
    	Tera.form.LupaFieldMultiSelect.superclass.onChange.call(this, field, nv, ov);
    	if (Tera.isEmpty(nv)) {
    		this.records = new Array();
		}
	},
	
	onTrigger1Click : function(){
		Tera.form.LupaFieldMultiSelect.superclass.onTrigger1Click.call(this);
	},
	
	setValue : function(v, searchDesc) {
		Tera.form.LupaField.superclass.setValue.call(this, v);
		Tera.form.LupaField.superclass.setRawValue.call(this, v);

		if (Tera.isEmpty(v)) {
			this.prepared = false;
		} else {
			if (searchDesc!=null && searchDesc) {
				this.onTriggerChange.call(this, this.value, '');
			}
		}
	},
	
	handleMouseDown : function(g, rowIndex, e){
        if(e.button !== 0 || this.isLocked()){
            return;
        };
        var view = this.grid.getView();
        if(e.shiftKey && this.last !== false){
            var last = this.last;
            this.selectRange(last, rowIndex, e.ctrlKey);
            this.last = last;
            view.focusRow(rowIndex);
        }else{
            var isSelected = this.isSelected(rowIndex);
            if(isSelected){
                this.deselectRow(rowIndex);
            }else if(!isSelected){
                this.selectRow(rowIndex, ! this.singleSelect);
                view.focusRow(rowIndex);
            }
        }
    }
});
Tera.reg('lupafieldms', Tera.form.LupaFieldMultiSelect);

Tera.form.LupaFieldEfectividad = Tera.extend(Tera.form.LupaField, {
	addFechaEfectividadUrl: null,
	
	initComponent : function(){
		Tera.form.LupaFieldEfectividad.superclass.initComponent.call(this);
	},
	
	onRender : function(container, position){
		Tera.form.LupaFieldEfectividad.superclass.onRender.call(this, container, position);
	},

	getAltaFechaEfectividadPanel : function(pTriggerField) {
		var triggerField = pTriggerField;
		
		var altaBtn = new Tera.Button({text:"Agregar"});
		
		var datefield = new Tera.form.DateField({
			fieldLabel: "F. Efectividad",
            name: "fec_efectiviLupa",
            id: 'fec_efectiviLupa',
            format: 'd/m/Y'
		});
		
		var descEfectivi = new Tera.form.TextField({
            fieldLabel:"Descripcion",
            name:"des_efectiviLupa",
            id:'des_efectiviLupa',
            width: 200
        });
		
		var panel = {
			xtype:"fieldset",
		    title:"Nueva Fecha Efectividad",
		    defaults:{
		    	border:false
		    },
		    collapsible: true,
		    layout: 'fit',
		    items:[{
		    	layout:"column",
		    	defaults:{
		        	border:false
		        },
		        items:[{
		        	columnWidth: 0.35,
		            layout:"form",
		            items:[datefield]
		        },{
		        	columnWidth: 0.53,
		        	layout:"form",
		            items:[descEfectivi]
		        },{
		        	columnWidth: 0.12,
			        layout: 'form',
			        items:[altaBtn]
		        }]
		    }]
		};
		
		return {
			getDateField : function() {
				return datefield;
			},
			getDescEfectividad : function() {
				return descEfectivi;
			},
			getContainer : function() {
				return panel;
			},
			getAgregarButton : function() {
				return altaBtn;
			}
		};
	},

	getWindowContainer : function(pTriggerField) {
		var triggerField = pTriggerField;
		var container = Tera.form.LupaFieldEfectividad.superclass.getWindowContainer.call(this,triggerField);
		
		var objPanel = this.getAltaFechaEfectividadPanel(triggerField);
		
		var panel = container.getPanel();
		panel.add({
			layout: 'fit',
			border: false,
			style: 'margin-top: 10px',
			items: [objPanel.getContainer()]
		});
		container.setPanel(panel);
		
		objPanel.getAgregarButton().handler = function() {
			
        	Tera.Ajax.request({
                url: triggerField.addFechaEfectividadUrl,
                method: 'POST',
                params: {
                    'fec_efectivi': Tera.util.Format.date(objPanel.getDateField().getValue(), 'd/m/Y'),
                    'des_efectivi': objPanel.getDescEfectividad().getValue()
                },
                callback: function(options, success, response) {
                    var data = Tera.decode(response.responseText);
                    if (data.success) {
                    	objPanel.getDateField().setValue('');
                    	objPanel.getDescEfectividad().setValue('');
                
                    	var code = data.data.fec_efectivi;
                    	var desc = data.data.des_efectivi;
                    	
                    	triggerField.assign(code, desc);
                    	triggerField.fireEvent('assigned', code);
            	
            	    	triggerField.focus();
                    	
                    	container.getCancelarButton().handler();
                    	
                    } else {
                        var responseText = Tera.decode(response.responseText);
                        Tera.MessageBox.alert('Error en el Alta', responseText.failure);
                    }
                }
            });
    	};
		
		return container;
	}
});
Tera.reg('lupafieldefectividad', Tera.form.LupaFieldEfectividad);

Tera.ns('Tera.ux.form');
Tera.ux.form.XCheckbox = Tera.extend(Tera.form.Checkbox, {
	offCls:'xcheckbox-off',
	onCls:'xcheckbox-on',
	disabledClass:'xcheckbox-disabled',
	submitOffValue:'false',
	submitOnValue:'true',
	width: 0,
	checked:false,

	onRender:function(ct) {
        Tera.ux.form.XCheckbox.superclass.onRender.apply(this, arguments);

        // save tabIndex remove & re-create this.el
        var tabIndex = this.el.dom.tabIndex;
        var id = this.el.dom.id;
        this.el.remove();
        this.el = ct.createChild({tag:'input', type:'hidden', name:this.name, id:id});

        // update value of hidden field
        this.updateHidden();

        // adjust wrap class and create link with bg image to click on
        this.wrap.replaceClass('x-form-check-wrap', 'xcheckbox-wrap');
        this.cbEl = this.wrap.createChild({tag:'a', href:'#', cls:this.checked ? this.onCls : this.offCls});

        // reposition boxLabel if any
        var boxLabel = this.wrap.down('label');
        if(boxLabel) {
            this.wrap.appendChild(boxLabel);
        }

        // support tooltip
        if(this.tooltip) {
            this.cbEl.set({qtip:this.tooltip});
        }

        // install event handlers
        this.wrap.on({click:{scope:this, fn:this.onClick, delegate:'a'}});
        this.wrap.on({keyup:{scope:this, fn:this.onClick, delegate:'a'}});

        // restore tabIndex
        this.cbEl.dom.tabIndex = tabIndex;
    },
    
    onClick:function(e) {
        if(this.disabled || this.readOnly) {
            return;
        }
        if(!e.isNavKeyPress()) {
            this.setValue(!this.checked);
        }
    },
    
    onDisable:function() {
        this.cbEl.addClass(this.disabledClass);
        this.el.dom.disabled = true;
    },
    
    onEnable:function() {
        this.cbEl.removeClass(this.disabledClass);
        this.el.dom.disabled = false;
    },
    
    setValue:function(val) {
        if('string' == typeof val) {
            this.checked = val === this.submitOnValue;
        }
        else {
            this.checked = !(!val);
        }

        if(this.rendered && this.cbEl) {
            this.updateHidden();
            this.cbEl.removeClass([this.offCls, this.onCls]);
            this.cbEl.addClass(this.checked ? this.onCls : this.offCls);
        }
        this.fireEvent('check', this, this.checked);

    },
    
    updateHidden:function() {
        this.el.dom.value = this.checked ? this.submitOnValue : this.submitOffValue;
    },
    
    getValue:function() {
        return this.checked;
    }
});
Tera.reg('xcheckbox', Tera.ux.form.XCheckbox);

Tera.MenuPanel = Tera.extend(Tera.Panel, {
    baseCls : 'menu-panel',
    collapsedCls : 'menu-panel-collapsed',
   	initComponent : function(){
		Tera.MenuPanel.superclass.initComponent.call(this);
		this.addEvents('handleClick');
	},
	onRender : function(ct, position) {
		Tera.MenuPanel.superclass.onRender.call(this, ct, position);

		var element = this.getEl();
		element.on('click', this.handleClick.createDelegate(this));
		
		this.on('render', function(e){
			e.body.on('click', this.handleBodyClick.createDelegate(this));	
		});
	},
	handleClick : function(obj) {
		if (this.getXType() != 'menuitem') {
			this.fireEvent('handleClick',this);
			this.toggleCollapse(true);
		}
	},
	handleBodyClick : function(obj) {
		obj.stopPropagation();
	},
	getXType : function() {
		return 'menupanel'
	}
});
Tera.reg('menupanel', Tera.MenuPanel);

Tera.MenuItem = Tera.extend(Tera.MenuPanel, {
    baseCls : 'menu-item',
    collapsedCls : 'menu-item-collapsed',
    selectedCls : 'menu-item-selected',
    selected : false,
    url : null,
    text : null,
    
   	initComponent : function(){
 		Tera.MenuItem.superclass.initComponent.call(this);
		
 		if (this.selected) {
			this.addClass(this.selectedCls);
			this.removeClass(this.baseCls);
		}
	},
	onRender : function(ct, position) {
		Tera.MenuPanel.superclass.onRender.call(this, ct, position);
		
		var element = this.getEl();
		Tera.DomHelper.append(element, {
			tag : "a",
			cls : "x-plain",
			href: this.url,
			html: this.text
		});
		
 		if (this.selected) {
 			var parent = this.findParentByType(Tera.MenuPanel);
 			if (Tera.isEmpty(parent)) {
 				parent = this.findParentByType(Tera.Panel);
 				parent.expand();
 			} else {
 				parent.toggleCollapse();
 			}
 		} else {
 			
 		}
	},
	unselect : function() {
		this.addClass(this.baseCls);
		this.removeClass(this.selectedCls);
		this.selected = false;
	},
	select : function() {
		this.addClass(this.selectedCls);
		this.removeClass(this.baseCls);
		this.selected = true;
	},
	toggleSelection : function() {
		if (this.selected) {
			this.unselect()
		} else {
			this.select()
		}
	},
	getXType : function() {
		return 'menuitem'
	}
});
Tera.reg('menuitem', Tera.MenuItem);

Tera.namespace('Tera.ux.plugins');

Tera.ux.plugins.MenuState = function(config) {
    Tera.apply(this, config);
};

Tera.extend(Tera.ux.plugins.MenuState, Tera.util.Observable, {
	items : [],
	animate: true,
    init : function(menupanel) {
		this.items.push(menupanel);
		menupanel.on('beforeexpand', this.collapseAll.createDelegate(this, [menupanel]));
    },
    put : function(element) {
	    this.items.push(element);
	},
	collapseAll : function(menupanel) {
		this.items.each(function(item){
			if (item.id != menupanel.id) {
				item.collapse(this.animate);
			}	
		});
	}
});

Tera.form.Field.prototype.msgTarget = 'side';

//cod_identifi puede ser: 'N':NIF, 'C':CIF, 'P':PASAPORTE y 'T':TARJETA RESIDENTE
function comprobarDocumento(cod_identifi, num_identifi){
	var vlong = String(num_identifi).length;
	var M4 = num_identifi.charAt(vlong-1);
	var M1 = num_identifi.charAt(0);

	if (cod_identifi=='C') return validaCif(num_identifi);
	if (cod_identifi=='N') {		
		if (vlong >= 10) return -1;
		var i = 1;
		while ((i<8) && (num_identifi.charAt(i) >= '0' && num_identifi.charAt(i) <= '9')){
			i++;
		}
		if (i < 8) return -1;
		if ((M1>='0' && M1<='9') &&  (M4>='A' && M4<='Z')){
			if(!Ident_control3(num_identifi,'S')) return -1;
		}else if (M1 =='K'){
			if(!Ident_control3(num_identifi,'N')) return -1;
		}else if (M1 =='L') {
			if(!Ident_control3(num_identifi,'N')) return -1;
		}else {
			return -1;
		}
	} else if (cod_identifi=='T') {
		if (vlong >= 10) return -1;
		if (vlong < 10){
			if (!validaNie(num_identifi)) return -1;
			return 0;
		}
		if (M1 != 'X') return -1;
		if(!Ident_control3(num_identifi,'N')) return -1;
	} else if (cod_identifi!='P'){
		return -1;
	}
	return 0;
}


function validaCif (elCIF){
	var pares = 0; 
	var impares = 0; 
	var suma; 
	var ultima;
	var unumero; 
	var uletra = new Array('J', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'); 
	var xxx; 
	var entidad; 
	var bCIFvalido = false;     
	var texto = new String(elCIF);		//elCIF.value.toUpperCase();
	texto = texto.toUpperCase();
	if (!/^[A-Za-z0-9]{9}$/.test(texto)) {
		bCIFvalido =false;
	}else if (!/^[ABCDEFGHJKLMNPQRSUVW]/.test(texto)) {
		bCIFvalido = false;
	}else{
		ultima = texto.substr(8,1); //DIGITO DE CONTROL
		entidad = texto.substr(0,1);
		for (var cont = 1 ; cont < 7 ; cont ++){
			xxx = (2 * parseFloat(texto.substr(cont++,1))).toString() + '0'; 
			impares += parseFloat(xxx.substr(0,1)) + parseFloat(xxx.substr(1,1)); 
			pares += parseFloat(texto.substr(cont,1)); 
		} 
		xxx = (2 * parseFloat(texto.substr(cont,1))).toString() + '0'; 
		impares += parseFloat(xxx.substr(0,1)) + parseFloat(xxx.substr(1,1)); 
		suma = (pares + impares).toString(); 
		unumero = parseFloat(suma)% 10; 
		unumero = (10 - unumero).toString(); 
		if(unumero == 10) unumero = 0; 
		if ((entidad == 'A') || (entidad == 'B') || (entidad == 'E') || (entidad == 'H'))  {
			if (ultima == unumero) bCIFvalido = true;
		}else if  ((entidad == 'K') || (entidad == 'P') || (entidad == 'Q') || (entidad == 'R')|| (entidad == 'S')){
			if (ultima == uletra[unumero]) bCIFvalido = true;
		}else {
			if ((ultima == unumero)|| (ultima == uletra[unumero])) bCIFvalido = true;
		}   
	} 
	if (bCIFvalido) return 0;
	return -1;
}

function bNumIdentifiControl3(num_ide, caracter) {
	var resto;
	var numero;
	var equival = new Array('T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E');
	var str_numero = new Array(11);
	if(caracter=='S'){
		str_numero = num_ide.substring(0,8);
		numero = parseFloat(str_numero);
	}else{
		str_numero = num_ide.substring(1, num_ide.length - 1);
		numero = parseFloat(str_numero);
	}
	resto = (numero%23);
	return (equival[resto] == num_ide.charAt(num_ide.length - 1)); 
}

function validaNie(NumIde) {
	var M1,M4,local;
	if (NumIde.indexOf('.')>=0) return false;
	local ='N';
	M1 = NumIde.charAt(0);
	M4 = NumIde.charAt(NumIde.length-1);
	if (NumIde.length<10){
	if (M1=='X'){
		return bNumIdentifiControl3(NumIde,'N');
	} else if(M1=='Y' || M1=='Z'){
		if(M1=='Y'){
			var aux = '1'+NumIde.substring(1, NumIde.length);
		}else{
			var aux = '2'+NumIde.substring(1, NumIde.length);
		}
		return bNumIdentifiControl3(aux,'S');
	}
	}else if (NumIde.length == 10){
		if (M1=='X'){
			return bNumIdentifiControl3(NumIde,'N');
		}
	}
	return false;
}


Tera.override(Tera.grid.GridView, {
	templates: {
    	cell: new Tera.Template(
    		'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}" tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}" {attr}>{value}</div>',
    		'</td>'
    	)
    }
});


Tera.form.TextFieldArray = Tera.extend(Tera.form.TextField, {
	
	value: null,
	
   	initComponent : function(){
		Tera.form.TextFieldArray.superclass.initComponent.call(this);
	},
	
	onRender : function(ct, position) {
		Tera.form.TextFieldArray.superclass.onRender.call(this, ct, position);
	},
	
	getValue : function() {
		return Tera.form.TextFieldArray.superclass.getValue.call(this); 
	}
});
Tera.reg('multitextbox', Tera.form.TextFieldArray);

//Con esto agregamos el comportamiento de uppercase
Tera.apply(Tera.form.VTypes,{
	uppercase:function(val,field) {		
		var texto = val;
		texto = Tera.util.Format.uppercase(texto);
		field.setRawValue(texto);
		return true;			 
	}
});
