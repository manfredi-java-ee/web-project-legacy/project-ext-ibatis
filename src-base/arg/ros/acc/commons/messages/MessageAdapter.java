 




 




 



package arg.ros.acc.commons.messages;

import java.util.Locale;

import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * @author rosario.argentina
 *
 */
public class MessageAdapter {

	/**
	 * 
	 */
	private ResourceBundleMessageSource bundle;
	
	/**
	 * @param pKey
	 * @return
	 */
	public String get(String pKey) {
		return this.bundle.getMessage(pKey, null, null);
	}
	
	/**
	 * @param pKey
	 * @param pParams
	 * @return
	 */
	public String get(String pKey, Object[] pParams) {
		return this.bundle.getMessage(pKey, pParams, null);
	}
	
	/**
	 * @param pKey
	 * @param pParams
	 * @param pLocale
	 * @return
	 */
	public String get(String pKey, Object[] pParams, Locale pLocale) {
		return this.bundle.getMessage(pKey, pParams, pLocale);
	}

	/**
	 * @param pBundle the bundle to set
	 */
	public void setBundle(ResourceBundleMessageSource pBundle) {
		this.bundle = pBundle;
	}
}
