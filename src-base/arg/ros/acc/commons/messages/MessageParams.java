 




 




 



package arg.ros.acc.commons.messages;

import org.apache.commons.lang.ArrayUtils;

/**
 * @author rosario.argentina
 *
 */
public class MessageParams {

	/**
	 * 
	 */
	private Object[] params;

	/**
	 * 
	 */
	public MessageParams() {
		this.params = ArrayUtils.EMPTY_OBJECT_ARRAY;
	}
	
	/**
	 * @param pObject
	 * @return
	 */
	public MessageParams add(Object pObject) {
		ArrayUtils.add(this.params, pObject);
		return this;
	}

	/**
	 * @return the params
	 */
	public Object[] getParams() {
		return this.params;
	}
	
}
