 




 




 



package arg.ros.acc.commons.config;

import java.io.FileInputStream;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * @author rosario.argentina
 *
 */
public class ConfigParams {

	/**
	 * Indica si activamos o no los LOGS
	 */
	private String logs;
	
	/**
	 * Path del fichero de Menu
	 */
	private String menuPath;
	
	/**
	 * Nombre del fichero de Menu
	 */
	private String menuFile;

	/**
	 * Tamano Aplicacion Menu
	 */
	private String longAplicacion;
	
	/**
	 * Tamano cabecera Menu
	 */
	private String longCabecera;
	
	/**
	 * Tamano detalle Menu
	 */
	private String longDetalle;

	/**
	 * Host de conexion a LDAP
	 */
	private String ldapHost;
	
	/**
	 * Puerto de conexion a LDAP
	 */
	private int ldapPort;
	
	/**
	 * Connection String.
	 */
	private String ldapConnectionString;
	
	/**
	 * Host de conexion a LDAP
	 */
	private String ldapHost2;
	
	/**
	 * Puerto de conexion a LDAP
	 */
	private int ldapPort2;
	
	/**
	 * Connection String.
	 */
	private String ldapConnectionString2;
	
	/**
	 * 
	 */
	private String uploadPath;
	
	/**
	 * 
	 */
	private String fileErrorPath;
	
	/**
	 * 
	 */
	private String uniMuestraNombreParamFecEmision;



	/**
	 * @return the logs
	 */
	public String getLogs() {
		return this.logs;
	}

	/**
	 * @param pLogs the logs to set
	 */
	public void setLogs(String pLogs) {
		this.logs = pLogs;
	}

	/**
	 * @return the menuPath
	 */
	public String getMenuPath() {
	
		
		return this.menuPath;
	
	
	}

	/**
	 * @param pMenuPath the menuPath to set
	 * @throws URISyntaxException 
	 */
	public void setMenuPath(String pMenuPath) throws URISyntaxException {
	
		//  Primero de todo obtenemos el Class Loader de thread actual:
		//	Esto nos retorna un objeto con la siguiente información:
		//
		//	context: /MyApp
		//	delegate: false
		//  repositories:
		//	/WEB-INF/classes/
				
		
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		
    	//	Obtenemos el path del fichero recuperándolo como recurso y 
	    // 	generando el path absoluto del fichero en el sistema.
		
		 pMenuPath = loader.getResource("ApplicationMenu.xml").toURI().getPath();		
 
		
		this.menuPath = pMenuPath;
	
	
	
	}

	/**
	 * @return the menuFile
	 */
	public String getMenuFile() {
		return this.menuFile;
	}

	/**
	 * @param pMenuFile the menuFile to set
	 */
	public void setMenuFile(String pMenuFile) {
		this.menuFile = pMenuFile;
	}

	/**
	 * @return the longAplicacion
	 */
	public String getLongAplicacion() {
		return this.longAplicacion;
	}

	/**
	 * @param pLongAplicacion the longAplicacion to set
	 */
	public void setLongAplicacion(String pLongAplicacion) {
		this.longAplicacion = pLongAplicacion;
	}

	/**
	 * @return the longCabecera
	 */
	public String getLongCabecera() {
		return this.longCabecera;
	}

	/**
	 * @param pLongCabecera the longCabecera to set
	 */
	public void setLongCabecera(String pLongCabecera) {
		this.longCabecera = pLongCabecera;
	}

	/**
	 * @return the longDetalle
	 */
	public String getLongDetalle() {
		return this.longDetalle;
	}

	/**
	 * @param pLongDetalle the longDetalle to set
	 */
	public void setLongDetalle(String pLongDetalle) {
		this.longDetalle = pLongDetalle;
	}

	/**
	 * @return the ldapHost
	 */
	public String getLdapHost() {
		return this.ldapHost;
	}

	/**
	 * @param pLdapHost the ldapHost to set
	 */
	public void setLdapHost(String pLdapHost) {
		this.ldapHost = pLdapHost;
	}

	/**
	 * @return the ldapPort
	 */
	public int getLdapPort() {
		return this.ldapPort;
	}

	/**
	 * @param pLdapPort the ldapPort to set
	 */
	public void setLdapPort(int pLdapPort) {
		this.ldapPort = pLdapPort;
	}

	/**
	 * @return the ldapConnectionString
	 */
	public String getLdapConnectionString() {
		return this.ldapConnectionString;
	}

	/**
	 * @param pLdapConnectionString the ldapConnectionString to set
	 */
	public void setLdapConnectionString(String pLdapConnectionString) {
		this.ldapConnectionString = pLdapConnectionString;
	}

	/**
	 * @return the ldapHost2
	 */
	public String getLdapHost2() {
		return this.ldapHost2;
	}

	/**
	 * @param pLdapHost2 the ldapHost2 to set
	 */
	public void setLdapHost2(String pLdapHost2) {
		this.ldapHost2 = pLdapHost2;
	}

	/**
	 * @return the ldapPort2
	 */
	public int getLdapPort2() {
		return this.ldapPort2;
	}

	/**
	 * @param pLdapPort2 the ldapPort2 to set
	 */
	public void setLdapPort2(int pLdapPort2) {
		this.ldapPort2 = pLdapPort2;
	}

	/**
	 * @return the ldapConnectionString2
	 */
	public String getLdapConnectionString2() {
		return this.ldapConnectionString2;
	}

	/**
	 * @param pLdapConnectionString2 the ldapConnectionString2 to set
	 */
	public void setLdapConnectionString2(String pLdapConnectionString2) {
		this.ldapConnectionString2 = pLdapConnectionString2;
	}

	/**
	 * @return the uploadPath
	 */
	public String getUploadPath() {
		return this.uploadPath;
	}

	/**
	 * @param pUploadPath the uploadPath to set
	 */
	public void setUploadPath(String pUploadPath) {
		this.uploadPath = pUploadPath;
	}

	/**
	 * @return the fileErrorPath
	 */
	public String getFileErrorPath() {
		return this.fileErrorPath;
	}

	/**
	 * @param pFileErrorPath the fileErrorPath to set
	 */
	public void setFileErrorPath(String pFileErrorPath) {
		this.fileErrorPath = pFileErrorPath;
	}

	/**
	 * @return the uniMuestraNombreParamFecEmision
	 */
	public String getUniMuestraNombreParamFecEmision() {
		return this.uniMuestraNombreParamFecEmision;
	}

	/**
	 * @param pUniMuestraNombreParamFecEmision the uniMuestraNombreParamFecEmision to set
	 */
	public void setUniMuestraNombreParamFecEmision(
			String pUniMuestraNombreParamFecEmision) {
		this.uniMuestraNombreParamFecEmision = pUniMuestraNombreParamFecEmision;
	}
	
}
