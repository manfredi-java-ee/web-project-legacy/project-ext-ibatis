 




 




 



package arg.ros.acc.commons.util;


import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * @author rosario.argentina
 *
 */
public class ExcelWriter extends AbstractWriter {

	/**
	 * 
	 */
	private List records;
	
	/**
	 * 
	 */
	private String[] headers;
	
	/**
	 * 
	 */
	private Map<String,String> headersMap;
	
	/**
	 * 
	 */
	private HSSFWorkbook excelBook;
	
	/**
	 * 
	 */
	private HSSFCellStyle headerStyle;

	/**
	 * 
	 */
	private String[] gridColums;
	
	
	/**
	 * 
	 */
//#ADC#060111#TDE#312512#INICIO
	private Map<String, String> pProps;
//#ADC#060111#TDE#312512#FIN
	/**
	 * 
	 * @param pRecords
	 * @param pHeadersMap
	 * @param sGridColums
	 */
	public ExcelWriter(final List<?> pRecords, final Map<String, String> pHeadersMap, String [] sGridColums) {
		this.records = pRecords;
		this.headersMap = pHeadersMap;
		this.headers = this.headersMap.keySet().toArray(new String[this.headersMap.keySet().size()]);
		this.gridColums = sGridColums;
	}

//#ADC#060111#TDE#312512#INICIO
	/**
	 * @param pRecords
	 * @param pHeadersMap
	 * @param sGridColums
	 * @param pProps
	 */
	public ExcelWriter(final List<?> pRecords, final Map<String, String> pHeadersMap, String [] sGridColums, final Map<String, String> pProps) {
		this(pRecords, pHeadersMap, sGridColums);
		this.pProps = pProps;
	}
//#ADC#060111#TDE#312512#FIN

	/**
	 * @param pOutputStream
	 * @throws IOException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	private void process(OutputStream pOutputStream) throws IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, SecurityException, NoSuchFieldException {
        excelBook = new HSSFWorkbook();
        final HSSFSheet sheet = excelBook.createSheet();
        //setStyleCell();
        setStyleHeaders();
        sheet.setFitToPage(true);
        printHeaders(sheet);
        printGridBody(sheet);
		excelBook.write(pOutputStream);
		pOutputStream.flush();
        pOutputStream.close();
	}

	/**
	 * @param sheet
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws NoSuchFieldException 
	 * @throws SecurityException 
	 */
	private void printGridBody(final HSSFSheet sheet) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, SecurityException, NoSuchFieldException {
		for (int idx = 1; idx < this.records.size()+1; idx++) {
        	final HSSFRow row = sheet.createRow(idx);
        	
			Object vo = this.records.get(idx-1);

			for (int idxHeaders = 0; idxHeaders < this.headers.length; idxHeaders++) {
				HSSFCell cell = row.createCell(idxHeaders);
				
				String value = BeanUtils.getProperty(vo, this.headers[idxHeaders]);
				
				if (value == null) 
					value = "";
				
				Field field = null; 
				try {
					field = vo.getClass().getDeclaredField(this.headers[idxHeaders]);
				} catch(Exception e) {
					field = vo.getClass().getField(this.headers[idxHeaders]);
				}
				
				if (field.getType().getName().equalsIgnoreCase("java.util.Date")){
					value = new String(formatDate(value));//formatear la fecha
				}
				
				if (field.getType().getName().equalsIgnoreCase("java.math.BigDecimal")) {
					NumberFormat formatter;
//#ADC#060111#TDE#312512#INICIO
					//verificar si la columna requiere un formateo de decimal especial
					if ( this.pProps == null || (this.pProps != null && !this.headers[idxHeaders].equals(pProps.get("columnaPuntoFlotanteEspecial")))) {
						formatter = new DecimalFormat("#0.00");
					}
					else {
						formatter = new DecimalFormat("#0.00000");
					}
//#ADC#060111#TDE#312512#FIN
					value = (StringUtils.isEmpty(value) ? "0" : value);
					value = new String(formatter.format(new BigDecimal(value)));
					
				}
				
				//if (value.endsWith(".0")) value = new String(value.replaceAll("\\.0", ""));
				value = new String(value.replace('.', ','));
				final HSSFRichTextString cellValue = new HSSFRichTextString(value);
				if (cellValue.length()*350 > sheet.getColumnWidth(cell.getColumnIndex())){
					sheet.setColumnWidth(cell.getColumnIndex(), cellValue.length()*350);
				}

				cell.setCellValue(cellValue);

			}
		}
	}

	/**
	 * @param sheet
	 */
	private void printHeaders(final HSSFSheet sheet) {
		final HSSFRow headerRow = sheet.createRow(0);
        String[] tempHeaders = this.headers;
        
		if (this.headersMap != null) {
        	tempHeaders = this.headersMap.values().toArray(new String[this.headersMap.keySet().size()]);
        }
        
/*		if (this.gridColums != null) {
			String[] headerGirdColums = new String[gridColums.length];	
			if (this.headersMap != null){
				for(int i=0; i <gridColums.length; i++){
					headerGirdColums[i]= this.headersMap.get(gridColums[i]);
				}
			}
			this.headers = this.gridColums;
			tempHeaders = headerGirdColums;
		}
*/		
        for (int idxHeaders = 0; idxHeaders < tempHeaders.length; idxHeaders++) {
        	HSSFCell headerCell = headerRow.createCell(idxHeaders);

        	final String value = tempHeaders[idxHeaders];
			final HSSFRichTextString cellValue = new HSSFRichTextString(value.toUpperCase());
			if (cellValue.length()*350 > sheet.getColumnWidth(headerCell.getColumnIndex())){
				sheet.setColumnWidth(headerCell.getColumnIndex(), cellValue.length()*350);	
			}
			
			headerCell.setCellStyle(headerStyle);
			headerCell.setCellValue(cellValue);
        }
	}
	
	/**
	 * {@inheritDoc}
	 */
	public byte[] write() {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
		final OutputStream os = new BufferedOutputStream(baos);
		
		try {
			this.process(os);
			return baos.toByteArray();

		} catch (IOException e) {
			System.out.println("Error escribiendo el excel.");
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 */
	private void setStyleHeaders(){
		headerStyle = excelBook.createCellStyle();
		HSSFFont fuente = excelBook.createFont();
		fuente.setFontName("Arial");
		headerStyle.setFont(fuente);
		headerStyle.setWrapText(true);
		headerStyle.setAlignment(HSSFCellStyle. ALIGN_JUSTIFY);
		headerStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		headerStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		headerStyle.setBottomBorderColor((short)8);
		headerStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		headerStyle.setLeftBorderColor((short)8);
		headerStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		headerStyle.setRightBorderColor((short)8);
		headerStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		headerStyle.setTopBorderColor((short)8);

		fuente.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		headerStyle.setFont(fuente);
	}

	/**
	 * 
	 * @param date1
	 * @return
	 */
	private String formatDate(String date1){
		SimpleDateFormat sdf = new SimpleDateFormat(
				"EEE MMM dd hh:mm:ss zzz yyyy", new java.util.Locale("EN",
						"EN"));
		
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");;
		
		try {
			Date d = sdf.parse(date1);
			return sdf2.format(d);
		} catch (Exception e) {
			return date1;
		}
		
	}

}
