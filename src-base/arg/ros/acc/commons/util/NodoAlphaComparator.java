package arg.ros.acc.commons.util;

import java.util.Comparator;

import arg.ros.acc.components.menu.Nodo;

/**
 * @author rosario.argentina
 *
 */
public class NodoAlphaComparator implements Comparator<Nodo> {

	private Nodo parent;
	
	/**
	 * @param pParent
	 */
	public NodoAlphaComparator(Nodo pParent) {
		parent = pParent;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Nodo pNodo1, Nodo pNodo2) {
		if (this.parent != null && this.parent.getId().equals("menuMonitor")) {
			return (pNodo1.equals(pNodo2) ? 0 : -1);			
		}
		
		return (pNodo1.getLabel().compareTo(pNodo2.getLabel()));
	}
}
