 




 




 



package arg.ros.acc.commons.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonBeanProcessor;

/**
 * @author ACCROS_Admin
 *
 */
public class JsonDateProcessor implements JsonBeanProcessor {

	final SimpleDateFormat sdf = new SimpleDateFormat("d/MM/yyyy");
	
	/* (non-Javadoc)
	 * @see net.sf.json.processors.JsonBeanProcessor#processBean(java.lang.Object, net.sf.json.JsonConfig)
	 */
	public JSONObject processBean(Object bean, JsonConfig jsonConfig) {

		JSONObject jsonObject = null;
    	if( bean instanceof java.sql.Date ){
    		bean = new Date( ((java.sql.Date) bean).getTime() );
    	}
    	
    	if( bean instanceof Date ){
	        Calendar c = Calendar.getInstance();
	        c.setTime( (Date) bean );
	
	        String date = sdf.format(c.getTime());
	        
	        jsonObject = new JSONObject().element( "year", c.get( Calendar.YEAR ))
	               .element( "month", c.get( Calendar.MONTH ) )
	               .element( "day", c.get( Calendar.DAY_OF_MONTH ) )
	               .element( "hours", c.get( Calendar.HOUR_OF_DAY ) )
	               .element( "minutes", c.get( Calendar.MINUTE ) )
	               .element( "seconds", c.get( Calendar.SECOND ) )
	               .element( "milliseconds", c.get( Calendar.MILLISECOND ))
	               .element( "value", date);
    	} else {
    		jsonObject = new JSONObject( true );
    	}
    	return jsonObject;
	}

}
