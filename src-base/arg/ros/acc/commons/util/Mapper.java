 




 




 



package arg.ros.acc.commons.util;

import java.util.HashMap;

/**
 * @author rosario.argentina
 *
 */
public class Mapper extends HashMap {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2367333036117682340L;

	/**
	 * 
	 */
	public Mapper add(String pProperty, Class pParameterClass) {
		this.put(pProperty, pParameterClass);
		
		return this;
	}
}
