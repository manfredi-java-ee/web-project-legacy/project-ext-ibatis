 




 




 



package arg.ros.acc.commons.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import arg.ros.acc.commons.Constants;

/**
 * @author rosario.argentina
 *
 */
public class AccRosUtils {
	
	/**
	 * @param pFileName
	 * @return
	 */
	public static String getFilename(String pFileName) {
		if (StringUtils.contains(pFileName, "/") ||
			StringUtils.contains(pFileName, "\\")) {

			pFileName = StringUtils.replace(pFileName, "\\", "/");
			pFileName = StringUtils.substringAfterLast(pFileName, "/");
		}  
		return pFileName;
	}
	
	/**
	 * @param pFileName
	 * @return
	 */
	public static String getFilenameWithOutExtension(String pFileName) {
        String valorFile = AccRosUtils.getFilename(pFileName);
		int index = valorFile.lastIndexOf(".");
		String valor = valorFile.substring(0,index);
		return valor;
	}
	
	/**
	 * @param pFileName
	 * @return
	 */
	public static String getFilenameGetExtension(String pFileName) {
        String valorFile = AccRosUtils.getFilename(pFileName);
		int index = valorFile.lastIndexOf(".");
		String valor = valorFile.substring(index);
		return valor;
	}
	
	/**
	 * @param pFileName
	 * @return
	 */
	public static String getPath(String pFileName) {
		if (StringUtils.contains(pFileName, "/") ||
			StringUtils.contains(pFileName, "\\")) {

			pFileName = StringUtils.replace(pFileName, "\\", "/");
			pFileName = StringUtils.substringBeforeLast(pFileName, "/");
		}  
		return pFileName;
	}
	
	/**
	 * <p>Metodo que devuelve un numero convertido en texto bien formateado como porcentaje. 
	 * El numero entrante debe ser del tipo 0.53 para convertirlo en 53</p>
	 *
	 *	Puntos a tener en cuenta:
	 *	<ul>
	 * 		<li>Utiliza la coma como separador decimal</li>
	 * 		<li>Utiliza el punto como separador de miles</li>
	 * 		<li>Si tiene mas decimales que los indicados realiza un redondeo hacia arriba</li>
	 *		<li>Aniade el simbolo de porcentaje al final</li>
	 *	</ul>
	 *
	 * @param java.math.BigDecimal cantidad: El numero a ser formateado.
	 * @param int nDecimales: El numero de decimales a mostrar.
	 * @return java.lang.String Numero ya formateado
	 */
	public static String formatearPorcentaje(BigDecimal importe, int nDecimales, boolean conSimbolo) {
		double imp = importe.doubleValue();

		//Preparo un objeto para realizar el formateo
		NumberFormat formateo = NumberFormat.getPercentInstance();
		formateo.setMaximumFractionDigits(nDecimales);
		formateo.setMinimumFractionDigits(nDecimales);
		
		String salida = formateo.format(importe);
		if (!conSimbolo){
			salida = salida.substring(0, salida.length()-1);
		}

		return salida;
	}

	/**
	 * @return
	 */
	public static boolean isPublicURI(HttpServletRequest pRequest) {
		final String requestedUri = pRequest.getRequestURI();
        return (!StringUtils.contains(requestedUri, Constants.ACCROS.APP_SECURE_ROOT_DIRECTORY));
	}
	
	/**
	 * @param TipIde
	 * @param NumIde
	 * @return
	 */
	public static boolean validarNumIdentifi(String TipIde, String NumIde) {
		char M1, M4, local;

		try {
			// System.out.println(Integer.parseInt(NumIde));
			if (NumIde.indexOf(".") >= 0)
				return false;
			local = 'N';

			M1 = NumIde.charAt(0);
			M4 = NumIde.charAt(NumIde.length() - 1);
			if (TipIde.charAt(0) == 'C') {
				if (NumIde.length() < 10) {
					// -----------------------------------SAMOA_106187-------------------------------//
					/*
					 * Integer.parseInt(NumIde.substring(1,NumIde.length()-1));
					 * if ((M1 >= 'A' && M1 <= 'H') && (M4 >= '0' && M4 <= '9'))
					 * return bNumIdentifiControl1(NumIde); if ((M1 == 'S') &&
					 * (M4 >= 'A' && M4 <= 'J')) return
					 * bNumIdentifiControl2(NumIde); if ((M1 >= 'P' && M1 <=
					 * 'Q') && (M4 >= 'A' && M4 <= 'J')) return
					 * bNumIdentifiControl2(NumIde); if ((M1 >= 'A' && M1 <=
					 * 'H') && (M4 >= 'A' && M4 <= 'J')) return
					 * bNumIdentifiControl2(NumIde); if ((M1 == 'N') && (M4 >=
					 * 'A' && M4 <= 'J')) return bNumIdentifiControl2(NumIde); }
					 * ese return false;
					 */
					// ABCDEFGHJKLMNPQRSUVW posibles valores del digito entidad
					// MODIFICACION DE LAS LETRAS QUE COMPONEN EL CIF
					int numcontrol, numM4;
					// Los CIF que empiezan con A, B, E y H tienen diito de
					// control
					if (M1 == 'A' || M1 == 'B' || M1 == 'E' || M1 == 'H') {
						if (!(M4 >= '0' && M4 <= '9'))
							return false;
						else {

							numcontrol = bNumIdentifiControl1(NumIde);
							numM4 = (int) M4 - 48;
							if (numcontrol == numM4)
								return true;
							else
								return false;
						}
					}

					// Los que empiezan con K, P, Q, R, S tienen letra de
					// control
					else if (M1 == 'K' || M1 == 'P' || M1 == 'Q' || M1 == 'R'
							|| M1 == 'S')

					{
						if (!(M4 >= 'A' && M4 <= 'J'))
							return false;
						else {
							char charcontrol = ' ';
							numcontrol = bNumIdentifiControl1(NumIde);
							charcontrol = bNumIdentifiControl2(numcontrol);

							if (charcontrol == M4)
								return true;
							else
								return false;
						}
					}

					// Los que empiezan con G, U, J, V, N, W, C, D, F, L, M
					// pueden tener
					// tanto letra como digito de control

					else if (M1 == 'G' || M1 == 'J' || M1 == 'N' || M1 == 'C'
							|| M1 == 'D' || M1 == 'F' || M1 == 'L' || M1 == 'M'
							|| M1 == 'U' || M1 == 'V' || M1 == 'W') {
						if (!((M4 >= 'A' && M4 <= 'J') || (M4 >= '1' && M4 <= '9')))
							return false;
						else {
							char charcontrol = ' ';
							numcontrol = bNumIdentifiControl1(NumIde);
							charcontrol = bNumIdentifiControl2(numcontrol);
							numM4 = (int) M4 - 48;
							if (charcontrol == M4 || numcontrol == numM4)
								return true;
							else
								return false;

						}
					}

					// Si la entidad no existe
					else
						return false;
				}

				// --------------------------------------------------------------------------//
				else
					return false;
			} else if (TipIde.charAt(0) == 'N') {
				if (NumIde.length() < 10) {
					Integer.parseInt(NumIde.substring(1, NumIde.length() - 1));
					if ((M1 >= '0' && M1 <= '9') && (M4 >= 'A' && M4 <= 'Z'))
						return bNumIdentifiControl3(NumIde, 'S');
					if (M1 == 'K')
						return bNumIdentifiControl3(NumIde, 'N');
					if (M1 == 'L')
						return bNumIdentifiControl3(NumIde, 'N');

				} else
					return false;
			}
			/* Inicio modificacion. Peticion #SAMOA#107039 */
			else if (TipIde.charAt(0) == 'T') {
				if (NumIde.length() < 10) { // Hasta 9 caracteres maximo
					if (M1 == 'X')
						return bNumIdentifiControl3(NumIde, 'N');

					else if (M1 == 'Y' || M1 == 'Z') {
						if (M1 == 'Y')
							return bNumIdentifiControl3("1"
									+ NumIde.substring(1, NumIde.length()), 'S');
						else
							return bNumIdentifiControl3("2"
									+ NumIde.substring(1, NumIde.length()), 'S');
					}
				} else if (NumIde.length() == 10) {
					if (M1 == 'X') {
						return bNumIdentifiControl3(NumIde, 'N');
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
			/* Fin modificacion. Peticion #SAMOA#107039 */
			else if (TipIde.charAt(0) == 'D') {
				if (NumIde.length() < 9) {
					Integer.parseInt(NumIde.substring(0, NumIde.length()));
					return true;
				} else
					return false;
			} else if (TipIde.charAt(0) == 'P') {
				return true;
			} else
				return false;

		} catch (NumberFormatException e) {
			return false;
		}
		return false;
	}

	/**
	 * @param NumIde
	 * @return
	 */
	public static int bNumIdentifiControl1(String NumIde) {
		String pos28 = "";
		int R, R1, R2, R21, R22, R23, R24, resto;
		pos28 = NumIde.substring(1, NumIde.length() - 1);

		if (pos28.length() != 7)
			return 10;// false
		R1 = new Integer(pos28.substring(1, 2)).intValue() + new Integer(pos28.substring(3, 4)).intValue() + new Integer(pos28.substring(5, 6)).intValue();
		R21 = new Integer(pos28.substring(0, 1)).intValue() * 2;

		if (R21 >= 10)
			R21 = 1 + R21 % 10;
		
		R22 = new Integer(pos28.substring(2, 3)).intValue() * 2;
		
		if (R22 >= 10)
			R22 = 1 + R22 % 10;
		
		R23 = new Integer(pos28.substring(4, 5)).intValue() * 2;
		
		if (R23 >= 10)
			R23 = 1 + R23 % 10;
		
		R24 = new Integer(pos28.substring(6, 7)).intValue() * 2;
		
		if (R24 >= 10)
			R24 = 1 + R24 % 10;
		
		R2 = R21 + R22 + R23 + R24;
		
		R = R1 + R2;
		
		resto = R % 10;
		
		if (resto != 0)
			resto = 10 - resto;

		return resto;

	}

	/**
	 * @param resto
	 * @return
	 */
	public static char bNumIdentifiControl2(int resto) {
		char control = ' ';

		switch (resto) {
		case 1:
			control = 'A';
			break;
		case 2:
			control = 'B';
			break;
		case 3:
			control = 'C';
			break;
		case 4:
			control = 'D';
			break;
		case 5:
			control = 'E';
			break;
		case 6:
			control = 'F';
			break;
		case 7:
			control = 'G';
			break;
		case 8:
			control = 'H';
			break;
		case 9:
			control = 'I';
			break;
		case 0:
			control = 'J';
			break;
		}
		return control;
	}
	
	/**
	 * @param NumIde
	 * @param Normal
	 * @return
	 */
	public static boolean bNumIdentifiControl3(String NumIde, char Normal) {
		long numero;
		int resto;
		char control = ' ';
		String sTemp = "";

		for (int x = 0; x < 11 - NumIde.length(); x++)
			sTemp = sTemp + "0";
		
		if (Normal == 'S') {
			numero = new Long((sTemp + NumIde).substring(0, 10)).longValue();
		} else
			numero = new Long(NumIde.substring(1, NumIde.length() - 1)).longValue();

		resto = (int) numero % 23;
		resto++;

		switch (resto) {
		case 1:
			control = 'T';
			break;
		case 2:
			control = 'R';
			break;
		case 3:
			control = 'W';
			break;
		case 4:
			control = 'A';
			break;
		case 5:
			control = 'G';
			break;
		case 6:
			control = 'M';
			break;
		case 7:
			control = 'Y';
			break;
		case 8:
			control = 'F';
			break;
		case 9:
			control = 'P';
			break;
		case 10:
			control = 'D';
			break;
		case 11:
			control = 'X';
			break;
		case 12:
			control = 'B';
			break;
		case 13:
			control = 'N';
			break;
		case 14:
			control = 'J';
			break;
		case 15:
			control = 'Z';
			break;
		case 16:
			control = 'S';
			break;
		case 17:
			control = 'Q';
			break;
		case 18:
			control = 'V';
			break;
		case 19:
			control = 'H';
			break;
		case 20:
			control = 'L';
			break;
		case 21:
			control = 'C';
			break;
		case 22:
			control = 'K';
			break;
		case 23:
			control = 'E';
			break;
		}
		
		if (control == (sTemp + NumIde).charAt(10))
			return true;
		else
			return false;
	}
}
