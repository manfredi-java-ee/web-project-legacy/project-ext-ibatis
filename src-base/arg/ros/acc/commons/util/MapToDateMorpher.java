package arg.ros.acc.commons.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.ezmorph.object.AbstractObjectMorpher;

import org.apache.commons.lang.StringUtils;

/**
 * @author rosario.argentina
 *
 */
public class MapToDateMorpher extends AbstractObjectMorpher {
	
	/**
	 * 
	 */
	private String dateTimeFormat = "dd/MM/yyyy";
	
	/**
	 * 
	 */
	private SimpleDateFormat sdFormat;

	/**
	 * 
	 */
	public MapToDateMorpher() {
	}
	
	/**
	 * @param pDateFormat
	 */
	public MapToDateMorpher(final String pDateFormat) {
		this.dateTimeFormat = pDateFormat;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Object morph(Object value) {
		if (value instanceof String) {
			String s = (String) value;
			return convertStringToTimestamp(s);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public Class morphsTo() {
		return Date.class;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean supports(Class clazz) {
		return String.class.isAssignableFrom(clazz);
	}

	/**
	 * @param input
	 * @return
	 */
	private Date convertStringToTimestamp(String input) {
		if (!StringUtils.isEmpty(input)) {
			sdFormat = new SimpleDateFormat(this.dateTimeFormat);
			
			try {
				return sdFormat.parse(input);
			} catch (ParseException e) {
				System.out.println("Error parseando la fecha: "+input+" - SKIPPED");
			}
		}
		return null;
	}
}
