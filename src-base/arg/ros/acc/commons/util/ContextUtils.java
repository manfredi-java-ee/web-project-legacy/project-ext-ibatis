 




 




 



package arg.ros.acc.commons.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import arg.ros.acc.commons.Constants;

/**
 * @author rosario.argentina
 *
 */
public class ContextUtils {

	/**
	 * @param pKey
	 * @param pRequest
	 * @return
	 */
	public static Object get(final String pKey, HttpServletRequest pRequest) {
		Object value = get(pKey, Constants.SCOPES.REQUEST_SCOPE, pRequest);
		if (value == null) {
			value = get(pKey, Constants.SCOPES.SESSION_SCOPE, pRequest);
			if (value == null) {
				value = get(pKey, Constants.SCOPES.THREAD_LOCAL_SCOPE, pRequest);
			}
		}
		return value;
	}
	
	/**
	 * @param pKey
	 * @param pWhere
	 * @param pRequest
	 * @return
	 */
	public static Object get(final String pKey, final int pWhere, final HttpServletRequest pRequest) {
		Object value = null;
		
		switch (pWhere) {
		case Constants.SCOPES.REQUEST_SCOPE:
			value = pRequest.getAttribute(pKey);
			break;
			
		case Constants.SCOPES.SESSION_SCOPE:
			value = pRequest.getSession().getAttribute(pKey);
			break;
		
		default:
			value = Container.retrive(pKey);
			break;
		}
		
		return value;
	}
	
	/**
	 * @param request
	 * @return
	 */
	public static void set(final String pKey, final Object pValue, final int pWhere, HttpServletRequest pRequest) {
		switch (pWhere) {
		case Constants.SCOPES.REQUEST_SCOPE:
			pRequest.setAttribute(pKey, pValue);
			break;
			
		case Constants.SCOPES.SESSION_SCOPE:
			pRequest.getSession().setAttribute(pKey, pValue);
			break;
		
		default:
			Container.store(pKey, pValue);
			break;
		}
	}
	
	/**
	 * 
	 * @param vListaPgaTarMensu
	 * @return
	 */
	public static Map <String, String> obtenerCodigosMap(List <String> listaALL, String listaValidos){
		if (listaALL == null || listaValidos == null){
			return null;
		}
		LinkedHashMap<String, String> resultado = new LinkedHashMap<String, String>();
		List <String> listaPT_validos = Arrays.asList(listaValidos.split(","));
		listaALL.removeAll(listaPT_validos);
		String listaNoValidos = obtenerCadena_Tipo_split(listaALL);
		resultado.put("listaValidos", listaValidos);
		resultado.put("listaNoValidos", listaNoValidos);
		
	return resultado;
	}
	
	/**
	 * Metodo usado para a partir de una List <String> obtener un String con formato: "value,...,value"
	 * @param listaCodigos
	 * @return
	 */
	public static String obtenerCadena_Tipo_split(List <String> listaCodigos) {
		if ((listaCodigos == null) || (listaCodigos.size() < 0)){
			return null;
	    } 
		int sizeArray = listaCodigos.size();
	    StringBuffer buf = new StringBuffer();
	    for (int i=0; i < sizeArray; i++){
	      buf.append(listaCodigos.get(i) + ",");
	    }
	    if (sizeArray > 0) {
	    	buf.deleteCharAt(buf.length() - 1);
	    }
	    return buf.toString();
	  }
	
	
	/**
	 * Metodo usado para eliminar los codigos duplicados
	 * @param listaCodigos
	 * @return
	 */
	public static List <String> eliminarCodigosDuplicados(List <String> listaCodigos) {
		if ((listaCodigos == null) || (listaCodigos.size() < 0)){
			return null;
	    } 
		Set<String> vSet = new HashSet<String>(listaCodigos);
		List<String> result = new ArrayList <String>(vSet);
		return result;
	}
}
