 




 




 



package arg.ros.acc.commons.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.Constants.ACCROS;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public class Container {

	/**
	 * 
	 */
	private static ThreadLocal threadLocal = new ThreadLocal();
	
	/**
	 * @param pKey
	 * @param pObject
	 */
	public static void store(String pKey, Object pObject) {
		final Map map = Collections.synchronizedMap(new HashMap());
		map.put(pKey, pObject);
		
		Container.threadLocal.set(map);
	}
	
	/**
	 * @param pKey
	 * @return
	 */
	public static Object retrive(String pKey) {
		final Map map = (Map) Container.threadLocal.get();
		if (map != null) {
			return map.get(pKey);
		}
		return null;
	}
	
	/**
	 * @return
	 */
	public static UsuarioVO getUser() {
		try {
			final Map map = (Map) Container.threadLocal.get();
            final UsuarioVO usuarioVO = (UsuarioVO) map.get(Constants.ACCROS.APP_USUARIO);
			return usuarioVO;
		}catch (Exception e) {
			return null;
		}
	}
	
	public static void borrarUser() {
		final Map map = (Map) Container.threadLocal.get();
        map.remove(Constants.ACCROS.APP_USUARIO);
	}
}
