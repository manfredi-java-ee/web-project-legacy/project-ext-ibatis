 




 




 



package arg.ros.acc.commons.util.writers;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import arg.ros.acc.commons.util.AbstractWriter;

/**
 * @author rosario.argentina
 *
 */
public class CSVWriter extends AbstractWriter {

	/**
	 * 
	 */
	private List records;
	
	/**
	 * 
	 */
	private Object model;
	
	/**
	 * 
	 */
	private Object[] headers;

	/**
	 * 
	 */
	private static String NEW_LINE_SEPARATOR = "\n";

	/**
	 * 
	 */
	private static String RECORD_SEPARATOR = ",";
	
	/**
	 * 
	 */
	private String newLineSeparator;
	
	/**
	 * 
	 */
	private String recordSeparator;
	
	/**
	 * @param pRecords
	 */
	public CSVWriter(List pRecords) {
		this(pRecords, null);
	}
	
	/**
	 * @param pRecords
	 * @param pModel
	 */
	public CSVWriter(List pRecords, Object pModel) {
		this.records = pRecords;
		this.model = pModel;
	
		this.newLineSeparator = NEW_LINE_SEPARATOR;
		this.recordSeparator = RECORD_SEPARATOR;
		
		initialize();
	}
	
	/**
	 * 
	 */
	private void initialize() {
		if (this.model==null) {
			return;
		}
		
		if (this.model.getClass().isAssignableFrom(String[].class)){ 
			this.headers = (Object[]) this.model;
		} else {
			try {
				Map properties = BeanUtils.describe(this.model);
                this.headers = properties.keySet().toArray();
				
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			} catch (NoSuchMethodException e) {
			}
		} 
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] write() {
		final StringBuffer buffer = new StringBuffer();
		
		for (int idxObj = 0; idxObj < this.records.size(); idxObj++) {
			final Object object = this.records.get(idxObj);
			
			if (model != null) {

				for (int idx = 0; idx < this.headers.length; idx++) {
					final String objProperty = this.headers[idx].toString();
					try {
						final Map properties = BeanUtils.describe(object);
                        final Object value = properties.get(objProperty);
						
						if (value==null) 
							continue;

						buffer.append(value.toString());
						
						// Ultimo elemento.
						if (idx != this.headers.length -1) {
							buffer.append(this.recordSeparator);
						} else {
							buffer.append(this.newLineSeparator);
						}
						
					} catch (IllegalAccessException e) {
					} catch (InvocationTargetException e) {
					} catch (NoSuchMethodException e) {
					}
				}

			} else {
				
				buffer.append(object.toString()).append(this.newLineSeparator);
			}
		}

		return buffer.toString().getBytes();
	}

	/**
	 * @param pNewLineSeparator the newLineSeparator to set
	 */
	public void setNewLineSeparator(String pNewLineSeparator) {
		this.newLineSeparator = pNewLineSeparator;
	}

	/**
	 * @param pRecordSeparator the recordSeparator to set
	 */
	public void setRecordSeparator(String pRecordSeparator) {
		this.recordSeparator = pRecordSeparator;
	}

}
