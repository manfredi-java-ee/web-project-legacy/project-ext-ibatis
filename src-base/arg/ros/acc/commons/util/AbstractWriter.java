 




 




 



package arg.ros.acc.commons.util;

/**
 * @author rosario.argentina
 *
 */
public abstract class AbstractWriter {

	/**
	 * @return
	 */
	public abstract byte[] write();
	
}
