 




 




 



package arg.ros.acc.commons.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

/**
 * @author rosario.argentina
 *
 */
public class JsonDateValueProcessor implements JsonValueProcessor {

	/**
	 * 
	 */
	final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	/* (non-Javadoc)
	 * @see net.sf.json.processors.JsonValueProcessor#processArrayValue(java.lang.Object, net.sf.json.JsonConfig)
	 */
	public Object processArrayValue(Object pArg0, JsonConfig pArg1) {
		return null;
	}

	/* (non-Javadoc)
	 * @see net.sf.json.processors.JsonValueProcessor#processObjectValue(java.lang.String, java.lang.Object, net.sf.json.JsonConfig)
	 */
	public Object processObjectValue(String pKey, Object pObject, JsonConfig pJsonConfig) {
		if (pObject instanceof Date) {
			final Date date = (Date) pObject;
			return sdf.format(date);
		}
		
		return null;
	}

}
