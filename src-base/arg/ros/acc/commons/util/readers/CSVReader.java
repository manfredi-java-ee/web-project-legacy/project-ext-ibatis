 




 




 



package arg.ros.acc.commons.util.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author rosario.argentina
 *
 */
public class CSVReader extends AbstractReader {

	/**
	 * 
	 */
	private String separator = ";";
	
	/**
	 * @param pFile
	 * @param pSeparator
	 */
	public CSVReader(File pFile, String pSeparator) {
		super(pFile);
		this.separator = pSeparator;
	}
	
	/**
	 * @param pFile
	 */
	public CSVReader(File pFile) {
		super(pFile);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> read() throws Exception {
		try {
			final List values = new ArrayList();
			
			final FileReader  fichero = new FileReader(this.file);
			final BufferedReader br = new BufferedReader(fichero);
			
			String linea = null;
			while((linea = br.readLine()) != null){
				try {
                    values.add(linea);
					
				} catch (Exception e) {
					throw e;
				}
			}
			return values;
				
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		return null;
	}

}
