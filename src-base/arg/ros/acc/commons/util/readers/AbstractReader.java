 




 




 



package arg.ros.acc.commons.util.readers;

import java.io.File;
import java.util.List;

/**
 * @author rosario.argentina
 *
 */
public abstract class AbstractReader {

	/**
	 * 
	 */
	protected File file;
	
	/**
	 * @param pFile
	 */
	public AbstractReader(File pFile ) {
		this.file = pFile;
	}
	
	/**
	 * @return
	 */
	public abstract List<Object> read() throws Exception;

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return this.file.getName();
	}
}
