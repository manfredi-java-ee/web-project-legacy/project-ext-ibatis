/**
 * 
 */
package arg.ros.acc.commons.logs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.config.ConfigParams;

  


/**
 * Clase Servicio Logs
 *
 */
public class LoggerWeb {

	
	public static final Logger logi = Logger.getLogger(LoggerWeb.class);
	
	/**
	 * Constantes de Errores de fichero
	 */
	public static final int FICH_NOTFOUND = 1;
	
	/**
	 * Constantes de Errores de fichero
	 */
	public static final int FICH_NOTVALID = 2;

	/**
	 * 
	 */
	private static final String CONSTANT_SI = "SI";
	
	/**
	 * Constantes de Errores de Sistema 
	 */
	public static final String SYS_DAO_OBTENER_FUENTE = "SYS-0001"; 
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_OBTENER_CONEXION = "SYS-0002";
	
	/**
	 * Constantes de Errores de Sistema 
	 */
	public static final String SYS_DAO_CARGAR_DRIVER = "SYS-0003";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_CERRAR_CONEXION = "SYS-0004";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_INICIAR_TRANSACCION = "SYS-0005";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_EJECUTAR_TRANSACCION = "SYS-0006";
	
	/**
	 * Constantes de Errores de Sistema 
	 */
	public static final String SYS_DAO_CANCELAR_TRANSACCION = "SYS-0007";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_PARSEAR_XML = "SYS-0008";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_ASIGNAR_VALOR = "SYS-0009";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_PREPARAR_SENTENCIA = "SYS-000A";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_ACCESO_ILEGAL = "SYS-000B";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_EJECUTAR_SELECT = "SYS-000C";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_RECORRER_RS = "SYS-000D";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_INSTANCIACION = "SYS-000E";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_CERRAR_RS = "SYS-000F";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_CERRAR_STATEMENT = "SYS-0010";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_EJECUTAR_INSERT = "SYS-0011";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_EJECUTAR_UPDATE = "SYS-0012";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_EJECUTAR_OTRA = "SYS-0013";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_DAO_COMPONER_SENTENCIA = "SYS-0014";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_ACCESO_DATOS_GENERALES = "SYS-0019";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_PARAR_SERVLET = "SYS-001B";
	
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_EMAIL_COMPONER_MSG = "SYS-001E";
	
	/**
	 * Constantes de Errores de Sistema
	 */
	public static final String SYS_EMAIL_ENVIAR_MSG = "SYS-001F";
	
	/**
	 * Constantes de Errores de Logica de Aplicacion 
	 */
	public static final int LOG_REGISTRO_NO_INSERTADO = 100;
	
	/**
	 * Constantes de Errores de Logica de Aplicacion 
	 */
	private static final String LOG_REGISTRO_NO_INSERTADO_STR = "El registro no se ha podido insertar";
	
	/**
	 * Constantes de Errores de Logica de Aplicacion
	 */
	public static final int LOG_REGISTRO_NO_ACTUALIZADO = 101;
	
	/**
	 * Constantes de Errores de Logica de Aplicacion
	 */
	private static final String LOG_REGISTRO_NO_ACTUALIZADO_STR = "El registro no se ha podido actualizar";
	
	/**
	 * Constantes de Errores de Logica de Aplicacion
	 */
	public static final int LOG_BD_PRIMARY_KEY = 102;
	
	/**
	 * Constantes de Errores de Logica de Aplicacion
	 */
	private static final String LOG_BD_PRIMARY_KEY_STR = "Clave primaria duplicada";
	
	/**
	 * Constantes de Errores de Logica de Aplicacion
	 */
	public static final int LOG_BD_INTEGRIDAD = 103;
	
	/**
	 * Constantes de Errores de Logica de Aplicacion
	 */
	private static final String LOG_BD_INTEGRIDAD_STR = "Clave principal no encontrada";
	
	/**
	 * Constantes de Errores de Logica de Aplicacion 
	 */
	public static final int LOG_ERROR_ROLLBACK = 104;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_COMENTARIO = 0;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar 
	 */
	private static final int METODO_ELEMENTO_PROCESADO = 1;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_INICIO_PROCESO = 2;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_FIN_PROCESO = 3;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar 
	 */
	private static final int METODO_INICIO_TUXEDO = 4;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar 
	 */
	private static final int METODO_FIN_TUXEDO = 5;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_ERROR_TUXEDO = 6;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_ERROR_BBDD = 7;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar 
	 */
	private static final int METODO_ERROR_FICHERO = 8;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_ERROR_SYS = 9;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_ERROR_LOGICA = 10;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar 
	 */
	private static final int METODO_INICIO_FTP = 11;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_FIN_FTP = 12;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_ERROR_FTP = 13;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar 
	 */
	private static final int METODO_INICIO_RMI = 14;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_FIN_RMI = 15;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_ERROR_RMI = 16;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_INICIO_JNDI_EJB = 17;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_FIN_JNDI_EJB = 18;
	
	/**
	 * Constantes que indican el metodo de log a ejecutar
	 */
	private static final int METODO_ERROR_JNDI_EJB = 19;

	/**
	 * Almacena el path del fichero de configuracion de logs
	 */
	private String path = null;

	/**
	 * Parametros de configuracion. 
	 */
	private ConfigParams configParams;
	

	
	
	
	
	
	/**
	 * Metodo para inicializar el API de Logs
	 * @throws MalformedURLException 
	 */
	public void inititialize() throws MalformedURLException {

		
		
		if (StringUtils.isNotEmpty(this.path)) {
    			
		
			//logj_lj_TrazadorWasTsm.setFicheroConfig(path, Constants.ACCROS.APP_NAME, Constants.ACCROS.APP_NAME);
	
		
		}
	
		
	}
	
	
	
	
	
	
	
	
 







	/**
	 * Metodo para escribir un comentario libre (MJS-Comentar)
	 * 
	 * @param elemento La clase desde la que se lanza la informacion
	 * @param comentario El texto libre a trazar
	 */
	public void Comentario(final Object pElemento, final String pComentario) {
		if (this.isLogEnable()) {
			final LogBean log = new LogBean();
			log.setElemento(pElemento.getClass().getName());
			log.setComentario(pComentario);
			
			this.log(LoggerWeb.METODO_COMENTARIO, log);
		}
	}
	
	/**
	 * Metodo para escribir informacion sobre un elemento procesado (MSJ-ElemProc)
	 * 
	 * @param elemento La clase desde la que se lanza la informacion
	 * @param accion Accion que se realiza sobre el elemento
	 * @param operacion La operacion que realiza (SELECT, INSERT, UPDATE, ...)
	 * @param valor El valor asociado al elemento
	 */
	public void ElementoProcesado(final Object pElemento, final String pAccion, final String pOperacion, final String pValor) {
		if (this.isLogEnable()) {
			final LogBean log = new LogBean();
			log.setElemento(pElemento.getClass().getName());
			log.setAccion(pAccion);
			log.setOperacion(pOperacion);
			log.setValor(pValor);
			
			this.log(LoggerWeb.METODO_ELEMENTO_PROCESADO, log);
		}
	}
	
	/**
	 * Metodo para escribir informacion indicando el inicio de una accion
	 * 
	 * @param elemento La clase que inicia la accion
	 * @param comentario Comentario libre que indica la funcion principal del proceso que se ejecuta
	 */
	public void InicioProceso(final Object pElemento, final String pComentario) {
		if (this.isLogEnable()) {
			final LogBean log = new LogBean();
			log.setElemento(pElemento.getClass().getName());
			log.setComentario(pComentario);
			
			this.log(LoggerWeb.METODO_INICIO_PROCESO, log);
		}
	}
	
	/**
	 * Metodo para escribir informacion indicando el fin de una accion
	 * 
	 * @param elemento La clase que finaliza la accion
	 * @param comentario Comentario libre que indica la funcion principal del proceso que se ha ejecutado
	 */
	public void FinProceso(final Object pElemento, final String pComentario) {
		if (this.isLogEnable()) {
			final LogBean log = new LogBean();
			log.setElemento(pElemento.getClass().getName());
			log.setComentario(pComentario);
			
			this.log(LoggerWeb.METODO_FIN_PROCESO, log);
		}
	}

	/**
	 * Indica un error fatal detectado durante la ejecucion de un acceso a Base de Datos. 
	 * 
	 * @param elemento La clase desde la que se produce el error
	 * @param accion La accion que estamos llevando a cabo
	 * @param excepcion La excepcion 
	 */
	
	public void ErrorBBDD(final Object pElemento, final String pAccion, final Exception pExcepcion) {
		SQLException eSql = null;
		
		if (this.isLogEnable()) {
			
			if (pExcepcion.getCause() instanceof SQLException) {
				Exception exc = (Exception) pExcepcion.getCause();
				eSql = (SQLException) exc;
				final LogBean log = new LogBean();
				log.setElemento(pElemento.getClass().getName());
				log.setAccion(pAccion);
				log.setCodigoError(eSql.getErrorCode());
				log.setComentario(eSql.getMessage());
				
				this.log(LoggerWeb.METODO_ERROR_BBDD, log);
			}
			
		}
	}
	
	/**
	 * Indica un error fatal detectado durante la ejecucion de la logica de un Servlet. 
	 * 
	 * @param elemento La clase desde la que se produce el error
	 * @param codigoError El codigo de error
	 * @param comentario La descripcion del error
	 */
	public void ErrorLogicaAplicacion(final Object pElemento, final int pCodigoError) {
		if (this.isLogEnable()) {
			final LogBean log = new LogBean();
			log.setElemento(pElemento.getClass().getName());
			log.setCodigoError(pCodigoError);
			
			switch (pCodigoError) {
				case LoggerWeb.LOG_REGISTRO_NO_INSERTADO:
					log.setComentario(LoggerWeb.LOG_REGISTRO_NO_INSERTADO_STR); 
					break;
				
				case LoggerWeb.LOG_REGISTRO_NO_ACTUALIZADO: 
					log.setComentario(LoggerWeb.LOG_REGISTRO_NO_ACTUALIZADO_STR);
					break;
					
				case LoggerWeb.LOG_BD_PRIMARY_KEY: 
					log.setComentario(LoggerWeb.LOG_BD_PRIMARY_KEY_STR);
					break;
				
				case LoggerWeb.LOG_BD_INTEGRIDAD:
					log.setComentario(LoggerWeb.LOG_BD_INTEGRIDAD_STR);
					break;
			}
			
			this.log(LoggerWeb.METODO_ERROR_LOGICA, log);
		}
	}
	
	/**
	 * Error producido durante la carga o procesado de un fichero
	 * 
	 * @param elemento La clase desde la que se produce el error
	 * @param fichero El nombre del fichero al que se intenta acceder
	 * @param codError El codigo del error producido 
	 * @param comentario Texto libre con la descripcion del error
	 */
	public void ErrorFichero(final Object elemento, final String fichero, final int codError, final String comentario) {
		if (this.isLogEnable()) {
			final LogBean log = new LogBean();
			log.setElemento(elemento.getClass().getName());
			log.setFichero(fichero);
			log.setCodigoError(codError);
			log.setComentario(comentario);
			
			this.log(LoggerWeb.METODO_ERROR_FICHERO, log);
		}
	}
		
	/**
	 * Error Generico. Utilizado cuando se produce una excepcion y no encaja con ningun tipo de error 
	 * 
	 * @param codigo El codigo del error
	 * @param comentario La descripcion del error
	 */
	public void ErrorSistema(final String pCodigo, final String pComentario) {
		if (this.isLogEnable()) {
			final LogBean log = new LogBean();
			log.setCodigo(pCodigo);
			log.setComentario(pComentario);
			
			this.log(LoggerWeb.METODO_ERROR_SYS, log);
		}
	}
	
	/**
	 * Metodo que hace el log 
	 * 
	 * @param tipo El tipo de log que vamos a hacer 
	 */
	private void log(final int pTipo, final LogBean pLog) {

		BasicConfigurator.configure(); 
	 
		switch (pTipo) {
			case LoggerWeb.METODO_COMENTARIO:
			
			//	logi.warn(pLog.getElemento() + pLog.getComentario());

				logi.getAppender(pLog.getElemento() + pLog.getComentario());
				break;
				
			case LoggerWeb.METODO_ELEMENTO_PROCESADO:
							 
		//		logi.debug(pLog.getElemento() + pLog.getOperacion() + pLog.getAccion() + pLog.getValor());
				
				break;
				
			case LoggerWeb.METODO_ERROR_BBDD:
				 	
			//	logi.error(pLog.getElemento() + pLog.getAccion() + pLog.getCodigoError()+ pLog.getComentario());
				
				break;
				
			case LoggerWeb.METODO_ERROR_FICHERO:
 			
		//		logi.error(pLog.getElemento() + pLog.getFichero() + pLog.getCodigoError() + pLog.getComentario());
				
				
				break;
				
			case LoggerWeb.METODO_ERROR_LOGICA:
				
		//		logi.error(pLog.getElemento() + pLog.getCodigoError() + pLog.getComentario());
				
				break;
				
			case LoggerWeb.METODO_ERROR_SYS:
				
		//		logi.error(pLog.getCodigo()+ pLog.getComentario());
				
			 				
				
				break;
				
			case LoggerWeb.METODO_FIN_PROCESO:
			 				
	//			logi.debug(pLog.getElemento()+ pLog.getComentario());
				
			 	
				break;
			
			case LoggerWeb.METODO_INICIO_PROCESO:
			
				
	//			logi.debug(pLog.getElemento()+ pLog.getComentario());
		 
				break;
		}
 
	}

	/**
	 * @return
	 */
	private boolean isLogEnable() {
		return (StringUtils.isNotBlank(this.configParams.getLogs()) && LoggerWeb.CONSTANT_SI.equals(this.configParams.getLogs()));
	}
	
	/**
	 * @return the configParams
	 */
	public ConfigParams getConfigParams() {
		return this.configParams;
	}

	/**
	 * @param pConfigParams the configParams to set
	 */
	public void setConfigParams(ConfigParams pConfigParams) {
		this.configParams = pConfigParams;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		
	
		return this.path;
	}

	/**
	 * @param pPath the path to set
	 * @throws URISyntaxException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void setPath(String pPath) throws URISyntaxException, FileNotFoundException, IOException {

		
		
		//  Primero de todo obtenemos el Class Loader de thread actual:
		//	Esto nos retorna un objeto con la siguiente información:
		//
		//	context: /MyApp
		//	delegate: false
		//  repositories:
		//	/WEB-INF/classes/
				
		
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		
    	//	Obtenemos el path del fichero recuperándolo como recurso y 
	    // 	generando el path absoluto del fichero en el sistema.
		
		String path = loader.getResource("logs-config.properties").toURI().getPath();		
		
       // Ahora ya podemos cargar el fichero usando el path recuperado. Por ejemplo si queremos cargar un fichero de propiedades 
       // .properties es tan simple como poner las siguientes líneas.
	   // Properties props = new Properties();
   	  //props.load(new  FileInputStream (path));
	
    
		
	//	this.path = pPath;
	
    	this.path = path;

	}

	
	/**
	 * @author rosario.argentina
	 *
	 */
	public interface OPERATIONS {
		String SELECTALL 	= "SELECTALL";
		String SELECT 		= "SELECT";
		String FILTER 		= "FILTER";
		String BUSCAR 		= "BUSCAR";
		String COUNT 		= "COUNT";
		String INSERT 		= "INSERT";
		String DELETE 		= "DELETE";
		String UPDATE 		= "UPDATE";
	}
}
