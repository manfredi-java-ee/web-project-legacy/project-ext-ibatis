 




 




 



package arg.ros.acc.commons.logs;

/**
 * @author rosario.argentina
 *
 */
public class LogBean {

	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs 
	 */
	private String elemento = null;
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private String accion = null;
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private String operacion = null;
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private String valor = null;
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private String servicio = null; 
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private int codigoError = 0;
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private String codigo = null;
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private String fichero = null;
	
	/**
	 * Variables que almacenan parametros que utilizan los metodos de logs
	 */
	private String comentario = null;

	/**
	 * @return the elemento
	 */
	public String getElemento() {
		return this.elemento;
	}

	/**
	 * @param pElemento the elemento to set
	 */
	public void setElemento(String pElemento) {
		this.elemento = pElemento;
	}

	/**
	 * @return the accion
	 */
	public String getAccion() {
		return this.accion;
	}

	/**
	 * @param pAccion the accion to set
	 */
	public void setAccion(String pAccion) {
		this.accion = pAccion;
	}

	/**
	 * @return the operacion
	 */
	public String getOperacion() {
		return this.operacion;
	}

	/**
	 * @param pOperacion the operacion to set
	 */
	public void setOperacion(String pOperacion) {
		this.operacion = pOperacion;
	}

	/**
	 * @return the valor
	 */
	public String getValor() {
		return this.valor;
	}

	/**
	 * @param pValor the valor to set
	 */
	public void setValor(String pValor) {
		this.valor = pValor;
	}

	/**
	 * @return the servicio
	 */
	public String getServicio() {
		return this.servicio;
	}

	/**
	 * @param pServicio the servicio to set
	 */
	public void setServicio(String pServicio) {
		this.servicio = pServicio;
	}

	/**
	 * @return the codigoError
	 */
	public int getCodigoError() {
		return this.codigoError;
	}

	/**
	 * @param pCodigoError the codigoError to set
	 */
	public void setCodigoError(int pCodigoError) {
		this.codigoError = pCodigoError;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return this.codigo;
	}

	/**
	 * @param pCodigo the codigo to set
	 */
	public void setCodigo(String pCodigo) {
		this.codigo = pCodigo;
	}

	/**
	 * @return the fichero
	 */
	public String getFichero() {
		return this.fichero;
	}

	/**
	 * @param pFichero the fichero to set
	 */
	public void setFichero(String pFichero) {
		this.fichero = pFichero;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return this.comentario;
	}

	/**
	 * @param pComentario the comentario to set
	 */
	public void setComentario(String pComentario) {
		this.comentario = pComentario;
	}

}
