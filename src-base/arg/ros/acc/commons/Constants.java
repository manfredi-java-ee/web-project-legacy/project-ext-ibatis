 




 




 



package arg.ros.acc.commons;


/**
 * @author rosario.argentina
 *
 */
public interface Constants {

	/**
	 * @author rosario.argentina
	 *
	 */
    interface ACCROS {
        String APP_NAME = "SIMAF";
		String APP_USUARIO = "APP_USUARIO";
		String APP_MENU = "APP_MENU";
		String APP_MENU_SELECTEDITEM = "selectedItem";
		String APP_SECURE_ROOT_DIRECTORY = "/app/";
		String APP_SECURE_ACCESS = "/app/startApp.do?method=viewApplication";
		String APP_ACTION_SUFIX = ".do";
		String APP_REJECT_URL = "/welcome.do?method=view";
		
		String APP_LICENSE_AGREMENT = "licenseAgreement";
		
		String APP_CHARSET_ENCODING_ISO88591 = "ISO-8859-1";
		String APP_CHARSET_ENCODING_UTF8 = "UTF8";
		String APP_JSON_CONTENT_TYPE = "text/json;charset=utf-8";

		String HEADER_XREQUESTEDWITH = "X-Requested-With";
		String XMLHTTP_REQUEST = "XMLHttpRequest";
		
		String APP_CONTENT_TYPE = "Content-type:";
		String APP_CONTENT_DISPOSITION = "Content-Disposition";
		
        String APP_REPORTS_PACKAGE = "/arg/ros/acc/reports/";
		String APP_REPORTS_EXTENSION = ".jrxml";
		String APP_COMPILED_REPORTS_EXTENSION = ".jasper";
		
		int APP_MENU_LONG_DETALLE = 46;
		double VALOR_EURO = 166.386;
		
		final String ENTIDAD_MOD_COBRO = "modCobro";
		final String ENTIDAD_PLANLOLLEFAC = "PlanLoLleFac";
		final String MAPEO_COLUMNAS = "MapeoColumnas";
//#ADC#060111#TDE#312512#INICIO
		final String GRILLA_COMPEMPR = "CompEmpr";
		final String GRILLA_COMPABON = "CompAbon";
		final String COLUMN_TOTCOMPROM = "tot_comprom";
//#ADC#060111#TDE#312512#FIN
		final String ENTIDAD_PGATARMENSU = "pgaTarMensu";
		final String ENTIDAD_TAR_PRIMLINE = "tar_primline";
	}
	
	/**
	 * @author rosario.argentina
	 *
	 */
	interface SCOPES {
		int REQUEST_SCOPE = 0;
		int SESSION_SCOPE = 1;
		int THREAD_LOCAL_SCOPE = 2;
	}
	
	/**
	 * @author rosario.argentina
	 *
	 */
	interface PAGING {
		final String START = "start";
		final String LIMIT = "limit";
		final int SORT_DESC = 0;
		final int SORT_ASC = 1;
		final int GRID_RECORDS_PER_PAGE = 15;
	}
	
	/**
	 * @author rosario.argentina
	 *
	 */
	interface JSON {
		String JSON_RESPONSE = "jsonResponse";
	}
	
	/**
	 * @author rosario.argentina
	 *
	 */
	interface CONFIG {
		String CONSTANT_NEWLINE = "\n";
		String CONSTANT_SEPARATOR = " - ";
		String CONSTANT_BLANK = " ";
		String CONSTANT_SI = "SI";
		String CONSTANT_SI_CHARACTER = "S";
		String CONSTANT_NO_CHARACTER = "N";
		String CONSTANT_POINT = ".";
		String CONSTANT_UNDERSCORE = "_";
		String SLASH = "/";
		String CONSTANT_STRING_DELIM = ",";
	}

	/**
	 * @author rosario.argentina
	 *
	 */
	interface ACTIONS {
		String SUCCESS = "success";
		String FAILURE = "failure";
	}

	/**
	 * @author rosario.argentina
	 *
	 */
	public interface OPERATIONS {
		String SELECTALL 	= "SELECTALL";
		String SELECT 		= "SELECT";
		String FILTER 		= "FILTER";
		String COUNT 		= "COUNT";
		String INSERT 		= "INSERT";
		String DELETE 		= "DELETE";
		String UPDATE 		= "UPDATE";
		String LOGIN 		= "LOGINUSER";
		String LOGOUT 		= "LOGOUTUSER";
		String MAX 			= "MAX";
	}
	
	/**
	 * @author rosario.argentina
	 *
	 */
	public interface BBDD_ERRORS {
		String DRIVER_NOT_FOUND	= "DRIVER_NOT_FOUND";
		String DRIVER_EXCEPTION	= "DRIVER_EXCEPTION";
		String CONNECTION_EXCEPTION	= "CONNECTION_EXCEPTION";
		
		String SELECTALL_MSG = "Error al realizar la consulta a la BD: (selectAll)";
		String SELECT_MSG	 = "Error al realizar la consulta a la BD: (select)";
		String FILTER_MSG	 = "Error al realizar la consulta a la BD: (filtrar)";
		String COUNT_MSG 	 = "Error al realizar la consulta a la BD: (count)";
		String INSERT_MSG	 = "Error al realizar el insert en BD: (insert)";
		String DELETE_MSG	 = "Error al realizar el delete en BD: (delete)";
		String UPDATE_MSG	 = "Error al realizar el update en BD: (update)";
		String MAX_MSG 	 	 = "Error al realizar la consulta a la BD: (max)";
	}
	
	/**
	 * @author rosario.argentina
	 *
	 */
	
	public interface TABLAS {
		String PRUEBA	= "Prueba";
	
	}
	
	/**
	 * @author rosario.argentina
	 *
	 */
	
}
