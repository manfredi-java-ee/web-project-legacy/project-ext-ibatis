package arg.ros.acc.commons;

public class Operaciones {

	
	public interface ERRORESBATCH {
		String OBTENER 		       = "SELECTALL";
		String OBTENERFILTRO   	   = "SELECT";
		String OBTENERSUBPROCESOS  = "FILTER";
		String COUNT 		= "COUNT";
		String INSERTAR 	= "INSERT";
		String ELIMINAR 	= "DELETE";
		String ACTUALIZAR  	= "UPDATE";
		String LOGIN 		= "LOGINUSER";
		String LOGOUT 		= "LOGOUTUSER";
		String MAX 			= "MAX";
		String TOTAL          = "TOTAL";
		String TOTALSUBPROCESO          = "TOTALSUBPROCESO";
	}
	
	
	public interface ERRORPROCESO {
		String OBTENER 		       = "SELECTALL";
		String OBTENERFILTRO   	   = "SELECT";
		String OBTENERSUBPROCESOS  = "FILTER";
		String COUNT 		= "COUNT";
		String INSERTAR 	= "INSERT";
		String ELIMINAR 	= "DELETE";
		String ACTUALIZAR  	= "UPDATE";
		String LOGIN 		= "LOGINUSER";
		String LOGOUT 		= "LOGOUTUSER";
		String MAX 			= "MAX";
		String TOTAL          = "TOTAL";
		String TOTALSUBPROCESO          = "TOTALSUBPROCESO";
	
	};
	
	
	
}
