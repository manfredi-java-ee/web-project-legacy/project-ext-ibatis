 



 




 


package arg.ros.acc.daos.commons;

import javax.sql.DataSource;

import arg.ros.acc.commons.config.ConfigParams;
import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.commons.messages.MessageAdapter;

/**
 * @author rosario.argentina
 *
 */
public abstract class BaseDAO {

	/**
	 * 
	 */
	protected LoggerWeb logger;

	/**
	 * 
	 */
	protected MessageAdapter messages;

	/**
	 * 
	 */
	protected ConfigParams configParams;
	
	/**
	 * 
	 */
	protected DataSource dataSource;
	
	/**
	 * @param pLogger the logger to set
	 */
	public void setLogger(LoggerWeb pLogger) {
		this.logger = pLogger;
	}

	/**
	 * @param pMessages the messages to set
	 */
	public void setMessages(MessageAdapter pMessages) {
		this.messages = pMessages;
	}

	/**
	 * @param pConfigParams the configParams to set
	 */
	public void setConfigParams(ConfigParams pConfigParams) {
		this.configParams = pConfigParams;
	}

	/**
	 * @param pDataSource the dataSource to set
	 */
	public void setDataSource(DataSource pDataSource) {
		this.dataSource = pDataSource;
	}

}
