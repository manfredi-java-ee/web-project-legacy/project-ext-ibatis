 




 




 



package arg.ros.acc.daos.commons;

import java.util.List;
import java.util.Map;

import com.ibatis.sqlmap.engine.mapping.result.ResultMap;

import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public interface ExportDAO {

	/**
	 * @param pGridName
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final String pGridName, final Map pFilter) throws DAOException;
	
	/**
	 * @param pStatement
	 * @return
	 */
	ResultMap getResultMap(String pStatement);
}
