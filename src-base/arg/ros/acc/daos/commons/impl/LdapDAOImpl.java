 




 




 



package arg.ros.acc.daos.commons.impl;

import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPException;

import org.springframework.ldap.core.LdapTemplate;

import arg.ros.acc.daos.commons.LdapDAO;
import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 * 
 */
public class LdapDAOImpl extends LdapTemplate implements LdapDAO {

	/**
	 * Host
	 */
	private String ldapHost;
	
	/**
	 * Port
	 */
	private int ldapPort;
	
	/**
	 * ConnectionString.
	 */
	private String ldapConnectionString;
	
	/**
	 * @param pUsuario
	 * @param pPassword
	 * @return
	 * @throws DAOException
	 */
	public boolean isUserSpring(String pUsuario, String pPassword) throws DAOException {
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isUser(String pUsuario, String pPassword) throws DAOException {
		LDAPConnection connection = new LDAPConnection();
		
		try {
			connection.connect(this.ldapHost, this.ldapPort);
			
		} catch (LDAPException e) {
			throw new DAOException(0, e);
		}
		
		String ldapUser = "uid=" + pUsuario + "," + this.ldapConnectionString;
		
		try {
			connection.authenticate(ldapUser, pPassword);
		} catch (LDAPException e) {
			return false;
			
		} finally {
			if ((connection != null) && connection.isConnected()) {
				try {
					connection.disconnect();
				} catch (netscape.ldap.LDAPException e) {}
			}
		}
		return true;
	}

	/**
	 * @param pLdapHost the ldapHost to set
	 */
	public void setLdapHost(String pLdapHost) {
		this.ldapHost = pLdapHost;
	}

	/**
	 * @param pLdapPort the ldapPort to set
	 */
	public void setLdapPort(int pLdapPort) {
		this.ldapPort = pLdapPort;
	}

	/**
	 * @param pLdapConnectionString the ldapConnectionString to set
	 */
	public void setLdapConnectionString(String pLdapConnectionString) {
		this.ldapConnectionString = pLdapConnectionString;
	}
	
}
