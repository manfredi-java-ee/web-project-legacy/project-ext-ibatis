 




 




 



package arg.ros.acc.daos.commons.impl;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.ibatis.sqlmap.engine.impl.ExtendedSqlMapClient;
import com.ibatis.sqlmap.engine.mapping.result.ResultMap;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.daos.commons.ExportDAO;
import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public class ExportIBatisDAOImpl extends IBatisBaseDAO implements ExportDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select(final String pGridName, final Map pFilter) throws DAOException {
		String exportQuery = "obtenerFiltro"+pGridName;
		
		try {
			return getSqlMapClientTemplate().queryForList(exportQuery, pFilter);
		
		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, Constants.OPERATIONS.FILTER, e);
			throw new DAOException("Error al filtrar los datos: filter.", e);
		}
	}

	/**
	 * @param pStatement
	 * @return
	 */
	public ResultMap getResultMap(String pStatement) {
		final String statement = "obtenerFiltro"+pStatement;
		final ExtendedSqlMapClient esmc = (ExtendedSqlMapClient) this.getSqlMapClient();
		
		return esmc.getMappedStatement(statement).getResultMap();
	}
}
