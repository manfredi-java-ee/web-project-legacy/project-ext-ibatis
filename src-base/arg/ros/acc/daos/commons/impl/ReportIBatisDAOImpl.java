 




 




 



package arg.ros.acc.daos.commons.impl;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.commons.ReportDAO;
import arg.ros.acc.exceptions.DAOException;

/**
 * @author jc053 - 25/03/2009
 *
 */
public class ReportIBatisDAOImpl extends IBatisBaseDAO implements ReportDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select(String pReportName, final Map pFilter) throws DAOException {
		final String reportQuery = "report" + pReportName;
		this.logger.Comentario(ReportIBatisDAOImpl.class, reportQuery);

		try {
			return getSqlMapClientTemplate().queryForList(reportQuery, pFilter);

		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new DAOException("Error al filtrar los datos: select.", e);
		}
	}
}
