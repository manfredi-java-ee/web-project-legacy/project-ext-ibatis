 




 




 



package arg.ros.acc.daos.commons.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.springframework.dao.DataAccessException;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.commons.LupaDAO;
import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public class LupaIBatisDAOImpl extends IBatisBaseDAO implements LupaDAO {

	/**
	 * 
	 */
	private static final String CONSTANT_LUPA = "lupa";
	
	/**
	 * 
	 */
	private static final String CONSTANT_TOTAL = "total";

	/**
	 * {@inheritDoc}
	 */
	public synchronized int count(final String pGridName) throws DAOException {
		return count(pGridName, MapUtils.EMPTY_MAP);
	}

	/**
	 * {@inheritDoc}
	 */
	public synchronized int count(final String pGridName, final Map pProperties) throws DAOException {
		String lupaQuery = LupaIBatisDAOImpl.CONSTANT_TOTAL+pGridName;
		Integer total = null;
		try {
			total = (Integer) getSqlMapClientTemplate().queryForObject(lupaQuery, pProperties);
		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException("Error al obtener los datos: count", e);
		}
		return total.intValue();
	}

	/**
	 * {@inheritDoc}
	 */
	public synchronized List select(final String pGridName, final int pStart, final int pLimit) throws DAOException {
		return select(pGridName, pStart, pLimit, new HashMap());
	}

	/**
	 * {@inheritDoc}
	 */
	public synchronized List select(final String pGridName, final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		String lupaQuery = LupaIBatisDAOImpl.CONSTANT_LUPA + pGridName;
		try {
			return getSqlMapClientTemplate().queryForList(lupaQuery, pFilter, pStart, pLimit);
		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, Constants.OPERATIONS.FILTER, e);
			throw new DAOException("Error al filtrar los datos: filter.", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public synchronized List select(final String pGridName) throws DAOException {
		String lupaQuery = LupaIBatisDAOImpl.CONSTANT_LUPA + pGridName;
		try {
			return getSqlMapClientTemplate().queryForList(lupaQuery);	
		
		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException("Error al obtener los datos: selectAll", e);
		}
	}

}
