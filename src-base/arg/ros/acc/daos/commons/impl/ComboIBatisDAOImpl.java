 




 




 



package arg.ros.acc.daos.commons.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.springframework.dao.DataAccessException;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.daos.commons.ComboDAO;
import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public class ComboIBatisDAOImpl extends IBatisBaseDAO implements ComboDAO {

	/**
	 * 
	 */
	private static final String CONSTANT_COMBO = "combo";
	
	/**
	 * 
	 */
	private static final String CONSTANT_TOTAL = "total";

	/**
	 * {@inheritDoc}
	 */
	public int count(final String pComboName) throws DAOException {
		return this.count(pComboName, MapUtils.EMPTY_MAP);
	}

	/**
	 * {@inheritDoc}
	 */
	public int count(final String pComboName, final Map pProperties) throws DAOException {
		String comboQuery = ComboIBatisDAOImpl.CONSTANT_TOTAL+pComboName;
		Integer total = null;
		try {
			total = (Integer) getSqlMapClientTemplate().queryForObject(comboQuery, pProperties);
		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException("Error al obtener los datos: count", e);
		}
		return total.intValue();
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final String pComboName, final int pStart, final int pLimit) throws DAOException {
		return this.select(pComboName, pStart, pLimit, new HashMap());
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final String pComboName, final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		String comboQuery = ComboIBatisDAOImpl.CONSTANT_COMBO + pComboName;
		try {
			return getSqlMapClientTemplate().queryForList(comboQuery, pFilter, pStart, pLimit);
		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, Constants.OPERATIONS.FILTER, e);
			throw new DAOException("Error al filtrar los datos: filter.", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final String pComboName) throws DAOException {
		return this.select(pComboName, new HashMap());
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(String pComboName, Map pFilter) throws DAOException {
		String comboQuery = ComboIBatisDAOImpl.CONSTANT_COMBO + pComboName;
		try {
			return getSqlMapClientTemplate().queryForList(comboQuery, pFilter);	
		
		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException("Error al obtener los datos: selectAll", e);
		}
	}
}
