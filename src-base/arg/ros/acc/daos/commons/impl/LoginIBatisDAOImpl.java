 




 




 



package arg.ros.acc.daos.commons.impl;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.commons.LoginDAO;
import arg.ros.acc.daos.commons.Operaciones;
import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public class LoginIBatisDAOImpl extends IBatisBaseDAO implements LoginDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select(String pOperation, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(pOperation, pFilter);

		} catch (DataAccessException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			
			throw new DAOException("Error al filtrar los datos: filter.", e);
		}
	}
}
