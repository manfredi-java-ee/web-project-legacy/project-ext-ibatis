 




 




 



package arg.ros.acc.daos.commons;

import java.util.List;
import java.util.Map;

import arg.ros.acc.exceptions.DAOException;

/**
 * @author jc053 - 25/03/2009
 *
 */
public interface ReportDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	public List select(final String reportName, final Map pFilter) throws DAOException;
	
}
