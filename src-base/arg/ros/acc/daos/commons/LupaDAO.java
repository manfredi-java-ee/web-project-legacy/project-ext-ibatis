 




 




 



package arg.ros.acc.daos.commons;

import java.util.List;
import java.util.Map;

import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public interface LupaDAO {

	/**
	 * @param pGridName
	 * @return
	 * @throws DAOException
	 */
	@SuppressWarnings("unchecked")
	List select(final String pGridName) throws DAOException;

	/**
	 * @param pGridName
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final String pGridName, final int pStart, final int pLimit) throws DAOException;
    
	/**
	 * @param pGridName
	 * @param pStart
	 * @param pLimit
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final String pGridName, final int pStart, final int pLimit, final Map pFilter) throws DAOException;

	/**
	 * @param pGridName
	 * @return
	 * @throws DAOException
	 */
	int count(final String pGridName) throws DAOException;
	
	/**
	 * @param pGridName
	 * @param pProperties
	 * @return
	 * @throws DAOException
	 */
	int count(final String pGridName, final Map pProperties) throws DAOException;
	
}
