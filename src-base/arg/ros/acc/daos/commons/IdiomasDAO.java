 




 




 



package arg.ros.acc.daos.commons;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.commons.IdiomasVO;

/**
 * @author rosario.argentina
 *
 */
public interface IdiomasDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param IdiomasVO
	 * @throws DAOException
	 */
	void insert(final IdiomasVO pIdiomasVO) throws DAOException;
    
	/**
	 * @param pIdiomasVO
	 * @throws DAOException
	 */
	void delete(final IdiomasVO pIdiomasVO) throws DAOException;
    
	/**
	 * @param pIdiomasVO
	 * @throws DAOException
	 */
	void update(final IdiomasVO pIdiomasVO) throws DAOException;

	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
}
