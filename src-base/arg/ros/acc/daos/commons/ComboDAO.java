 




 




 



package arg.ros.acc.daos.commons;

import java.util.List;
import java.util.Map;

import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public interface ComboDAO {

	/**
	 * @param pComboName
	 * @return
	 * @throws DAOException
	 */
	List select(final String pComboName) throws DAOException;
	
	/**
	 * @param pComboName
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final String pComboName, final Map pFilter) throws DAOException;

	/**
	 * @param pComboName
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final String pComboName, final int pStart, final int pLimit) throws DAOException;
    
	/**
	 * @param pComboName
	 * @param pStart
	 * @param pLimit
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final String pComboName, final int pStart, final int pLimit, final Map pFilter) throws DAOException;

	/**
	 * @param pComboName
	 * @return
	 * @throws DAOException
	 */
	int count(final String pComboName) throws DAOException;
	
	/**
	 * @param pComboName
	 * @param pProperties
	 * @return
	 * @throws DAOException
	 */
	int count(final String pComboName, final Map pProperties) throws DAOException;
	
}
