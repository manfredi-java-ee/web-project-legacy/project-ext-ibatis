 




 




 



package arg.ros.acc.daos.commons;

import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public interface LdapDAO {

	/**
	 * @param pUsuario
	 * @param pPassword
	 * @return
	 * @throws DAOException
	 */
	public boolean isUser(String pUsuario, String pPassword) throws DAOException;
	
	/**
	 * @param pUsuario
	 * @param pPassword
	 * @return
	 * @throws DAOException
	 */
	public boolean isUserSpring(String pUsuario, String pPassword) throws DAOException;
	
}
