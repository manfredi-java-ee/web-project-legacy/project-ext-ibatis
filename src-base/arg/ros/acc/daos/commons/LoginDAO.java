 




 




 



package arg.ros.acc.daos.commons;

import java.util.List;
import java.util.Map;

import arg.ros.acc.exceptions.DAOException;

/**
 * @author rosario.argentina
 *
 */
public interface LoginDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	public List select(final String pOperation, final Map pFilter) throws DAOException;
	
}
