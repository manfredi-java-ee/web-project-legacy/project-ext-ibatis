 




 




 



package arg.ros.acc.daos.commons;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import arg.ros.acc.commons.config.ConfigParams;
import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.commons.messages.MessageAdapter;

/**
 * @author rosario.argentina
 *
 */
public abstract class IBatisBaseDAO extends SqlMapClientDaoSupport {

	/**
	 * 
	 */
	private ConfigParams configParams;
	
	/**
	 * 
	 */
	protected LoggerWeb logger;

	/**
	 * 
	 */
	private MessageAdapter messages;

	/**
	 * @return the logger
	 */
	public LoggerWeb getLogger() {
		return this.logger;
	}

	/**
	 * @param pLogger the logger to set
	 */
	public void setLogger(LoggerWeb pLogger) {
		this.logger = pLogger;
	}

	/**
	 * @return the messages
	 */
	public MessageAdapter getMessages() {
		return this.messages;
	}

	/**
	 * @param pMessages the messages to set
	 */
	public void setMessages(MessageAdapter pMessages) {
		this.messages = pMessages;
	}

	/**
	 * @return the configParams
	 */
	public ConfigParams getConfigParams() {
		return this.configParams;
	}

	/**
	 * @param pConfigParams the configParams to set
	 */
	public void setConfigParams(ConfigParams pConfigParams) {
		this.configParams = pConfigParams;
	}
	
}
