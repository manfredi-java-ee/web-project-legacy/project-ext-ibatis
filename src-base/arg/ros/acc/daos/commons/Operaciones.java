package arg.ros.acc.daos.commons;

/**
 * @author rosario.argentina
 *
 */
public interface Operaciones {

	/**
	 * Interface que define las operaciones que se pueden realizar sobre el DAO de 
	 * Login.
	 * 
	 * @author rosario.argentina
	 *
	 */
	interface LOGIN {
		public static String OBTENERRECURSOS = "obtenerRecursos";
		public static String OBTENERCODIGOUSUARIO = "obtenerCodigoUsuario";
	}

	/**
	 * Interface que define las operaciones que se pueden realizar sobre el DAO de 
	 * Lupas GENERICAS.
	 * 
	 * @author jc053 - 26/01/09
	 *
	 */
	interface LUPAS {
		public static String OBTENERCONFACTAGRU      = "getConFactAgru";
		public static String OBTENERCONFACTURABLE    = "getConFacturable";
		public static String OBTENERPLANFACTURACION  = "getPlanFacturacion";
		public static String OBTENERLOCLLAMADAS      = "getLocLlamadas";
		public static String OBTENERBANCOS			 = "getBancos";
		public static String OBTENERTIPOIMPUESTO     = "getTipoImpuesto";
		public static String OBTENERIDIOMAS		     = "getIdiomas";
		public static String OBTENERTIPOSFORMATOS    = "getTiposFormato";
		public static String OBTENERTIPOSFACTURA     = "getTiposFactura";
		public static String OBTENERTRAFICO          = "getAgruTrafico";
		public static String OBTENERTIPOCLIENTE      = "getTipoCliente";
		public static String OBTENERSERVGRAL         = "getServGral";
		public static String OBTENERFECEFECTIVIDAD   = "getFechaEfectividad";
		public static String OBTENERFAMIIMPU     	 = "getFamiImpu";
		public static String OBTENERPLANTARIFICACION = "getPlanTarificacion";
		public static String OBTENERSERVICIOS        = "getServicios";
		public static String OBTENERTIPOSSERVICIO    = "getTiposServicio";
		public static String OBTENERCONCIERTO    	 = "getConcierto";
		public static String OBTENERNOMBRES          = "getNombres";
		public static String OBTENERVENTASESC1   	 = "getVentasEsc1";
		public static String OBTENERVENTASESC2   	 = "getVentasEsc2";
		public static String OBTENERVENTASESC3     	 = "getVentasEsc3";
		public static String OBTENERVENTASESC4     	 = "getVentasEsc4";
		public static String OBTENEROPERESP     	 = "getOperEsp";
		public static String OBTENERVALOR  	 	  	 = "getValor";
		public static String OBTENERIDENTIFICADOR    = "getIdentificador";
		public static String OBTENERFAMICOMP    	 = "getFamiComp";
		public static String OBTENERIMPOMAXCOMP      = "getImpoMaxComp";
		public static String OBTENERTIPOCONCEPFACTU  = "getTipoConcepFactu";
		public static String OBTENERPLANAGRUPATRAFI  = "getPlanAgruTrafico";
	}
	
	/**
	 * Interface que define las operaciones que se pueden realizar sobre el DAO de 
	 * Idiomas.
	 * 
	 * @author rosario.argentina
	 *
	 */
	interface IDIOMAS {
		public static String INSERTAR 		= "insertarIdiomas";
		public static String ACTUALIZAR 	= "actualizarIdiomas";
		public static String ELIMINAR 		= "eliminarIdiomas";
		public static String OBTENER 		= "obtenerIdiomas";
		public static String OBTENERFILTRO 	= "obtenerFiltroIdiomas";
		public static String TOTAL	 		= "totalIdiomas";
	}
}
