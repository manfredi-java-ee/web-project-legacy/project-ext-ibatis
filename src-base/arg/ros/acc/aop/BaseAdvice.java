 




 




 



package arg.ros.acc.aop;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;

import arg.ros.acc.commons.config.ConfigParams;
import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.commons.messages.MessageAdapter;

/**
 * @author rosario.argentina
 *
 */
public abstract class BaseAdvice {
	
	/**
	 * 
	 */
	private static final String VENTANA = "VENTANA";
	
	/**
	 * 
	 */
	protected ConfigParams configParams;

	/**
	 * 
	 */
	protected MessageAdapter messageAdapter;
	
	/**
	 * 
	 */
	protected LoggerWeb logger;
	
	/**
	 * @param pConfigParams the configParams to set
	 */
	public void setConfigParams(ConfigParams pConfigParams) {
		this.configParams = pConfigParams;
	}

	/**
	 * @param pMessageAdapter the messageAdapter to set
	 */
	public void setMessageAdapter(MessageAdapter pMessageAdapter) {
		this.messageAdapter = pMessageAdapter;
	}

	/**
	 * @param pLogger the logger to set
	 */
	public void setLogger(LoggerWeb pLogger) {
		this.logger = pLogger;
	}

	/**
	 * @param pJoinPoint
	 * @return
	 */
	public Properties getVentana(final JoinPoint pJoinPoint) {
		Properties properties = new Properties(); 
		properties.setProperty(BaseAdvice.VENTANA, pJoinPoint.getTarget().getClass().getName());
		
		return properties;
	}
	
}
