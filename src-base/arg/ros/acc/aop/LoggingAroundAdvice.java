package arg.ros.acc.aop;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;

import arg.ros.acc.commons.Constants;

/**
 * @author rosario.argentina
 *
 */
public class LoggingAroundAdvice extends BaseAdvice {

	/**
	 * @return
	 */
	public Object around(final ProceedingJoinPoint pPjp) throws Throwable {
		final Object[] arguments = pPjp.getArgs();
		final Object target = pPjp.getTarget();
		final Signature signature = pPjp.getSignature();
		
		this.logger.InicioProceso(target, "Invocando metodo: " + signature.toShortString());
		
		Object result = null;
		try {
			final String callComment = getCallComment(arguments, target, signature);		
			this.logger.Comentario(target, callComment);
			
			result = pPjp.proceed();
		
			final String resultComment = getResultComment(result);
			if (resultComment != null) {
				this.logger.Comentario(target, resultComment);	
			}
						
		} catch (Throwable e) {
			throw e;
		}
		
		this.logger.FinProceso(target, "Finalizando la invocacion del metodo: " + signature.toShortString());

		return result;
	}

	/**
	 * @param arguments
	 * @param target
	 * @param signature
	 */
	private String getCallComment(final Object[] arguments, final Object target, final Signature signature) {
		StringBuffer comment = new StringBuffer();
		
		comment.append(Constants.CONFIG.CONSTANT_NEWLINE).append("[").append(Constants.CONFIG.CONSTANT_NEWLINE);
		comment.append("Target: ").append(target.getClass().getCanonicalName()).append(Constants.CONFIG.CONSTANT_NEWLINE);
		comment.append("Method: ").append(signature.toShortString()).append(Constants.CONFIG.CONSTANT_NEWLINE);
		comment.append("Arguments: ").append(ToStringBuilder.reflectionToString(arguments)).append(Constants.CONFIG.CONSTANT_NEWLINE);
		comment.append("]").append(Constants.CONFIG.CONSTANT_NEWLINE);
		
		return comment.toString();
	}

	/**
	 * @param result
	 */
	private String getResultComment(Object pResult) {
		final StringBuffer comment = new StringBuffer();
		
		if (pResult == null) {
			return null;
		}
		
		comment.append(Constants.CONFIG.CONSTANT_NEWLINE).append("[").append(Constants.CONFIG.CONSTANT_NEWLINE);
		comment.append("Result: ").append(pResult.getClass().getCanonicalName()).append(Constants.CONFIG.CONSTANT_NEWLINE);
		comment.append("ToString: ").append(ToStringBuilder.reflectionToString(pResult)).append(Constants.CONFIG.CONSTANT_NEWLINE);
		comment.append("]");
		
		return comment.toString();
	} 
	
}
