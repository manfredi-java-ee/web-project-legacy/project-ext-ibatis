package arg.ros.acc.aop;

import java.sql.SQLException;

import netscape.ldap.LDAPException;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;

/**
 * @author rosario.argentina
 *
 */
public class ServiceAfterThrowingAdvice extends BaseAdvice {

	/**
	 * 
	 */
	private static final String SQL_PREFIX = "SQL-";

	/**
	 * @param pMethod
	 * @param pArgs
	 * @param pTarget
	 * @param pException
	 * @throws Exception 
	 */
	public void afterThrowing(DAOException pThrowable) throws Throwable {
		if (pThrowable.getE() instanceof LDAPException) {
			LDAPException ldapException = (LDAPException) pThrowable.getE();
			throw new ServiceException("Error al conectarse con LDAP", ldapException);
		}
		final SQLException sqlException = this.getSQLException(pThrowable);

		int errorCode = sqlException.getErrorCode();
		
		final String prefix = ServiceAfterThrowingAdvice.SQL_PREFIX;
		final String code = prefix + errorCode;

		final String message = this.messageAdapter.get(code);
		
		throw new ServiceException(message, pThrowable);
	}
	
	/**
	 * @param pException
	 * @return
	 */
	private SQLException getSQLException(Throwable pThrowable) {
		Throwable throwable = pThrowable.getCause();
		if (!(throwable instanceof SQLException)) {
			throwable = this.getSQLException(pThrowable.getCause());
		} 
		return (SQLException) throwable;
	}

}
