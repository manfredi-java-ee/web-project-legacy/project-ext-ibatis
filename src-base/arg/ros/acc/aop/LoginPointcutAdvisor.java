 




 




 



package arg.ros.acc.aop;

import java.lang.reflect.Method;

import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;

/**
 * @author rosario.argentina
 *
 */
public class LoginPointcutAdvisor extends StaticMethodMatcherPointcutAdvisor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2414699343306619041L;

	/**
	 * {@inheritDoc}
	 */
	public boolean matches(Method pMethod, Class pClass) {
		return false;
	}

}
