 




 




 



package arg.ros.acc.aop;

import java.lang.reflect.Method;

import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;

/**
 * @author rosario.argentina
 *
 */
public class LogoutPointcutAdvisor extends StaticMethodMatcherPointcutAdvisor {

	/**
	 * 
	 */
	private Class actionClass;
	
	/**
	 * 
	 */
	private String method;
	
	/**
	 * {@inheritDoc}
	 */
	public boolean matches(Method pMethod, Class pClass) {
		final String methodName = pMethod.getName(); 
		return (pClass.equals(this.actionClass) && this.method.equals(methodName));
	}

	/**
	 * @return the actionClass
	 */
	public Class getActionClass() {
		return this.actionClass;
	}

	/**
	 * @param pActionClass the actionClass to set
	 */
	public void setActionClass(Class pActionClass) {
		this.actionClass = pActionClass;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return this.method;
	}

	/**
	 * @param pMethod the method to set
	 */
	public void setMethod(String pMethod) {
		this.method = pMethod;
	}

}
