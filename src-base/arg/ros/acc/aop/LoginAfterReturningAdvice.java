package arg.ros.acc.aop;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.ContextUtils;
import arg.ros.acc.components.menu.Application;
import arg.ros.acc.components.menu.MenuComponent;
import arg.ros.acc.components.menu.Nodo;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public class LoginAfterReturningAdvice extends BaseAdvice {

	/**
	 * 
	 */
	private MenuComponent menuComponent;
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public void afterReturning(JoinPoint pJoinPoint, final Object pReturnValue) throws Throwable {
		final Object[] arguments = pJoinPoint.getArgs();		
		final HttpServletRequest request = (HttpServletRequest) arguments[0];;
		final UsuarioVO usuarioVO = (UsuarioVO) arguments[1];
		
		this.logger.Comentario(this, "CARGA DE PERMISOS DE USUARIO...");
		
		// Si el usuario no tiene permisos...
		if (!((Boolean) pReturnValue)) {
			logger.Comentario(this, "El usuario no tiene permisos.");
			return;
		}
		Application menuApplication = null; 
		try {
			this.logger.Comentario(this, "Parseando fichero de menu.");
			menuApplication = this.menuComponent.read(true);
			logger.Comentario(this, "Fichero de menu inicializado correctamente.");
			
		} catch (Exception e) {
			logger.Comentario(this, "Error al inicializar el fichero del menu. Message: " + e.getMessage());
		}
		
		if (menuApplication != null) {
			
			logger.Comentario(this, "Procesando parametrizacion de menu.");
			
				for (Nodo submenu : menuApplication.getAllSubMenuItems()) {
					if (submenu.getPath() != null) {
						submenu.setRender(true);
						submenu.solveExpressions(menuApplication.getParams());
					}
					for (Nodo submenu2: submenu.getItems()) {
						if (submenu2.getPath() != null) {
							submenu2.setRender(true);
							submenu2.solveExpressions(menuApplication.getParams());
						}
						for (Nodo submenu3: submenu.getItems()) {
							if (submenu3.getPath() != null) {
								submenu3.setRender(true);
								submenu3.solveExpressions(menuApplication.getParams());
							}
						}
					}
				}
		}
		
		menuApplication.removeItem(menuApplication.getItems());
		
		logger.Comentario(this, "Menu de usuario inicializado correctamente.");
		
        ContextUtils.set(Constants.ACCROS.APP_USUARIO, usuarioVO, Constants.SCOPES.THREAD_LOCAL_SCOPE, request);
		this.logger.Comentario(this, "Usuario: " + (usuarioVO == null ? "NULO" : usuarioVO.getUsuario()) +" seteado en scope de thread local.");
		
        ContextUtils.set(Constants.ACCROS.APP_USUARIO, usuarioVO, Constants.SCOPES.SESSION_SCOPE, request);
		this.logger.Comentario(this, "Usuario: " + (usuarioVO == null ? "NULO" : usuarioVO.getUsuario()) +" seteado en scope de sesion.");
		
        ContextUtils.set(Constants.ACCROS.APP_MENU, menuApplication, Constants.SCOPES.SESSION_SCOPE, request);
		this.logger.Comentario(this, "Menu de aplicacion: " + (menuApplication == null ? "NULO" : menuApplication.toString()) + " seteado en scope de session.");
		
		logger.Comentario(this, "Variables de sesion inicializadas.");
	}

	/**
	 * @param pMenuComponent the menuComponent to set
	 */
	public void setMenuComponent(MenuComponent pMenuComponent) {
		this.menuComponent = pMenuComponent;
	}

}
