 




 




 



package arg.ros.acc.vo.commons;

/**
 * @author rosario.argentina
 *
 */
public class ReportConFactuVO extends BaseVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5808820475046483825L;

	/**
	 * 
	 */
	private String cod_ctaMarkt;
	
	/**
	 * 
	 */
	private String des_ctaMarkt;
	
	/**
	 * 
	 */
	private String cod_ctaTesor;
	
	/**
	 * 
	 */
	private String des_ctaTesor;
	
	/**
	 * 
	 */
	private String cod_confactu;
	
	/**
	 * 
	 */
	private String des_confactu;
	
	/**
	 * 
	 */
	private String cfa_devoluci;
	
	/**
	 * 
	 */
	private String des_devoluci;
	
	/**
	 * 
	 */
	private String cfa_descuent;
	
	/**
	 * 
	 */
	private String des_descuent;
	
	/**
	 * 
	 */
	private String ind_imprFech;

	/**
	 * @return the cod_ctaMarkt
	 */
	public String getCod_ctaMarkt() {
		return this.cod_ctaMarkt;
	}

	/**
	 * @param pCod_ctaMarkt the cod_ctaMarkt to set
	 */
	public void setCod_ctaMarkt(String pCod_ctaMarkt) {
		this.cod_ctaMarkt = pCod_ctaMarkt;
	}

	/**
	 * @return the des_ctaMarkt
	 */
	public String getDes_ctaMarkt() {
		return this.des_ctaMarkt;
	}

	/**
	 * @param pDes_ctaMarkt the des_ctaMarkt to set
	 */
	public void setDes_ctaMarkt(String pDes_ctaMarkt) {
		this.des_ctaMarkt = pDes_ctaMarkt;
	}

	/**
	 * @return the cod_ctaTesor
	 */
	public String getCod_ctaTesor() {
		return this.cod_ctaTesor;
	}

	/**
	 * @param pCod_ctaTesor the cod_ctaTesor to set
	 */
	public void setCod_ctaTesor(String pCod_ctaTesor) {
		this.cod_ctaTesor = pCod_ctaTesor;
	}

	/**
	 * @return the des_ctaTesor
	 */
	public String getDes_ctaTesor() {
		return this.des_ctaTesor;
	}

	/**
	 * @param pDes_ctaTesor the des_ctaTesor to set
	 */
	public void setDes_ctaTesor(String pDes_ctaTesor) {
		this.des_ctaTesor = pDes_ctaTesor;
	}

	/**
	 * @return the cod_confactu
	 */
	public String getCod_confactu() {
		return this.cod_confactu;
	}

	/**
	 * @param pCod_confactu the cod_confactu to set
	 */
	public void setCod_confactu(String pCod_confactu) {
		this.cod_confactu = pCod_confactu;
	}

	/**
	 * @return the des_confactu
	 */
	public String getDes_confactu() {
		return this.des_confactu;
	}

	/**
	 * @param pDes_confactu the des_confactu to set
	 */
	public void setDes_confactu(String pDes_confactu) {
		this.des_confactu = pDes_confactu;
	}

	/**
	 * @return the cfa_devoluci
	 */
	public String getCfa_devoluci() {
		return this.cfa_devoluci;
	}

	/**
	 * @param pCfa_devoluci the cfa_devoluci to set
	 */
	public void setCfa_devoluci(String pCfa_devoluci) {
		this.cfa_devoluci = pCfa_devoluci;
	}

	/**
	 * @return the des_devoluci
	 */
	public String getDes_devoluci() {
		return this.des_devoluci;
	}

	/**
	 * @param pDes_devoluci the des_devoluci to set
	 */
	public void setDes_devoluci(String pDes_devoluci) {
		this.des_devoluci = pDes_devoluci;
	}

	/**
	 * @return the cfa_descuent
	 */
	public String getCfa_descuent() {
		return this.cfa_descuent;
	}

	/**
	 * @param pCfa_descuent the cfa_descuent to set
	 */
	public void setCfa_descuent(String pCfa_descuent) {
		this.cfa_descuent = pCfa_descuent;
	}

	/**
	 * @return the des_descuent
	 */
	public String getDes_descuent() {
		return this.des_descuent;
	}

	/**
	 * @param pDes_descuent the des_descuent to set
	 */
	public void setDes_descuent(String pDes_descuent) {
		this.des_descuent = pDes_descuent;
	}

	/**
	 * @return the ind_imprFech
	 */
	public String getInd_imprFech() {
		return this.ind_imprFech;
	}

	/**
	 * @param pInd_imprFech the ind_imprFech to set
	 */
	public void setInd_imprFech(String pInd_imprFech) {
		this.ind_imprFech = pInd_imprFech;
	}

}
