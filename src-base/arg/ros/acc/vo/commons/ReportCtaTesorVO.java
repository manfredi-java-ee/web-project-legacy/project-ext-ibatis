 




 




 



package arg.ros.acc.vo.commons;

/**
 * @author rosario.argentina
 *
 */
public class ReportCtaTesorVO extends BaseVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2588131218648625093L;

	/**
	 * 
	 */
	private String cod_gruTesor;
	
	/**
	 * 
	 */
	private String des_gruTesor;
	
	/**
	 * 
	 */
	private String cod_ctaTesor;
	
	/**
	 * 
	 */
	private String des_ctaTesor;
	
	/**
	 * 
	 */
	private String cta_tesoreri;
	
	/**
	 * 
	 */
	private String cod_tesor1800;
	
	/**
	 * 
	 */
	private String des_tesor1800;
	
	/**
	 * 
	 */
	private String cod_tesorUmts;
	
	/**
	 * 
	 */
	private String des_tesorUmts;

	/**
	 * @return the cod_gruTesor
	 */
	public String getCod_gruTesor() {
		return this.cod_gruTesor;
	}

	/**
	 * @param pCod_gruTesor the cod_gruTesor to set
	 */
	public void setCod_gruTesor(String pCod_gruTesor) {
		this.cod_gruTesor = pCod_gruTesor;
	}

	/**
	 * @return the des_gruTesor
	 */
	public String getDes_gruTesor() {
		return this.des_gruTesor;
	}

	/**
	 * @param pDes_gruTesor the des_gruTesor to set
	 */
	public void setDes_gruTesor(String pDes_gruTesor) {
		this.des_gruTesor = pDes_gruTesor;
	}

	/**
	 * @return the cod_ctaTesor
	 */
	public String getCod_ctaTesor() {
		return this.cod_ctaTesor;
	}

	/**
	 * @param pCod_ctaTesor the cod_ctaTesor to set
	 */
	public void setCod_ctaTesor(String pCod_ctaTesor) {
		this.cod_ctaTesor = pCod_ctaTesor;
	}

	/**
	 * @return the des_ctaTesor
	 */
	public String getDes_ctaTesor() {
		return this.des_ctaTesor;
	}

	/**
	 * @param pDes_ctaTesor the des_ctaTesor to set
	 */
	public void setDes_ctaTesor(String pDes_ctaTesor) {
		this.des_ctaTesor = pDes_ctaTesor;
	}

	/**
	 * @return the cta_tesoreri
	 */
	public String getCta_tesoreri() {
		return this.cta_tesoreri;
	}

	/**
	 * @param pCta_tesoreri the cta_tesoreri to set
	 */
	public void setCta_tesoreri(String pCta_tesoreri) {
		this.cta_tesoreri = pCta_tesoreri;
	}

	/**
	 * @return the cod_tesor1800
	 */
	public String getCod_tesor1800() {
		return this.cod_tesor1800;
	}

	/**
	 * @param pCod_tesor1800 the cod_tesor1800 to set
	 */
	public void setCod_tesor1800(String pCod_tesor1800) {
		this.cod_tesor1800 = pCod_tesor1800;
	}

	/**
	 * @return the des_tesor1800
	 */
	public String getDes_tesor1800() {
		return this.des_tesor1800;
	}

	/**
	 * @param pDes_tesor1800 the des_tesor1800 to set
	 */
	public void setDes_tesor1800(String pDes_tesor1800) {
		this.des_tesor1800 = pDes_tesor1800;
	}

	/**
	 * @return the cod_tesorUmts
	 */
	public String getCod_tesorUmts() {
		return this.cod_tesorUmts;
	}

	/**
	 * @param pCod_tesorUmts the cod_tesorUmts to set
	 */
	public void setCod_tesorUmts(String pCod_tesorUmts) {
		this.cod_tesorUmts = pCod_tesorUmts;
	}

	/**
	 * @return the des_tesorUmts
	 */
	public String getDes_tesorUmts() {
		return this.des_tesorUmts;
	}

	/**
	 * @param pDes_tesorUmts the des_tesorUmts to set
	 */
	public void setDes_tesorUmts(String pDes_tesorUmts) {
		this.des_tesorUmts = pDes_tesorUmts;
	}
	
}
