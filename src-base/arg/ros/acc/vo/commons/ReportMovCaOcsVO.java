 




 




 



package arg.ros.acc.vo.commons;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author rosario.argentina
 *
 */
public class ReportMovCaOcsVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4942487458248800052L;

	/**
	 * 
	 */
	//private Date feh_operacio;
	private String feh_operacio;
	
	/**
	 * 
	 */
	private String cta_facturac;
	
	/**
	 * 
	 */
	private String num_telefono;
	
	/**
	 * 
	 */
	private Date feh_cargo;
	
	/**
	 * 
	 */
	private String cta_confactu;
	
	/**
	 * 
	 */
	private String des_confactu;
	
	/**
	 * 
	 */
	private BigDecimal tot_cargoE;
	
	/**
	 * 
	 */
	private String cod_usuario;
	
	/**
	 * 
	 */
	private String tipoOperacio;

	/**
	 * @return the cta_facturac
	 */
	public String getCta_facturac() {
		return this.cta_facturac;
	}

	/**
	 * @param pCta_facturac the cta_facturac to set
	 */
	public void setCta_facturac(String pCta_facturac) {
		this.cta_facturac = pCta_facturac;
	}

	/**
	 * @return the num_telefono
	 */
	public String getNum_telefono() {
		return this.num_telefono;
	}

	/**
	 * @param pNum_telefono the num_telefono to set
	 */
	public void setNum_telefono(String pNum_telefono) {
		this.num_telefono = pNum_telefono;
	}

	/**
	 * @return the feh_cargo
	 */
	public Date getFeh_cargo() {
		return this.feh_cargo;
	}

	/**
	 * @param pFeh_cargo the feh_cargo to set
	 */
	public void setFeh_cargo(Date pFeh_cargo) {
		this.feh_cargo = pFeh_cargo;
	}

	/**
	 * @return the cta_confactu
	 */
	public String getCta_confactu() {
		return this.cta_confactu;
	}

	/**
	 * @param pCta_confactu the cta_confactu to set
	 */
	public void setCta_confactu(String pCta_confactu) {
		this.cta_confactu = pCta_confactu;
	}

	/**
	 * @return the des_confactu
	 */
	public String getDes_confactu() {
		return this.des_confactu;
	}

	/**
	 * @param pDes_confactu the des_confactu to set
	 */
	public void setDes_confactu(String pDes_confactu) {
		this.des_confactu = pDes_confactu;
	}

	/**
	 * @return the tot_cargoE
	 */
	public BigDecimal getTot_cargoE() {
		return this.tot_cargoE;
	}

	/**
	 * @param pTot_cargoE the tot_cargoE to set
	 */
	public void setTot_cargoE(BigDecimal pTot_cargoE) {
		this.tot_cargoE = pTot_cargoE;
	}

	/**
	 * @return the cod_usuario
	 */
	public String getCod_usuario() {
		return this.cod_usuario;
	}

	/**
	 * @param pCod_usuario the cod_usuario to set
	 */
	public void setCod_usuario(String pCod_usuario) {
		this.cod_usuario = pCod_usuario;
	}

	/**
	 * @return the tipoOperacio
	 */
	public String getTipoOperacio() {
		return this.tipoOperacio;
	}

	/**
	 * @param pTipoOperacio the tipoOperacio to set
	 */
	public void setTipoOperacio(String pTipoOperacio) {
		this.tipoOperacio = pTipoOperacio;
	}

	/**
	 * @return the feh_operacio
	 */
	public String getFeh_operacio() {
		return this.feh_operacio;
	}

	/**
	 * @param pFeh_operacio the feh_operacio to set
	 */
	public void setFeh_operacio(String pFeh_operacio) {
		this.feh_operacio = pFeh_operacio;
	}
		
}
