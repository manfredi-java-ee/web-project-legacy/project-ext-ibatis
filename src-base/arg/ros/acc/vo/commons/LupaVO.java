 




 




 



package arg.ros.acc.vo.commons;


/**
 * VO creado para implementar el mapeo de las lupas genericas Codigo - Descripcion
 *  
 * @author jc053 27/01/2009
 *
 */
public class LupaVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4830737452654355867L;
	
	/**
	 * Codigo
	 */
	private String codigo;

	/**
	 * Descripcion 
	 */
	private String descripcion;

	private String cod_planfact;
	
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCod_planfact() {
		return cod_planfact;
	}

	public void setCod_planfact(String cod_planfact) {
		this.cod_planfact = cod_planfact;
	}

}
