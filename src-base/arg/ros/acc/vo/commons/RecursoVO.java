 




 




 



package arg.ros.acc.vo.commons;


/**
 * @author rosario.argentina
 *
 */
public class RecursoVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Integer codigo;
   
	/**
	 * 
	 */
	private String path;
	
	/**
	 * 
	 */
	private String permiso;

	/**
	 * @return the permiso
	 */
	public String getPermiso() {
		return this.permiso;
	}

	/**
	 * @param pPermiso the permiso to set
	 */
	public void setPermiso(String pPermiso) {
		this.permiso = pPermiso;
	}

	/**
	 * @return the codigo
	 */
	public Integer getCodigo() {
		return this.codigo;
	}

	/**
	 * @param pCodigo the codigo to set
	 */
	public void setCodigo(Integer pCodigo) {
		this.codigo = pCodigo;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return this.path;
	}

	/**
	 * @param pPath the path to set
	 */
	public void setPath(String pPath) {
		this.path = pPath;
	}

}
