package arg.ros.acc.vo.commons;

/**
 * @author rosario.argentina
 *
 */
public class UsuarioVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4830737452654355867L;
	
	/**
	 * 
	 */
	private String usuario;

	/**
	 * 
	 */
	private String password;
	
	
	/**
	 * 
	 */
	private String nombre;
	/**
	 * 
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
	
}
