 




 




 



package arg.ros.acc.vo.commons;

/**
 * @author rosario.argentina
 *
 */
public class ReportPlanFacTariVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -371722863849821497L;

	/**
	 * 
	 */
	private String cod_planFac;
	
	/**
	 * 
	 */
	private String des_planFac;
	
	/**
	 * 
	 */
	private String ind_dispVent;
	
	/**
	 * 
	 */
	private String ind_ctaUnica;
	
	/**
	 * 
	 */
	private String ind_descMing;
	
	/**
	 * 
	 */
	private String ind_dupEnvio;
	
	/**
	 * 
	 */
	private String ind_escalon;
	
	/**
	 * 
	 */
	private String ind_franquic;
	
	/**
	 * 
	 */
	private String ind_planTari;
	
	/**
	 * 
	 */
	private String num_maxLinea;
	
	/**
	 * 
	 */
	private String por_defecto;
	
	/**
	 * 
	 */
	private String plantari;
	
	/**
	 * 
	 */
	private String fec_efectivi;

	/**
	 * @return the cod_planFac
	 */
	public String getCod_planFac() {
		return this.cod_planFac;
	}

	/**
	 * @param pCod_planFac the cod_planFac to set
	 */
	public void setCod_planFac(String pCod_planFac) {
		this.cod_planFac = pCod_planFac;
	}

	/**
	 * @return the des_planFac
	 */
	public String getDes_planFac() {
		return this.des_planFac;
	}

	/**
	 * @param pDes_planFac the des_planFac to set
	 */
	public void setDes_planFac(String pDes_planFac) {
		this.des_planFac = pDes_planFac;
	}

	/**
	 * @return the ind_dispVent
	 */
	public String getInd_dispVent() {
		return this.ind_dispVent;
	}

	/**
	 * @param pInd_dispVent the ind_dispVent to set
	 */
	public void setInd_dispVent(String pInd_dispVent) {
		this.ind_dispVent = pInd_dispVent;
	}

	/**
	 * @return the ind_ctaUnica
	 */
	public String getInd_ctaUnica() {
		return this.ind_ctaUnica;
	}

	/**
	 * @param pInd_ctaUnica the ind_ctaUnica to set
	 */
	public void setInd_ctaUnica(String pInd_ctaUnica) {
		this.ind_ctaUnica = pInd_ctaUnica;
	}

	/**
	 * @return the ind_descMing
	 */
	public String getInd_descMing() {
		return this.ind_descMing;
	}

	/**
	 * @param pInd_descMing the ind_descMing to set
	 */
	public void setInd_descMing(String pInd_descMing) {
		this.ind_descMing = pInd_descMing;
	}

	/**
	 * @return the ind_dupEnvio
	 */
	public String getInd_dupEnvio() {
		return this.ind_dupEnvio;
	}

	/**
	 * @param pInd_dupEnvio the ind_dupEnvio to set
	 */
	public void setInd_dupEnvio(String pInd_dupEnvio) {
		this.ind_dupEnvio = pInd_dupEnvio;
	}

	/**
	 * @return the ind_escalon
	 */
	public String getInd_escalon() {
		return this.ind_escalon;
	}

	/**
	 * @param pInd_escalon the ind_escalon to set
	 */
	public void setInd_escalon(String pInd_escalon) {
		this.ind_escalon = pInd_escalon;
	}

	/**
	 * @return the ind_franquic
	 */
	public String getInd_franquic() {
		return this.ind_franquic;
	}

	/**
	 * @param pInd_franquic the ind_franquic to set
	 */
	public void setInd_franquic(String pInd_franquic) {
		this.ind_franquic = pInd_franquic;
	}

	/**
	 * @return the ind_planTari
	 */
	public String getInd_planTari() {
		return this.ind_planTari;
	}

	/**
	 * @param pInd_planTari the ind_planTari to set
	 */
	public void setInd_planTari(String pInd_planTari) {
		this.ind_planTari = pInd_planTari;
	}

	/**
	 * @return the num_maxLinea
	 */
	public String getNum_maxLinea() {
		return this.num_maxLinea;
	}

	/**
	 * @param pNum_maxLinea the num_maxLinea to set
	 */
	public void setNum_maxLinea(String pNum_maxLinea) {
		this.num_maxLinea = pNum_maxLinea;
	}

	/**
	 * @return the por_defecto
	 */
	public String getPor_defecto() {
		return this.por_defecto;
	}

	/**
	 * @param pPor_defecto the por_defecto to set
	 */
	public void setPor_defecto(String pPor_defecto) {
		this.por_defecto = pPor_defecto;
	}

	/**
	 * @return the plantari
	 */
	public String getPlantari() {
		return this.plantari;
	}

	/**
	 * @param pPlantari the plantari to set
	 */
	public void setPlantari(String pPlantari) {
		this.plantari = pPlantari;
	}

	/**
	 * @return the fec_efectivi
	 */
	public String getFec_efectivi() {
		return this.fec_efectivi;
	}

	/**
	 * @param pFec_efectivi the fec_efectivi to set
	 */
	public void setFec_efectivi(String pFec_efectivi) {
		this.fec_efectivi = pFec_efectivi;
	}
	
}
