package arg.ros.acc.vo.commons;


/**
 * VO creado para implementar el mapeo de las combos genericos Codigo - Descripcion
 *  
 * @author jc053 27/01/2009
 *
 */
public class ComboVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4830737452654355867L;
	
	/**
	 * Codigo
	 */
	private String codigo;

	/**
	 * Descripcion 
	 */
	private String descripcion;

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
