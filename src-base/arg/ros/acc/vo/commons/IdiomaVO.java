 




 




 



package arg.ros.acc.vo.commons;

import arg.ros.acc.vo.commons.BaseVO;


/**
 * @author ACCROS_Admin
 *
 */
public class IdiomaVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5746986565346581615L;

	/**
	 * 
	 */
	private String codIdioma;
	
	/**
	 * 
	 */
	private String desIdioma;

	/**
	 * @return the codIdioma
	 */
	public String getCodIdioma() {
		return this.codIdioma;
	}

	/**
	 * @param pCodIdioma the codIdioma to set
	 */
	public void setCodIdioma(String pCodIdioma) {
		this.codIdioma = pCodIdioma;
	}

	/**
	 * @return the desIdioma
	 */
	public String getDesIdioma() {
		return this.desIdioma;
	}

	/**
	 * @param pDesIdioma the desIdioma to set
	 */
	public void setDesIdioma(String pDesIdioma) {
		this.desIdioma = pDesIdioma;
	}
	
}
