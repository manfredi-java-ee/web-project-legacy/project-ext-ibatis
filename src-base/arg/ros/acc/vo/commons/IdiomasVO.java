 




 




 



package arg.ros.acc.vo.commons;

/**
* IdiomasVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class IdiomasVO {

	private String cod_idioma;
	private String des_idioma;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public IdiomasVO() {

	}
	
	/**
	* Recupera el valor del atributo Cod_idioma
	* @return el valor de Cod_idioma
	*/ 
	public String getCod_idioma() {
		return cod_idioma;
	}
    	
	/**
	* Fija el valor del atributo Cod_idioma
	* @param valor el valor de Cod_idioma
	*/ 
	public void setCod_idioma(String valor) {
		this.cod_idioma = valor;
	}
	/**
	* Recupera el valor del atributo Des_idioma
	* @return el valor de Des_idioma
	*/ 
	public String getDes_idioma() {
		return des_idioma;
	}
    	
	/**
	* Fija el valor del atributo Des_idioma
	* @param valor el valor de Des_idioma
	*/ 
	public void setDes_idioma(String valor) {
		this.des_idioma = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Cod_idioma=["+this.getCod_idioma()+"]\r\n");
				sb.append("Des_idioma=["+this.getDes_idioma()+"]\r\n");
				return sb.toString();
	}
}
