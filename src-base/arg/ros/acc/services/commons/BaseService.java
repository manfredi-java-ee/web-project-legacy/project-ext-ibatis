 




 




 



package arg.ros.acc.services.commons;

import arg.ros.acc.commons.config.ConfigParams;
import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.commons.messages.MessageAdapter;

/**
 * Clase Base para los servicios.
 * 
 * @author rosario.argentina
 *
 */
public abstract class BaseService {

	/**
	 * 
	 */
	protected LoggerWeb logger;

	/**
	 * 
	 */
	protected MessageAdapter messages;
	
	/**
	 * 
	 */
	protected ConfigParams configParams;
	
	/**
	 * @param pLogger the logger to set
	 */
	public void setLogger(LoggerWeb pLogger) {
		this.logger = pLogger;
	}

	/**
	 * @param pMessages the messages to set
	 */
	public void setMessages(MessageAdapter pMessages) {
		this.messages = pMessages;
	}

	/**
	 * @param pConfigParams the configParams to set
	 */
	public void setConfigParams(ConfigParams pConfigParams) {
		this.configParams = pConfigParams;
	}

	/**
	 * @return the logger
	 */
	public LoggerWeb getLogger() {
		return this.logger;
	}
	
}
