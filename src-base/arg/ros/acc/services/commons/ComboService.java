 




 




 



package arg.ros.acc.services.commons;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

/**
 * @author rosario.argentina
 *
 */
public interface ComboService {

	/**
	 * @param pPaginatedList
	 * @return
	 * @throws ServiceException
	 */
	PaginatedList obtener(final String pGridname, PaginatedList pPaginatedList) throws ServiceException;

	/**
	 * @param pComboName
	 * @param pPaginatedList
	 * @param pLimit
	 * @return
	 * @throws ServiceException
	 */
	PaginatedList obtener(final String pComboName, PaginatedList pPaginatedList, boolean pLimit) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	PaginatedList filtrar(final String pGridname, PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;

	/**
	 * @param pComboName
	 * @param pPaginatedList
	 * @param pFilter
	 * @param pLimit
	 * @return
	 * @throws ServiceException
	 */
	PaginatedList filtrar(final String pComboName, PaginatedList pPaginatedList, Filter pFilter, boolean pLimit) throws ServiceException;
	
}
