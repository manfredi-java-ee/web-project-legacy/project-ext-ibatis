 




 




 



package arg.ros.acc.services.commons;

import javax.servlet.http.HttpServletRequest;

import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public interface LogoutService {

	/**
	 * @param pRequest
	 * @param pUserVO
	 * @throws Exception
	 */
	void logoutUser(HttpServletRequest pRequest, UsuarioVO pUserVO) throws Exception;
	
}
