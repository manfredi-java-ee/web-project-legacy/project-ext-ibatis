 




 




 



package arg.ros.acc.services.commons;

import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public interface LdapService {

	/**
	 * @param pUsuario
	 * @return
	 * @throws ServiceException 
	 */
	boolean authenticateUser(UsuarioVO pUsuario) throws ServiceException;
	
}
