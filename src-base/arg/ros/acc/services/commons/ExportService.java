 




 




 



package arg.ros.acc.services.commons;

import java.util.List;
import java.util.Map;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.exceptions.ServiceException;

/**
 * @author rosario.argentina
 *
 */
public interface ExportService {

	/**
	 * @param pGridName
	 * @return
	 */
	List export(final String pGridName, final Filter pFilter) throws ServiceException;

	/**
	 * @param pRecords
	 * @return
	 */
	String[] getFields(List pRecords) throws ServiceException;
	
	/**
	 * @param pRecords
	 * @param pExcludeKeys
	 * @return
	 * @throws ServiceException
	 */
	String[] getFields(List pRecords, String[] pExcludeKeys) throws ServiceException;
		
	/**
	 * @param pRecords
	 * @return
	 * @throws ServiceException
	 */
	Object[] getFieldsValues(List pRecords) throws ServiceException;
	
	/**
	 * @param pRecords
	 * @param pExcludeKeys
	 * @return
	 * @throws ServiceException
	 */
	Object[] getFieldsValues(List pRecords, String[] pExcludeKeys) throws ServiceException;
	
	/**
	 * @param pGridName
	 * @return
	 */
	Map<String, String> getColumnsHeadersMap(String pGridName);
}
