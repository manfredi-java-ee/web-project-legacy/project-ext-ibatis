 




 




 



package arg.ros.acc.services.commons;

import java.util.*;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

import arg.ros.acc.vo.commons.IdiomasVO;

/**
 * @author rosario.argentina
 *
 */
public interface IdiomasService {

	/**
	 * @return
	 * @throws ServiceException
	 */
	List obtener() throws ServiceException;

	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	List obtener(final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList obtener(final PaginatedList pPaginatedList) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @return
	 * @throws ServiceException
	 */
	int count() throws ServiceException;
	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int count(Filter pFilter) throws ServiceException;

	/**
	 * @param pIdiomasVO
	 * @throws ServiceException
	 */
	void agregar(final IdiomasVO pIdiomasVO) throws ServiceException;
		
	/**
	 * @param pIdiomasVO
	 * @throws ServiceException
	 */
	void eliminar(final IdiomasVO pIdiomasVO) throws ServiceException;

	/**
	 * @param pIdiomasVO
	 * @throws ServiceException
	 */
	void modificar(final IdiomasVO pIdiomasVO) throws ServiceException;
}
