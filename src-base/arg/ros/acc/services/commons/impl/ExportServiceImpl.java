 




 




 



package arg.ros.acc.services.commons.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.ArrayUtils;

import com.ibatis.sqlmap.engine.mapping.result.BasicResultMapping;
import com.ibatis.sqlmap.engine.mapping.result.ResultMap;
import com.ibatis.sqlmap.engine.mapping.result.ResultMapping;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.messages.MessageAdapter;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.daos.commons.ExportDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.ExportService;

/**
 * @author rosario.argentina
 *
 */
public class ExportServiceImpl extends BaseService implements ExportService {

	/**
	 * 
	 */
    private static final String[] PREFIXES = { "arg" , "pfa", "acc", "pgsm", "pga" };
	
	/**
	 * 
	 */
	private static final String[] SUFIXES = { "corto", "largo" };
	
	/**
	 * 
	 */
	private ExportDAO exportDAO;
	
	/**
	 * 
	 */
	private MessageAdapter fields;
	
	/**
	 * @param pGridName
	 * @return
	 */
	public Map<String, String> getColumnsHeadersMap(String pGridName) {
		LinkedHashMap<String, String> columnsHeadersMap = new LinkedHashMap<String, String>();
		
		ResultMap resultMap = this.exportDAO.getResultMap(pGridName);
		List<ResultMapping> mappings = Arrays.asList(resultMap.getResultMappings());
		
		final String stringPrefijo = PREFIXES[PREFIXES.length - 1] + "_";
		
		for (ResultMapping resultMapping : mappings) {
			final BasicResultMapping basicResultMapping = (BasicResultMapping) resultMapping;
			
			final String columnName = basicResultMapping.getColumnName();
			final String propertyName = basicResultMapping.getPropertyName();
			
			String message = getMessage(pGridName, propertyName); 
			if (message == null) {
				message = columnName;
			}

			//para evitar label <tabla>.<columna>
			if(	message.startsWith(stringPrefijo)){
				message = columnName;
			}
			
			columnsHeadersMap.put(propertyName, message);
		}
		return columnsHeadersMap;
	}
	
	/**
	 * @param pGridName
	 * @param pPropertyName
	 * @return
	 */
	private String getMessage(String pGridName, String pPropertyName) {
		String message = null;
		StringBuffer sbKey = new StringBuffer();
		
		for (int idxSufixes = 0; idxSufixes < ExportServiceImpl.SUFIXES.length; idxSufixes++) {
			for (int idxPrefixes = 0; idxPrefixes < ExportServiceImpl.PREFIXES.length; idxPrefixes++) {
				String prefix = ExportServiceImpl.PREFIXES[idxPrefixes];
				sbKey.append(prefix).append(Constants.CONFIG.CONSTANT_UNDERSCORE);
				sbKey.append(pGridName.toLowerCase()).append(Constants.CONFIG.CONSTANT_POINT);
				sbKey.append(pPropertyName.toLowerCase()).append(Constants.CONFIG.CONSTANT_UNDERSCORE);
				sbKey.append(ExportServiceImpl.SUFIXES[idxSufixes]);
				
				message = sbKey.toString();
				
				try {
					message = fields.get(sbKey.toString());
					return message;

				} catch (Exception e) {
					sbKey = new StringBuffer();
					System.err.println("message not found:" + message);
				}
			}
		}
		return message;
	}
	
	/**
	 * {@inheritDoc}
	 * @throws ServiceException 
	 */
	public List export(String pGridName, Filter pFilter) throws ServiceException {
		try {
			return exportDAO.select(pGridName, pFilter.getProperties());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String[] getFields(List pRecords) throws ServiceException {
		String[] pExclude = { "class" };
		return getFields(pRecords, pExclude); 
	}
	
	/**
	 * @param pRecords
	 * @param pExcludeKeys
	 * @return
	 * @throws ServiceException
	 */
	public String[] getFields(List pRecords, String[] pExcludeKeys) throws ServiceException {
		Object object = pRecords.get(0);
		Map describedObject;
		try {
			describedObject = this.describeObject(object);
			
			for (int i = 0; i < pExcludeKeys.length; i++) {
				String key = pExcludeKeys[i];
				if (describedObject.containsKey(key)) {
					describedObject.remove(key);
				}
			}
			
			return (String[]) describedObject.keySet().toArray(ArrayUtils.EMPTY_STRING_ARRAY);
			
		} catch (IllegalAccessException e) {
			throw new ServiceException(e);
		} catch (InvocationTargetException e) {
			throw new ServiceException(e);
		} catch (NoSuchMethodException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Object[] getFieldsValues(List pRecords, String[] pExcludeKeys) throws ServiceException {
		Object object = pRecords.get(0);
		Map describedObject;
		try {
			describedObject = this.describeObject(object);
			
			for (int i = 0; i < pExcludeKeys.length; i++) {
				String key = pExcludeKeys[i];
				if (describedObject.containsKey(key)) {
					describedObject.remove(key);
				}
			}
			
			return (Object[]) describedObject.values().toArray();
			
		} catch (IllegalAccessException e) {
			throw new ServiceException(e);
		} catch (InvocationTargetException e) {
			throw new ServiceException(e);
		} catch (NoSuchMethodException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Object[] getFieldsValues(List pRecords) throws ServiceException{
		String[] pExclude = { "class" };
		return getFieldsValues(pRecords, pExclude); 
	}
	
	/**
	 * @param pObject
	 * @return
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	private Map describeObject(Object pObject) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Map describedObject = BeanUtils.describe(pObject);
		return describedObject;
	}

	/**
	 * @param pExportDAO the exportDAO to set
	 */
	public void setExportDAO(ExportDAO pExportDAO) {
		this.exportDAO = pExportDAO;
	}

	/**
	 * @return the fields
	 */
	public MessageAdapter getFields() {
		return this.fields;
	}

	/**
	 * @param pFields the fields to set
	 */
	public void setFields(MessageAdapter pFields) {
		this.fields = pFields;
	}

}
