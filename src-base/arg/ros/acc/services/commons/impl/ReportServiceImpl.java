package arg.ros.acc.services.commons.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import arg.ros.acc.commons.Constants;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.daos.commons.ReportDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.ReportService;

/**
 * @author jc053 - 25/03/2009
 *
 */
public class ReportServiceImpl extends BaseService implements ReportService {

	/**
	 * DAO de Reportes
	 */
	private ReportDAO reportDAO;
	
	/**
	 * Realiza la generacion del Report
	 * @throws ServiceException 
	 */
	public byte[] generateReport(final String pReportName, final Filter pFilter) throws ServiceException {
		
        final String reportSource = Constants.ACCROS.APP_REPORTS_PACKAGE + pReportName + Constants.ACCROS.APP_REPORTS_EXTENSION;
		this.logger.Comentario(ReportServiceImpl.class , "ReportServiceImpl.reportSource: " + reportSource);
		
		final InputStream ris = this.getClass().getResourceAsStream(reportSource);
		this.logger.Comentario(ReportServiceImpl.class , "ReportServiceImpl.ris: " + ris.toString());
		
		List coleccionBeans = null;
		
		try {
			this.logger.Comentario(ReportServiceImpl.class , "ReportServiceImpl.pReportName: " + pReportName.toString());
			this.logger.Comentario(ReportServiceImpl.class , "ReportServiceImpl.pFilter: " + pFilter.getProperties());
			coleccionBeans = reportDAO.select(pReportName, pFilter.getProperties());
			this.logger.Comentario(ReportServiceImpl.class , "ReportServiceImpl.reportDAO.select().coleccionBeans: " + coleccionBeans.toString());
		} catch (DAOException e1) {
			e1.printStackTrace();
			this.logger.Comentario(ReportServiceImpl.class , "ReportServiceImpl - DAOException: " + e1.getMessage());
			throw new ServiceException("Error en ReportServiceImpl.reportDAO.select().coleccionBeans: " + coleccionBeans.toString() + 
				" pFilter: " + pFilter.getProperties().toString() + " reportSource: " + reportSource , e1);
		}
		
		JasperReport report;
		try {
			report = JasperCompileManager.compileReport(ris);
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl.generateReport.JasperCompileManager.compileReport.report: " + report.toString());
			
		} catch (JRException e2) {
			e2.printStackTrace();
			this.logger.Comentario(ReportServiceImpl.class , "ReportServiceImpl-JasperCompileManager.compileReport - JRException: " + e2.getMessage());
			throw new ServiceException("Error en ReportServiceImpl.JasperCompileManager.compileReport().ris: " + ris.toString() , e2);
		}
		
		final JRDataSource source = new JRBeanCollectionDataSource(coleccionBeans, false);
		this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl.source: " + source.toString());
		
		JasperPrint print = null;
		try {
			
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl.JasperFillManager.fillReport().print (antes): " + print);
			print = JasperFillManager.fillReport(report, new HashMap(), source);
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl.JasperFillManager.fillReport().print: " + print);
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl.JasperFillManager.fillReport().getPropertyNames(): " + print.getPropertyNames());
			
		} catch (JRException e3) {
			e3.printStackTrace();
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl-JasperFillManager.fillReport - JRException: " + e3.getMessage());
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl-JasperFillManager.fillReport - JRException: " + e3.toString());
			
			throw new ServiceException("Error en ReportServiceImpl.JasperFillManager.fillReport().print: " + print +
				" report: " + report + " source: " + source , e3);
		}
		
		byte[] arrBytes = null;
		try {
			arrBytes = JasperExportManager.exportReportToPdf(print);
            this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl.JasperExportManager.exportReportToPdf().arrBytes" + arrBytes.toString());
			return arrBytes;
 			
		} catch (JRException e4) {
			e4.printStackTrace();
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl-JasperExportManager.exportReportToPdf - JRException" + e4.getMessage());
			this.logger.Comentario(ReportServiceImpl.class, "ReportServiceImpl-JasperExportManager.exportReportToPdf - JRException: " + e4.toString());
			throw new ServiceException("Error en ReportServiceImpl.JasperExportManager.exportReportToPdf().arrBytes: " + arrBytes + " print: " + print.getPropertyNames(), e4);
		}
	}

	/**
	 * @param pDAO the reportDAO to set
	 */
	public void setReportDAO(ReportDAO pDAO) {
		this.reportDAO = pDAO;
	}

}
