 




 




 



package arg.ros.acc.services.commons.impl;

import arg.ros.acc.daos.commons.LdapDAO;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.LdapService;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public class LdapServiceImpl extends BaseService implements LdapService {

	/**
	 * 
	 */
	private LdapDAO ldapDAO;
	private LdapDAO ldapDAO2;

	/**
	 * {@inheritDoc}
	 */
	public boolean authenticateUser(UsuarioVO pUsuario) throws ServiceException {
		boolean isUser = false;
		try {
			isUser = ldapDAO.isUser(pUsuario.getNombre(), pUsuario.getPassword());
		} catch (Exception e) {
			this.logger.Comentario(this, "Error al conectarse con LDAP#1");
			try {
				isUser = ldapDAO2.isUser(pUsuario.getNombre(), pUsuario.getPassword());
			} catch (Exception e2) {
				this.logger.Comentario(this, "Error al conectarse con LDAP#2");
				new ServiceException(e2);
			}
		}
		return isUser;
	}
	
	/**
	 * @param pLdapDAO the ldapDAO to set
	 */
	public void setLdapDAO(LdapDAO pLdapDAO) {
		this.ldapDAO = pLdapDAO;
	}

	public void setLdapDAO2(LdapDAO ldapDAO2) {
		this.ldapDAO2 = ldapDAO2;
	}
	
}
