 




 




 



package arg.ros.acc.services.commons.impl;

import java.util.Map;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.daos.commons.ComboDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.ComboService;
import arg.ros.acc.services.commons.LupaService;

/**
 * @author rosario.argentina
 *
 */
public class ComboServiceImpl extends BaseService implements ComboService {

	/**
	 * 
	 */
	private ComboDAO comboDAO;
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList filtrar(final String pComboName, PaginatedList pPaginatedList, Filter pFilter) throws ServiceException {
		pPaginatedList = this.filtrar(pComboName, pPaginatedList, pFilter, true);
		
		return pPaginatedList;
	}

	/**
	 * @param pComboName
	 * @param pPaginatedList
	 * @param pFilter
	 * @param pLimit
	 * @return
	 * @throws ServiceException
	 */
	public PaginatedList filtrar(final String pComboName, PaginatedList pPaginatedList, Filter pFilter, boolean pLimit) throws ServiceException {
		final Map filter = pFilter.getProperties();

		try {
			if (pLimit) {
			
				final int start = pPaginatedList.getPageNumber();
				final int limit = pPaginatedList.getObjectsPerPage();
				
				pPaginatedList.setData(this.comboDAO.select(pComboName, start, limit, filter));
				pPaginatedList.setFullListSize(this.comboDAO.count(pComboName, filter));
	
			} else {
	
				pPaginatedList.setData(this.comboDAO.select(pComboName, filter));
				pPaginatedList.setFullListSize(this.comboDAO.count(pComboName, filter));
			}
			
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
		return pPaginatedList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList obtener(final String pComboName, PaginatedList pPaginatedList) throws ServiceException {
		pPaginatedList = this.obtener(pComboName, pPaginatedList, true);
		
		return pPaginatedList;
	}

	/**
	 * @param pComboName
	 * @param pPaginatedList
	 * @param pLimit
	 * @return
	 * @throws ServiceException
	 */
	public PaginatedList obtener(final String pComboName, PaginatedList pPaginatedList, boolean pLimit) throws ServiceException {
		try {
			if (pLimit) {
				final int start = pPaginatedList.getPageNumber();
				final int limit = pPaginatedList.getObjectsPerPage();
				
				pPaginatedList.setData(this.comboDAO.select(pComboName, start, limit));
				pPaginatedList.setFullListSize(this.comboDAO.count(pComboName));

			} else {
	
				pPaginatedList.setData(this.comboDAO.select(pComboName));
				pPaginatedList.setFullListSize(this.comboDAO.count(pComboName));
			}
			
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
		return pPaginatedList;
	}

	/**
	 * @param pComboDAO the comboDAO to set
	 */
	public void setComboDAO(ComboDAO pComboDAO) {
		this.comboDAO = pComboDAO;
	}

}
