package arg.ros.acc.services.commons.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.filter.FilterFactory;
import arg.ros.acc.components.filter.FilterFactoryImpl;
import arg.ros.acc.daos.commons.LoginDAO;
import arg.ros.acc.daos.commons.Operaciones;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.LoginService;
import arg.ros.acc.vo.commons.RecursoVO;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public class LoginServiceImpl extends BaseService implements LoginService {
	
	private LoginDAO loginDAO;
	
	/**
	 * {@inheritDoc}
	 */
	public boolean loginUser(HttpServletRequest pRequest, UsuarioVO pUsuarioVO) throws Exception {
		boolean permits = true;
		this.logger.FinProceso(this, "Fin ComprobarUsuario");
		return permits;
	}

	/**
	 * {@inheritDoc}
	 */
	public Map loadResources() throws Exception {
		return loadResources(null);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Map loadResources(RecursoVO pResource) throws Exception {
		final FilterFactory filterFactory = FilterFactoryImpl.getInstance();
		final Filter filter = filterFactory.emptyFilter();
		
		Map resources = new HashMap();
		
		try {
		    if (pResource != null) {
				filter.addFilterProperty("codigo", pResource.getCodigo());
				filter.addFilterProperty("permiso", pResource.getPermiso());
				filter.addFilterProperty("path", pResource.getPath());
		    } 
            resources = getResourceMap(this.loginDAO.select(Operaciones.LOGIN.OBTENERRECURSOS, filter.getProperties()));
		    		    
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
		return resources;
	}
 
	
	public List obtenerCodigoUsuario(String nombreUsuario) throws Exception{
		final FilterFactory filterFactory = FilterFactoryImpl.getInstance();
		final Filter filter = filterFactory.emptyFilter();
		filter.addFilterProperty("nombreUsuario", nombreUsuario);
		
		List lista  = new ArrayList();
		try {
			
            lista = this.loginDAO.select(Operaciones.LOGIN.OBTENERCODIGOUSUARIO, filter.getProperties());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return lista;
	}
	
	/**
	 * @param pResources
	 * @return
	 */
	private Map getResourceMap(List pResources) {
		final Map resources = new HashMap();
        for (Iterator iterator = pResources.iterator(); iterator.hasNext();) {
			RecursoVO recursoVO = (RecursoVO) iterator.next();
            resources.put(recursoVO.getPath(), recursoVO.getPermiso());
		}
		return resources;
	}


	/**
	 * @param pLoginDAO the loginDAO to set
	 */
	public void setLoginDAO(LoginDAO pLoginDAO) {
		this.loginDAO = pLoginDAO;
	}

	public void setUserAuthorization(UsuarioVO pUsuario) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
