 




 




 



package arg.ros.acc.services.commons.impl;

import java.util.Map;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.daos.commons.LupaDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.LupaService;

/**
 * @author rosario.argentina
 *
 */
public class LupaServiceImpl extends BaseService implements LupaService {

	/**
	 * 
	 */
	protected LupaDAO lupaDAO;
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList filtrar(final String pGridname, PaginatedList pPaginatedList, Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();
		
		try {
			pPaginatedList.setData(this.lupaDAO.select(pGridname, start, limit, filter));
			pPaginatedList.setFullListSize(this.lupaDAO.count(pGridname, filter));
		
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}

	/**
	 * {@inheritDoc}
	 */
	public PaginatedList obtener(final String pGridname, PaginatedList pPaginatedList) throws ServiceException {
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();
			
		try {
			pPaginatedList.setData(this.lupaDAO.select(pGridname, start, limit));
			pPaginatedList.setFullListSize(this.lupaDAO.count(pGridname));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}

	/**
	 * @param pLupaDAO the lupaDAO to set
	 */
	public void setLupaDAO(LupaDAO pLupaDAO) {
		this.lupaDAO = pLupaDAO;
	}

}
