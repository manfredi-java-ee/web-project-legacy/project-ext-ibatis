package arg.ros.acc.services.commons.impl;

import javax.servlet.http.HttpServletRequest;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.Container;
import arg.ros.acc.commons.util.ContextUtils;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.LogoutService;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public class LogoutServiceImpl extends BaseService implements LogoutService {

	/**
	 * {@inheritDoc}
	 */
	public void logoutUser(HttpServletRequest pRequest, UsuarioVO pUserVO) throws Exception {
		this.logger.Comentario(this, "Eliminando Usuario de la Session.");
		
        ContextUtils.set(Constants.ACCROS.APP_USUARIO, null, Constants.SCOPES.THREAD_LOCAL_SCOPE, pRequest);
        ContextUtils.set(Constants.ACCROS.APP_USUARIO, null, Constants.SCOPES.SESSION_SCOPE, pRequest);
        ContextUtils.set(Constants.ACCROS.APP_MENU, null, Constants.SCOPES.SESSION_SCOPE, pRequest);
		
		this.logger.Comentario(this, "Invalidando Session.");
		pRequest.getSession().invalidate();
	}
	
}
