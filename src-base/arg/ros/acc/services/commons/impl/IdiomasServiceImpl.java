 




 




 



package arg.ros.acc.services.commons.impl;

import java.util.*;

import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.filter.FilterFactory;
import arg.ros.acc.components.filter.FilterFactoryImpl;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.daos.commons.IdiomasDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.commons.IdiomasService;
import arg.ros.acc.vo.commons.IdiomasVO;

/**
 * @author rosario.argentina
 *
 */
public class IdiomasServiceImpl extends BaseService implements IdiomasService {

	/**
	 * DAO para el acceso a la tabla PGSM_IDIOMAS.
	 */
	private IdiomasDAO idiomasDao;

	/**
	 * {@inheritDoc}
	 */
	public List obtener() throws ServiceException {
		List results = new ArrayList();
		try {
			results = this.idiomasDao.select();

		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECTALL, e);
			throw new ServiceException(e);
		}
		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	public List obtener(Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		List results = new ArrayList();
		try {
			results = this.idiomasDao.select(filter);
			return results;
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public PaginatedList obtener(PaginatedList pPaginatedList) throws ServiceException {
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.idiomasDao.select(start, limit));
			pPaginatedList.setFullListSize(this.idiomasDao.count());
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECT, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.idiomasDao.select(start, limit, filter));
			pPaginatedList.setFullListSize(this.idiomasDao.count(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}

	/**
	 * {@inheritDoc}
	 */
	public int count() throws ServiceException {
		FilterFactory filterFactory = FilterFactoryImpl.getInstance();
		Filter filter = filterFactory.emptyFilter();
		
		return this.count(filter);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Filter pFilter) throws ServiceException {
		Map properties = pFilter.getProperties();
		int count = 0;
		try {
			count = this.idiomasDao.count(properties);
			
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.COUNT, e);
			throw new ServiceException(e);
		}
		return count;
	}

	/**
	 * {@inheritDoc}
	 */
	public void agregar(final IdiomasVO pIdiomasVO) throws ServiceException {
		try {
			this.idiomasDao.insert(pIdiomasVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.INSERT, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void eliminar(final IdiomasVO pIdiomasVO) throws ServiceException {
		try {
			this.idiomasDao.delete(pIdiomasVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.DELETE, e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void modificar(final IdiomasVO pIdiomasVO) throws ServiceException {
		try {
			this.idiomasDao.update(pIdiomasVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.UPDATE, e);
			throw new ServiceException(e);
		}			
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setIdiomasDAO(IdiomasDAO pIdiomasDAO) {
		this.idiomasDao = pIdiomasDAO;
	}

}
