package arg.ros.acc.services.commons;

import org.apache.commons.lang.StringUtils;

public class BaseRegularService extends BaseService {

	/**	
	 * Metodo que retorna el tiempo en formato hh:mm:ss segun la cantidad de milisegundos
	 * @param vartiempo
	 * @return
	 */
	public String calculaDuracion(Long vartiempo){
		String horas;
		String minutos;
		String segundos;
		String tiempoSalida = "";
		double totalSegundos = (double)vartiempo;
				
		if(totalSegundos < 0){
			totalSegundos = totalSegundos * -1;
		}
		
		if(totalSegundos == 0){
			return ("00:00:00");
		}
		
		//1h-> 3600 seg
		double cantidadHoras = totalSegundos/3600;
		double resto_de_horas_en_segundos = (totalSegundos%3600);
		
		//1m-> 60 seg
		double cantidadMinutos = resto_de_horas_en_segundos/60;
		double resto_de_minutos_en_segundos = (resto_de_horas_en_segundos%60);
		
		double cantidadSegundos = resto_de_minutos_en_segundos;
		
		horas = Integer.toString((new Double(cantidadHoras).intValue()));
		minutos = Integer.toString((new Double(cantidadMinutos).intValue()));
		segundos = Integer.toString((new Double(cantidadSegundos).intValue()));
		
		//buscando 2 digitos
		if(cantidadHoras < 10) {
			horas = "0" + horas;
		}
		
		if(vartiempo<0){
			tiempoSalida = "- ";
		}
		
		tiempoSalida += StringUtils.leftPad(horas, 5," ")+":"+StringUtils.leftPad(minutos, 2, "0")+":"+StringUtils.leftPad(segundos,2, "0");
		
		return tiempoSalida.trim();
		
	} 
	
	
}
