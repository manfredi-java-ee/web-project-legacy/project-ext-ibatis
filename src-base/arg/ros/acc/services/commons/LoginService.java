package arg.ros.acc.services.commons;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import arg.ros.acc.vo.commons.RecursoVO;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public interface LoginService {

	/**
	 * @param pUserVO
	 * @return
	 * @throws Exception
	 */
	public boolean loginUser(HttpServletRequest pRequest, UsuarioVO pUserVO) throws Exception;
	
	/**
	 * @param pResource
	 * @param pUser
	 * @return
	 * @throws Exception
	 */
	public Map loadResources(RecursoVO pResource) throws Exception;

	/**
	 * @param pUsuario
	 * @throws Exception
	 */
	public void setUserAuthorization(UsuarioVO pUsuario) throws Exception;
	
	/**
	 * @return
	 * @throws Exception
	 */
	public Map loadResources() throws Exception;
	
	/**
	 * 
	 * @param nombreUsuario
	 * @return
	 * @throws Exception
	 */
	public List obtenerCodigoUsuario(String nombreUsuario) throws Exception;
	
}
