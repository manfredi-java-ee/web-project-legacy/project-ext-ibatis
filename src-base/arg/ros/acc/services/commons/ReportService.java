 




 




 



package arg.ros.acc.services.commons;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.exceptions.ServiceException;

/**
 * @author jc053
 *
 */
public interface ReportService {

	/**
	 * @param pGridName
	 * @return
	 */
	 byte[] generateReport(final String pReportName, final Filter pFilter) throws ServiceException;

	
}
