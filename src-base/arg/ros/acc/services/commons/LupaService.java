 




 




 



package arg.ros.acc.services.commons;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

/**
 * @author rosario.argentina
 *
 */
public interface LupaService {

	/**
	 * @param pPaginatedList
	 * @return
	 * @throws ServiceException
	 */
	PaginatedList obtener(final String pGridname, final PaginatedList pPaginatedList) throws ServiceException;

	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	PaginatedList filtrar(final String pGridname, final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;

	
}
