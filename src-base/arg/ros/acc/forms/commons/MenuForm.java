 




 




 



package arg.ros.acc.forms.commons;


/**
 * @author rosario.argentina
 *
 */
public class MenuForm extends ActionFormBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8308502752479488487L;
	
	/**
	 * 
	 */
	private String selectedItem;

	/**
	 * @return the selectedItem
	 */
	public String getSelectedItem() {
		return selectedItem;
	}

	/**
	 * @param selectedItem the selectedItem to set
	 */
	public void setSelectedItem(String selectedItem) {
		this.selectedItem = selectedItem;
	}
	
}
