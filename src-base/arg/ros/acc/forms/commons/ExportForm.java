 




 




 



package arg.ros.acc.forms.commons;

/**
 * @author rosario.argentina
 *
 */
public class ExportForm extends ActionFormBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7014020723627186404L;
	/**
	 * 
	 */
	private String gridName;
	
	/**
	 * 
	 */
	private String gridColum;

	/**
	 * @return the gridName
	 */
	public String getGridName() {
		return this.gridName;
	}

	/**
	 * @param pGridName the gridName to set
	 */
	public void setGridName(String pGridName) {
		this.gridName = pGridName;
	}

	public String getGridColum() {
		return gridColum;
	}

	public void setGridColum(String gridColum) {
		this.gridColum = gridColum;
	}
	
}
