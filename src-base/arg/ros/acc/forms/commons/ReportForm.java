 




 




 



package arg.ros.acc.forms.commons;

/**
 * @author jc053
 *
 */
public class ReportForm extends ActionFormBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7014020723627186404L;

	/**
	 * 
	 */
	private String reportName;

	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return this.reportName;
	}

	/**
	 * @param pReportName the reportName to set
	 */
	public void setReportName(String pReportName) {
		this.reportName = pReportName;
	}
}
