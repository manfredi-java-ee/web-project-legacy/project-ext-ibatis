 




 




 



package arg.ros.acc.forms.commons;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import net.sf.ezmorph.object.AbstractObjectMorpher;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONUtils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.springframework.util.ReflectionUtils;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.MapToDateMorpher;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
@SuppressWarnings("unchecked")
public class ActionFormBase extends ActionForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -654099570785725688L;

	/**
	 * 
	 */
	protected static final String DEFAULT_DATE_PATTERN = "dd/MM/yyyy";

	/**
	 * 
	 */
	private static final String FILTER_SUFIX = "Filter";

	/**
	 * 
	 */
	private static final String GET = "get";

	/**
	 * 
	 */
	final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ActionFormBase.DEFAULT_DATE_PATTERN);

	/**
	 * @return
	 */
	public Map getAtributesAsMap(final HttpServletRequest pRequest) {
		final Map description = new HashMap();
		final List fields = Arrays.asList(this.getClass().getDeclaredFields());
		
		for (Iterator iterator = fields.iterator(); iterator.hasNext();) {
			
			final Field field = (Field) iterator.next();
			final int modifier = field.getModifiers();
			
			if (!Modifier.isTransient(modifier)) {
				final String name = field.getName() + ActionFormBase.FILTER_SUFIX;
				
				pRequest.getParameterMap();
				Object value = null;
				
				try {
					value = BeanUtils.getProperty(this, name);
					if (value != null) {
						description.put(name, value);
					}
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}
		return (description);
	}
	
	/**
	 * @param pProperty Property del ActionForm que contiene el JSON enviado por la grilla.
	 * @param pVOClass Clase a la cual se instanciara y se setearan los datos.
	 * @param pMapping Mapa con las Clases que tiene que mapear el JSONObject cuando hace la conversion.
	 * @return
	 */
	public Object getFromJson(final String pProperty, final Class pVOClass, final Map pMapping, final AbstractObjectMorpher pMorpher) {
		final String jsonString = (String) getValueFromFormField(pProperty);
		
		if (StringUtils.isEmpty(jsonString)) {
			return null;
		}

		if (pMorpher!=null) {
			JSONUtils.getMorpherRegistry().registerMorpher(pMorpher);
		} else {
			JSONUtils.getMorpherRegistry().registerMorpher(new MapToDateMorpher());
		}
		
		final JSONObject jsonObject = JSONObject.fromObject(jsonString);
		final Object vo = JSONObject.toBean(jsonObject, pVOClass, pMapping);
	
		return vo;
	}

	/**
	 * @param pProperty
	 * @param pVOClass
	 * @param pMapping
	 * @return
	 */
	public Object getFromJson(final String pProperty, final Class pVOClass, final Map pMapping) {
		return this.getFromJson(pProperty, pVOClass, pMapping, null);
	}
	
	/**
	 * @param pProperty
	 * @param pVOClass
	 * @return
	 */
	public Object getFromJson(final String pProperty, final Class pVOClass) {
		return this.getFromJson(pProperty, pVOClass, null, null);
	}
	
	/**
	 * @param pProperty
	 * @param pVOClass
	 * @param pMorpher
	 * @return
	 */
	public Object getFromJson(final String pProperty, final Class pVOClass, final AbstractObjectMorpher pMorpher) {
		return this.getFromJson(pProperty, pVOClass, null, pMorpher);
	}
			
	
	/**
	 * @param pProperty
	 */
	private Object getValueFromFormField(final String pProperty) {
		final Method getter = ReflectionUtils.findMethod(this.getClass(), ActionFormBase.GET + StringUtils.capitalize(pProperty));
		return ReflectionUtils.invokeMethod(getter, this);
	}

	/**
	 * @param pDSV
	 * @param pDelim
	 * @return
	 */
	public String[] getAsArray(String pDSV, String pDelim) {
		if (StringUtils.isEmpty(pDSV) || StringUtils.isEmpty(pDelim)) {
			return new String[0];
		}
		
		final List valueList = new ArrayList();
		
		final StringTokenizer tokenizer = new StringTokenizer(pDSV, pDelim);
		while (tokenizer.hasMoreTokens()) {
			String element = tokenizer.nextToken();
			if (StringUtils.isNotEmpty(element)) {
				valueList.add(element);
			}
		}
			
		return (String[]) valueList.toArray(new String[valueList.size()]);
	}
	
	/**
	 * @param pValue
	 * @return
	 */
	public Date getAsDate(final String pValue) {
		return getAsDate(pValue, ActionFormBase.DEFAULT_DATE_PATTERN);
	}

	/**
	 * @param pValue
	 * @param pPattern
	 * @return
	 */
	public Date getAsDate(final String pValue, String pPattern) {
		simpleDateFormat.applyPattern(pPattern);
		
		if (StringUtils.isBlank(pValue)) { 
			return null;
		}
		
		try {
			Date date = simpleDateFormat.parse(pValue);
			return date;
			
		} catch (ParseException e) {
			return null;
		}
	}
	
	/**
	 * @param pValue
	 * @return
	 */
	public Double getAsDouble(String pValue) {
		Double doubleValue = null;
			
		if (StringUtils.isNotEmpty(pValue)) {
			pValue = new String(pValue.replace(',', '.'));

			doubleValue = new Double(pValue);
		} else {
			doubleValue = new Double(0);
		} 
		
		return doubleValue;
	}
	
	public String getAsBooleanString(final String pValue) {
		if (StringUtils.isEmpty(pValue)) {
			return "N";
		}
		if (pValue.trim().toUpperCase().equals("TRUE") 
				|| pValue.trim().toUpperCase().equals("S")
				|| pValue.trim().toUpperCase().equals("SI")) {
			
			return "S";
		} else {
			return "N";
		}
	}
	
	/**
	 * @param pValue
	 * @return
	 */
	public Integer getAsInteger(final String pValue) {
		Integer integerValue = null;
		if (StringUtils.isNotEmpty(pValue)) {
			integerValue = new Integer(pValue);
		} else {
			integerValue = new Integer(0);
		}
		return integerValue;
	}
	
	/**
	 * @param pValue
	 * @return
	 */
	public BigDecimal getAsBigDecimal(String pValue) {
		BigDecimal bigDecimal = null;
		if (StringUtils.isNotEmpty(pValue)) {
			pValue = StringUtils.replace(pValue, ",", ".");
			bigDecimal = new BigDecimal(pValue);
		} else {
			bigDecimal = new BigDecimal(0);
		}
		return bigDecimal;
	}
	
	/**
	 * @param pRequest
	 * @return
	 */
	public UsuarioVO getUserInSession(HttpServletRequest pRequest) {
        UsuarioVO usuarioVO = (UsuarioVO) pRequest.getSession().getAttribute(Constants.ACCROS.APP_USUARIO);
		
		if (usuarioVO != null) {
			return usuarioVO;
		}
		
		usuarioVO = new UsuarioVO();
		usuarioVO.setUsuario("USER");
		
		return usuarioVO;
	}
	
}
