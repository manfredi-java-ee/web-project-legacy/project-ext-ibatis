 




 




 



package arg.ros.acc.forms.commons;

/**
 * @author rosario.argentina
 *
 */
public class ComboForm extends ActionFormBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1569311186946521837L;

	/**
	 * 
	 */
	private String codigoFilter;
	
	/**
	 * 
	 */
	private String descripcionFilter;

	/**
	 * 
	 */
	private String entityName;
	
	/**
	 * @return the codigoFilter
	 */
	public String getCodigoFilter() {
		return this.codigoFilter;
	}

	/**
	 * @param pCodigoFilter the codigoFilter to set
	 */
	public void setCodigoFilter(String pCodigoFilter) {
		this.codigoFilter = pCodigoFilter;
	}

	/**
	 * @return the descripcionFilter
	 */
	public String getDescripcionFilter() {
		return this.descripcionFilter;
	}

	/**
	 * @param pDescripcionFilter the descripcionFilter to set
	 */
	public void setDescripcionFilter(String pDescripcionFilter) {
		this.descripcionFilter = pDescripcionFilter;
	}

	/**
	 * @return the entityName
	 */
	public String getEntityName() {
		return this.entityName;
	}

	/**
	 * @param pEntityName the entityName to set
	 */
	public void setEntityName(String pEntityName) {
		this.entityName = pEntityName;
	}
	
}
