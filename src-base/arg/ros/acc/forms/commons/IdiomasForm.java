 




 




 



package arg.ros.acc.forms.commons;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.commons.IdiomasVO;

/**
* IdiomasForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class IdiomasForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String cod_idioma;
	private String des_idioma;

	public IdiomasVO getVO() {
		IdiomasVO vo = new IdiomasVO();
		
		vo.setCod_idioma(this.cod_idioma);
		vo.setDes_idioma(this.des_idioma);
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Cod_idioma
	* @return el valor de Cod_idioma
	*/ 
	public String getCod_idioma() {
		return cod_idioma;
	}
    	
	/**
	* Fija el valor del atributo Cod_idioma
	* @param valor el valor de Cod_idioma
	*/ 
	public void setCod_idioma(String valor) {
		this.cod_idioma = valor;
	}
	/**
	* Recupera el valor del atributo Des_idioma
	* @return el valor de Des_idioma
	*/ 
	public String getDes_idioma() {
		return des_idioma;
	}
    	
	/**
	* Fija el valor del atributo Des_idioma
	* @param valor el valor de Des_idioma
	*/ 
	public void setDes_idioma(String valor) {
		this.des_idioma = valor;
	}
}
