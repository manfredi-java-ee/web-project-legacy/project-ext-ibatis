 




 




 



package arg.ros.acc.forms.commons;


/**
 * @author rosario.argentina
 *
 */
public class StartAppForm extends ActionFormBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1050325186207275487L;
	
	/**
	 * 
	 */
	private boolean agree;

	/**
	 * @return the agree
	 */
	public boolean isAgree() {
		return agree;
	}

	/**
	 * @param agree the agree to set
	 */
	public void setAgree(boolean agree) {
		this.agree = agree;
	}
}
