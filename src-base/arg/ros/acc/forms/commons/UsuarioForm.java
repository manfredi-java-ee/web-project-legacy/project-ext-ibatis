 




 




 



package arg.ros.acc.forms.commons;

import arg.ros.acc.vo.commons.UsuarioVO;


/**
 * @author rosario.argentina
 *
 */
public class UsuarioForm extends ActionFormBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5733362294319569775L;

	/**
	 * 
	 */
	private String usuario;
	
	/**
	 * 
	 */
	private String password;
	
	/**
	 * @return
	 */
	public UsuarioVO getVO() {
		UsuarioVO vo = new UsuarioVO();
		vo.setNombre(this.usuario);
		vo.setPassword(this.password);
		
		return vo;
	}
	
	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
}
