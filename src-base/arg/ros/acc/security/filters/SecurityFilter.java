package arg.ros.acc.security.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public class SecurityFilter implements Filter {

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig config) throws ServletException {
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		final HttpSession session = httpRequest.getSession();
		
		if (isPublicURI(httpRequest)) {
			chain.doFilter(httpRequest, httpResponse);
		}
		
		// Si caduco la session.
		if (session == null) {
			kickOff(httpRequest, httpResponse);
		}
				
        final UsuarioVO usuario = (UsuarioVO) session.getAttribute(Constants.ACCROS.APP_USUARIO);
		if (usuario != null) {
			chain.doFilter(request, response);	
		} else {
			kickOff(httpRequest, httpResponse);
		}
	}

	/**
	 * @param pResonse
	 * @throws IOException 
	 */
	public void kickOff(final HttpServletRequest pRequest, final HttpServletResponse pResponse) throws IOException {
        pResponse.sendRedirect(pRequest.getContextPath()+Constants.ACCROS.APP_REJECT_URL);
	}
	
	/**
	 * @return
	 */
	private boolean isPublicURI(HttpServletRequest pRequest) {
		final String requestedUri = pRequest.getRequestURI();
        return (!StringUtils.contains(requestedUri, Constants.ACCROS.APP_SECURE_ROOT_DIRECTORY));
	}
}
