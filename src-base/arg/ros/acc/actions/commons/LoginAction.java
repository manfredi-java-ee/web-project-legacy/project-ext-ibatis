package arg.ros.acc.actions.commons;

import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.commons.lang.StringUtils;


import arg.ros.acc.commons.Constants;
import arg.ros.acc.forms.commons.UsuarioForm;
import arg.ros.acc.services.commons.LdapService;
import arg.ros.acc.services.commons.LoginService;
import arg.ros.acc.vo.commons.UsuarioVO;


/**
 * @author rosario.argentina
 *
 */
public class LoginAction extends BaseAction {

	/**
	 * Service para el login. 
	 */
	private LoginService loginService;
	
	/**
	 * Servicio para authenticar usuarios con LDAP. 
	 */
	private LdapService ldapService;
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		
		boolean AutentificaporSUA= (request.getRemoteUser() != null && request.getRemoteUser().trim().length() > 0);
		
		AutentificaporSUA=true;
		
		if (AutentificaporSUA) {
			
			return this.login(mapping, form, request, response);
			
		} else {
			return mapping.findForward(FWD_JSP_VIEW);	

		}
	
	
	
	
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ActionForward login(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		UsuarioForm uf = (UsuarioForm) form;
		ActionErrors errors = new ActionErrors();
		
//		boolean evitarLogin = false;
//
//		if (evitarLogin){
//			String username = "default-user"; 
//			UsuarioVO usuarioVo = new UsuarioVO();
//			usuarioVo.setNombre(username);
//			request.getSession().setAttribute(Constants.ACCROS.APP_USUARIO, usuarioVo);
//			
//			return mapping.findForward(Constants.ACTIONS.SUCCESS);
//		}
		
		
		
		boolean AutentificaporSUA = (request.getRemoteUser() != null && request.getRemoteUser().trim().length() > 0);
		
		AutentificaporSUA = true;
		
		if (AutentificaporSUA){
			
			
			// Recupero por Cookie el USUARIO SUA 	
						
			String cookie = request.getHeader( "Cookie" );
			String usuarioSUA = request.getHeader( "UCID" );

			
			if (usuarioSUA == null) // No entra por SUA
			{
				logger.Comentario(this, "No ingresa por sua");
 				String username = "User Desa"; 
 				UsuarioVO usuarioVo = new UsuarioVO();
 				usuarioVo.setNombre(username);
 				request.getSession().setAttribute(Constants.ACCROS.APP_USUARIO, usuarioVo);
				return mapping.findForward(Constants.ACTIONS.SUCCESS);
       
	        } else
	        {
	         	UsuarioVO usuarioVo = new UsuarioVO();
				usuarioVo.setNombre(usuarioSUA);
				
//				//Seteo el objeto usuario en una variable de sesi�n 
//				
//				request.getSession().setAttribute(Constants.ACCROS.APP_USUARIO, usuarioVo);
//				
//	            
//	            session.setAttribute(IConstants.MENU_ITEMS,ub.getMenu());
//	            session.setAttribute(IConstants.MENU_ITEMS_TILES,ub.getMenuTiles());
//	            
//	            ub.setUserSua(dn);
//	            session.setAttribute(IConstants.PERFILES, ub.getVPerfiles());
//	                       
//	            //se setea usuario_macval para usarlo en las paginas de macval
//	            if(ub.isAdmin()){
//	                session.setAttribute(IConstants.USUARIO_MACVAL,"admin");
//	            }else if(ub.isCons()){
//	            	session.setAttribute(IConstants.USUARIO_MACVAL,"consulta");
//	            }
//
//	            //Usuario de SUA para auditoria
//	            session.setAttribute(IConstants.USUARIO_SUA, ub.getUserSua());
//	            logger.debug("Usuario de SUA: " + ub.getUserSua());
//	            
//	            forward = mapping.findForward(IConstants.SHOW);
	                    
	        }
			
//			this.logger.Comentario(this, "USUARIO: " + username);
//			UsuarioVO usuarioVo = new UsuarioVO();
//			usuarioVo.setNombre(username);
//			final String fwdName = checkUser(request, errors, usuarioVo);
//			this.logger.Comentario(this, "HACIENDO FWD: " + username);
//			return mapping.findForward(fwdName);
		
		}
		
		
		
		
		
		
		
		
		
		boolean isGetAccess = (request.getRemoteUser() != null && request.getRemoteUser().trim().length() > 0);
	
		
		this.logger.Comentario(this, "Autenticacion a traves de GETACCESS: " + isGetAccess);
		this.logger.Comentario(this, "Iniciando Autentificacion del Usuario");

		if (isGetAccess){
			String username = request.getRemoteUser(); 
			this.logger.Comentario(this, "USUARIO: " + username);
			UsuarioVO usuarioVo = new UsuarioVO();
			usuarioVo.setNombre(username);
			final String fwdName = checkUser(request, errors, usuarioVo);
			this.logger.Comentario(this, "HACIENDO FWD: " + username);
			return mapping.findForward(fwdName);
		}
		
        // Si la autenticacion se hace a traves del login de accros.
		final UsuarioVO usuarioVo = uf.getVO();
	
		this.logger.Comentario(this, "USUARIO: " + usuarioVo.getNombre());
		//this.logger.Comentario(this, "PASSWORD: " + usuarioVo.getPassword());
		
		if (uf == null || StringUtils.isEmpty(uf.getUsuario()) || StringUtils.isEmpty(uf.getPassword())) {
            errors.add("errorldap", new ActionMessage("accros.error.login.ldap"));
			this.logger.Comentario(this, "El usuario o el password es invalido");
			
		}else{
			try {
				this.logger.Comentario(this, "AUTENTICANDO USUARIO CONTRA SERVIDOR LDAP...");
				if (!ldapService.authenticateUser(usuarioVo)) {
					this.logger.Comentario(this, "El usuario no existe en el directorio de LDAP");
                    errors.add("errorldap", new ActionMessage("accros.error.login.ldap"));
				}
				this.logger.Comentario(this, "USUARIO AUTENTICADO.");
				
			} catch (Exception e) {
				this.logger.ErrorSistema("LOGIN", "ERROR SISTEMA: "+e.getMessage());
                errors.add("errorldap", new ActionMessage("accros.error.login.ldap2"));
			}
		}

		final String fwdName = checkUser(request, errors, usuarioVo);
		this.logger.Comentario(this, "HACIENDO FWD: " + fwdName);
		
		return mapping.findForward(fwdName);
	}

	/**
	 * @param mapping
	 * @param request
	 * @param errors
	 * @param usuarioVo
	 * @return
	 */
	private String checkUser(HttpServletRequest request, ActionErrors errors, final UsuarioVO usuarioVo) {
		try {
			// Authenticacion Tablas
			this.logger.Comentario(this, "AUTENTICANDO USUARIO CONTRA TABLA PGSM_USUARIOS: " + usuarioVo.getNombre());
			List lista = this.loginService.obtenerCodigoUsuario(usuarioVo.getNombre());
			
			if (lista.size() == 0){
				this.logger.Comentario(this, "El usuario no existe en la tabla PGSM_USUARIOS");
                errors.add("errortabla",new ActionMessage("accros.error.login.noexiste"));
			
			}else{
				this.logger.Comentario(this, "USUARIO AUTENTICADO.");
				usuarioVo.setUsuario(((UsuarioVO)lista.get(0)).getUsuario());
			}
		
		} catch (Exception e) {
			this.logger.ErrorSistema("LOGIN", "ERROR SISTEMA: "+e.getMessage());
            errors.add("errordb", new ActionMessage("accros.error.login.errordb.obtenerCodigoUsuario"));
		}
		
		if (errors.size() > 0) {
			this.saveErrors(request, errors);
			return Constants.ACTIONS.FAILURE;
		}
		
		return Constants.ACTIONS.SUCCESS;
	}
 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * @param pLoginService the loginService to set
	 */
	public void setLoginService(LoginService pLoginService) {
		this.loginService = pLoginService;
	}

	/**
	 * @param pLdapService the ldapService to set
	 */
	public void setLdapService(LdapService pLdapService) {
		this.ldapService = pLdapService;
	}
}
