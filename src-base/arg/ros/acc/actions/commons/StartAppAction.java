package arg.ros.acc.actions.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.ContextUtils;

/**
 * @author rosario.argentina
 *
 */
public class StartAppAction extends BaseAction {
	
	/**
	 * 
	 */
	private static final String FWD_JSP_VIEW_LOGIN = "viewLogin";
	
	/**
	 * 
	 */
	private static final String FWD_JSP_VIEW_APPLICATION = "viewApplication";
	
	/**
	 * 
	 */
	private static final String FWD_JSP_VIEW_LICENSE = "viewLicense";
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewApplication(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(FWD_JSP_VIEW_APPLICATION);
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewLicense(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(FWD_JSP_VIEW_LICENSE);
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward viewLogin(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		session.invalidate();
		
		return mapping.findForward(FWD_JSP_VIEW_LOGIN);
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward acceptAgreement(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
        ContextUtils.set(Constants.ACCROS.APP_LICENSE_AGREMENT, Boolean.TRUE, Constants.SCOPES.SESSION_SCOPE, pRequest);
		
		return pMapping.findForward(FWD_JSP_VIEW_APPLICATION);
	}
	
}
