package arg.ros.acc.actions.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.services.commons.LupaService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonFilterResponse;

public class LupaInversaAction extends LupaAction {
	/**
	 * 
	 */
	protected static final String STR_NO_RECORD_FOUND = "[ C�digo Nuevo ]";
	

	/**
	 * 
	 */
	protected static final String STR_RECORD_FOUND = "[ C�digo Existente ]";

	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDescription(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final JsonActionForward jaf = (JsonActionForward) super.filtrar(pMapping, pForm, pRequest, pResponse);
		final JsonFilterResponse jfr = (JsonFilterResponse) jaf.getData();
		
		if (jfr.getTotalCount() == LupaAction.INT_NO_RECORD_FOUND) {
			JsonActionResponse response = new JsonActionResponse(new Object(), LupaInversaAction.STR_NO_RECORD_FOUND);
			response.setSuccess(Boolean.TRUE);
						
			return new JsonActionForward(response);
		} else {
			JsonActionResponse response = new JsonActionResponse(LupaInversaAction.STR_RECORD_FOUND);
			response.setSuccess(Boolean.FALSE);
			
			return new JsonActionForward(response);
		}
	}
	
	/**
	 * @param pLupaService the lupaService to set
	 */
	public void setLupaService(LupaService pLupaService) {
		super.lupaService = pLupaService;
	}
}
