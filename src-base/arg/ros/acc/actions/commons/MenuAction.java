 




 




 



package arg.ros.acc.actions.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.ContextUtils;
import arg.ros.acc.components.menu.Application;
import arg.ros.acc.forms.commons.MenuForm;

/**
 * @author rosario.argentina
 *
 */
public class MenuAction extends Action {

	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final String selectedItem = ((MenuForm)form).getSelectedItem();
        final Application application = (Application) ContextUtils.get(Constants.ACCROS.APP_MENU, request);
		
		if (application != null) {
//			Nodo menu = application.getMenu(selectedItem);
//			menu.setOpen(!menu.isOpen());
//			
//			if (menu.isOpen()) {
//				application.setSelectedMenu(menu);
//			}
		}
		return null;
	}
}
