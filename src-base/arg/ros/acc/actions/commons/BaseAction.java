package arg.ros.acc.actions.commons;

import java.io.BufferedWriter;

import java.io.FileNotFoundException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.struts.DispatchActionSupport;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.config.ConfigParams;
import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.commons.util.Container;
import arg.ros.acc.commons.util.ContextUtils;
import arg.ros.acc.commons.util.AccRosUtils;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.filter.impl.FilterImpl;
import arg.ros.acc.components.menu.Application;
import arg.ros.acc.components.menu.MenuComponent;
import arg.ros.acc.components.menu.Nodo;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.components.paging.impl.PaginatedListImpl;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonData;
import arg.ros.acc.vo.commons.UsuarioVO;

  


/**
 * @author rosario.argentina
 *
 */
public abstract class BaseAction extends DispatchActionSupport {


	/**
	 * Constante VACIO. 
	 */
	private static final String BLANK = "";
	
	/**
	 * Sufijo utilizado para obtener los valores de los campos que forman parte de un filtro.
	 */
	private static final String FILTER_SUFFIX = "Filter";
	
	/**
	 * Forward a la JSP definida en la lista de forwards del action. 
	 */
	protected static final String FWD_JSP_VIEW = "view";

	/**
	 * 
	 */
	protected ConfigParams configParams;
	
	/**
	 * 
	 */
	private MenuComponent menuComponent;
	
	/**
	 * 
	 */
	protected LoggerWeb logger;
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	protected void beforeExecution(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	protected void afterExecution(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
	}
		
	
	//Este metodo se ejecuta siempre que se llame a alguna action 
	
	
	protected ActionForward dispatchMethod(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, String method) throws Exception {
		
		initializeUserMenu(mapping, form, request, response);
		initilizeUser(request);
		
		// Hook - Antes de la ejecucion.
		this.beforeExecution(mapping, form, request, response);
		
		if (method.equalsIgnoreCase("loadData") && StringUtils.isNotBlank(request.getParameter("sort"))){
			method = new String("filtrar");	
		}
		Object result = super.dispatchMethod(mapping, form, request, response, method);
		
		// Hook - Despues de la ejecucion.
		this.afterExecution(mapping, form, request, response);

		// Evaluacion de la respuesta.
		final ActionForward forward = this.evaluateResponse(result, response);
		
		return forward;
	}
	
	
	

	/**
	 * @param pRequest
	 * @param pForm
	 * @return
	 */
	private ActionForward checkAccess(HttpServletRequest pRequest, ActionForm pForm) {
        if (AccRosUtils.isPublicURI(pRequest)) {
			return null;
		}
		
		return null;
	}

	/**
	 * @param pForward
	 * @param pHttpServletResponse
	 * @return
	 * @throws Exception
	 */
	private ActionForward evaluateResponse(final Object pForward, final HttpServletResponse pHttpServletResponse) throws Exception {
		if (!(pForward instanceof JsonActionForward)) {
			return (ActionForward) pForward;			
		}
		
		final JsonActionForward jsonActionForward = (JsonActionForward) pForward;
		final JsonData data = (JsonData)jsonActionForward.getData();
		this.logJsonData(data.asJson());
		System.out.println(data.asJson());
		if (data != null) {
            pHttpServletResponse.setCharacterEncoding(Constants.ACCROS.APP_CHARSET_ENCODING_UTF8);
			pHttpServletResponse.setContentType("text/html; charset=UTF-8");
			Writer writer = new BufferedWriter(new OutputStreamWriter(pHttpServletResponse.getOutputStream(), "UTF8"));
			writer.write(data.asJson());
			writer.flush();
			writer.close();
		}
		return null;
	}

	/**
	 * 
	 * @param jsonData
	 */
	private void logJsonData(String jsonData) {
		String s = "";
		if (this.logger == null) return;
		try {
			if (jsonData == null) {
				this.logger.Comentario(this, "JsonData:: Null");
			} else {
				this.logger.Comentario(this, "JsonData:: ");
				for (int i = 1; i < jsonData.length(); i++) {
					s += jsonData.substring(i, i + 1);
					if (s.length() == 400) {
						this.logger.Comentario(this, s);
						s = new String("");
					}
				}
				this.logger.Comentario(this, s);
			}
		}catch (Exception e) {
			this.logger.Comentario(this, "JsonData:: Exception: " + e.getMessage());
		}
		
	}
		
	
	/**
	 * @param request
	 */
	private void initilizeUser(HttpServletRequest request) {
        final UsuarioVO usuarioVO = (UsuarioVO) ContextUtils.get(Constants.ACCROS.APP_USUARIO, request);
		if (usuarioVO != null) {
            Container.store(Constants.ACCROS.APP_USUARIO, usuarioVO);
		}
	}

	/**
	 * @param request
	 */
	private void initializeUserMenu(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) {
        
		
		
		Application application = (Application) ContextUtils.get(Constants.ACCROS.APP_MENU, Constants.SCOPES.SESSION_SCOPE, pRequest);
	
        
        String requestedUrl = pRequest.getRequestURI();
		
		if (application == null) {
			try {
				application = menuComponent.read();

			} catch (FileNotFoundException e) {
				logger.Comentario(this, "No se pudo leer el fichero de menu. Mensaje: " + e.getMessage());
			}

            ContextUtils.set(Constants.ACCROS.APP_MENU, application, Constants.SCOPES.SESSION_SCOPE, pRequest);
			
		} else if (requestedUrl.indexOf("/app/") != -1) {
			String action = requestedUrl.substring(requestedUrl.indexOf("/app/"), requestedUrl.lastIndexOf(".do"));
			
			Nodo selectedNode = application.getNodoByPath(application.getItems(), action);
            ContextUtils.set(Constants.ACCROS.APP_MENU, application, Constants.SCOPES.SESSION_SCOPE, pRequest);
			if (selectedNode != null) {
				//selectedNode.setSelected(true);
				
			}
		}
	}

	/**
	 * @param pHttpServletRequest
	 * @return
	 */
	protected PaginatedList getPaginatedList(final HttpServletRequest pHttpServletRequest) {
		final String startParameter = pHttpServletRequest.getParameter(Constants.PAGING.START);
		final String limitParameter = pHttpServletRequest.getParameter(Constants.PAGING.LIMIT);
		
		int start = 0;
		int limit = Constants.PAGING.GRID_RECORDS_PER_PAGE;
		
		if (StringUtils.isNotBlank(startParameter)) {
			start = Integer.valueOf(startParameter).intValue();
		}
		if (StringUtils.isNotBlank(limitParameter)) {
			limit = Integer.valueOf(limitParameter).intValue();
		}
		
		return new PaginatedListImpl(start, limit);
	}
	
	/**
	 * Metodo usado por 
	 * @param pRequest
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Filter getFilter(final HttpServletRequest pRequest) {
		
		final Map filterProperties = new HashMap(); 
		final Map requestMap = pRequest.getParameterMap();

		final Set keys = requestMap.keySet();
		for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
			final String key = (String) iterator.next();
		
			if (!StringUtils.contains(key, BaseAction.FILTER_SUFFIX)){
				continue;
			}
			
			final String value = ((String[])requestMap.get(key))[0];
			final String newKey = StringUtils.replace(key, BaseAction.FILTER_SUFFIX, BaseAction.BLANK);
			
			if (value != null && StringUtils.isNotEmpty(value) && StringUtils.isNotBlank(value)) {
			//Agregado manfredi para que se pase el filter a mayuscula
			 
				filterProperties.put(newKey, this.convertFromUTF8(value.toUpperCase()));
			}
		}
		
		final String columnParameter = pRequest.getParameter("sort");
		final String dirParameter = pRequest.getParameter("dir");
		if (StringUtils.isNotBlank(columnParameter) && StringUtils.isNotBlank(dirParameter)) {
            filterProperties.put("sortDirection", dirParameter.toUpperCase());
            filterProperties.put("sortCriterion", columnParameter.toUpperCase());
		}
		return new FilterImpl(filterProperties); 
	}

	/**
	 * Metodo usado solo por lupafield
	 * @param pRequest
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Filter getFilterLupa(final HttpServletRequest pRequest) {
		
		boolean isIEBrowser = detectUserAgent(pRequest);
		boolean isLocalHost = detectLocalHost(pRequest);
		
		final Map filterProperties = new HashMap(); 
		final Map requestMap = pRequest.getParameterMap();

		final Set keys = requestMap.keySet();
		for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
			final String key = (String) iterator.next();
		
			if (!StringUtils.contains(key, BaseAction.FILTER_SUFFIX)){
				continue;
			}
			
			final String value = ((String[])requestMap.get(key))[0];
			final String newKey = StringUtils.replace(key, BaseAction.FILTER_SUFFIX, BaseAction.BLANK);
			
			if (value != null && StringUtils.isNotEmpty(value) && StringUtils.isNotBlank(value)) {
				
				if (isLocalHost){
					//en local server
					if (isIEBrowser){
						filterProperties.put(newKey, this.convertFromHTML_ISO_8859_1(value));    
					}else{
						filterProperties.put(newKey, this.convertFromHTML_UTF8(value));
					}
				}else{
					//en remote server
					filterProperties.put(newKey, this.convertFromHTML_UTF8(value));                    
				}
			}
		}

		final String columnParameter = pRequest.getParameter("sort");
		final String dirParameter = pRequest.getParameter("dir");
		if (StringUtils.isNotBlank(columnParameter) && StringUtils.isNotBlank(dirParameter)) {
			filterProperties.put("sortDirection", dirParameter.toUpperCase());
            filterProperties.put("sortCriterion", columnParameter.toUpperCase());
		}
		
		return new FilterImpl(filterProperties); 
	}
	
	/** 
	 * Convert from UTF-8 encoded HTML-Pages -> internal Java String Format 
	 **/
	protected String convertFromUTF8(String s) {
		String out = null;
		try {
			out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
	
		} catch (java.io.UnsupportedEncodingException e) {
			return null;
		}
		return out;
	}

	/** 
	 * Convert from UTF-8 encoded HTML-Pages -> internal Java String Format
	 * Use for GET in NOT IE 
	 **/
	protected String convertFromHTML_UTF8(String s) {
		String out = null;
		try {
			out = new String(s.getBytes("UTF-8"), "UTF-8");
		} catch (java.io.UnsupportedEncodingException e) {
			return null;
		}
		return out;
	}

	/** 
	 * Convert from UTF-8 encoded HTML-Pages -> internal Java String Format
	 * Use for GET in IE 
	 **/
	protected String convertFromHTML_ISO_8859_1(String s) {
		String out = null;
		try {
			out = new String(s.getBytes("ISO-8859-1"), "UTF-8");
		} catch (java.io.UnsupportedEncodingException e) {
			return null;
		}
		return out;
	}
	
	/**
	 * Convert from internal Java String Format -> UTF-8 encoded HTML/JSP-Pages  
	 **/
	private String convertToUTF8(String s) {
		String out = null;
		try {
			out = new String(s.getBytes("UTF-8"));
		} catch (java.io.UnsupportedEncodingException e) {
			return null;
		}
		return out;
	}

	
	/**
	 * @param menuComponent the menuComponent to set
	 */
	public void setMenuComponent(MenuComponent menuComponent) {
		this.menuComponent = menuComponent;
	}

	/**
	 * @param pLogger the logger to set
	 */
	public void setLogger(LoggerWeb pLogger) {
		this.logger = pLogger;
	}

	/**
	 * @param pConfigParams the configParams to set
	 */
	public void setConfigParams(ConfigParams pConfigParams) {
		this.configParams = pConfigParams;
	}
	
	/**
	 * Deteccion de IE /Other Browser
	 * @param pRequest
	 * @return  
	 */
	private boolean detectUserAgent(final HttpServletRequest pRequest){
		String userAgent = pRequest.getHeader("User-Agent");  
		String user = userAgent.toLowerCase();  
		if(user.indexOf("msie") != -1) {  
		    return true;  
	     }
		return false;
	}

	/**
	 * 
	 * @param pRequest
	 * @return
	 */
	private boolean detectLocalHost(final HttpServletRequest pRequest){
		String hostRec = pRequest.getHeader("host");
		hostRec = hostRec.toLowerCase();
		if((hostRec.indexOf("127.0.0.1") != -1) || (hostRec.indexOf("localhost")!= -1) ) {  
		    return true;  
	     }
		return false;
	}
}
