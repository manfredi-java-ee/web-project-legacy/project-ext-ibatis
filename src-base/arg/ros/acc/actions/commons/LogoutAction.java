package arg.ros.acc.actions.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.Container;
import arg.ros.acc.commons.util.ContextUtils;
import arg.ros.acc.services.commons.LogoutService;
import arg.ros.acc.vo.commons.UsuarioVO;

/**
 * @author rosario.argentina
 *
 */
public class LogoutAction extends BaseAction {

	/**
	 * Logout Service.
	 */
	private LogoutService logoutService;
	
	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward doLogout(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
        final UsuarioVO usuarioVO = (UsuarioVO) ContextUtils.get(Constants.ACCROS.APP_USUARIO, pRequest);
		
		if (usuarioVO != null) {
			this.logoutService.logoutUser(pRequest, usuarioVO);
			Container.borrarUser();
		}
		
		return pMapping.findForward(BaseAction.FWD_JSP_VIEW);
	}

	/**
	 * @param pLogoutService the logoutService to set
	 */
	public void setLogoutService(LogoutService pLogoutService) {
		this.logoutService = pLogoutService;
	}
}
