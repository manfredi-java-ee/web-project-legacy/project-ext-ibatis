 




 




 



package arg.ros.acc.actions.commons;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.AbstractWriter;
import arg.ros.acc.commons.util.ExcelWriter;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.forms.commons.ExportForm;
import arg.ros.acc.services.commons.ExportService;

/**
 * @author rosario.argentina
 *
 */
public class ExportAction extends BaseAction {

	/**
	 * 
	 */
	private static final String XLS_EXTENSION = ".xls";
	
	/**
	 * 
	 */
	private static final String EXPORT_PREFIX = "Grid-";

	/**
	 * 
	 */
	private static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";

	/**
	 * 
	 */
	private static final String CONTENT_DISPOSITION_ATTACHMENT = "attachment;filename=";
	
	/**
	 * 
	 */
	private ExportService exportService;
	
	/**
	 * 
	 */
	private static final String NOMBRE_ARCHIVO_EXCEL_CONFACTUESPECIAL_OLD = "ConFactuEspecial";
	
	/**
	 * 
	 */
	private static final String NOMBRE_ARCHIVO_EXCEL_CONFACTUESPECIAL_NEW = "ConFactu";
	
	/**
	 * 
	 */
	
	/**
	 * Metodo mantenido para mantener los export antiguos
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward exportExcel(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final Filter filter = this.getFilter(pRequest);
		String gridName = ((ExportForm) pForm).getGridName();
		String[] colums = null;
		String gridColum = ((ExportForm) pForm).getGridColum();
		
		if ((!StringUtils.isEmpty(gridColum)) && (!gridColum.trim().equals(""))){
			colums = gridColum.split(",");
		}

		final List records = exportService.export(gridName, filter);
		
		if (records.size() > 0) {
			boolean isUsed = false;
			Map<String, String> pvMap = null;
			
            if (Constants.ACCROS.ENTIDAD_PLANLOLLEFAC.equals(gridName)){
                pvMap = exportService.getColumnsHeadersMap(gridName + Constants.ACCROS.MAPEO_COLUMNAS);
				isUsed = true;
			}
			
			if (!isUsed){
				pvMap = exportService.getColumnsHeadersMap(gridName);	
			}
			
			if (this.NOMBRE_ARCHIVO_EXCEL_CONFACTUESPECIAL_OLD.equals(gridName)){
				gridName = this.NOMBRE_ARCHIVO_EXCEL_CONFACTUESPECIAL_NEW;
			}
		
			final String filename = ExportAction.EXPORT_PREFIX + gridName + ExportAction.XLS_EXTENSION;
			final String contentType = ExportAction.CONTENT_TYPE_EXCEL;
			final String header = ExportAction.CONTENT_DISPOSITION_ATTACHMENT + filename;
			final ExcelWriter excelWriter;
//#ADC#060111#TDE#312512#INICIO
			//verificar si las grillas requieren tratamiento diferencial en precisión de punto flotante
            if (!gridName.trim().equalsIgnoreCase(Constants.ACCROS.GRILLA_COMPEMPR) && !gridName.trim().equalsIgnoreCase(Constants.ACCROS.GRILLA_COMPABON)) {
				excelWriter = new ExcelWriter(records, pvMap, colums);
			}
			else {
				Map<String, String> pProps = new LinkedHashMap<String, String>();
				pProps.put("gridName", gridName);
                pProps.put("columnaPuntoFlotanteEspecial", Constants.ACCROS.COLUMN_TOTCOMPROM);
				excelWriter = new ExcelWriter(records, pvMap, colums, pProps);
			}
//#ADC#060111#TDE#312512#FIN
			this.writeFile(excelWriter, contentType, header, pResponse);
		}
		return null;
	}
	
	/**
	 * Metodo utilizado para reusar el metodo "obtenerFiltro" del servicio
	 */
	@SuppressWarnings({ "unused", "static-access", "unchecked" })
	public ActionForward exportExcelEspecial(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final Filter filter = this.getFilter(pRequest);
		String gridName = ((ExportForm) pForm).getGridName();
		List records = null;
		String[] colums = null;
		String gridColum = ((ExportForm) pForm).getGridColum();
		
		if ((!StringUtils.isEmpty(gridColum)) && (!gridColum.trim().equals(""))){
			colums = gridColum.split(",");
		}
		
	
		if (records.size() > 0) {
            Map<String, String> pvMap = exportService.getColumnsHeadersMap(gridName + Constants.ACCROS.MAPEO_COLUMNAS);
			final String filename = ExportAction.EXPORT_PREFIX + gridName + ExportAction.XLS_EXTENSION;
			final String contentType = ExportAction.CONTENT_TYPE_EXCEL;
			final String header = ExportAction.CONTENT_DISPOSITION_ATTACHMENT + filename;
			final ExcelWriter excelWriter = new ExcelWriter(records, pvMap, colums);
			this.writeFile(excelWriter, contentType, header, pResponse);
		}
		return null;
	}
	
	/**
	 * @param pRecords
	 * @param pResponse
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 */
	private void writeFile(AbstractWriter pWriter, String pContentType, String pHeader, HttpServletResponse pResponse) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, IOException {
		final OutputStream outStream = pResponse.getOutputStream();
        final String contentType = Constants.ACCROS.APP_CONTENT_TYPE + pContentType;

        pResponse.reset();	        
		pResponse.addHeader("cache-control", "must-revalidate");
		
		pResponse.setContentType(contentType);
        pResponse.setHeader(Constants.ACCROS.APP_CONTENT_DISPOSITION, pHeader);

        final byte[] arrBytes = pWriter.write();
        outStream.write(arrBytes);
		outStream.flush();
		outStream.close();
		
	}

	/**
	 * @param pExportService the exportService to set
	 */
	public void setExportService(ExportService pExportService) {
		this.exportService = pExportService;
	}

	/**
	 * @param modCobroService
	 */
	
}
