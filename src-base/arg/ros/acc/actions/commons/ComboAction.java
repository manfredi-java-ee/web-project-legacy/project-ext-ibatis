package arg.ros.acc.actions.commons;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.commons.ComboForm;
import arg.ros.acc.services.commons.ComboService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;

/**
 * @author rosario.argentina
 *
 */
public class ComboAction extends BaseAction {

	/**
	 * 
	 */
	private static final String STR_NO_RECORD_FOUND = "Registro No Encontrado";

	/**
	 * 
	 */
	private static final int INT_NO_RECORD_FOUND = 0;

	/**
	 * 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private ComboService comboService;
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(ComboAction.FWD_JSP_VIEW);
	}
	
	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadLimit(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final String gridName = ((ComboForm) pForm).getEntityName();
		PaginatedList paginatedList = getPaginatedList(pRequest);
		
		paginatedList = comboService.obtener(gridName, paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}

	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward load(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final String gridName = ((ComboForm) pForm).getEntityName();
		PaginatedList paginatedList = getPaginatedList(pRequest);
		
		paginatedList = comboService.obtener(gridName, paginatedList, false);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}

	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrarLimit(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final String gridName = ((ComboForm) pForm).getEntityName();
		final Filter filter = getFilter(pRequest);
		PaginatedList paginatedList = getPaginatedList(pRequest);
		
		paginatedList = comboService.filtrar(gridName, paginatedList, filter);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}

	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final String gridName = pRequest.getParameter("entityName");//((ComboForm) pForm).getEntityName();
		final Filter filter = getFilter(pRequest);
		PaginatedList paginatedList = getPaginatedList(pRequest);
		
		paginatedList = comboService.filtrar(gridName, paginatedList, filter, false);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}

	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDescription(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final JsonActionForward jaf = (JsonActionForward) this.filtrar(pMapping, pForm, pRequest, pResponse);
		final JsonListResponse jlr = (JsonListResponse) jaf.getData();
		final List data = (List) jlr.getData();
		
		if (jlr.getTotalCount() == ComboAction.INT_NO_RECORD_FOUND) {
			return new JsonActionForward(new JsonActionResponse(ComboAction.STR_NO_RECORD_FOUND));
		} else {
			return new JsonActionForward(new JsonActionResponse(data));
		}
	}
	
	/**
	 * @param pComboService the comboService to set
	 */
	public void setComboService(ComboService pComboService) {
		this.comboService = pComboService;
	}
}
