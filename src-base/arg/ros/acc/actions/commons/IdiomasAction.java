 




 




 



package arg.ros.acc.actions.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.services.commons.IdiomasService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;

/**
 * @author rosario.argentina
 *
 */
public class IdiomasAction extends BaseAction {
	
	/**
	 * 
	 */
	private IdiomasService idiomasService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.idiomasService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.idiomasService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pIdiomasService the idiomasService to set
	 */
	public final void setIdiomasService(final IdiomasService pIdiomasService) {
		this.idiomasService = pIdiomasService;
	}

}
