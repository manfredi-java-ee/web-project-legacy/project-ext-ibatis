 




 




 



package arg.ros.acc.actions.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.commons.LupaForm;
import arg.ros.acc.services.commons.LupaService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonFilterResponse;
import arg.ros.acc.struts.JsonListResponse;

/**
 * @author rosario.argentina
 *
 */
public class LupaAction extends BaseAction {

	private static final String STR_NO_RECORD_FOUND = "Registro No Encontrado";

	/**
	 * 
	 */
	protected static final int INT_NO_RECORD_FOUND = 0;

	/**
	 * 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	protected LupaService lupaService;
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(LupaAction.FWD_JSP_VIEW);
	}
	
	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward load(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final String gridName = ((LupaForm) pForm).getEntityName();
		PaginatedList paginatedList = getPaginatedList(pRequest);
		
		paginatedList = lupaService.obtener(gridName, paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}

	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final String gridName = ((LupaForm) pForm).getEntityName();
		final Filter filter = getFilterLupa(pRequest);
		PaginatedList paginatedList = getPaginatedList(pRequest);
		
		paginatedList = lupaService.filtrar(gridName, paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}

	/**
	 * @param pMapping
	 * @param pForm
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws Exception
	 */
	public ActionForward getDescription(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse) throws Exception {
		final JsonActionForward jaf = (JsonActionForward) this.filtrar(pMapping, pForm, pRequest, pResponse);
		final JsonFilterResponse jfr = (JsonFilterResponse) jaf.getData();
		
		if (jfr.getTotalCount() == LupaAction.INT_NO_RECORD_FOUND) {
			return new JsonActionForward(new JsonActionResponse(LupaAction.STR_NO_RECORD_FOUND));
		} else {
			return jaf;
		}
	}
	
	/**
	 * @param pLupaService the lupaService to set
	 */
	public void setLupaService(LupaService pLupaService) {
		this.lupaService = pLupaService;
	}
}
