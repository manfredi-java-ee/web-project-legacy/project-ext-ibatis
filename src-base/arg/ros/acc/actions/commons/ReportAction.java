 




 




 



package arg.ros.acc.actions.commons;

import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.forms.commons.ReportForm;
import arg.ros.acc.services.commons.ReportService;

/**
 * @author jc053 - 25/03/2009
 *
 */
public class ReportAction extends BaseAction {

    /**
     * 
     */
    private static final String PDF_EXTENSION = ".pdf";

	/**
	 * 
	 */
	private static final String REPORT_PREFIX = "Report-";

	/**
	 * 
	 */
	private static final String CONTENT_DISPOSITION_INLINE = "inline; filename=";

	/**
	 * 
	 */
	private static final String CONTENT_DISPOSITION_ATTACHMENT = "attachment;filename=";
	
	/**
     * 
     */
    private static final String CONTENT_TYPE_PDF = "application/pdf";
    
	/**
     * 
     */
    private ReportService reportService;

    /**
     * @param pMapping
     * @param pForm
     * @param pRequest
     * @param pResponse
     * @return
     * @throws Exception
     */
    public ActionForward getReport(ActionMapping pMapping, ActionForm pForm, HttpServletRequest pRequest, HttpServletResponse pResponse)  throws Exception {
    	
        	final Filter filter = this.getFilter(pRequest);
        	final String reportName = ((ReportForm) pForm).getReportName();

        	pResponse.reset();
        	pResponse.resetBuffer();
        	
    		final OutputStream outStream = pResponse.getOutputStream();
    		
    		//final byte[] report = null;
    		byte[] report = null;

    		try {
        		report = reportService.generateReport(reportName, filter);
        		this.logger.Comentario(ReportAction.class, "ActionForward.getReport.reportName:" + reportName.toString());
        		
	    	} catch (Exception e1) {
				e1.printStackTrace();
				throw new Exception("Error en ReportAction -> generateReport -> report: " + report + " filter: " + filter.getProperties().toString(), e1);
			} 
    		
    		if (report == null) {
    			return null;
    		}
    		
    		final String filename = ReportAction.REPORT_PREFIX + reportName + ReportAction.PDF_EXTENSION;
    		//pResponse.setContentType(ReportAction.CONTENT_TYPE_PDF);
    		pResponse.setContentType("application/pdf");
    		
            //pResponse.setHeader(Constants.ACCROS.APP_CONTENT_DISPOSITION, ReportAction.CONTENT_DISPOSITION_ATTACHMENT + filename);
    		pResponse.setHeader("Content-Disposition", "inline;filename=\"LicenseAgreement.pdf\"");
    		
    		pResponse.setContentLength(report.length);	
    		
    		pResponse.setHeader("Cache-Control", "cache"); // to make IE work!
    		pResponse.setHeader("Pragma", "cache"); // to make IE work!
    		pResponse.setDateHeader("Expires", 1);

    		outStream.write(report);
            outStream.flush();
            outStream.close();	

            return null;
    	
	}

    /**
     * @param repService
     */
    public void setReportService(ReportService repService) {
    	this.reportService = repService;
    }
}
