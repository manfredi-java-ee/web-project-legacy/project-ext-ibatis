 




 




 



package arg.ros.acc.components.paging;


/**
 * @author rosario.argentina
 *
 */
public interface PaginatedList {

	/**
	 * @return
	 */
	int getFullListSize();

	/**
	 * @return
	 */
	Object getData();

	/**
	 * @return
	 */
	int getObjectsPerPage();
	
	/**
	 * @param pPageSize
	 */
	void setObjectsPerPage(final int pPageSize);
	
	/**
	 * @return
	 */
	int getPageNumber();
    
	/**
	 * @return
	 */
	String getSearchId();
	
	/**
	 * @return
	 */
	String getSortCriterion();

	/**
	 * @return
	 */
	int getSortDirection(); 

	/**
	 * @param pData the data to set
	 */
	void setData(Object pData);

	/**
	 * @param pFullListSize
	 */
	void setFullListSize(int pFullListSize);

}
