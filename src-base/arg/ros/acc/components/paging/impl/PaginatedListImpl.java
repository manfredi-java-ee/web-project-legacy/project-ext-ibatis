 




 




 



package arg.ros.acc.components.paging.impl;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import arg.ros.acc.components.paging.PaginatedList;

/**
 * @author rosario.argentina
 *
 */
public class PaginatedListImpl implements PaginatedList, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private Object data;

	/**
	 * 
	 */
	private int fullListSize;

	/**
	 * 
	 */
	private int objectPerPage;

	/**
	 * 
	 */
	private int pageNumber;

	/**
	 * 
	 */
	private String sortCriteria;

	/**
	 * 
	 */
	private int sortDirection;
	
	/**
	 * 
	 */
	public PaginatedListImpl(final int pStart, final int pLimit) {
		this.pageNumber = pStart;
		this.objectPerPage = pLimit;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Object getData() {
		return this.data;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getFullListSize() {
		return this.fullListSize;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getObjectsPerPage() {
		return this.objectPerPage;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setObjectsPerPage(int pPageSize) {
		this.objectPerPage = pPageSize;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int getPageNumber() {
		return this.pageNumber;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getSearchId() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getSortCriterion() {
		return sortCriteria;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getSortDirection() {
		return sortDirection;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setData(Object pData) {
		this.data = pData;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setFullListSize(int pFullListSize) {
		this.fullListSize = pFullListSize;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
