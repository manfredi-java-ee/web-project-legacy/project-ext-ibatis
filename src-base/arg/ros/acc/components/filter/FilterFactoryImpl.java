 




 




 



package arg.ros.acc.components.filter;

import java.util.HashMap;

import arg.ros.acc.components.filter.impl.FilterImpl;

/**
 * @author rosario.argentina
 *
 */
public class FilterFactoryImpl implements FilterFactory {

	private static FilterFactory instance = null;
	
	/**
	 * 
	 */
	private FilterFactoryImpl() { }

	/**
	 * 
	 */
	public static final FilterFactory getInstance() {
		if (instance == null) {
			instance = new FilterFactoryImpl();
		}
		return instance;
	}
	
	/* (non-Javadoc)
     * @see arg.ros.acc.components.filter.FilterFactory#emptyFilter()
	 */
	public Filter emptyFilter() {
		final Filter filter = new FilterImpl(new HashMap());
		return filter;
	}

}
