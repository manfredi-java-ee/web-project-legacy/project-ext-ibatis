 




 




 



package arg.ros.acc.components.filter;

/**
 * @author rosario.argentina
 *
 */
public interface FilterConstants {

	/**
	 * @author rosario.argentina
	 *
	 */
	interface PAGING {
		final String START = "start";
		final String LIMIT = "limit";
		final int SORT_DESC = 0;
		final int SORT_ASC = 1;
		final int GRID_RECORDS_PER_PAGE = 10;
	}
}
