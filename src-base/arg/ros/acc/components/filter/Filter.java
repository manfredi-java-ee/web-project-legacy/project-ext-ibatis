 




 




 



package arg.ros.acc.components.filter;

import java.util.Map;

/**
 * @author rosario.argentina
 *
 */
public interface Filter {

	/**
	 * @return
	 */
	Map getProperties();
	
	/**
	 * @param pProperty
	 * @param pValue
	 */
	void addFilterProperty(String pProperty, Object pValue);
	
	/**
	 * @param pPropertiesMap
	 */
	void addAll(Map pPropertiesMap);

	/**
	 * @param pObject
	 */
	void addObject(Object pObject, boolean pFilterNullProps);
}
