 




 




 



package arg.ros.acc.components.filter.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import arg.ros.acc.components.filter.Filter;

/**
 * @author rosario.argentina
 *
 */
public class FilterImpl implements Filter {

	/**
	 * 
	 */
	private Map<String, Object> properties = new HashMap<String, Object>();

	/**
	 * 
	 */
	public FilterImpl(final Map pProperties) {
		this.properties = pProperties;
	}
	
	/* (non-Javadoc)
     * @see arg.ros.acc.components.filter.Filter#addFilterProperty(java.lang.String, java.lang.Object)
	 */
	public void addFilterProperty(String pProperty, Object pValue) {
        this.properties.put(pProperty, pValue);
	}

	/* (non-Javadoc)
     * @see arg.ros.acc.components.filter.Filter#addAll(java.util.Map)
	 */
	public void addAll(Map pPropertiesMap) {
        this.properties.putAll(pPropertiesMap);
	}

	/* (non-Javadoc)
     * @see arg.ros.acc.components.filter.Filter#getProperties()
	 */
	public Map getProperties() {
		return this.properties;
	}
	
	/* (non-Javadoc)
     * @see arg.ros.acc.components.filter.Filter#emptyMap()
	 */
	public Map emptyMap() {
		return new HashMap();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void addObject(Object pObject, boolean pFilterNullProps) {
		final Map beanProps = new HashMap();
		try {
			final Map propertiesMap = BeanUtils.describe(pObject);
			beanProps.putAll(propertiesMap);
			
			if (pFilterNullProps) {
				for (Iterator iterator = propertiesMap.keySet().iterator(); iterator.hasNext();) {
					final String key = (String) iterator.next();
					if (propertiesMap.get(key) == null) {
						beanProps.remove(key);
					}
				}
			}
			
			this.properties = beanProps;
			
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
}
