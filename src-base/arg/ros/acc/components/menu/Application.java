/**
 * 
 */
package arg.ros.acc.components.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

/**
 * @author rosario.argentina
 *
 */
public class Application implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1114782573238301796L;
	
	/**
	 * 
	 */
	private List<Nodo> items;

	/**
	 * 
	 */
	private HashMap<String, Object> params;
	
	/**
	 * @return the items
	 */
	public List<Nodo> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<Nodo> items) {
		this.items = items;
	}

	public HashMap<String, Object> getParams() {
		if (this.params==null) {
			this.params = new HashMap<String, Object>();
		}
		return this.params;
	}
	
	/**
	 * @param pParamName
	 * @param pParamValue
	 */
	public void addParams(String pParamName, String pParamValue) {
		this.getParams().put(pParamName, pParamValue);
	}
	
	/**
	 * 
	 * @param pIdMenu
	 * @return
	 */
	public Nodo getMenu(String pIdMenu) {
		Nodo paramNodo = new Nodo();
		paramNodo.setId(pIdMenu);
		for(Nodo nodo: this.getItems()){
			if (nodo.equals(paramNodo)) {
				return nodo;
			}
		}
		return null;
	}

	/**
	 * @return
	 */
	public List<Nodo> getAllSubMenuItems() {
		final List<Nodo> subnodoItems = new ArrayList<Nodo>();
		for (Nodo menu: this.getItems()) {
			for (Nodo submenu: menu.getItems()) {
				subnodoItems.add(submenu);
			}
		}
		return subnodoItems;
	}
	
	/**
	 * @param pNodos
	 * @param pPath
	 * @return
	 */
	public Nodo getNodoByPath(List<Nodo> pNodos, String pPath) {
		Nodo selected = null;
		for (Nodo nodo : pNodos) {
			if (nodo.getPath() == null) {
				if (nodo.getItems() != null || nodo.getItems().size() > 0) {
					Nodo sn = this.getNodoByPath(nodo.getItems(), pPath);
					if (sn!=null) {
						return sn;
					}
				}
			} else {
				String pathNodo = StringUtils.substringBefore(nodo.getPath(), ".").toUpperCase();
				String requestedPath = StringUtils.substringBefore(pPath, ".").toUpperCase();
				if (StringUtils.equals(pathNodo, requestedPath)) {
					nodo.setSelected(true);
				} else {
					nodo.setSelected(false);
				}
			}
		}
		return selected;
	}

	/**
	 * @param pNodos
	 * @param pPath
	 */
	public void setNodeAsSelected(List<Nodo> pNodos, String pPath) {
		for (Nodo nodo : pNodos) {
			if (nodo.getPath() == null) {
				if (nodo.getItems() != null || nodo.getItems().size() > 0) {
					this.setNodeAsSelected(nodo.getItems(), pPath);
				}
			} else {
				String pathNodo = StringUtils.substringBefore(nodo.getPath(), ".").toUpperCase();
				String requestedPath = StringUtils.substringBefore(pPath, ".").toUpperCase();
				if (StringUtils.equals(pathNodo, requestedPath)) {
					nodo.setSelected(true);
				} else {
					nodo.setSelected(false);
				}
			}
		}
	}
	
	/**
	 * @param pListNodes
	 */
	public void removeItem(List<Nodo> pListNodes) {
        for (Iterator iterator = pListNodes.iterator(); iterator.hasNext();) {
			Nodo currentNode = (Nodo) iterator.next();
			if (currentNode == null) iterator.remove();
			List<Nodo> childs = currentNode.getItems();
			if (!CollectionUtils.isEmpty(childs)) {
				this.removeItem(childs);
				if (CollectionUtils.isEmpty(childs)) iterator.remove();
			} else {
				if (!currentNode.isRender()) {
					iterator.remove();
				}
			}
		}
	}
	
}
