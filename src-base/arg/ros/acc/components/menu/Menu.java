/**
 * 
 */
package arg.ros.acc.components.menu;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * @author rosario.argentina
 *
 */
public class Menu implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5571789479756250275L;

	/**
	 * 
	 */
	private String id;
	
	/**
	 * 
	 */
	private String label;
	
	/**
	 * 
	 */
	private boolean open;

	/**
	 * 
	 */
	private List items;

	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * @return the label + "..."
	 */
	public String getFormatedLabel() {
		if (getLabel().length() > 20) {
			return getLabel().substring(0,20)+"...";
		}
		return getLabel();
	}
	
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the items
	 */
	public List getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List items) {
		this.items = items;
	}

	/**
	 * @return the open
	 */
	public boolean isOpen() {
		return open;
	}

	/**
	 * @param open the open to set
	 */
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	
	
	
	/**
	 * @param pPath
	 * @return
	 */
	public Submenu getSubmenuByPath(final String pPath) {
		for (Iterator iterator = this.items.iterator(); iterator.hasNext();) {
			Submenu submenu = (Submenu) iterator.next();
			String action = submenu.getPath();
			int finish = action.lastIndexOf(".do");
			if (finish > 0) {
				action = action.substring(0, action.lastIndexOf(".do"));
				
				if (action.equals(pPath)) {
					return submenu;
				}
			}
		}
		return null;
	}

	/**
	 * Setea a false todos los seleccionados.
	 */
	public void reset() {
		for (Iterator iterator = this.items.iterator(); iterator.hasNext();) {
			Submenu submenu = (Submenu) iterator.next();
			submenu.setSelected(false);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object pMenu) {
		Menu menu = (Menu) pMenu;
		if (this.id.equals(menu.getId())) {
			return true;
		}
		return false;
	}

}
