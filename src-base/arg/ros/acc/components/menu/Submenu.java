/**
 * 
 */
package arg.ros.acc.components.menu;

import java.io.Serializable;


/**
 * @author rosario.argentina
 *
 */
public class Submenu implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -765099537907097490L;

	/**
	 * 
	 */
	private String id;
	
	/**
	 * 
	 */
	private String label;

	/**
	 * 
	 */
	private String path;

	/**
	 * 
	 */
	private boolean selected;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the label + "..."
	 */
	public String getFormatedLabel() {
		if (getLabel().length() > 20) {
			return getLabel().substring(0,20)+"...";
		}
		return getLabel();
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object pSubmenu) {
		Submenu menu = (Submenu) pSubmenu;
		if (this.id.equals(menu.getId())) {
			return true;
		}
		return false;
	}

}
