/**
 * 
 */
package arg.ros.acc.components.menu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;

import com.thoughtworks.xstream.XStream;

/**
 * @author rosario.argentina
 *
 */
public class MenuComponent {

	/**
	 * 
	 */
	private String fileName;
	
	/**
	 * 
	 */
	private boolean forceReRead;
	
	/**
	 * 
	 */
	private Application application;
	
	/**
	 * 
	 */
	private XStream xStream;
	
	/**
	 * @param pForceReRead
	 * @return
	 */
 	public Application read() throws FileNotFoundException {
 		return read(this.forceReRead);
 	}
	
	/**
	 * @param pFileName
	 * @param pForceReRead
	 * @return
	 */
	
	
	// Lee el menu de aplicacion 
	
	public Application read(boolean pForceReRead) throws FileNotFoundException {
		return readFromFile(this.fileName, pForceReRead);
	}

	/**
	 * @param pFileName
	 * @return
	 */
	public Application readFromFile(final String pFileName) throws FileNotFoundException {
		return readFromFile(pFileName, this.forceReRead);
	}
	
	/**
	 * @return
	 */
	private void parse() throws FileNotFoundException {
		InputStream is = new FileInputStream(new File(this.fileName));
		this.application = (Application) xStream.fromXML(is);
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 * @throws URISyntaxException 
	 */
	public void setFileName(String fileName) throws URISyntaxException {
		
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		
    	//	Obtenemos el path del fichero recuperándolo como recurso y 
	    // 	generando el path absoluto del fichero en el sistema.
		
		String fileNamePath = loader.getResource(fileName).toURI().getPath();		
		
		this.fileName = fileNamePath;
	}

	
	
	
	
	
	
	/**
	 * @return the forceReRead
	 */
	public boolean isForceReRead() {
		return forceReRead;
	}

	/**
	 * @param forceReRead the forceReRead to set
	 */
	public void setForceReRead(boolean forceReRead) {
		this.forceReRead = forceReRead;
	}
	
	
	public Application readFromFile(final String pFileName, boolean pForceReRead) throws FileNotFoundException {
		this.fileName = pFileName;

		if (this.fileName == null) {
			throw new NullPointerException("El nombre del archivo no puede ser nulo.");
		}
	
		if (application == null) {
			xStream = new XStream();
	
			xStream.alias("application", Application.class);
			xStream.addImplicitCollection(Application.class, "items", Nodo.class);
	
			xStream.alias("nodo", Nodo.class);
			xStream.useAttributeFor(Nodo.class, "id");
			xStream.useAttributeFor(Nodo.class, "label");
			xStream.useAttributeFor(Nodo.class, "path");
			xStream.addImplicitCollection(Nodo.class, "items", Nodo.class);
			
			parse();
			
		} else {
			if (pForceReRead) {
				parse();
			}	
		}
		return this.application; 
	}
	
	
}
