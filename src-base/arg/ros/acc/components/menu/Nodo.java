package arg.ros.acc.components.menu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import arg.ros.acc.commons.Constants;
import arg.ros.acc.commons.util.NodoAlphaComparator;

public class Nodo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -765099537907097490L;
	
	/**
	 * 
	 */
	private String id;
	
	/**
	 * 
	 */
	private String label;
	
	/**
	 * 
	 */
	private String tooltip;

	/**
	 * 
	 */
	private String path;

	/**
	 * 
	 */
	private String sortable;
	
	/**
	 * 
	 */
	//private boolean render = true;

	/**
	 * 
	 */
	private List<Nodo> items;
	
	/**
	 * 
	 */
	private boolean render;
	
	/**
	 * 
	 */
	private boolean selected;
	
	/**
	 * 
	 * @return
	 */
	public boolean isSortable() {
		boolean sortable2 = true;
		if (this.sortable != null && this.sortable.trim().equalsIgnoreCase("false")) sortable2= false;
		return sortable2;
	}

	/**
	 * 
	 * @param sortable
	 */
	public void setSortable(String sortable) {
		this.sortable = sortable;
	}

	/**
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getLabel() {
        if (label.length() > Constants.ACCROS.APP_MENU_LONG_DETALLE) {
            return label.substring(0,Constants.ACCROS.APP_MENU_LONG_DETALLE)+"...";
		}
		return label;
	}
	
	/**
	 * 
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * 
	 * @return
	 */
	public String getPath() {
		return path;
	}

	public void solveExpressions(HashMap params) {
		if (haveExpression(this.path)) {
			
			String newPath = StringUtils.substringBetween(this.path, "[", "]");
			
			String[] arrParams = StringUtils.substringsBetween(newPath, "#", "#");
			
			for (String key : arrParams) {
				if (params!=null) {
					Object value = params.get(key);
					String paramValue = (value != null) ? value.toString().trim() : null;
					if (paramValue != null) {
						this.path = StringUtils.replace(newPath, "#" + key + "#", paramValue);;
					}	
				}
			}
		}
	}
	
	/**
	 * @param pPath
	 * @return
	 */
	private boolean haveExpression(String pPath) {
		return (!StringUtils.isEmpty(pPath) && StringUtils.containsAny(pPath, "[") && StringUtils.containsAny(pPath, "]"));
	}
	
	/**
	 * 
	 * @param path
	 */
	public void setPath(String path) {
		this.path = path;
	}
		
	/**
	 * 
	 * @param render
	 */
	public void setRender(boolean render){
		this.render = render;
	}
	
	/**
	 * @return the items
	 */
	public List<Nodo> getItems() {
		if (isSortable() && items != null) Collections.sort(items, new NodoAlphaComparator(this));
		if (items == null) return new ArrayList<Nodo>();
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<Nodo> items) {
		this.items = items;
	}
	
	/**
	 * 
	 */
	public boolean equals(Nodo pMenu) {
		Nodo nodo = (Nodo) pMenu;
		if (this.id.equals(nodo.getId())) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isRender(){
		if (this.getItems() == null || this.getItems().size() == 0) {
			return render;
		}
		boolean isrender = false;
		for(Nodo n: this.getItems()){
			if (n.isRender()) {
				isrender = true;
			}
		}
		return isrender;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return this.selected;
	}

	/**
	 * @param pSelected the selected to set
	 */
	public void setSelected(boolean pSelected) {
		this.selected = pSelected;
	}

	/**
	 * @return the tooltip
	 */
	public String getTooltip() {
		return this.label;
	}
}
