 




 




 



package arg.ros.acc.struts;

/**
 * @author rosario.argentina
 *
 */
public class JsonMessageResponse extends JsonActionResponse {

	/**
	 * 
	 */
	private String mensaje;

	/**
	 * @param pObject
	 * @param mensaje
	 * @param success
	 */
	public JsonMessageResponse(final Object pObject, String pMensaje, boolean pSuccess) {
		super(pObject);
		this.success = pSuccess;
		this.mensaje = pMensaje;
	}
	
	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return this.mensaje;
	}

	/**
	 * @param pMensaje the mensaje to set
	 */
	public void setMensaje(String pMensaje) {
		this.mensaje = pMensaje;
	}
}
