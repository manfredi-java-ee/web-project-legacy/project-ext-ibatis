 




 




 



package arg.ros.acc.struts;

import java.util.Date;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import arg.ros.acc.commons.util.JsonDateValueProcessor;

/**
 * @author rosario.argentina
 *
 */
public class JsonObjectResponse implements JsonData {

	/**
	 * 
	 */
	protected Object data;
	
	/**
	 * 
	 */
	public JsonObjectResponse(final Object pData) {
		this.data = pData;
	}
	
	/* (non-Javadoc)
     * @see arg.ros.acc.struts.JsonData#asJson()
	 */
	public String asJson() {
		final JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor());

		final JSONObject jsonObject = JSONObject.fromObject(this, jsonConfig);
		return jsonObject.toString();
	}

	/* (non-Javadoc)
     * @see arg.ros.acc.struts.JsonData#getData()
	 */
	public Object getData() {
		return this.data;
	}
}
