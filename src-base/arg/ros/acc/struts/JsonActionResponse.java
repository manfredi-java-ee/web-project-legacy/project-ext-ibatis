 




 




 



package arg.ros.acc.struts;



/**
 * @author rosario.argentina
 *
 */
public class JsonActionResponse extends JsonObjectResponse {

	/**
	 * 
	 */
	protected boolean success;
	
	/**
	 * 
	 */
	protected String failure;
	protected String mensaje;
		
	/**
	 * 
	 */
	public JsonActionResponse() {
		super(null);
	}
	
	/**
	 * 
	 */
	public JsonActionResponse(final Object pObject) {
		super(pObject);
		this.success = true;
	}

	/**
	 * @param pFailureMessage
	 */
	public JsonActionResponse(final String pFailureMessage) {
		super(null);
		this.success = false;
		this.failure = pFailureMessage;
	}
	
	
	/**
	 * @param pObject
	 * @param pFailureMessage
	 */
	public JsonActionResponse(final Object pObject, String pFailureMessage) {
		super(pObject);
		this.success = false;
		this.failure = pFailureMessage;
	}

	public JsonActionResponse(final Object pObject, String mensaje, boolean success) {
		super(pObject);
		this.success = success;
		this.mensaje = mensaje;
	}
	/**
	 * @param pSuccess the success to set
	 */
	public void setSuccess(boolean pSuccess) {
		this.success = pSuccess;
	}

	/**
	 * @param pFailure the failure to set
	 */
	public void setFailure(String pFailure) {
		this.failure = pFailure;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess() {
		return this.success;
	}

	/**
	 * @return the failure
	 */
	public String getFailure() {
		return this.failure;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	

}
