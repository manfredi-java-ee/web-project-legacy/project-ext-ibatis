 




 




 



package arg.ros.acc.struts;

import arg.ros.acc.components.paging.PaginatedList;

/**
 * @author rosario.argentina
 *
 */
public class JsonFilterResponse extends JsonActionResponse implements JsonData {
	
	/**
	 * 
	 */
	private int totalCount;
	
	/**
	 * 
	 */
	public JsonFilterResponse(final PaginatedList pPaginatedList) {
		super(pPaginatedList.getData());
		this.setTotalCount(pPaginatedList.getFullListSize());
		this.setSuccess(true);
	}
	
	/**
	 * @param pObject
	 * @param pFailureMessage
	 */
	public JsonFilterResponse(String pFailureMessage) {
		super(pFailureMessage);
	}

	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return this.totalCount;
	}

	/**
	 * @param pTotalCount the totalCount to set
	 */
	public void setTotalCount(int pTotalCount) {
		this.totalCount = pTotalCount;
	}
}
