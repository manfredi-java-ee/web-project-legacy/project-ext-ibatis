 




 




 



package arg.ros.acc.struts;

import org.apache.struts.action.ActionForward;

import arg.ros.acc.components.paging.PaginatedList;

/**
 * @author rosario.argentina
 *
 */
public class JsonActionForward extends ActionForward {

	private Object data;

	/**
	 * 
	 */
	private static final long serialVersionUID = 4744823548822710745L;

	/**
	 * 
	 */
	public JsonActionForward(Object pData) {
		this.data = pData;
	}
	
	/**
	 * @param pData
	 */
	public JsonActionForward(PaginatedList pPaginatedList) {
		this.data = pPaginatedList.getData();
	}
	
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * 
	 */
	public String getResponse() {
		JsonData jsonData = (JsonData) this.data;
		return jsonData.asJson();
	}
}
