 




 




 



package arg.ros.acc.struts;

import java.util.List;

import arg.ros.acc.components.paging.PaginatedList;

/**
 * @author rosario.argentina
 *
 */
public class JsonListResponse extends JsonObjectResponse {

	/**
	 * 
	 */
	private int totalCount;
	
	/**
	 * 
	 */
	public JsonListResponse(final List pData, int pTotalCount) {
		super(pData);
		this.totalCount = pTotalCount;
	}
	
	/**
	 * @param pPaginatedList
	 */
	public JsonListResponse(final PaginatedList pPaginatedList) {
		super(pPaginatedList.getData());
		this.totalCount = pPaginatedList.getFullListSize();
	}

	/**
	 * @param pTotalCount the totalCount to set
	 */
	public void setTotalCount(int pTotalCount) {
		this.totalCount = pTotalCount;
	}

	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return this.totalCount;
	}

}
