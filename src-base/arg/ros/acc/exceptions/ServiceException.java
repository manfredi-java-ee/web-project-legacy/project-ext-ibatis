 




 




 



package arg.ros.acc.exceptions;

/**
 * @author rosario.argentina
 *
 */
public class ServiceException extends RuntimeException {

	/**
	 * 
	 */
	private String message;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7537533294960563871L;

	/**
	 * @param pMessage
	 * @param pThrowable
	 */
	public ServiceException(final String pMessage, final Throwable pThrowable) {
		super(pMessage, pThrowable);
		this.message = pMessage;
	}
	
	/**
	 * @param pThrowable
	 */
	public ServiceException(Throwable pThrowable) {
		super(pThrowable);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String getMessage() {
		if (this.message != null) {
			return this.message;
		}
		return super.getMessage();
	}
}
