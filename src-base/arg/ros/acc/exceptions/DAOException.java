package arg.ros.acc.exceptions;

import java.sql.SQLException;

/**
 * Excepcion BO
 */
public class DAOException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador de la Excepcion
	 */
	private int id;
	
	/**
	 * Mensaje de la excepcion
	 */
	private String msg;
	
	/**
	 * Excepcion que la provoco
	 */
	private Exception e;
	
	/**
	 * Constructor con parametros
	 * 
	 * @param id Identificador de la excepcion
	 * @param msg Mensaje de la excepcion
	 * @param e Excepcion que la provoco
	 */
	public DAOException(int id, String msg, Exception e){
		super(msg);
		
		this.id = id;
		this.msg = msg;
		this.e = e;
	}
	
	/**
	 * Constructor con parametros
	 * 
	 * @param id Identificador de la excepcion
	 * @param msg Mensaje de la excepcion
	 */
	public DAOException(int id, String msg){
		super(msg);
		
		this.id = id;
		this.msg = msg;
		this.e = null;
	}
	
	/**
	 * @param id
	 * @param e
	 */
	public DAOException(int id, Exception e){
		super("");
		
		this.id = id;
		this.msg = null;
		this.e = e;
	}
	
	/**
	 * Constructor con parametros
	 * 
	 * @param msg Mensaje de la excepcion
	 * @param e Excepcion que la provoco
	 */
	public DAOException(String msg, Exception e){
		super(msg);
		
		this.id = 10000;
		this.msg = msg;
		this.e = e;
	}
	
	/**
	 * Obtiene el mensaje
	 * 
	 * @return Mensaje de la excepcion
	 */
	public String getMsg(){
		return msg;
	}
	
	/**
	 * Obtiene el identificador de la excepcion
	 * 
	 * @return Identificador de la excepcion
	 */
	public int getId(){
		return id;
	}
	
	/**
	 * Obtiene la excepcion que la provoco
	 * 
	 * @return Excepcion que la provoco
	 */
	public Exception getE(){
		return e;
	}

	/**
	 * @return
	 */
	public int getErrorCode() {
		SQLException sqlException = null;
		
		try {
			sqlException = (SQLException) this.getE();
			
		} catch (Exception e) {
			return -1;
		}
		
		return sqlException.getErrorCode();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Throwable getCause() {
		return this.getE().getCause(); 		
	}
}
