 




 




 




package arg.ros.acc.exceptions;

/**
 * @author rosario.argentina
 *
 */
public class BusinessException extends ServiceException {

	public BusinessException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}

}
