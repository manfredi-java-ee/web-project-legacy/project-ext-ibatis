
package es.tsm.logj.lj.log;

import es.tsm.logj.lj.conf.logj_lj_Configuracion;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_FileHandler, logj_lj_TrazadorTsm

class logj_lj_DateSizeFileHandler extends logj_lj_FileHandler
{
    private class MeteredStream extends OutputStream
    {

        public void write(int b)
            throws IOException
        {
            out.write(b);
            written++;
        }

        public void write(byte buff[])
            throws IOException
        {
            out.write(buff);
            written += buff.length;
        }

        public void write(byte buff[], int off, int len)
            throws IOException
        {
            out.write(buff, off, len);
            written += len;
        }

        public void flush()
            throws IOException
        {
            out.flush();
        }

        public void close()
            throws IOException
        {
            out.close();
        }

        OutputStream out;
        int written;

        MeteredStream(OutputStream out)
        {
            written = 0;
            this.out = out;
        }
    }

    private class Filtro
        implements FilenameFilter
    {

        public boolean accept(File dir, String name)
        {
            new File(dir, name);
            int digito = fich.length() + 1;
            return name.startsWith(fich) && (Character.isDigit(name.charAt(digito)) || name.charAt(fich.length()) == '.') && name.endsWith(".log");
        }

        String fich;

        public Filtro(String fichero)
        {
            fich = fichero;
        }
    }


    public logj_lj_DateSizeFileHandler(File fichero, int tiempoRotacion, int count, int limit)
        throws IOException
    {
       // super(fichero);
        formatter = new SimpleDateFormat();
        this.tiempoRotacion = 0;
        this.count = 0;
        this.limit = 0;
        tamanoFichCreado = 0L;
        this.tiempoRotacion = tiempoRotacion * 1000;
        this.count = count;
        this.limit = limit;
        openFiles();
    }

    public logj_lj_DateSizeFileHandler(File directorio, String entorno, String modulo, int tiempoRotacion, int count, int limit)
        throws IOException
    {
    //    super(new File(directorio, entorno + "_" + modulo + ".log"));
        formatter = new SimpleDateFormat();
        this.tiempoRotacion = 0;
        this.count = 0;
        this.limit = 0;
        tamanoFichCreado = 0L;
        this.tiempoRotacion = tiempoRotacion * 1000;
        this.count = count;
        this.limit = limit;
        openFiles();
    }

//    public void close()
//    {
//        super.close();
//        try
//        {
//            meter.close();
//        }
//        catch(IOException _ex) { }
//    }

    public void controlarNumMaxFich()
    {
        StringTokenizer s = new StringTokenizer(super.ficheroLog.getName(), "/ \\");
        String fich;
        for(fich = ""; s.hasMoreTokens(); fich = s.nextToken());
        File directorio = new File(super.ficheroLog.getPath().substring(0, super.ficheroLog.getPath().length() - fich.length()));
        fich = fich.substring(0, fich.length() - 4);
        Filtro filtro = new Filtro(fich);
        String lista[] = directorio.list(filtro);
        long aux = 0x7fffffffffffffffL;
        String auxStr = "";
        int auxborrar = 0;
        if(lista != null && lista.length > count)
        {
            auxborrar = lista.length - count;
            for(int j = 0; j < auxborrar; j++)
            {
                s = new StringTokenizer(super.ficheroLog.getName(), "/ \\");
                for(fich = ""; s.hasMoreTokens(); fich = s.nextToken());
                directorio = new File(super.ficheroLog.getPath().substring(0, super.ficheroLog.getPath().length() - fich.length()));
                fich = fich.substring(0, fich.length() - 4);
                filtro = new Filtro(fich);
                lista = directorio.list(filtro);
                aux = 0x7fffffffffffffffL;
                for(int i = 0; i < lista.length; i++)
                {
                    String fecha = lista[i].substring(fich.length(), lista[i].length() - 4);
                    if(fecha.length() > 0)
                    {
                        fecha = fecha.replace('_', '0');
                        aux = Math.min(aux, Long.parseLong(fecha));
                        if(aux == Long.parseLong(fecha))
                            auxStr = lista[i];
                    }
                }

                File ficheroABorrar = new File(directorio, auxStr);
                ficheroABorrar.delete();
            }

        }
    }

    boolean isRotationNecessary()
    {
        Date ahora = new Date();
        boolean condicionTiempo = ahora.getTime() - fechaFicheroLog.getTime() >= (long)tiempoRotacion;
        boolean condicionTamano = limit > 0 && (long)meter.written + tamanoFichCreado >= (long)limit;
        return condicionTiempo || condicionTamano;
    }

    protected void open()
        throws IOException
    {
        super.fout = new FileOutputStream(super.ficheroLog.getAbsolutePath(), true);
        super.bout = new BufferedOutputStream(super.fout);
        meter = new MeteredStream(super.fout);
        super.printStream = new PrintStream(meter);
        if(super.ficheroLog.length() == 0L)
        {
            TimeZone tz = TimeZone.getDefault();
            formatter2 = new SimpleDateFormat("yyyyMMdd HHmmss");
            formatter2.setTimeZone(tz);
            String fecha = formatter2.format(new Date());
            super.printStream.print(fecha + " MSJ-Version  LOG Ver: " + logj_lj_Configuracion.cteVersionLog1 + "." + logj_lj_Configuracion.cteVersionLog2 + ".(" + logj_lj_Configuracion.cteVersionLog3 + ")" + "\n");
        }
    }

    private void openFiles()
        throws IOException
    {
        if(count < 1)
            throw new IllegalArgumentException("Error: n\372mero de ficheros = " + count);
        if(limit < 0)
            limit = 0;
        fechaFicheroLog = new Date();
        if(super.ficheroLog.exists() && logj_lj_TrazadorTsm.APPEND_LOG.equals("no"))
        {
            rotate();
            tamanoFichCreado = 0L;
        } else
        {
          //  setLogFile(super.ficheroLog);
            try
            {
                tamanoFichCreado = super.ficheroLog.length();
            }
            catch(Exception _ex)
            {
                tamanoFichCreado = 0L;
            }
        }
   //     Exception ex = getException();
      /*  if(ex != null)
        {
            if(ex instanceof IOException)
                throw (IOException)ex;
            if(ex instanceof SecurityException)
                throw (SecurityException)ex;
            else
                throw new IOException("Excepci\363n: " + ex);
        } else
        {
            return;
        }*/
    }

    protected void rotate()
    {
        if(super.printStream != null)
            close();
        StringTokenizer s = new StringTokenizer(super.ficheroLog.getName(), "/ \\");
        String fich;
        for(fich = ""; s.hasMoreTokens(); fich = s.nextToken());
        File directorio = new File(super.ficheroLog.getPath().substring(0, super.ficheroLog.getPath().length() - fich.length()));
        formatter.applyPattern("_yyyyMMdd_HHmmss");
        String strFecha = formatter.format(new Date());
        String cadenaCortar = strFecha + ".log";
        File nuevoNombre = new File(directorio, super.ficheroLog.getName().substring(0, super.ficheroLog.getName().length() - 4) + cadenaCortar);
        super.ficheroLog.renameTo(nuevoNombre);
        fechaFicheroLog = new Date();
    //    setLogFile(super.ficheroLog);
        controlarNumMaxFich();
    }

    protected void setConfig(int segRotacion, int tamanoMax, int numMaxFich)
    {
        tiempoRotacion = segRotacion * 1000;
        limit = tamanoMax;
        count = numMaxFich;
    }

    private SimpleDateFormat formatter;
    int tiempoRotacion;
    int count;
    int limit;
    private Date fechaFicheroLog;
    protected MeteredStream meter;
    private static final String PATTERN_FECHA_FICHERO_SEG = "_yyyyMMdd_HHmmss";
    private long tamanoFichCreado;
    private transient SimpleDateFormat formatter2;
	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void publish(String s) {
		// TODO Auto-generated method stub
		
	}
}