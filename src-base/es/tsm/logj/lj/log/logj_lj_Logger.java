package es.tsm.logj.lj.log;

import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_Handler

class logj_lj_Logger
{
	
	
	 protected Vector handlers;

    logj_lj_Logger()
    {
        handlers = new Vector();
    }

    public void addHandler(logj_lj_Handler handler)
    {
        handlers.addElement(handler);
    }

    public void closeAllHandlers()
    {
        if(handlers != null)
        {
            logj_lj_Handler handler;
            for(Enumeration it = handlers.elements(); it.hasMoreElements(); handler.close())
                handler = (logj_lj_Handler)it.nextElement();

        }
    }

    public final synchronized void log(String message)
    {
        int indice = message.indexOf("]") + 1;
        String nivelTraza = message.substring(0, indice);
        String mensaje = message.substring(indice);
        int nivelTrazaMensaje = Integer.parseInt(nivelTraza.substring(1, nivelTraza.length() - 1));
        Enumeration e = handlers.elements();
        for(int cont = 1; e.hasMoreElements(); cont++)
        {
            logj_lj_Handler handler = (logj_lj_Handler)e.nextElement();
            if((handler.getLevel() & nivelTrazaMensaje) != 0 || nivelTrazaMensaje == 0)
                handler.publish(mensaje);
        }

    }

    public void removeHandler(logj_lj_Handler handler)
    {
        handlers.removeElement(handler);
    }

   
}