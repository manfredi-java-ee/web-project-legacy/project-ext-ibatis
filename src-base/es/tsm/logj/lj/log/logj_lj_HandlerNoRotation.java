package es.tsm.logj.lj.log;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_FileHandler, logj_lj_TrazadorTsm

class logj_lj_HandlerNoRotation extends logj_lj_FileHandler
{

    private SimpleDateFormat formatter;
    private String directorio;
    private String entornoModulo;
    private static final String PATTERN_FECHA_FICHERO_HORA = "yyyyMMddHH";
    private static final String PATTERN_FECHA_FICHERO_MIN = "yyyyMMddHHmm";
    private static final String PATTERN_FECHA_FICHERO_SEG = "_yyyyMMdd_HHmmss";
    private static final String PATTERN_FECHA_FICHEROSEG = "yyyyMMdd_HHmmss";
    private static final String PATTERN_FECHA_FICHERO_MILISEG = "_yyyyMMdd_HHmmss";
    private int count;
    private int tiempoInicio;
    private Date fechaFicheroLog;
    private static String FECHA_LOG = "";
	
	
    public logj_lj_HandlerNoRotation(String directorio, String nombreFichero, String idJVM)
    {
        this.directorio = directorio;
        entornoModulo = nombreFichero;
        String fechaLOG = "";
        fechaLOG = System.getProperty("FECHA_LOG");
        if(fechaLOG != null)
            FECHA_LOG = fechaLOG;
        if(fechaLOG == null || fechaLOG.equals(""))
            fechaLOG = FECHA_LOG;
        if(idJVM != null && !idJVM.equals(""))
        {
            String _tmp = "_" + String.valueOf(idJVM);
        }
        super.idJVM = idJVM;
        formatter = new SimpleDateFormat("yyyyMMddHHmm");
        TimeZone tz = TimeZone.getDefault();
        formatter.setTimeZone(tz);
        setFichero();
    }

    public boolean isRotationNecessary()
    {
        return false;
    }

    protected void rotate()
    {
    }

    protected void setConfig(int numMaxFich)
    {
        count = numMaxFich;
    }

    private void setFichero()
    {
        File fichero;
        if(FECHA_LOG != null && !FECHA_LOG.equals(""))
        {
            if(super.idJVM != null && !super.idJVM.equals(""))
                fichero = new File(directorio, entornoModulo + "_" + FECHA_LOG + "_" + super.idJVM + ".log");
            else
                fichero = new File(directorio, entornoModulo + "_" + FECHA_LOG + ".log");
            FECHA_LOG = FECHA_LOG;
        } else
        {
            formatter.applyPattern("yyyyMMdd_HHmmss");
            String strFecha = formatter.format(new Date());
            FECHA_LOG = strFecha;
            if(super.idJVM != null && !super.idJVM.equals(""))
                fichero = new File(directorio, entornoModulo + "_" + strFecha + "_" + super.idJVM + ".log");
            else
                fichero = new File(directorio, entornoModulo + "_" + strFecha + ".log");
        }
        if(fichero.exists())
        {
            setFichero(new Date());
        } else
        {
            fechaFicheroLog = new Date();
         //   setLogFile(fichero);
        }
    }

    private void setFichero(Date fecha)
    {
        formatter.applyPattern("_yyyyMMdd_HHmmss");
        String strFecha;
        if(FECHA_LOG != null && !FECHA_LOG.equals(""))
            strFecha = FECHA_LOG;
        else
            strFecha = formatter.format(fecha);
        String jvm = "";
        File fichero;
        if(super.idJVM != null && !super.idJVM.equals(""))
        {
            jvm = "_" + String.valueOf(super.idJVM);
            fichero = new File(directorio, entornoModulo + "_" + strFecha + jvm + ".log");
        } else
        {
            fichero = new File(directorio, entornoModulo + "_" + strFecha + ".log");
        }
        if(logj_lj_TrazadorTsm.APPEND_LOG.equals("no"))
        {
            File ficheroRotado;
            if(jvm != null && !jvm.equals(""))
                ficheroRotado = new File(directorio, entornoModulo + "_" + strFecha + jvm + ".log");
            else
                ficheroRotado = new File(directorio, entornoModulo + "_" + strFecha + ".log");
            fichero.renameTo(ficheroRotado);
        }
        fechaFicheroLog = fecha;
    //    setLogFile(fichero);
    }

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void publish(String s) {
		// TODO Auto-generated method stub
		
	}




}
