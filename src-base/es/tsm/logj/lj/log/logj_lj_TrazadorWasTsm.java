package es.tsm.logj.lj.log;

import es.tsm.logj.lj.conf.logj_lj_Configuracion;
import java.io.File;
import java.io.PrintStream;
import java.util.*;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_TrazadorTsm, logj_lj_FileHandlerFactory, logj_lj_FileHandler, logj_lj_Handler, 
//            logj_lj_Logger, logj_lj_ElementosWas, logj_lj_PoolTrazadores

public class logj_lj_TrazadorWasTsm extends logj_lj_TrazadorTsm
{
	
    long tiempoInicio;
    private String nombreFichConfig;
    private String clavePool;
    private boolean estado;
    private boolean versionGetTraz;
    private int modo;
    protected static logj_lj_ElementosWas elementosWas;
    private long tiempoCreacion;
    private long tiempoLiberacion;
    
    
    protected String getNombreFich()
    {
        return nombreFichConfig;
    }
    
    protected void setNombreFich(String nombreFichConfig)
    {
        this.nombreFichConfig = nombreFichConfig;
    }

   
    
    
    boolean getEstado()
    {
        return estado;
    }

    void setEstado(boolean estados)
    {
        estado = estados;
    }
    
       
    
    long getTiempoLiberacion()
    {
        return tiempoLiberacion;
    }

    protected void setTiempoLiberacion(long segundosLiberacion)
    {
        tiempoLiberacion = segundosLiberacion;
    }  
      
    
    
    
    
    boolean getVersionGetTraz()
    {
        return versionGetTraz;
    }

    protected void setVersionGetTraz(boolean version)
    {
        versionGetTraz = version;
    }
    
    
    
    
    protected boolean isLibre()
    {
        return getEstado();
    }
    
    
    private String getClavePool()
    {
        return clavePool;
    }

    
    private void setClavePool(String nomFich)
    {
        clavePool = nomFich;
    }
   
  

    protected long getTiempoCreacion()
    {
        return tiempoCreacion;
    }
    
    
    protected void setTiempoCreacion(long segundosCreacion)
    {
        tiempoCreacion = segundosCreacion;
    }
    
    
    
    
    protected logj_lj_TrazadorWasTsm obtener()
    {
        estado = false;
        return this;
    }

    

    protected void setModo(int modo)
    {
        this.modo = modo;
    }


   

  


    
   

    
    //Constructor ---///

    protected logj_lj_TrazadorWasTsm()
    {
        tiempoInicio = 0L;
        estado = true;
        versionGetTraz = false;
        tiempoCreacion = 0L;
        tiempoLiberacion = 0L;
    }

    
    
    
    protected logj_lj_TrazadorWasTsm(Properties confFile, int modo, int nivel, logj_lj_Handler handler)
    {
        super(confFile, modo, nivel, handler);
        tiempoInicio = 0L;
        estado = true;
        versionGetTraz = false;
        tiempoCreacion = 0L;
        tiempoLiberacion = 0L;
    }

//
    
    
    
    
    public static logj_lj_TrazadorWasTsm getTrazador(String nombreFichConfig, int modo)
    throws Exception
{
    long tiempoInactividad = 0L;
    long comprobarTiempoInactividad = 0L;
    int numTrazadoresCrear = 0;
    long numTrazadoresaIncrDecr = 0L;
    logj_lj_TrazadorWasTsm oTrazador = null;
    logj_lj_PoolTrazadores instancia = null;
    if(nombreFichConfig == null || nombreFichConfig.equals(""))
    {
        Exception ex = new Exception("Es necesario incluir el nombre del fichero en el m\351todo setFicheroConfig()");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el nombre del fichero en el m\351todo setFicheroConfig()"));
        throw ex;
    }
    if(elementosWas.getEntorno(nombreFichConfig) == null || elementosWas.getEntorno(nombreFichConfig).equals(""))
    {
        Exception ex = new Exception("Es necesario incluir el entorno de la aplicaci\363n en el m\351todo setFicheroConfig()");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el entorno de la aplicaci\363n en el m\351todo setFicheroConfig()"));
        throw ex;
    }
    if(elementosWas.getModulo(nombreFichConfig) == null || elementosWas.getModulo(nombreFichConfig).equals(""))
    {
        Exception ex = new Exception("Es necesario incluir el m\363dulo de la aplicaci\363n en el m\351todo setFicheroConfig()");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el m\363dulo de la aplicaci\363n en el m\351todo setFicheroConfig()"));
        throw ex;
    }
    instancia = elementosWas.getPool(nombreFichConfig);
    if(instancia == null)
        elementosWas.setProp(nombreFichConfig, logj_lj_Configuracion.getConfFile(nombreFichConfig));
    if(logj_lj_Configuracion.getVersion().equals("2.0"))
    {
        if(instancia == null)
        {
            instancia = new logj_lj_PoolTrazadores();
            instancia.init(elementosWas.getProp(nombreFichConfig), nombreFichConfig, elementosWas.getEntorno(nombreFichConfig), elementosWas.getModulo(nombreFichConfig), modo);
            elementosWas.setPool(nombreFichConfig, instancia);
        }
        if(logj_lj_Configuracion.hayQrefrescar(nombreFichConfig))
        {
            Properties confFileAnterior = elementosWas.getProp(nombreFichConfig);
            int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
            elementosWas.setNivelTrazaAnterior(nombreFichConfig, nivelAnterior);
            elementosWas.setProp(nombreFichConfig, logj_lj_Configuracion.getConfFile(nombreFichConfig));
            instancia.refrescarTrazadores(elementosWas.getProp(nombreFichConfig));
        }
        tiempoInactividad = Long.parseLong(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteTiempoInactTrazador));
        tiempoInactividad *= 1000L;
        comprobarTiempoInactividad = Long.parseLong(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteTiempoComprobacionInactTrazador));
        comprobarTiempoInactividad *= 1000L;
        numTrazadoresCrear = Integer.parseInt(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteNTrazador));
        numTrazadoresaIncrDecr = Long.parseLong(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteIncDecTrazador));
        long resultado = 0L;
        
        if(instancia.getTiempoDecremento() == 0L)
            resultado = (new Date()).getTime() - instancia.getTiempoCracion();
        else
            resultado = (new Date()).getTime() - instancia.getTiempoDecremento();
        if(resultado >= comprobarTiempoInactividad)
            instancia.liberarNoUtilizados(tiempoInactividad, numTrazadoresaIncrDecr, numTrazadoresCrear);
        instancia.libTrazadorTimeOut(elementosWas.getProp(nombreFichConfig));
        oTrazador = instancia.retornarTrazador(elementosWas.getProp(nombreFichConfig), elementosWas.getEntorno(nombreFichConfig), elementosWas.getModulo(nombreFichConfig), modo);
        oTrazador.setClavePool(nombreFichConfig);
        return oTrazador;
        
    } else
    {
        Exception ex = new Exception("No se puede invocar al m\351todo getTrazador, ya que no ha cambiado la nomenclatura de los par\341metros del fichero properties");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "No se puede invocar al m\351todo getTrazador, ya que no ha cambiado la nomenclatura de los par\341metros del fichero properties"));
        throw ex;
    }
    
    
    
    
    
    
}
   
    
    
    
    
    
    
    
    
    public static logj_lj_TrazadorWasTsm getTrazador(String nombreFichConfig, String identificador, int modo)
    throws Exception
{
    long tiempoInactividad = 0L;
    long comprobarTiempoInactividad = 0L;
    int numTrazadoresCrear = 0;
    long numTrazadoresaIncrDecr = 0L;
    logj_lj_TrazadorWasTsm oTrazador = null;
    logj_lj_PoolTrazadores instancia = null;
    if(nombreFichConfig == null || nombreFichConfig.equals(""))
    {
        Exception ex = new Exception("Es necesario incluir el nombre del fichero en el m\351todo setFicheroConfig()");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el nombre del fichero en el m\351todo setFicheroConfig()"));
        throw ex;
    }
    if(elementosWas.getEntorno(nombreFichConfig) == null || elementosWas.getEntorno(nombreFichConfig).equals(""))
    {
        Exception ex = new Exception("Es necesario incluir el entorno de la aplicaci\363n en el m\351todo setFicheroConfig()");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el entorno de la aplicaci\363n en el m\351todo setFicheroConfig()"));
        throw ex;
    }
    if(elementosWas.getModulo(nombreFichConfig) == null || elementosWas.getModulo(nombreFichConfig).equals(""))
    {
        Exception ex = new Exception("Es necesario incluir el m\363dulo de la aplicaci\363n en el m\351todo setFicheroConfig()");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el m\363dulo de la aplicaci\363n en el m\351todo setFicheroConfig()"));
        throw ex;
    }
    if(identificador == null || identificador.equals(""))
    {
        Exception ex = new Exception("Es necesario incluir el identificador.");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el identificador ."));
        throw ex;
    }
    elementosWas.setIdentificador(nombreFichConfig, identificador);
    instancia = elementosWas.getPool(nombreFichConfig);
    if(instancia == null)
        elementosWas.setProp(nombreFichConfig, logj_lj_Configuracion.getConfFile(nombreFichConfig));
    if(logj_lj_Configuracion.getVersion().equals("2.0"))
    {
        if(instancia == null)
        {
            instancia = new logj_lj_PoolTrazadores();
            instancia.init(elementosWas.getProp(nombreFichConfig), nombreFichConfig, elementosWas.getEntorno(nombreFichConfig), elementosWas.getModulo(nombreFichConfig), identificador, modo);
            elementosWas.setPool(nombreFichConfig, instancia);
        }
        if(logj_lj_Configuracion.hayQrefrescar(nombreFichConfig))
        {
            Properties confFileAnterior = elementosWas.getProp(nombreFichConfig);
            int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
            elementosWas.setNivelTrazaAnterior(nombreFichConfig, nivelAnterior);
            elementosWas.setProp(nombreFichConfig, logj_lj_Configuracion.getConfFile(nombreFichConfig));
            instancia.refrescarTrazadores(elementosWas.getProp(nombreFichConfig));
        }
        tiempoInactividad = Long.parseLong(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteTiempoInactTrazador));
        tiempoInactividad *= 1000L;
        comprobarTiempoInactividad = Long.parseLong(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteTiempoComprobacionInactTrazador));
        comprobarTiempoInactividad *= 1000L;
        numTrazadoresCrear = Integer.parseInt(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteNTrazador));
        numTrazadoresaIncrDecr = Long.parseLong(elementosWas.getProp(nombreFichConfig).getProperty(logj_lj_Configuracion.cteIncDecTrazador));
        long resultado = 0L;
        if(instancia.getTiempoDecremento() == 0L)
            resultado = (new Date()).getTime() - instancia.getTiempoCracion();
        else
            resultado = (new Date()).getTime() - instancia.getTiempoDecremento();
        if(resultado >= comprobarTiempoInactividad)
            instancia.liberarNoUtilizados(tiempoInactividad, numTrazadoresaIncrDecr, numTrazadoresCrear);
        instancia.libTrazadorTimeOut(elementosWas.getProp(nombreFichConfig));
        oTrazador = instancia.retornarTrazador(elementosWas.getProp(nombreFichConfig), elementosWas.getEntorno(nombreFichConfig), elementosWas.getModulo(nombreFichConfig), identificador, modo);
        oTrazador.setClavePool(nombreFichConfig);
        return oTrazador;
    } else
    {
        Exception ex = new Exception("No se puede invocar al m\351todo getTrazador, ya que no ha cambiado la nomenclatura de los par\341metros del fichero properties");
        System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "No se puede invocar al m\351todo getTrazador, ya que no ha cambiado la nomenclatura de los par\341metros del fichero properties"));
        throw ex;
    }
}


    
    public static void setFicheroConfig(String nombreFichero, String entor, String mod)
    {
         elementosWas = new logj_lj_ElementosWas(nombreFichero);
         elementosWas.setEntorno(nombreFichero, entor.toLowerCase());
         elementosWas.setModulo(nombreFichero, mod.toLowerCase());
    }
     
    
    
    public void liberar()
    {
        if(!isLibre())
        {
            setEstado(true);
            setTiempoLiberacion((new Date()).getTime());
            flush();
        }
    }



    public void trazarCALFinal(String nombreProceso, String comentario)
    {
        try
        {
            if(getNomProcesoExt().equals(nombreProceso))
            {
                long segFin = (new Date()).getTime();
                super.tiempoEjecLlamadaExt = segFin - getIniLlamadaExt();
            }
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x2000010, nombreProceso, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarCALInicio(String nombreProceso, String comentario)
    {
        try
        {
            long segIni = (new Date()).getTime();
            logj_lj_TrazadorTsm.setIniLlamadaExt(segIni);
            setNomProcesoExt(nombreProceso);
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x2000008, nombreProceso, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarError(Exception excepcion)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(excepcion);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorApps(String nombreServlet, String nombreApp, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000004, nombreServlet, "APPS", nombreApp, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorBBDD(String nombreServlet, String bbddSID, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000004, nombreServlet, "BBDD", bbddSID, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorFichero(String nombreServlet, String nombreFic, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000004, nombreServlet, "FICH", nombreFic, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    public void trazarErrorLDAP(String nombreServlet, String nombreLDAP, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000004, nombreServlet, "LDAP", nombreLDAP, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorServlet(String nombreServlet, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000002, nombreServlet, "APPS", "SERVLET", codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarERRWasFatalApps(String nombreServlet, String nombreApp, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000008, nombreServlet, "APPS", nombreApp, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarERRWasFatalTuxedo(String nombreServlet, String nombreTux, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000008, nombreServlet, "TUXE", nombreTux, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMSJCabeceraGet(String mensaje)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x8000020, mensaje);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMSJCabeceraGet(String origen, String mensaje)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x8000020, origen, mensaje);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMSJCabeceraPost(String mensaje)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x8000040, mensaje);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMSJCabeceraPost(String origen, String mensaje)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x8000040, origen, mensaje);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMSJComentario(String mensaje)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x8000002, mensaje);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMSJComentario(String origen, String mensaje)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x8000002, origen, mensaje);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMSJElemProc(String proceso, String nombreElemento, String accion, String valor)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x8000004, proceso, nombreElemento + ' ' + accion + ' ' + valor);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarPROInicio(String nombreServlet, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            tiempoInicio = (new Date()).getTime();
            trazar(0x2000001, nombreServlet, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarPROWasFinal(String nombreServlet, long tiempoEjecucion, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0x2000002, nombreServlet, Long.toString(tiempoEjecucion) + ' ' + comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarPROWasFinal(String nombreServlet, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            long tiempoEjec = (new Date()).getTime() - tiempoInicio;
            tiempoInicio = 0L;
            trazar(0x2000002, nombreServlet, Long.toString(tiempoEjec) + ' ' + comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMensajeGenericoCAL(String tipo, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            StringBuffer blancos = new StringBuffer();
            if(tipo.length() > 8)
            {
                tipo = tipo.substring(0, 8);
            } else
            {
                for(int i = tipo.length(); i < 8; i++)
                    blancos.append(" ");

            }
            tipo = tipo + blancos.toString();
            trazarGenericos(0x2001000, tipo, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMensajeGenericoERR(String tipo, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            StringBuffer blancos = new StringBuffer();
            if(tipo.length() > 8)
            {
                tipo = tipo.substring(0, 8);
            } else
            {
                for(int i = tipo.length(); i < 8; i++)
                    blancos.append(" ");

            }
            tipo = tipo + blancos.toString();
            trazarGenericos(0x1001000, tipo, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMensajeGenericoMSJ(String tipo, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            StringBuffer blancos = new StringBuffer();
            if(tipo.length() > 8)
            {
                tipo = tipo.substring(0, 8);
            } else
            {
                for(int i = tipo.length(); i < 8; i++)
                    blancos.append(" ");

            }
            tipo = tipo + blancos.toString();
            trazarGenericos(0x8000080, tipo, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarMensajeGenericoPRO(String tipo, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            StringBuffer blancos = new StringBuffer();
            if(tipo.length() > 8)
            {
                tipo = tipo.substring(0, 8);
            } else
            {
                for(int i = tipo.length(); i < 8; i++)
                    blancos.append(" ");

            }
            tipo = tipo + blancos.toString();
            trazarGenericos(0x2000400, tipo, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorGenerico(String proceso, String tipo, String nombreServicio, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            if(tipo.length() > 4)
                tipo = tipo.substring(0, 4);
            trazarError(0x1000008, null, null, proceso, tipo, nombreServicio, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorGenericoFatal(String proceso, String tipo, String nombreServicio, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            if(tipo.length() > 4)
                tipo = tipo.substring(0, 4);
            trazarError(0x1000008, null, null, proceso, tipo, nombreServicio, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }



    public void trazarVersion(String nombreComponente, int version1, int version2, int version3, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazar(0, nombreComponente, version1, version2, version3, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorVignette(String nombreServlet, String aplicacion, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000002, nombreServlet, "VTTE", aplicacion, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarErrorVolantis(String nombreServlet, String aplicacion, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000002, nombreServlet, "VTIS", aplicacion, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarERRWasFatalVignette(String nombreServlet, String aplicacion, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000008, nombreServlet, "VTTE", aplicacion, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void trazarERRWasFatalVolantis(String nombreServlet, String aplicacion, int codigoError, String comentario)
    {
        try
        {
            if(getVersionGetTraz())
            {
                String clavePool_nomFich = getClavePool();
                logj_lj_PoolTrazadores instancia = elementosWas.getPool(clavePool_nomFich);
                if(logj_lj_Configuracion.hayQrefrescar(clavePool_nomFich))
                {
                    Properties confFileAnterior = elementosWas.getProp(clavePool_nomFich);
                    int nivelAnterior = Integer.parseInt(confFileAnterior.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    elementosWas.setNivelTrazaAnterior(clavePool_nomFich, nivelAnterior);
                    elementosWas.setProp(clavePool_nomFich, logj_lj_Configuracion.getConfFile(clavePool_nomFich));
                    instancia.refrescarTrazadores(elementosWas.getProp(clavePool_nomFich));
                }
            }
            trazarError(0x1000008, nombreServlet, "VTIS", aplicacion, codigoError, comentario);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }


    
    
}