package es.tsm.logj.lj.log;

import java.util.HashMap;
import java.util.Properties;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_PoolTrazadores

class logj_lj_ElementosWas extends HashMap
{
	
	   logj_lj_ElementosWas(String fichConfig)
	    {
	    }
	   
	   
	   
  class Elementos
    {
	  
	  
      private String nombreFichero;
      private String entorno;
      private String modulo;
      private Properties prop;
      private logj_lj_PoolTrazadores pool;
      private String identificador;
      private int nivelTrazaAnterior;

      Elementos()
      {
          prop = null;
      }
	  
 
        public void setPool(logj_lj_PoolTrazadores newPool)
        {
            pool = newPool;
        }

        public logj_lj_PoolTrazadores getPool()
        {
            return pool;
        }

        public String getEntorno()
        {
            return entorno;
        }

        public String getModulo()
        {
            return modulo;
        }

        public String getIdentificador()
        {
            return identificador;
        }

        public Properties getProp()
        {
            return prop;
        }

        public int getNivelTrazaAnterior()
        {
            return nivelTrazaAnterior;
        }

        public void setEntorno(String newEntorno)
        {
            entorno = newEntorno;
        }

        public void setModulo(String newModulo)
        {
            modulo = newModulo;
        }

        public void setIdentificador(String newIdentificador)
        {
            identificador = newIdentificador;
        }

        public void setProp(Properties newProp)
        {
            prop = newProp;
        }

        public void setNivelTrazaAnterior(int newNivelTrazaAnterior)
        {
            nivelTrazaAnterior = newNivelTrazaAnterior;
        }


    }


 

    public String getEntorno(String fichConfig)
    {
        String entorno = "";
        if(get(fichConfig) != null)
            entorno = ((Elementos)get(fichConfig)).getEntorno();
        return entorno;
    }

    public String getIdentificador(String fichConfig)
    {
        String identificador = "";
        if(get(fichConfig) != null)
            identificador = ((Elementos)get(fichConfig)).getIdentificador();
        return identificador;
    }

    public String getModulo(String fichConfig)
    {
        String modulo = "";
        if(get(fichConfig) != null)
            modulo = ((Elementos)get(fichConfig)).getModulo();
        return modulo;
    }

    public logj_lj_PoolTrazadores getPool(String fichConfig)
    {
        logj_lj_PoolTrazadores pool = null;
        if(get(fichConfig) != null)
            pool = ((Elementos)get(fichConfig)).getPool();
        return pool;
    }

    public Properties getProp(String fichConfig)
    {
        Properties prop = null;
        if(get(fichConfig) != null)
            prop = ((Elementos)get(fichConfig)).getProp();
        return prop;
    }

    public void setEntorno(String fichConfig, String newEntorno)
    {
        if(get(fichConfig) == null)
        {
            Elementos ele = new Elementos();
            ele.setEntorno(newEntorno);
            put(fichConfig, ele);
        } else
        {
            ((Elementos)get(fichConfig)).setEntorno(newEntorno);
        }
    }

    public void setModulo(String fichConfig, String newModulo)
    {
        if(get(fichConfig) == null)
        {
            Elementos ele = new Elementos();
            ele.setModulo(newModulo);
            put(fichConfig, ele);
        } else
        {
            ((Elementos)get(fichConfig)).setModulo(newModulo);
        }
    }

    public void setIdentificador(String fichConfig, String newIdentificador)
    {
        if(get(fichConfig) == null)
        {
            Elementos ele = new Elementos();
            ele.setIdentificador(newIdentificador);
            put(fichConfig, ele);
        } else
        {
            ((Elementos)get(fichConfig)).setIdentificador(newIdentificador);
        }
    }

    public void setPool(String fichConfig, logj_lj_PoolTrazadores newPool)
    {
        if(get(fichConfig) == null)
        {
            Elementos ele = new Elementos();
            ele.setPool(newPool);
            put(fichConfig, ele);
        } else
        {
            ((Elementos)get(fichConfig)).setPool(newPool);
        }
    }

    public void setProp(String fichConfig, Properties newProp)
    {
        if(get(fichConfig) == null)
        {
            Elementos ele = new Elementos();
            ele.setProp(newProp);
            put(fichConfig, ele);
        } else
        {
            ((Elementos)get(fichConfig)).setProp(newProp);
        }
    }

    public void setNivelTrazaAnterior(String fichConfig, int nivelAnterior)
    {
        if(get(fichConfig) == null)
        {
            Elementos ele = new Elementos();
            ele.setNivelTrazaAnterior(nivelAnterior);
            put(fichConfig, ele);
        } else
        {
            ((Elementos)get(fichConfig)).setNivelTrazaAnterior(nivelAnterior);
        }
    }

    public int getNivelTrazaAnterior(String fichConfig)
    {
        int nivTraza = 0;
        if(get(fichConfig) != null)
            nivTraza = ((Elementos)get(fichConfig)).getNivelTrazaAnterior();
        return nivTraza;
    } 
}