package es.tsm.logj.lj.log;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_FileHandler, logj_lj_TrazadorTsm

class logj_lj_DateFileHandler extends logj_lj_FileHandler
{
    private class MeteredStream extends OutputStream
    {

        public void write(int b)
            throws IOException
        {
            out.write(b);
            written++;
        }

        public void write(byte buff[])
            throws IOException
        {
            out.write(buff);
            written += buff.length;
        }

        public void write(byte buff[], int off, int len)
            throws IOException
        {
            out.write(buff, off, len);
            written += len;
        }

        public void flush()
            throws IOException
        {
            out.flush();
        }

        public void close()
            throws IOException
        {
            out.close();
        }

        OutputStream out;
        int written;

        MeteredStream(OutputStream out)
        {
            written = 0;
            this.out = out;
        }
    }

    private class Filtro
        implements FilenameFilter
    {

        public boolean accept(File dir, String name)
        {
            new File(dir, name);
            int digito = fich.length() + 1;
            return name.startsWith(fich) && (Character.isDigit(name.charAt(digito)) || name.charAt(fich.length()) == '.') && name.endsWith(".log");
        }

        String fich;

        public Filtro(String fichero)
        {
            fich = fichero;
        }
    }


    public logj_lj_DateFileHandler(File ficheroLog)
    {
        directorio = new File("");
        entornoModulo = ficheroLog.getPath();
        tiempoRotacion = 0x5265c00;
        super.idJVM = super.idJVM;
        formatter = new SimpleDateFormat("yyyyMMddHHmm");
        TimeZone tz = TimeZone.getDefault();
        formatter.setTimeZone(tz);
        setFichero();
    }

    public logj_lj_DateFileHandler(File directorio, String entorno, String modulo, int tiempoRotacion, int numMaxFich)
    {
        this(directorio, entorno, modulo, tiempoRotacion, "", numMaxFich);
    }

    public logj_lj_DateFileHandler(File directorio, String entorno, String modulo, int tiempo, String idJVM, int numMaxFich)
    {
        this.directorio = directorio;
        String jvm = "";
        if(idJVM != null && !idJVM.equals(""))
            jvm = "_" + String.valueOf(idJVM);
        entornoModulo = entorno + '_' + modulo + jvm;
        tiempoRotacion = tiempo * 1000;
        super.idJVM = idJVM;
        count = numMaxFich;
        formatter = new SimpleDateFormat("yyyyMMddHHmm");
        TimeZone tz = TimeZone.getDefault();
        formatter.setTimeZone(tz);
        setFichero();
    }

    public logj_lj_DateFileHandler(String nombreFicherLog)
    {
        directorio = new File("");
        entornoModulo = nombreFicherLog;
        tiempoRotacion = 0x5265c00;
        super.idJVM = super.idJVM;
        formatter = new SimpleDateFormat("yyyyMMddHHmm");
        TimeZone tz = TimeZone.getDefault();
        formatter.setTimeZone(tz);
        setFichero();
    }

    public logj_lj_DateFileHandler(String directorio, String nombreFicherLog)
    {
        this.directorio = new File(directorio);
        entornoModulo = nombreFicherLog;
        tiempoRotacion = 0x5265c00;
        super.idJVM = super.idJVM;
        formatter = new SimpleDateFormat("yyyyMMddHHmm");
        TimeZone tz = TimeZone.getDefault();
        formatter.setTimeZone(tz);
        setFichero();
    }

    public logj_lj_DateFileHandler(String directorio, String nombreFicherLog, int tiempoRotacion, int numMaxFich)
    {
        this.directorio = new File(directorio);
        entornoModulo = nombreFicherLog;
        this.tiempoRotacion = tiempoRotacion * 1000;
        super.idJVM = super.idJVM;
        count = numMaxFich;
        formatter = new SimpleDateFormat("yyyyMMddHHmm");
        TimeZone tz = TimeZone.getDefault();
        formatter.setTimeZone(tz);
        setFichero();
    }

    public void controlarNumMaxFich()
    {
        StringTokenizer s = new StringTokenizer(super.ficheroLog.getName(), "/ \\");
        String fich;
        for(fich = ""; s.hasMoreTokens(); fich = s.nextToken());
        File directorio = new File(super.ficheroLog.getPath().substring(0, super.ficheroLog.getPath().length() - fich.length()));
        fich = fich.substring(0, fich.length() - 4);
        Filtro filtro = new Filtro(fich);
        String lista[] = directorio.list(filtro);
        long aux = 0x7fffffffffffffffL;
        String auxStr = "";
        int auxborrar = 0;
        if(lista != null && lista.length > count)
        {
            auxborrar = lista.length - count;
            for(int j = 0; j < auxborrar; j++)
            {
                s = new StringTokenizer(super.ficheroLog.getName(), "/ \\");
                for(fich = ""; s.hasMoreTokens(); fich = s.nextToken());
                directorio = new File(super.ficheroLog.getPath().substring(0, super.ficheroLog.getPath().length() - fich.length()));
                fich = fich.substring(0, fich.length() - 4);
                filtro = new Filtro(fich);
                lista = directorio.list(filtro);
                aux = 0x7fffffffffffffffL;
                for(int i = 0; i < lista.length; i++)
                {
                    String fecha = lista[i].substring(fich.length(), lista[i].length() - 4);
                    if(fecha.length() > 0)
                    {
                        fecha = fecha.replace('_', '0');
                        aux = Math.min(aux, Long.parseLong(fecha));
                        if(aux == Long.parseLong(fecha))
                            auxStr = lista[i];
                    }
                }

                File ficheroABorrar = new File(directorio, auxStr);
                ficheroABorrar.delete();
            }

        }
    }

    public boolean isRotationNecessary()
    {
        Date ahora = new Date();
        boolean retorno = ahora.getTime() - fechaFicheroLog.getTime() >= (long)tiempoRotacion;
        return retorno;
    }

    protected void rotate()
    {
        close();
        setFichero(new Date());
        controlarNumMaxFich();
    }

    protected void setConfig(int segRotacion, int numMaxFich)
    {
        tiempoRotacion = segRotacion * 1000;
        count = numMaxFich;
    }

    private void setFichero()
    {
        formatter.applyPattern("_yyyyMMdd_HHmmss");
        File fichero = new File(directorio, entornoModulo + ".log");
        if(fichero.exists())
        {
            setFichero(new Date());
        } else
        {
            fechaFicheroLog = new Date();
          //  setLogFile(fichero);
        }
        controlarNumMaxFich();
    }

    private void setFichero(Date fecha)
    {
        formatter.applyPattern("_yyyyMMdd_HHmmss");
        String strFecha = formatter.format(fecha);
        if(super.idJVM != null && !super.idJVM.equals(""))
        {
            String _tmp = "_" + String.valueOf(super.idJVM);
        }
        File fichero = new File(directorio, entornoModulo + ".log");
        if(logj_lj_TrazadorTsm.APPEND_LOG.equals("no"))
        {
            File ficheroRotado = new File(directorio, entornoModulo + strFecha + ".log");
            fichero.renameTo(ficheroRotado);
        }
        fechaFicheroLog = fecha;
     //   setLogFile(fichero);
    }

    private SimpleDateFormat formatter;
    private File directorio;
    private String entornoModulo;
    private Date fechaFicheroLog;
    private static final String PATTERN_FECHA_FICHERO_HORA = "yyyyMMddHH";
    private static final String PATTERN_FECHA_FICHERO_MIN = "yyyyMMddHHmm";
    private static final String PATTERN_FECHA_FICHERO_SEG = "_yyyyMMdd_HHmmss";
    private int tiempoRotacion;
    public int count;
	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void publish(String s) {
		// TODO Auto-generated method stub
		
	}
}