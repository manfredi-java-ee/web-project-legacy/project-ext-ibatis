package es.tsm.logj.lj.log;

import es.tsm.logj.lj.conf.logj_lj_Configuracion;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_Logger, logj_lj_ConsoleHandler, logj_lj_Handler

public class logj_lj_TrazadorTsm extends logj_lj_Logger
{
	
	   public static final int MASCARA_NIVEL1 = 0x1000000;
	    public static final int MASCARA_NIVEL2 = 0x2000000;
	    public static final int MASCARA_NIVEL3 = 0x4000000;
	    public static final int MASCARA_NIVEL4 = 0x8000000;
	    public static final int cteTrazarProError = 0x1000001;
	    public static final int cteTrazarError = 0x1000001;
	    private int modo;
	    private Vector lineas;
	    public static final int cteAgruparTrazas = 1;
	    public static final int cteNoAgruparTrazas = 2;
	    protected long ultimoMovTrazador;
	    public static final int cteTrazarProInicio = 0x2000001;
	    public static final int cteTrazarProFinal = 0x2000004;
	    public static final int cteTrazarSubInicio = 0x2000020;
	    public static final int cteTrazarSubFinal = 0x2000040;
	    public static final int cteTrazarTuxedo = 0x8000008;
	    public static final int cteTrazarTuxedoParametro = 0x8000010;
	    public static final int cteTrazarCabeceraGet = 0x8000020;
	    public static final int cteTrazarCabeceraPost = 0x8000040;
	    public static final int cteTrazarTodo = -1;
	    public static final String MSJ_COM = "MSJ-Comentar";
	    public static final String MSJ_ELEMPROC = "MSJ-ElemProc";
	    public static final String PRO_INI = "PRO-Inicio  ";
	    public static final String PRO_FIN = "PRO-Final   ";
	    public static final String PRO_ERR = "PRO-Error   ";
	    public static final String SUB_INI = "SUB-Inicio  ";
	    public static final String SUB_FIN = "SUB-Final   ";
	    public static final String SUB_ERR = "SUB-Error   ";
	    public static final String ERR_SIS = "ERR-Sistema ";
	    public static final String ERR_BD = "ERR-BaseDato";
	    public static final String ERR_PARAM = "ERR-Parametr";
	    public static final String REA_FICH = "REA-Fichero ";
	    public static final String REA_TABLA = "REA-Tabla   ";
	    private transient SimpleDateFormat formatter;
	    public static final String EBD = "EBD-";
	    public static final String SYS = "SYS-";
	    public static final String TUX = "TUX-";
	    public static final String ERR_VOLANTIS = "ERR-Volantis";
	    public static final int cteTrazarErrVolantis = 0x1002000;
	    public static final int cteTrazarVOL = 0x1002002;
	    public static final String VOL = "VOL-";
	    public static final String ERR_VIGNETTE = "ERR-Vignette";
	    public static final int cteTrazarErrVignette = 0x1004000;
	    protected int nivelTraza;
	    public static final String CAL_FIN = "CAL-Final   ";
	    public static final String CAL_INI = "CAL-Inicio  ";
	    public static final int cteTrazarCalFinal = 0x2000010;
	    public static final int cteTrazarCalInicio = 0x2000008;
	    public static final int cteTrazarErrBaseDatos = 0x1000010;
	    public static final int cteTrazarEBD = 0x1000100;
	    public static final int cteTrazarErrParametro = 0x1000080;
	    public static final int cteTrazarErrSistema = 0x1000020;
	    public static final int cteTrazarSYS = 0x1000200;
	    public static final int cteTrazarErrTuxedo = 0x1000040;
	    public static final int cteTrazarTUX = 0x1000400;
	    public static final int cteTrazarErrWasCall = 0x1000004;
	    public static final int cteTrazarErrWasFatal = 0x1000008;
	    public static final int cteTrazarErrWasPro = 0x1000002;
	    public static final int cteTrazarExcepcion = 0x8000001;
	    public static final int cteTrazarMsjComentar = 0x8000002;
	    public static final int cteTrazarMsjElemProc = 0x8000004;
	    public static final int cteTrazarProWasFinal = 0x2000002;
	    public static final int cteTrazarReaFichero = 0x2000100;
	    public static final int cteTrazarReaTabla = 0x2000200;
	    public static final int cteTrazarSubError = 0x1000800;
	    public static final int cteTrazarMSJ = 0x8000080;
	    public static final int cteTrazarPRO = 0x2000400;
	    public static final int cteTrazarSUB = 0x2000800;
	    public static final int cteTrazarCAL = 0x2001000;
	    public static final int cteTrazarERR = 0x1001000;
	    public static final int cteTrazarREA = 0x2002000;
	    public static final String MSJ = "MSJ-";
	    public static final String CAL = "CAL-";
	    public static final String PRO = "PRO-";
	    public static final String ERR = "ERR-";
	    public static final String REA = "REA-";
	    public static final String SUB = "SUB-";
	    public static final String ERR_TUXEDO = "ERR-Tuxedo  ";
	    public static final String ERR_WASCALL = "ERR-WasCall ";
	    public static final String ERR_WASFATAL = "ERR-WasFatal";
	    public static final String ERR_WASPRO = "ERR-WasPro  ";
	    public static final String MSJ_EXCEPCIO = "MSJ-Excepcio";
	    public static final String PRO_WASFINAL = "PRO-WasFinal";
	    public static final String SERVICIO_SERVLET = "SERVLET";
	    public static final String TIPO_APPS = "APPS";
	    public static final String TIPO_BBDD = "BBDD";
	    public static final String TIPO_FICHERO = "FICH";
	    public static final String TIPO_LDAP = "LDAP";
	    public static final String TIPO_TUXEDO = "TUXE";
	    public static final String TIPO_VTTE = "VTTE";
	    public static final String TIPO_VTIS = "VTIS";
	    protected static String APPEND_LOG = "no";
	    public Properties prop;
	    private static boolean newTrazar;
	    private static long tiempoIniLlamaExt = 0L;
	    private static String nombreProcesoExt = "";
	    long tiempoEjecLlamadaExt;
	
	
	    static void setIniLlamadaExt(long segLlamadaIni)
	    {
	        tiempoIniLlamaExt = segLlamadaIni;
	    }

	    protected void setNomProcesoExt(String nombreProceso)
	    {
	        nombreProcesoExt = nombreProceso;
	    }

	    protected void setTiempoUltimoMov(long tiempoMov)
	    {
	        ultimoMovTrazador = tiempoMov;
	    }
	    
	    protected void setConfig(Properties properties)
	    {
	        prop = properties;
	    }
	    
	    protected String getNomProcesoExt()
	    {
	        return nombreProcesoExt;
	    }

	    protected long getTiempoUltimoMov()
	    {
	        return ultimoMovTrazador;
	    }

	    

    public logj_lj_TrazadorTsm()
    {
        this(2, 4);
    }
   
    
    
    public logj_lj_TrazadorTsm(int modo, int nivel)
    {
        this(modo, nivel, ((logj_lj_Handler) (new logj_lj_ConsoleHandler(System.out))));
    }

    public logj_lj_TrazadorTsm(int modo, int nivel, logj_lj_Handler handler)
    {
        ultimoMovTrazador = 0L;
        nivelTraza = 0xf000000;
        tiempoEjecLlamadaExt = 0L;
        this.modo = modo;
        switch(modo)
        {
        case 1: // '\001'
            lineas = new Vector();
            break;
        }
        nivelTraza = getMascaraNivelTraza(nivel);
        handler.setLevel(nivelTraza);
        addHandler(handler);
        TimeZone tz = TimeZone.getDefault();
        formatter = new SimpleDateFormat("yyyyMMdd HHmmss");
        formatter.setTimeZone(tz);
    }

    public logj_lj_TrazadorTsm(Properties conf, int modo, int nivel)
    {
        this(conf, modo, nivel, ((logj_lj_Handler) (new logj_lj_ConsoleHandler(System.out))));
    }

    public logj_lj_TrazadorTsm(Properties confFile, int modo, int nivel, logj_lj_Handler handler)
    {
        ultimoMovTrazador = 0L;
        nivelTraza = 0xf000000;
        tiempoEjecLlamadaExt = 0L;
        prop = confFile;
        this.modo = modo;
        switch(modo)
        {
        case 1: // '\001'
            lineas = new Vector();
            break;
        }
        nivelTraza = getMascaraNivelTraza(nivel);
        handler.setLevel(nivelTraza);
        addHandler(handler);
        TimeZone tz = TimeZone.getDefault();
        formatter = new SimpleDateFormat("yyyyMMdd HHmmss");
        formatter.setTimeZone(tz);
    }

    
    
    public void cerrar()
    {
        flush();
        closeAllHandlers();
    }

    private synchronized void escribir(String cadena)
    {
        log(cadena);
    }

    private synchronized void escribir(Vector cadenas)
    {
        if(cadenas != null)
        {
            for(Enumeration e = cadenas.elements(); e.hasMoreElements(); escribir((String)e.nextElement()));
        }
    }

 
    
    public synchronized void flush()
    {
        switch(modo)
        {
        case 1: // '\001'
            escribir(lineas);
            lineas.removeAllElements();
            break;
        }
    }

    private void formatearTraza(int nivelTraza, String codigoMensaje, String entorno, String modulo, String origen, String textoMensaje, String resultado)
    {
        setTiempoUltimoMov((new Date()).getTime());
        String nombreThread = "";
        StringBuffer trazaFinal = new StringBuffer();
        String fecha = obtenerFechaYHora();
        trazaFinal.append(fecha);
        trazaFinal.append(' ');
        trazaFinal.append(codigoMensaje);
        if(nivelTraza != 0x1000200 && nivelTraza != 0x1000400 && nivelTraza != 0x1000100 && nivelTraza != 0x1002002)
            trazaFinal.append(' ');
        int thread = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteThread));
        if(thread == 1 && nivelTraza != 0x1000200 && nivelTraza != 0x1000100 && nivelTraza != 0x1000400 && codigoMensaje != null && nivelTraza != 0x1002002 && codigoMensaje != null && !codigoMensaje.equals("MSJ-StatProc") && !codigoMensaje.equals("MSJ-StatTec"))
        {
            nombreThread = Thread.currentThread().getName();
            trazaFinal.append(nombreThread);
            trazaFinal.append(' ');
        }
        if(entorno != null)
        {
            trazaFinal.append(entorno);
            trazaFinal.append(' ');
        }
        if(modulo != null)
        {
            trazaFinal.append(modulo);
            trazaFinal.append(' ');
        }
        if(origen != null)
        {
            trazaFinal.append(origen);
            trazaFinal.append(' ');
            if(getNomProcesoExt().equals(origen) && tiempoEjecLlamadaExt != 0L && nivelTraza == 0x2000010)
            {
                long seg = tiempoEjecLlamadaExt / 1000L;
                long milseg = tiempoEjecLlamadaExt % 1000L;
                String punto = ".";
                trazaFinal.append(seg);
                trazaFinal.append(punto);
                trazaFinal.append(milseg);
                trazaFinal.append(' ');
                tiempoEjecLlamadaExt = 0L;
                setIniLlamadaExt(0L);
                setNomProcesoExt("");
            }
        }
        if(codigoMensaje != null)
        {
            if(codigoMensaje.equals("MSJ-StatTec") && textoMensaje.equals(" PID"))
                resultado = Thread.currentThread().getName();
            if((codigoMensaje.equals("MSJ-StatProc") || codigoMensaje.equals("MSJ-StatTec")) && !textoMensaje.equals("Cabecera") && textoMensaje != null && !textoMensaje.equals(""))
            {
                if(resultado == null)
                    resultado = "";
                String Puntos = "";
                for(int i = textoMensaje.length() + trazaFinal.length() + resultado.length(); i < 512; i++)
                    Puntos = Puntos + ".";

                textoMensaje = textoMensaje + Puntos + resultado;
            }
        }
        trazaFinal.append(textoMensaje);
        trazaFinal.setLength(512);
        StringBuffer strNivelTraza = new StringBuffer("[");
        strNivelTraza.append(nivelTraza);
        strNivelTraza.append("]");
        StringBuffer strTrazaFinal = new StringBuffer(strNivelTraza.toString());
        strTrazaFinal.append(trazaFinal);
        int tamanoBuffer = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteTamanoBuffer));
        switch(modo)
        {
        case 1: // '\001'
            if(lineas == null)
                lineas = new Vector();
            lineas.addElement(strTrazaFinal.toString().trim());
            if(lineas.size() >= tamanoBuffer)
                flush();
            break;

        default:
            escribir(strTrazaFinal.toString().trim());
            break;
        }
    }

    private String formatoFecha(String elemento)
    {
        if(elemento.length() == 1)
            elemento = "0" + elemento;
        return elemento;
    }

    protected long getIniLlamadaExt()
    {
        return tiempoIniLlamaExt;
    }

    static int getMascaraNivelTraza(int nivel)
    {
        int mascara;
        switch(nivel)
        {
        case 1: // '\001'
            mascara = 0x1000000;
            break;

        case 2: // '\002'
            mascara = 0x3000000;
            break;

        case 3: // '\003'
            mascara = 0x7000000;
            break;

        case 4: // '\004'
            mascara = 0xf000000;
            break;

        default:
            throw new IllegalArgumentException("par\341metro 'nivel' incorrecto, nivel = 1..4");
        }
        return mascara;
    }

    private static final String getMensaje(int nivel)
    {
        String aux = "------------";
        switch(nivel)
        {
        case 33554433: 
            aux = "PRO-Inicio  ";
            break;

        case 33554436: 
            aux = "PRO-Final   ";
            break;

        case 33554434: 
            aux = "PRO-WasFinal";
            break;

        case 16777217: 
            aux = "PRO-Error   ";
            break;

        case 33554440: 
            aux = "CAL-Inicio  ";
            break;

        case 33554448: 
            aux = "CAL-Final   ";
            break;

        case 33554464: 
            aux = "SUB-Inicio  ";
            break;

        case 33554496: 
            aux = "SUB-Final   ";
            break;

        case 16779264: 
            aux = "SUB-Error   ";
            break;

        case 16777220: 
            aux = "ERR-WasCall ";
            break;

        case 16777218: 
            aux = "ERR-WasPro  ";
            break;

        case 16777224: 
            aux = "ERR-WasFatal";
            break;

        case 16777232: 
            aux = "ERR-BaseDato";
            break;

        case 16777472: 
            aux = "EBD-";
            break;

        case 16777344: 
            aux = "ERR-Parametr";
            break;

        case 16777280: 
            aux = "ERR-Tuxedo  ";
            break;

        case 16778240: 
            aux = "TUX-";
            break;

        case 16785408: 
            aux = "ERR-Volantis";
            break;

        case 16785410: 
            aux = "VOL-";
            break;

        case 16793600: 
            aux = "ERR-Vignette";
            break;

        case 16777248: 
            aux = "ERR-Sistema ";
            break;

        case 16777728: 
            aux = "SYS-";
            break;

        case 134217729: 
            aux = "MSJ-Excepcio";
            break;

        case 33554688: 
            aux = "REA-Fichero ";
            break;

        case 33554944: 
            aux = "REA-Tabla   ";
            break;

        case 134217730: 
        case 134217736: 
            aux = "MSJ-Comentar";
            break;

        case 134217732: 
        case 134217744: 
        case 134217760: 
        case 134217792: 
            aux = "MSJ-ElemProc";
            break;

        case 134217856: 
            aux = "MSJ-";
            break;

        case 33558528: 
            aux = "CAL-";
            break;

        case 16781312: 
            aux = "ERR-";
            break;

        case 33555456: 
            aux = "PRO-";
            break;

        case 33562624: 
            aux = "REA-";
            break;

        case 33556480: 
            aux = "SUB-";
            break;
        }
        return aux;
    }

 
    private final boolean hayQtrazar(int nivel)
    {
        boolean hayQtrazar = false;
        if(super.handlers != null)
        {
            for(Enumeration it = super.handlers.elements(); it.hasMoreElements();)
            {
                logj_lj_Handler handler = (logj_lj_Handler)it.nextElement();
                hayQtrazar = (handler.getLevel() & nivel) != 0;
                if(hayQtrazar)
                    return hayQtrazar;
            }

        }
        return hayQtrazar;
    }

    protected String obtenerFechaYHora()
    {
        return formatter.format(new Date());
    }

    protected static final void parsearListaCadenas(String lista, Vector destino)
    {
        if(lista == null)
        {
            destino.removeAllElements();
        } else
        {
            destino.removeAllElements();
            for(StringTokenizer tokenizer = new StringTokenizer(lista); tokenizer.hasMoreTokens(); destino.addElement(tokenizer.nextToken()));
        }
    }

    protected static final int[] parsearListaEnteros(String lista)
    {
        int destino[] = null;
        if(lista != null)
            try
            {
                Vector v = new Vector();
                for(StringTokenizer tokenizer = new StringTokenizer(lista); tokenizer.hasMoreTokens(); v.addElement(new Integer(tokenizer.nextToken())));
                destino = new int[v.size()];
                for(int i = 0; i < v.size(); i++)
                    destino[i] = ((Integer)v.elementAt(i)).intValue();

            }
            catch(NumberFormatException _ex) { }
        return destino;
    }

    protected static Properties parsearProperties(Properties conf)
    {
        if(conf.getProperty("MAY") == null || !conf.getProperty("MAY").equals("SI"))
        {
            Properties prop = new Properties();
            String clave;
            for(Enumeration param = conf.keys(); param.hasMoreElements(); prop.put(clave.toUpperCase(), conf.getProperty(clave)))
                clave = (String)param.nextElement();

            prop.put("MAY", "SI");
            return prop;
        } else
        {
            return conf;
        }
    }

  

 

    public void trazar(int nivelTraza, String textoMensaje)
    {
        if(hayQtrazar(nivelTraza))
            formatearTraza(nivelTraza, getMensaje(nivelTraza), null, null, null, textoMensaje, null);
    }

    public void trazar(int nivelTraza, String origen, int version1, int version2, int version3, String comentario)
    {
        StringBuffer comentarioVersion = new StringBuffer();
        comentarioVersion.append(String.valueOf(version1));
        comentarioVersion.append(".");
        comentarioVersion.append(String.valueOf(version2));
        comentarioVersion.append(".");
        comentarioVersion.append("(");
        comentarioVersion.append(String.valueOf(version3));
        comentarioVersion.append(")");
        comentarioVersion.append(" ");
        comentarioVersion.append(comentario);
        formatearTraza(nivelTraza, "MSJ-Version ", null, null, origen, comentarioVersion.toString(), null);
    }

    public void trazar(int nivelTraza, String origen, String textoMensaje)
    {
        if(hayQtrazar(nivelTraza))
            formatearTraza(nivelTraza, getMensaje(nivelTraza), null, null, origen, textoMensaje, null);
    }

    public void trazar(int nivelTraza, String codigoMensaje, String textoMensaje, String resultado)
    {
        formatearTraza(nivelTraza, codigoMensaje, null, null, null, textoMensaje, resultado);
    }

    public void trazar(int nivelTraza, String entorno, String modulo, String origen, String textoMensaje)
    {
        if(hayQtrazar(nivelTraza))
            formatearTraza(nivelTraza, getMensaje(nivelTraza), entorno, modulo, origen, textoMensaje, null);
    }

    public void trazar(String entorno, String modulo, String origen, Throwable e)
    {
        if(hayQtrazar(0x8000001))
        {
            StringWriter writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));
            String stackTrace = writer.toString();
            for(StringTokenizer token = new StringTokenizer(stackTrace, "\n"); token.hasMoreTokens();)
            {
                String linea = token.nextToken();
                if(linea.length() > 0)
                    formatearTraza(0x8000001, getMensaje(0x8000001), entorno, modulo, origen, linea.substring(0, linea.length()), null);
            }

        }
    }

    public void trazar(Throwable e)
    {
        trazar(((String) (null)), null, null, e);
    }

    public void trazarError(int nivelTraza, String proceso, String tipo, String idServicio, int codigoError, String comentario)
    {
        StringBuffer textoMensaje = new StringBuffer();
        textoMensaje.append(tipo);
        textoMensaje.append(' ');
        textoMensaje.append(idServicio);
        textoMensaje.append(' ');
        textoMensaje.append(codigoError);
        textoMensaje.append(' ');
        textoMensaje.append(comentario);
        formatearTraza(nivelTraza, getMensaje(nivelTraza), null, null, proceso, textoMensaje.toString(), null);
    }

    protected void trazarError(int nivelTraza, String proceso, String tipo, String idServicio, String comentario)
    {
        StringBuffer textoMensaje = new StringBuffer();
        textoMensaje.append(tipo);
        textoMensaje.append(' ');
        textoMensaje.append(idServicio);
        textoMensaje.append(' ');
        textoMensaje.append(comentario);
        formatearTraza(nivelTraza, getMensaje(nivelTraza), null, null, proceso, textoMensaje.toString(), null);
    }

    public void trazarError(int nivelTraza, String entorno, String modulo, String proceso, String tipo, String idServicio, int codigoError, 
            String comentario)
    {
        StringBuffer textoMensaje = new StringBuffer();
        textoMensaje.append(tipo);
        textoMensaje.append(' ');
        textoMensaje.append(idServicio);
        textoMensaje.append(' ');
        textoMensaje.append(codigoError);
        textoMensaje.append(' ');
        textoMensaje.append(comentario);
        formatearTraza(nivelTraza, getMensaje(nivelTraza), entorno, modulo, proceso, textoMensaje.toString(), null);
    }

    public void trazarGenericos(int nivelTraza, String tipo, String textoMensaje)
    {
        if(hayQtrazar(nivelTraza))
            formatearTraza(nivelTraza, getMensaje(nivelTraza) + tipo, null, null, null, textoMensaje, null);
    }

 

}