package es.tsm.logj.lj.log;

public  abstract class logj_lj_Handler {

	
    
		int level; 
		
		
    	protected logj_lj_Handler()
	    {
	    }

	    public abstract void close();

	    public int getLevel()
	    {
	        return level;
	    }

	    public abstract void publish(String s);

	    public void setLevel(int newLevel)
	    {
	        level = newLevel;
	    }

}
