package es.tsm.logj.lj.log;

import es.tsm.logj.lj.conf.logj_lj_Configuracion;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_Handler

abstract class logj_lj_FileHandler extends logj_lj_Handler
{

//    public logj_lj_FileHandler()
//    {
//        idJVM = "";
//    }
//
//    public logj_lj_FileHandler(File fichero)
//    {
//        idJVM = "";
//        ficheroLog = fichero;
//    }
//
//    public synchronized void checkRotation()
//    {
//        if(!ficheroLog.exists())
//            setLogFile(ficheroLog);
//        if(isRotationNecessary())
//            rotate();
//    }
//
//    public void close()
//    {
//        try
//        {
//            printStream.flush();
//            printStream.close();
//            bout.close();
//            fout.close();
//        }
//        catch(IOException e)
//        {
//            setException(e);
//            System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Error al cerrar el fichero"));
//        }
//    }
//
//    public synchronized void flush()
//    {
//        if(printStream != null)
//            try
//            {
//                printStream.flush();
//            }
//            catch(Exception ex)
//            {
//                setException(ex);
//            }
//    }
//
//    public Exception getException()
//    {
//        flush();
//        return lastException;
//    }
//
//    public String getIdJVM()
//    {
//        return idJVM;
//    }
//
//    abstract boolean isRotationNecessary();
//
//    protected void open()
//        throws IOException
//    {
//        fout = new FileOutputStream(ficheroLog.getAbsolutePath(), true);
//        bout = new BufferedOutputStream(fout);
//        printStream = new PrintStream(bout);
//        if(ficheroLog.length() == 0L)
//        {
//            TimeZone tz = TimeZone.getDefault();
//            formatter2 = new SimpleDateFormat("yyyyMMdd HHmmss");
//            formatter2.setTimeZone(tz);
//            String fecha = formatter2.format(new Date());
//            printStream.print(fecha + " MSJ-Version  LOG Ver: " + logj_lj_Configuracion.cteVersionLog1 + "." + logj_lj_Configuracion.cteVersionLog2 + ".(" + logj_lj_Configuracion.cteVersionLog3 + ")" + "\n");
//        }
//    }
//
//    public void publish(String msg)
//    {
//        checkRotation();
//        if(printStream != null)
//        {
//            printStream.println(msg);
//            printStream.flush();
//        }
//    }
//
//    protected abstract void rotate();
//
//    protected void setException(Exception exception)
//    {
//        lastException = exception;
//    }
//
//    public void setLogFile(File file)
//    {
//        try
//        {
//            ficheroLog = file;
//            open();
//        }
//        catch(IOException e)
//        {
//            setException(e);
//        }
//    }

    protected File ficheroLog;
    private Exception lastException;
    protected PrintStream printStream;
    protected String idJVM;
    private long lastCreated;
    protected BufferedOutputStream bout;
    protected FileOutputStream fout;
    private static transient SimpleDateFormat formatter2;
    private static boolean PasaTamnoBuffer = false;

}
