
package es.tsm.logj.lj.log;

import java.io.PrintStream;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_Handler

class logj_lj_ConsoleHandler extends logj_lj_Handler
{
	
    private transient PrintStream outStream;

    public logj_lj_ConsoleHandler()
    {
    }

    public logj_lj_ConsoleHandler(PrintStream outStream)
    {
        this.outStream = outStream;
    }

    public void close()
    {
        outStream.close();
    }

    public final void publish(String msg)
    {
        outStream.println(msg);
        outStream.flush();
    }


    
}
