package es.tsm.logj.lj.log;

import es.tsm.logj.lj.conf.logj_lj_Configuracion;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Hashtable;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_FileHandler, logj_lj_HandlerNoRotation, logj_lj_DateFileHandler, logj_lj_DateSizeFileHandler, 
//            logj_lj_SizeFileHandler

class logj_lj_FileHandlerFactory
{

    logj_lj_FileHandlerFactory()
    {
    }

    private static Hashtable handlers = new Hashtable();
    private static String vm = null;
    private static boolean flagVM;
    
    
    private static String getVM()
    {
        if(flagVM)
        {
            vm = System.getProperty("NUM_ENGINE");
            if(vm == null)
                if(System.getProperty("weblogic.domain") != null || System.getProperty("cm.log.proceso") != null)
                    vm = "";
                else
                    try
                    {
                        Class engineClass = Class.forName("com.kivasoft.engine.Engine");
                        Method currentEngineMethod = engineClass.getMethod("getCurrentEngine", null);
                        Object obj = currentEngineMethod.invoke(engineClass, null);
                        Field engId = obj.getClass().getField("eng_id");
                        vm = (String)engId.get(obj);
                    }
                    catch(Exception _ex)
                    {
                        vm = "";
                    }
        }
        return vm;
    }

    protected static void setFlagVM(boolean flag)
    {
        flagVM = flag;
    }

    public static synchronized logj_lj_FileHandler getHandler(String nombreFich, File par_directorio)
    {
        String nombreFichero = nombreFich;
        String idVm = getVM();
        File directorio = par_directorio;
        File fichCreado = new File(directorio, nombreFichero + ".log");
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
            handler = null;
        handler = null;
        handler = new logj_lj_HandlerNoRotation(directorio.getPath(), nombreFichero, idVm);
        handlers.put(nombreFichero, handler);
        return handler;
    }

    public static synchronized logj_lj_FileHandler getHandler(String nombreFich, File par_directorio, int par_tiempoRotacion, int numMaxFich)
    {
        String nombreFichero = nombreFich;
        String idVm = getVM();
        if(idVm != null && !idVm.equals(""))
            nombreFichero = nombreFich + "_" + idVm;
        File directorio = par_directorio;
        int tiempoRotacion = par_tiempoRotacion;
        File fichCreado = new File(directorio, nombreFichero + ".log");
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
        {
            handler.close();
            handlers.remove(nombreFichero);
            handler = null;
        }
        if(handler != null)
            if(handler instanceof logj_lj_DateFileHandler)
            {
                ((logj_lj_DateFileHandler)handler).setConfig(tiempoRotacion, numMaxFich);
            } else
            {
                handler.close();
                handlers.remove(nombreFichero);
                handler = null;
            }
        if(handler == null)
        {
            handler = new logj_lj_DateFileHandler(directorio.getPath(), nombreFichero, tiempoRotacion, numMaxFich);
            handler.idJVM = idVm;
            handlers.put(nombreFichero, handler);
        }
        return handler;
    }

    public static synchronized logj_lj_FileHandler getHandler(String nombreFich, File par_directorio, int par_tiempoRotacion, int par_tamano, int par_numMaxFicheros)
    {
        String nombreFichero = nombreFich;
        String idVm = getVM();
        if(idVm != null && !idVm.equals(""))
            nombreFichero = nombreFich + "_" + idVm;
        File directorio = new File(par_directorio, nombreFichero + ".log");
        int tamano = par_tamano;
        int tiempoRotacion = par_tiempoRotacion;
        int numMaxFicheros = par_numMaxFicheros;
        File fichCreado = directorio;
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
        {
            handler.close();
            handlers.remove(nombreFichero);
            handler = null;
        }
        if(handler != null)
            if(handler instanceof logj_lj_DateSizeFileHandler)
            {
                ((logj_lj_DateSizeFileHandler)handler).setConfig(tiempoRotacion, tamano, numMaxFicheros);
            } else
            {
                handler.close();
                handlers.remove(nombreFichero);
                handler = null;
            }
        if(handler == null)
        {
            try
            {
                handler = new logj_lj_DateSizeFileHandler(directorio, tiempoRotacion, numMaxFicheros, tamano);
            }
            catch(IOException _ex)
            {
                System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Error al crear el fichero por tiempo y tama\361o"));
            }
            handler.idJVM = idVm;
            handlers.put(nombreFichero, handler);
        }
        return handler;
    }

    public static synchronized logj_lj_FileHandler getHandler(String entorno, String modulo, int tiempoRotacion, File directorio, int numMaxFich)
    {
        String nombreFichero = entorno + "_" + modulo;
        String idVm = getVM();
        if(idVm != null && !idVm.equals(""))
            nombreFichero = entorno + "_" + modulo + "_" + idVm;
        File fichCreado = new File(directorio, nombreFichero + ".log");
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
        {
            handler.close();
            handlers.remove(nombreFichero);
            handler = null;
        }
        if(handler != null)
            if(handler instanceof logj_lj_DateFileHandler)
            {
                ((logj_lj_DateFileHandler)handler).setConfig(tiempoRotacion, numMaxFich);
            } else
            {
                handler.close();
                handlers.remove(nombreFichero);
                handler = null;
            }
        if(handler == null)
        {
            handler = new logj_lj_DateFileHandler(directorio, entorno, modulo, tiempoRotacion, idVm, numMaxFich);
            handler.idJVM = idVm;
            handlers.put(nombreFichero, handler);
        }
        return handler;
    }

    public static synchronized logj_lj_FileHandler getHandler(String entorno, String modulo, File directorio, int tamano, int numMaxFicheros)
    {
        String nombreFichero = entorno + "_" + modulo;
        String idVm = getVM();
        if(idVm != null && !idVm.equals(""))
            nombreFichero = entorno + "_" + modulo + "_" + idVm;
        File dir = new File(directorio, nombreFichero + ".log");
        File fichCreado = dir;
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
        {
            handler.close();
            handlers.remove(nombreFichero);
            handler = null;
        }
        if(handler != null)
            if(handler instanceof logj_lj_SizeFileHandler)
            {
                ((logj_lj_SizeFileHandler)handler).setConfig(tamano, numMaxFicheros);
            } else
            {
                handler.close();
                handlers.remove(nombreFichero);
                handler = null;
            }
        if(handler == null)
        {
            try
            {
                handler = new logj_lj_SizeFileHandler(dir, numMaxFicheros, tamano);
            }
            catch(IOException _ex)
            {
                System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Error al crear el fichero por tama\361o"));
            }
            handler.idJVM = idVm;
            handlers.put(nombreFichero, handler);
        }
        return handler;
    }

    public static synchronized logj_lj_FileHandler getHandler(String entorno, String modulo, File directorio, int tiempoRotacion, int tamano, int numMaxFicheros)
    {
        String nombreFichero = entorno + "_" + modulo;
        String idVm = getVM();
        if(idVm != null && !idVm.equals(""))
            nombreFichero = entorno + "_" + modulo + "_" + idVm;
        File fichCreado = new File(directorio, nombreFichero + ".log");
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
        {
            handler.close();
            handlers.remove(nombreFichero);
            handler = null;
        }
        if(handler != null)
            if(handler instanceof logj_lj_DateSizeFileHandler)
            {
                ((logj_lj_DateSizeFileHandler)handler).setConfig(tiempoRotacion, tamano, numMaxFicheros);
            } else
            {
                handler.close();
                handlers.remove(nombreFichero);
                handler = null;
            }
        if(handler == null)
        {
            try
            {
                handler = new logj_lj_DateSizeFileHandler(fichCreado, tiempoRotacion, numMaxFicheros, tamano);
            }
            catch(IOException _ex)
            {
                System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Error al crear el fichero por tiempo y tama\361o"));
            }
            handler.idJVM = idVm;
            handlers.put(nombreFichero, handler);
        }
        return handler;
    }

 
    public static synchronized logj_lj_FileHandler getHandler(File par_directorio, String nombreFich, int par_tamano, int par_numMaxFicheros)
    {
        String nombreFichero = nombreFich;
        String idVm = getVM();
        if(idVm != null && !idVm.equals(""))
            nombreFichero = nombreFich + "_" + idVm;
        File directorio = new File(par_directorio, nombreFichero + ".log");
        int tamano = par_tamano;
        int numMaxFicheros = par_numMaxFicheros;
        File fichCreado = new File(par_directorio, nombreFichero + ".log");
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
        {
            handler.close();
            handlers.remove(nombreFichero);
            handler = null;
        }
        if(handler != null)
            if(handler instanceof logj_lj_SizeFileHandler)
            {
                ((logj_lj_SizeFileHandler)handler).setConfig(tamano, numMaxFicheros);
            } else
            {
                handler.close();
                handlers.remove(nombreFichero);
                handler = null;
            }
        if(handler == null)
        {
            try
            {
                handler = new logj_lj_SizeFileHandler(directorio, numMaxFicheros, tamano);
            }
            catch(IOException _ex)
            {
                System.out.println(logj_lj_Configuracion.formatearMensaje("PRO-Error   ", "", "", "", "Error al crear el fichero por tama\361o"));
            }
            handler.idJVM = idVm;
            handlers.put(nombreFichero, handler);
        }
        return handler;
    }

    public static synchronized logj_lj_FileHandler getHandlerRefresco(String nombreFich, File par_directorio, int par_tiempoRotacion, int numMaxFich)
    {
        String nombreFichero = nombreFich;
        String idVm = getVM();
        if(idVm != null && !idVm.equals(""))
            nombreFichero = nombreFich + "_" + idVm;
        File directorio = par_directorio;
        int tiempoRotacion = par_tiempoRotacion;
        File fichCreado = new File(directorio, nombreFichero + ".log");
        logj_lj_FileHandler handler = (logj_lj_FileHandler)handlers.get(nombreFichero);
        if(handler != null && !fichCreado.exists())
        {
            handler.close();
            handlers.remove(nombreFichero);
            handler = null;
        }
        if(handler != null)
            if(handler instanceof logj_lj_DateFileHandler)
            {
                ((logj_lj_DateFileHandler)handler).setConfig(tiempoRotacion, numMaxFich);
            } else
            {
                handler.close();
                handlers.remove(nombreFichero);
                handler = null;
            }
        if(handler == null)
        {
            handler = new logj_lj_DateFileHandler(directorio.getPath(), nombreFichero, tiempoRotacion, numMaxFich);
            handler.idJVM = idVm;
            handlers.put(nombreFichero, handler);
        }
        return handler;
    }



}