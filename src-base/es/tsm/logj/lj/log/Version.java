
package es.tsm.logj.lj.log;

import es.tsm.logj.lj.conf.logj_lj_Configuracion;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Version
{

    public Version()
    {
    }

    public static void main(String args[])
    {
        TimeZone tz = TimeZone.getDefault();
        formatter2 = new SimpleDateFormat("yyyyMMdd HHmmss");
        formatter2.setTimeZone(tz);
        fecha = formatter2.format(new Date());
        System.out.println(fecha + " MSJ-Version  LOG Ver: " + logj_lj_Configuracion.cteVersionLog1 + "." + logj_lj_Configuracion.cteVersionLog2 + ".(" + logj_lj_Configuracion.cteVersionLog3 + ")" + "\n");
    }

    private static transient SimpleDateFormat formatter2;
    private static String fecha = "";

}