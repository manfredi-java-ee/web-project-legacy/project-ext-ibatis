package es.tsm.logj.lj.log;

import es.tsm.logj.lj.conf.logj_lj_Configuracion;
import java.io.File;
import java.util.*;

// Referenced classes of package es.tsm.logj.lj.log:
//            logj_lj_TrazadorTsm, logj_lj_TrazadorWasTsm, logj_lj_Logger, logj_lj_ElementosWas, 
//            logj_lj_SizeFileHandler, logj_lj_Handler, logj_lj_DateSizeFileHandler, logj_lj_DateFileHandler, 
//            logj_lj_FileHandlerFactory, logj_lj_FileHandler

class logj_lj_PoolTrazadores extends logj_lj_TrazadorTsm
{
	
	
    private Vector vPool;
    private long tiempoDecremento;
    private long tiempoCracion;

    logj_lj_PoolTrazadores()
    {
        tiempoDecremento = 0L;
        tiempoCracion = 0L;
    }

    protected long getTiempoCracion()
    {
        return tiempoCracion;
    }

    protected long getTiempoDecremento()
    {
        return tiempoDecremento;
    }

    public void incrementaNumTrazadores(Properties prop, String entorno, String modulo, int modo)
        throws Exception
    {
        int numTrazadoresIncDec = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteIncDecTrazador));
        logj_lj_TrazadorWasTsm trazador = null;
        try
        {
            for(int i = 0; i < numTrazadoresIncDec; i++)
            {
         //       trazador = logj_lj_TrazadorWasTsm.crearTrazadorWas(prop, entorno, modulo, modo);
                trazador.setEstado(true);
                vPool.add(trazador);
                trazador.setTiempoCreacion((new Date()).getTime());
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    public void incrementaNumTrazadores(Properties prop, String entorno, String modulo, String identificador, int modo)
        throws Exception
    {
        int numTrazadoresIncDec = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteIncDecTrazador));
        logj_lj_TrazadorWasTsm trazador = null;
        try
        {
            for(int i = 0; i < numTrazadoresIncDec; i++)
            {
         //       trazador = logj_lj_TrazadorWasTsm.crearTrazadorWas(prop, entorno, modulo, identificador, modo);
                trazador.setEstado(true);
                vPool.add(trazador);
                trazador.setTiempoCreacion((new Date()).getTime());
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    public void init(Properties prop, String nombreFichConfig, String entorno, String modulo, int modo)
        throws Exception
    {
        logj_lj_TrazadorWasTsm trazador = null;
        try
        {
            int numTrazadores = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNTrazador));
            vPool = new Vector(numTrazadores);
            for(int i = 0; i < vPool.capacity(); i++)
            {
             //   trazador = logj_lj_TrazadorWasTsm.crearTrazadorWas(prop, entorno, modulo, modo);
                trazador.prop = prop;
                trazador.setTiempoCreacion((new Date()).getTime());
                trazador.setNombreFich(nombreFichConfig);
                trazador.setVersionGetTraz(true);
                trazador.setEstado(true);
                vPool.add(trazador);
            }

            setTiempoCreacion((new Date()).getTime());
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    public void init(Properties prop, String nombreFichConfig, String entorno, String modulo, String identificador, int modo)
        throws Exception
    {
        logj_lj_TrazadorWasTsm trazador = null;
        try
        {
            int numTrazadores = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNTrazador));
            vPool = new Vector(numTrazadores);
            for(int i = 0; i < vPool.capacity(); i++)
            {
           //     trazador = logj_lj_TrazadorWasTsm.crearTrazadorWas(prop, entorno, modulo, identificador, modo);
                trazador.prop = prop;
                trazador.setTiempoCreacion((new Date()).getTime());
                trazador.setNombreFich(nombreFichConfig);
                trazador.setVersionGetTraz(true);
                trazador.setEstado(true);
                vPool.add(trazador);
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    public void libTrazadorTimeOut(Properties prop)
    {
        long timeOut = Long.parseLong(prop.getProperty(logj_lj_Configuracion.cteTimeOut));
        timeOut *= 1000L;
        long tiempoUltimoMov = 0L;
        logj_lj_TrazadorWasTsm oTrazador = null;
        for(int i = 0; i < vPool.size(); i++)
        {
            oTrazador = (logj_lj_TrazadorWasTsm)vPool.elementAt(i);
            if(!oTrazador.isLibre())
            {
                tiempoUltimoMov = oTrazador.getTiempoUltimoMov();
                if(tiempoUltimoMov == 0L)
                    tiempoUltimoMov = (new Date()).getTime() - oTrazador.getTiempoCreacion();
                else
                    tiempoUltimoMov = (new Date()).getTime() - tiempoUltimoMov;
                if(tiempoUltimoMov > timeOut)
                {
                    oTrazador.setTiempoLiberacion((new Date()).getTime());
                    oTrazador.setEstado(true);
                }
            }
        }

    }

    public void refrescarTrazadores(Properties prop)
    {
        int tamanoMax = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteTamanoMax));
        int numMaxFich = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNumMaxFich));
        int segRotacion = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteSegundosRotacion));
        int nivel = 0;
        nivel = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNiveltraza));
        String identificador = "";
        for(int i = 0; i < vPool.size(); i++)
        {
            logj_lj_TrazadorWasTsm oTrazador = (logj_lj_TrazadorWasTsm)vPool.elementAt(i);
            oTrazador.setConfig(prop);
            int tamanoGestor = ((logj_lj_Logger) (oTrazador)).handlers.size();
            oTrazador.getNombreFich();
            identificador = logj_lj_TrazadorWasTsm.elementosWas.getIdentificador(oTrazador.getNombreFich());
            int nivelTrazaAnterior = logj_lj_TrazadorWasTsm.elementosWas.getNivelTrazaAnterior(oTrazador.getNombreFich());
            for(int x = 0; x < tamanoGestor; x++)
            {
                if(((logj_lj_Logger) (oTrazador)).handlers.elementAt(x) instanceof logj_lj_SizeFileHandler)
                {
                    ((logj_lj_SizeFileHandler)((logj_lj_Logger) (oTrazador)).handlers.elementAt(x)).setConfig(tamanoMax, numMaxFich);
                    nivel = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    super.nivelTraza = logj_lj_TrazadorTsm.getMascaraNivelTraza(nivel);
                    ((logj_lj_SizeFileHandler)((logj_lj_Logger) (oTrazador)).handlers.elementAt(x)).setLevel(super.nivelTraza);
                    oTrazador.nivelTraza = super.nivelTraza;
                } else
                if(((logj_lj_Logger) (oTrazador)).handlers.elementAt(x) instanceof logj_lj_DateSizeFileHandler)
                {
                    ((logj_lj_DateSizeFileHandler)((logj_lj_Logger) (oTrazador)).handlers.elementAt(x)).setConfig(segRotacion, tamanoMax, numMaxFich);
                    nivel = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    super.nivelTraza = logj_lj_TrazadorTsm.getMascaraNivelTraza(nivel);
                    ((logj_lj_DateSizeFileHandler)((logj_lj_Logger) (oTrazador)).handlers.elementAt(x)).setLevel(super.nivelTraza);
                    oTrazador.nivelTraza = super.nivelTraza;
                } else
                if(((logj_lj_Logger) (oTrazador)).handlers.elementAt(x) instanceof logj_lj_DateFileHandler)
                {
                    ((logj_lj_DateFileHandler)((logj_lj_Logger) (oTrazador)).handlers.elementAt(x)).setConfig(segRotacion, numMaxFich);
                    nivel = Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNiveltraza));
                    super.nivelTraza = logj_lj_TrazadorTsm.getMascaraNivelTraza(nivel);
                    ((logj_lj_DateFileHandler)((logj_lj_Logger) (oTrazador)).handlers.elementAt(x)).setLevel(super.nivelTraza);
                    oTrazador.nivelTraza = super.nivelTraza;
                }
                if(nivelTrazaAnterior != 3 && nivelTrazaAnterior != 0)
                    try
                    {
                        if(Integer.parseInt(prop.getProperty(logj_lj_Configuracion.cteNiveltraza)) == 3)
                        {
                            logj_lj_FileHandler handlerN3 = refrescoTrazdorWasIdentif(prop, identificador);
                            if(handlerN3 != null)
                                ((logj_lj_Logger) (oTrazador)).handlers.addElement(handlerN3);
                        }
                    }
                    catch(Exception _ex) { }
                if(nivel != 3 && ((logj_lj_Logger) (oTrazador)).handlers.size() > 1 && x > 0)
                    ((logj_lj_Logger) (oTrazador)).handlers.remove(x);
            }

        }

    }

    protected logj_lj_FileHandler refrescoTrazdorWasIdentif(Properties conf, String identificador)
        throws Exception
    {
        int nivel = Integer.parseInt(conf.getProperty(logj_lj_Configuracion.cteNiveltraza));
        int tipoRotacion = Integer.parseInt(conf.getProperty(logj_lj_Configuracion.cteTipoRotacion));
        File directorio = new File(conf.getProperty(logj_lj_Configuracion.ctePathArchivoLog));
        int segundosRotacion = Integer.parseInt(conf.getProperty(logj_lj_Configuracion.cteSegundosRotacion));
        int tamano = Integer.parseInt(conf.getProperty(logj_lj_Configuracion.cteTamanoMax));
        int numFich = Integer.parseInt(conf.getProperty(logj_lj_Configuracion.cteNumMaxFich));
        Vector identificadores = new Vector();
        logj_lj_FileHandler handlerN3 = null;
        int nivelesIds[] = null;
        Vector nombresFicherosIds = new Vector();
        if(nivel == 3)
        {
            logj_lj_TrazadorTsm.parsearListaCadenas(conf.getProperty(logj_lj_Configuracion.cteIDTraza), identificadores);
            logj_lj_TrazadorTsm.parsearListaCadenas(conf.getProperty(logj_lj_Configuracion.cteIDFichTraza), nombresFicherosIds);
            nivelesIds = logj_lj_TrazadorTsm.parsearListaEnteros(conf.getProperty(logj_lj_Configuracion.cteIDNivelTraza));
        }
        if(nivel == 3 && identificadores.contains(identificador))
        {
            String nombreFich = (String)nombresFicherosIds.elementAt(identificadores.indexOf(identificador));
            nombreFich = nombreFich.toLowerCase();
            switch(tipoRotacion)
            {
            case 0: // '\0'
                handlerN3 = logj_lj_FileHandlerFactory.getHandler(nombreFich, directorio, segundosRotacion, numFich);
                break;

            case 1: // '\001'
                handlerN3 = logj_lj_FileHandlerFactory.getHandler(directorio, nombreFich, tamano, numFich);
                break;

            case 2: // '\002'
                handlerN3 = logj_lj_FileHandlerFactory.getHandler(nombreFich, directorio, segundosRotacion, tamano, numFich);
                break;
            }
            if(handlerN3 != null)
            {
             //   if(handlerN3.getException() != null)
             //       throw handlerN3.getException();
                int nivel3 = 1;
                try
                {
                    nivel3 = nivelesIds[identificadores.indexOf(identificador)];
                }
                catch(Exception _ex) { }
                handlerN3.setLevel(logj_lj_TrazadorTsm.getMascaraNivelTraza(nivel3));
                addHandler(handlerN3);
            }
        }
        return handlerN3;
    }

    public logj_lj_TrazadorWasTsm retornarTrazador(Properties prop, String entorno, String modulo, int modo)
        throws Exception
    {
        logj_lj_TrazadorWasTsm oTrazador = null;
        try
        {
            int i;
            for(i = 0; i < vPool.size(); i++)
                if(((logj_lj_TrazadorWasTsm)vPool.elementAt(i)).getEstado())
                {
                    oTrazador = ((logj_lj_TrazadorWasTsm)vPool.elementAt(i)).obtener();
                    oTrazador.setModo(modo);
                    return oTrazador;
                }

            incrementaNumTrazadores(prop, entorno, modulo, modo);
            oTrazador = ((logj_lj_TrazadorWasTsm)vPool.elementAt(i)).obtener();
            oTrazador.setModo(modo);
            return oTrazador;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    public logj_lj_TrazadorWasTsm retornarTrazador(Properties prop, String entorno, String modulo, String identificador, int modo)
        throws Exception
    {
        logj_lj_TrazadorWasTsm oTrazador = null;
        try
        {
            int i;
            for(i = 0; i < vPool.size(); i++)
                if(((logj_lj_TrazadorWasTsm)vPool.elementAt(i)).getEstado())
                {
                    oTrazador = ((logj_lj_TrazadorWasTsm)vPool.elementAt(i)).obtener();
                    oTrazador.setModo(modo);
                    return oTrazador;
                }

            incrementaNumTrazadores(prop, entorno, modulo, identificador, modo);
            oTrazador = ((logj_lj_TrazadorWasTsm)vPool.elementAt(i)).obtener();
            oTrazador.setModo(modo);
            return oTrazador;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    protected void setTiempoCreacion(long tTiempoCracion)
    {
        tiempoCracion = tTiempoCracion;
    }

    protected void setTiempoDecremento(long tDecremento)
    {
        tiempoDecremento = tDecremento;
    }

    public void liberarNoUtilizados(long tiempoInactividad, long numTrazadoresaIncrDecr, int numTrazadoresCrear)
    {
        long contBorrados = 0L;
        long contLibres = 0L;
        long tiempoLibre = 0L;
        long tiempoActual = 0L;
        long tiempoTotal = 0L;
        for(int i = 0; i < vPool.size(); i++)
        {
            logj_lj_TrazadorWasTsm oTrazador = (logj_lj_TrazadorWasTsm)vPool.elementAt(i);
            if(oTrazador.isLibre())
            {
                tiempoLibre = oTrazador.getTiempoLiberacion();
                if(tiempoLibre == 0L)
                    tiempoLibre = oTrazador.getTiempoCreacion();
                tiempoActual = (new Date()).getTime();
                tiempoTotal = tiempoActual - tiempoLibre;
                if(tiempoTotal > tiempoInactividad)
                    contLibres++;
            }
        }

        if(contLibres >= numTrazadoresaIncrDecr)
        {
            for(int i = 0; i < vPool.size(); i++)
            {
                logj_lj_TrazadorWasTsm oTrazador = (logj_lj_TrazadorWasTsm)vPool.elementAt(i);
                if(oTrazador.isLibre())
                {
                    tiempoLibre = oTrazador.getTiempoLiberacion();
                    if(tiempoLibre == 0L)
                        tiempoLibre = oTrazador.getTiempoCreacion();
                    tiempoActual = (new Date()).getTime();
                    tiempoTotal = tiempoActual - tiempoLibre;
                    if(tiempoTotal > tiempoInactividad && contBorrados < numTrazadoresaIncrDecr && vPool.size() > numTrazadoresCrear)
                    {
                        vPool.remove(i);
                        contBorrados++;
                        oTrazador = null;
                        i--;
                        setTiempoDecremento((new Date()).getTime());
                    }
                }
            }

        }
    }


}
