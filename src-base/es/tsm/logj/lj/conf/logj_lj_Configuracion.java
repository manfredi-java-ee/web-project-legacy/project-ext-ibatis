package es.tsm.logj.lj.conf;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class logj_lj_Configuracion
{
    private static final String KEY_CADUCIDAD_CONFIG = "SEGUNDOSCONFIGURACIONVALIDA";
    private static Hashtable configuracion = new Hashtable();
    private String nombreFicheroConfiguracion;
    private long tiempoLectura;
    private static final int TIMEOUT_DEFECTO = 30000;
    private long timeOutLectura;
    private Properties confFile;
    public static int cteVersionLog1 = 1;
    public static int cteVersionLog2 = 2;
    public static int cteVersionLog3 = 0;
    public static String cteNiveltraza = "NIVELTRAZA";
    public static String cteSegundosRotacion = "SEGUNDOSROTACION";
    public static String ctePathArchivoLog = "PATHARCHIVOLOG";
    public static String cteTamanoMax = "TAMANOMAX";
    public static String cteNumMaxFich = "NUMMAXFICH";
    public static String cteTipoRotacion = "TIPOROTACION";
    public static String cteIDTraza = "IDTRAZA";
    public static String cteIDFichTraza = "IDFICHTRAZA";
    public static String cteIDNivelTraza = "IDNIVELTRAZA";
    public static String cteSegundosConfiguracionValida = "SEGUNDOSCONFIGURACIONVALIDA";
    public static String cteVM = "VM";
    public static String cteNombreSH = "NOMBRESH";
    public static String cteSalida = "SALIDA";
    public static String cteEstadistica = "ESTADISTICA";
    public static String cteSegundosVolcadoEstadisticas = "SEGUNDOSVOLCADOESTADISTICAS";
    public static String cteNTrazador = "NTRAZADOR";
    public static String cteIncDecTrazador = "INCDECTRAZADOR";
    public static String cteTimeOut = "TIMEOUT";
    public static String cteTiempoInactTrazador = "TIEMPOINACTIVIDADTRAZADOR";
    public static String cteTiempoComprobacionInactTrazador = "TIEMPOCOMPROBACIONINACTRAZADOR";
    public static String cteTamanoBuffer = "TAMANOBUFFER";
    public static String cteThread = "THREAD";
    public static final String cteLogPunto = "LOG.";
    public static String tiempoUltimaLectura = "TIEMPOULTIMALECTURA";
    private static String version;
    private static String proceso;

    protected logj_lj_Configuracion(String nombreFichConf)
        throws Exception
    {
        tiempoLectura = 0L;
        timeOutLectura = 30000L;
        confFile = null;
        nombreFicheroConfiguracion = nombreFichConf;
        try
        {
            leerConfiguracion(nombreFicheroConfiguracion);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    protected logj_lj_Configuracion(String nombreFichConf, String nombreProceso)
        throws Exception
    {
        tiempoLectura = 0L;
        timeOutLectura = 30000L;
        confFile = null;
        nombreFicheroConfiguracion = nombreFichConf;
        try
        {
            leerConfiguracion(nombreFicheroConfiguracion, nombreProceso);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    protected void cargarConstantesPorVersion()
        throws Exception
    {
        if(getVersion() == null || getVersion().equals("1.0"))
        {
            cteNiveltraza = "NIVELTRAZA";
            cteSegundosRotacion = "SEGUNDOSROTACION";
            ctePathArchivoLog = "PATHARCHIVOLOG";
            cteTamanoMax = "TAMANOMAX";
            cteNumMaxFich = "NUMMAXFICH";
            cteTipoRotacion = "TIPOROTACION";
            cteIDTraza = "IDTRAZA";
            cteIDFichTraza = "IDFICHTRAZA";
            cteIDNivelTraza = "IDNIVELTRAZA";
            cteSegundosConfiguracionValida = "SEGUNDOSCONFIGURACIONVALIDA";
            cteVM = "VM";
            cteNombreSH = "NOMBRESH";
        } else
        if(getVersion().equals("2.0"))
        {
            cteNiveltraza = "LOG.NIVELTRAZA";
            cteSegundosRotacion = "LOG.SEGUNDOSROTACION";
            ctePathArchivoLog = "LOG.PATHARCHIVOLOG";
            cteTamanoMax = "LOG.TAMANOMAX";
            cteNumMaxFich = "LOG.NUMMAXFICH";
            cteTipoRotacion = "LOG.TIPOROTACION";
            cteIDTraza = "LOG.IDTRAZA";
            cteIDFichTraza = "LOG.IDFICHTRAZA";
            cteIDNivelTraza = "LOG.IDNIVELTRAZA";
            cteSegundosConfiguracionValida = "LOG.SEGUNDOSCONFIGURACIONVALIDA";
            cteVM = "LOG.VM";
            cteNombreSH = "LOG.NOMBRESH";
            cteSalida = "LOG.SALIDA";
            cteEstadistica = "LOG.ESTADISTICA";
            cteSegundosVolcadoEstadisticas = "LOG.SEGUNDOSVOLCADOESTADISTICAS";
            cteNTrazador = "LOG.NTRAZADOR";
            cteIncDecTrazador = "LOG.INCDECTRAZADOR";
            cteTimeOut = "LOG.TIMEOUT";
            cteTiempoInactTrazador = "LOG.TIEMPOINACTIVIDADTRAZADOR";
            cteTiempoComprobacionInactTrazador = "LOG.TIEMPOCOMPROBACIONINACTRAZADOR";
            cteTamanoBuffer = "LOG.TAMANOBUFFER";
            cteThread = "LOG.THREAD";
        }
    }

    protected void comprobarNomenclatura(Properties confFile)
        throws Exception
    {
        String PosibilidadVersion = "";
        String VersionError = "";
        PosibilidadVersion = getVersion();
        if(PosibilidadVersion == null)
            VersionError = "";
        else
            VersionError = PosibilidadVersion;
        PosibilidadVersion = "";
        cteNiveltraza = "NIVELTRAZA";
        cteSegundosRotacion = "SEGUNDOSROTACION";
        ctePathArchivoLog = "PATHARCHIVOLOG";
        cteTamanoMax = "TAMANOMAX";
        cteNumMaxFich = "NUMMAXFICH";
        cteTipoRotacion = "TIPOROTACION";
        cteIDTraza = "IDTRAZA";
        cteIDFichTraza = "IDFICHTRAZA";
        cteIDNivelTraza = "IDNIVELTRAZA";
        cteSegundosConfiguracionValida = "SEGUNDOSCONFIGURACIONVALIDA";
        cteVM = "VM";
        cteNombreSH = "NOMBRESH";
        String EncontradoVariable = "";
        String ValorGetVersion = confFile.getProperty(cteNiveltraza);
        if(ValorGetVersion != null)
        {
            PosibilidadVersion = "1.0";
            EncontradoVariable = "1";
        }
        ValorGetVersion = confFile.getProperty("LOG." + cteNiveltraza);
        if(ValorGetVersion != null)
            if(EncontradoVariable.equals(""))
            {
                PosibilidadVersion = "2.0";
            } else
            {
                Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                throw ex;
            }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteSegundosRotacion);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteSegundosRotacion);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(ctePathArchivoLog);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + ctePathArchivoLog);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteTamanoMax);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteTamanoMax);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteNumMaxFich);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteNumMaxFich);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteTipoRotacion);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteTipoRotacion);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteIDTraza);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteIDTraza);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteIDFichTraza);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
                EncontradoVariable = "1";
            } else
            if(PosibilidadVersion.equals(""))
                PosibilidadVersion = "1.0";
        ValorGetVersion = confFile.getProperty("LOG." + cteIDFichTraza);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        ValorGetVersion = confFile.getProperty(cteIDNivelTraza);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteIDNivelTraza);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteSegundosConfiguracionValida);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteSegundosConfiguracionValida);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteVM);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteVM);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        EncontradoVariable = "";
        ValorGetVersion = confFile.getProperty(cteNombreSH);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("2.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
            {
                PosibilidadVersion = "1.0";
                EncontradoVariable = "1";
            }
        ValorGetVersion = confFile.getProperty("LOG." + cteNombreSH);
        if(ValorGetVersion != null)
            if(PosibilidadVersion.equals("1.0"))
            {
                if(VersionError.equals(""))
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
                PosibilidadVersion = "no";
            } else
            if(PosibilidadVersion.equals(""))
                if(EncontradoVariable.equals(""))
                {
                    PosibilidadVersion = "2.0";
                } else
                {
                    Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                    throw ex;
                }
        cteSalida = "SALIDA";
        cteEstadistica = "ESTADISTICA";
        cteSegundosVolcadoEstadisticas = "SEGUNDOSVOLCADOESTADISTICAS";
        cteNTrazador = "NTRAZADOR";
        cteIncDecTrazador = "INCDECTRAZADOR";
        cteTimeOut = "TIMEOUT";
        cteTiempoInactTrazador = "TIEMPOINACTIVIDADTRAZADOR";
        cteTiempoComprobacionInactTrazador = "TIEMPOCOMPROBACIONINACTRAZADOR";
        cteTamanoBuffer = "TAMANOBUFFER";
        cteThread = "THREAD";
        ValorGetVersion = confFile.getProperty(cteSalida);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteSalida);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteEstadistica);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteEstadistica);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteSegundosVolcadoEstadisticas);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteSegundosVolcadoEstadisticas);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteNTrazador);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteNTrazador);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteIncDecTrazador);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteIncDecTrazador);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteTimeOut);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteTimeOut);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteTiempoInactTrazador);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteTiempoInactTrazador);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteTiempoComprobacionInactTrazador);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteTiempoComprobacionInactTrazador);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        ValorGetVersion = confFile.getProperty(cteTamanoBuffer);
        if(ValorGetVersion != null)
        {
            if(VersionError.equals(""))
            {
                Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
                throw ex;
            }
        } else
        {
            ValorGetVersion = confFile.getProperty("LOG." + cteTamanoBuffer);
            if(ValorGetVersion != null)
                if(PosibilidadVersion.equals("1.0"))
                {
                    if(VersionError.equals(""))
                    {
                        Exception ex = new Exception("Se han encontrado par\341metros en distintas versiones.");
                        System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Se han encontrado par\341metros en distintas versiones."));
                        throw ex;
                    }
                    PosibilidadVersion = "no";
                } else
                if(PosibilidadVersion.equals(""))
                    PosibilidadVersion = "2.0";
        }
        for(Enumeration e = confFile.propertyNames(); e.hasMoreElements();)
        {
            String key = (String)e.nextElement();
            if(key.indexOf("LOG.") == 0 && key.substring(4, key.length()).indexOf("LOG.") == 0)
            {
                Exception ex = new Exception("Hay parametros con Log.Log. cuando tan s\363lo debe tener Log.");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Hay parametros con Log.Log. cuando tan s\363lo debe tener Log."));
                throw ex;
            }
        }

        if(PosibilidadVersion.equals("no"))
            setVersion(VersionError);
        else
            setVersion(PosibilidadVersion);
    }

    protected void comprobarNomenclaturaSecciones(Properties confFile)
        throws Exception
    {
        setVersion("2.0");
        cteNiveltraza = "NIVELTRAZA";
        cteSegundosRotacion = "SEGUNDOSROTACION";
        ctePathArchivoLog = "PATHARCHIVOLOG";
        cteTamanoMax = "TAMANOMAX";
        cteNumMaxFich = "NUMMAXFICH";
        cteTipoRotacion = "TIPOROTACION";
        cteIDTraza = "IDTRAZA";
        cteIDFichTraza = "IDFICHTRAZA";
        cteIDNivelTraza = "IDNIVELTRAZA";
        cteSegundosConfiguracionValida = "SEGUNDOSCONFIGURACIONVALIDA";
        cteVM = "VM";
        cteNombreSH = "NOMBRESH";
        String ValorGetVersion = confFile.getProperty(cteNiveltraza);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteSegundosRotacion);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(ctePathArchivoLog);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteTamanoMax);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteNumMaxFich);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteTipoRotacion);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteIDTraza);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteIDFichTraza);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteIDNivelTraza);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteSegundosConfiguracionValida);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteVM);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteNombreSH);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG.");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.0.2 deben tener el formato 1.2.0 con LOG."));
            throw ex;
        }
        cteSalida = "SALIDA";
        cteEstadistica = "ESTADISTICA";
        cteSegundosVolcadoEstadisticas = "SEGUNDOSVOLCADOESTADISTICAS";
        cteNTrazador = "NTRAZADOR";
        cteIncDecTrazador = "INCDECTRAZADOR";
        cteTimeOut = "TIMEOUT";
        cteTiempoInactTrazador = "TIEMPOINACTIVIDADTRAZADOR";
        cteTiempoComprobacionInactTrazador = "TIEMPOCOMPROBACIONINACTRAZADOR";
        cteTamanoBuffer = "TAMANOBUFFER";
        ValorGetVersion = confFile.getProperty(cteSalida);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteEstadistica);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteSegundosVolcadoEstadisticas);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteNTrazador);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteIncDecTrazador);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteTimeOut);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteTiempoInactTrazador);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteTiempoComprobacionInactTrazador);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteTamanoBuffer);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        ValorGetVersion = confFile.getProperty(cteThread);
        if(ValorGetVersion != null)
        {
            Exception ex = new Exception("Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2");
            System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros de versi\363n 1.2.0 no pueden tener la configuraci\363n de los par\341metros en 1.0.2"));
            throw ex;
        }
        for(Enumeration e = confFile.propertyNames(); e.hasMoreElements();)
        {
            String key = (String)e.nextElement();
            if(key.indexOf("LOG.") == 0 && key.substring(4, key.length()).indexOf("LOG.") == 0)
            {
                Exception ex = new Exception("Hay parametros con Log.Log. cuando tan s\363lo debe tener Log.");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Hay parametros con Log.Log. cuando tan s\363lo debe tener Log."));
                throw ex;
            }
        }

    }

    private void comprobarParametros(Properties confAnterior)
        throws Exception
    {
        String SegundosRotacion = "";
        String SegundosConfiguracionValida = "";
        String VM = "";
        String NombreSH = "";
        String niveltraza = "";
        String segundosRotacion = "";
        String pathArchivoLog = "";
        String tamanoMax = "";
        String numMaxFich = "";
        String tipoRotacion = "";
        String IDTraza = "";
        String IDFichTraza = "";
        String IDNivelTraza = "";
        String niveltrazaAnt = "";
        String segundosRotacionAnt = "";
        String tamanoMaxAnt = "";
        String numMaxFichAnt = "";
        String tipoRotacionAnt = "";
        String IDTrazaAnt = "";
        String IDFichTrazaAnt = "";
        String IDNivelTrazaAnt = "";
        String SegundosRotacionAnt = "";
        String SegundosConfiguracionValidaAnt = "";
        String VMAnt = "";
        String NombreSHAnt = "";
        String Salida = "";
        String Estadistica = "";
        String SegundosVolcadoEstadisticas = "";
        String NTrazador = "";
        String IncDecTrazador = "";
        String TimeOut = "";
        String TiempoInactTrazador = "";
        String TiempoComprobacionInactTrazador = "";
        String TamanoBuffer = "";
        String pThread = "";
        String SalidaAnt = "";
        String EstadisticaAnt = "";
        String SegundosVolcadoEstadisticasAnt = "";
        String NTrazadorAnt = "";
        String IncDecTrazadorAnt = "";
        String TimeOutAnt = "";
        String TiempoInactTrazadorAnt = "";
        String TiempoComprobacionInactTrazadorAnt = "";
        String TamanoBufferAnt = "";
        long AuxiliarComprobacion = 0L;
        
        
        
        if(confAnterior != null)
        {
            SegundosRotacionAnt = confAnterior.getProperty(cteSegundosRotacion);
            SegundosConfiguracionValidaAnt = confAnterior.getProperty(cteSegundosConfiguracionValida);
            VMAnt = confAnterior.getProperty(cteVM);
            NombreSHAnt = confAnterior.getProperty(cteNombreSH);
            niveltrazaAnt = confAnterior.getProperty(cteNiveltraza);
            segundosRotacionAnt = confAnterior.getProperty(cteSegundosRotacion);
            tamanoMaxAnt = confAnterior.getProperty(cteTamanoMax);
            numMaxFichAnt = confAnterior.getProperty(cteNumMaxFich);
            tipoRotacionAnt = confAnterior.getProperty(cteTipoRotacion);
            IDTrazaAnt = confAnterior.getProperty(cteIDTraza);
            IDFichTrazaAnt = confAnterior.getProperty(cteIDFichTraza);
            IDNivelTrazaAnt = confAnterior.getProperty(cteIDNivelTraza);
            SalidaAnt = confAnterior.getProperty(cteSalida);
            EstadisticaAnt = confAnterior.getProperty(cteEstadistica);
            SegundosVolcadoEstadisticasAnt = confAnterior.getProperty(cteSegundosVolcadoEstadisticas);
            NTrazadorAnt = confAnterior.getProperty(cteNTrazador);
            IncDecTrazadorAnt = confAnterior.getProperty(cteIncDecTrazador);
            TimeOutAnt = confAnterior.getProperty(cteTimeOut);
            TiempoInactTrazadorAnt = confAnterior.getProperty(cteTiempoInactTrazador);
            TiempoComprobacionInactTrazadorAnt = confAnterior.getProperty(cteTiempoComprobacionInactTrazador);
            TamanoBufferAnt = confAnterior.getProperty(cteTamanoBuffer);
        }
        SegundosRotacion = confFile.getProperty(cteSegundosRotacion);
        SegundosConfiguracionValida = confFile.getProperty(cteSegundosConfiguracionValida);
        VM = confFile.getProperty(cteVM);
        NombreSH = confFile.getProperty(cteNombreSH);
        pathArchivoLog = confFile.getProperty(ctePathArchivoLog);
        niveltraza = confFile.getProperty(cteNiveltraza);
        tipoRotacion = confFile.getProperty(cteTipoRotacion);
        segundosRotacion = confFile.getProperty(cteSegundosRotacion);
        tamanoMax = confFile.getProperty(cteTamanoMax);
        numMaxFich = confFile.getProperty(cteNumMaxFich);
        Salida = confFile.getProperty(cteSalida);
        Estadistica = confFile.getProperty(cteEstadistica);
        SegundosVolcadoEstadisticas = confFile.getProperty(cteSegundosVolcadoEstadisticas);
        NTrazador = confFile.getProperty(cteNTrazador);
        IncDecTrazador = confFile.getProperty(cteIncDecTrazador);
        TimeOut = confFile.getProperty(cteTimeOut);
        TiempoInactTrazador = confFile.getProperty(cteTiempoInactTrazador);
        TiempoComprobacionInactTrazador = confFile.getProperty(cteTiempoComprobacionInactTrazador);
        TamanoBuffer = confFile.getProperty(cteTamanoBuffer);
        IDTraza = confFile.getProperty(cteIDTraza);
        IDFichTraza = confFile.getProperty(cteIDFichTraza);
        IDNivelTraza = confFile.getProperty(cteIDNivelTraza);
        pThread = confFile.getProperty(cteThread);
        confFile.clear();
        if(pathArchivoLog != null && !pathArchivoLog.equals(""))
            confFile.put(ctePathArchivoLog, pathArchivoLog);
        if(niveltraza != null && !niveltraza.equals(""))
            confFile.put(cteNiveltraza, niveltraza);
        if(tipoRotacion != null && !tipoRotacion.equals(""))
            confFile.put(cteTipoRotacion, tipoRotacion);
        if(segundosRotacion != null && !segundosRotacion.equals(""))
            confFile.put(cteSegundosRotacion, segundosRotacion);
        if(tamanoMax != null && !tamanoMax.equals(""))
            confFile.put(cteTamanoMax, tamanoMax);
        if(numMaxFich != null && !numMaxFich.equals(""))
            confFile.put(cteNumMaxFich, numMaxFich);
        if(Salida != null && !Salida.equals(""))
            confFile.put(cteSalida, Salida);
        if(Estadistica != null && !Estadistica.equals(""))
            confFile.put(cteEstadistica, Estadistica);
        if(SegundosVolcadoEstadisticas != null && !SegundosVolcadoEstadisticas.equals(""))
            confFile.put(cteSegundosVolcadoEstadisticas, SegundosVolcadoEstadisticas);
        if(NTrazador != null && !NTrazador.equals(""))
            confFile.put(cteNTrazador, NTrazador);
        if(IncDecTrazador != null && !IncDecTrazador.equals(""))
            confFile.put(cteIncDecTrazador, IncDecTrazador);
        if(TimeOut != null && !TimeOut.equals(""))
            confFile.put(cteTimeOut, TimeOut);
        if(TiempoInactTrazador != null && !TiempoInactTrazador.equals(""))
            confFile.put(cteTiempoInactTrazador, TiempoInactTrazador);
        if(TiempoComprobacionInactTrazador != null && !TiempoComprobacionInactTrazador.equals(""))
            confFile.put(cteTiempoComprobacionInactTrazador, TiempoComprobacionInactTrazador);
        if(TamanoBuffer != null && !TamanoBuffer.equals(""))
            confFile.put(cteTamanoBuffer, TamanoBuffer);
        if(pThread != null && !pThread.equals(""))
            confFile.put(cteThread, pThread);
        if(IDTraza != null && !IDTraza.equals(""))
            confFile.put(cteIDTraza, IDTraza);
        if(IDFichTraza != null && !IDFichTraza.equals(""))
            confFile.put(cteIDFichTraza, IDFichTraza);
        if(IDNivelTraza != null && !IDNivelTraza.equals(""))
            confFile.put(cteIDNivelTraza, IDNivelTraza);
        if(SegundosRotacion != null && !SegundosRotacion.equals(""))
            confFile.put(cteSegundosRotacion, SegundosRotacion);
        if(SegundosConfiguracionValida != null && !SegundosConfiguracionValida.equals(""))
            confFile.put(cteSegundosConfiguracionValida, SegundosConfiguracionValida);
        if(VM != null && !VM.equals(""))
            confFile.put(cteVM, VM);
        if(NombreSH != null && !NombreSH.equals(""))
            confFile.put(cteNombreSH, NombreSH);
        if((IDTraza == null || IDTraza.equals("")) && IDTrazaAnt != null)
            confFile.put(cteIDTraza, IDTrazaAnt);
        if((IDFichTraza == null || IDFichTraza.equals("")) && IDFichTrazaAnt != null)
            confFile.put(cteIDFichTraza, IDFichTrazaAnt);
        if((IDNivelTraza == null || IDNivelTraza.equals("")) && IDNivelTrazaAnt != null)
            confFile.put(cteIDNivelTraza, IDNivelTrazaAnt);
        if((IDTraza == null || IDTraza.equals("")) && IDTrazaAnt != null)
            confFile.put(cteIDTraza, IDTrazaAnt);
        if(SegundosRotacion == null || SegundosRotacion.equals(""))
        {
            if(SegundosRotacionAnt == null)
                confFile.put(cteSegundosRotacion, "86400");
            else
                confFile.put(cteSegundosRotacion, SegundosRotacionAnt);
        } else
        {
            try
            {
                Integer.parseInt(SegundosRotacion);
            }
            catch(Exception _ex)
            {
                if(SegundosRotacionAnt == null)
                    confFile.put(cteSegundosRotacion, "86400");
                else
                    confFile.put(cteSegundosRotacion, SegundosRotacionAnt);
            }
        }
        if(SegundosConfiguracionValida == null || SegundosConfiguracionValida.equals(""))
        {
            if(SegundosConfiguracionValidaAnt == null)
                confFile.put(cteSegundosConfiguracionValida, "86400");
            else
                confFile.put(cteSegundosConfiguracionValida, SegundosConfiguracionValidaAnt);
        } else
        {
            try
            {
                Integer.parseInt(SegundosConfiguracionValida);
            }
            catch(Exception _ex)
            {
                if(SegundosConfiguracionValidaAnt == null)
                    confFile.put(cteSegundosConfiguracionValida, "86400");
                else
                    confFile.put(cteSegundosConfiguracionValida, SegundosConfiguracionValidaAnt);
            }
        }
        if(VM == null || VM.equals(""))
            if(VMAnt == null)
                confFile.put(cteVM, "0");
            else
                confFile.put(cteVM, VMAnt);
        if((NombreSH == null || NombreSH.equals("")) && NombreSHAnt != null)
            confFile.put(cteNombreSH, NombreSHAnt);
        if(Salida == null || Salida.equals(""))
        {
            if(SalidaAnt == null)
            {
                confFile.put(cteSalida, "0");
                Salida = "0";
            } else
            {
                confFile.put(cteSalida, SalidaAnt);
                Salida = SalidaAnt;
            }
        } else
        if(!Salida.equals("0") && !Salida.equals("1"))
            if(SalidaAnt == null)
            {
                confFile.remove(cteSalida);
                confFile.put(cteSalida, "0");
            } else
            {
                confFile.remove(cteSalida);
                confFile.put(cteSalida, SalidaAnt);
            }
        if(Estadistica == null || Estadistica.equals(""))
        {
            if(EstadisticaAnt == null)
                confFile.put(cteEstadistica, "0");
            else
                confFile.put(cteEstadistica, EstadisticaAnt);
        } else
        if(!Estadistica.equals("0") && !Estadistica.equals("1"))
            if(EstadisticaAnt == null)
            {
                confFile.remove(cteEstadistica);
                confFile.put(cteEstadistica, "0");
            } else
            {
                confFile.remove(cteEstadistica);
                confFile.put(cteEstadistica, EstadisticaAnt);
            }
        AuxiliarComprobacion = 0L;
        if(SegundosVolcadoEstadisticas == null || SegundosVolcadoEstadisticas.equals(""))
        {
            if(SegundosVolcadoEstadisticasAnt == null)
                confFile.put(cteSegundosVolcadoEstadisticas, "0");
            else
                confFile.put(cteSegundosVolcadoEstadisticas, SegundosVolcadoEstadisticasAnt);
        } else
        {
            try
            {
                AuxiliarComprobacion = Long.parseLong(SegundosVolcadoEstadisticas);
            }
            catch(Exception _ex)
            {
                AuxiliarComprobacion = 0L;
            }
            if(AuxiliarComprobacion < 1L)
                if(SegundosVolcadoEstadisticasAnt == null)
                {
                    confFile.remove(cteSegundosVolcadoEstadisticas);
                    confFile.put(cteSegundosVolcadoEstadisticas, "0");
                } else
                {
                    confFile.remove(cteSegundosVolcadoEstadisticas);
                    confFile.put(cteSegundosVolcadoEstadisticas, SegundosVolcadoEstadisticasAnt);
                }
        }
        AuxiliarComprobacion = 0L;
        if(NTrazador == null || NTrazador.equals(""))
        {
            if(NTrazadorAnt == null)
                confFile.put(cteNTrazador, "10");
            else
                confFile.put(cteNTrazador, NTrazadorAnt);
        } else
        {
            try
            {
                AuxiliarComprobacion = Long.parseLong(NTrazador);
            }
            catch(Exception _ex)
            {
                AuxiliarComprobacion = 0L;
            }
            if(AuxiliarComprobacion < 1L)
                if(NTrazadorAnt == null)
                {
                    confFile.remove(cteNTrazador);
                    confFile.put(cteNTrazador, "10");
                } else
                {
                    confFile.remove(cteNTrazador);
                    confFile.put(cteNTrazador, NTrazadorAnt);
                }
        }
        AuxiliarComprobacion = 0L;
        if(IncDecTrazador == null || IncDecTrazador.equals(""))
        {
            if(IncDecTrazadorAnt == null)
                confFile.put(cteIncDecTrazador, "5");
            else
                confFile.put(cteIncDecTrazador, IncDecTrazadorAnt);
        } else
        {
            try
            {
                AuxiliarComprobacion = Long.parseLong(IncDecTrazador);
            }
            catch(Exception _ex)
            {
                AuxiliarComprobacion = 0L;
            }
            if(AuxiliarComprobacion < 1L)
                if(IncDecTrazadorAnt == null)
                {
                    confFile.remove(cteIncDecTrazador);
                    confFile.put(cteIncDecTrazador, "5");
                } else
                {
                    confFile.remove(cteIncDecTrazador);
                    confFile.put(cteIncDecTrazador, IncDecTrazadorAnt);
                }
        }
        AuxiliarComprobacion = 0L;
        if(TimeOut == null || TimeOut.equals(""))
        {
            if(TimeOutAnt == null)
                confFile.put(cteTimeOut, "1800");
            else
                confFile.put(cteTimeOut, TimeOutAnt);
        } else
        {
            try
            {
                AuxiliarComprobacion = Long.parseLong(TimeOut);
            }
            catch(Exception _ex)
            {
                AuxiliarComprobacion = 0L;
            }
            if(AuxiliarComprobacion < 1L)
                if(TimeOutAnt == null)
                {
                    confFile.remove(cteTimeOut);
                    confFile.put(cteTimeOut, "1800");
                } else
                {
                    confFile.remove(cteTimeOut);
                    confFile.put(cteTimeOut, TimeOutAnt);
                }
        }
        AuxiliarComprobacion = 0L;
        if(TiempoInactTrazador == null || TiempoInactTrazador.equals(""))
        {
            if(TiempoInactTrazadorAnt == null)
                confFile.put(cteTiempoInactTrazador, "1750");
            else
                confFile.put(cteTiempoInactTrazador, TiempoInactTrazadorAnt);
        } else
        {
            try
            {
                AuxiliarComprobacion = Long.parseLong(TiempoInactTrazador);
            }
            catch(Exception _ex)
            {
                AuxiliarComprobacion = 0L;
            }
            if(AuxiliarComprobacion < 1L)
                if(TiempoInactTrazadorAnt == null)
                {
                    confFile.remove(cteTiempoInactTrazador);
                    confFile.put(cteTiempoInactTrazador, "1800");
                } else
                {
                    confFile.remove(cteTiempoInactTrazador);
                    confFile.put(cteTiempoInactTrazador, TiempoInactTrazadorAnt);
                }
        }
        AuxiliarComprobacion = 0L;
        if(TiempoComprobacionInactTrazador == null || TiempoComprobacionInactTrazador.equals(""))
        {
            if(TiempoComprobacionInactTrazadorAnt == null)
                confFile.put(cteTiempoComprobacionInactTrazador, "1800");
            else
                confFile.put(cteTiempoComprobacionInactTrazador, TiempoComprobacionInactTrazadorAnt);
        } else
        {
            try
            {
                AuxiliarComprobacion = Long.parseLong(TiempoComprobacionInactTrazador);
            }
            catch(Exception _ex)
            {
                AuxiliarComprobacion = 0L;
            }
            if(AuxiliarComprobacion < 1L)
                if(TiempoComprobacionInactTrazadorAnt == null)
                {
                    confFile.remove(cteTiempoComprobacionInactTrazador);
                    confFile.put(cteTiempoComprobacionInactTrazador, "1800");
                } else
                {
                    confFile.remove(cteTiempoComprobacionInactTrazador);
                    confFile.put(cteTiempoComprobacionInactTrazador, TiempoComprobacionInactTrazadorAnt);
                }
        }
        int tamBuf = 0;
        if(TamanoBuffer == null || TamanoBuffer.equals(""))
        {
            if(TamanoBufferAnt == null)
                confFile.put(cteTamanoBuffer, "100");
            else
                confFile.put(cteTamanoBuffer, TamanoBufferAnt);
        } else
        {
            try
            {
                tamBuf = Integer.parseInt(TamanoBuffer);
            }
            catch(Exception _ex)
            {
                tamBuf = 0;
            }
            if(tamBuf < 1)
                if(TamanoBufferAnt == null)
                {
                    confFile.remove(cteTamanoBuffer);
                    confFile.put(cteTamanoBuffer, "100");
                } else
                {
                    confFile.remove(cteTamanoBuffer);
                    confFile.put(cteTamanoBuffer, TamanoBufferAnt);
                }
        }
        int thread = 0;
        if(pThread == null || pThread.equals(""))
        {
            confFile.put(cteThread, "0");
        } else
        {
            try
            {
                thread = Integer.parseInt(pThread);
            }
            catch(Exception _ex)
            {
                thread = 0;
            }
            if(thread > 1)
            {
                confFile.remove(cteThread);
                confFile.put(cteThread, "0");
            }
        }
        if(getProceso() != null && getProceso().equals("batch"))
        {
            if(Salida.equals("0"))
            {
                if(pathArchivoLog == null || pathArchivoLog.equals(""))
                {
                    Exception ex = new Exception("Es necesario incluir el parametro pathArchivoLog en el fichero " + nombreFicheroConfiguracion);
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el parametro pathArchivoLog en el fichero " + nombreFicheroConfiguracion));
                    throw ex;
                }
                File path = new File(pathArchivoLog);
                if(!path.exists())
                {
                    Exception ex = new Exception("El valor " + pathArchivoLog + " del parametro pathArchivoLog es incorrecto o no existe el directorio");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "El valor " + pathArchivoLog + " del parametro pathArchivoLog es incorrecto o no existe el directorio"));
                    throw ex;
                }
            }
        } else
        {
            if(pathArchivoLog == null || pathArchivoLog.equals(""))
            {
                Exception ex = new Exception("Es necesario incluir el parametro pathArchivoLog en el fichero " + nombreFicheroConfiguracion);
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Es necesario incluir el parametro pathArchivoLog en el fichero " + nombreFicheroConfiguracion));
                throw ex;
            }
            File path = new File(pathArchivoLog);
            if(!path.exists())
            {
                Exception ex = new Exception("El valor " + pathArchivoLog + " del parametro pathArchivoLog es incorrecto o no existe el directorio");
                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "El valor " + pathArchivoLog + " del parametro pathArchivoLog es incorrecto o no existe el directorio"));
                throw ex;
            }
        }
        AuxiliarComprobacion = 0L;
        if(niveltraza == null || niveltraza.equals(""))
        {
            if(niveltrazaAnt != null && !niveltrazaAnt.equals(""))
                confFile.put(cteNiveltraza, niveltrazaAnt);
            else
                confFile.put(cteNiveltraza, "1");
        } else
        {
            try
            {
                AuxiliarComprobacion = Integer.parseInt(niveltraza);
            }
            catch(Exception _ex)
            {
                AuxiliarComprobacion = 0L;
            }
            if(AuxiliarComprobacion < 1L || AuxiliarComprobacion > 4L)
                if(niveltrazaAnt != null && !niveltrazaAnt.equals(""))
                {
                    confFile.remove(cteNiveltraza);
                    confFile.put(cteNiveltraza, niveltrazaAnt);
                } else
                {
                    confFile.remove(cteNiveltraza);
                    confFile.put(cteNiveltraza, "1");
                }
        }
        int tipoRota = 0;
        if(tipoRotacionAnt != null && !tipoRotacionAnt.equals("") && tipoRotacion != null && !tipoRotacionAnt.equals(tipoRotacion))
            tipoRotacion = tipoRotacionAnt;
        if(tipoRotacion == null || tipoRotacion.equals(""))
        {
            if(tipoRotacionAnt != null && !tipoRotacionAnt.equals(""))
                confFile.put(cteTipoRotacion, tipoRotacionAnt);
            else
                confFile.put(cteTipoRotacion, "0");
        } else
        {
            try
            {
                tipoRota = Integer.parseInt(tipoRotacion);
            }
            catch(Exception _ex)
            {
                tipoRota = 3;
            }
        }
        if(tipoRota > 2)
            if(tipoRotacionAnt != null && !tipoRotacionAnt.equals(""))
            {
                confFile.remove(cteTipoRotacion);
                confFile.put(cteTipoRotacion, tipoRotacionAnt);
                AuxiliarComprobacion = Long.parseLong(tipoRotacionAnt);
            } else
            {
                confFile.remove(cteTipoRotacion);
                confFile.put(cteTipoRotacion, "0");
            }
        int segundos = 0;
        AuxiliarComprobacion = 0L;
        if(segundosRotacion == null || segundosRotacion.equals(""))
        {
            if(segundosRotacionAnt != null && !segundosRotacionAnt.equals(""))
                confFile.put(cteSegundosRotacion, segundosRotacionAnt);
            else
                confFile.put(cteSegundosRotacion, "86400");
        } else
        {
            try
            {
                segundos = Integer.parseInt(segundosRotacion);
            }
            catch(Exception _ex)
            {
                segundos = 0;
            }
            if(segundos < 1)
                if(segundosRotacionAnt != null && !segundosRotacionAnt.equals(""))
                {
                    confFile.remove(cteSegundosRotacion);
                    confFile.put(cteSegundosRotacion, segundosRotacionAnt);
                } else
                {
                    confFile.remove(cteSegundosRotacion);
                    confFile.put(cteSegundosRotacion, "86400");
                }
        }
        int tamano = 0;
        if(tamanoMax == null || tamanoMax.equals(""))
        {
            if(tamanoMaxAnt != null && !tamanoMaxAnt.equals(""))
                confFile.put(cteTamanoMax, tamanoMaxAnt);
            else
                confFile.put(cteTamanoMax, "10485760");
        } else
        {
            try
            {
                tamano = Integer.parseInt(tamanoMax);
            }
            catch(Exception _ex)
            {
                tamano = 0;
            }
            if(tamano < 1)
                if(tamanoMaxAnt != null && !tamanoMaxAnt.equals(""))
                {
                    confFile.remove(cteTamanoMax);
                    confFile.put(cteTamanoMax, tamanoMaxAnt);
                } else
                {
                    confFile.remove(cteTamanoMax);
                    confFile.put(cteTamanoMax, "10485760");
                }
        }
        int numFich = 0;
        if(numMaxFich == null || numMaxFich.equals(""))
        {
            if(numMaxFichAnt != null && !numMaxFichAnt.equals(""))
                confFile.put(cteNumMaxFich, numMaxFichAnt);
            else
                confFile.put(cteNumMaxFich, "5");
        } else
        {
            try
            {
                numFich = Integer.parseInt(numMaxFich);
            }
            catch(Exception _ex)
            {
                numFich = 0;
            }
            if(numFich < 1)
                if(numMaxFichAnt != null && !numMaxFichAnt.equals(""))
                {
                    confFile.remove(cteNumMaxFich);
                    confFile.put(cteNumMaxFich, numMaxFichAnt);
                } else
                {
                    confFile.remove(cteNumMaxFich);
                    confFile.put(cteNumMaxFich, "5");
                }
        }
    }

    public static String formatearMensaje(String codigoMensaje, String entorno, String modulo, String origen, String textoMensaje)
    {
        StringBuffer trazaFinal = new StringBuffer();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HHmmss");
        String fecha = formatter.format(new Date());
        trazaFinal.append(fecha);
        trazaFinal.append(' ');
        trazaFinal.append(codigoMensaje);
        trazaFinal.append(' ');
        if(entorno != null)
        {
            trazaFinal.append(entorno);
            trazaFinal.append(' ');
        }
        if(modulo != null)
        {
            trazaFinal.append(modulo);
            trazaFinal.append(' ');
        }
        if(origen != null)
        {
            trazaFinal.append(origen);
            trazaFinal.append(' ');
        }
        trazaFinal.append(textoMensaje);
        trazaFinal.setLength(512);
        return trazaFinal.toString().trim();
    }

    public static Properties getConfFile(String nombreFichConfig)
        throws Exception
    {
        logj_lj_Configuracion conf = null;
        try
        {
            conf = instance(nombreFichConfig);
            if((new Date()).getTime() - conf.tiempoLectura >= conf.timeOutLectura)
            {
                conf.leerConfiguracion(conf.nombreFicheroConfiguracion);
                configuracion.remove(nombreFichConfig);
                configuracion.put(nombreFichConfig, conf);
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        return conf.confFile;
    }

    public static Properties getConfFile(String nombreFichConfig, String nombreProceso)
        throws Exception
    {
        logj_lj_Configuracion conf = null;
        try
        {
            conf = instance(nombreFichConfig, nombreProceso);
            if((new Date()).getTime() - conf.tiempoLectura >= conf.timeOutLectura)
            {
                conf.leerConfiguracion(conf.nombreFicheroConfiguracion, nombreProceso);
                configuracion.remove(nombreFichConfig);
                configuracion.put(nombreFichConfig, conf);
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        return conf.confFile;
    }

    static String getProceso()
    {
        return proceso;
    }

    public static String getVersion()
    {
        return version;
    }

    public static boolean hayQrefrescar(String nombreFichConfig)
        throws Exception
    {
        logj_lj_Configuracion prop = null;
        try
        {
            prop = instance(nombreFichConfig);
            long segConfigValida = Long.parseLong(prop.confFile.getProperty(cteSegundosConfiguracionValida));
            
            long TiempoUltimaLectura = Long.parseLong(prop.confFile.getProperty(tiempoUltimaLectura));
            
            
            if((new Date()).getTime() - TiempoUltimaLectura >= segConfigValida * 1000L)
                return true;
        }
        catch(Exception e)
        {
            throw e;
        }
        return false;
    }

    protected static logj_lj_Configuracion instance(String nombreFichConfig)
        throws Exception
    {
        if(configuracion.get(nombreFichConfig) == null)
            try
            {
                logj_lj_Configuracion config = new logj_lj_Configuracion(nombreFichConfig);
                configuracion.put(nombreFichConfig, config);
            }
            catch(Exception e)
            {
                throw e;
            }
        return (logj_lj_Configuracion)configuracion.get(nombreFichConfig);
    }

    protected static logj_lj_Configuracion instance(String nombreFichConfig, String nombreProceso)
        throws Exception
    {
        if(configuracion.get(nombreFichConfig) == null)
            try
            {
                logj_lj_Configuracion config = new logj_lj_Configuracion(nombreFichConfig, nombreProceso);
                configuracion.put(nombreFichConfig, config);
            }
            catch(Exception e)
            {
                throw e;
            }
        return (logj_lj_Configuracion)configuracion.get(nombreFichConfig);
    }

    protected synchronized void leerConfiguracion(String nombreFicheroConfiguracion)
        throws Exception
    {
        try
        {
            String tipoRota = "";
            try
            {
                tipoRota = (String)((logj_lj_Configuracion)configuracion.get(nombreFicheroConfiguracion)).confFile.get(cteTipoRotacion);
            }
            catch(Exception _ex)
            {
                tipoRota = "";
            }
            
            logj_lj_Configuracion confAnterior = null;
           
            Properties pConfigAnt = null;
            
            
            try
            {
            	//Ver porque no carga config anterior
            	
                confAnterior = (logj_lj_Configuracion)configuracion.get(nombreFicheroConfiguracion);
                
                
                pConfigAnt = new Properties();
                pConfigAnt.put(cteNiveltraza, confAnterior.confFile.getProperty(cteNiveltraza));
                try
                {
                    pConfigAnt.put(cteSegundosRotacion, confAnterior.confFile.getProperty(cteSegundosRotacion));
                }
                catch(Exception _ex) { }
                try
                {
                    pConfigAnt.put(cteTamanoMax, confAnterior.confFile.getProperty(cteTamanoMax));
                    pConfigAnt.put(cteNumMaxFich, confAnterior.confFile.getProperty(cteNumMaxFich));
                }
                catch(Exception _ex) 
                
                
                { 
                pConfigAnt.put(cteTipoRotacion, confAnterior.confFile.getProperty(cteTipoRotacion));
                pConfigAnt.put(cteIDTraza, confAnterior.confFile.getProperty(cteIDTraza));
                pConfigAnt.put(cteIDFichTraza, confAnterior.confFile.getProperty(cteIDFichTraza));
                pConfigAnt.put(cteIDNivelTraza, confAnterior.confFile.getProperty(cteIDNivelTraza));
                pConfigAnt.put(cteSalida, confAnterior.confFile.getProperty(cteSalida));
                pConfigAnt.put(cteEstadistica, confAnterior.confFile.getProperty(cteEstadistica));
                pConfigAnt.put(cteSegundosVolcadoEstadisticas, confAnterior.confFile.getProperty(cteSegundosVolcadoEstadisticas));
                pConfigAnt.put(cteNTrazador, confAnterior.confFile.getProperty(cteNTrazador));
                pConfigAnt.put(cteIncDecTrazador, confAnterior.confFile.getProperty(cteIncDecTrazador));
                pConfigAnt.put(cteTimeOut, confAnterior.confFile.getProperty(cteTimeOut));
                pConfigAnt.put(cteTiempoInactTrazador, confAnterior.confFile.getProperty(cteTiempoInactTrazador));
                pConfigAnt.put(cteTiempoComprobacionInactTrazador, confAnterior.confFile.getProperty(cteTiempoComprobacionInactTrazador));
                pConfigAnt.put(cteTamanoBuffer, confAnterior.confFile.getProperty(cteTamanoBuffer));
                pConfigAnt.put(cteSegundosConfiguracionValida, confAnterior.confFile.getProperty(cteSegundosConfiguracionValida));
                pConfigAnt.put(cteVM, confAnterior.confFile.getProperty(cteVM));
                pConfigAnt.put(cteNombreSH, confAnterior.confFile.getProperty(cteNombreSH));
                pConfigAnt.put(ctePathArchivoLog, confAnterior.confFile.getProperty(ctePathArchivoLog));
                }
           }
            catch(Exception _ex)
            {
                confAnterior = null;
            }
            
            //cargo datos del archivo en una variable propertiees
            confFile = new Properties();
            File inputFile = new File(nombreFicheroConfiguracion);
            FileReader in = new FileReader(inputFile);
            BufferedReader fichero;
            for(fichero = new BufferedReader(in); fichero.ready();)
            {
                String linea = fichero.readLine();
                if(linea == null)
                    linea = "";
                linea = linea.trim();
                if(!linea.equals("") && linea.indexOf("#") != 0)
                {
                    int PosicionIgual = 0;
                    String Propiedad = "";
                    String ValorProp = "";
                    PosicionIgual = linea.indexOf("=");
                    if(PosicionIgual > 0)
                    {
                        Propiedad = linea.substring(0, PosicionIgual);
                        ValorProp = linea.substring(PosicionIgual + 1, linea.length());
                        confFile.put(Propiedad.trim().toUpperCase(), ValorProp.trim());
                    }
                }
            }

            fichero.close();
            in.close();
            
            
            
            if(tipoRota != null && !tipoRota.equals(""))
            {
                confFile.remove(cteTipoRotacion);
                confFile.put(cteTipoRotacion, tipoRota);
            }
            if(pConfigAnt.get(ctePathArchivoLog) != null && !pConfigAnt.get(ctePathArchivoLog).equals(""))
            {
                confFile.remove(ctePathArchivoLog);
                confFile.put(ctePathArchivoLog, pConfigAnt.get(ctePathArchivoLog));
            }
            if(pConfigAnt.get(cteEstadistica) != null && !pConfigAnt.get(cteEstadistica).equals(""))
            {
                confFile.remove(cteEstadistica);
                confFile.put(cteEstadistica, pConfigAnt.get(cteEstadistica));
            }
            if(pConfigAnt.get(cteThread) != null && !pConfigAnt.get(cteThread).equals(""))
            {
                confFile.remove(cteThread);
                confFile.put(cteThread, pConfigAnt.get(cteThread));
            }
            if(pConfigAnt.get(cteSalida) != null && !pConfigAnt.get(cteSalida).equals(""))
            {
                confFile.remove(cteSalida);
                confFile.put(cteSalida, pConfigAnt.get(cteSalida));
            }
            if(pConfigAnt.get(cteNombreSH) != null && !pConfigAnt.get(cteNombreSH).equals(""))
            {
                confFile.remove(cteNombreSH);
                confFile.put(cteNombreSH, pConfigAnt.get(cteNombreSH));
            }
            if(getProceso() == null || getProceso().equals(""))
                setProceso("web");
            comprobarNomenclatura(confFile);
            cargarConstantesPorVersion();
            
            //* julian manfredi
            comprobarParametros(pConfigAnt);
            
            
            confFile.put("MAY", "SI");
            String timeOutStr = confFile.getProperty(cteSegundosConfiguracionValida);
            if(timeOutStr != null)
                timeOutLectura = Integer.parseInt(timeOutStr) * 1000;
            else
                timeOutLectura = 30000L;
            tiempoLectura = (new Date()).getTime();
            confFile.put(tiempoUltimaLectura, String.valueOf(tiempoLectura));
        }
        catch(MissingResourceException f)
        {
            throw f;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    

    protected synchronized void leerConfiguracion(String nombreFicheroConfiguracion, String nombreProceso)
        throws Exception
    {
        try
        {
            nombreProceso = nombreProceso + "_SYS";
            String tipoRota = "";
            try
            {
                tipoRota = (String)((logj_lj_Configuracion)configuracion.get(nombreFicheroConfiguracion)).confFile.get(cteTipoRotacion);
            }
            catch(Exception _ex)
            {
                tipoRota = "";
            }
            logj_lj_Configuracion confAnterior = null;
            Properties pConfigAnt = null;
            try
            {
                confAnterior = (logj_lj_Configuracion)configuracion.get(nombreFicheroConfiguracion);
                pConfigAnt = new Properties();
                pConfigAnt.put(cteNiveltraza, confAnterior.confFile.getProperty(cteNiveltraza));
                try
                {
                    pConfigAnt.put(cteSegundosRotacion, confAnterior.confFile.getProperty(cteSegundosRotacion));
                }
                catch(Exception _ex) { }
                try
                {
                    pConfigAnt.put(cteTamanoMax, confAnterior.confFile.getProperty(cteTamanoMax));
                    pConfigAnt.put(cteNumMaxFich, confAnterior.confFile.getProperty(cteNumMaxFich));
                }
                catch(Exception _ex) { }
                pConfigAnt.put(cteTipoRotacion, confAnterior.confFile.getProperty(cteTipoRotacion));
                pConfigAnt.put(cteIDTraza, confAnterior.confFile.getProperty(cteIDTraza));
                pConfigAnt.put(cteIDFichTraza, confAnterior.confFile.getProperty(cteIDFichTraza));
                pConfigAnt.put(cteIDNivelTraza, confAnterior.confFile.getProperty(cteIDNivelTraza));
                pConfigAnt.put(cteSalida, confAnterior.confFile.getProperty(cteSalida));
                pConfigAnt.put(cteEstadistica, confAnterior.confFile.getProperty(cteEstadistica));
                pConfigAnt.put(cteSegundosVolcadoEstadisticas, confAnterior.confFile.getProperty(cteSegundosVolcadoEstadisticas));
                pConfigAnt.put(cteNTrazador, confAnterior.confFile.getProperty(cteNTrazador));
                pConfigAnt.put(cteIncDecTrazador, confAnterior.confFile.getProperty(cteIncDecTrazador));
                pConfigAnt.put(cteTimeOut, confAnterior.confFile.getProperty(cteTimeOut));
                pConfigAnt.put(cteTiempoInactTrazador, confAnterior.confFile.getProperty(cteTiempoInactTrazador));
                pConfigAnt.put(cteTiempoComprobacionInactTrazador, confAnterior.confFile.getProperty(cteTiempoComprobacionInactTrazador));
                pConfigAnt.put(cteTamanoBuffer, confAnterior.confFile.getProperty(cteTamanoBuffer));
            }
            catch(Exception _ex)
            {
                confAnterior = null;
            }
            confFile = new Properties();
            File inputFile = new File(nombreFicheroConfiguracion);
            FileReader in = new FileReader(inputFile);
            BufferedReader fichero = new BufferedReader(in);
            String nombreSeccion = "";
            boolean Encuentra_AlgunaSeccion = false;
            boolean global_sys_flagh = false;
            boolean SeccionABuscar_sys_flagh = false;
            int sw_Carga_Parametros = 0;
            Hashtable AuxParametrosSeccion = new Hashtable();
            while(fichero.ready()) 
            {
                String linea = fichero.readLine();
                if(linea == null)
                    linea = "";
                linea = linea.trim();
                if(!linea.equals("") && linea.indexOf("#") != 0)
                    if(linea.indexOf("[") == 0)
                    {
                        Encuentra_AlgunaSeccion = true;
                        sw_Carga_Parametros = 0;
                        if(AuxParametrosSeccion != null)
                            AuxParametrosSeccion.clear();
                        nombreSeccion = linea.substring(1);
                        nombreSeccion = nombreSeccion.substring(0, nombreSeccion.length() - 1);
                        if(nombreSeccion.toUpperCase().equals("GLOBAL_SYS"))
                        {
                            if(!global_sys_flagh)
                            {
                                global_sys_flagh = true;
                                sw_Carga_Parametros = 1;
                            } else
                            {
                                Exception ex = new Exception("Error en el fichero de configuraci\363n " + nombreFicheroConfiguracion + " se ha encontrado la secci\363n global_sys repetida , siendo " + nombreFicheroConfiguracion + " el nombre del fichero de configuraci\363n");
                                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Error en el fichero de configuraci\363n " + nombreFicheroConfiguracion + " se ha encontrado la secci\363n global_sys repetida , siendo " + nombreFicheroConfiguracion + " el nombre del fichero de configuraci\363n"));
                                fichero.close();
                                in.close();
                                throw ex;
                            }
                        } else
                        if(nombreSeccion.toUpperCase().equals(nombreProceso.toUpperCase().trim()))
                        {
                            if(SeccionABuscar_sys_flagh)
                            {
                                Exception ex = new Exception("Error en el fichero de configuraci\363n " + nombreFicheroConfiguracion + " se ha encontrado la secci\363n " + nombreSeccion + " repetida");
                                System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Error en el fichero de configuraci\363n " + nombreFicheroConfiguracion + " se ha encontrado la secci\363n " + nombreSeccion + " repetida"));
                                fichero.close();
                                in.close();
                                throw ex;
                            }
                            SeccionABuscar_sys_flagh = true;
                            sw_Carga_Parametros = 3;
                        }
                    } else
                    if(sw_Carga_Parametros == 1 || sw_Carga_Parametros == 3)
                    {
                        int PosicionIgual = 0;
                        String Propiedad = "";
                        String ValorProp = "";
                        String ValidaRepetido = null;
                        PosicionIgual = linea.indexOf("=");
                        if(PosicionIgual > 0)
                        {
                            Propiedad = linea.substring(0, PosicionIgual);
                            ValorProp = linea.substring(PosicionIgual + 1, linea.length());
                            AuxParametrosSeccion.put(Propiedad.trim().toUpperCase(), ValorProp.trim());
                            ValidaRepetido = confFile.getProperty(Propiedad.trim().toUpperCase());
                            if(ValidaRepetido == null || sw_Carga_Parametros == 3)
                                confFile.put(Propiedad.trim().toUpperCase(), ValorProp.trim());
                        }
                    }
            }
            fichero.close();
            in.close();
            if(Encuentra_AlgunaSeccion)
            {
                if(tipoRota != null && !tipoRota.equals(""))
                {
                    confFile.remove(cteTipoRotacion);
                    confFile.put(cteTipoRotacion, tipoRota);
                }
                if(pConfigAnt.get(ctePathArchivoLog) != null && !pConfigAnt.get(ctePathArchivoLog).equals(""))
                {
                    confFile.remove(ctePathArchivoLog);
                    confFile.put(ctePathArchivoLog, pConfigAnt.get(ctePathArchivoLog));
                }
                if(pConfigAnt.get(cteEstadistica) != null && !pConfigAnt.get(cteEstadistica).equals(""))
                {
                    confFile.remove(cteEstadistica);
                    confFile.put(cteEstadistica, pConfigAnt.get(cteEstadistica));
                }
                if(pConfigAnt.get(cteThread) != null && !pConfigAnt.get(cteThread).equals(""))
                {
                    confFile.remove(cteThread);
                    confFile.put(cteThread, pConfigAnt.get(cteThread));
                }
                if(pConfigAnt.get(cteSalida) != null && !pConfigAnt.get(cteSalida).equals(""))
                {
                    confFile.remove(cteSalida);
                    confFile.put(cteSalida, pConfigAnt.get(cteSalida));
                }
                if(pConfigAnt.get(cteNombreSH) != null && !pConfigAnt.get(cteNombreSH).equals(""))
                {
                    confFile.remove(cteNombreSH);
                    confFile.put(cteNombreSH, pConfigAnt.get(cteNombreSH));
                }
                setProceso("batch");
                comprobarNomenclaturaSecciones(confFile);
                cargarConstantesPorVersion();
                comprobarParametros(pConfigAnt);
                if(!getVersion().equals("2.0"))
                {
                    Exception ex = new Exception("Los par\341metros deben estar en formato versi\363n 1.2.0");
                    System.out.println(formatearMensaje("PRO-Error   ", "", "", "", "Los par\341metros deben estar en formato versi\363n 1.2.0"));
                    throw ex;
                }
                confFile.put("MAY", "SI");
                String timeOutStr = confFile.getProperty(cteSegundosConfiguracionValida);
                if(timeOutStr != null)
                    timeOutLectura = Integer.parseInt(timeOutStr) * 1000;
                else
                    timeOutLectura = 30000L;
                tiempoLectura = (new Date()).getTime();
                confFile.put(tiempoUltimaLectura, String.valueOf(tiempoLectura));
            } else
            {
                setProceso("batch");
                leerConfiguracion(nombreFicheroConfiguracion);
            }
        }
        catch(MissingResourceException f)
        {
            throw f;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
    }

    void setProceso(String proceso)
    {
        proceso = proceso;
    }

    void setVersion(String version)
    {
        version = version;
    }


    
}

 
