/*
 *  Copyright 2004 Clinton Begin
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.ibatis.sqlmap.engine.mapping.statement;

import com.ibatis.common.jdbc.exception.NestedSQLException;
import com.ibatis.common.io.ReaderInputStream;

import com.ibatis.sqlmap.client.event.RowHandler;
import com.ibatis.sqlmap.engine.execution.SqlExecutor;
import com.ibatis.sqlmap.engine.mapping.parameter.ParameterMap;
import com.ibatis.sqlmap.engine.mapping.result.BasicResultMapping;
import com.ibatis.sqlmap.engine.mapping.result.ResultMap;
import com.ibatis.sqlmap.engine.mapping.result.ResultMapping;
import com.ibatis.sqlmap.engine.mapping.sql.Sql;
import com.ibatis.sqlmap.engine.scope.ErrorContext;
import com.ibatis.sqlmap.engine.scope.RequestScope;
import com.ibatis.sqlmap.engine.transaction.Transaction;
import com.ibatis.sqlmap.engine.transaction.TransactionException;
import com.ibatis.sqlmap.engine.type.*;
import org.w3c.dom.Document;

import javax.xml.parsers.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.io.*;

public class GeneralStatement extends BaseStatement {

	interface CONSTANTS {
		public static String SQLSTRING = "sqlString";
		public static String SKIPRESULTS = "skipResults";
		public static String SORTCRITERION = "sortCriterion";
		public static String SORTDIRECTION = "sortDirection";

		interface SQLSORT {
			public static String ASC = "ASC";
			public static String DESC = "DESC";
		}

		interface CONSULTAS {
			public static String OBTENER = "obtener";
			public static String LUPAS = "lupa";
			public static int MAXRESULTS = -999999;
		}

		interface PAGINGCRITERIA {
		     public static String SQLPART1 = "SELECT * FROM (SELECT /*+ FIRST_ROWS(n) */ A.*, ROWNUM rnum FROM (";
			 public static String SQLPART2 = ") A where ROWNUM <= ";
			 public static String SQLPART3 = ") WHERE rnum  > ";
			 
			 public static String SQLPART1SQLSERVER = "SELECT * FROM ( SELECT TOP ";
			 public static String SQLPART2SQLSERVER = " A.*, ROW_NUMBER() OVER( order by ";
			 public static String SQLPART3SQLSERVER = " ) AS rnum FROM ( ";
			 public static String SQLPART4SQLSERVER = " ) AS A ) AS C  WHERE C.rnum > ";
		}

		interface SORTCRITERIA {
			public static String SQLPART1 = "SELECT * FROM (";
			public static String SQLPART2 = ") ORDER BY ";
		}
	}

	/**
	 * 
	 */
	public StatementType getStatementType() {
		return StatementType.UNKNOWN;
	}

	/**
	 * 
	 */
	public int executeUpdate(RequestScope request, Transaction trans, Object parameterObject) throws SQLException {
		ErrorContext errorContext = request.getErrorContext();
		errorContext.setActivity("preparing the mapped statement for execution");
		errorContext.setObjectId(this.getId());
		errorContext.setResource(this.getResource());

		request.getSession().setCommitRequired(true);

		try {
			parameterObject = validateParameter(parameterObject);

			Sql sql = getSql();

			errorContext.setMoreInfo("Check the parameter map.");
			ParameterMap parameterMap = sql.getParameterMap(request, parameterObject);

			errorContext.setMoreInfo("Check the result map.");
			ResultMap resultMap = sql.getResultMap(request, parameterObject);

			request.setResultMap(resultMap);
			request.setParameterMap(parameterMap);

			int rows = 0;

			errorContext.setMoreInfo("Check the parameter map.");
			Object[] parameters = parameterMap.getParameterObjectValues(request, parameterObject);

			errorContext.setMoreInfo("Check the SQL statement.");
			String sqlString = sql.getSql(request, parameterObject);

			errorContext.setActivity("executing mapped statement");
			errorContext.setMoreInfo("Check the statement or the result map.");
			rows = sqlExecuteUpdate(request, trans.getConnection(), sqlString, parameters);

			errorContext.setMoreInfo("Check the output parameters.");
			if (parameterObject != null) {
				postProcessParameterObject(request, parameterObject, parameters);
			}

			errorContext.reset();
			sql.cleanup(request);
			notifyListeners();
			return rows;
		} catch (SQLException e) {
			errorContext.setCause(e);
			throw new NestedSQLException(errorContext.toString(), e.getSQLState(), e.getErrorCode(), e);
		} catch (Exception e) {
			errorContext.setCause(e);
			throw new NestedSQLException(errorContext.toString(), e);
		}
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Object executeQueryForObject(RequestScope request, Transaction trans, Object parameterObject,
			Object resultObject) throws SQLException {
		try {
			Object object = null;

			DefaultRowHandler rowHandler = new DefaultRowHandler();
			executeQueryWithCallback(request, trans.getConnection(), parameterObject, resultObject, rowHandler,
					SqlExecutor.NO_SKIPPED_RESULTS, SqlExecutor.NO_MAXIMUM_RESULTS);
			List list = rowHandler.getList();

			if (list.size() > 1) {
				throw new SQLException("Error: executeQueryForObject returned too many results.");
			} else if (list.size() > 0) {
				object = list.get(0);
			}

			return object;
		} catch (TransactionException e) {
			throw new NestedSQLException("Error getting Connection from Transaction.  Cause: " + e, e);
		}
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List executeQueryForList(RequestScope request, Transaction trans, Object parameterObject, int skipResults,
			int maxResults) throws SQLException {
		try {
			DefaultRowHandler rowHandler = new DefaultRowHandler();
			executeQueryWithCallback(request, trans.getConnection(), parameterObject, null, rowHandler, skipResults,
					maxResults);
			return rowHandler.getList();
		} catch (TransactionException e) {
			throw new NestedSQLException("Error getting Connection from Transaction.  Cause: " + e, e);
		}
	}

	/**
	 * 
	 */
	public void executeQueryWithRowHandler(RequestScope request, Transaction trans, Object parameterObject,
			RowHandler rowHandler) throws SQLException {
		try {
			executeQueryWithCallback(request, trans.getConnection(), parameterObject, null, rowHandler,
					SqlExecutor.NO_SKIPPED_RESULTS, SqlExecutor.NO_MAXIMUM_RESULTS);
		} catch (TransactionException e) {
			throw new NestedSQLException("Error getting Connection from Transaction.  Cause: " + e, e);
		}
	}

	/**
	 * 
	 * @param request
	 * @param conn
	 * @param parameterObject
	 * @param resultObject
	 * @param rowHandler
	 * @param skipResults
	 * @param maxResults
	 * @throws SQLException
	 */
	protected void executeQueryWithCallback(RequestScope request, Connection conn, Object parameterObject,
		Object resultObject, RowHandler rowHandler, int skipResults, int maxResults) throws SQLException {
		ErrorContext errorContext = request.getErrorContext();
		errorContext.setActivity("preparing the mapped statement for execution");
		errorContext.setObjectId(this.getId());
		errorContext.setResource(this.getResource());

		try {
			parameterObject = validateParameter(parameterObject);

			Sql sql = getSql();

			errorContext.setMoreInfo("Check the parameter map.");
			ParameterMap parameterMap = sql.getParameterMap(request, parameterObject);

			errorContext.setMoreInfo("Check the result map.");
			ResultMap resultMap = sql.getResultMap(request, parameterObject);

			request.setResultMap(resultMap);
			request.setParameterMap(parameterMap);

			errorContext.setMoreInfo("Check the parameter map.");
			Object[] parameters = parameterMap.getParameterObjectValues(request, parameterObject);

			errorContext.setMoreInfo("Check the SQL statement.");
			String sqlString = sql.getSql(request, parameterObject).replace(';', ' ');

			sqlString = new String(sqlString.replace('	', ' '));
			for(int i=0;i<10;i++){
				sqlString = new String(sqlString.replaceAll("  ", " "));
			}
			
			String sqlStringSorted = addSortCriteria(parameterObject, resultMap, sqlString, errorContext);

			errorContext.setActivity("executing mapped statement");
			errorContext.setMoreInfo("Check the SQL statement or the result map.");
			RowHandlerCallback callback = new RowHandlerCallback(resultMap, resultObject, rowHandler);

			HashMap<String, String> sqlStringAndResult = addPagingCriteria(skipResults, maxResults, sqlString);
			HashMap<String, String> sqlStringSortedAndResult = addPagingCriteria(skipResults, maxResults,sqlStringSorted);

			sqlExecuteSortedAndPagingQuery(request, conn, maxResults, parameters, (sqlStringSorted != null), callback,
					sqlStringAndResult, sqlStringSortedAndResult);

			errorContext.setMoreInfo("Check the output parameters.");
			if (parameterObject != null) {
				postProcessParameterObject(request, parameterObject, parameters);
			}

			errorContext.reset();
			sql.cleanup(request);
			notifyListeners();
		} catch (SQLException e) {
			errorContext.setCause(e);
			throw new NestedSQLException(errorContext.toString(), e.getSQLState(), e.getErrorCode(), e);
		} catch (Exception e) {
			errorContext.setCause(e);
			throw new NestedSQLException(errorContext.toString(), e);
		}
	}

	/**
	 * 
	 * @param request
	 * @param conn
	 * @param maxResults
	 * @param parameters
	 * @param sortedQuery
	 * @param callback
	 * @param sqlStringAndResult
	 * @param sqlStringSortedAndResult
	 * @throws SQLException
	 */
	private void sqlExecuteSortedAndPagingQuery(RequestScope request, Connection conn, int maxResults,
			Object[] parameters, boolean sortedQuery, RowHandlerCallback callback,
			HashMap<String, String> sqlStringAndResult, HashMap<String, String> sqlStringSortedAndResult)
			throws SQLException {

		HashMap<String, String> sqlString;
		if (sortedQuery) {
			sqlString = new HashMap<String, String>(sqlStringSortedAndResult);
		} else {
			sqlString = new HashMap<String, String>(sqlStringAndResult);
		}

		try {
			sqlExecuteQuery(request, conn, sqlString.get(CONSTANTS.SQLSTRING), parameters, Integer.valueOf(
					sqlString.get(CONSTANTS.SKIPRESULTS)).intValue(), maxResults, callback);
		} catch (SQLException e) {
			if (!sortedQuery)
				throw e;

			sqlExecuteSortedAndPagingQuery(request, conn, maxResults, parameters, false, callback, sqlStringAndResult,
					sqlString);
		}
	}

	/**
	 * 
	 * @param skipResults
	 * @param maxResults
	 * @param sqlString
	 * @return
	 */
	private HashMap<String, String> addPagingCriteria(int skipResults, int maxResults, String sqlString) {

		HashMap<String, String> sqlStringAndResult = new HashMap<String, String>();
		if ((this.getId().toLowerCase().startsWith(CONSTANTS.CONSULTAS.OBTENER) || this.getId().toLowerCase()
				.startsWith(CONSTANTS.CONSULTAS.LUPAS))
				&& maxResults != CONSTANTS.CONSULTAS.MAXRESULTS) {

		 
//			// sqlserver
//			HashMap<String, String> sqlStringAndResultsqlserver = sqlStringAndResultsqlserver;
//			
//			String primarykey = this.getId().replaceAll("obtener","ID").toUpperCase();
//			
//			
//			sqlStringAndResultsqlserver.put( CONSTANTS.SQLSTRING , 
//			new String(CONSTANTS.PAGINGCRITERIA.SQLPART1SQLSERVER + new Integer(maxResults + skipResults)  
//					+ CONSTANTS.PAGINGCRITERIA.SQLPART2SQLSERVER + primarykey 
//			        + CONSTANTS.PAGINGCRITERIA.SQLPART3SQLSERVER + sqlString  
//			        + CONSTANTS.PAGINGCRITERIA.SQLPART4SQLSERVER + skipResults + ""));
//			// fin sql server 
			
//			sqlStringAndResult.put(CONSTANTS.SQLSTRING, 
//					new String(CONSTANTS.PAGINGCRITERIA.SQLPART1 + sqlString
//					+ CONSTANTS.PAGINGCRITERIA.SQLPART2 + new Integer(maxResults + skipResults)
//					+ CONSTANTS.PAGINGCRITERIA.SQLPART3 + skipResults + ""));

			sqlStringAndResult.put(CONSTANTS.SQLSTRING, sqlString);
			sqlStringAndResult.put(CONSTANTS.SKIPRESULTS, String.valueOf(skipResults));
		
			sqlStringAndResult.put(CONSTANTS.SKIPRESULTS, "0");
		} else {
			sqlStringAndResult.put(CONSTANTS.SQLSTRING, sqlString);
			sqlStringAndResult.put(CONSTANTS.SKIPRESULTS, String.valueOf(skipResults));
		}
		return sqlStringAndResult;
	}

	/**
	 * 
	 * @param parameterObject
	 * @param resultMap
	 * @param sqlString
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private String addSortCriteria(Object pObject, ResultMap resultMap, String sqlString, ErrorContext errorContext) {

		errorContext.setActivity("adding sort criteria");
		try {
			if ((!(this.getId().toLowerCase().startsWith(CONSTANTS.CONSULTAS.OBTENER) || this.getId().toLowerCase()
					.startsWith(CONSTANTS.CONSULTAS.LUPAS)) || pObject == null || !(pObject instanceof HashMap))) {
				return null;
			}
				
			
			/* Obteniendo la columna del que se ordena */
			String sortCritProp = (String) ((HashMap<String, String>) pObject).get(CONSTANTS.SORTCRITERION);
			if (sortCritProp == null)
				return null;

			String sortCritCol = null;
			for (ResultMapping resultMapping : resultMap.getResultMappings()) {

				if (resultMapping instanceof BasicResultMapping) {

					BasicResultMapping basicResultMapping = (BasicResultMapping) resultMapping;

					if (basicResultMapping.getPropertyName().equalsIgnoreCase(sortCritProp)) {
						sortCritCol = basicResultMapping.getColumnName();
						break;
					}
					
					//para reordenar los tiempos en "Factura obtenidos en los muestreos" 
					if (basicResultMapping.getPropertyName().equalsIgnoreCase( (sortCritProp + "_TIMER_BD"))) {
						sortCritCol = basicResultMapping.getColumnName();
						break;
					}
				}
			}
			
			if (sortCritCol == null)
				return null;
			if (sortCritCol.toUpperCase().startsWith("FEC_") || sortCritCol.toUpperCase().startsWith("FEH_")) {
				String sqlString2 = new String(sqlString.replaceAll(", 'DD", ",'DD"));
				sqlString2 = new String(sqlString2.replaceAll(" ,'DD", ",'DD"));
				if (sqlString2.indexOf(sortCritCol + ",'DD/MM/YYYY HH:MI:SS'") > -1){
					sortCritCol = new String("TO_DATE(" + sortCritCol + ",'DD/MM/YYYY HH:MI:SS')");
				} else if (sqlString2.indexOf(sortCritCol + ",'DD/MM/YYYY'") > -1){
					sortCritCol = new String("TO_DATE(" + sortCritCol + ",'DD/MM/YYYY')");
				}
			}
			
			if (sqlString.toUpperCase().indexOf("'DD/MM/YYYY') "+sortCritCol)>-1) {
				sortCritCol = new String("TO_DATE(" + sortCritCol + ",'DD/MM/YYYY')");
			}
			else if (sqlString.toUpperCase().indexOf("'DD/MM/YYYYHH:MI:SS') "+sortCritCol)>-1) {
				sortCritCol = new String("TO_DATE(" + sortCritCol + ",'DD/MM/YYYY HH:MI:SS')");
			}
			
			/* Obteniendo la direccion en la que se ordena */
			String sortDirection = (String) ((HashMap<String, String>) pObject).get(CONSTANTS.SORTDIRECTION);
			if (sortDirection == null || 
				(!sortDirection.equalsIgnoreCase(CONSTANTS.SQLSORT.ASC) && 
				 !sortDirection.equalsIgnoreCase(CONSTANTS.SQLSORT.DESC))) {

				sortDirection = new String(CONSTANTS.SQLSORT.ASC);
			}

			/* Armo el nuevo SQL String */
			return new String(CONSTANTS.SORTCRITERIA.SQLPART1 + sqlString + CONSTANTS.SORTCRITERIA.SQLPART2
					+ sortCritCol + " " + sortDirection);

		} catch (Exception e) {
			errorContext.setActivity("error adding sort criteria");
			errorContext.setCause(e);
			return null;
		}
	}

	/**
	 * 
	 * @param request
	 * @param parameterObject
	 * @param parameters
	 */
	protected void postProcessParameterObject(RequestScope request, Object parameterObject, Object[] parameters) {
	}

	/**
	 * 
	 * @param request
	 * @param conn
	 * @param sqlString
	 * @param parameters
	 * @return
	 * @throws SQLException
	 */
	protected int sqlExecuteUpdate(RequestScope request, Connection conn, String sqlString, Object[] parameters)
			throws SQLException {
		if (request.getSession().isInBatch()) {
			getSqlExecutor().addBatch(request, conn, sqlString, parameters);
			return 0;
		} else {
			return getSqlExecutor().executeUpdate(request, conn, sqlString, parameters);
		}
	}

	/**
	 * 
	 * @param request
	 * @param conn
	 * @param sqlString
	 * @param parameters
	 * @param skipResults
	 * @param maxResults
	 * @param callback
	 * @throws SQLException
	 */
	protected void sqlExecuteQuery(RequestScope request, Connection conn, String sqlString, Object[] parameters,
			int skipResults, int maxResults, RowHandlerCallback callback) throws SQLException {
		getSqlExecutor().executeQuery(request, conn, sqlString, parameters, skipResults, maxResults, callback);
	}

	/**
	 * 
	 * @param param
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	protected Object validateParameter(Object param) throws SQLException {
		Object newParam = param;
		Class parameterClass = getParameterClass();
		if (newParam != null && parameterClass != null) {
			if (DomTypeMarker.class.isAssignableFrom(parameterClass)) {
				if (XmlTypeMarker.class.isAssignableFrom(parameterClass)) {
					if (!(newParam instanceof String) && !(newParam instanceof Document)) {
						throw new SQLException("Invalid parameter object type. Expected '" + String.class.getName()
								+ "' or '" + Document.class.getName() + "' but found '" + newParam.getClass().getName()
								+ "'.");
					}
					if (!(newParam instanceof Document)) {
						newParam = stringToDocument((String) newParam);
					}
				} else {
					if (!Document.class.isAssignableFrom(newParam.getClass())) {
						throw new SQLException("Invalid parameter object type. Expected '" + Document.class.getName()
								+ "' but found '" + newParam.getClass().getName() + "'.");
					}
				}
			} else {
				if (!parameterClass.isAssignableFrom(newParam.getClass())) {
					throw new SQLException("Invalid parameter object type. Expected '" + parameterClass.getName()
							+ "' but found '" + newParam.getClass().getName() + "'.");
				}
			}
		}
		return newParam;
	}

	/**
	 * 
	 * @param s
	 * @return
	 */
	private Document stringToDocument(String s) {
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			return documentBuilder.parse(new ReaderInputStream(new StringReader(s)));
		} catch (Exception e) {
			throw new RuntimeException("Error occurred. Cause: " + e, e);
		}
	}
}
