package arg.ros.acc.vo.packgenerales;

/**
* GeNERALVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class GeNERALVO {

	private Double adabas_isn;
	private String fa_clave;
	private String fa_descripcion;
	private String fa_descrip_codigos;
	private Double fa_codigo_tabla;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public GeNERALVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_clave
	* @return el valor de Fa_clave
	*/ 
	public String getFa_clave() {
		return fa_clave;
	}
    	
	/**
	* Fija el valor del atributo Fa_clave
	* @param valor el valor de Fa_clave
	*/ 
	public void setFa_clave(String valor) {
		this.fa_clave = valor;
	}
	/**
	* Recupera el valor del atributo Fa_descripcion
	* @return el valor de Fa_descripcion
	*/ 
	public String getFa_descripcion() {
		return fa_descripcion;
	}
    	
	/**
	* Fija el valor del atributo Fa_descripcion
	* @param valor el valor de Fa_descripcion
	*/ 
	public void setFa_descripcion(String valor) {
		this.fa_descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_descrip_codigos
	* @return el valor de Fa_descrip_codigos
	*/ 
	public String getFa_descrip_codigos() {
		return fa_descrip_codigos;
	}
    	
	/**
	* Fija el valor del atributo Fa_descrip_codigos
	* @param valor el valor de Fa_descrip_codigos
	*/ 
	public void setFa_descrip_codigos(String valor) {
		this.fa_descrip_codigos = valor;
	}
	/**
	* Recupera el valor del atributo Fa_codigo_tabla
	* @return el valor de Fa_codigo_tabla
	*/ 
	public Double getFa_codigo_tabla() {
		return fa_codigo_tabla;
	}
    	
	/**
	* Fija el valor del atributo Fa_codigo_tabla
	* @param valor el valor de Fa_codigo_tabla
	*/ 
	public void setFa_codigo_tabla(Double valor) {
		this.fa_codigo_tabla = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Fa_clave=["+this.getFa_clave()+"]\r\n");
				sb.append("Fa_descripcion=["+this.getFa_descripcion()+"]\r\n");
				sb.append("Fa_descrip_codigos=["+this.getFa_descrip_codigos()+"]\r\n");
				sb.append("Fa_codigo_tabla=["+this.getFa_codigo_tabla()+"]\r\n");
				return sb.toString();
	}
}