package arg.ros.acc.vo.contacto;

/**
* ContactoVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ContactoVO {

	private Double idpersonacontacto;
	private Double idpersona;
	private Integer idcontactotipo;
	private String valor;
	private String observacion;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public ContactoVO() {

	}
	
	/**
	* Recupera el valor del atributo Idpersonacontacto
	* @return el valor de Idpersonacontacto
	*/ 
	public Double getIdpersonacontacto() {
		return idpersonacontacto;
	}
    	
	/**
	* Fija el valor del atributo Idpersonacontacto
	* @param valor el valor de Idpersonacontacto
	*/ 
	public void setIdpersonacontacto(Double valor) {
		this.idpersonacontacto = valor;
	}
	/**
	* Recupera el valor del atributo Idpersona
	* @return el valor de Idpersona
	*/ 
	public Double getIdpersona() {
		return idpersona;
	}
    	
	/**
	* Fija el valor del atributo Idpersona
	* @param valor el valor de Idpersona
	*/ 
	public void setIdpersona(Double valor) {
		this.idpersona = valor;
	}
	/**
	* Recupera el valor del atributo Idcontactotipo
	* @return el valor de Idcontactotipo
	*/ 
	public Integer getIdcontactotipo() {
		return idcontactotipo;
	}
    	
	/**
	* Fija el valor del atributo Idcontactotipo
	* @param valor el valor de Idcontactotipo
	*/ 
	public void setIdcontactotipo(Integer valor) {
		this.idcontactotipo = valor;
	}
	/**
	* Recupera el valor del atributo Valor
	* @return el valor de Valor
	*/ 
	public String getValor() {
		return valor;
	}
    	
	/**
	* Fija el valor del atributo Valor
	* @param valor el valor de Valor
	*/ 
	public void setValor(String valor) {
		this.valor = valor;
	}
	/**
	* Recupera el valor del atributo Observacion
	* @return el valor de Observacion
	*/ 
	public String getObservacion() {
		return observacion;
	}
    	
	/**
	* Fija el valor del atributo Observacion
	* @param valor el valor de Observacion
	*/ 
	public void setObservacion(String valor) {
		this.observacion = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Idpersonacontacto=["+this.getIdpersonacontacto()+"]\r\n");
				sb.append("Idpersona=["+this.getIdpersona()+"]\r\n");
				sb.append("Idcontactotipo=["+this.getIdcontactotipo()+"]\r\n");
				sb.append("Valor=["+this.getValor()+"]\r\n");
				sb.append("Observacion=["+this.getObservacion()+"]\r\n");
				return sb.toString();
	}
}