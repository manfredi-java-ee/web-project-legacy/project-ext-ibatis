package arg.ros.acc.vo.contacto;

/**
* ContactotipoVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ContactotipoVO {

	private Integer idcontactotipo;
	private String descripcion;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public ContactotipoVO() {

	}
	
	/**
	* Recupera el valor del atributo Idcontactotipo
	* @return el valor de Idcontactotipo
	*/ 
	public Integer getIdcontactotipo() {
		return idcontactotipo;
	}
    	
	/**
	* Fija el valor del atributo Idcontactotipo
	* @param valor el valor de Idcontactotipo
	*/ 
	public void setIdcontactotipo(Integer valor) {
		this.idcontactotipo = valor;
	}
	/**
	* Recupera el valor del atributo Descripcion
	* @return el valor de Descripcion
	*/ 
	public String getDescripcion() {
		return descripcion;
	}
    	
	/**
	* Fija el valor del atributo Descripcion
	* @param valor el valor de Descripcion
	*/ 
	public void setDescripcion(String valor) {
		this.descripcion = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Idcontactotipo=["+this.getIdcontactotipo()+"]\r\n");
				sb.append("Descripcion=["+this.getDescripcion()+"]\r\n");
				return sb.toString();
	}
}