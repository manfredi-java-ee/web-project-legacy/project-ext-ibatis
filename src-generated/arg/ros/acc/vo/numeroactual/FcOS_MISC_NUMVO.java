package arg.ros.acc.vo.numeroactual;

/**
* FcOS_MISC_NUMVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FcOS_MISC_NUMVO {

	private Double adabas_isn;
	private Double os_cod_concepto;
	private Double os_marca_cl;
	private String os_descripcion;
	private String os_act_u_clase;
	private Double os_num_sec_num;
	private Double os_num_os_misc;
	private String os_interurb_ant;
	private String os_estado_renm;
	private String os_act_est_medidor;
	private Double os_importe;
	private String os_urb_act;
	private String os_estado_misc;
	private Double os_abonado;
	private java.util.Date os_fecha_cambio_renm;
	private String os_facturado;
	private String os_u_medida;
	private Double os_oficina;
	private String os_urb_ant;
	private Double os_periodo_misc;
	private java.util.Date os_fecha_expir;
	private String os_cod_mov_num;
	private Double os_num_sec_misc;
	private String os_cod_mov_renm;
	private String os_linea_ant;
	private String os_ant_u_clase;
	private String os_interurb_act;
	private Double os_ant_cod_clie;
	private Double os_anio_misc;
	private Double os_act_cod_clie;
	private Double os_ident_abo;
	private Double os_num_os_renm;
	private Double os_num_os_num;
	private Double os_num_sec_renm;
	private String os_cod_mov_misc;
	private Double os_importe_mensual;
	private Double os_zona;
	private Double os_act_oficina;
	private Double os_act_zona;
	private Double os_ant_zona;
	private Double os_vto_misc;
	private String os_ant_est_medidor;
	private Double os_act_abonado;
	private java.util.Date os_fecha_empalme;
	private java.util.Date os_fecha_expir_renm;
	private java.util.Date os_fecha_cambio;
	private java.util.Date os_fecha_cumplim;
	private Double os_ant_oficina;
	private Double os_ant_abonado;
	private String os_estado_num;
	private String os_linea_act;
	private java.util.Date os_fecha_bajada_misc;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FcOS_MISC_NUMVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_concepto
	* @return el valor de Os_cod_concepto
	*/ 
	public Double getOs_cod_concepto() {
		return os_cod_concepto;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_concepto
	* @param valor el valor de Os_cod_concepto
	*/ 
	public void setOs_cod_concepto(Double valor) {
		this.os_cod_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Os_marca_cl
	* @return el valor de Os_marca_cl
	*/ 
	public Double getOs_marca_cl() {
		return os_marca_cl;
	}
    	
	/**
	* Fija el valor del atributo Os_marca_cl
	* @param valor el valor de Os_marca_cl
	*/ 
	public void setOs_marca_cl(Double valor) {
		this.os_marca_cl = valor;
	}
	/**
	* Recupera el valor del atributo Os_descripcion
	* @return el valor de Os_descripcion
	*/ 
	public String getOs_descripcion() {
		return os_descripcion;
	}
    	
	/**
	* Fija el valor del atributo Os_descripcion
	* @param valor el valor de Os_descripcion
	*/ 
	public void setOs_descripcion(String valor) {
		this.os_descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_u_clase
	* @return el valor de Os_act_u_clase
	*/ 
	public String getOs_act_u_clase() {
		return os_act_u_clase;
	}
    	
	/**
	* Fija el valor del atributo Os_act_u_clase
	* @param valor el valor de Os_act_u_clase
	*/ 
	public void setOs_act_u_clase(String valor) {
		this.os_act_u_clase = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_sec_num
	* @return el valor de Os_num_sec_num
	*/ 
	public Double getOs_num_sec_num() {
		return os_num_sec_num;
	}
    	
	/**
	* Fija el valor del atributo Os_num_sec_num
	* @param valor el valor de Os_num_sec_num
	*/ 
	public void setOs_num_sec_num(Double valor) {
		this.os_num_sec_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_os_misc
	* @return el valor de Os_num_os_misc
	*/ 
	public Double getOs_num_os_misc() {
		return os_num_os_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_num_os_misc
	* @param valor el valor de Os_num_os_misc
	*/ 
	public void setOs_num_os_misc(Double valor) {
		this.os_num_os_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_interurb_ant
	* @return el valor de Os_interurb_ant
	*/ 
	public String getOs_interurb_ant() {
		return os_interurb_ant;
	}
    	
	/**
	* Fija el valor del atributo Os_interurb_ant
	* @param valor el valor de Os_interurb_ant
	*/ 
	public void setOs_interurb_ant(String valor) {
		this.os_interurb_ant = valor;
	}
	/**
	* Recupera el valor del atributo Os_estado_renm
	* @return el valor de Os_estado_renm
	*/ 
	public String getOs_estado_renm() {
		return os_estado_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_estado_renm
	* @param valor el valor de Os_estado_renm
	*/ 
	public void setOs_estado_renm(String valor) {
		this.os_estado_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_est_medidor
	* @return el valor de Os_act_est_medidor
	*/ 
	public String getOs_act_est_medidor() {
		return os_act_est_medidor;
	}
    	
	/**
	* Fija el valor del atributo Os_act_est_medidor
	* @param valor el valor de Os_act_est_medidor
	*/ 
	public void setOs_act_est_medidor(String valor) {
		this.os_act_est_medidor = valor;
	}
	/**
	* Recupera el valor del atributo Os_importe
	* @return el valor de Os_importe
	*/ 
	public Double getOs_importe() {
		return os_importe;
	}
    	
	/**
	* Fija el valor del atributo Os_importe
	* @param valor el valor de Os_importe
	*/ 
	public void setOs_importe(Double valor) {
		this.os_importe = valor;
	}
	/**
	* Recupera el valor del atributo Os_urb_act
	* @return el valor de Os_urb_act
	*/ 
	public String getOs_urb_act() {
		return os_urb_act;
	}
    	
	/**
	* Fija el valor del atributo Os_urb_act
	* @param valor el valor de Os_urb_act
	*/ 
	public void setOs_urb_act(String valor) {
		this.os_urb_act = valor;
	}
	/**
	* Recupera el valor del atributo Os_estado_misc
	* @return el valor de Os_estado_misc
	*/ 
	public String getOs_estado_misc() {
		return os_estado_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_estado_misc
	* @param valor el valor de Os_estado_misc
	*/ 
	public void setOs_estado_misc(String valor) {
		this.os_estado_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_abonado
	* @return el valor de Os_abonado
	*/ 
	public Double getOs_abonado() {
		return os_abonado;
	}
    	
	/**
	* Fija el valor del atributo Os_abonado
	* @param valor el valor de Os_abonado
	*/ 
	public void setOs_abonado(Double valor) {
		this.os_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_cambio_renm
	* @return el valor de Os_fecha_cambio_renm
	*/ 
	public java.util.Date getOs_fecha_cambio_renm() {
		return os_fecha_cambio_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_cambio_renm
	* @param valor el valor de Os_fecha_cambio_renm
	*/ 
	public void setOs_fecha_cambio_renm(java.util.Date valor) {
		this.os_fecha_cambio_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_facturado
	* @return el valor de Os_facturado
	*/ 
	public String getOs_facturado() {
		return os_facturado;
	}
    	
	/**
	* Fija el valor del atributo Os_facturado
	* @param valor el valor de Os_facturado
	*/ 
	public void setOs_facturado(String valor) {
		this.os_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Os_u_medida
	* @return el valor de Os_u_medida
	*/ 
	public String getOs_u_medida() {
		return os_u_medida;
	}
    	
	/**
	* Fija el valor del atributo Os_u_medida
	* @param valor el valor de Os_u_medida
	*/ 
	public void setOs_u_medida(String valor) {
		this.os_u_medida = valor;
	}
	/**
	* Recupera el valor del atributo Os_oficina
	* @return el valor de Os_oficina
	*/ 
	public Double getOs_oficina() {
		return os_oficina;
	}
    	
	/**
	* Fija el valor del atributo Os_oficina
	* @param valor el valor de Os_oficina
	*/ 
	public void setOs_oficina(Double valor) {
		this.os_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Os_urb_ant
	* @return el valor de Os_urb_ant
	*/ 
	public String getOs_urb_ant() {
		return os_urb_ant;
	}
    	
	/**
	* Fija el valor del atributo Os_urb_ant
	* @param valor el valor de Os_urb_ant
	*/ 
	public void setOs_urb_ant(String valor) {
		this.os_urb_ant = valor;
	}
	/**
	* Recupera el valor del atributo Os_periodo_misc
	* @return el valor de Os_periodo_misc
	*/ 
	public Double getOs_periodo_misc() {
		return os_periodo_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_periodo_misc
	* @param valor el valor de Os_periodo_misc
	*/ 
	public void setOs_periodo_misc(Double valor) {
		this.os_periodo_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_expir
	* @return el valor de Os_fecha_expir
	*/ 
	public java.util.Date getOs_fecha_expir() {
		return os_fecha_expir;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_expir
	* @param valor el valor de Os_fecha_expir
	*/ 
	public void setOs_fecha_expir(java.util.Date valor) {
		this.os_fecha_expir = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_mov_num
	* @return el valor de Os_cod_mov_num
	*/ 
	public String getOs_cod_mov_num() {
		return os_cod_mov_num;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_mov_num
	* @param valor el valor de Os_cod_mov_num
	*/ 
	public void setOs_cod_mov_num(String valor) {
		this.os_cod_mov_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_sec_misc
	* @return el valor de Os_num_sec_misc
	*/ 
	public Double getOs_num_sec_misc() {
		return os_num_sec_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_num_sec_misc
	* @param valor el valor de Os_num_sec_misc
	*/ 
	public void setOs_num_sec_misc(Double valor) {
		this.os_num_sec_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_mov_renm
	* @return el valor de Os_cod_mov_renm
	*/ 
	public String getOs_cod_mov_renm() {
		return os_cod_mov_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_mov_renm
	* @param valor el valor de Os_cod_mov_renm
	*/ 
	public void setOs_cod_mov_renm(String valor) {
		this.os_cod_mov_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_linea_ant
	* @return el valor de Os_linea_ant
	*/ 
	public String getOs_linea_ant() {
		return os_linea_ant;
	}
    	
	/**
	* Fija el valor del atributo Os_linea_ant
	* @param valor el valor de Os_linea_ant
	*/ 
	public void setOs_linea_ant(String valor) {
		this.os_linea_ant = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_u_clase
	* @return el valor de Os_ant_u_clase
	*/ 
	public String getOs_ant_u_clase() {
		return os_ant_u_clase;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_u_clase
	* @param valor el valor de Os_ant_u_clase
	*/ 
	public void setOs_ant_u_clase(String valor) {
		this.os_ant_u_clase = valor;
	}
	/**
	* Recupera el valor del atributo Os_interurb_act
	* @return el valor de Os_interurb_act
	*/ 
	public String getOs_interurb_act() {
		return os_interurb_act;
	}
    	
	/**
	* Fija el valor del atributo Os_interurb_act
	* @param valor el valor de Os_interurb_act
	*/ 
	public void setOs_interurb_act(String valor) {
		this.os_interurb_act = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_cod_clie
	* @return el valor de Os_ant_cod_clie
	*/ 
	public Double getOs_ant_cod_clie() {
		return os_ant_cod_clie;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_cod_clie
	* @param valor el valor de Os_ant_cod_clie
	*/ 
	public void setOs_ant_cod_clie(Double valor) {
		this.os_ant_cod_clie = valor;
	}
	/**
	* Recupera el valor del atributo Os_anio_misc
	* @return el valor de Os_anio_misc
	*/ 
	public Double getOs_anio_misc() {
		return os_anio_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_anio_misc
	* @param valor el valor de Os_anio_misc
	*/ 
	public void setOs_anio_misc(Double valor) {
		this.os_anio_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_cod_clie
	* @return el valor de Os_act_cod_clie
	*/ 
	public Double getOs_act_cod_clie() {
		return os_act_cod_clie;
	}
    	
	/**
	* Fija el valor del atributo Os_act_cod_clie
	* @param valor el valor de Os_act_cod_clie
	*/ 
	public void setOs_act_cod_clie(Double valor) {
		this.os_act_cod_clie = valor;
	}
	/**
	* Recupera el valor del atributo Os_ident_abo
	* @return el valor de Os_ident_abo
	*/ 
	public Double getOs_ident_abo() {
		return os_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Os_ident_abo
	* @param valor el valor de Os_ident_abo
	*/ 
	public void setOs_ident_abo(Double valor) {
		this.os_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_os_renm
	* @return el valor de Os_num_os_renm
	*/ 
	public Double getOs_num_os_renm() {
		return os_num_os_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_num_os_renm
	* @param valor el valor de Os_num_os_renm
	*/ 
	public void setOs_num_os_renm(Double valor) {
		this.os_num_os_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_os_num
	* @return el valor de Os_num_os_num
	*/ 
	public Double getOs_num_os_num() {
		return os_num_os_num;
	}
    	
	/**
	* Fija el valor del atributo Os_num_os_num
	* @param valor el valor de Os_num_os_num
	*/ 
	public void setOs_num_os_num(Double valor) {
		this.os_num_os_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_sec_renm
	* @return el valor de Os_num_sec_renm
	*/ 
	public Double getOs_num_sec_renm() {
		return os_num_sec_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_num_sec_renm
	* @param valor el valor de Os_num_sec_renm
	*/ 
	public void setOs_num_sec_renm(Double valor) {
		this.os_num_sec_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_mov_misc
	* @return el valor de Os_cod_mov_misc
	*/ 
	public String getOs_cod_mov_misc() {
		return os_cod_mov_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_mov_misc
	* @param valor el valor de Os_cod_mov_misc
	*/ 
	public void setOs_cod_mov_misc(String valor) {
		this.os_cod_mov_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_importe_mensual
	* @return el valor de Os_importe_mensual
	*/ 
	public Double getOs_importe_mensual() {
		return os_importe_mensual;
	}
    	
	/**
	* Fija el valor del atributo Os_importe_mensual
	* @param valor el valor de Os_importe_mensual
	*/ 
	public void setOs_importe_mensual(Double valor) {
		this.os_importe_mensual = valor;
	}
	/**
	* Recupera el valor del atributo Os_zona
	* @return el valor de Os_zona
	*/ 
	public Double getOs_zona() {
		return os_zona;
	}
    	
	/**
	* Fija el valor del atributo Os_zona
	* @param valor el valor de Os_zona
	*/ 
	public void setOs_zona(Double valor) {
		this.os_zona = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_oficina
	* @return el valor de Os_act_oficina
	*/ 
	public Double getOs_act_oficina() {
		return os_act_oficina;
	}
    	
	/**
	* Fija el valor del atributo Os_act_oficina
	* @param valor el valor de Os_act_oficina
	*/ 
	public void setOs_act_oficina(Double valor) {
		this.os_act_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_zona
	* @return el valor de Os_act_zona
	*/ 
	public Double getOs_act_zona() {
		return os_act_zona;
	}
    	
	/**
	* Fija el valor del atributo Os_act_zona
	* @param valor el valor de Os_act_zona
	*/ 
	public void setOs_act_zona(Double valor) {
		this.os_act_zona = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_zona
	* @return el valor de Os_ant_zona
	*/ 
	public Double getOs_ant_zona() {
		return os_ant_zona;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_zona
	* @param valor el valor de Os_ant_zona
	*/ 
	public void setOs_ant_zona(Double valor) {
		this.os_ant_zona = valor;
	}
	/**
	* Recupera el valor del atributo Os_vto_misc
	* @return el valor de Os_vto_misc
	*/ 
	public Double getOs_vto_misc() {
		return os_vto_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_vto_misc
	* @param valor el valor de Os_vto_misc
	*/ 
	public void setOs_vto_misc(Double valor) {
		this.os_vto_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_est_medidor
	* @return el valor de Os_ant_est_medidor
	*/ 
	public String getOs_ant_est_medidor() {
		return os_ant_est_medidor;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_est_medidor
	* @param valor el valor de Os_ant_est_medidor
	*/ 
	public void setOs_ant_est_medidor(String valor) {
		this.os_ant_est_medidor = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_abonado
	* @return el valor de Os_act_abonado
	*/ 
	public Double getOs_act_abonado() {
		return os_act_abonado;
	}
    	
	/**
	* Fija el valor del atributo Os_act_abonado
	* @param valor el valor de Os_act_abonado
	*/ 
	public void setOs_act_abonado(Double valor) {
		this.os_act_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_empalme
	* @return el valor de Os_fecha_empalme
	*/ 
	public java.util.Date getOs_fecha_empalme() {
		return os_fecha_empalme;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_empalme
	* @param valor el valor de Os_fecha_empalme
	*/ 
	public void setOs_fecha_empalme(java.util.Date valor) {
		this.os_fecha_empalme = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_expir_renm
	* @return el valor de Os_fecha_expir_renm
	*/ 
	public java.util.Date getOs_fecha_expir_renm() {
		return os_fecha_expir_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_expir_renm
	* @param valor el valor de Os_fecha_expir_renm
	*/ 
	public void setOs_fecha_expir_renm(java.util.Date valor) {
		this.os_fecha_expir_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_cambio
	* @return el valor de Os_fecha_cambio
	*/ 
	public java.util.Date getOs_fecha_cambio() {
		return os_fecha_cambio;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_cambio
	* @param valor el valor de Os_fecha_cambio
	*/ 
	public void setOs_fecha_cambio(java.util.Date valor) {
		this.os_fecha_cambio = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_cumplim
	* @return el valor de Os_fecha_cumplim
	*/ 
	public java.util.Date getOs_fecha_cumplim() {
		return os_fecha_cumplim;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_cumplim
	* @param valor el valor de Os_fecha_cumplim
	*/ 
	public void setOs_fecha_cumplim(java.util.Date valor) {
		this.os_fecha_cumplim = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_oficina
	* @return el valor de Os_ant_oficina
	*/ 
	public Double getOs_ant_oficina() {
		return os_ant_oficina;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_oficina
	* @param valor el valor de Os_ant_oficina
	*/ 
	public void setOs_ant_oficina(Double valor) {
		this.os_ant_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_abonado
	* @return el valor de Os_ant_abonado
	*/ 
	public Double getOs_ant_abonado() {
		return os_ant_abonado;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_abonado
	* @param valor el valor de Os_ant_abonado
	*/ 
	public void setOs_ant_abonado(Double valor) {
		this.os_ant_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Os_estado_num
	* @return el valor de Os_estado_num
	*/ 
	public String getOs_estado_num() {
		return os_estado_num;
	}
    	
	/**
	* Fija el valor del atributo Os_estado_num
	* @param valor el valor de Os_estado_num
	*/ 
	public void setOs_estado_num(String valor) {
		this.os_estado_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_linea_act
	* @return el valor de Os_linea_act
	*/ 
	public String getOs_linea_act() {
		return os_linea_act;
	}
    	
	/**
	* Fija el valor del atributo Os_linea_act
	* @param valor el valor de Os_linea_act
	*/ 
	public void setOs_linea_act(String valor) {
		this.os_linea_act = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_bajada_misc
	* @return el valor de Os_fecha_bajada_misc
	*/ 
	public java.util.Date getOs_fecha_bajada_misc() {
		return os_fecha_bajada_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_bajada_misc
	* @param valor el valor de Os_fecha_bajada_misc
	*/ 
	public void setOs_fecha_bajada_misc(java.util.Date valor) {
		this.os_fecha_bajada_misc = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Os_cod_concepto=["+this.getOs_cod_concepto()+"]\r\n");
				sb.append("Os_marca_cl=["+this.getOs_marca_cl()+"]\r\n");
				sb.append("Os_descripcion=["+this.getOs_descripcion()+"]\r\n");
				sb.append("Os_act_u_clase=["+this.getOs_act_u_clase()+"]\r\n");
				sb.append("Os_num_sec_num=["+this.getOs_num_sec_num()+"]\r\n");
				sb.append("Os_num_os_misc=["+this.getOs_num_os_misc()+"]\r\n");
				sb.append("Os_interurb_ant=["+this.getOs_interurb_ant()+"]\r\n");
				sb.append("Os_estado_renm=["+this.getOs_estado_renm()+"]\r\n");
				sb.append("Os_act_est_medidor=["+this.getOs_act_est_medidor()+"]\r\n");
				sb.append("Os_importe=["+this.getOs_importe()+"]\r\n");
				sb.append("Os_urb_act=["+this.getOs_urb_act()+"]\r\n");
				sb.append("Os_estado_misc=["+this.getOs_estado_misc()+"]\r\n");
				sb.append("Os_abonado=["+this.getOs_abonado()+"]\r\n");
				sb.append("Os_fecha_cambio_renm=["+this.getOs_fecha_cambio_renm()+"]\r\n");
				sb.append("Os_facturado=["+this.getOs_facturado()+"]\r\n");
				sb.append("Os_u_medida=["+this.getOs_u_medida()+"]\r\n");
				sb.append("Os_oficina=["+this.getOs_oficina()+"]\r\n");
				sb.append("Os_urb_ant=["+this.getOs_urb_ant()+"]\r\n");
				sb.append("Os_periodo_misc=["+this.getOs_periodo_misc()+"]\r\n");
				sb.append("Os_fecha_expir=["+this.getOs_fecha_expir()+"]\r\n");
				sb.append("Os_cod_mov_num=["+this.getOs_cod_mov_num()+"]\r\n");
				sb.append("Os_num_sec_misc=["+this.getOs_num_sec_misc()+"]\r\n");
				sb.append("Os_cod_mov_renm=["+this.getOs_cod_mov_renm()+"]\r\n");
				sb.append("Os_linea_ant=["+this.getOs_linea_ant()+"]\r\n");
				sb.append("Os_ant_u_clase=["+this.getOs_ant_u_clase()+"]\r\n");
				sb.append("Os_interurb_act=["+this.getOs_interurb_act()+"]\r\n");
				sb.append("Os_ant_cod_clie=["+this.getOs_ant_cod_clie()+"]\r\n");
				sb.append("Os_anio_misc=["+this.getOs_anio_misc()+"]\r\n");
				sb.append("Os_act_cod_clie=["+this.getOs_act_cod_clie()+"]\r\n");
				sb.append("Os_ident_abo=["+this.getOs_ident_abo()+"]\r\n");
				sb.append("Os_num_os_renm=["+this.getOs_num_os_renm()+"]\r\n");
				sb.append("Os_num_os_num=["+this.getOs_num_os_num()+"]\r\n");
				sb.append("Os_num_sec_renm=["+this.getOs_num_sec_renm()+"]\r\n");
				sb.append("Os_cod_mov_misc=["+this.getOs_cod_mov_misc()+"]\r\n");
				sb.append("Os_importe_mensual=["+this.getOs_importe_mensual()+"]\r\n");
				sb.append("Os_zona=["+this.getOs_zona()+"]\r\n");
				sb.append("Os_act_oficina=["+this.getOs_act_oficina()+"]\r\n");
				sb.append("Os_act_zona=["+this.getOs_act_zona()+"]\r\n");
				sb.append("Os_ant_zona=["+this.getOs_ant_zona()+"]\r\n");
				sb.append("Os_vto_misc=["+this.getOs_vto_misc()+"]\r\n");
				sb.append("Os_ant_est_medidor=["+this.getOs_ant_est_medidor()+"]\r\n");
				sb.append("Os_act_abonado=["+this.getOs_act_abonado()+"]\r\n");
				sb.append("Os_fecha_empalme=["+this.getOs_fecha_empalme()+"]\r\n");
				sb.append("Os_fecha_expir_renm=["+this.getOs_fecha_expir_renm()+"]\r\n");
				sb.append("Os_fecha_cambio=["+this.getOs_fecha_cambio()+"]\r\n");
				sb.append("Os_fecha_cumplim=["+this.getOs_fecha_cumplim()+"]\r\n");
				sb.append("Os_ant_oficina=["+this.getOs_ant_oficina()+"]\r\n");
				sb.append("Os_ant_abonado=["+this.getOs_ant_abonado()+"]\r\n");
				sb.append("Os_estado_num=["+this.getOs_estado_num()+"]\r\n");
				sb.append("Os_linea_act=["+this.getOs_linea_act()+"]\r\n");
				sb.append("Os_fecha_bajada_misc=["+this.getOs_fecha_bajada_misc()+"]\r\n");
				return sb.toString();
	}
}