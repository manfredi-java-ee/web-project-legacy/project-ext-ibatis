package arg.ros.acc.vo.equiposyservicios;

/**
* FcMTABPPVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FcMTABPPVO {

	private Double adabas_isn;
	private Double mt_origen;
	private Double mt_cta_com_usu;
	private Double mt_venc;
	private Double mt_benef_jub;
	private String mt_alta_pura;
	private Double mt_bloque1_cbu;
	private String mt_motivo_baja;
	private Double mt_d_t_cta;
	private java.util.Date mt_fecha_vto_porc_loc;
	private java.util.Date mt_b_fecha;
	private String mt_n_doc;
	private String mt_muestra_estadistica;
	private Double mt_ident_abo;
	private java.util.Date mt_fecha_form_dgi;
	private Double mt_cons_p_interurb_ne_ant;
	private String mt_i_resp_ant;
	private Double mt_legajo;
	private Double mt_fed_ult_abono;
	private String mt_nombre_loc;
	private String mt_grupo_atis;
	private String mt_x_cuit;
	private java.util.Date mt_fecha_cdn;
	private String mt_x_cuit_loc;
	private String mt_ingbru_nro_vie;
	private Double mt_megat_adher;
	private String mt_z_renglon2;
	private java.util.Date mt_a_fecha;
	private String mt_bajo_consumo;
	private String mt_nombre_1;
	private java.util.Date mt_fecha_vto_porc;
	private Double mt_f_hasta_consumo;
	private Double mt_tipo_linea;
	private Double mt_z_resguardo;
	private String mt_icc_code_usu;
	private Double mt_porc_percep_loc;
	private java.util.Date mt_j_fecha;
	private String mt_z_ind_8100;
	private String mt_d_orig_db;
	private String mt_clase_fact;
	private Double mt_cant_cuotas;
	private Double mt_zona;
	private Double mt_f_baja_asoc;
	private Double mt_i_ult_abono;
	private Double mt_porc_percep;
	private Double mt_categ;
	private Double mt_cargo_conexion;
	private Double mt_cta_fact;
	private Double mt_f_alta_asoc;
	private String mt_i_medido;
	private String mt_scoring_pago;
	private Double mt_febj_vto;
	private Double mt_fed_abono_ant;
	private String mt_z_renglon3;
	private String mt_icc_code;
	private java.util.Date mt_u_fecha;
	private String mt_est_medidor;
	private String mt_linea;
	private Double mt_cons_p_interurb_br_ant;
	private Double mt_d_banco;
	private Double mt_ingbru_fec_vto_porc;
	private String mt_estado;
	private String mt_ingbru_nro_nue;
	private Double mt_med_corte;
	private Double mt_cons_p_urb_ne_ant;
	private Double mt_cta_atis;
	private Double mt_i_lineas;
	private Double mt_abonado;
	private Double mt_a_num_os;
	private String mt_z_est_direc;
	private String mt_marca_baja;
	private String mt_d_cuenta;
	private Double mt_tarjeta;
	private Double mt_v_fecha;
	private Double mt_fe_corte;
	private Double mt_cli_atis;
	private String mt_ult_fact;
	private String mt_urb;
	private Double mt_b_num_os;
	private String mt_mot_noinnovar;
	private String mt_form_dgi;
	private Double mt_cons_p_urb_br_ant;
	private String mt_m_descuentos;
	private Double mt_oficina;
	private String mt_interurb;
	private Double mt_g_oficina_tr;
	private String mt_i_origen;
	private String mt_z_renglon1;
	private java.util.Date mt_fecha_cat_bajo_consumo;
	private String mt_contrato;
	private java.util.Date mt_fecha_form_dgi_loc;
	private Double mt_x_resp_loc;
	private Double mt_marca_aparato;
	private java.util.Date mt_fecha_alta_db;
	private Double mt_t_doc;
	private Double mt_u_num_os;
	private Double mt_cta_com_tit;
	private java.util.Date mt_x_fecha;
	private String mt_locatario;
	private String mt_megat_serie;
	private Double mt_x_resp;
	private String mt_ingbru_tipo_inscr;
	private Double mt_g_zona_tr;
	private Double mt_nro_arcom;
	private String mt_ani;
	private Double mt_tipo_alta;
	private String mt_i_act_abono;
	private Double mt_ingbru_porc_desc;
	private Double mt_g_abonado_tr;
	private String mt_j_exped;
	private java.math.BigDecimal mt_bloque2_cbu;
	private String mt_j_tipo;
	private Double mt_feh_ult_abono;
	private Double mt_z_sobretasa;
	private String mt_form_dgi_loc;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FcMTABPPVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Mt_origen
	* @return el valor de Mt_origen
	*/ 
	public Double getMt_origen() {
		return mt_origen;
	}
    	
	/**
	* Fija el valor del atributo Mt_origen
	* @param valor el valor de Mt_origen
	*/ 
	public void setMt_origen(Double valor) {
		this.mt_origen = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cta_com_usu
	* @return el valor de Mt_cta_com_usu
	*/ 
	public Double getMt_cta_com_usu() {
		return mt_cta_com_usu;
	}
    	
	/**
	* Fija el valor del atributo Mt_cta_com_usu
	* @param valor el valor de Mt_cta_com_usu
	*/ 
	public void setMt_cta_com_usu(Double valor) {
		this.mt_cta_com_usu = valor;
	}
	/**
	* Recupera el valor del atributo Mt_venc
	* @return el valor de Mt_venc
	*/ 
	public Double getMt_venc() {
		return mt_venc;
	}
    	
	/**
	* Fija el valor del atributo Mt_venc
	* @param valor el valor de Mt_venc
	*/ 
	public void setMt_venc(Double valor) {
		this.mt_venc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_benef_jub
	* @return el valor de Mt_benef_jub
	*/ 
	public Double getMt_benef_jub() {
		return mt_benef_jub;
	}
    	
	/**
	* Fija el valor del atributo Mt_benef_jub
	* @param valor el valor de Mt_benef_jub
	*/ 
	public void setMt_benef_jub(Double valor) {
		this.mt_benef_jub = valor;
	}
	/**
	* Recupera el valor del atributo Mt_alta_pura
	* @return el valor de Mt_alta_pura
	*/ 
	public String getMt_alta_pura() {
		return mt_alta_pura;
	}
    	
	/**
	* Fija el valor del atributo Mt_alta_pura
	* @param valor el valor de Mt_alta_pura
	*/ 
	public void setMt_alta_pura(String valor) {
		this.mt_alta_pura = valor;
	}
	/**
	* Recupera el valor del atributo Mt_bloque1_cbu
	* @return el valor de Mt_bloque1_cbu
	*/ 
	public Double getMt_bloque1_cbu() {
		return mt_bloque1_cbu;
	}
    	
	/**
	* Fija el valor del atributo Mt_bloque1_cbu
	* @param valor el valor de Mt_bloque1_cbu
	*/ 
	public void setMt_bloque1_cbu(Double valor) {
		this.mt_bloque1_cbu = valor;
	}
	/**
	* Recupera el valor del atributo Mt_motivo_baja
	* @return el valor de Mt_motivo_baja
	*/ 
	public String getMt_motivo_baja() {
		return mt_motivo_baja;
	}
    	
	/**
	* Fija el valor del atributo Mt_motivo_baja
	* @param valor el valor de Mt_motivo_baja
	*/ 
	public void setMt_motivo_baja(String valor) {
		this.mt_motivo_baja = valor;
	}
	/**
	* Recupera el valor del atributo Mt_d_t_cta
	* @return el valor de Mt_d_t_cta
	*/ 
	public Double getMt_d_t_cta() {
		return mt_d_t_cta;
	}
    	
	/**
	* Fija el valor del atributo Mt_d_t_cta
	* @param valor el valor de Mt_d_t_cta
	*/ 
	public void setMt_d_t_cta(Double valor) {
		this.mt_d_t_cta = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fecha_vto_porc_loc
	* @return el valor de Mt_fecha_vto_porc_loc
	*/ 
	public java.util.Date getMt_fecha_vto_porc_loc() {
		return mt_fecha_vto_porc_loc;
	}
    	
	/**
	* Fija el valor del atributo Mt_fecha_vto_porc_loc
	* @param valor el valor de Mt_fecha_vto_porc_loc
	*/ 
	public void setMt_fecha_vto_porc_loc(java.util.Date valor) {
		this.mt_fecha_vto_porc_loc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_b_fecha
	* @return el valor de Mt_b_fecha
	*/ 
	public java.util.Date getMt_b_fecha() {
		return mt_b_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_b_fecha
	* @param valor el valor de Mt_b_fecha
	*/ 
	public void setMt_b_fecha(java.util.Date valor) {
		this.mt_b_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_n_doc
	* @return el valor de Mt_n_doc
	*/ 
	public String getMt_n_doc() {
		return mt_n_doc;
	}
    	
	/**
	* Fija el valor del atributo Mt_n_doc
	* @param valor el valor de Mt_n_doc
	*/ 
	public void setMt_n_doc(String valor) {
		this.mt_n_doc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_muestra_estadistica
	* @return el valor de Mt_muestra_estadistica
	*/ 
	public String getMt_muestra_estadistica() {
		return mt_muestra_estadistica;
	}
    	
	/**
	* Fija el valor del atributo Mt_muestra_estadistica
	* @param valor el valor de Mt_muestra_estadistica
	*/ 
	public void setMt_muestra_estadistica(String valor) {
		this.mt_muestra_estadistica = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ident_abo
	* @return el valor de Mt_ident_abo
	*/ 
	public Double getMt_ident_abo() {
		return mt_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Mt_ident_abo
	* @param valor el valor de Mt_ident_abo
	*/ 
	public void setMt_ident_abo(Double valor) {
		this.mt_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fecha_form_dgi
	* @return el valor de Mt_fecha_form_dgi
	*/ 
	public java.util.Date getMt_fecha_form_dgi() {
		return mt_fecha_form_dgi;
	}
    	
	/**
	* Fija el valor del atributo Mt_fecha_form_dgi
	* @param valor el valor de Mt_fecha_form_dgi
	*/ 
	public void setMt_fecha_form_dgi(java.util.Date valor) {
		this.mt_fecha_form_dgi = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cons_p_interurb_ne_ant
	* @return el valor de Mt_cons_p_interurb_ne_ant
	*/ 
	public Double getMt_cons_p_interurb_ne_ant() {
		return mt_cons_p_interurb_ne_ant;
	}
    	
	/**
	* Fija el valor del atributo Mt_cons_p_interurb_ne_ant
	* @param valor el valor de Mt_cons_p_interurb_ne_ant
	*/ 
	public void setMt_cons_p_interurb_ne_ant(Double valor) {
		this.mt_cons_p_interurb_ne_ant = valor;
	}
	/**
	* Recupera el valor del atributo Mt_i_resp_ant
	* @return el valor de Mt_i_resp_ant
	*/ 
	public String getMt_i_resp_ant() {
		return mt_i_resp_ant;
	}
    	
	/**
	* Fija el valor del atributo Mt_i_resp_ant
	* @param valor el valor de Mt_i_resp_ant
	*/ 
	public void setMt_i_resp_ant(String valor) {
		this.mt_i_resp_ant = valor;
	}
	/**
	* Recupera el valor del atributo Mt_legajo
	* @return el valor de Mt_legajo
	*/ 
	public Double getMt_legajo() {
		return mt_legajo;
	}
    	
	/**
	* Fija el valor del atributo Mt_legajo
	* @param valor el valor de Mt_legajo
	*/ 
	public void setMt_legajo(Double valor) {
		this.mt_legajo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fed_ult_abono
	* @return el valor de Mt_fed_ult_abono
	*/ 
	public Double getMt_fed_ult_abono() {
		return mt_fed_ult_abono;
	}
    	
	/**
	* Fija el valor del atributo Mt_fed_ult_abono
	* @param valor el valor de Mt_fed_ult_abono
	*/ 
	public void setMt_fed_ult_abono(Double valor) {
		this.mt_fed_ult_abono = valor;
	}
	/**
	* Recupera el valor del atributo Mt_nombre_loc
	* @return el valor de Mt_nombre_loc
	*/ 
	public String getMt_nombre_loc() {
		return mt_nombre_loc;
	}
    	
	/**
	* Fija el valor del atributo Mt_nombre_loc
	* @param valor el valor de Mt_nombre_loc
	*/ 
	public void setMt_nombre_loc(String valor) {
		this.mt_nombre_loc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_grupo_atis
	* @return el valor de Mt_grupo_atis
	*/ 
	public String getMt_grupo_atis() {
		return mt_grupo_atis;
	}
    	
	/**
	* Fija el valor del atributo Mt_grupo_atis
	* @param valor el valor de Mt_grupo_atis
	*/ 
	public void setMt_grupo_atis(String valor) {
		this.mt_grupo_atis = valor;
	}
	/**
	* Recupera el valor del atributo Mt_x_cuit
	* @return el valor de Mt_x_cuit
	*/ 
	public String getMt_x_cuit() {
		return mt_x_cuit;
	}
    	
	/**
	* Fija el valor del atributo Mt_x_cuit
	* @param valor el valor de Mt_x_cuit
	*/ 
	public void setMt_x_cuit(String valor) {
		this.mt_x_cuit = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fecha_cdn
	* @return el valor de Mt_fecha_cdn
	*/ 
	public java.util.Date getMt_fecha_cdn() {
		return mt_fecha_cdn;
	}
    	
	/**
	* Fija el valor del atributo Mt_fecha_cdn
	* @param valor el valor de Mt_fecha_cdn
	*/ 
	public void setMt_fecha_cdn(java.util.Date valor) {
		this.mt_fecha_cdn = valor;
	}
	/**
	* Recupera el valor del atributo Mt_x_cuit_loc
	* @return el valor de Mt_x_cuit_loc
	*/ 
	public String getMt_x_cuit_loc() {
		return mt_x_cuit_loc;
	}
    	
	/**
	* Fija el valor del atributo Mt_x_cuit_loc
	* @param valor el valor de Mt_x_cuit_loc
	*/ 
	public void setMt_x_cuit_loc(String valor) {
		this.mt_x_cuit_loc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ingbru_nro_vie
	* @return el valor de Mt_ingbru_nro_vie
	*/ 
	public String getMt_ingbru_nro_vie() {
		return mt_ingbru_nro_vie;
	}
    	
	/**
	* Fija el valor del atributo Mt_ingbru_nro_vie
	* @param valor el valor de Mt_ingbru_nro_vie
	*/ 
	public void setMt_ingbru_nro_vie(String valor) {
		this.mt_ingbru_nro_vie = valor;
	}
	/**
	* Recupera el valor del atributo Mt_megat_adher
	* @return el valor de Mt_megat_adher
	*/ 
	public Double getMt_megat_adher() {
		return mt_megat_adher;
	}
    	
	/**
	* Fija el valor del atributo Mt_megat_adher
	* @param valor el valor de Mt_megat_adher
	*/ 
	public void setMt_megat_adher(Double valor) {
		this.mt_megat_adher = valor;
	}
	/**
	* Recupera el valor del atributo Mt_z_renglon2
	* @return el valor de Mt_z_renglon2
	*/ 
	public String getMt_z_renglon2() {
		return mt_z_renglon2;
	}
    	
	/**
	* Fija el valor del atributo Mt_z_renglon2
	* @param valor el valor de Mt_z_renglon2
	*/ 
	public void setMt_z_renglon2(String valor) {
		this.mt_z_renglon2 = valor;
	}
	/**
	* Recupera el valor del atributo Mt_a_fecha
	* @return el valor de Mt_a_fecha
	*/ 
	public java.util.Date getMt_a_fecha() {
		return mt_a_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_a_fecha
	* @param valor el valor de Mt_a_fecha
	*/ 
	public void setMt_a_fecha(java.util.Date valor) {
		this.mt_a_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_bajo_consumo
	* @return el valor de Mt_bajo_consumo
	*/ 
	public String getMt_bajo_consumo() {
		return mt_bajo_consumo;
	}
    	
	/**
	* Fija el valor del atributo Mt_bajo_consumo
	* @param valor el valor de Mt_bajo_consumo
	*/ 
	public void setMt_bajo_consumo(String valor) {
		this.mt_bajo_consumo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_nombre_1
	* @return el valor de Mt_nombre_1
	*/ 
	public String getMt_nombre_1() {
		return mt_nombre_1;
	}
    	
	/**
	* Fija el valor del atributo Mt_nombre_1
	* @param valor el valor de Mt_nombre_1
	*/ 
	public void setMt_nombre_1(String valor) {
		this.mt_nombre_1 = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fecha_vto_porc
	* @return el valor de Mt_fecha_vto_porc
	*/ 
	public java.util.Date getMt_fecha_vto_porc() {
		return mt_fecha_vto_porc;
	}
    	
	/**
	* Fija el valor del atributo Mt_fecha_vto_porc
	* @param valor el valor de Mt_fecha_vto_porc
	*/ 
	public void setMt_fecha_vto_porc(java.util.Date valor) {
		this.mt_fecha_vto_porc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_f_hasta_consumo
	* @return el valor de Mt_f_hasta_consumo
	*/ 
	public Double getMt_f_hasta_consumo() {
		return mt_f_hasta_consumo;
	}
    	
	/**
	* Fija el valor del atributo Mt_f_hasta_consumo
	* @param valor el valor de Mt_f_hasta_consumo
	*/ 
	public void setMt_f_hasta_consumo(Double valor) {
		this.mt_f_hasta_consumo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_tipo_linea
	* @return el valor de Mt_tipo_linea
	*/ 
	public Double getMt_tipo_linea() {
		return mt_tipo_linea;
	}
    	
	/**
	* Fija el valor del atributo Mt_tipo_linea
	* @param valor el valor de Mt_tipo_linea
	*/ 
	public void setMt_tipo_linea(Double valor) {
		this.mt_tipo_linea = valor;
	}
	/**
	* Recupera el valor del atributo Mt_z_resguardo
	* @return el valor de Mt_z_resguardo
	*/ 
	public Double getMt_z_resguardo() {
		return mt_z_resguardo;
	}
    	
	/**
	* Fija el valor del atributo Mt_z_resguardo
	* @param valor el valor de Mt_z_resguardo
	*/ 
	public void setMt_z_resguardo(Double valor) {
		this.mt_z_resguardo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_icc_code_usu
	* @return el valor de Mt_icc_code_usu
	*/ 
	public String getMt_icc_code_usu() {
		return mt_icc_code_usu;
	}
    	
	/**
	* Fija el valor del atributo Mt_icc_code_usu
	* @param valor el valor de Mt_icc_code_usu
	*/ 
	public void setMt_icc_code_usu(String valor) {
		this.mt_icc_code_usu = valor;
	}
	/**
	* Recupera el valor del atributo Mt_porc_percep_loc
	* @return el valor de Mt_porc_percep_loc
	*/ 
	public Double getMt_porc_percep_loc() {
		return mt_porc_percep_loc;
	}
    	
	/**
	* Fija el valor del atributo Mt_porc_percep_loc
	* @param valor el valor de Mt_porc_percep_loc
	*/ 
	public void setMt_porc_percep_loc(Double valor) {
		this.mt_porc_percep_loc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_j_fecha
	* @return el valor de Mt_j_fecha
	*/ 
	public java.util.Date getMt_j_fecha() {
		return mt_j_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_j_fecha
	* @param valor el valor de Mt_j_fecha
	*/ 
	public void setMt_j_fecha(java.util.Date valor) {
		this.mt_j_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_z_ind_8100
	* @return el valor de Mt_z_ind_8100
	*/ 
	public String getMt_z_ind_8100() {
		return mt_z_ind_8100;
	}
    	
	/**
	* Fija el valor del atributo Mt_z_ind_8100
	* @param valor el valor de Mt_z_ind_8100
	*/ 
	public void setMt_z_ind_8100(String valor) {
		this.mt_z_ind_8100 = valor;
	}
	/**
	* Recupera el valor del atributo Mt_d_orig_db
	* @return el valor de Mt_d_orig_db
	*/ 
	public String getMt_d_orig_db() {
		return mt_d_orig_db;
	}
    	
	/**
	* Fija el valor del atributo Mt_d_orig_db
	* @param valor el valor de Mt_d_orig_db
	*/ 
	public void setMt_d_orig_db(String valor) {
		this.mt_d_orig_db = valor;
	}
	/**
	* Recupera el valor del atributo Mt_clase_fact
	* @return el valor de Mt_clase_fact
	*/ 
	public String getMt_clase_fact() {
		return mt_clase_fact;
	}
    	
	/**
	* Fija el valor del atributo Mt_clase_fact
	* @param valor el valor de Mt_clase_fact
	*/ 
	public void setMt_clase_fact(String valor) {
		this.mt_clase_fact = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cant_cuotas
	* @return el valor de Mt_cant_cuotas
	*/ 
	public Double getMt_cant_cuotas() {
		return mt_cant_cuotas;
	}
    	
	/**
	* Fija el valor del atributo Mt_cant_cuotas
	* @param valor el valor de Mt_cant_cuotas
	*/ 
	public void setMt_cant_cuotas(Double valor) {
		this.mt_cant_cuotas = valor;
	}
	/**
	* Recupera el valor del atributo Mt_zona
	* @return el valor de Mt_zona
	*/ 
	public Double getMt_zona() {
		return mt_zona;
	}
    	
	/**
	* Fija el valor del atributo Mt_zona
	* @param valor el valor de Mt_zona
	*/ 
	public void setMt_zona(Double valor) {
		this.mt_zona = valor;
	}
	/**
	* Recupera el valor del atributo Mt_f_baja_asoc
	* @return el valor de Mt_f_baja_asoc
	*/ 
	public Double getMt_f_baja_asoc() {
		return mt_f_baja_asoc;
	}
    	
	/**
	* Fija el valor del atributo Mt_f_baja_asoc
	* @param valor el valor de Mt_f_baja_asoc
	*/ 
	public void setMt_f_baja_asoc(Double valor) {
		this.mt_f_baja_asoc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_i_ult_abono
	* @return el valor de Mt_i_ult_abono
	*/ 
	public Double getMt_i_ult_abono() {
		return mt_i_ult_abono;
	}
    	
	/**
	* Fija el valor del atributo Mt_i_ult_abono
	* @param valor el valor de Mt_i_ult_abono
	*/ 
	public void setMt_i_ult_abono(Double valor) {
		this.mt_i_ult_abono = valor;
	}
	/**
	* Recupera el valor del atributo Mt_porc_percep
	* @return el valor de Mt_porc_percep
	*/ 
	public Double getMt_porc_percep() {
		return mt_porc_percep;
	}
    	
	/**
	* Fija el valor del atributo Mt_porc_percep
	* @param valor el valor de Mt_porc_percep
	*/ 
	public void setMt_porc_percep(Double valor) {
		this.mt_porc_percep = valor;
	}
	/**
	* Recupera el valor del atributo Mt_categ
	* @return el valor de Mt_categ
	*/ 
	public Double getMt_categ() {
		return mt_categ;
	}
    	
	/**
	* Fija el valor del atributo Mt_categ
	* @param valor el valor de Mt_categ
	*/ 
	public void setMt_categ(Double valor) {
		this.mt_categ = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cargo_conexion
	* @return el valor de Mt_cargo_conexion
	*/ 
	public Double getMt_cargo_conexion() {
		return mt_cargo_conexion;
	}
    	
	/**
	* Fija el valor del atributo Mt_cargo_conexion
	* @param valor el valor de Mt_cargo_conexion
	*/ 
	public void setMt_cargo_conexion(Double valor) {
		this.mt_cargo_conexion = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cta_fact
	* @return el valor de Mt_cta_fact
	*/ 
	public Double getMt_cta_fact() {
		return mt_cta_fact;
	}
    	
	/**
	* Fija el valor del atributo Mt_cta_fact
	* @param valor el valor de Mt_cta_fact
	*/ 
	public void setMt_cta_fact(Double valor) {
		this.mt_cta_fact = valor;
	}
	/**
	* Recupera el valor del atributo Mt_f_alta_asoc
	* @return el valor de Mt_f_alta_asoc
	*/ 
	public Double getMt_f_alta_asoc() {
		return mt_f_alta_asoc;
	}
    	
	/**
	* Fija el valor del atributo Mt_f_alta_asoc
	* @param valor el valor de Mt_f_alta_asoc
	*/ 
	public void setMt_f_alta_asoc(Double valor) {
		this.mt_f_alta_asoc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_i_medido
	* @return el valor de Mt_i_medido
	*/ 
	public String getMt_i_medido() {
		return mt_i_medido;
	}
    	
	/**
	* Fija el valor del atributo Mt_i_medido
	* @param valor el valor de Mt_i_medido
	*/ 
	public void setMt_i_medido(String valor) {
		this.mt_i_medido = valor;
	}
	/**
	* Recupera el valor del atributo Mt_scoring_pago
	* @return el valor de Mt_scoring_pago
	*/ 
	public String getMt_scoring_pago() {
		return mt_scoring_pago;
	}
    	
	/**
	* Fija el valor del atributo Mt_scoring_pago
	* @param valor el valor de Mt_scoring_pago
	*/ 
	public void setMt_scoring_pago(String valor) {
		this.mt_scoring_pago = valor;
	}
	/**
	* Recupera el valor del atributo Mt_febj_vto
	* @return el valor de Mt_febj_vto
	*/ 
	public Double getMt_febj_vto() {
		return mt_febj_vto;
	}
    	
	/**
	* Fija el valor del atributo Mt_febj_vto
	* @param valor el valor de Mt_febj_vto
	*/ 
	public void setMt_febj_vto(Double valor) {
		this.mt_febj_vto = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fed_abono_ant
	* @return el valor de Mt_fed_abono_ant
	*/ 
	public Double getMt_fed_abono_ant() {
		return mt_fed_abono_ant;
	}
    	
	/**
	* Fija el valor del atributo Mt_fed_abono_ant
	* @param valor el valor de Mt_fed_abono_ant
	*/ 
	public void setMt_fed_abono_ant(Double valor) {
		this.mt_fed_abono_ant = valor;
	}
	/**
	* Recupera el valor del atributo Mt_z_renglon3
	* @return el valor de Mt_z_renglon3
	*/ 
	public String getMt_z_renglon3() {
		return mt_z_renglon3;
	}
    	
	/**
	* Fija el valor del atributo Mt_z_renglon3
	* @param valor el valor de Mt_z_renglon3
	*/ 
	public void setMt_z_renglon3(String valor) {
		this.mt_z_renglon3 = valor;
	}
	/**
	* Recupera el valor del atributo Mt_icc_code
	* @return el valor de Mt_icc_code
	*/ 
	public String getMt_icc_code() {
		return mt_icc_code;
	}
    	
	/**
	* Fija el valor del atributo Mt_icc_code
	* @param valor el valor de Mt_icc_code
	*/ 
	public void setMt_icc_code(String valor) {
		this.mt_icc_code = valor;
	}
	/**
	* Recupera el valor del atributo Mt_u_fecha
	* @return el valor de Mt_u_fecha
	*/ 
	public java.util.Date getMt_u_fecha() {
		return mt_u_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_u_fecha
	* @param valor el valor de Mt_u_fecha
	*/ 
	public void setMt_u_fecha(java.util.Date valor) {
		this.mt_u_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_est_medidor
	* @return el valor de Mt_est_medidor
	*/ 
	public String getMt_est_medidor() {
		return mt_est_medidor;
	}
    	
	/**
	* Fija el valor del atributo Mt_est_medidor
	* @param valor el valor de Mt_est_medidor
	*/ 
	public void setMt_est_medidor(String valor) {
		this.mt_est_medidor = valor;
	}
	/**
	* Recupera el valor del atributo Mt_linea
	* @return el valor de Mt_linea
	*/ 
	public String getMt_linea() {
		return mt_linea;
	}
    	
	/**
	* Fija el valor del atributo Mt_linea
	* @param valor el valor de Mt_linea
	*/ 
	public void setMt_linea(String valor) {
		this.mt_linea = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cons_p_interurb_br_ant
	* @return el valor de Mt_cons_p_interurb_br_ant
	*/ 
	public Double getMt_cons_p_interurb_br_ant() {
		return mt_cons_p_interurb_br_ant;
	}
    	
	/**
	* Fija el valor del atributo Mt_cons_p_interurb_br_ant
	* @param valor el valor de Mt_cons_p_interurb_br_ant
	*/ 
	public void setMt_cons_p_interurb_br_ant(Double valor) {
		this.mt_cons_p_interurb_br_ant = valor;
	}
	/**
	* Recupera el valor del atributo Mt_d_banco
	* @return el valor de Mt_d_banco
	*/ 
	public Double getMt_d_banco() {
		return mt_d_banco;
	}
    	
	/**
	* Fija el valor del atributo Mt_d_banco
	* @param valor el valor de Mt_d_banco
	*/ 
	public void setMt_d_banco(Double valor) {
		this.mt_d_banco = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ingbru_fec_vto_porc
	* @return el valor de Mt_ingbru_fec_vto_porc
	*/ 
	public Double getMt_ingbru_fec_vto_porc() {
		return mt_ingbru_fec_vto_porc;
	}
    	
	/**
	* Fija el valor del atributo Mt_ingbru_fec_vto_porc
	* @param valor el valor de Mt_ingbru_fec_vto_porc
	*/ 
	public void setMt_ingbru_fec_vto_porc(Double valor) {
		this.mt_ingbru_fec_vto_porc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_estado
	* @return el valor de Mt_estado
	*/ 
	public String getMt_estado() {
		return mt_estado;
	}
    	
	/**
	* Fija el valor del atributo Mt_estado
	* @param valor el valor de Mt_estado
	*/ 
	public void setMt_estado(String valor) {
		this.mt_estado = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ingbru_nro_nue
	* @return el valor de Mt_ingbru_nro_nue
	*/ 
	public String getMt_ingbru_nro_nue() {
		return mt_ingbru_nro_nue;
	}
    	
	/**
	* Fija el valor del atributo Mt_ingbru_nro_nue
	* @param valor el valor de Mt_ingbru_nro_nue
	*/ 
	public void setMt_ingbru_nro_nue(String valor) {
		this.mt_ingbru_nro_nue = valor;
	}
	/**
	* Recupera el valor del atributo Mt_med_corte
	* @return el valor de Mt_med_corte
	*/ 
	public Double getMt_med_corte() {
		return mt_med_corte;
	}
    	
	/**
	* Fija el valor del atributo Mt_med_corte
	* @param valor el valor de Mt_med_corte
	*/ 
	public void setMt_med_corte(Double valor) {
		this.mt_med_corte = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cons_p_urb_ne_ant
	* @return el valor de Mt_cons_p_urb_ne_ant
	*/ 
	public Double getMt_cons_p_urb_ne_ant() {
		return mt_cons_p_urb_ne_ant;
	}
    	
	/**
	* Fija el valor del atributo Mt_cons_p_urb_ne_ant
	* @param valor el valor de Mt_cons_p_urb_ne_ant
	*/ 
	public void setMt_cons_p_urb_ne_ant(Double valor) {
		this.mt_cons_p_urb_ne_ant = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cta_atis
	* @return el valor de Mt_cta_atis
	*/ 
	public Double getMt_cta_atis() {
		return mt_cta_atis;
	}
    	
	/**
	* Fija el valor del atributo Mt_cta_atis
	* @param valor el valor de Mt_cta_atis
	*/ 
	public void setMt_cta_atis(Double valor) {
		this.mt_cta_atis = valor;
	}
	/**
	* Recupera el valor del atributo Mt_i_lineas
	* @return el valor de Mt_i_lineas
	*/ 
	public Double getMt_i_lineas() {
		return mt_i_lineas;
	}
    	
	/**
	* Fija el valor del atributo Mt_i_lineas
	* @param valor el valor de Mt_i_lineas
	*/ 
	public void setMt_i_lineas(Double valor) {
		this.mt_i_lineas = valor;
	}
	/**
	* Recupera el valor del atributo Mt_abonado
	* @return el valor de Mt_abonado
	*/ 
	public Double getMt_abonado() {
		return mt_abonado;
	}
    	
	/**
	* Fija el valor del atributo Mt_abonado
	* @param valor el valor de Mt_abonado
	*/ 
	public void setMt_abonado(Double valor) {
		this.mt_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Mt_a_num_os
	* @return el valor de Mt_a_num_os
	*/ 
	public Double getMt_a_num_os() {
		return mt_a_num_os;
	}
    	
	/**
	* Fija el valor del atributo Mt_a_num_os
	* @param valor el valor de Mt_a_num_os
	*/ 
	public void setMt_a_num_os(Double valor) {
		this.mt_a_num_os = valor;
	}
	/**
	* Recupera el valor del atributo Mt_z_est_direc
	* @return el valor de Mt_z_est_direc
	*/ 
	public String getMt_z_est_direc() {
		return mt_z_est_direc;
	}
    	
	/**
	* Fija el valor del atributo Mt_z_est_direc
	* @param valor el valor de Mt_z_est_direc
	*/ 
	public void setMt_z_est_direc(String valor) {
		this.mt_z_est_direc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_marca_baja
	* @return el valor de Mt_marca_baja
	*/ 
	public String getMt_marca_baja() {
		return mt_marca_baja;
	}
    	
	/**
	* Fija el valor del atributo Mt_marca_baja
	* @param valor el valor de Mt_marca_baja
	*/ 
	public void setMt_marca_baja(String valor) {
		this.mt_marca_baja = valor;
	}
	/**
	* Recupera el valor del atributo Mt_d_cuenta
	* @return el valor de Mt_d_cuenta
	*/ 
	public String getMt_d_cuenta() {
		return mt_d_cuenta;
	}
    	
	/**
	* Fija el valor del atributo Mt_d_cuenta
	* @param valor el valor de Mt_d_cuenta
	*/ 
	public void setMt_d_cuenta(String valor) {
		this.mt_d_cuenta = valor;
	}
	/**
	* Recupera el valor del atributo Mt_tarjeta
	* @return el valor de Mt_tarjeta
	*/ 
	public Double getMt_tarjeta() {
		return mt_tarjeta;
	}
    	
	/**
	* Fija el valor del atributo Mt_tarjeta
	* @param valor el valor de Mt_tarjeta
	*/ 
	public void setMt_tarjeta(Double valor) {
		this.mt_tarjeta = valor;
	}
	/**
	* Recupera el valor del atributo Mt_v_fecha
	* @return el valor de Mt_v_fecha
	*/ 
	public Double getMt_v_fecha() {
		return mt_v_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_v_fecha
	* @param valor el valor de Mt_v_fecha
	*/ 
	public void setMt_v_fecha(Double valor) {
		this.mt_v_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fe_corte
	* @return el valor de Mt_fe_corte
	*/ 
	public Double getMt_fe_corte() {
		return mt_fe_corte;
	}
    	
	/**
	* Fija el valor del atributo Mt_fe_corte
	* @param valor el valor de Mt_fe_corte
	*/ 
	public void setMt_fe_corte(Double valor) {
		this.mt_fe_corte = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cli_atis
	* @return el valor de Mt_cli_atis
	*/ 
	public Double getMt_cli_atis() {
		return mt_cli_atis;
	}
    	
	/**
	* Fija el valor del atributo Mt_cli_atis
	* @param valor el valor de Mt_cli_atis
	*/ 
	public void setMt_cli_atis(Double valor) {
		this.mt_cli_atis = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ult_fact
	* @return el valor de Mt_ult_fact
	*/ 
	public String getMt_ult_fact() {
		return mt_ult_fact;
	}
    	
	/**
	* Fija el valor del atributo Mt_ult_fact
	* @param valor el valor de Mt_ult_fact
	*/ 
	public void setMt_ult_fact(String valor) {
		this.mt_ult_fact = valor;
	}
	/**
	* Recupera el valor del atributo Mt_urb
	* @return el valor de Mt_urb
	*/ 
	public String getMt_urb() {
		return mt_urb;
	}
    	
	/**
	* Fija el valor del atributo Mt_urb
	* @param valor el valor de Mt_urb
	*/ 
	public void setMt_urb(String valor) {
		this.mt_urb = valor;
	}
	/**
	* Recupera el valor del atributo Mt_b_num_os
	* @return el valor de Mt_b_num_os
	*/ 
	public Double getMt_b_num_os() {
		return mt_b_num_os;
	}
    	
	/**
	* Fija el valor del atributo Mt_b_num_os
	* @param valor el valor de Mt_b_num_os
	*/ 
	public void setMt_b_num_os(Double valor) {
		this.mt_b_num_os = valor;
	}
	/**
	* Recupera el valor del atributo Mt_mot_noinnovar
	* @return el valor de Mt_mot_noinnovar
	*/ 
	public String getMt_mot_noinnovar() {
		return mt_mot_noinnovar;
	}
    	
	/**
	* Fija el valor del atributo Mt_mot_noinnovar
	* @param valor el valor de Mt_mot_noinnovar
	*/ 
	public void setMt_mot_noinnovar(String valor) {
		this.mt_mot_noinnovar = valor;
	}
	/**
	* Recupera el valor del atributo Mt_form_dgi
	* @return el valor de Mt_form_dgi
	*/ 
	public String getMt_form_dgi() {
		return mt_form_dgi;
	}
    	
	/**
	* Fija el valor del atributo Mt_form_dgi
	* @param valor el valor de Mt_form_dgi
	*/ 
	public void setMt_form_dgi(String valor) {
		this.mt_form_dgi = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cons_p_urb_br_ant
	* @return el valor de Mt_cons_p_urb_br_ant
	*/ 
	public Double getMt_cons_p_urb_br_ant() {
		return mt_cons_p_urb_br_ant;
	}
    	
	/**
	* Fija el valor del atributo Mt_cons_p_urb_br_ant
	* @param valor el valor de Mt_cons_p_urb_br_ant
	*/ 
	public void setMt_cons_p_urb_br_ant(Double valor) {
		this.mt_cons_p_urb_br_ant = valor;
	}
	/**
	* Recupera el valor del atributo Mt_m_descuentos
	* @return el valor de Mt_m_descuentos
	*/ 
	public String getMt_m_descuentos() {
		return mt_m_descuentos;
	}
    	
	/**
	* Fija el valor del atributo Mt_m_descuentos
	* @param valor el valor de Mt_m_descuentos
	*/ 
	public void setMt_m_descuentos(String valor) {
		this.mt_m_descuentos = valor;
	}
	/**
	* Recupera el valor del atributo Mt_oficina
	* @return el valor de Mt_oficina
	*/ 
	public Double getMt_oficina() {
		return mt_oficina;
	}
    	
	/**
	* Fija el valor del atributo Mt_oficina
	* @param valor el valor de Mt_oficina
	*/ 
	public void setMt_oficina(Double valor) {
		this.mt_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Mt_interurb
	* @return el valor de Mt_interurb
	*/ 
	public String getMt_interurb() {
		return mt_interurb;
	}
    	
	/**
	* Fija el valor del atributo Mt_interurb
	* @param valor el valor de Mt_interurb
	*/ 
	public void setMt_interurb(String valor) {
		this.mt_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Mt_g_oficina_tr
	* @return el valor de Mt_g_oficina_tr
	*/ 
	public Double getMt_g_oficina_tr() {
		return mt_g_oficina_tr;
	}
    	
	/**
	* Fija el valor del atributo Mt_g_oficina_tr
	* @param valor el valor de Mt_g_oficina_tr
	*/ 
	public void setMt_g_oficina_tr(Double valor) {
		this.mt_g_oficina_tr = valor;
	}
	/**
	* Recupera el valor del atributo Mt_i_origen
	* @return el valor de Mt_i_origen
	*/ 
	public String getMt_i_origen() {
		return mt_i_origen;
	}
    	
	/**
	* Fija el valor del atributo Mt_i_origen
	* @param valor el valor de Mt_i_origen
	*/ 
	public void setMt_i_origen(String valor) {
		this.mt_i_origen = valor;
	}
	/**
	* Recupera el valor del atributo Mt_z_renglon1
	* @return el valor de Mt_z_renglon1
	*/ 
	public String getMt_z_renglon1() {
		return mt_z_renglon1;
	}
    	
	/**
	* Fija el valor del atributo Mt_z_renglon1
	* @param valor el valor de Mt_z_renglon1
	*/ 
	public void setMt_z_renglon1(String valor) {
		this.mt_z_renglon1 = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fecha_cat_bajo_consumo
	* @return el valor de Mt_fecha_cat_bajo_consumo
	*/ 
	public java.util.Date getMt_fecha_cat_bajo_consumo() {
		return mt_fecha_cat_bajo_consumo;
	}
    	
	/**
	* Fija el valor del atributo Mt_fecha_cat_bajo_consumo
	* @param valor el valor de Mt_fecha_cat_bajo_consumo
	*/ 
	public void setMt_fecha_cat_bajo_consumo(java.util.Date valor) {
		this.mt_fecha_cat_bajo_consumo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_contrato
	* @return el valor de Mt_contrato
	*/ 
	public String getMt_contrato() {
		return mt_contrato;
	}
    	
	/**
	* Fija el valor del atributo Mt_contrato
	* @param valor el valor de Mt_contrato
	*/ 
	public void setMt_contrato(String valor) {
		this.mt_contrato = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fecha_form_dgi_loc
	* @return el valor de Mt_fecha_form_dgi_loc
	*/ 
	public java.util.Date getMt_fecha_form_dgi_loc() {
		return mt_fecha_form_dgi_loc;
	}
    	
	/**
	* Fija el valor del atributo Mt_fecha_form_dgi_loc
	* @param valor el valor de Mt_fecha_form_dgi_loc
	*/ 
	public void setMt_fecha_form_dgi_loc(java.util.Date valor) {
		this.mt_fecha_form_dgi_loc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_x_resp_loc
	* @return el valor de Mt_x_resp_loc
	*/ 
	public Double getMt_x_resp_loc() {
		return mt_x_resp_loc;
	}
    	
	/**
	* Fija el valor del atributo Mt_x_resp_loc
	* @param valor el valor de Mt_x_resp_loc
	*/ 
	public void setMt_x_resp_loc(Double valor) {
		this.mt_x_resp_loc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_marca_aparato
	* @return el valor de Mt_marca_aparato
	*/ 
	public Double getMt_marca_aparato() {
		return mt_marca_aparato;
	}
    	
	/**
	* Fija el valor del atributo Mt_marca_aparato
	* @param valor el valor de Mt_marca_aparato
	*/ 
	public void setMt_marca_aparato(Double valor) {
		this.mt_marca_aparato = valor;
	}
	/**
	* Recupera el valor del atributo Mt_fecha_alta_db
	* @return el valor de Mt_fecha_alta_db
	*/ 
	public java.util.Date getMt_fecha_alta_db() {
		return mt_fecha_alta_db;
	}
    	
	/**
	* Fija el valor del atributo Mt_fecha_alta_db
	* @param valor el valor de Mt_fecha_alta_db
	*/ 
	public void setMt_fecha_alta_db(java.util.Date valor) {
		this.mt_fecha_alta_db = valor;
	}
	/**
	* Recupera el valor del atributo Mt_t_doc
	* @return el valor de Mt_t_doc
	*/ 
	public Double getMt_t_doc() {
		return mt_t_doc;
	}
    	
	/**
	* Fija el valor del atributo Mt_t_doc
	* @param valor el valor de Mt_t_doc
	*/ 
	public void setMt_t_doc(Double valor) {
		this.mt_t_doc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_u_num_os
	* @return el valor de Mt_u_num_os
	*/ 
	public Double getMt_u_num_os() {
		return mt_u_num_os;
	}
    	
	/**
	* Fija el valor del atributo Mt_u_num_os
	* @param valor el valor de Mt_u_num_os
	*/ 
	public void setMt_u_num_os(Double valor) {
		this.mt_u_num_os = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cta_com_tit
	* @return el valor de Mt_cta_com_tit
	*/ 
	public Double getMt_cta_com_tit() {
		return mt_cta_com_tit;
	}
    	
	/**
	* Fija el valor del atributo Mt_cta_com_tit
	* @param valor el valor de Mt_cta_com_tit
	*/ 
	public void setMt_cta_com_tit(Double valor) {
		this.mt_cta_com_tit = valor;
	}
	/**
	* Recupera el valor del atributo Mt_x_fecha
	* @return el valor de Mt_x_fecha
	*/ 
	public java.util.Date getMt_x_fecha() {
		return mt_x_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_x_fecha
	* @param valor el valor de Mt_x_fecha
	*/ 
	public void setMt_x_fecha(java.util.Date valor) {
		this.mt_x_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_locatario
	* @return el valor de Mt_locatario
	*/ 
	public String getMt_locatario() {
		return mt_locatario;
	}
    	
	/**
	* Fija el valor del atributo Mt_locatario
	* @param valor el valor de Mt_locatario
	*/ 
	public void setMt_locatario(String valor) {
		this.mt_locatario = valor;
	}
	/**
	* Recupera el valor del atributo Mt_megat_serie
	* @return el valor de Mt_megat_serie
	*/ 
	public String getMt_megat_serie() {
		return mt_megat_serie;
	}
    	
	/**
	* Fija el valor del atributo Mt_megat_serie
	* @param valor el valor de Mt_megat_serie
	*/ 
	public void setMt_megat_serie(String valor) {
		this.mt_megat_serie = valor;
	}
	/**
	* Recupera el valor del atributo Mt_x_resp
	* @return el valor de Mt_x_resp
	*/ 
	public Double getMt_x_resp() {
		return mt_x_resp;
	}
    	
	/**
	* Fija el valor del atributo Mt_x_resp
	* @param valor el valor de Mt_x_resp
	*/ 
	public void setMt_x_resp(Double valor) {
		this.mt_x_resp = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ingbru_tipo_inscr
	* @return el valor de Mt_ingbru_tipo_inscr
	*/ 
	public String getMt_ingbru_tipo_inscr() {
		return mt_ingbru_tipo_inscr;
	}
    	
	/**
	* Fija el valor del atributo Mt_ingbru_tipo_inscr
	* @param valor el valor de Mt_ingbru_tipo_inscr
	*/ 
	public void setMt_ingbru_tipo_inscr(String valor) {
		this.mt_ingbru_tipo_inscr = valor;
	}
	/**
	* Recupera el valor del atributo Mt_g_zona_tr
	* @return el valor de Mt_g_zona_tr
	*/ 
	public Double getMt_g_zona_tr() {
		return mt_g_zona_tr;
	}
    	
	/**
	* Fija el valor del atributo Mt_g_zona_tr
	* @param valor el valor de Mt_g_zona_tr
	*/ 
	public void setMt_g_zona_tr(Double valor) {
		this.mt_g_zona_tr = valor;
	}
	/**
	* Recupera el valor del atributo Mt_nro_arcom
	* @return el valor de Mt_nro_arcom
	*/ 
	public Double getMt_nro_arcom() {
		return mt_nro_arcom;
	}
    	
	/**
	* Fija el valor del atributo Mt_nro_arcom
	* @param valor el valor de Mt_nro_arcom
	*/ 
	public void setMt_nro_arcom(Double valor) {
		this.mt_nro_arcom = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ani
	* @return el valor de Mt_ani
	*/ 
	public String getMt_ani() {
		return mt_ani;
	}
    	
	/**
	* Fija el valor del atributo Mt_ani
	* @param valor el valor de Mt_ani
	*/ 
	public void setMt_ani(String valor) {
		this.mt_ani = valor;
	}
	/**
	* Recupera el valor del atributo Mt_tipo_alta
	* @return el valor de Mt_tipo_alta
	*/ 
	public Double getMt_tipo_alta() {
		return mt_tipo_alta;
	}
    	
	/**
	* Fija el valor del atributo Mt_tipo_alta
	* @param valor el valor de Mt_tipo_alta
	*/ 
	public void setMt_tipo_alta(Double valor) {
		this.mt_tipo_alta = valor;
	}
	/**
	* Recupera el valor del atributo Mt_i_act_abono
	* @return el valor de Mt_i_act_abono
	*/ 
	public String getMt_i_act_abono() {
		return mt_i_act_abono;
	}
    	
	/**
	* Fija el valor del atributo Mt_i_act_abono
	* @param valor el valor de Mt_i_act_abono
	*/ 
	public void setMt_i_act_abono(String valor) {
		this.mt_i_act_abono = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ingbru_porc_desc
	* @return el valor de Mt_ingbru_porc_desc
	*/ 
	public Double getMt_ingbru_porc_desc() {
		return mt_ingbru_porc_desc;
	}
    	
	/**
	* Fija el valor del atributo Mt_ingbru_porc_desc
	* @param valor el valor de Mt_ingbru_porc_desc
	*/ 
	public void setMt_ingbru_porc_desc(Double valor) {
		this.mt_ingbru_porc_desc = valor;
	}
	/**
	* Recupera el valor del atributo Mt_g_abonado_tr
	* @return el valor de Mt_g_abonado_tr
	*/ 
	public Double getMt_g_abonado_tr() {
		return mt_g_abonado_tr;
	}
    	
	/**
	* Fija el valor del atributo Mt_g_abonado_tr
	* @param valor el valor de Mt_g_abonado_tr
	*/ 
	public void setMt_g_abonado_tr(Double valor) {
		this.mt_g_abonado_tr = valor;
	}
	/**
	* Recupera el valor del atributo Mt_j_exped
	* @return el valor de Mt_j_exped
	*/ 
	public String getMt_j_exped() {
		return mt_j_exped;
	}
    	
	/**
	* Fija el valor del atributo Mt_j_exped
	* @param valor el valor de Mt_j_exped
	*/ 
	public void setMt_j_exped(String valor) {
		this.mt_j_exped = valor;
	}
	/**
	* Recupera el valor del atributo Mt_bloque2_cbu
	* @return el valor de Mt_bloque2_cbu
	*/ 
	public java.math.BigDecimal getMt_bloque2_cbu() {
		return mt_bloque2_cbu;
	}
    	
	/**
	* Fija el valor del atributo Mt_bloque2_cbu
	* @param valor el valor de Mt_bloque2_cbu
	*/ 
	public void setMt_bloque2_cbu(java.math.BigDecimal valor) {
		this.mt_bloque2_cbu = valor;
	}
	/**
	* Recupera el valor del atributo Mt_j_tipo
	* @return el valor de Mt_j_tipo
	*/ 
	public String getMt_j_tipo() {
		return mt_j_tipo;
	}
    	
	/**
	* Fija el valor del atributo Mt_j_tipo
	* @param valor el valor de Mt_j_tipo
	*/ 
	public void setMt_j_tipo(String valor) {
		this.mt_j_tipo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_feh_ult_abono
	* @return el valor de Mt_feh_ult_abono
	*/ 
	public Double getMt_feh_ult_abono() {
		return mt_feh_ult_abono;
	}
    	
	/**
	* Fija el valor del atributo Mt_feh_ult_abono
	* @param valor el valor de Mt_feh_ult_abono
	*/ 
	public void setMt_feh_ult_abono(Double valor) {
		this.mt_feh_ult_abono = valor;
	}
	/**
	* Recupera el valor del atributo Mt_z_sobretasa
	* @return el valor de Mt_z_sobretasa
	*/ 
	public Double getMt_z_sobretasa() {
		return mt_z_sobretasa;
	}
    	
	/**
	* Fija el valor del atributo Mt_z_sobretasa
	* @param valor el valor de Mt_z_sobretasa
	*/ 
	public void setMt_z_sobretasa(Double valor) {
		this.mt_z_sobretasa = valor;
	}
	/**
	* Recupera el valor del atributo Mt_form_dgi_loc
	* @return el valor de Mt_form_dgi_loc
	*/ 
	public String getMt_form_dgi_loc() {
		return mt_form_dgi_loc;
	}
    	
	/**
	* Fija el valor del atributo Mt_form_dgi_loc
	* @param valor el valor de Mt_form_dgi_loc
	*/ 
	public void setMt_form_dgi_loc(String valor) {
		this.mt_form_dgi_loc = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Mt_origen=["+this.getMt_origen()+"]\r\n");
				sb.append("Mt_cta_com_usu=["+this.getMt_cta_com_usu()+"]\r\n");
				sb.append("Mt_venc=["+this.getMt_venc()+"]\r\n");
				sb.append("Mt_benef_jub=["+this.getMt_benef_jub()+"]\r\n");
				sb.append("Mt_alta_pura=["+this.getMt_alta_pura()+"]\r\n");
				sb.append("Mt_bloque1_cbu=["+this.getMt_bloque1_cbu()+"]\r\n");
				sb.append("Mt_motivo_baja=["+this.getMt_motivo_baja()+"]\r\n");
				sb.append("Mt_d_t_cta=["+this.getMt_d_t_cta()+"]\r\n");
				sb.append("Mt_fecha_vto_porc_loc=["+this.getMt_fecha_vto_porc_loc()+"]\r\n");
				sb.append("Mt_b_fecha=["+this.getMt_b_fecha()+"]\r\n");
				sb.append("Mt_n_doc=["+this.getMt_n_doc()+"]\r\n");
				sb.append("Mt_muestra_estadistica=["+this.getMt_muestra_estadistica()+"]\r\n");
				sb.append("Mt_ident_abo=["+this.getMt_ident_abo()+"]\r\n");
				sb.append("Mt_fecha_form_dgi=["+this.getMt_fecha_form_dgi()+"]\r\n");
				sb.append("Mt_cons_p_interurb_ne_ant=["+this.getMt_cons_p_interurb_ne_ant()+"]\r\n");
				sb.append("Mt_i_resp_ant=["+this.getMt_i_resp_ant()+"]\r\n");
				sb.append("Mt_legajo=["+this.getMt_legajo()+"]\r\n");
				sb.append("Mt_fed_ult_abono=["+this.getMt_fed_ult_abono()+"]\r\n");
				sb.append("Mt_nombre_loc=["+this.getMt_nombre_loc()+"]\r\n");
				sb.append("Mt_grupo_atis=["+this.getMt_grupo_atis()+"]\r\n");
				sb.append("Mt_x_cuit=["+this.getMt_x_cuit()+"]\r\n");
				sb.append("Mt_fecha_cdn=["+this.getMt_fecha_cdn()+"]\r\n");
				sb.append("Mt_x_cuit_loc=["+this.getMt_x_cuit_loc()+"]\r\n");
				sb.append("Mt_ingbru_nro_vie=["+this.getMt_ingbru_nro_vie()+"]\r\n");
				sb.append("Mt_megat_adher=["+this.getMt_megat_adher()+"]\r\n");
				sb.append("Mt_z_renglon2=["+this.getMt_z_renglon2()+"]\r\n");
				sb.append("Mt_a_fecha=["+this.getMt_a_fecha()+"]\r\n");
				sb.append("Mt_bajo_consumo=["+this.getMt_bajo_consumo()+"]\r\n");
				sb.append("Mt_nombre_1=["+this.getMt_nombre_1()+"]\r\n");
				sb.append("Mt_fecha_vto_porc=["+this.getMt_fecha_vto_porc()+"]\r\n");
				sb.append("Mt_f_hasta_consumo=["+this.getMt_f_hasta_consumo()+"]\r\n");
				sb.append("Mt_tipo_linea=["+this.getMt_tipo_linea()+"]\r\n");
				sb.append("Mt_z_resguardo=["+this.getMt_z_resguardo()+"]\r\n");
				sb.append("Mt_icc_code_usu=["+this.getMt_icc_code_usu()+"]\r\n");
				sb.append("Mt_porc_percep_loc=["+this.getMt_porc_percep_loc()+"]\r\n");
				sb.append("Mt_j_fecha=["+this.getMt_j_fecha()+"]\r\n");
				sb.append("Mt_z_ind_8100=["+this.getMt_z_ind_8100()+"]\r\n");
				sb.append("Mt_d_orig_db=["+this.getMt_d_orig_db()+"]\r\n");
				sb.append("Mt_clase_fact=["+this.getMt_clase_fact()+"]\r\n");
				sb.append("Mt_cant_cuotas=["+this.getMt_cant_cuotas()+"]\r\n");
				sb.append("Mt_zona=["+this.getMt_zona()+"]\r\n");
				sb.append("Mt_f_baja_asoc=["+this.getMt_f_baja_asoc()+"]\r\n");
				sb.append("Mt_i_ult_abono=["+this.getMt_i_ult_abono()+"]\r\n");
				sb.append("Mt_porc_percep=["+this.getMt_porc_percep()+"]\r\n");
				sb.append("Mt_categ=["+this.getMt_categ()+"]\r\n");
				sb.append("Mt_cargo_conexion=["+this.getMt_cargo_conexion()+"]\r\n");
				sb.append("Mt_cta_fact=["+this.getMt_cta_fact()+"]\r\n");
				sb.append("Mt_f_alta_asoc=["+this.getMt_f_alta_asoc()+"]\r\n");
				sb.append("Mt_i_medido=["+this.getMt_i_medido()+"]\r\n");
				sb.append("Mt_scoring_pago=["+this.getMt_scoring_pago()+"]\r\n");
				sb.append("Mt_febj_vto=["+this.getMt_febj_vto()+"]\r\n");
				sb.append("Mt_fed_abono_ant=["+this.getMt_fed_abono_ant()+"]\r\n");
				sb.append("Mt_z_renglon3=["+this.getMt_z_renglon3()+"]\r\n");
				sb.append("Mt_icc_code=["+this.getMt_icc_code()+"]\r\n");
				sb.append("Mt_u_fecha=["+this.getMt_u_fecha()+"]\r\n");
				sb.append("Mt_est_medidor=["+this.getMt_est_medidor()+"]\r\n");
				sb.append("Mt_linea=["+this.getMt_linea()+"]\r\n");
				sb.append("Mt_cons_p_interurb_br_ant=["+this.getMt_cons_p_interurb_br_ant()+"]\r\n");
				sb.append("Mt_d_banco=["+this.getMt_d_banco()+"]\r\n");
				sb.append("Mt_ingbru_fec_vto_porc=["+this.getMt_ingbru_fec_vto_porc()+"]\r\n");
				sb.append("Mt_estado=["+this.getMt_estado()+"]\r\n");
				sb.append("Mt_ingbru_nro_nue=["+this.getMt_ingbru_nro_nue()+"]\r\n");
				sb.append("Mt_med_corte=["+this.getMt_med_corte()+"]\r\n");
				sb.append("Mt_cons_p_urb_ne_ant=["+this.getMt_cons_p_urb_ne_ant()+"]\r\n");
				sb.append("Mt_cta_atis=["+this.getMt_cta_atis()+"]\r\n");
				sb.append("Mt_i_lineas=["+this.getMt_i_lineas()+"]\r\n");
				sb.append("Mt_abonado=["+this.getMt_abonado()+"]\r\n");
				sb.append("Mt_a_num_os=["+this.getMt_a_num_os()+"]\r\n");
				sb.append("Mt_z_est_direc=["+this.getMt_z_est_direc()+"]\r\n");
				sb.append("Mt_marca_baja=["+this.getMt_marca_baja()+"]\r\n");
				sb.append("Mt_d_cuenta=["+this.getMt_d_cuenta()+"]\r\n");
				sb.append("Mt_tarjeta=["+this.getMt_tarjeta()+"]\r\n");
				sb.append("Mt_v_fecha=["+this.getMt_v_fecha()+"]\r\n");
				sb.append("Mt_fe_corte=["+this.getMt_fe_corte()+"]\r\n");
				sb.append("Mt_cli_atis=["+this.getMt_cli_atis()+"]\r\n");
				sb.append("Mt_ult_fact=["+this.getMt_ult_fact()+"]\r\n");
				sb.append("Mt_urb=["+this.getMt_urb()+"]\r\n");
				sb.append("Mt_b_num_os=["+this.getMt_b_num_os()+"]\r\n");
				sb.append("Mt_mot_noinnovar=["+this.getMt_mot_noinnovar()+"]\r\n");
				sb.append("Mt_form_dgi=["+this.getMt_form_dgi()+"]\r\n");
				sb.append("Mt_cons_p_urb_br_ant=["+this.getMt_cons_p_urb_br_ant()+"]\r\n");
				sb.append("Mt_m_descuentos=["+this.getMt_m_descuentos()+"]\r\n");
				sb.append("Mt_oficina=["+this.getMt_oficina()+"]\r\n");
				sb.append("Mt_interurb=["+this.getMt_interurb()+"]\r\n");
				sb.append("Mt_g_oficina_tr=["+this.getMt_g_oficina_tr()+"]\r\n");
				sb.append("Mt_i_origen=["+this.getMt_i_origen()+"]\r\n");
				sb.append("Mt_z_renglon1=["+this.getMt_z_renglon1()+"]\r\n");
				sb.append("Mt_fecha_cat_bajo_consumo=["+this.getMt_fecha_cat_bajo_consumo()+"]\r\n");
				sb.append("Mt_contrato=["+this.getMt_contrato()+"]\r\n");
				sb.append("Mt_fecha_form_dgi_loc=["+this.getMt_fecha_form_dgi_loc()+"]\r\n");
				sb.append("Mt_x_resp_loc=["+this.getMt_x_resp_loc()+"]\r\n");
				sb.append("Mt_marca_aparato=["+this.getMt_marca_aparato()+"]\r\n");
				sb.append("Mt_fecha_alta_db=["+this.getMt_fecha_alta_db()+"]\r\n");
				sb.append("Mt_t_doc=["+this.getMt_t_doc()+"]\r\n");
				sb.append("Mt_u_num_os=["+this.getMt_u_num_os()+"]\r\n");
				sb.append("Mt_cta_com_tit=["+this.getMt_cta_com_tit()+"]\r\n");
				sb.append("Mt_x_fecha=["+this.getMt_x_fecha()+"]\r\n");
				sb.append("Mt_locatario=["+this.getMt_locatario()+"]\r\n");
				sb.append("Mt_megat_serie=["+this.getMt_megat_serie()+"]\r\n");
				sb.append("Mt_x_resp=["+this.getMt_x_resp()+"]\r\n");
				sb.append("Mt_ingbru_tipo_inscr=["+this.getMt_ingbru_tipo_inscr()+"]\r\n");
				sb.append("Mt_g_zona_tr=["+this.getMt_g_zona_tr()+"]\r\n");
				sb.append("Mt_nro_arcom=["+this.getMt_nro_arcom()+"]\r\n");
				sb.append("Mt_ani=["+this.getMt_ani()+"]\r\n");
				sb.append("Mt_tipo_alta=["+this.getMt_tipo_alta()+"]\r\n");
				sb.append("Mt_i_act_abono=["+this.getMt_i_act_abono()+"]\r\n");
				sb.append("Mt_ingbru_porc_desc=["+this.getMt_ingbru_porc_desc()+"]\r\n");
				sb.append("Mt_g_abonado_tr=["+this.getMt_g_abonado_tr()+"]\r\n");
				sb.append("Mt_j_exped=["+this.getMt_j_exped()+"]\r\n");
				sb.append("Mt_bloque2_cbu=["+this.getMt_bloque2_cbu()+"]\r\n");
				sb.append("Mt_j_tipo=["+this.getMt_j_tipo()+"]\r\n");
				sb.append("Mt_feh_ult_abono=["+this.getMt_feh_ult_abono()+"]\r\n");
				sb.append("Mt_z_sobretasa=["+this.getMt_z_sobretasa()+"]\r\n");
				sb.append("Mt_form_dgi_loc=["+this.getMt_form_dgi_loc()+"]\r\n");
				return sb.toString();
	}
}