package arg.ros.acc.vo.equiposyservicios;

/**
* FcMTEQSUVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FcMTEQSUVO {

	private Double adabas_isn;
	private String mt_estado;
	private Double mt_b_num_os;
	private Double mt_a_carga;
	private Double mt_cpost;
	private Double mt_cant_tope;
	private Double mt_ident_abo;
	private String mt_interurb;
	private Double mt_abonado;
	private String mt_t_calle;
	private String mt_cod_postal;
	private String mt_calle;
	private String mt_linea_gral;
	private Double mt_a_num_os;
	private Double mt_b_carga;
	private String mt_num;
	private String mt_ofic_dist;
	private String mt_urb;
	private Double mt_cod_eq;
	private Double mt_zona;
	private java.util.Date mt_b_fecha;
	private String mt_i_origen;
	private String mt_ind_2;
	private Double mt_oficina;
	private String mt_domic1_notif;
	private String mt_descto;
	private Double mt_cant;
	private String mt_linea;
	private String mt_domic3_notif;
	private java.util.Date mt_a_fecha;
	private Double mt_prov;
	private String mt_loca;
	private String mt_ind_1;
	private Double mt_id_sec;
	private String mt_domic2_notif;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FcMTEQSUVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Mt_estado
	* @return el valor de Mt_estado
	*/ 
	public String getMt_estado() {
		return mt_estado;
	}
    	
	/**
	* Fija el valor del atributo Mt_estado
	* @param valor el valor de Mt_estado
	*/ 
	public void setMt_estado(String valor) {
		this.mt_estado = valor;
	}
	/**
	* Recupera el valor del atributo Mt_b_num_os
	* @return el valor de Mt_b_num_os
	*/ 
	public Double getMt_b_num_os() {
		return mt_b_num_os;
	}
    	
	/**
	* Fija el valor del atributo Mt_b_num_os
	* @param valor el valor de Mt_b_num_os
	*/ 
	public void setMt_b_num_os(Double valor) {
		this.mt_b_num_os = valor;
	}
	/**
	* Recupera el valor del atributo Mt_a_carga
	* @return el valor de Mt_a_carga
	*/ 
	public Double getMt_a_carga() {
		return mt_a_carga;
	}
    	
	/**
	* Fija el valor del atributo Mt_a_carga
	* @param valor el valor de Mt_a_carga
	*/ 
	public void setMt_a_carga(Double valor) {
		this.mt_a_carga = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cpost
	* @return el valor de Mt_cpost
	*/ 
	public Double getMt_cpost() {
		return mt_cpost;
	}
    	
	/**
	* Fija el valor del atributo Mt_cpost
	* @param valor el valor de Mt_cpost
	*/ 
	public void setMt_cpost(Double valor) {
		this.mt_cpost = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cant_tope
	* @return el valor de Mt_cant_tope
	*/ 
	public Double getMt_cant_tope() {
		return mt_cant_tope;
	}
    	
	/**
	* Fija el valor del atributo Mt_cant_tope
	* @param valor el valor de Mt_cant_tope
	*/ 
	public void setMt_cant_tope(Double valor) {
		this.mt_cant_tope = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ident_abo
	* @return el valor de Mt_ident_abo
	*/ 
	public Double getMt_ident_abo() {
		return mt_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Mt_ident_abo
	* @param valor el valor de Mt_ident_abo
	*/ 
	public void setMt_ident_abo(Double valor) {
		this.mt_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Mt_interurb
	* @return el valor de Mt_interurb
	*/ 
	public String getMt_interurb() {
		return mt_interurb;
	}
    	
	/**
	* Fija el valor del atributo Mt_interurb
	* @param valor el valor de Mt_interurb
	*/ 
	public void setMt_interurb(String valor) {
		this.mt_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Mt_abonado
	* @return el valor de Mt_abonado
	*/ 
	public Double getMt_abonado() {
		return mt_abonado;
	}
    	
	/**
	* Fija el valor del atributo Mt_abonado
	* @param valor el valor de Mt_abonado
	*/ 
	public void setMt_abonado(Double valor) {
		this.mt_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Mt_t_calle
	* @return el valor de Mt_t_calle
	*/ 
	public String getMt_t_calle() {
		return mt_t_calle;
	}
    	
	/**
	* Fija el valor del atributo Mt_t_calle
	* @param valor el valor de Mt_t_calle
	*/ 
	public void setMt_t_calle(String valor) {
		this.mt_t_calle = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cod_postal
	* @return el valor de Mt_cod_postal
	*/ 
	public String getMt_cod_postal() {
		return mt_cod_postal;
	}
    	
	/**
	* Fija el valor del atributo Mt_cod_postal
	* @param valor el valor de Mt_cod_postal
	*/ 
	public void setMt_cod_postal(String valor) {
		this.mt_cod_postal = valor;
	}
	/**
	* Recupera el valor del atributo Mt_calle
	* @return el valor de Mt_calle
	*/ 
	public String getMt_calle() {
		return mt_calle;
	}
    	
	/**
	* Fija el valor del atributo Mt_calle
	* @param valor el valor de Mt_calle
	*/ 
	public void setMt_calle(String valor) {
		this.mt_calle = valor;
	}
	/**
	* Recupera el valor del atributo Mt_linea_gral
	* @return el valor de Mt_linea_gral
	*/ 
	public String getMt_linea_gral() {
		return mt_linea_gral;
	}
    	
	/**
	* Fija el valor del atributo Mt_linea_gral
	* @param valor el valor de Mt_linea_gral
	*/ 
	public void setMt_linea_gral(String valor) {
		this.mt_linea_gral = valor;
	}
	/**
	* Recupera el valor del atributo Mt_a_num_os
	* @return el valor de Mt_a_num_os
	*/ 
	public Double getMt_a_num_os() {
		return mt_a_num_os;
	}
    	
	/**
	* Fija el valor del atributo Mt_a_num_os
	* @param valor el valor de Mt_a_num_os
	*/ 
	public void setMt_a_num_os(Double valor) {
		this.mt_a_num_os = valor;
	}
	/**
	* Recupera el valor del atributo Mt_b_carga
	* @return el valor de Mt_b_carga
	*/ 
	public Double getMt_b_carga() {
		return mt_b_carga;
	}
    	
	/**
	* Fija el valor del atributo Mt_b_carga
	* @param valor el valor de Mt_b_carga
	*/ 
	public void setMt_b_carga(Double valor) {
		this.mt_b_carga = valor;
	}
	/**
	* Recupera el valor del atributo Mt_num
	* @return el valor de Mt_num
	*/ 
	public String getMt_num() {
		return mt_num;
	}
    	
	/**
	* Fija el valor del atributo Mt_num
	* @param valor el valor de Mt_num
	*/ 
	public void setMt_num(String valor) {
		this.mt_num = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ofic_dist
	* @return el valor de Mt_ofic_dist
	*/ 
	public String getMt_ofic_dist() {
		return mt_ofic_dist;
	}
    	
	/**
	* Fija el valor del atributo Mt_ofic_dist
	* @param valor el valor de Mt_ofic_dist
	*/ 
	public void setMt_ofic_dist(String valor) {
		this.mt_ofic_dist = valor;
	}
	/**
	* Recupera el valor del atributo Mt_urb
	* @return el valor de Mt_urb
	*/ 
	public String getMt_urb() {
		return mt_urb;
	}
    	
	/**
	* Fija el valor del atributo Mt_urb
	* @param valor el valor de Mt_urb
	*/ 
	public void setMt_urb(String valor) {
		this.mt_urb = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cod_eq
	* @return el valor de Mt_cod_eq
	*/ 
	public Double getMt_cod_eq() {
		return mt_cod_eq;
	}
    	
	/**
	* Fija el valor del atributo Mt_cod_eq
	* @param valor el valor de Mt_cod_eq
	*/ 
	public void setMt_cod_eq(Double valor) {
		this.mt_cod_eq = valor;
	}
	/**
	* Recupera el valor del atributo Mt_zona
	* @return el valor de Mt_zona
	*/ 
	public Double getMt_zona() {
		return mt_zona;
	}
    	
	/**
	* Fija el valor del atributo Mt_zona
	* @param valor el valor de Mt_zona
	*/ 
	public void setMt_zona(Double valor) {
		this.mt_zona = valor;
	}
	/**
	* Recupera el valor del atributo Mt_b_fecha
	* @return el valor de Mt_b_fecha
	*/ 
	public java.util.Date getMt_b_fecha() {
		return mt_b_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_b_fecha
	* @param valor el valor de Mt_b_fecha
	*/ 
	public void setMt_b_fecha(java.util.Date valor) {
		this.mt_b_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_i_origen
	* @return el valor de Mt_i_origen
	*/ 
	public String getMt_i_origen() {
		return mt_i_origen;
	}
    	
	/**
	* Fija el valor del atributo Mt_i_origen
	* @param valor el valor de Mt_i_origen
	*/ 
	public void setMt_i_origen(String valor) {
		this.mt_i_origen = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ind_2
	* @return el valor de Mt_ind_2
	*/ 
	public String getMt_ind_2() {
		return mt_ind_2;
	}
    	
	/**
	* Fija el valor del atributo Mt_ind_2
	* @param valor el valor de Mt_ind_2
	*/ 
	public void setMt_ind_2(String valor) {
		this.mt_ind_2 = valor;
	}
	/**
	* Recupera el valor del atributo Mt_oficina
	* @return el valor de Mt_oficina
	*/ 
	public Double getMt_oficina() {
		return mt_oficina;
	}
    	
	/**
	* Fija el valor del atributo Mt_oficina
	* @param valor el valor de Mt_oficina
	*/ 
	public void setMt_oficina(Double valor) {
		this.mt_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Mt_domic1_notif
	* @return el valor de Mt_domic1_notif
	*/ 
	public String getMt_domic1_notif() {
		return mt_domic1_notif;
	}
    	
	/**
	* Fija el valor del atributo Mt_domic1_notif
	* @param valor el valor de Mt_domic1_notif
	*/ 
	public void setMt_domic1_notif(String valor) {
		this.mt_domic1_notif = valor;
	}
	/**
	* Recupera el valor del atributo Mt_descto
	* @return el valor de Mt_descto
	*/ 
	public String getMt_descto() {
		return mt_descto;
	}
    	
	/**
	* Fija el valor del atributo Mt_descto
	* @param valor el valor de Mt_descto
	*/ 
	public void setMt_descto(String valor) {
		this.mt_descto = valor;
	}
	/**
	* Recupera el valor del atributo Mt_cant
	* @return el valor de Mt_cant
	*/ 
	public Double getMt_cant() {
		return mt_cant;
	}
    	
	/**
	* Fija el valor del atributo Mt_cant
	* @param valor el valor de Mt_cant
	*/ 
	public void setMt_cant(Double valor) {
		this.mt_cant = valor;
	}
	/**
	* Recupera el valor del atributo Mt_linea
	* @return el valor de Mt_linea
	*/ 
	public String getMt_linea() {
		return mt_linea;
	}
    	
	/**
	* Fija el valor del atributo Mt_linea
	* @param valor el valor de Mt_linea
	*/ 
	public void setMt_linea(String valor) {
		this.mt_linea = valor;
	}
	/**
	* Recupera el valor del atributo Mt_domic3_notif
	* @return el valor de Mt_domic3_notif
	*/ 
	public String getMt_domic3_notif() {
		return mt_domic3_notif;
	}
    	
	/**
	* Fija el valor del atributo Mt_domic3_notif
	* @param valor el valor de Mt_domic3_notif
	*/ 
	public void setMt_domic3_notif(String valor) {
		this.mt_domic3_notif = valor;
	}
	/**
	* Recupera el valor del atributo Mt_a_fecha
	* @return el valor de Mt_a_fecha
	*/ 
	public java.util.Date getMt_a_fecha() {
		return mt_a_fecha;
	}
    	
	/**
	* Fija el valor del atributo Mt_a_fecha
	* @param valor el valor de Mt_a_fecha
	*/ 
	public void setMt_a_fecha(java.util.Date valor) {
		this.mt_a_fecha = valor;
	}
	/**
	* Recupera el valor del atributo Mt_prov
	* @return el valor de Mt_prov
	*/ 
	public Double getMt_prov() {
		return mt_prov;
	}
    	
	/**
	* Fija el valor del atributo Mt_prov
	* @param valor el valor de Mt_prov
	*/ 
	public void setMt_prov(Double valor) {
		this.mt_prov = valor;
	}
	/**
	* Recupera el valor del atributo Mt_loca
	* @return el valor de Mt_loca
	*/ 
	public String getMt_loca() {
		return mt_loca;
	}
    	
	/**
	* Fija el valor del atributo Mt_loca
	* @param valor el valor de Mt_loca
	*/ 
	public void setMt_loca(String valor) {
		this.mt_loca = valor;
	}
	/**
	* Recupera el valor del atributo Mt_ind_1
	* @return el valor de Mt_ind_1
	*/ 
	public String getMt_ind_1() {
		return mt_ind_1;
	}
    	
	/**
	* Fija el valor del atributo Mt_ind_1
	* @param valor el valor de Mt_ind_1
	*/ 
	public void setMt_ind_1(String valor) {
		this.mt_ind_1 = valor;
	}
	/**
	* Recupera el valor del atributo Mt_id_sec
	* @return el valor de Mt_id_sec
	*/ 
	public Double getMt_id_sec() {
		return mt_id_sec;
	}
    	
	/**
	* Fija el valor del atributo Mt_id_sec
	* @param valor el valor de Mt_id_sec
	*/ 
	public void setMt_id_sec(Double valor) {
		this.mt_id_sec = valor;
	}
	/**
	* Recupera el valor del atributo Mt_domic2_notif
	* @return el valor de Mt_domic2_notif
	*/ 
	public String getMt_domic2_notif() {
		return mt_domic2_notif;
	}
    	
	/**
	* Fija el valor del atributo Mt_domic2_notif
	* @param valor el valor de Mt_domic2_notif
	*/ 
	public void setMt_domic2_notif(String valor) {
		this.mt_domic2_notif = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Mt_estado=["+this.getMt_estado()+"]\r\n");
				sb.append("Mt_b_num_os=["+this.getMt_b_num_os()+"]\r\n");
				sb.append("Mt_a_carga=["+this.getMt_a_carga()+"]\r\n");
				sb.append("Mt_cpost=["+this.getMt_cpost()+"]\r\n");
				sb.append("Mt_cant_tope=["+this.getMt_cant_tope()+"]\r\n");
				sb.append("Mt_ident_abo=["+this.getMt_ident_abo()+"]\r\n");
				sb.append("Mt_interurb=["+this.getMt_interurb()+"]\r\n");
				sb.append("Mt_abonado=["+this.getMt_abonado()+"]\r\n");
				sb.append("Mt_t_calle=["+this.getMt_t_calle()+"]\r\n");
				sb.append("Mt_cod_postal=["+this.getMt_cod_postal()+"]\r\n");
				sb.append("Mt_calle=["+this.getMt_calle()+"]\r\n");
				sb.append("Mt_linea_gral=["+this.getMt_linea_gral()+"]\r\n");
				sb.append("Mt_a_num_os=["+this.getMt_a_num_os()+"]\r\n");
				sb.append("Mt_b_carga=["+this.getMt_b_carga()+"]\r\n");
				sb.append("Mt_num=["+this.getMt_num()+"]\r\n");
				sb.append("Mt_ofic_dist=["+this.getMt_ofic_dist()+"]\r\n");
				sb.append("Mt_urb=["+this.getMt_urb()+"]\r\n");
				sb.append("Mt_cod_eq=["+this.getMt_cod_eq()+"]\r\n");
				sb.append("Mt_zona=["+this.getMt_zona()+"]\r\n");
				sb.append("Mt_b_fecha=["+this.getMt_b_fecha()+"]\r\n");
				sb.append("Mt_i_origen=["+this.getMt_i_origen()+"]\r\n");
				sb.append("Mt_ind_2=["+this.getMt_ind_2()+"]\r\n");
				sb.append("Mt_oficina=["+this.getMt_oficina()+"]\r\n");
				sb.append("Mt_domic1_notif=["+this.getMt_domic1_notif()+"]\r\n");
				sb.append("Mt_descto=["+this.getMt_descto()+"]\r\n");
				sb.append("Mt_cant=["+this.getMt_cant()+"]\r\n");
				sb.append("Mt_linea=["+this.getMt_linea()+"]\r\n");
				sb.append("Mt_domic3_notif=["+this.getMt_domic3_notif()+"]\r\n");
				sb.append("Mt_a_fecha=["+this.getMt_a_fecha()+"]\r\n");
				sb.append("Mt_prov=["+this.getMt_prov()+"]\r\n");
				sb.append("Mt_loca=["+this.getMt_loca()+"]\r\n");
				sb.append("Mt_ind_1=["+this.getMt_ind_1()+"]\r\n");
				sb.append("Mt_id_sec=["+this.getMt_id_sec()+"]\r\n");
				sb.append("Mt_domic2_notif=["+this.getMt_domic2_notif()+"]\r\n");
				return sb.toString();
	}
}