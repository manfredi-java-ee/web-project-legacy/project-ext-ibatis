package arg.ros.acc.vo.movimientoscontables;

/**
* SfaMovimientosContablesVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class SfaMovimientosContablesVO {

	private Double adabas_isn;
	private String fa_domicilio;
	private String fa_codigo_movimiento;
	private Double fa_c_estado_asiento;
	private Double fa_marca_recargo;
	private Double fa_cod_banco;
	private Double fa_nro_tarjeta;
	private String fa_hora_ingreso;
	private Double fa_abonado;
	private java.util.Date fa_fecha_desimputacion;
	private java.math.BigDecimal fa_importe_pdte_gire;
	private String fa_razon_social;
	private Double fa_oficina;
	private String fa_cod_postal;
	private String fa_interurb;
	private Double fa_tipo_fact;
	private Double fa_tipo_contrib;
	private String fa_marca_desimputacion;
	private java.util.Date fa_fecha_marca_nvo;
	private String fa_urb;
	private String fa_localidad;
	private Double fa_numero_pago;
	private String fa_fecha_proc_ctgo;
	private Double fa_c_tipo_registro;
	private Double fa_r_cajero;
	private java.util.Date fa_fecha_entrega;
	private String fa_fecha_acreditacion_nvo;
	private Double fa_tipo_contrib_nvo;
	private java.math.BigDecimal fa_total_facturado;
	private String fa_usuario_terminal;
	private Double fa_anio_lote;
	private Double fa_cod_entrega;
	private Double fa_ident_abo;
	private Double fa_zona;
	private java.math.BigDecimal fa_importe_iva;
	private java.math.BigDecimal fa_serv_medido;
	private java.math.BigDecimal fa_abono;
	private Double fa_banco_zona;
	private java.util.Date fa_fecha_ingreso_nvo;
	private String fa_linea;
	private Double fa_marca_tarea;
    
	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public SfaMovimientosContablesVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_domicilio
	* @return el valor de Fa_domicilio
	*/ 
	public String getFa_domicilio() {
		return fa_domicilio;
	}
    	
	/**
	* Fija el valor del atributo Fa_domicilio
	* @param valor el valor de Fa_domicilio
	*/ 
	public void setFa_domicilio(String valor) {
		this.fa_domicilio = valor;
	}
	/**
	* Recupera el valor del atributo Fa_codigo_movimiento
	* @return el valor de Fa_codigo_movimiento
	*/ 
	public String getFa_codigo_movimiento() {
		return fa_codigo_movimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_codigo_movimiento
	* @param valor el valor de Fa_codigo_movimiento
	*/ 
	public void setFa_codigo_movimiento(String valor) {
		this.fa_codigo_movimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_estado_asiento
	* @return el valor de Fa_c_estado_asiento
	*/ 
	public Double getFa_c_estado_asiento() {
		return fa_c_estado_asiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_estado_asiento
	* @param valor el valor de Fa_c_estado_asiento
	*/ 
	public void setFa_c_estado_asiento(Double valor) {
		this.fa_c_estado_asiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_recargo
	* @return el valor de Fa_marca_recargo
	*/ 
	public Double getFa_marca_recargo() {
		return fa_marca_recargo;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_recargo
	* @param valor el valor de Fa_marca_recargo
	*/ 
	public void setFa_marca_recargo(Double valor) {
		this.fa_marca_recargo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_banco
	* @return el valor de Fa_cod_banco
	*/ 
	public Double getFa_cod_banco() {
		return fa_cod_banco;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_banco
	* @param valor el valor de Fa_cod_banco
	*/ 
	public void setFa_cod_banco(Double valor) {
		this.fa_cod_banco = valor;
	}
	/**
	* Recupera el valor del atributo Fa_nro_tarjeta
	* @return el valor de Fa_nro_tarjeta
	*/ 
	public Double getFa_nro_tarjeta() {
		return fa_nro_tarjeta;
	}
    	
	/**
	* Fija el valor del atributo Fa_nro_tarjeta
	* @param valor el valor de Fa_nro_tarjeta
	*/ 
	public void setFa_nro_tarjeta(Double valor) {
		this.fa_nro_tarjeta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_hora_ingreso
	* @return el valor de Fa_hora_ingreso
	*/ 
	public String getFa_hora_ingreso() {
		return fa_hora_ingreso;
	}
    	
	/**
	* Fija el valor del atributo Fa_hora_ingreso
	* @param valor el valor de Fa_hora_ingreso
	*/ 
	public void setFa_hora_ingreso(String valor) {
		this.fa_hora_ingreso = valor;
	}
	/**
	* Recupera el valor del atributo Fa_abonado
	* @return el valor de Fa_abonado
	*/ 
	public Double getFa_abonado() {
		return fa_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_abonado
	* @param valor el valor de Fa_abonado
	*/ 
	public void setFa_abonado(Double valor) {
		this.fa_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_desimputacion
	* @return el valor de Fa_fecha_desimputacion
	*/ 
	public java.util.Date getFa_fecha_desimputacion() {
		return fa_fecha_desimputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_desimputacion
	* @param valor el valor de Fa_fecha_desimputacion
	*/ 
	public void setFa_fecha_desimputacion(java.util.Date valor) {
		this.fa_fecha_desimputacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_importe_pdte_gire
	* @return el valor de Fa_importe_pdte_gire
	*/ 
	public java.math.BigDecimal getFa_importe_pdte_gire() {
		return fa_importe_pdte_gire;
	}
    	
	/**
	* Fija el valor del atributo Fa_importe_pdte_gire
	* @param valor el valor de Fa_importe_pdte_gire
	*/ 
	public void setFa_importe_pdte_gire(java.math.BigDecimal valor) {
		this.fa_importe_pdte_gire = valor;
	}
	/**
	* Recupera el valor del atributo Fa_razon_social
	* @return el valor de Fa_razon_social
	*/ 
	public String getFa_razon_social() {
		return fa_razon_social;
	}
    	
	/**
	* Fija el valor del atributo Fa_razon_social
	* @param valor el valor de Fa_razon_social
	*/ 
	public void setFa_razon_social(String valor) {
		this.fa_razon_social = valor;
	}
	/**
	* Recupera el valor del atributo Fa_oficina
	* @return el valor de Fa_oficina
	*/ 
	public Double getFa_oficina() {
		return fa_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_oficina
	* @param valor el valor de Fa_oficina
	*/ 
	public void setFa_oficina(Double valor) {
		this.fa_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_postal
	* @return el valor de Fa_cod_postal
	*/ 
	public String getFa_cod_postal() {
		return fa_cod_postal;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_postal
	* @param valor el valor de Fa_cod_postal
	*/ 
	public void setFa_cod_postal(String valor) {
		this.fa_cod_postal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_interurb
	* @return el valor de Fa_interurb
	*/ 
	public String getFa_interurb() {
		return fa_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_interurb
	* @param valor el valor de Fa_interurb
	*/ 
	public void setFa_interurb(String valor) {
		this.fa_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_fact
	* @return el valor de Fa_tipo_fact
	*/ 
	public Double getFa_tipo_fact() {
		return fa_tipo_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_fact
	* @param valor el valor de Fa_tipo_fact
	*/ 
	public void setFa_tipo_fact(Double valor) {
		this.fa_tipo_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_contrib
	* @return el valor de Fa_tipo_contrib
	*/ 
	public Double getFa_tipo_contrib() {
		return fa_tipo_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_contrib
	* @param valor el valor de Fa_tipo_contrib
	*/ 
	public void setFa_tipo_contrib(Double valor) {
		this.fa_tipo_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_desimputacion
	* @return el valor de Fa_marca_desimputacion
	*/ 
	public String getFa_marca_desimputacion() {
		return fa_marca_desimputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_desimputacion
	* @param valor el valor de Fa_marca_desimputacion
	*/ 
	public void setFa_marca_desimputacion(String valor) {
		this.fa_marca_desimputacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_marca_nvo
	* @return el valor de Fa_fecha_marca_nvo
	*/ 
	public java.util.Date getFa_fecha_marca_nvo() {
		return fa_fecha_marca_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_marca_nvo
	* @param valor el valor de Fa_fecha_marca_nvo
	*/ 
	public void setFa_fecha_marca_nvo(java.util.Date valor) {
		this.fa_fecha_marca_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_urb
	* @return el valor de Fa_urb
	*/ 
	public String getFa_urb() {
		return fa_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_urb
	* @param valor el valor de Fa_urb
	*/ 
	public void setFa_urb(String valor) {
		this.fa_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_localidad
	* @return el valor de Fa_localidad
	*/ 
	public String getFa_localidad() {
		return fa_localidad;
	}
    	
	/**
	* Fija el valor del atributo Fa_localidad
	* @param valor el valor de Fa_localidad
	*/ 
	public void setFa_localidad(String valor) {
		this.fa_localidad = valor;
	}
	/**
	* Recupera el valor del atributo Fa_numero_pago
	* @return el valor de Fa_numero_pago
	*/ 
	public Double getFa_numero_pago() {
		return fa_numero_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_numero_pago
	* @param valor el valor de Fa_numero_pago
	*/ 
	public void setFa_numero_pago(Double valor) {
		this.fa_numero_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_proc_ctgo
	* @return el valor de Fa_fecha_proc_ctgo
	*/ 
	public String getFa_fecha_proc_ctgo() {
		return fa_fecha_proc_ctgo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_proc_ctgo
	* @param valor el valor de Fa_fecha_proc_ctgo
	*/ 
	public void setFa_fecha_proc_ctgo(String valor) {
		this.fa_fecha_proc_ctgo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_tipo_registro
	* @return el valor de Fa_c_tipo_registro
	*/ 
	public Double getFa_c_tipo_registro() {
		return fa_c_tipo_registro;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_tipo_registro
	* @param valor el valor de Fa_c_tipo_registro
	*/ 
	public void setFa_c_tipo_registro(Double valor) {
		this.fa_c_tipo_registro = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_cajero
	* @return el valor de Fa_r_cajero
	*/ 
	public Double getFa_r_cajero() {
		return fa_r_cajero;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_cajero
	* @param valor el valor de Fa_r_cajero
	*/ 
	public void setFa_r_cajero(Double valor) {
		this.fa_r_cajero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_entrega
	* @return el valor de Fa_fecha_entrega
	*/ 
	public java.util.Date getFa_fecha_entrega() {
		return fa_fecha_entrega;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_entrega
	* @param valor el valor de Fa_fecha_entrega
	*/ 
	public void setFa_fecha_entrega(java.util.Date valor) {
		this.fa_fecha_entrega = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_acreditacion_nvo
	* @return el valor de Fa_fecha_acreditacion_nvo
	*/ 
	public String getFa_fecha_acreditacion_nvo() {
		return fa_fecha_acreditacion_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_acreditacion_nvo
	* @param valor el valor de Fa_fecha_acreditacion_nvo
	*/ 
	public void setFa_fecha_acreditacion_nvo(String valor) {
		this.fa_fecha_acreditacion_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_contrib_nvo
	* @return el valor de Fa_tipo_contrib_nvo
	*/ 
	public Double getFa_tipo_contrib_nvo() {
		return fa_tipo_contrib_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_contrib_nvo
	* @param valor el valor de Fa_tipo_contrib_nvo
	*/ 
	public void setFa_tipo_contrib_nvo(Double valor) {
		this.fa_tipo_contrib_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_total_facturado
	* @return el valor de Fa_total_facturado
	*/ 
	public java.math.BigDecimal getFa_total_facturado() {
		return fa_total_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_total_facturado
	* @param valor el valor de Fa_total_facturado
	*/ 
	public void setFa_total_facturado(java.math.BigDecimal valor) {
		this.fa_total_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_usuario_terminal
	* @return el valor de Fa_usuario_terminal
	*/ 
	public String getFa_usuario_terminal() {
		return fa_usuario_terminal;
	}
    	
	/**
	* Fija el valor del atributo Fa_usuario_terminal
	* @param valor el valor de Fa_usuario_terminal
	*/ 
	public void setFa_usuario_terminal(String valor) {
		this.fa_usuario_terminal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_anio_lote
	* @return el valor de Fa_anio_lote
	*/ 
	public Double getFa_anio_lote() {
		return fa_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_anio_lote
	* @param valor el valor de Fa_anio_lote
	*/ 
	public void setFa_anio_lote(Double valor) {
		this.fa_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_entrega
	* @return el valor de Fa_cod_entrega
	*/ 
	public Double getFa_cod_entrega() {
		return fa_cod_entrega;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_entrega
	* @param valor el valor de Fa_cod_entrega
	*/ 
	public void setFa_cod_entrega(Double valor) {
		this.fa_cod_entrega = valor;
	}
	/**
	* Recupera el valor del atributo Fa_ident_abo
	* @return el valor de Fa_ident_abo
	*/ 
	public Double getFa_ident_abo() {
		return fa_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_ident_abo
	* @param valor el valor de Fa_ident_abo
	*/ 
	public void setFa_ident_abo(Double valor) {
		this.fa_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_zona
	* @return el valor de Fa_zona
	*/ 
	public Double getFa_zona() {
		return fa_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_zona
	* @param valor el valor de Fa_zona
	*/ 
	public void setFa_zona(Double valor) {
		this.fa_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_importe_iva
	* @return el valor de Fa_importe_iva
	*/ 
	public java.math.BigDecimal getFa_importe_iva() {
		return fa_importe_iva;
	}
    	
	/**
	* Fija el valor del atributo Fa_importe_iva
	* @param valor el valor de Fa_importe_iva
	*/ 
	public void setFa_importe_iva(java.math.BigDecimal valor) {
		this.fa_importe_iva = valor;
	}
	/**
	* Recupera el valor del atributo Fa_serv_medido
	* @return el valor de Fa_serv_medido
	*/ 
	public java.math.BigDecimal getFa_serv_medido() {
		return fa_serv_medido;
	}
    	
	/**
	* Fija el valor del atributo Fa_serv_medido
	* @param valor el valor de Fa_serv_medido
	*/ 
	public void setFa_serv_medido(java.math.BigDecimal valor) {
		this.fa_serv_medido = valor;
	}
	/**
	* Recupera el valor del atributo Fa_abono
	* @return el valor de Fa_abono
	*/ 
	public java.math.BigDecimal getFa_abono() {
		return fa_abono;
	}
    	
	/**
	* Fija el valor del atributo Fa_abono
	* @param valor el valor de Fa_abono
	*/ 
	public void setFa_abono(java.math.BigDecimal valor) {
		this.fa_abono = valor;
	}
	/**
	* Recupera el valor del atributo Fa_banco_zona
	* @return el valor de Fa_banco_zona
	*/ 
	public Double getFa_banco_zona() {
		return fa_banco_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_banco_zona
	* @param valor el valor de Fa_banco_zona
	*/ 
	public void setFa_banco_zona(Double valor) {
		this.fa_banco_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_ingreso_nvo
	* @return el valor de Fa_fecha_ingreso_nvo
	*/ 
	public java.util.Date getFa_fecha_ingreso_nvo() {
		return fa_fecha_ingreso_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_ingreso_nvo
	* @param valor el valor de Fa_fecha_ingreso_nvo
	*/ 
	public void setFa_fecha_ingreso_nvo(java.util.Date valor) {
		this.fa_fecha_ingreso_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_linea
	* @return el valor de Fa_linea
	*/ 
	public String getFa_linea() {
		return fa_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_linea
	* @param valor el valor de Fa_linea
	*/ 
	public void setFa_linea(String valor) {
		this.fa_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_tarea
	* @return el valor de Fa_marca_tarea
	*/ 
	public Double getFa_marca_tarea() {
		return fa_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_tarea
	* @param valor el valor de Fa_marca_tarea
	*/ 
	public void setFa_marca_tarea(Double valor) {
		this.fa_marca_tarea = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Fa_domicilio=["+this.getFa_domicilio()+"]\r\n");
				sb.append("Fa_codigo_movimiento=["+this.getFa_codigo_movimiento()+"]\r\n");
				sb.append("Fa_c_estado_asiento=["+this.getFa_c_estado_asiento()+"]\r\n");
				sb.append("Fa_marca_recargo=["+this.getFa_marca_recargo()+"]\r\n");
				sb.append("Fa_cod_banco=["+this.getFa_cod_banco()+"]\r\n");
				sb.append("Fa_nro_tarjeta=["+this.getFa_nro_tarjeta()+"]\r\n");
				sb.append("Fa_hora_ingreso=["+this.getFa_hora_ingreso()+"]\r\n");
				sb.append("Fa_abonado=["+this.getFa_abonado()+"]\r\n");
				sb.append("Fa_fecha_desimputacion=["+this.getFa_fecha_desimputacion()+"]\r\n");
				sb.append("Fa_importe_pdte_gire=["+this.getFa_importe_pdte_gire()+"]\r\n");
				sb.append("Fa_razon_social=["+this.getFa_razon_social()+"]\r\n");
				sb.append("Fa_oficina=["+this.getFa_oficina()+"]\r\n");
				sb.append("Fa_cod_postal=["+this.getFa_cod_postal()+"]\r\n");
				sb.append("Fa_interurb=["+this.getFa_interurb()+"]\r\n");
				sb.append("Fa_tipo_fact=["+this.getFa_tipo_fact()+"]\r\n");
				sb.append("Fa_tipo_contrib=["+this.getFa_tipo_contrib()+"]\r\n");
				sb.append("Fa_marca_desimputacion=["+this.getFa_marca_desimputacion()+"]\r\n");
				sb.append("Fa_fecha_marca_nvo=["+this.getFa_fecha_marca_nvo()+"]\r\n");
				sb.append("Fa_urb=["+this.getFa_urb()+"]\r\n");
				sb.append("Fa_localidad=["+this.getFa_localidad()+"]\r\n");
				sb.append("Fa_numero_pago=["+this.getFa_numero_pago()+"]\r\n");
				sb.append("Fa_fecha_proc_ctgo=["+this.getFa_fecha_proc_ctgo()+"]\r\n");
				sb.append("Fa_c_tipo_registro=["+this.getFa_c_tipo_registro()+"]\r\n");
				sb.append("Fa_r_cajero=["+this.getFa_r_cajero()+"]\r\n");
				sb.append("Fa_fecha_entrega=["+this.getFa_fecha_entrega()+"]\r\n");
				sb.append("Fa_fecha_acreditacion_nvo=["+this.getFa_fecha_acreditacion_nvo()+"]\r\n");
				sb.append("Fa_tipo_contrib_nvo=["+this.getFa_tipo_contrib_nvo()+"]\r\n");
				sb.append("Fa_total_facturado=["+this.getFa_total_facturado()+"]\r\n");
				sb.append("Fa_usuario_terminal=["+this.getFa_usuario_terminal()+"]\r\n");
				sb.append("Fa_anio_lote=["+this.getFa_anio_lote()+"]\r\n");
				sb.append("Fa_cod_entrega=["+this.getFa_cod_entrega()+"]\r\n");
				sb.append("Fa_ident_abo=["+this.getFa_ident_abo()+"]\r\n");
				sb.append("Fa_zona=["+this.getFa_zona()+"]\r\n");
				sb.append("Fa_importe_iva=["+this.getFa_importe_iva()+"]\r\n");
				sb.append("Fa_serv_medido=["+this.getFa_serv_medido()+"]\r\n");
				sb.append("Fa_abono=["+this.getFa_abono()+"]\r\n");
				sb.append("Fa_banco_zona=["+this.getFa_banco_zona()+"]\r\n");
				sb.append("Fa_fecha_ingreso_nvo=["+this.getFa_fecha_ingreso_nvo()+"]\r\n");
				sb.append("Fa_linea=["+this.getFa_linea()+"]\r\n");
				sb.append("Fa_marca_tarea=["+this.getFa_marca_tarea()+"]\r\n");
				return sb.toString();
	}
}