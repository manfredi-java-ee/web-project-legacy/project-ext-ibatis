package arg.ros.acc.vo.descripciones;

/**
* FcTBEQSPVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FcTBEQSPVO {

	private Double adabas_isn;
	private String tb_estado;
	private Double tb_codigo_rvi;
	private Double tb_clase_equipo;
	private String tb_desc_cant;
	private Double tb_cod_cpto_pfnn;
	private Double tb_valoriza_promopack;
	private String tb_tipo_comp;
	private Double tb_descuento;
	private String tb_descripcion;
	private Double tb_ind_resguardo;
	private Double tb_tipo_serv_filler;
	private String tb_descrip_det;
	private String tb_um_campo_cant;
	private String tb_factura;
	private Double tb_equipo;
	private Double tb_cod_cpto;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FcTBEQSPVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Tb_estado
	* @return el valor de Tb_estado
	*/ 
	public String getTb_estado() {
		return tb_estado;
	}
    	
	/**
	* Fija el valor del atributo Tb_estado
	* @param valor el valor de Tb_estado
	*/ 
	public void setTb_estado(String valor) {
		this.tb_estado = valor;
	}
	/**
	* Recupera el valor del atributo Tb_codigo_rvi
	* @return el valor de Tb_codigo_rvi
	*/ 
	public Double getTb_codigo_rvi() {
		return tb_codigo_rvi;
	}
    	
	/**
	* Fija el valor del atributo Tb_codigo_rvi
	* @param valor el valor de Tb_codigo_rvi
	*/ 
	public void setTb_codigo_rvi(Double valor) {
		this.tb_codigo_rvi = valor;
	}
	/**
	* Recupera el valor del atributo Tb_clase_equipo
	* @return el valor de Tb_clase_equipo
	*/ 
	public Double getTb_clase_equipo() {
		return tb_clase_equipo;
	}
    	
	/**
	* Fija el valor del atributo Tb_clase_equipo
	* @param valor el valor de Tb_clase_equipo
	*/ 
	public void setTb_clase_equipo(Double valor) {
		this.tb_clase_equipo = valor;
	}
	/**
	* Recupera el valor del atributo Tb_desc_cant
	* @return el valor de Tb_desc_cant
	*/ 
	public String getTb_desc_cant() {
		return tb_desc_cant;
	}
    	
	/**
	* Fija el valor del atributo Tb_desc_cant
	* @param valor el valor de Tb_desc_cant
	*/ 
	public void setTb_desc_cant(String valor) {
		this.tb_desc_cant = valor;
	}
	/**
	* Recupera el valor del atributo Tb_cod_cpto_pfnn
	* @return el valor de Tb_cod_cpto_pfnn
	*/ 
	public Double getTb_cod_cpto_pfnn() {
		return tb_cod_cpto_pfnn;
	}
    	
	/**
	* Fija el valor del atributo Tb_cod_cpto_pfnn
	* @param valor el valor de Tb_cod_cpto_pfnn
	*/ 
	public void setTb_cod_cpto_pfnn(Double valor) {
		this.tb_cod_cpto_pfnn = valor;
	}
	/**
	* Recupera el valor del atributo Tb_valoriza_promopack
	* @return el valor de Tb_valoriza_promopack
	*/ 
	public Double getTb_valoriza_promopack() {
		return tb_valoriza_promopack;
	}
    	
	/**
	* Fija el valor del atributo Tb_valoriza_promopack
	* @param valor el valor de Tb_valoriza_promopack
	*/ 
	public void setTb_valoriza_promopack(Double valor) {
		this.tb_valoriza_promopack = valor;
	}
	/**
	* Recupera el valor del atributo Tb_tipo_comp
	* @return el valor de Tb_tipo_comp
	*/ 
	public String getTb_tipo_comp() {
		return tb_tipo_comp;
	}
    	
	/**
	* Fija el valor del atributo Tb_tipo_comp
	* @param valor el valor de Tb_tipo_comp
	*/ 
	public void setTb_tipo_comp(String valor) {
		this.tb_tipo_comp = valor;
	}
	/**
	* Recupera el valor del atributo Tb_descuento
	* @return el valor de Tb_descuento
	*/ 
	public Double getTb_descuento() {
		return tb_descuento;
	}
    	
	/**
	* Fija el valor del atributo Tb_descuento
	* @param valor el valor de Tb_descuento
	*/ 
	public void setTb_descuento(Double valor) {
		this.tb_descuento = valor;
	}
	/**
	* Recupera el valor del atributo Tb_descripcion
	* @return el valor de Tb_descripcion
	*/ 
	public String getTb_descripcion() {
		return tb_descripcion;
	}
    	
	/**
	* Fija el valor del atributo Tb_descripcion
	* @param valor el valor de Tb_descripcion
	*/ 
	public void setTb_descripcion(String valor) {
		this.tb_descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Tb_ind_resguardo
	* @return el valor de Tb_ind_resguardo
	*/ 
	public Double getTb_ind_resguardo() {
		return tb_ind_resguardo;
	}
    	
	/**
	* Fija el valor del atributo Tb_ind_resguardo
	* @param valor el valor de Tb_ind_resguardo
	*/ 
	public void setTb_ind_resguardo(Double valor) {
		this.tb_ind_resguardo = valor;
	}
	/**
	* Recupera el valor del atributo Tb_tipo_serv_filler
	* @return el valor de Tb_tipo_serv_filler
	*/ 
	public Double getTb_tipo_serv_filler() {
		return tb_tipo_serv_filler;
	}
    	
	/**
	* Fija el valor del atributo Tb_tipo_serv_filler
	* @param valor el valor de Tb_tipo_serv_filler
	*/ 
	public void setTb_tipo_serv_filler(Double valor) {
		this.tb_tipo_serv_filler = valor;
	}
	/**
	* Recupera el valor del atributo Tb_descrip_det
	* @return el valor de Tb_descrip_det
	*/ 
	public String getTb_descrip_det() {
		return tb_descrip_det;
	}
    	
	/**
	* Fija el valor del atributo Tb_descrip_det
	* @param valor el valor de Tb_descrip_det
	*/ 
	public void setTb_descrip_det(String valor) {
		this.tb_descrip_det = valor;
	}
	/**
	* Recupera el valor del atributo Tb_um_campo_cant
	* @return el valor de Tb_um_campo_cant
	*/ 
	public String getTb_um_campo_cant() {
		return tb_um_campo_cant;
	}
    	
	/**
	* Fija el valor del atributo Tb_um_campo_cant
	* @param valor el valor de Tb_um_campo_cant
	*/ 
	public void setTb_um_campo_cant(String valor) {
		this.tb_um_campo_cant = valor;
	}
	/**
	* Recupera el valor del atributo Tb_factura
	* @return el valor de Tb_factura
	*/ 
	public String getTb_factura() {
		return tb_factura;
	}
    	
	/**
	* Fija el valor del atributo Tb_factura
	* @param valor el valor de Tb_factura
	*/ 
	public void setTb_factura(String valor) {
		this.tb_factura = valor;
	}
	/**
	* Recupera el valor del atributo Tb_equipo
	* @return el valor de Tb_equipo
	*/ 
	public Double getTb_equipo() {
		return tb_equipo;
	}
    	
	/**
	* Fija el valor del atributo Tb_equipo
	* @param valor el valor de Tb_equipo
	*/ 
	public void setTb_equipo(Double valor) {
		this.tb_equipo = valor;
	}
	/**
	* Recupera el valor del atributo Tb_cod_cpto
	* @return el valor de Tb_cod_cpto
	*/ 
	public Double getTb_cod_cpto() {
		return tb_cod_cpto;
	}
    	
	/**
	* Fija el valor del atributo Tb_cod_cpto
	* @param valor el valor de Tb_cod_cpto
	*/ 
	public void setTb_cod_cpto(Double valor) {
		this.tb_cod_cpto = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Tb_estado=["+this.getTb_estado()+"]\r\n");
				sb.append("Tb_codigo_rvi=["+this.getTb_codigo_rvi()+"]\r\n");
				sb.append("Tb_clase_equipo=["+this.getTb_clase_equipo()+"]\r\n");
				sb.append("Tb_desc_cant=["+this.getTb_desc_cant()+"]\r\n");
				sb.append("Tb_cod_cpto_pfnn=["+this.getTb_cod_cpto_pfnn()+"]\r\n");
				sb.append("Tb_valoriza_promopack=["+this.getTb_valoriza_promopack()+"]\r\n");
				sb.append("Tb_tipo_comp=["+this.getTb_tipo_comp()+"]\r\n");
				sb.append("Tb_descuento=["+this.getTb_descuento()+"]\r\n");
				sb.append("Tb_descripcion=["+this.getTb_descripcion()+"]\r\n");
				sb.append("Tb_ind_resguardo=["+this.getTb_ind_resguardo()+"]\r\n");
				sb.append("Tb_tipo_serv_filler=["+this.getTb_tipo_serv_filler()+"]\r\n");
				sb.append("Tb_descrip_det=["+this.getTb_descrip_det()+"]\r\n");
				sb.append("Tb_um_campo_cant=["+this.getTb_um_campo_cant()+"]\r\n");
				sb.append("Tb_factura=["+this.getTb_factura()+"]\r\n");
				sb.append("Tb_equipo=["+this.getTb_equipo()+"]\r\n");
				sb.append("Tb_cod_cpto=["+this.getTb_cod_cpto()+"]\r\n");
				return sb.toString();
	}
}