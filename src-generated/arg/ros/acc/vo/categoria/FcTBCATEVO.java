package arg.ros.acc.vo.categoria;

/**
* FcTBCATEVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FcTBCATEVO {

	private Double adabas_isn;
	private String tb_estado;
	private Double tb_p_lim_rel_sup;
	private Double tb_p_lim_rel_inf;
	private Double tb_categoria;
	private Double tb_relle1;
	private Double tb_descuento;
	private Double tb_p_adic;
	private Double tb_relle2;
	private String tb_u_medida;
	private Double tb_q_lim_abs_pul;
	private String tb_descripcion;
	private Double tb_categoria_sigeco;
	private String tb_permite_cuit;
	private Double tb_q_asel;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FcTBCATEVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Tb_estado
	* @return el valor de Tb_estado
	*/ 
	public String getTb_estado() {
		return tb_estado;
	}
    	
	/**
	* Fija el valor del atributo Tb_estado
	* @param valor el valor de Tb_estado
	*/ 
	public void setTb_estado(String valor) {
		this.tb_estado = valor;
	}
	/**
	* Recupera el valor del atributo Tb_p_lim_rel_sup
	* @return el valor de Tb_p_lim_rel_sup
	*/ 
	public Double getTb_p_lim_rel_sup() {
		return tb_p_lim_rel_sup;
	}
    	
	/**
	* Fija el valor del atributo Tb_p_lim_rel_sup
	* @param valor el valor de Tb_p_lim_rel_sup
	*/ 
	public void setTb_p_lim_rel_sup(Double valor) {
		this.tb_p_lim_rel_sup = valor;
	}
	/**
	* Recupera el valor del atributo Tb_p_lim_rel_inf
	* @return el valor de Tb_p_lim_rel_inf
	*/ 
	public Double getTb_p_lim_rel_inf() {
		return tb_p_lim_rel_inf;
	}
    	
	/**
	* Fija el valor del atributo Tb_p_lim_rel_inf
	* @param valor el valor de Tb_p_lim_rel_inf
	*/ 
	public void setTb_p_lim_rel_inf(Double valor) {
		this.tb_p_lim_rel_inf = valor;
	}
	/**
	* Recupera el valor del atributo Tb_categoria
	* @return el valor de Tb_categoria
	*/ 
	public Double getTb_categoria() {
		return tb_categoria;
	}
    	
	/**
	* Fija el valor del atributo Tb_categoria
	* @param valor el valor de Tb_categoria
	*/ 
	public void setTb_categoria(Double valor) {
		this.tb_categoria = valor;
	}
	/**
	* Recupera el valor del atributo Tb_relle1
	* @return el valor de Tb_relle1
	*/ 
	public Double getTb_relle1() {
		return tb_relle1;
	}
    	
	/**
	* Fija el valor del atributo Tb_relle1
	* @param valor el valor de Tb_relle1
	*/ 
	public void setTb_relle1(Double valor) {
		this.tb_relle1 = valor;
	}
	/**
	* Recupera el valor del atributo Tb_descuento
	* @return el valor de Tb_descuento
	*/ 
	public Double getTb_descuento() {
		return tb_descuento;
	}
    	
	/**
	* Fija el valor del atributo Tb_descuento
	* @param valor el valor de Tb_descuento
	*/ 
	public void setTb_descuento(Double valor) {
		this.tb_descuento = valor;
	}
	/**
	* Recupera el valor del atributo Tb_p_adic
	* @return el valor de Tb_p_adic
	*/ 
	public Double getTb_p_adic() {
		return tb_p_adic;
	}
    	
	/**
	* Fija el valor del atributo Tb_p_adic
	* @param valor el valor de Tb_p_adic
	*/ 
	public void setTb_p_adic(Double valor) {
		this.tb_p_adic = valor;
	}
	/**
	* Recupera el valor del atributo Tb_relle2
	* @return el valor de Tb_relle2
	*/ 
	public Double getTb_relle2() {
		return tb_relle2;
	}
    	
	/**
	* Fija el valor del atributo Tb_relle2
	* @param valor el valor de Tb_relle2
	*/ 
	public void setTb_relle2(Double valor) {
		this.tb_relle2 = valor;
	}
	/**
	* Recupera el valor del atributo Tb_u_medida
	* @return el valor de Tb_u_medida
	*/ 
	public String getTb_u_medida() {
		return tb_u_medida;
	}
    	
	/**
	* Fija el valor del atributo Tb_u_medida
	* @param valor el valor de Tb_u_medida
	*/ 
	public void setTb_u_medida(String valor) {
		this.tb_u_medida = valor;
	}
	/**
	* Recupera el valor del atributo Tb_q_lim_abs_pul
	* @return el valor de Tb_q_lim_abs_pul
	*/ 
	public Double getTb_q_lim_abs_pul() {
		return tb_q_lim_abs_pul;
	}
    	
	/**
	* Fija el valor del atributo Tb_q_lim_abs_pul
	* @param valor el valor de Tb_q_lim_abs_pul
	*/ 
	public void setTb_q_lim_abs_pul(Double valor) {
		this.tb_q_lim_abs_pul = valor;
	}
	/**
	* Recupera el valor del atributo Tb_descripcion
	* @return el valor de Tb_descripcion
	*/ 
	public String getTb_descripcion() {
		return tb_descripcion;
	}
    	
	/**
	* Fija el valor del atributo Tb_descripcion
	* @param valor el valor de Tb_descripcion
	*/ 
	public void setTb_descripcion(String valor) {
		this.tb_descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Tb_categoria_sigeco
	* @return el valor de Tb_categoria_sigeco
	*/ 
	public Double getTb_categoria_sigeco() {
		return tb_categoria_sigeco;
	}
    	
	/**
	* Fija el valor del atributo Tb_categoria_sigeco
	* @param valor el valor de Tb_categoria_sigeco
	*/ 
	public void setTb_categoria_sigeco(Double valor) {
		this.tb_categoria_sigeco = valor;
	}
	/**
	* Recupera el valor del atributo Tb_permite_cuit
	* @return el valor de Tb_permite_cuit
	*/ 
	public String getTb_permite_cuit() {
		return tb_permite_cuit;
	}
    	
	/**
	* Fija el valor del atributo Tb_permite_cuit
	* @param valor el valor de Tb_permite_cuit
	*/ 
	public void setTb_permite_cuit(String valor) {
		this.tb_permite_cuit = valor;
	}
	/**
	* Recupera el valor del atributo Tb_q_asel
	* @return el valor de Tb_q_asel
	*/ 
	public Double getTb_q_asel() {
		return tb_q_asel;
	}
    	
	/**
	* Fija el valor del atributo Tb_q_asel
	* @param valor el valor de Tb_q_asel
	*/ 
	public void setTb_q_asel(Double valor) {
		this.tb_q_asel = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Tb_estado=["+this.getTb_estado()+"]\r\n");
				sb.append("Tb_p_lim_rel_sup=["+this.getTb_p_lim_rel_sup()+"]\r\n");
				sb.append("Tb_p_lim_rel_inf=["+this.getTb_p_lim_rel_inf()+"]\r\n");
				sb.append("Tb_categoria=["+this.getTb_categoria()+"]\r\n");
				sb.append("Tb_relle1=["+this.getTb_relle1()+"]\r\n");
				sb.append("Tb_descuento=["+this.getTb_descuento()+"]\r\n");
				sb.append("Tb_p_adic=["+this.getTb_p_adic()+"]\r\n");
				sb.append("Tb_relle2=["+this.getTb_relle2()+"]\r\n");
				sb.append("Tb_u_medida=["+this.getTb_u_medida()+"]\r\n");
				sb.append("Tb_q_lim_abs_pul=["+this.getTb_q_lim_abs_pul()+"]\r\n");
				sb.append("Tb_descripcion=["+this.getTb_descripcion()+"]\r\n");
				sb.append("Tb_categoria_sigeco=["+this.getTb_categoria_sigeco()+"]\r\n");
				sb.append("Tb_permite_cuit=["+this.getTb_permite_cuit()+"]\r\n");
				sb.append("Tb_q_asel=["+this.getTb_q_asel()+"]\r\n");
				return sb.toString();
	}
}