package arg.ros.acc.vo.inhibiciones;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

/**
* MovimientoFisicoVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class MovimientoFisicoVO {

	private Double adabas_isn;
	private Double fa_r_oficina;
	private java.math.BigDecimal fa_c_importe_total;
	private java.math.BigDecimal fa_abono;
	private Double fa_r_cajero;
	private Double fa_nro_tarjeta;
	private Double campo_no_usado_3;
	private String fa_r_usuario;
	private Double fa_r_tipo_pago;
	private Double fa_c_estado_conciliacion;
	private java.math.BigDecimal fa_r_total_sin_recargo;
	private Double fa_ident_abo;
	private String fa_fecha_acreditacion_nvo;
	private String fa_localidad;
	private Double fa_r_zona_pago;
	private String fa_marca_desimputacion;
	private String fa_c_usuario_baja;
	private java.util.Date fa_c_fecha_pago_nvo;
	private java.util.Date fa_r_fecha_proceso_nvo;
	private Double fa_r_categoria_gran_cliente;
	private String fa_urb;
	private java.util.Date fa_c_fecha_hora_alta;
	private Double fa_c_zona_pago;
	private java.util.Date fa_c_fecha_hora_modif;
	private Double campo_no_usado_7;
	private java.util.Date fa_fecha_marca_nvo;
	private java.math.BigDecimal fa_serv_medido;
	private Double fa_r_factura;
	private Double fa_c_tipo_registro;
	private String fa_r_terminal;
	private Double campo_no_usado;
	private String fa_razon_social;
	private Double campo_no_usado_8;
	private java.math.BigDecimal fa_r_iva_mora_r;
	private String fa_domicilio;
	private Double fa_r_nro_nota_cred;
	private Double fa_c_cant_concp_c_apert;
	private Double fa_r_marca_tarea;
	private String fa_r_hora_ingreso;
	private Double fa_r_marca_carta_motivo;
	private Double fa_cod_entrega;
	private Double fa_r_ident_abo;
	private java.util.Date fa_r_fecha_reconcil_nvo;
	private Double fa_r_reconciliacion;
	private java.util.Date fa_fecha_desimputacion;
	private Double campo_no_usado_5;
	private String fa_c_ident_aper_man;
	private Double fa_c_nro_conciliacion;
	private Double fa_tipo_contrib;
	private java.math.BigDecimal fa_r_importe_fuera_term;
	private Double campo_no_usado_4;
	private String fa_r_linea;
	private String fa_cod_postal;
	private Double fa_tipo_fact;
	private Double fa_c_cod_banco_depos;
	private java.math.BigDecimal fa_total_facturado;
	private java.util.Date fa_r_fecha_pago_orig;
	private String fa_c_usuario_modif;
	private Double fa_oficina;
	private Double fa_c_nro_boleta_deposito;
	private String fa_linea;
	private Double fa_r_anio_lote;
	private String fa_r_codigo_movimiento;
	private java.math.BigDecimal fa_r_importe_cobrado;
	private java.math.BigDecimal fa_importe_pdte_gire;
	private Double fa_r_zona_pago_orig;
	private String fa_c_usuario_alta;
	private Double fa_numero_pago;
	private Double campo_no_usado_9;
	private Double fa_cod_banco;
	private Double fa_c_cant_reg;
	private String fa_r_urb;
	private Double fa_marca_tarea;
	private Double fa_r_codigo_iva;
	private Double fa_r_nro_reintegro;
	private Double fa_tipo_contrib_nvo;
	private Double fa_r_abonado;
	private Double fa_r_tipo_facturacion;
	private java.util.Date fa_c_fecha_hora_baja;
	private Double fa_r_numero_pago;
	private String fa_codigo_movimiento;
	
	// Modificado Manfredi por traer nulos
	private String fa_anio_lote;
	
	
	private Double fa_banco_zona;
	private Double fa_c_estado_asiento;
	private Double fa_abonado;
	private Double campo_no_usado_6;
	private String fa_r_interurb;
	private Double fa_r_nro_copia;
	private java.util.Date fa_r_fecha_pago_nvo;
	private String fa_r_marca_pago;
	private Double fa_zona_banco_pago_doble;
	private String fa_usuario_terminal;
	private String fa_fecha_proc_ctgo;
	private Double fa_c_marca_tarea;
	private java.util.Date fa_fecha_entrega;
	private Double fa_marca_recargo;
	private Double fa_c_cod_concepto;
	private String fa_interurb;
	private Double fa_r_zona;
	private java.util.Date fa_r_fecha_ingreso_nvo;
	private String fa_hora_ingreso;
	private java.util.Date fa_c_fecha_env_contab_nvo;
	private Double fa_r_estado_real_base;
	private java.math.BigDecimal fa_importe_iva;
	private Double fa_zona;
	private java.util.Date fa_fecha_ingreso_nvo;
	private String desc_movimiento;
	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public MovimientoFisicoVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_oficina
	* @return el valor de Fa_r_oficina
	*/ 
	public Double getFa_r_oficina() {
		return fa_r_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_oficina
	* @param valor el valor de Fa_r_oficina
	*/ 
	public void setFa_r_oficina(Double valor) {
		this.fa_r_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_importe_total
	* @return el valor de Fa_c_importe_total
	*/ 
	public java.math.BigDecimal getFa_c_importe_total() {
		return fa_c_importe_total;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_importe_total
	* @param valor el valor de Fa_c_importe_total
	*/ 
	public void setFa_c_importe_total(java.math.BigDecimal valor) {
		this.fa_c_importe_total = valor;
	}
	/**
	* Recupera el valor del atributo Fa_abono
	* @return el valor de Fa_abono
	*/ 
	public java.math.BigDecimal getFa_abono() {
		return fa_abono;
	}
    	
	/**
	* Fija el valor del atributo Fa_abono
	* @param valor el valor de Fa_abono
	*/ 
	public void setFa_abono(java.math.BigDecimal valor) {
		this.fa_abono = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_cajero
	* @return el valor de Fa_r_cajero
	*/ 
	public Double getFa_r_cajero() {
		return fa_r_cajero;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_cajero
	* @param valor el valor de Fa_r_cajero
	*/ 
	public void setFa_r_cajero(Double valor) {
		this.fa_r_cajero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_nro_tarjeta
	* @return el valor de Fa_nro_tarjeta
	*/ 
	public Double getFa_nro_tarjeta() {
		return fa_nro_tarjeta;
	}
    	
	/**
	* Fija el valor del atributo Fa_nro_tarjeta
	* @param valor el valor de Fa_nro_tarjeta
	*/ 
	public void setFa_nro_tarjeta(Double valor) {
		this.fa_nro_tarjeta = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_3
	* @return el valor de Campo_no_usado_3
	*/ 
	public Double getCampo_no_usado_3() {
		return campo_no_usado_3;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_3
	* @param valor el valor de Campo_no_usado_3
	*/ 
	public void setCampo_no_usado_3(Double valor) {
		this.campo_no_usado_3 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_usuario
	* @return el valor de Fa_r_usuario
	*/ 
	public String getFa_r_usuario() {
		return fa_r_usuario;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_usuario
	* @param valor el valor de Fa_r_usuario
	*/ 
	public void setFa_r_usuario(String valor) {
		this.fa_r_usuario = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_tipo_pago
	* @return el valor de Fa_r_tipo_pago
	*/ 
	public Double getFa_r_tipo_pago() {
		return fa_r_tipo_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_tipo_pago
	* @param valor el valor de Fa_r_tipo_pago
	*/ 
	public void setFa_r_tipo_pago(Double valor) {
		this.fa_r_tipo_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_estado_conciliacion
	* @return el valor de Fa_c_estado_conciliacion
	*/ 
	public Double getFa_c_estado_conciliacion() {
		return fa_c_estado_conciliacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_estado_conciliacion
	* @param valor el valor de Fa_c_estado_conciliacion
	*/ 
	public void setFa_c_estado_conciliacion(Double valor) {
		this.fa_c_estado_conciliacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_total_sin_recargo
	* @return el valor de Fa_r_total_sin_recargo
	*/ 
	public java.math.BigDecimal getFa_r_total_sin_recargo() {
		return fa_r_total_sin_recargo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_total_sin_recargo
	* @param valor el valor de Fa_r_total_sin_recargo
	*/ 
	public void setFa_r_total_sin_recargo(java.math.BigDecimal valor) {
		this.fa_r_total_sin_recargo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_ident_abo
	* @return el valor de Fa_ident_abo
	*/ 
	public Double getFa_ident_abo() {
		return fa_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_ident_abo
	* @param valor el valor de Fa_ident_abo
	*/ 
	public void setFa_ident_abo(Double valor) {
		this.fa_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_acreditacion_nvo
	* @return el valor de Fa_fecha_acreditacion_nvo
	*/ 
	public String getFa_fecha_acreditacion_nvo() {
		return fa_fecha_acreditacion_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_acreditacion_nvo
	* @param valor el valor de Fa_fecha_acreditacion_nvo
	*/ 
	public void setFa_fecha_acreditacion_nvo(String valor) {
		this.fa_fecha_acreditacion_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_localidad
	* @return el valor de Fa_localidad
	*/ 
	public String getFa_localidad() {
		return fa_localidad;
	}
    	
	/**
	* Fija el valor del atributo Fa_localidad
	* @param valor el valor de Fa_localidad
	*/ 
	public void setFa_localidad(String valor) {
		this.fa_localidad = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_zona_pago
	* @return el valor de Fa_r_zona_pago
	*/ 
	public Double getFa_r_zona_pago() {
		return fa_r_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_zona_pago
	* @param valor el valor de Fa_r_zona_pago
	*/ 
	public void setFa_r_zona_pago(Double valor) {
		this.fa_r_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_desimputacion
	* @return el valor de Fa_marca_desimputacion
	*/ 
	public String getFa_marca_desimputacion() {
		return fa_marca_desimputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_desimputacion
	* @param valor el valor de Fa_marca_desimputacion
	*/ 
	public void setFa_marca_desimputacion(String valor) {
		this.fa_marca_desimputacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_usuario_baja
	* @return el valor de Fa_c_usuario_baja
	*/ 
	public String getFa_c_usuario_baja() {
		return fa_c_usuario_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_usuario_baja
	* @param valor el valor de Fa_c_usuario_baja
	*/ 
	public void setFa_c_usuario_baja(String valor) {
		this.fa_c_usuario_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_pago_nvo
	* @return el valor de Fa_c_fecha_pago_nvo
	*/ 
	public java.util.Date getFa_c_fecha_pago_nvo() {
		return fa_c_fecha_pago_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_pago_nvo
	* @param valor el valor de Fa_c_fecha_pago_nvo
	*/ 
	public void setFa_c_fecha_pago_nvo(java.util.Date valor) {
		this.fa_c_fecha_pago_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_proceso_nvo
	* @return el valor de Fa_r_fecha_proceso_nvo
	*/ 
	public java.util.Date getFa_r_fecha_proceso_nvo() {
		return fa_r_fecha_proceso_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_proceso_nvo
	* @param valor el valor de Fa_r_fecha_proceso_nvo
	*/ 
	public void setFa_r_fecha_proceso_nvo(java.util.Date valor) {
		this.fa_r_fecha_proceso_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_categoria_gran_cliente
	* @return el valor de Fa_r_categoria_gran_cliente
	*/ 
	public Double getFa_r_categoria_gran_cliente() {
		return fa_r_categoria_gran_cliente;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_categoria_gran_cliente
	* @param valor el valor de Fa_r_categoria_gran_cliente
	*/ 
	public void setFa_r_categoria_gran_cliente(Double valor) {
		this.fa_r_categoria_gran_cliente = valor;
	}
	/**
	* Recupera el valor del atributo Fa_urb
	* @return el valor de Fa_urb
	*/ 
	public String getFa_urb() {
		return fa_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_urb
	* @param valor el valor de Fa_urb
	*/ 
	public void setFa_urb(String valor) {
		this.fa_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_hora_alta
	* @return el valor de Fa_c_fecha_hora_alta
	*/ 
	public java.util.Date getFa_c_fecha_hora_alta() {
		return fa_c_fecha_hora_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_hora_alta
	* @param valor el valor de Fa_c_fecha_hora_alta
	*/ 
	public void setFa_c_fecha_hora_alta(java.util.Date valor) {
		this.fa_c_fecha_hora_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_zona_pago
	* @return el valor de Fa_c_zona_pago
	*/ 
	public Double getFa_c_zona_pago() {
		return fa_c_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_zona_pago
	* @param valor el valor de Fa_c_zona_pago
	*/ 
	public void setFa_c_zona_pago(Double valor) {
		this.fa_c_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_hora_modif
	* @return el valor de Fa_c_fecha_hora_modif
	*/ 
	public java.util.Date getFa_c_fecha_hora_modif() {
		return fa_c_fecha_hora_modif;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_hora_modif
	* @param valor el valor de Fa_c_fecha_hora_modif
	*/ 
	public void setFa_c_fecha_hora_modif(java.util.Date valor) {
		this.fa_c_fecha_hora_modif = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_7
	* @return el valor de Campo_no_usado_7
	*/ 
	public Double getCampo_no_usado_7() {
		return campo_no_usado_7;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_7
	* @param valor el valor de Campo_no_usado_7
	*/ 
	public void setCampo_no_usado_7(Double valor) {
		this.campo_no_usado_7 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_marca_nvo
	* @return el valor de Fa_fecha_marca_nvo
	*/ 
	public java.util.Date getFa_fecha_marca_nvo() {
		return fa_fecha_marca_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_marca_nvo
	* @param valor el valor de Fa_fecha_marca_nvo
	*/ 
	public void setFa_fecha_marca_nvo(java.util.Date valor) {
		this.fa_fecha_marca_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_serv_medido
	* @return el valor de Fa_serv_medido
	*/ 
	public java.math.BigDecimal getFa_serv_medido() {
		return fa_serv_medido;
	}
    	
	/**
	* Fija el valor del atributo Fa_serv_medido
	* @param valor el valor de Fa_serv_medido
	*/ 
	public void setFa_serv_medido(java.math.BigDecimal valor) {
		this.fa_serv_medido = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_factura
	* @return el valor de Fa_r_factura
	*/ 
	public Double getFa_r_factura() {
		return fa_r_factura;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_factura
	* @param valor el valor de Fa_r_factura
	*/ 
	public void setFa_r_factura(Double valor) {
		this.fa_r_factura = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_tipo_registro
	* @return el valor de Fa_c_tipo_registro
	*/ 
	public Double getFa_c_tipo_registro() {
		return fa_c_tipo_registro;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_tipo_registro
	* @param valor el valor de Fa_c_tipo_registro
	*/ 
	public void setFa_c_tipo_registro(Double valor) {
		this.fa_c_tipo_registro = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_terminal
	* @return el valor de Fa_r_terminal
	*/ 
	public String getFa_r_terminal() {
		return fa_r_terminal;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_terminal
	* @param valor el valor de Fa_r_terminal
	*/ 
	public void setFa_r_terminal(String valor) {
		this.fa_r_terminal = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado
	* @return el valor de Campo_no_usado
	*/ 
	public Double getCampo_no_usado() {
		return campo_no_usado;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado
	* @param valor el valor de Campo_no_usado
	*/ 
	public void setCampo_no_usado(Double valor) {
		this.campo_no_usado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_razon_social
	* @return el valor de Fa_razon_social
	*/ 
	public String getFa_razon_social() {
		return fa_razon_social;
	}
    	
	/**
	* Fija el valor del atributo Fa_razon_social
	* @param valor el valor de Fa_razon_social
	*/ 
	public void setFa_razon_social(String valor) {
		this.fa_razon_social = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_8
	* @return el valor de Campo_no_usado_8
	*/ 
	public Double getCampo_no_usado_8() {
		return campo_no_usado_8;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_8
	* @param valor el valor de Campo_no_usado_8
	*/ 
	public void setCampo_no_usado_8(Double valor) {
		this.campo_no_usado_8 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_iva_mora_r
	* @return el valor de Fa_r_iva_mora_r
	*/ 
	public java.math.BigDecimal getFa_r_iva_mora_r() {
		return fa_r_iva_mora_r;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_iva_mora_r
	* @param valor el valor de Fa_r_iva_mora_r
	*/ 
	public void setFa_r_iva_mora_r(java.math.BigDecimal valor) {
		this.fa_r_iva_mora_r = valor;
	}
	/**
	* Recupera el valor del atributo Fa_domicilio
	* @return el valor de Fa_domicilio
	*/ 
	public String getFa_domicilio() {
		return fa_domicilio;
	}
    	
	/**
	* Fija el valor del atributo Fa_domicilio
	* @param valor el valor de Fa_domicilio
	*/ 
	public void setFa_domicilio(String valor) {
		this.fa_domicilio = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_nro_nota_cred
	* @return el valor de Fa_r_nro_nota_cred
	*/ 
	public Double getFa_r_nro_nota_cred() {
		return fa_r_nro_nota_cred;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_nro_nota_cred
	* @param valor el valor de Fa_r_nro_nota_cred
	*/ 
	public void setFa_r_nro_nota_cred(Double valor) {
		this.fa_r_nro_nota_cred = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cant_concp_c_apert
	* @return el valor de Fa_c_cant_concp_c_apert
	*/ 
	public Double getFa_c_cant_concp_c_apert() {
		return fa_c_cant_concp_c_apert;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cant_concp_c_apert
	* @param valor el valor de Fa_c_cant_concp_c_apert
	*/ 
	public void setFa_c_cant_concp_c_apert(Double valor) {
		this.fa_c_cant_concp_c_apert = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_marca_tarea
	* @return el valor de Fa_r_marca_tarea
	*/ 
	public Double getFa_r_marca_tarea() {
		return fa_r_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_marca_tarea
	* @param valor el valor de Fa_r_marca_tarea
	*/ 
	public void setFa_r_marca_tarea(Double valor) {
		this.fa_r_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_hora_ingreso
	* @return el valor de Fa_r_hora_ingreso
	*/ 
	public String getFa_r_hora_ingreso() {
		return fa_r_hora_ingreso;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_hora_ingreso
	* @param valor el valor de Fa_r_hora_ingreso
	*/ 
	public void setFa_r_hora_ingreso(String valor) {
		this.fa_r_hora_ingreso = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_marca_carta_motivo
	* @return el valor de Fa_r_marca_carta_motivo
	*/ 
	public Double getFa_r_marca_carta_motivo() {
		return fa_r_marca_carta_motivo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_marca_carta_motivo
	* @param valor el valor de Fa_r_marca_carta_motivo
	*/ 
	public void setFa_r_marca_carta_motivo(Double valor) {
		this.fa_r_marca_carta_motivo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_entrega
	* @return el valor de Fa_cod_entrega
	*/ 
	public Double getFa_cod_entrega() {
		return fa_cod_entrega;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_entrega
	* @param valor el valor de Fa_cod_entrega
	*/ 
	public void setFa_cod_entrega(Double valor) {
		this.fa_cod_entrega = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_ident_abo
	* @return el valor de Fa_r_ident_abo
	*/ 
	public Double getFa_r_ident_abo() {
		return fa_r_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_ident_abo
	* @param valor el valor de Fa_r_ident_abo
	*/ 
	public void setFa_r_ident_abo(Double valor) {
		this.fa_r_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_reconcil_nvo
	* @return el valor de Fa_r_fecha_reconcil_nvo
	*/ 
	public java.util.Date getFa_r_fecha_reconcil_nvo() {
		return fa_r_fecha_reconcil_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_reconcil_nvo
	* @param valor el valor de Fa_r_fecha_reconcil_nvo
	*/ 
	public void setFa_r_fecha_reconcil_nvo(java.util.Date valor) {
		this.fa_r_fecha_reconcil_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_reconciliacion
	* @return el valor de Fa_r_reconciliacion
	*/ 
	public Double getFa_r_reconciliacion() {
		return fa_r_reconciliacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_reconciliacion
	* @param valor el valor de Fa_r_reconciliacion
	*/ 
	public void setFa_r_reconciliacion(Double valor) {
		this.fa_r_reconciliacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_desimputacion
	* @return el valor de Fa_fecha_desimputacion
	*/ 
	public java.util.Date getFa_fecha_desimputacion() {
		return fa_fecha_desimputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_desimputacion
	* @param valor el valor de Fa_fecha_desimputacion
	*/ 
	public void setFa_fecha_desimputacion(java.util.Date valor) {
		this.fa_fecha_desimputacion = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_5
	* @return el valor de Campo_no_usado_5
	*/ 
	public Double getCampo_no_usado_5() {
		return campo_no_usado_5;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_5
	* @param valor el valor de Campo_no_usado_5
	*/ 
	public void setCampo_no_usado_5(Double valor) {
		this.campo_no_usado_5 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_ident_aper_man
	* @return el valor de Fa_c_ident_aper_man
	*/ 
	public String getFa_c_ident_aper_man() {
		return fa_c_ident_aper_man;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_ident_aper_man
	* @param valor el valor de Fa_c_ident_aper_man
	*/ 
	public void setFa_c_ident_aper_man(String valor) {
		this.fa_c_ident_aper_man = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_nro_conciliacion
	* @return el valor de Fa_c_nro_conciliacion
	*/ 
	public Double getFa_c_nro_conciliacion() {
		return fa_c_nro_conciliacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_nro_conciliacion
	* @param valor el valor de Fa_c_nro_conciliacion
	*/ 
	public void setFa_c_nro_conciliacion(Double valor) {
		this.fa_c_nro_conciliacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_contrib
	* @return el valor de Fa_tipo_contrib
	*/ 
	public Double getFa_tipo_contrib() {
		return fa_tipo_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_contrib
	* @param valor el valor de Fa_tipo_contrib
	*/ 
	public void setFa_tipo_contrib(Double valor) {
		this.fa_tipo_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_importe_fuera_term
	* @return el valor de Fa_r_importe_fuera_term
	*/ 
	public java.math.BigDecimal getFa_r_importe_fuera_term() {
		return fa_r_importe_fuera_term;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_importe_fuera_term
	* @param valor el valor de Fa_r_importe_fuera_term
	*/ 
	public void setFa_r_importe_fuera_term(java.math.BigDecimal valor) {
		this.fa_r_importe_fuera_term = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_4
	* @return el valor de Campo_no_usado_4
	*/ 
	public Double getCampo_no_usado_4() {
		return campo_no_usado_4;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_4
	* @param valor el valor de Campo_no_usado_4
	*/ 
	public void setCampo_no_usado_4(Double valor) {
		this.campo_no_usado_4 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_linea
	* @return el valor de Fa_r_linea
	*/ 
	public String getFa_r_linea() {
		return fa_r_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_linea
	* @param valor el valor de Fa_r_linea
	*/ 
	public void setFa_r_linea(String valor) {
		this.fa_r_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_postal
	* @return el valor de Fa_cod_postal
	*/ 
	public String getFa_cod_postal() {
		return fa_cod_postal;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_postal
	* @param valor el valor de Fa_cod_postal
	*/ 
	public void setFa_cod_postal(String valor) {
		this.fa_cod_postal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_fact
	* @return el valor de Fa_tipo_fact
	*/ 
	public Double getFa_tipo_fact() {
		return fa_tipo_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_fact
	* @param valor el valor de Fa_tipo_fact
	*/ 
	public void setFa_tipo_fact(Double valor) {
		this.fa_tipo_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cod_banco_depos
	* @return el valor de Fa_c_cod_banco_depos
	*/ 
	public Double getFa_c_cod_banco_depos() {
		return fa_c_cod_banco_depos;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cod_banco_depos
	* @param valor el valor de Fa_c_cod_banco_depos
	*/ 
	public void setFa_c_cod_banco_depos(Double valor) {
		this.fa_c_cod_banco_depos = valor;
	}
	/**
	* Recupera el valor del atributo Fa_total_facturado
	* @return el valor de Fa_total_facturado
	*/ 
	public java.math.BigDecimal getFa_total_facturado() {
		return fa_total_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_total_facturado
	* @param valor el valor de Fa_total_facturado
	*/ 
	public void setFa_total_facturado(java.math.BigDecimal valor) {
		this.fa_total_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_pago_orig
	* @return el valor de Fa_r_fecha_pago_orig
	*/ 
	public java.util.Date getFa_r_fecha_pago_orig() {
		return fa_r_fecha_pago_orig;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_pago_orig
	* @param valor el valor de Fa_r_fecha_pago_orig
	*/ 
	public void setFa_r_fecha_pago_orig(java.util.Date valor) {
		this.fa_r_fecha_pago_orig = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_usuario_modif
	* @return el valor de Fa_c_usuario_modif
	*/ 
	public String getFa_c_usuario_modif() {
		return fa_c_usuario_modif;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_usuario_modif
	* @param valor el valor de Fa_c_usuario_modif
	*/ 
	public void setFa_c_usuario_modif(String valor) {
		this.fa_c_usuario_modif = valor;
	}
	/**
	* Recupera el valor del atributo Fa_oficina
	* @return el valor de Fa_oficina
	*/ 
	public Double getFa_oficina() {
		return fa_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_oficina
	* @param valor el valor de Fa_oficina
	*/ 
	public void setFa_oficina(Double valor) {
		this.fa_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_nro_boleta_deposito
	* @return el valor de Fa_c_nro_boleta_deposito
	*/ 
	public Double getFa_c_nro_boleta_deposito() {
		return fa_c_nro_boleta_deposito;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_nro_boleta_deposito
	* @param valor el valor de Fa_c_nro_boleta_deposito
	*/ 
	public void setFa_c_nro_boleta_deposito(Double valor) {
		this.fa_c_nro_boleta_deposito = valor;
	}
	/**
	* Recupera el valor del atributo Fa_linea
	* @return el valor de Fa_linea
	*/ 
	public String getFa_linea() {
		return fa_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_linea
	* @param valor el valor de Fa_linea
	*/ 
	public void setFa_linea(String valor) {
		this.fa_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_anio_lote
	* @return el valor de Fa_r_anio_lote
	*/ 
	public Double getFa_r_anio_lote() {
		return fa_r_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_anio_lote
	* @param valor el valor de Fa_r_anio_lote
	*/ 
	public void setFa_r_anio_lote(Double valor) {
		this.fa_r_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_codigo_movimiento
	* @return el valor de Fa_r_codigo_movimiento
	*/ 
	public String getFa_r_codigo_movimiento() {
		return fa_r_codigo_movimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_codigo_movimiento
	* @param valor el valor de Fa_r_codigo_movimiento
	*/ 
	public void setFa_r_codigo_movimiento(String valor) {
		this.fa_r_codigo_movimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_importe_cobrado
	* @return el valor de Fa_r_importe_cobrado
	*/ 
	public java.math.BigDecimal getFa_r_importe_cobrado() {
		return fa_r_importe_cobrado;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_importe_cobrado
	* @param valor el valor de Fa_r_importe_cobrado
	*/ 
	public void setFa_r_importe_cobrado(java.math.BigDecimal valor) {
		this.fa_r_importe_cobrado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_importe_pdte_gire
	* @return el valor de Fa_importe_pdte_gire
	*/ 
	public java.math.BigDecimal getFa_importe_pdte_gire() {
		return fa_importe_pdte_gire;
	}
    	
	/**
	* Fija el valor del atributo Fa_importe_pdte_gire
	* @param valor el valor de Fa_importe_pdte_gire
	*/ 
	public void setFa_importe_pdte_gire(java.math.BigDecimal valor) {
		this.fa_importe_pdte_gire = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_zona_pago_orig
	* @return el valor de Fa_r_zona_pago_orig
	*/ 
	public Double getFa_r_zona_pago_orig() {
		return fa_r_zona_pago_orig;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_zona_pago_orig
	* @param valor el valor de Fa_r_zona_pago_orig
	*/ 
	public void setFa_r_zona_pago_orig(Double valor) {
		this.fa_r_zona_pago_orig = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_usuario_alta
	* @return el valor de Fa_c_usuario_alta
	*/ 
	public String getFa_c_usuario_alta() {
		return fa_c_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_usuario_alta
	* @param valor el valor de Fa_c_usuario_alta
	*/ 
	public void setFa_c_usuario_alta(String valor) {
		this.fa_c_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_numero_pago
	* @return el valor de Fa_numero_pago
	*/ 
	public Double getFa_numero_pago() {
		return fa_numero_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_numero_pago
	* @param valor el valor de Fa_numero_pago
	*/ 
	public void setFa_numero_pago(Double valor) {
		this.fa_numero_pago = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_9
	* @return el valor de Campo_no_usado_9
	*/ 
	public Double getCampo_no_usado_9() {
		return campo_no_usado_9;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_9
	* @param valor el valor de Campo_no_usado_9
	*/ 
	public void setCampo_no_usado_9(Double valor) {
		this.campo_no_usado_9 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_banco
	* @return el valor de Fa_cod_banco
	*/ 
	public Double getFa_cod_banco() {
		return fa_cod_banco;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_banco
	* @param valor el valor de Fa_cod_banco
	*/ 
	public void setFa_cod_banco(Double valor) {
		this.fa_cod_banco = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cant_reg
	* @return el valor de Fa_c_cant_reg
	*/ 
	public Double getFa_c_cant_reg() {
		return fa_c_cant_reg;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cant_reg
	* @param valor el valor de Fa_c_cant_reg
	*/ 
	public void setFa_c_cant_reg(Double valor) {
		this.fa_c_cant_reg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_urb
	* @return el valor de Fa_r_urb
	*/ 
	public String getFa_r_urb() {
		return fa_r_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_urb
	* @param valor el valor de Fa_r_urb
	*/ 
	public void setFa_r_urb(String valor) {
		this.fa_r_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_tarea
	* @return el valor de Fa_marca_tarea
	*/ 
	public Double getFa_marca_tarea() {
		return fa_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_tarea
	* @param valor el valor de Fa_marca_tarea
	*/ 
	public void setFa_marca_tarea(Double valor) {
		this.fa_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_codigo_iva
	* @return el valor de Fa_r_codigo_iva
	*/ 
	public Double getFa_r_codigo_iva() {
		return fa_r_codigo_iva;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_codigo_iva
	* @param valor el valor de Fa_r_codigo_iva
	*/ 
	public void setFa_r_codigo_iva(Double valor) {
		this.fa_r_codigo_iva = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_nro_reintegro
	* @return el valor de Fa_r_nro_reintegro
	*/ 
	public Double getFa_r_nro_reintegro() {
		return fa_r_nro_reintegro;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_nro_reintegro
	* @param valor el valor de Fa_r_nro_reintegro
	*/ 
	public void setFa_r_nro_reintegro(Double valor) {
		this.fa_r_nro_reintegro = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_contrib_nvo
	* @return el valor de Fa_tipo_contrib_nvo
	*/ 
	public Double getFa_tipo_contrib_nvo() {
		return fa_tipo_contrib_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_contrib_nvo
	* @param valor el valor de Fa_tipo_contrib_nvo
	*/ 
	public void setFa_tipo_contrib_nvo(Double valor) {
		this.fa_tipo_contrib_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_abonado
	* @return el valor de Fa_r_abonado
	*/ 
	public Double getFa_r_abonado() {
		return fa_r_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_abonado
	* @param valor el valor de Fa_r_abonado
	*/ 
	public void setFa_r_abonado(Double valor) {
		this.fa_r_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_tipo_facturacion
	* @return el valor de Fa_r_tipo_facturacion
	*/ 
	public Double getFa_r_tipo_facturacion() {
		return fa_r_tipo_facturacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_tipo_facturacion
	* @param valor el valor de Fa_r_tipo_facturacion
	*/ 
	public void setFa_r_tipo_facturacion(Double valor) {
		this.fa_r_tipo_facturacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_hora_baja
	* @return el valor de Fa_c_fecha_hora_baja
	*/ 
	public java.util.Date getFa_c_fecha_hora_baja() {
		return fa_c_fecha_hora_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_hora_baja
	* @param valor el valor de Fa_c_fecha_hora_baja
	*/ 
	public void setFa_c_fecha_hora_baja(java.util.Date valor) {
		this.fa_c_fecha_hora_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_numero_pago
	* @return el valor de Fa_r_numero_pago
	*/ 
	public Double getFa_r_numero_pago() {
		return fa_r_numero_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_numero_pago
	* @param valor el valor de Fa_r_numero_pago
	*/ 
	public void setFa_r_numero_pago(Double valor) {
		this.fa_r_numero_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_codigo_movimiento
	* @return el valor de Fa_codigo_movimiento
	*/ 
	public String getFa_codigo_movimiento() {
		return fa_codigo_movimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_codigo_movimiento
	* @param valor el valor de Fa_codigo_movimiento
	*/ 
	public void setFa_codigo_movimiento(String valor) {
		this.fa_codigo_movimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_anio_lote
	* @return el valor de Fa_anio_lote
	*/ 
	public String getFa_anio_lote() {
		
		return fa_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_anio_lote
	* @param valor el valor de Fa_anio_lote
	*/ 
	public void setFa_anio_lote(String valor) {
		this.fa_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_banco_zona
	* @return el valor de Fa_banco_zona
	*/ 
	public Double getFa_banco_zona() {
		return fa_banco_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_banco_zona
	* @param valor el valor de Fa_banco_zona
	*/ 
	public void setFa_banco_zona(Double valor) {
		this.fa_banco_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_estado_asiento
	* @return el valor de Fa_c_estado_asiento
	*/ 
	public Double getFa_c_estado_asiento() {
		return fa_c_estado_asiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_estado_asiento
	* @param valor el valor de Fa_c_estado_asiento
	*/ 
	public void setFa_c_estado_asiento(Double valor) {
		this.fa_c_estado_asiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_abonado
	* @return el valor de Fa_abonado
	*/ 
	public Double getFa_abonado() {
		return fa_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_abonado
	* @param valor el valor de Fa_abonado
	*/ 
	public void setFa_abonado(Double valor) {
		this.fa_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_6
	* @return el valor de Campo_no_usado_6
	*/ 
	public Double getCampo_no_usado_6() {
		return campo_no_usado_6;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_6
	* @param valor el valor de Campo_no_usado_6
	*/ 
	public void setCampo_no_usado_6(Double valor) {
		this.campo_no_usado_6 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_interurb
	* @return el valor de Fa_r_interurb
	*/ 
	public String getFa_r_interurb() {
		return fa_r_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_interurb
	* @param valor el valor de Fa_r_interurb
	*/ 
	public void setFa_r_interurb(String valor) {
		this.fa_r_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_nro_copia
	* @return el valor de Fa_r_nro_copia
	*/ 
	public Double getFa_r_nro_copia() {
		return fa_r_nro_copia;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_nro_copia
	* @param valor el valor de Fa_r_nro_copia
	*/ 
	public void setFa_r_nro_copia(Double valor) {
		this.fa_r_nro_copia = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_pago_nvo
	* @return el valor de Fa_r_fecha_pago_nvo
	*/ 
	public java.util.Date getFa_r_fecha_pago_nvo() {
		return fa_r_fecha_pago_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_pago_nvo
	* @param valor el valor de Fa_r_fecha_pago_nvo
	*/ 
	public void setFa_r_fecha_pago_nvo(java.util.Date valor) {
		this.fa_r_fecha_pago_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_marca_pago
	* @return el valor de Fa_r_marca_pago
	*/ 
	public String getFa_r_marca_pago() {
		return fa_r_marca_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_marca_pago
	* @param valor el valor de Fa_r_marca_pago
	*/ 
	public void setFa_r_marca_pago(String valor) {
		this.fa_r_marca_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_zona_banco_pago_doble
	* @return el valor de Fa_zona_banco_pago_doble
	*/ 
	public Double getFa_zona_banco_pago_doble() {
		return fa_zona_banco_pago_doble;
	}
    	
	/**
	* Fija el valor del atributo Fa_zona_banco_pago_doble
	* @param valor el valor de Fa_zona_banco_pago_doble
	*/ 
	public void setFa_zona_banco_pago_doble(Double valor) {
		this.fa_zona_banco_pago_doble = valor;
	}
	/**
	* Recupera el valor del atributo Fa_usuario_terminal
	* @return el valor de Fa_usuario_terminal
	*/ 
	public String getFa_usuario_terminal() {
		return fa_usuario_terminal;
	}
    	
	/**
	* Fija el valor del atributo Fa_usuario_terminal
	* @param valor el valor de Fa_usuario_terminal
	*/ 
	public void setFa_usuario_terminal(String valor) {
		this.fa_usuario_terminal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_proc_ctgo
	* @return el valor de Fa_fecha_proc_ctgo
	*/ 
	public String getFa_fecha_proc_ctgo() {
		return fa_fecha_proc_ctgo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_proc_ctgo
	* @param valor el valor de Fa_fecha_proc_ctgo
	*/ 
	public void setFa_fecha_proc_ctgo(String valor) {
		this.fa_fecha_proc_ctgo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_marca_tarea
	* @return el valor de Fa_c_marca_tarea
	*/ 
	public Double getFa_c_marca_tarea() {
		return fa_c_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_marca_tarea
	* @param valor el valor de Fa_c_marca_tarea
	*/ 
	public void setFa_c_marca_tarea(Double valor) {
		this.fa_c_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_entrega
	* @return el valor de Fa_fecha_entrega
	*/ 
	public java.util.Date getFa_fecha_entrega() {
		return fa_fecha_entrega;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_entrega
	* @param valor el valor de Fa_fecha_entrega
	*/ 
	public void setFa_fecha_entrega(java.util.Date valor) {
		this.fa_fecha_entrega = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_recargo
	* @return el valor de Fa_marca_recargo
	*/ 
	public Double getFa_marca_recargo() {
		return fa_marca_recargo;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_recargo
	* @param valor el valor de Fa_marca_recargo
	*/ 
	public void setFa_marca_recargo(Double valor) {
		this.fa_marca_recargo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cod_concepto
	* @return el valor de Fa_c_cod_concepto
	*/ 
	public Double getFa_c_cod_concepto() {
		return fa_c_cod_concepto;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cod_concepto
	* @param valor el valor de Fa_c_cod_concepto
	*/ 
	public void setFa_c_cod_concepto(Double valor) {
		this.fa_c_cod_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Fa_interurb
	* @return el valor de Fa_interurb
	*/ 
	public String getFa_interurb() {
		return fa_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_interurb
	* @param valor el valor de Fa_interurb
	*/ 
	public void setFa_interurb(String valor) {
		this.fa_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_zona
	* @return el valor de Fa_r_zona
	*/ 
	public Double getFa_r_zona() {
		return fa_r_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_zona
	* @param valor el valor de Fa_r_zona
	*/ 
	public void setFa_r_zona(Double valor) {
		this.fa_r_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_ingreso_nvo
	* @return el valor de Fa_r_fecha_ingreso_nvo
	*/ 
	public java.util.Date getFa_r_fecha_ingreso_nvo() {
		return fa_r_fecha_ingreso_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_ingreso_nvo
	* @param valor el valor de Fa_r_fecha_ingreso_nvo
	*/ 
	public void setFa_r_fecha_ingreso_nvo(java.util.Date valor) {
		this.fa_r_fecha_ingreso_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_hora_ingreso
	* @return el valor de Fa_hora_ingreso
	*/ 
	public String getFa_hora_ingreso() {
		return fa_hora_ingreso;
	}
    	
	/**
	* Fija el valor del atributo Fa_hora_ingreso
	* @param valor el valor de Fa_hora_ingreso
	*/ 
	public void setFa_hora_ingreso(String valor) {
		this.fa_hora_ingreso = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_env_contab_nvo
	* @return el valor de Fa_c_fecha_env_contab_nvo
	*/ 
	public java.util.Date getFa_c_fecha_env_contab_nvo() {
		return fa_c_fecha_env_contab_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_env_contab_nvo
	* @param valor el valor de Fa_c_fecha_env_contab_nvo
	*/ 
	public void setFa_c_fecha_env_contab_nvo(java.util.Date valor) {
		this.fa_c_fecha_env_contab_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_estado_real_base
	* @return el valor de Fa_r_estado_real_base
	*/ 
	public Double getFa_r_estado_real_base() {
		return fa_r_estado_real_base;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_estado_real_base
	* @param valor el valor de Fa_r_estado_real_base
	*/ 
	public void setFa_r_estado_real_base(Double valor) {
		this.fa_r_estado_real_base = valor;
	}
	/**
	* Recupera el valor del atributo Fa_importe_iva
	* @return el valor de Fa_importe_iva
	*/ 
	public java.math.BigDecimal getFa_importe_iva() {
		return fa_importe_iva;
	}
    	
	/**
	* Fija el valor del atributo Fa_importe_iva
	* @param valor el valor de Fa_importe_iva
	*/ 
	public void setFa_importe_iva(java.math.BigDecimal valor) {
		this.fa_importe_iva = valor;
	}
	/**
	* Recupera el valor del atributo Fa_zona
	* @return el valor de Fa_zona
	*/ 
	public Double getFa_zona() {
		return fa_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_zona
	* @param valor el valor de Fa_zona
	*/ 
	public void setFa_zona(Double valor) {
		this.fa_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_ingreso_nvo
	* @return el valor de Fa_fecha_ingreso_nvo
	*/ 
	public java.util.Date getFa_fecha_ingreso_nvo() {
		return fa_fecha_ingreso_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_ingreso_nvo
	* @param valor el valor de Fa_fecha_ingreso_nvo
	*/ 
	public void setFa_fecha_ingreso_nvo(java.util.Date valor) {
		this.fa_fecha_ingreso_nvo = valor;
	}
		
	
	/** Julian Manfredi
	* Recupera el valor del atributo Fa_fecha_ingreso_nvo
	* @return el valor de Fa_fecha_ingreso_nvo
	*/ 
	public String getdesc_movimiento() {
		
		String descripcionVieja;
		String descripcionNueva;
		
		
		if(this.fa_codigo_movimiento== null)
			
		{
		
			return "SIN DESCRIPCION";
		}
		
		
		String codigo = this.fa_codigo_movimiento;
		
		int cod;
		cod=Integer.parseInt(codigo.substring( (codigo.length()-2),codigo.length()));
		
		
		
		
//		if(cod == 01)
//			
//		{	descripcionVieja = "DESINHIBIDO";
//			descripcionNueva = "DESINHIBIDO";
//		}
//		else
//			
//		{
//			descripcionVieja = "Prueba";
//			
//		}
		
		
	     switch (cod) {
         case 01:  
        	 descripcionVieja = "DESINHIBIDO";
         	 descripcionNueva = "DESINHIBIDO";

         case 21:  
        	 descripcionVieja = "SERVICIO DADO DE BAJA";
         	 descripcionNueva = "SERVICIO DADO DE BAJA";

                  break;
         case 22	:  
        	 descripcionVieja = "FACTURA DADA DE BAJA";
         	 descripcionNueva = "FACTURA DADA DE BAJA";

                  break;
         case 30:  
        	 descripcionVieja = "TE";
         	 descripcionNueva = "INH. TELEF. EQUIPOS";

                  break;
         case 31:  
        	 descripcionVieja = "MU";
         	 descripcionNueva = "INH. SERV. MEDIDO URB.";

                  break;
         case 32:  
        	 descripcionVieja = "INH. SERV. MED. INTERURB";
         	 descripcionNueva = "INH. SERV. MED. INTERURB";

                  break;
         case 33:  
        	 descripcionVieja = "SI";
         	 descripcionNueva = "INH. SEGUNDA INSTANCIA	";

                  break;
         case 34:  
        	 descripcionVieja = "MV";
         	 descripcionNueva = "POR CUENTA Y ORDEN DE MOVICOM	";

                  break;
         case 35:  
        	 descripcionVieja = "NP";
         	 descripcionNueva = "POR NEGATIVA DE PAGO";

                  break;
         case 36:  
        	 descripcionVieja = "PI";
         	 descripcionNueva = "PAGO INFORMADO	";

                  break;
         case 40:  
        	 descripcionVieja = "CD";
         	 descripcionNueva = "INHIBICION CALLING CARD";

                  break;
         case 41:  
        	 descripcionVieja = "CY";
         	 descripcionNueva = "INH. CALLING PARTY PAYS";

                  break;
         case 42:  
        	 descripcionVieja = "IC";
         	 descripcionNueva = "ROBO DE CABLES	";

                  break;
         case 43:  
        	 descripcionVieja = "IP";
         	 descripcionNueva = "INHIB.PAGO BCO. PROVINCIA";

                  break;
         case 44:  
        	 descripcionVieja = "CT";
         	 descripcionNueva = "INHIB.CONCEPTO TELINVER";

                  break;
         case 45	:  
        	 descripcionVieja = "CA";
         	 descripcionNueva = "INHIB.CONCEPTO AUDIOTEXTO";

                  break;
         case 46	:  
        	 descripcionVieja = "CH";
         	 descripcionNueva = "INHIB.CONC.HILO MUSICAL";

                  break;
                  
         case 48:  
        	 descripcionVieja = "CF";
         	 descripcionNueva = "INHIB.CONC. TELEFONOGRAMA";

                  break;
                  
         case 50:  
        	 descripcionVieja = "R2";
         	 descripcionNueva = "INHIB.CONVENIOS ESPECIALES";

                  break;
                  
         case 51	:  
        	 descripcionVieja = "INCOMUNICACION";
         	 descripcionNueva = "INHIB.ABONO";

                  break;
                  
         case 52:  
        	 descripcionVieja = "BAJA";
         	 descripcionNueva = "INHIB.SERVICIO MEDIDO";

                  break;
         case 53:  
        	 descripcionVieja = "RECLAMOS SERVICIO MEDIDO";
         	 descripcionNueva = "INHIB.COMUNICAC.INTERNAC.	";

                  break;
                  
         case 54:  
        	 descripcionVieja = "RECLAMOS VARIOS";
         	 descripcionNueva = "INHIB.IVA";

                  break;
         case 55:  
        	 descripcionVieja = "NO INNOVAR";
         	 descripcionNueva = "INHIB.CUENTA/ORDEN TERCEROS";

                  break;
                  
         case 56:  
        	 descripcionVieja = "REFACTURACION";
         	 descripcionNueva = "INHIB.CONCEPTOS VARIOS";

                  break;
                  
         case 57:  
        	 descripcionVieja = "IGC 1030";
         	 descripcionNueva = "INHIB.IGC 1030";

                  break;
         case 58:  
        	 descripcionVieja = "CC";
         	 descripcionNueva = "INHIB.CUOTA DE CONEXION";

                  break;
         case 59:  
        	 descripcionVieja = "RT";
         	 descripcionNueva = "RECLAMO COM.NAC.TELEC.";

                  break;
         case 61:  
        	 descripcionVieja = "RO";
         	 descripcionNueva = "RECLAMO DE PAGO EN OFICINA";

                  break;
         case 62:  
        	 descripcionVieja = "RB";
         	 descripcionNueva = "RECLAMO DE PAGO EN BANCO";

                  break;
                  
         case 70:  
        	 descripcionVieja = "JUICIOS INICIADOS";
         	 descripcionNueva = "JUICIOS INICIADOS";

                  break;
         case 71:  
        	 descripcionVieja = "CUENTA FINAL";
         	 descripcionNueva = "CUENTA FINAL";

                  break;
         case 72:  
        	 descripcionVieja = "COBRO JUDICIAL";
         	 descripcionNueva = "COBRO JUDICIAL";

                  break;
                  
                  
         case 82:  
        	 descripcionVieja = "COBRO EXTRAJUDICIAL";
         	 descripcionNueva = "COBRO EXTRAJUDICIAL";

                  break;
                  
         case 91:  
        	 descripcionVieja = "PROCESOS JUDICIALES";
         	 descripcionNueva = "PROCESOS JUDICIALES";

                  break;
                  
         case 92:  
        	 descripcionVieja = "QUIEBRAS";
         	 descripcionNueva = "QUIEBRAS";

                  break;
                  
         case 93:  
        	 descripcionVieja = "CONCURSOS PREVENTIVOS";
         	 descripcionNueva = "CONCURSOS PREVENTIVOS";

                  break;
                  
         case 94:  
        	 descripcionVieja = "OTROS JUICIOS";
         	 descripcionNueva = "OTROS JUICIOS";

                  break;
                  
         case 95:  
        	 descripcionVieja = "MEDIDAS ADMINISTRATIVAS";
         	 descripcionNueva = "MEDIDAS ADMINISTRATIVAS";

                  break;
                  
                    
         default: 
         	 descripcionVieja = "CODIGO DE MOVIEMIENTO 'SIN DESCRIPCION";
            descripcionNueva = "CODIGO DE MOVIMIENTO 'SIN DESCRIPCION'";
         
         break;
     }
		
	     
	     Date fecha = null;
	     
	     
	    
	     
	     
	     SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy-MM-dd");
	     
	     String strFecha = "1993-10-01";

	     try {
			fecha = formatoDeFecha.parse(strFecha);
		
	     } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	     
	     if(this.fa_fecha_ingreso_nvo != null)
	     {
	     
		     if (this.fa_fecha_ingreso_nvo.compareTo(fecha) < 0)
		     {
		    	 return descripcionVieja;
		    	 
		     }
		     else
		    	 
		     {
		    	 return descripcionNueva; 
		    	 
		     }
	     }
	     else
	    	 
	     {
	    	 return descripcionNueva; 	
	     }
	 }

	     
	     
	     
	     
	     
		

	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	* Fija el valor del atributo Fa_fecha_ingreso_nvo
	* @param valor el valor de Fa_fecha_ingreso_nvo
	*/ 
	public void setdesc_movimiento(String valor) {
		this.desc_movimiento = valor;
	}
		
	
	
	
	

	
	
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Fa_r_oficina=["+this.getFa_r_oficina()+"]\r\n");
				sb.append("Fa_c_importe_total=["+this.getFa_c_importe_total()+"]\r\n");
				sb.append("Fa_abono=["+this.getFa_abono()+"]\r\n");
				sb.append("Fa_r_cajero=["+this.getFa_r_cajero()+"]\r\n");
				sb.append("Fa_nro_tarjeta=["+this.getFa_nro_tarjeta()+"]\r\n");
				sb.append("Campo_no_usado_3=["+this.getCampo_no_usado_3()+"]\r\n");
				sb.append("Fa_r_usuario=["+this.getFa_r_usuario()+"]\r\n");
				sb.append("Fa_r_tipo_pago=["+this.getFa_r_tipo_pago()+"]\r\n");
				sb.append("Fa_c_estado_conciliacion=["+this.getFa_c_estado_conciliacion()+"]\r\n");
				sb.append("Fa_r_total_sin_recargo=["+this.getFa_r_total_sin_recargo()+"]\r\n");
				sb.append("Fa_ident_abo=["+this.getFa_ident_abo()+"]\r\n");
				sb.append("Fa_fecha_acreditacion_nvo=["+this.getFa_fecha_acreditacion_nvo()+"]\r\n");
				sb.append("Fa_localidad=["+this.getFa_localidad()+"]\r\n");
				sb.append("Fa_r_zona_pago=["+this.getFa_r_zona_pago()+"]\r\n");
				sb.append("Fa_marca_desimputacion=["+this.getFa_marca_desimputacion()+"]\r\n");
				sb.append("Fa_c_usuario_baja=["+this.getFa_c_usuario_baja()+"]\r\n");
				sb.append("Fa_c_fecha_pago_nvo=["+this.getFa_c_fecha_pago_nvo()+"]\r\n");
				sb.append("Fa_r_fecha_proceso_nvo=["+this.getFa_r_fecha_proceso_nvo()+"]\r\n");
				sb.append("Fa_r_categoria_gran_cliente=["+this.getFa_r_categoria_gran_cliente()+"]\r\n");
				sb.append("Fa_urb=["+this.getFa_urb()+"]\r\n");
				sb.append("Fa_c_fecha_hora_alta=["+this.getFa_c_fecha_hora_alta()+"]\r\n");
				sb.append("Fa_c_zona_pago=["+this.getFa_c_zona_pago()+"]\r\n");
				sb.append("Fa_c_fecha_hora_modif=["+this.getFa_c_fecha_hora_modif()+"]\r\n");
				sb.append("Campo_no_usado_7=["+this.getCampo_no_usado_7()+"]\r\n");
				sb.append("Fa_fecha_marca_nvo=["+this.getFa_fecha_marca_nvo()+"]\r\n");
				sb.append("Fa_serv_medido=["+this.getFa_serv_medido()+"]\r\n");
				sb.append("Fa_r_factura=["+this.getFa_r_factura()+"]\r\n");
				sb.append("Fa_c_tipo_registro=["+this.getFa_c_tipo_registro()+"]\r\n");
				sb.append("Fa_r_terminal=["+this.getFa_r_terminal()+"]\r\n");
				sb.append("Campo_no_usado=["+this.getCampo_no_usado()+"]\r\n");
				sb.append("Fa_razon_social=["+this.getFa_razon_social()+"]\r\n");
				sb.append("Campo_no_usado_8=["+this.getCampo_no_usado_8()+"]\r\n");
				sb.append("Fa_r_iva_mora_r=["+this.getFa_r_iva_mora_r()+"]\r\n");
				sb.append("Fa_domicilio=["+this.getFa_domicilio()+"]\r\n");
				sb.append("Fa_r_nro_nota_cred=["+this.getFa_r_nro_nota_cred()+"]\r\n");
				sb.append("Fa_c_cant_concp_c_apert=["+this.getFa_c_cant_concp_c_apert()+"]\r\n");
				sb.append("Fa_r_marca_tarea=["+this.getFa_r_marca_tarea()+"]\r\n");
				sb.append("Fa_r_hora_ingreso=["+this.getFa_r_hora_ingreso()+"]\r\n");
				sb.append("Fa_r_marca_carta_motivo=["+this.getFa_r_marca_carta_motivo()+"]\r\n");
				sb.append("Fa_cod_entrega=["+this.getFa_cod_entrega()+"]\r\n");
				sb.append("Fa_r_ident_abo=["+this.getFa_r_ident_abo()+"]\r\n");
				sb.append("Fa_r_fecha_reconcil_nvo=["+this.getFa_r_fecha_reconcil_nvo()+"]\r\n");
				sb.append("Fa_r_reconciliacion=["+this.getFa_r_reconciliacion()+"]\r\n");
				sb.append("Fa_fecha_desimputacion=["+this.getFa_fecha_desimputacion()+"]\r\n");
				sb.append("Campo_no_usado_5=["+this.getCampo_no_usado_5()+"]\r\n");
				sb.append("Fa_c_ident_aper_man=["+this.getFa_c_ident_aper_man()+"]\r\n");
				sb.append("Fa_c_nro_conciliacion=["+this.getFa_c_nro_conciliacion()+"]\r\n");
				sb.append("Fa_tipo_contrib=["+this.getFa_tipo_contrib()+"]\r\n");
				sb.append("Fa_r_importe_fuera_term=["+this.getFa_r_importe_fuera_term()+"]\r\n");
				sb.append("Campo_no_usado_4=["+this.getCampo_no_usado_4()+"]\r\n");
				sb.append("Fa_r_linea=["+this.getFa_r_linea()+"]\r\n");
				sb.append("Fa_cod_postal=["+this.getFa_cod_postal()+"]\r\n");
				sb.append("Fa_tipo_fact=["+this.getFa_tipo_fact()+"]\r\n");
				sb.append("Fa_c_cod_banco_depos=["+this.getFa_c_cod_banco_depos()+"]\r\n");
				sb.append("Fa_total_facturado=["+this.getFa_total_facturado()+"]\r\n");
				sb.append("Fa_r_fecha_pago_orig=["+this.getFa_r_fecha_pago_orig()+"]\r\n");
				sb.append("Fa_c_usuario_modif=["+this.getFa_c_usuario_modif()+"]\r\n");
				sb.append("Fa_oficina=["+this.getFa_oficina()+"]\r\n");
				sb.append("Fa_c_nro_boleta_deposito=["+this.getFa_c_nro_boleta_deposito()+"]\r\n");
				sb.append("Fa_linea=["+this.getFa_linea()+"]\r\n");
				sb.append("Fa_r_anio_lote=["+this.getFa_r_anio_lote()+"]\r\n");
				sb.append("Fa_r_codigo_movimiento=["+this.getFa_r_codigo_movimiento()+"]\r\n");
				sb.append("Fa_r_importe_cobrado=["+this.getFa_r_importe_cobrado()+"]\r\n");
				sb.append("Fa_importe_pdte_gire=["+this.getFa_importe_pdte_gire()+"]\r\n");
				sb.append("Fa_r_zona_pago_orig=["+this.getFa_r_zona_pago_orig()+"]\r\n");
				sb.append("Fa_c_usuario_alta=["+this.getFa_c_usuario_alta()+"]\r\n");
				sb.append("Fa_numero_pago=["+this.getFa_numero_pago()+"]\r\n");
				sb.append("Campo_no_usado_9=["+this.getCampo_no_usado_9()+"]\r\n");
				sb.append("Fa_cod_banco=["+this.getFa_cod_banco()+"]\r\n");
				sb.append("Fa_c_cant_reg=["+this.getFa_c_cant_reg()+"]\r\n");
				sb.append("Fa_r_urb=["+this.getFa_r_urb()+"]\r\n");
				sb.append("Fa_marca_tarea=["+this.getFa_marca_tarea()+"]\r\n");
				sb.append("Fa_r_codigo_iva=["+this.getFa_r_codigo_iva()+"]\r\n");
				sb.append("Fa_r_nro_reintegro=["+this.getFa_r_nro_reintegro()+"]\r\n");
				sb.append("Fa_tipo_contrib_nvo=["+this.getFa_tipo_contrib_nvo()+"]\r\n");
				sb.append("Fa_r_abonado=["+this.getFa_r_abonado()+"]\r\n");
				sb.append("Fa_r_tipo_facturacion=["+this.getFa_r_tipo_facturacion()+"]\r\n");
				sb.append("Fa_c_fecha_hora_baja=["+this.getFa_c_fecha_hora_baja()+"]\r\n");
				sb.append("Fa_r_numero_pago=["+this.getFa_r_numero_pago()+"]\r\n");
				sb.append("Fa_codigo_movimiento=["+this.getFa_codigo_movimiento()+"]\r\n");
				sb.append("Fa_anio_lote=["+this.getFa_anio_lote()+"]\r\n");
				sb.append("Fa_banco_zona=["+this.getFa_banco_zona()+"]\r\n");
				sb.append("Fa_c_estado_asiento=["+this.getFa_c_estado_asiento()+"]\r\n");
				sb.append("Fa_abonado=["+this.getFa_abonado()+"]\r\n");
				sb.append("Campo_no_usado_6=["+this.getCampo_no_usado_6()+"]\r\n");
				sb.append("Fa_r_interurb=["+this.getFa_r_interurb()+"]\r\n");
				sb.append("Fa_r_nro_copia=["+this.getFa_r_nro_copia()+"]\r\n");
				sb.append("Fa_r_fecha_pago_nvo=["+this.getFa_r_fecha_pago_nvo()+"]\r\n");
				sb.append("Fa_r_marca_pago=["+this.getFa_r_marca_pago()+"]\r\n");
				sb.append("Fa_zona_banco_pago_doble=["+this.getFa_zona_banco_pago_doble()+"]\r\n");
				sb.append("Fa_usuario_terminal=["+this.getFa_usuario_terminal()+"]\r\n");
				sb.append("Fa_fecha_proc_ctgo=["+this.getFa_fecha_proc_ctgo()+"]\r\n");
				sb.append("Fa_c_marca_tarea=["+this.getFa_c_marca_tarea()+"]\r\n");
				sb.append("Fa_fecha_entrega=["+this.getFa_fecha_entrega()+"]\r\n");
				sb.append("Fa_marca_recargo=["+this.getFa_marca_recargo()+"]\r\n");
				sb.append("Fa_c_cod_concepto=["+this.getFa_c_cod_concepto()+"]\r\n");
				sb.append("Fa_interurb=["+this.getFa_interurb()+"]\r\n");
				sb.append("Fa_r_zona=["+this.getFa_r_zona()+"]\r\n");
				sb.append("Fa_r_fecha_ingreso_nvo=["+this.getFa_r_fecha_ingreso_nvo()+"]\r\n");
				sb.append("Fa_hora_ingreso=["+this.getFa_hora_ingreso()+"]\r\n");
				sb.append("Fa_c_fecha_env_contab_nvo=["+this.getFa_c_fecha_env_contab_nvo()+"]\r\n");
				sb.append("Fa_r_estado_real_base=["+this.getFa_r_estado_real_base()+"]\r\n");
				sb.append("Fa_importe_iva=["+this.getFa_importe_iva()+"]\r\n");
				sb.append("Fa_zona=["+this.getFa_zona()+"]\r\n");
				sb.append("Fa_fecha_ingreso_nvo=["+this.getFa_fecha_ingreso_nvo()+"]\r\n");
				sb.append("desc_movimiento=["+this.getdesc_movimiento()+"]\r\n");
				
				
				return sb.toString();
	}
}