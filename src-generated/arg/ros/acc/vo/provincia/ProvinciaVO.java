package arg.ros.acc.vo.provincia;

/**
* ProvinciaVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ProvinciaVO {

	private Integer idprovincia;
	private String nombre;
	private Integer idpais;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public ProvinciaVO() {

	}
	
	/**
	* Recupera el valor del atributo Idprovincia
	* @return el valor de Idprovincia
	*/ 
	public Integer getIdprovincia() {
		return idprovincia;
	}
    	
	/**
	* Fija el valor del atributo Idprovincia
	* @param valor el valor de Idprovincia
	*/ 
	public void setIdprovincia(Integer valor) {
		this.idprovincia = valor;
	}
	/**
	* Recupera el valor del atributo Nombre
	* @return el valor de Nombre
	*/ 
	public String getNombre() {
		return nombre;
	}
    	
	/**
	* Fija el valor del atributo Nombre
	* @param valor el valor de Nombre
	*/ 
	public void setNombre(String valor) {
		this.nombre = valor;
	}
	/**
	* Recupera el valor del atributo Idpais
	* @return el valor de Idpais
	*/ 
	public Integer getIdpais() {
		return idpais;
	}
    	
	/**
	* Fija el valor del atributo Idpais
	* @param valor el valor de Idpais
	*/ 
	public void setIdpais(Integer valor) {
		this.idpais = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Idprovincia=["+this.getIdprovincia()+"]\r\n");
				sb.append("Nombre=["+this.getNombre()+"]\r\n");
				sb.append("Idpais=["+this.getIdpais()+"]\r\n");
				return sb.toString();
	}
}