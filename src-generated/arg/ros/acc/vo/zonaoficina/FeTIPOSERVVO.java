package arg.ros.acc.vo.zonaoficina;

/**
* FeTIPOSERVVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeTIPOSERVVO {

	private Double adabas_isn;
	private String dps_mu_seq;
	private Double fe_tipo_serv;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FeTIPOSERVVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Dps_mu_seq
	* @return el valor de Dps_mu_seq
	*/ 
	public String getDps_mu_seq() {
		return dps_mu_seq;
	}
    	
	/**
	* Fija el valor del atributo Dps_mu_seq
	* @param valor el valor de Dps_mu_seq
	*/ 
	public void setDps_mu_seq(String valor) {
		this.dps_mu_seq = valor;
	}
	/**
	* Recupera el valor del atributo Fe_tipo_serv
	* @return el valor de Fe_tipo_serv
	*/ 
	public Double getFe_tipo_serv() {
		return fe_tipo_serv;
	}
    	
	/**
	* Fija el valor del atributo Fe_tipo_serv
	* @param valor el valor de Fe_tipo_serv
	*/ 
	public void setFe_tipo_serv(Double valor) {
		this.fe_tipo_serv = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Dps_mu_seq=["+this.getDps_mu_seq()+"]\r\n");
				sb.append("Fe_tipo_serv=["+this.getFe_tipo_serv()+"]\r\n");
				return sb.toString();
	}
}