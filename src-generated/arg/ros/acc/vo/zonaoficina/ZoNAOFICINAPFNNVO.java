package arg.ros.acc.vo.zonaoficina;

/**
* ZoNAOFICINAPFNNVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ZoNAOFICINAPFNNVO {

	private Double adabas_isn;
	private Double fe_grupo_medido;
	private String fe_descripcion;
	private Double fe_zona_ofic_pla;
	private Double fe_cf_canal_dd;
	private Double fe_long_abo;
	private Double fe_oficina_baja_tf;
	private String fe_ciclo_sigeco;
	private Double fe_provincia;
	private Double fe_zona;
	private Double fe_zona_baja;
	private Double fe_zona_baja_tf;
	private String fe_marca_cdn;
	private Double fe_cod_interurbano;
	private String fe_marca_sigeco;
	private Double fe_nro_area;
	private Double fe_long_abo_tec;
	private Double fe_cf_canal_hh24;
	private java.util.Date fe_fecha_vigencia;
	private Double fe_zona_uo;
	private String fe_recolector_info;
	private String fe_adherido_843;
	private String fe_fundamental_baja;
	private String fe_cod_estado_zo_ofi;
	private Double fe_oficina;
	private String fe_region;
	private Double fe_cod_vto_proc;
	private String fe_territorio;
	private String fe_marca_ddi;
	private String fe_tecnologia;
	private Double fe_oficina_baja;
	private Double fe_cod_empresa;
	private String fe_unidad_central;
	private Double fe_cod_muni;
	private java.util.Date fe_fecha_carga;
	private String fe_nro_fundamental;
	private String fe_usuario;
	private Double fe_cod_vto_real;
	private Double tipoServicio;
	private Double ZONAVAR;
	private Double OFICINAVAR;
	private String LEYENDA;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public ZoNAOFICINAPFNNVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fe_grupo_medido
	* @return el valor de Fe_grupo_medido
	*/ 
	public Double getFe_grupo_medido() {
		return fe_grupo_medido;
	}
    	
	/**
	* Fija el valor del atributo Fe_grupo_medido
	* @param valor el valor de Fe_grupo_medido
	*/ 
	public void setFe_grupo_medido(Double valor) {
		this.fe_grupo_medido = valor;
	}
	/**
	* Recupera el valor del atributo Fe_descripcion
	* @return el valor de Fe_descripcion
	*/ 
	public String getFe_descripcion() {
		return fe_descripcion;
	}
    	
	/**
	* Fija el valor del atributo Fe_descripcion
	* @param valor el valor de Fe_descripcion
	*/ 
	public void setFe_descripcion(String valor) {
		this.fe_descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_ofic_pla
	* @return el valor de Fe_zona_ofic_pla
	*/ 
	public Double getFe_zona_ofic_pla() {
		return fe_zona_ofic_pla;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_ofic_pla
	* @param valor el valor de Fe_zona_ofic_pla
	*/ 
	public void setFe_zona_ofic_pla(Double valor) {
		this.fe_zona_ofic_pla = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cf_canal_dd
	* @return el valor de Fe_cf_canal_dd
	*/ 
	public Double getFe_cf_canal_dd() {
		return fe_cf_canal_dd;
	}
    	
	/**
	* Fija el valor del atributo Fe_cf_canal_dd
	* @param valor el valor de Fe_cf_canal_dd
	*/ 
	public void setFe_cf_canal_dd(Double valor) {
		this.fe_cf_canal_dd = valor;
	}
	/**
	* Recupera el valor del atributo Fe_long_abo
	* @return el valor de Fe_long_abo
	*/ 
	public Double getFe_long_abo() {
		return fe_long_abo;
	}
    	
	/**
	* Fija el valor del atributo Fe_long_abo
	* @param valor el valor de Fe_long_abo
	*/ 
	public void setFe_long_abo(Double valor) {
		this.fe_long_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fe_oficina_baja_tf
	* @return el valor de Fe_oficina_baja_tf
	*/ 
	public Double getFe_oficina_baja_tf() {
		return fe_oficina_baja_tf;
	}
    	
	/**
	* Fija el valor del atributo Fe_oficina_baja_tf
	* @param valor el valor de Fe_oficina_baja_tf
	*/ 
	public void setFe_oficina_baja_tf(Double valor) {
		this.fe_oficina_baja_tf = valor;
	}
	/**
	* Recupera el valor del atributo Fe_ciclo_sigeco
	* @return el valor de Fe_ciclo_sigeco
	*/ 
	public String getFe_ciclo_sigeco() {
		return fe_ciclo_sigeco;
	}
    	
	/**
	* Fija el valor del atributo Fe_ciclo_sigeco
	* @param valor el valor de Fe_ciclo_sigeco
	*/ 
	public void setFe_ciclo_sigeco(String valor) {
		this.fe_ciclo_sigeco = valor;
	}
	/**
	* Recupera el valor del atributo Fe_provincia
	* @return el valor de Fe_provincia
	*/ 
	public Double getFe_provincia() {
		return fe_provincia;
	}
    	
	/**
	* Fija el valor del atributo Fe_provincia
	* @param valor el valor de Fe_provincia
	*/ 
	public void setFe_provincia(Double valor) {
		this.fe_provincia = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona
	* @return el valor de Fe_zona
	*/ 
	public Double getFe_zona() {
		return fe_zona;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona
	* @param valor el valor de Fe_zona
	*/ 
	public void setFe_zona(Double valor) {
		this.fe_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_baja
	* @return el valor de Fe_zona_baja
	*/ 
	public Double getFe_zona_baja() {
		return fe_zona_baja;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_baja
	* @param valor el valor de Fe_zona_baja
	*/ 
	public void setFe_zona_baja(Double valor) {
		this.fe_zona_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_baja_tf
	* @return el valor de Fe_zona_baja_tf
	*/ 
	public Double getFe_zona_baja_tf() {
		return fe_zona_baja_tf;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_baja_tf
	* @param valor el valor de Fe_zona_baja_tf
	*/ 
	public void setFe_zona_baja_tf(Double valor) {
		this.fe_zona_baja_tf = valor;
	}
	/**
	* Recupera el valor del atributo Fe_marca_cdn
	* @return el valor de Fe_marca_cdn
	*/ 
	public String getFe_marca_cdn() {
		return fe_marca_cdn;
	}
    	
	/**
	* Fija el valor del atributo Fe_marca_cdn
	* @param valor el valor de Fe_marca_cdn
	*/ 
	public void setFe_marca_cdn(String valor) {
		this.fe_marca_cdn = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_interurbano
	* @return el valor de Fe_cod_interurbano
	*/ 
	public Double getFe_cod_interurbano() {
		return fe_cod_interurbano;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_interurbano
	* @param valor el valor de Fe_cod_interurbano
	*/ 
	public void setFe_cod_interurbano(Double valor) {
		this.fe_cod_interurbano = valor;
	}
	/**
	* Recupera el valor del atributo Fe_marca_sigeco
	* @return el valor de Fe_marca_sigeco
	*/ 
	public String getFe_marca_sigeco() {
		return fe_marca_sigeco;
	}
    	
	/**
	* Fija el valor del atributo Fe_marca_sigeco
	* @param valor el valor de Fe_marca_sigeco
	*/ 
	public void setFe_marca_sigeco(String valor) {
		this.fe_marca_sigeco = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_area
	* @return el valor de Fe_nro_area
	*/ 
	public Double getFe_nro_area() {
		return fe_nro_area;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_area
	* @param valor el valor de Fe_nro_area
	*/ 
	public void setFe_nro_area(Double valor) {
		this.fe_nro_area = valor;
	}
	/**
	* Recupera el valor del atributo Fe_long_abo_tec
	* @return el valor de Fe_long_abo_tec
	*/ 
	public Double getFe_long_abo_tec() {
		return fe_long_abo_tec;
	}
    	
	/**
	* Fija el valor del atributo Fe_long_abo_tec
	* @param valor el valor de Fe_long_abo_tec
	*/ 
	public void setFe_long_abo_tec(Double valor) {
		this.fe_long_abo_tec = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cf_canal_hh24
	* @return el valor de Fe_cf_canal_hh24
	*/ 
	public Double getFe_cf_canal_hh24() {
		return fe_cf_canal_hh24;
	}
    	
	/**
	* Fija el valor del atributo Fe_cf_canal_hh24
	* @param valor el valor de Fe_cf_canal_hh24
	*/ 
	public void setFe_cf_canal_hh24(Double valor) {
		this.fe_cf_canal_hh24 = valor;
	}
	/**
	* Recupera el valor del atributo Fe_fecha_vigencia
	* @return el valor de Fe_fecha_vigencia
	*/ 
	public java.util.Date getFe_fecha_vigencia() {
		return fe_fecha_vigencia;
	}
    	
	/**
	* Fija el valor del atributo Fe_fecha_vigencia
	* @param valor el valor de Fe_fecha_vigencia
	*/ 
	public void setFe_fecha_vigencia(java.util.Date valor) {
		this.fe_fecha_vigencia = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_uo
	* @return el valor de Fe_zona_uo
	*/ 
	public Double getFe_zona_uo() {
		return fe_zona_uo;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_uo
	* @param valor el valor de Fe_zona_uo
	*/ 
	public void setFe_zona_uo(Double valor) {
		this.fe_zona_uo = valor;
	}
	/**
	* Recupera el valor del atributo Fe_recolector_info
	* @return el valor de Fe_recolector_info
	*/ 
	public String getFe_recolector_info() {
		return fe_recolector_info;
	}
    	
	/**
	* Fija el valor del atributo Fe_recolector_info
	* @param valor el valor de Fe_recolector_info
	*/ 
	public void setFe_recolector_info(String valor) {
		this.fe_recolector_info = valor;
	}
	/**
	* Recupera el valor del atributo Fe_adherido_843
	* @return el valor de Fe_adherido_843
	*/ 
	public String getFe_adherido_843() {
		return fe_adherido_843;
	}
    	
	/**
	* Fija el valor del atributo Fe_adherido_843
	* @param valor el valor de Fe_adherido_843
	*/ 
	public void setFe_adherido_843(String valor) {
		this.fe_adherido_843 = valor;
	}
	/**
	* Recupera el valor del atributo Fe_fundamental_baja
	* @return el valor de Fe_fundamental_baja
	*/ 
	public String getFe_fundamental_baja() {
		return fe_fundamental_baja;
	}
    	
	/**
	* Fija el valor del atributo Fe_fundamental_baja
	* @param valor el valor de Fe_fundamental_baja
	*/ 
	public void setFe_fundamental_baja(String valor) {
		this.fe_fundamental_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_estado_zo_ofi
	* @return el valor de Fe_cod_estado_zo_ofi
	*/ 
	public String getFe_cod_estado_zo_ofi() {
		return fe_cod_estado_zo_ofi;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_estado_zo_ofi
	* @param valor el valor de Fe_cod_estado_zo_ofi
	*/ 
	public void setFe_cod_estado_zo_ofi(String valor) {
		this.fe_cod_estado_zo_ofi = valor;
	}
	/**
	* Recupera el valor del atributo Fe_oficina
	* @return el valor de Fe_oficina
	*/ 
	public Double getFe_oficina() {
		return fe_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fe_oficina
	* @param valor el valor de Fe_oficina
	*/ 
	public void setFe_oficina(Double valor) {
		this.fe_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fe_region
	* @return el valor de Fe_region
	*/ 
	public String getFe_region() {
		return fe_region;
	}
    	
	/**
	* Fija el valor del atributo Fe_region
	* @param valor el valor de Fe_region
	*/ 
	public void setFe_region(String valor) {
		this.fe_region = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_vto_proc
	* @return el valor de Fe_cod_vto_proc
	*/ 
	public Double getFe_cod_vto_proc() {
		return fe_cod_vto_proc;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_vto_proc
	* @param valor el valor de Fe_cod_vto_proc
	*/ 
	public void setFe_cod_vto_proc(Double valor) {
		this.fe_cod_vto_proc = valor;
	}
	/**
	* Recupera el valor del atributo Fe_territorio
	* @return el valor de Fe_territorio
	*/ 
	public String getFe_territorio() {
		return fe_territorio;
	}
    	
	/**
	* Fija el valor del atributo Fe_territorio
	* @param valor el valor de Fe_territorio
	*/ 
	public void setFe_territorio(String valor) {
		this.fe_territorio = valor;
	}
	/**
	* Recupera el valor del atributo Fe_marca_ddi
	* @return el valor de Fe_marca_ddi
	*/ 
	public String getFe_marca_ddi() {
		return fe_marca_ddi;
	}
    	
	/**
	* Fija el valor del atributo Fe_marca_ddi
	* @param valor el valor de Fe_marca_ddi
	*/ 
	public void setFe_marca_ddi(String valor) {
		this.fe_marca_ddi = valor;
	}
	/**
	* Recupera el valor del atributo Fe_tecnologia
	* @return el valor de Fe_tecnologia
	*/ 
	public String getFe_tecnologia() {
		return fe_tecnologia;
	}
    	
	/**
	* Fija el valor del atributo Fe_tecnologia
	* @param valor el valor de Fe_tecnologia
	*/ 
	public void setFe_tecnologia(String valor) {
		this.fe_tecnologia = valor;
	}
	/**
	* Recupera el valor del atributo Fe_oficina_baja
	* @return el valor de Fe_oficina_baja
	*/ 
	public Double getFe_oficina_baja() {
		return fe_oficina_baja;
	}
    	
	/**
	* Fija el valor del atributo Fe_oficina_baja
	* @param valor el valor de Fe_oficina_baja
	*/ 
	public void setFe_oficina_baja(Double valor) {
		this.fe_oficina_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_empresa
	* @return el valor de Fe_cod_empresa
	*/ 
	public Double getFe_cod_empresa() {
		return fe_cod_empresa;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_empresa
	* @param valor el valor de Fe_cod_empresa
	*/ 
	public void setFe_cod_empresa(Double valor) {
		this.fe_cod_empresa = valor;
	}
	/**
	* Recupera el valor del atributo Fe_unidad_central
	* @return el valor de Fe_unidad_central
	*/ 
	public String getFe_unidad_central() {
		return fe_unidad_central;
	}
    	
	/**
	* Fija el valor del atributo Fe_unidad_central
	* @param valor el valor de Fe_unidad_central
	*/ 
	public void setFe_unidad_central(String valor) {
		this.fe_unidad_central = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_muni
	* @return el valor de Fe_cod_muni
	*/ 
	public Double getFe_cod_muni() {
		return fe_cod_muni;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_muni
	* @param valor el valor de Fe_cod_muni
	*/ 
	public void setFe_cod_muni(Double valor) {
		this.fe_cod_muni = valor;
	}
	/**
	* Recupera el valor del atributo Fe_fecha_carga
	* @return el valor de Fe_fecha_carga
	*/ 
	public java.util.Date getFe_fecha_carga() {
		return fe_fecha_carga;
	}
    	
	/**
	* Fija el valor del atributo Fe_fecha_carga
	* @param valor el valor de Fe_fecha_carga
	*/ 
	public void setFe_fecha_carga(java.util.Date valor) {
		this.fe_fecha_carga = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_fundamental
	* @return el valor de Fe_nro_fundamental
	*/ 
	public String getFe_nro_fundamental() {
		return fe_nro_fundamental;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_fundamental
	* @param valor el valor de Fe_nro_fundamental
	*/ 
	public void setFe_nro_fundamental(String valor) {
		this.fe_nro_fundamental = valor;
	}
	/**
	* Recupera el valor del atributo Fe_usuario
	* @return el valor de Fe_usuario
	*/ 
	public String getFe_usuario() {
		return fe_usuario;
	}
    	
	/**
	* Fija el valor del atributo Fe_usuario
	* @param valor el valor de Fe_usuario
	*/ 
	public void setFe_usuario(String valor) {
		this.fe_usuario = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_vto_real
	* @return el valor de Fe_cod_vto_real
	*/ 
	public Double getFe_cod_vto_real() {
		return fe_cod_vto_real;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_vto_real
	* @param valor el valor de Fe_cod_vto_real
	*/ 
	public void setFe_cod_vto_real(Double valor) {
		this.fe_cod_vto_real = valor;
	}
	
	
	/*Agregados  */
	
	public Double gettipoServicio() {
		return tipoServicio ;
	}
	
	
	public void settipoServicio(Double tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	
	
	
	
	/*Agregados  */
	
	public Double getZonaVar() {
		return  ZONAVAR ;
	}
	
	
	public void setZonaVar(Double Zona) {
		this.ZONAVAR = Zona;
	}
	
	
	/*Agregados  */
	
	public Double getOficinaVar() {
		return  OFICINAVAR ;
	}
	
	
	public void setOficinaVar(Double oficina) {
		this.OFICINAVAR = oficina;
	}
	
	/*Agregados  */
	
	public String getLeyenda() {
		return  LEYENDA ;
	}
	
	
	public void setLeyenda(String leyenda) {
		this.LEYENDA = leyenda;
	}
		
	
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Fe_grupo_medido=["+this.getFe_grupo_medido()+"]\r\n");
				sb.append("Fe_descripcion=["+this.getFe_descripcion()+"]\r\n");
				sb.append("Fe_zona_ofic_pla=["+this.getFe_zona_ofic_pla()+"]\r\n");
				sb.append("Fe_cf_canal_dd=["+this.getFe_cf_canal_dd()+"]\r\n");
				sb.append("Fe_long_abo=["+this.getFe_long_abo()+"]\r\n");
				sb.append("Fe_oficina_baja_tf=["+this.getFe_oficina_baja_tf()+"]\r\n");
				sb.append("Fe_ciclo_sigeco=["+this.getFe_ciclo_sigeco()+"]\r\n");
				sb.append("Fe_provincia=["+this.getFe_provincia()+"]\r\n");
				sb.append("Fe_zona=["+this.getFe_zona()+"]\r\n");
				sb.append("Fe_zona_baja=["+this.getFe_zona_baja()+"]\r\n");
				sb.append("Fe_zona_baja_tf=["+this.getFe_zona_baja_tf()+"]\r\n");
				sb.append("Fe_marca_cdn=["+this.getFe_marca_cdn()+"]\r\n");
				sb.append("Fe_cod_interurbano=["+this.getFe_cod_interurbano()+"]\r\n");
				sb.append("Fe_marca_sigeco=["+this.getFe_marca_sigeco()+"]\r\n");
				sb.append("Fe_nro_area=["+this.getFe_nro_area()+"]\r\n");
				sb.append("Fe_long_abo_tec=["+this.getFe_long_abo_tec()+"]\r\n");
				sb.append("Fe_cf_canal_hh24=["+this.getFe_cf_canal_hh24()+"]\r\n");
				sb.append("Fe_fecha_vigencia=["+this.getFe_fecha_vigencia()+"]\r\n");
				sb.append("Fe_zona_uo=["+this.getFe_zona_uo()+"]\r\n");
				sb.append("Fe_recolector_info=["+this.getFe_recolector_info()+"]\r\n");
				sb.append("Fe_adherido_843=["+this.getFe_adherido_843()+"]\r\n");
				sb.append("Fe_fundamental_baja=["+this.getFe_fundamental_baja()+"]\r\n");
				sb.append("Fe_cod_estado_zo_ofi=["+this.getFe_cod_estado_zo_ofi()+"]\r\n");
				sb.append("Fe_oficina=["+this.getFe_oficina()+"]\r\n");
				sb.append("Fe_region=["+this.getFe_region()+"]\r\n");
				sb.append("Fe_cod_vto_proc=["+this.getFe_cod_vto_proc()+"]\r\n");
				sb.append("Fe_territorio=["+this.getFe_territorio()+"]\r\n");
				sb.append("Fe_marca_ddi=["+this.getFe_marca_ddi()+"]\r\n");
				sb.append("Fe_tecnologia=["+this.getFe_tecnologia()+"]\r\n");
				sb.append("Fe_oficina_baja=["+this.getFe_oficina_baja()+"]\r\n");
				sb.append("Fe_cod_empresa=["+this.getFe_cod_empresa()+"]\r\n");
				sb.append("Fe_unidad_central=["+this.getFe_unidad_central()+"]\r\n");
				sb.append("Fe_cod_muni=["+this.getFe_cod_muni()+"]\r\n");
				sb.append("Fe_fecha_carga=["+this.getFe_fecha_carga()+"]\r\n");
				sb.append("Fe_nro_fundamental=["+this.getFe_nro_fundamental()+"]\r\n");
				sb.append("Fe_usuario=["+this.getFe_usuario()+"]\r\n");
				sb.append("Fe_cod_vto_real=["+this.getFe_cod_vto_real()+"]\r\n");
				
				/*agregados*/
				sb.append("tipoServicio=["+this.gettipoServicio()+"]\r\n");
				sb.append("ZONAVAR=["+this.getZonaVar()+"]\r\n");
				sb.append("OFICINAVAR=["+this.getOficinaVar()+"]\r\n");
				sb.append("LEYENDA=["+this.getLeyenda()+"]\r\n");
				
								
				return sb.toString();
	}
}