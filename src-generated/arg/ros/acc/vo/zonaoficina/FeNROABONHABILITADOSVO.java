package arg.ros.acc.vo.zonaoficina;

/**
* FeNROABONHABILITADOSVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeNROABONHABILITADOSVO {

	private Double adabas_isn;
	private String dps_pe_seq;
	private Double fe_nro_habil_hasta;
	private Double fe_nro_habil_desde;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FeNROABONHABILITADOSVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Dps_pe_seq
	* @return el valor de Dps_pe_seq
	*/ 
	public String getDps_pe_seq() {
		return dps_pe_seq;
	}
    	
	/**
	* Fija el valor del atributo Dps_pe_seq
	* @param valor el valor de Dps_pe_seq
	*/ 
	public void setDps_pe_seq(String valor) {
		this.dps_pe_seq = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_habil_hasta
	* @return el valor de Fe_nro_habil_hasta
	*/ 
	public Double getFe_nro_habil_hasta() {
		return fe_nro_habil_hasta;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_habil_hasta
	* @param valor el valor de Fe_nro_habil_hasta
	*/ 
	public void setFe_nro_habil_hasta(Double valor) {
		this.fe_nro_habil_hasta = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_habil_desde
	* @return el valor de Fe_nro_habil_desde
	*/ 
	public Double getFe_nro_habil_desde() {
		return fe_nro_habil_desde;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_habil_desde
	* @param valor el valor de Fe_nro_habil_desde
	*/ 
	public void setFe_nro_habil_desde(Double valor) {
		this.fe_nro_habil_desde = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Dps_pe_seq=["+this.getDps_pe_seq()+"]\r\n");
				sb.append("Fe_nro_habil_hasta=["+this.getFe_nro_habil_hasta()+"]\r\n");
				sb.append("Fe_nro_habil_desde=["+this.getFe_nro_habil_desde()+"]\r\n");
				return sb.toString();
	}
}