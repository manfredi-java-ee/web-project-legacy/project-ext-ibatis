package arg.ros.acc.vo.localidad;

/**
* LocalidadVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class LocalidadVO {

	private Double idlocalidad;
	private String nombre;
	private Integer idprovincia;
	private Integer codigopostal;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public LocalidadVO() {

	}
	
	/**
	* Recupera el valor del atributo Idlocalidad
	* @return el valor de Idlocalidad
	*/ 
	public Double getIdlocalidad() {
		return idlocalidad;
	}
    	
	/**
	* Fija el valor del atributo Idlocalidad
	* @param valor el valor de Idlocalidad
	*/ 
	public void setIdlocalidad(Double valor) {
		this.idlocalidad = valor;
	}
	/**
	* Recupera el valor del atributo Nombre
	* @return el valor de Nombre
	*/ 
	public String getNombre() {
		return nombre;
	}
    	
	/**
	* Fija el valor del atributo Nombre
	* @param valor el valor de Nombre
	*/ 
	public void setNombre(String valor) {
		this.nombre = valor;
	}
	/**
	* Recupera el valor del atributo Idprovincia
	* @return el valor de Idprovincia
	*/ 
	public Integer getIdprovincia() {
		return idprovincia;
	}
    	
	/**
	* Fija el valor del atributo Idprovincia
	* @param valor el valor de Idprovincia
	*/ 
	public void setIdprovincia(Integer valor) {
		this.idprovincia = valor;
	}
	/**
	* Recupera el valor del atributo Codigopostal
	* @return el valor de Codigopostal
	*/ 
	public Integer getCodigopostal() {
		return codigopostal;
	}
    	
	/**
	* Fija el valor del atributo Codigopostal
	* @param valor el valor de Codigopostal
	*/ 
	public void setCodigopostal(Integer valor) {
		this.codigopostal = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Idlocalidad=["+this.getIdlocalidad()+"]\r\n");
				sb.append("Nombre=["+this.getNombre()+"]\r\n");
				sb.append("Idprovincia=["+this.getIdprovincia()+"]\r\n");
				sb.append("Codigopostal=["+this.getCodigopostal()+"]\r\n");
				return sb.toString();
	}
}