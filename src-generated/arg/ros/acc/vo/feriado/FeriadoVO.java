package arg.ros.acc.vo.feriado;

/**
* FeriadoVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeriadoVO {

	private Integer idferiado;
	private Integer anio;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FeriadoVO() {

	}
	
	/**
	* Recupera el valor del atributo Idferiado
	* @return el valor de Idferiado
	*/ 
	public Integer getIdferiado() {
		return idferiado;
	}
    	
	/**
	* Fija el valor del atributo Idferiado
	* @param valor el valor de Idferiado
	*/ 
	public void setIdferiado(Integer valor) {
		this.idferiado = valor;
	}
	/**
	* Recupera el valor del atributo Anio
	* @return el valor de Anio
	*/ 
	public Integer getAnio() {
		return anio;
	}
    	
	/**
	* Fija el valor del atributo Anio
	* @param valor el valor de Anio
	*/ 
	public void setAnio(Integer valor) {
		this.anio = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Idferiado=["+this.getIdferiado()+"]\r\n");
				sb.append("Anio=["+this.getAnio()+"]\r\n");
				return sb.toString();
	}
}