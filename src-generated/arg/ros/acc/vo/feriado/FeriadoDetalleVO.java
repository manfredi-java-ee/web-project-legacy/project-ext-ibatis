package arg.ros.acc.vo.feriado;

/**
* FeriadoDetalleVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeriadoDetalleVO {

	private Double idferiadodetalle;
	private Integer idferiado;
	private String descripcion;
	private java.util.Date fecha;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public FeriadoDetalleVO() {

	}
	
	/**
	* Recupera el valor del atributo Idferiadodetalle
	* @return el valor de Idferiadodetalle
	*/ 
	public Double getIdferiadodetalle() {
		return idferiadodetalle;
	}
    	
	/**
	* Fija el valor del atributo Idferiadodetalle
	* @param valor el valor de Idferiadodetalle
	*/ 
	public void setIdferiadodetalle(Double valor) {
		this.idferiadodetalle = valor;
	}
	/**
	* Recupera el valor del atributo Idferiado
	* @return el valor de Idferiado
	*/ 
	public Integer getIdferiado() {
		return idferiado;
	}
    	
	/**
	* Fija el valor del atributo Idferiado
	* @param valor el valor de Idferiado
	*/ 
	public void setIdferiado(Integer valor) {
		this.idferiado = valor;
	}
	/**
	* Recupera el valor del atributo Descripcion
	* @return el valor de Descripcion
	*/ 
	public String getDescripcion() {
		return descripcion;
	}
    	
	/**
	* Fija el valor del atributo Descripcion
	* @param valor el valor de Descripcion
	*/ 
	public void setDescripcion(String valor) {
		this.descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Fecha
	* @return el valor de Fecha
	*/ 
	public java.util.Date getFecha() {
		return fecha;
	}
    	
	/**
	* Fija el valor del atributo Fecha
	* @param valor el valor de Fecha
	*/ 
	public void setFecha(java.util.Date valor) {
		this.fecha = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Idferiadodetalle=["+this.getIdferiadodetalle()+"]\r\n");
				sb.append("Idferiado=["+this.getIdferiado()+"]\r\n");
				sb.append("Descripcion=["+this.getDescripcion()+"]\r\n");
				sb.append("Fecha=["+this.getFecha()+"]\r\n");
				return sb.toString();
	}
}