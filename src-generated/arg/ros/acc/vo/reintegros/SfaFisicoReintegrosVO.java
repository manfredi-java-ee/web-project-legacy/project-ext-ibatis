package arg.ros.acc.vo.reintegros;

import java.util.Date;

/**
* SfaFisicoReintegrosVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class SfaFisicoReintegrosVO {

	private Double campo_no_usado_17;
	private String fa_no_hora_alta_nc;
	private Double campo_no_usado_18;
	private java.util.Date fa_no_hora_alta_nd;
	private Double campo_no_usado_19;
	private java.util.Date fa_no_hora_mod_nc;
	private Double campo_no_usado_20;
	private java.util.Date fa_no_hora_mod_nd;
	private Double fa_no_cat_gran_cli;
	private Double fa_no_nro_gran_cli;
	private String fa_no_interurb;
	private String fa_no_urb;
	private String fa_no_linea;
	private Double fa_no_ident_abo;
	private String fa_re_interurb;
	private String fa_re_urb;
	private String fa_re_linea;
	private Double fa_re_ident_abo;
	private String fa_re_cod_postal_loc;
	private String fa_re_cod_postal_loc_benf;
	private java.util.Date fa_re_fecha_solic_nvo;
	private java.util.Date fa_re_fecha_emis_op_nvo;
	private String fa_re_fecha_pago_nvo;
	private java.util.Date fa_re_fecha_gener_cheque_nvo;
	private java.util.Date fa_re_fecha_para_dolariz_nvo;
	private java.util.Date fa_re_fecha_alta_nvo;
	private java.util.Date fa_re_fecha_modific_nvo;
	private java.util.Date fa_re_fecha_baja_nvo;
	private java.util.Date fa_re_fecha_superv_nvo;
	private java.util.Date fa_re_fecha_autoriz_nvo;
	private String fa_re_fecha_pago_misc;
	private Double fa_re_origen;
	private java.util.Date fa_no_fecha_alta_reintegro_nvo;
	private Double fa_no_fec_ing_contable_nc_nvo;
	private Double fa_no_fec_ing_contable_nd_nvo;
	private java.util.Date fa_no_fecha_para_dolariz_nvo;
	private java.util.Date fa_no_fecha_envio_gl_nc_nvo;
	private java.util.Date fa_no_fecha_envio_gl_nd_nvo;
	private java.util.Date fa_no_fecha_alta_nc_nvo;
	private java.util.Date fa_no_fecha_alta_nd_nvo;
	private java.util.Date fa_no_fecha_mod_nc_nvo;
	private java.util.Date fa_no_fecha_mod_nd_nvo;
	private Double fa_re_imputacion_estado;
	private String fa_re_cod_barra;
	private Double fa_re_e_marca_tarea;
	private Double fa_re_e_banco;
	private String fa_re_e_imp_pagado;
	private java.util.Date fa_re_e_fecha_acreditac;
	private java.util.Date fa_re_e_fecha_proceso;
	private Double fa_re_e_sucursal;
	private java.util.Date fa_re_e_fecha_pres;
	private java.util.Date fa_re_e_fecha_pago;
	private String fa_re_fecha_bloqueo;
	private String fa_re_usuario_bloqueo;
	private java.util.Date fa_re_hora_bloqueo;
	private Double fa_re_pago_duplicado;
	private Double fa_re_pcia_base_perc;
	private Double fa_re_nro_movimiento;
	private java.util.Date fa_re_fecha_desbloqueo;
	private String fa_re_usu_desbloqueo;
	private String fa_re_fecha_migracion;
	private String fa_re_cliente_atis;
	private String fa_re_cuenta_atis;
	private String fa_re_motivo_transf;
	private String fa_re_operador_loc;
	private String fa_re_gp_apynom;
	private String fa_re_gp_tipo_doc;
	private String fa_re_gp_nro_doc;
	private String fa_re_gp_domic;
	private String fa_re_gp_cod_postal;
	private String fa_re_gp_localidad;
	private String fa_re_gp_provincia;
	private String fa_re_numero_pago;
	private Double adabas_isn;
	private String fa_co_clave_numero;
	private Double fa_co_prox_numero;
	private Double fa_co_ident_abo;
	private Double fa_re_num_reintegro;
	private Double fa_re_zona_solic;
	private Double campo_no_usado_1;
	private Double fa_re_zona;
	private Double fa_re_oficina;
	private Double fa_re_abonado;
	private Double fa_re_tip_fact;
	private Double fa_re_anio_lote;
	private String fa_re_serie_meg;
	private Double fa_re_n_contrato_meg;
	private Double fa_re_n_cuota_meg;
	private Double fa_re_nro_factura;
	private Double campo_no_usado_2;
	
	private Double fa_re_motivo;
	// AGREGADO DESCRIPCION DEL CAMPO MOTIVO
	private String FA_RE_MOTIVO_DESCRIPCION;
	

	private Double fa_re_estado;
    // AGREGADO DESCRIPCION DEL CAMPO ESTADO
	private String FA_RE_ESTADO_DESCRIPCION;
	
	
	
	
	
	private Double fa_re_nro_copia;
	//AGREGADO DESCRIPCION DEL CAMPO NRO COPIA
	private String FA_RE_NRO_COPIA_DESCRIPCION;
	
	
	private java.math.BigDecimal fa_re_importe_tot_concepto;
	private java.math.BigDecimal fa_re_importe_tot_iva;
	private java.math.BigDecimal fa_re_importe_tot_iva_adic;
	private java.math.BigDecimal fa_re_importe_mora;
	private java.math.BigDecimal fa_re_importe_tot_actualiz;
	private java.math.BigDecimal fa_re_importe_tot_ing_b_adic;
	private String fa_re_apell_nom_lin1;
	private String fa_re_an_domic_lin2;
	private String fa_re_domicil_lin3;
	private String fa_re_codpos_loc_lin4;
	private String fa_re_dpto_lin6;
	private Double fa_re_categoria;
	private Double fa_re_tip_contrib;
	private Double fa_re_nro_contrib;
	private String fa_re_apell_nom_lin1_benf;
	
	//AGREGADO DE LA DESCRIPCION PARA EL CAMPO APELLIDO
	
	private String FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION;
	
	
	private String fa_re_an_domic_lin2_benf;
	
	//AGREGADO DE LA DESCRIPCION PARA EL CAMPO DOMICILIO
	private String FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION;
	
	
	private String fa_re_domicil_lin3_benf;
	private String fa_re_codpos_loc_lin4_benf;
	private String fa_re_dpto_loc_lin5_benf;
	private Double fa_re_tipo_contrib_loc;
	private Double fa_re_nro_contrib_loc;
	private Double fa_re_tipo_documento;
	private Double fa_re_num_documento;
	private Double fa_re_n_cuenta_clie_gob;
	private Double fa_re_dias_actualiz;
	private Double fa_re_valor_pulso;
	private Double campo_no_usado_3;
	private Double fa_re_zona_pago;
	private Double fa_re_cajero;
	
	private Double fa_re_tipo_pago;
	// AGREGADO DESCRIPCION DEL CAMPO TIPO DE PAGO
	private String FA_RE_TIPO_PAGO_DESCRIPCION;
	
		
	private Double campo_no_usado_4;
	private Double campo_no_usado_5;
	private Double fa_re_marca_tarea;
	private Double fa_re_nro_orden_pago;
	private Double campo_no_usado_6;
	private String fa_re_hora_alta;
	private String fa_re_usuario_alta;
	private Double campo_no_usado_7;
	private java.util.Date fa_re_hora_modific;
	private String fa_re_usuario_modific;
	private Double campo_no_usado_8;
	private java.util.Date fa_re_hora_baja;
	private String fa_re_usuario_baja;
	private String fa_re_usuario_superv;
	private Double campo_no_usado_9;
	private java.util.Date fa_re_hora_superv;
	private String fa_re_usuario_autoriz;
	private Double campo_no_usado_10;
	private String fa_re_hora_autoriz;
	private Double fa_re_cat_gran_cli;
	private Double fa_re_nro_gran_cli;
	private Double fa_re_pcia_porc;
	private String fa_re_fecha_impre;
	private Double fa_re_porc_iva_rg17;
	private Double fa_no_nro_reintegro;
	private Double campo_no_usado_11;
	private Double fa_no_nro_nc;
	private Double campo_no_usado_12;
	private Double fa_no_nro_nd;
	private Double campo_no_usado_13;
	private Double campo_no_usado_14;
	private Double fa_no_motivo;
	private Double fa_no_zona;
	private Double fa_no_oficina;
	private Double fa_no_abonado;
	private Double fa_no_tipo_fact;
	private Double fa_no_ano_lote;
	private String fa_no_serie_meg;
	private Double fa_no_n_contrato_meg;
	private Double fa_no_n_cuota_meg;
	private Double fa_no_categoria;
	private Double fa_no_tipo_contrib;
	private Double fa_no_tipo_contrib_nvo;
	private Double campo_no_usado_15;
	private Double campo_no_usado_16;
	private Double fa_no_marca_tarea;	
	/**
	 * CAMPO FECHA DINAMICA-AGREGADO PARA REINTEGROS
	 */
	private String FECHA_PAGO_DINAMICA;
	
	/**
	 * CAMPO FECHA PAGO PRESCRIPTO PARA REINTEGROS
	 */
	private java.util.Date FA_RE_FECHAPAGO_PRESC; 
	
	
	/**
	 * CAMPO BOOLEANO PRESCRIPTO PARA REINTEGROS
	 */
	private Boolean PRESCRIPTO;
	
	
		

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public SfaFisicoReintegrosVO() {

	}
	
	/**
	* Recupera el valor del atributo Campo_no_usado_17
	* @return el valor de Campo_no_usado_17
	*/ 
	public Double getCampo_no_usado_17() {
		return campo_no_usado_17;
	}
 
	/**
	* Fija el valor del atributo Campo_no_usado_17
	* @param valor el valor de Campo_no_usado_17
	*/ 
	public void setCampo_no_usado_17(Double valor) {
		this.campo_no_usado_17 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_alta_nc
	* @return el valor de Fa_no_hora_alta_nc
	*/ 
	public String getFa_no_hora_alta_nc() {
		return fa_no_hora_alta_nc;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_alta_nc
	* @param valor el valor de Fa_no_hora_alta_nc
	*/ 
	public void setFa_no_hora_alta_nc(String valor) {
		this.fa_no_hora_alta_nc = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_18
	* @return el valor de Campo_no_usado_18
	*/ 
	public Double getCampo_no_usado_18() {
		return campo_no_usado_18;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_18
	* @param valor el valor de Campo_no_usado_18
	*/ 
	public void setCampo_no_usado_18(Double valor) {
		this.campo_no_usado_18 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_alta_nd
	* @return el valor de Fa_no_hora_alta_nd
	*/ 
	public java.util.Date getFa_no_hora_alta_nd() {
		return fa_no_hora_alta_nd;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_alta_nd
	* @param valor el valor de Fa_no_hora_alta_nd
	*/ 
	public void setFa_no_hora_alta_nd(java.util.Date valor) {
		this.fa_no_hora_alta_nd = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_19
	* @return el valor de Campo_no_usado_19
	*/ 
	public Double getCampo_no_usado_19() {
		return campo_no_usado_19;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_19
	* @param valor el valor de Campo_no_usado_19
	*/ 
	public void setCampo_no_usado_19(Double valor) {
		this.campo_no_usado_19 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_mod_nc
	* @return el valor de Fa_no_hora_mod_nc
	*/ 
	public java.util.Date getFa_no_hora_mod_nc() {
		return fa_no_hora_mod_nc;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_mod_nc
	* @param valor el valor de Fa_no_hora_mod_nc
	*/ 
	public void setFa_no_hora_mod_nc(java.util.Date valor) {
		this.fa_no_hora_mod_nc = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_20
	* @return el valor de Campo_no_usado_20
	*/ 
	public Double getCampo_no_usado_20() {
		return campo_no_usado_20;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_20
	* @param valor el valor de Campo_no_usado_20
	*/ 
	public void setCampo_no_usado_20(Double valor) {
		this.campo_no_usado_20 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_mod_nd
	* @return el valor de Fa_no_hora_mod_nd
	*/ 
	public java.util.Date getFa_no_hora_mod_nd() {
		return fa_no_hora_mod_nd;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_mod_nd
	* @param valor el valor de Fa_no_hora_mod_nd
	*/ 
	public void setFa_no_hora_mod_nd(java.util.Date valor) {
		this.fa_no_hora_mod_nd = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_cat_gran_cli
	* @return el valor de Fa_no_cat_gran_cli
	*/ 
	public Double getFa_no_cat_gran_cli() {
		return fa_no_cat_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_cat_gran_cli
	* @param valor el valor de Fa_no_cat_gran_cli
	*/ 
	public void setFa_no_cat_gran_cli(Double valor) {
		this.fa_no_cat_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_gran_cli
	* @return el valor de Fa_no_nro_gran_cli
	*/ 
	public Double getFa_no_nro_gran_cli() {
		return fa_no_nro_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_gran_cli
	* @param valor el valor de Fa_no_nro_gran_cli
	*/ 
	public void setFa_no_nro_gran_cli(Double valor) {
		this.fa_no_nro_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_interurb
	* @return el valor de Fa_no_interurb
	*/ 
	public String getFa_no_interurb() {
		return fa_no_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_interurb
	* @param valor el valor de Fa_no_interurb
	*/ 
	public void setFa_no_interurb(String valor) {
		this.fa_no_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_urb
	* @return el valor de Fa_no_urb
	*/ 
	public String getFa_no_urb() {
		return fa_no_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_urb
	* @param valor el valor de Fa_no_urb
	*/ 
	public void setFa_no_urb(String valor) {
		this.fa_no_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_linea
	* @return el valor de Fa_no_linea
	*/ 
	public String getFa_no_linea() {
		return fa_no_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_linea
	* @param valor el valor de Fa_no_linea
	*/ 
	public void setFa_no_linea(String valor) {
		this.fa_no_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_ident_abo
	* @return el valor de Fa_no_ident_abo
	*/ 
	public Double getFa_no_ident_abo() {
		return fa_no_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_ident_abo
	* @param valor el valor de Fa_no_ident_abo
	*/ 
	public void setFa_no_ident_abo(Double valor) {
		this.fa_no_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_interurb
	* @return el valor de Fa_re_interurb
	*/ 
	public String getFa_re_interurb() {
		return fa_re_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_interurb
	* @param valor el valor de Fa_re_interurb
	*/ 
	public void setFa_re_interurb(String valor) {
		this.fa_re_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_urb
	* @return el valor de Fa_re_urb
	*/ 
	public String getFa_re_urb() {
		return fa_re_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_urb
	* @param valor el valor de Fa_re_urb
	*/ 
	public void setFa_re_urb(String valor) {
		this.fa_re_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_linea
	* @return el valor de Fa_re_linea
	*/ 
	public String getFa_re_linea() {
		return fa_re_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_linea
	* @param valor el valor de Fa_re_linea
	*/ 
	public void setFa_re_linea(String valor) {
		this.fa_re_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_ident_abo
	* @return el valor de Fa_re_ident_abo
	*/ 
	public Double getFa_re_ident_abo() {
		return fa_re_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_ident_abo
	* @param valor el valor de Fa_re_ident_abo
	*/ 
	public void setFa_re_ident_abo(Double valor) {
		this.fa_re_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cod_postal_loc
	* @return el valor de Fa_re_cod_postal_loc
	*/ 
	public String getFa_re_cod_postal_loc() {
		return fa_re_cod_postal_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cod_postal_loc
	* @param valor el valor de Fa_re_cod_postal_loc
	*/ 
	public void setFa_re_cod_postal_loc(String valor) {
		this.fa_re_cod_postal_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cod_postal_loc_benf
	* @return el valor de Fa_re_cod_postal_loc_benf
	*/ 
	public String getFa_re_cod_postal_loc_benf() {
		return fa_re_cod_postal_loc_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cod_postal_loc_benf
	* @param valor el valor de Fa_re_cod_postal_loc_benf
	*/ 
	public void setFa_re_cod_postal_loc_benf(String valor) {
		this.fa_re_cod_postal_loc_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_solic_nvo
	* @return el valor de Fa_re_fecha_solic_nvo
	*/ 
	public java.util.Date getFa_re_fecha_solic_nvo() {
		return fa_re_fecha_solic_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_solic_nvo
	* @param valor el valor de Fa_re_fecha_solic_nvo
	*/ 
	public void setFa_re_fecha_solic_nvo(java.util.Date valor) {
		this.fa_re_fecha_solic_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_emis_op_nvo
	* @return el valor de Fa_re_fecha_emis_op_nvo
	*/ 
	public java.util.Date getFa_re_fecha_emis_op_nvo() {
		return fa_re_fecha_emis_op_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_emis_op_nvo
	* @param valor el valor de Fa_re_fecha_emis_op_nvo
	*/ 
	public void setFa_re_fecha_emis_op_nvo(java.util.Date valor) {
		this.fa_re_fecha_emis_op_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_pago_nvo
	* @return el valor de Fa_re_fecha_pago_nvo
	*/ 
	public String getFa_re_fecha_pago_nvo() {
		return fa_re_fecha_pago_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_pago_nvo
	* @param valor el valor de Fa_re_fecha_pago_nvo
	*/ 
	public void setFa_re_fecha_pago_nvo(String valor) {
		this.fa_re_fecha_pago_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_gener_cheque_nvo
	* @return el valor de Fa_re_fecha_gener_cheque_nvo
	*/ 
	public java.util.Date getFa_re_fecha_gener_cheque_nvo() {
		return fa_re_fecha_gener_cheque_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_gener_cheque_nvo
	* @param valor el valor de Fa_re_fecha_gener_cheque_nvo
	*/ 
	public void setFa_re_fecha_gener_cheque_nvo(java.util.Date valor) {
		this.fa_re_fecha_gener_cheque_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_para_dolariz_nvo
	* @return el valor de Fa_re_fecha_para_dolariz_nvo
	*/ 
	public java.util.Date getFa_re_fecha_para_dolariz_nvo() {
		return fa_re_fecha_para_dolariz_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_para_dolariz_nvo
	* @param valor el valor de Fa_re_fecha_para_dolariz_nvo
	*/ 
	public void setFa_re_fecha_para_dolariz_nvo(java.util.Date valor) {
		this.fa_re_fecha_para_dolariz_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_alta_nvo
	* @return el valor de Fa_re_fecha_alta_nvo
	*/ 
	public java.util.Date getFa_re_fecha_alta_nvo() {
		return fa_re_fecha_alta_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_alta_nvo
	* @param valor el valor de Fa_re_fecha_alta_nvo
	*/ 
	public void setFa_re_fecha_alta_nvo(java.util.Date valor) {
		this.fa_re_fecha_alta_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_modific_nvo
	* @return el valor de Fa_re_fecha_modific_nvo
	*/ 
	public java.util.Date getFa_re_fecha_modific_nvo() {
		return fa_re_fecha_modific_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_modific_nvo
	* @param valor el valor de Fa_re_fecha_modific_nvo
	*/ 
	public void setFa_re_fecha_modific_nvo(java.util.Date valor) {
		this.fa_re_fecha_modific_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_baja_nvo
	* @return el valor de Fa_re_fecha_baja_nvo
	*/ 
	public java.util.Date getFa_re_fecha_baja_nvo() {
		return fa_re_fecha_baja_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_baja_nvo
	* @param valor el valor de Fa_re_fecha_baja_nvo
	*/ 
	public void setFa_re_fecha_baja_nvo(java.util.Date valor) {
		this.fa_re_fecha_baja_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_superv_nvo
	* @return el valor de Fa_re_fecha_superv_nvo
	*/ 
	public java.util.Date getFa_re_fecha_superv_nvo() {
		return fa_re_fecha_superv_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_superv_nvo
	* @param valor el valor de Fa_re_fecha_superv_nvo
	*/ 
	public void setFa_re_fecha_superv_nvo(java.util.Date valor) {
		this.fa_re_fecha_superv_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_autoriz_nvo
	* @return el valor de Fa_re_fecha_autoriz_nvo
	*/ 
	public java.util.Date getFa_re_fecha_autoriz_nvo() {
		return fa_re_fecha_autoriz_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_autoriz_nvo
	* @param valor el valor de Fa_re_fecha_autoriz_nvo
	*/ 
	public void setFa_re_fecha_autoriz_nvo(java.util.Date valor) {
		this.fa_re_fecha_autoriz_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_pago_misc
	* @return el valor de Fa_re_fecha_pago_misc
	*/ 
	public String getFa_re_fecha_pago_misc() {
		return fa_re_fecha_pago_misc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_pago_misc
	* @param valor el valor de Fa_re_fecha_pago_misc
	*/ 
	public void setFa_re_fecha_pago_misc(String valor) {
		this.fa_re_fecha_pago_misc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_origen
	* @return el valor de Fa_re_origen
	*/ 
	public Double getFa_re_origen() {
		return fa_re_origen;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_origen
	* @param valor el valor de Fa_re_origen
	*/ 
	public void setFa_re_origen(Double valor) {
		this.fa_re_origen = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_alta_reintegro_nvo
	* @return el valor de Fa_no_fecha_alta_reintegro_nvo
	*/ 
	public java.util.Date getFa_no_fecha_alta_reintegro_nvo() {
		return fa_no_fecha_alta_reintegro_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_alta_reintegro_nvo
	* @param valor el valor de Fa_no_fecha_alta_reintegro_nvo
	*/ 
	public void setFa_no_fecha_alta_reintegro_nvo(java.util.Date valor) {
		this.fa_no_fecha_alta_reintegro_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fec_ing_contable_nc_nvo
	* @return el valor de Fa_no_fec_ing_contable_nc_nvo
	*/ 
	public Double getFa_no_fec_ing_contable_nc_nvo() {
		return fa_no_fec_ing_contable_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fec_ing_contable_nc_nvo
	* @param valor el valor de Fa_no_fec_ing_contable_nc_nvo
	*/ 
	public void setFa_no_fec_ing_contable_nc_nvo(Double valor) {
		this.fa_no_fec_ing_contable_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fec_ing_contable_nd_nvo
	* @return el valor de Fa_no_fec_ing_contable_nd_nvo
	*/ 
	public Double getFa_no_fec_ing_contable_nd_nvo() {
		return fa_no_fec_ing_contable_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fec_ing_contable_nd_nvo
	* @param valor el valor de Fa_no_fec_ing_contable_nd_nvo
	*/ 
	public void setFa_no_fec_ing_contable_nd_nvo(Double valor) {
		this.fa_no_fec_ing_contable_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_para_dolariz_nvo
	* @return el valor de Fa_no_fecha_para_dolariz_nvo
	*/ 
	public java.util.Date getFa_no_fecha_para_dolariz_nvo() {
		return fa_no_fecha_para_dolariz_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_para_dolariz_nvo
	* @param valor el valor de Fa_no_fecha_para_dolariz_nvo
	*/ 
	public void setFa_no_fecha_para_dolariz_nvo(java.util.Date valor) {
		this.fa_no_fecha_para_dolariz_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_envio_gl_nc_nvo
	* @return el valor de Fa_no_fecha_envio_gl_nc_nvo
	*/ 
	public java.util.Date getFa_no_fecha_envio_gl_nc_nvo() {
		return fa_no_fecha_envio_gl_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_envio_gl_nc_nvo
	* @param valor el valor de Fa_no_fecha_envio_gl_nc_nvo
	*/ 
	public void setFa_no_fecha_envio_gl_nc_nvo(java.util.Date valor) {
		this.fa_no_fecha_envio_gl_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_envio_gl_nd_nvo
	* @return el valor de Fa_no_fecha_envio_gl_nd_nvo
	*/ 
	public java.util.Date getFa_no_fecha_envio_gl_nd_nvo() {
		return fa_no_fecha_envio_gl_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_envio_gl_nd_nvo
	* @param valor el valor de Fa_no_fecha_envio_gl_nd_nvo
	*/ 
	public void setFa_no_fecha_envio_gl_nd_nvo(java.util.Date valor) {
		this.fa_no_fecha_envio_gl_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_alta_nc_nvo
	* @return el valor de Fa_no_fecha_alta_nc_nvo
	*/ 
	public java.util.Date getFa_no_fecha_alta_nc_nvo() {
		return fa_no_fecha_alta_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_alta_nc_nvo
	* @param valor el valor de Fa_no_fecha_alta_nc_nvo
	*/ 
	public void setFa_no_fecha_alta_nc_nvo(java.util.Date valor) {
		this.fa_no_fecha_alta_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_alta_nd_nvo
	* @return el valor de Fa_no_fecha_alta_nd_nvo
	*/ 
	public java.util.Date getFa_no_fecha_alta_nd_nvo() {
		return fa_no_fecha_alta_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_alta_nd_nvo
	* @param valor el valor de Fa_no_fecha_alta_nd_nvo
	*/ 
	public void setFa_no_fecha_alta_nd_nvo(java.util.Date valor) {
		this.fa_no_fecha_alta_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_mod_nc_nvo
	* @return el valor de Fa_no_fecha_mod_nc_nvo
	*/ 
	public java.util.Date getFa_no_fecha_mod_nc_nvo() {
		return fa_no_fecha_mod_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_mod_nc_nvo
	* @param valor el valor de Fa_no_fecha_mod_nc_nvo
	*/ 
	public void setFa_no_fecha_mod_nc_nvo(java.util.Date valor) {
		this.fa_no_fecha_mod_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_mod_nd_nvo
	* @return el valor de Fa_no_fecha_mod_nd_nvo
	*/ 
	public java.util.Date getFa_no_fecha_mod_nd_nvo() {
		return fa_no_fecha_mod_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_mod_nd_nvo
	* @param valor el valor de Fa_no_fecha_mod_nd_nvo
	*/ 
	public void setFa_no_fecha_mod_nd_nvo(java.util.Date valor) {
		this.fa_no_fecha_mod_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_imputacion_estado
	* @return el valor de Fa_re_imputacion_estado
	*/ 
	public Double getFa_re_imputacion_estado() {
		return fa_re_imputacion_estado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_imputacion_estado
	* @param valor el valor de Fa_re_imputacion_estado
	*/ 
	public void setFa_re_imputacion_estado(Double valor) {
		this.fa_re_imputacion_estado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cod_barra
	* @return el valor de Fa_re_cod_barra
	*/ 
	public String getFa_re_cod_barra() {
		return fa_re_cod_barra;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cod_barra
	* @param valor el valor de Fa_re_cod_barra
	*/ 
	public void setFa_re_cod_barra(String valor) {
		this.fa_re_cod_barra = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_marca_tarea
	* @return el valor de Fa_re_e_marca_tarea
	*/ 
	public Double getFa_re_e_marca_tarea() {
		return fa_re_e_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_marca_tarea
	* @param valor el valor de Fa_re_e_marca_tarea
	*/ 
	public void setFa_re_e_marca_tarea(Double valor) {
		this.fa_re_e_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_banco
	* @return el valor de Fa_re_e_banco
	*/ 
	public Double getFa_re_e_banco() {
		return fa_re_e_banco;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_banco
	* @param valor el valor de Fa_re_e_banco
	*/ 
	public void setFa_re_e_banco(Double valor) {
		this.fa_re_e_banco = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_imp_pagado
	* @return el valor de Fa_re_e_imp_pagado
	*/ 
	public String getFa_re_e_imp_pagado() {
		return fa_re_e_imp_pagado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_imp_pagado
	* @param valor el valor de Fa_re_e_imp_pagado
	*/ 
	public void setFa_re_e_imp_pagado(String valor) {
		this.fa_re_e_imp_pagado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_acreditac
	* @return el valor de Fa_re_e_fecha_acreditac
	*/ 
	public java.util.Date getFa_re_e_fecha_acreditac() {
		return fa_re_e_fecha_acreditac;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_acreditac
	* @param valor el valor de Fa_re_e_fecha_acreditac
	*/ 
	public void setFa_re_e_fecha_acreditac(java.util.Date valor) {
		this.fa_re_e_fecha_acreditac = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_proceso
	* @return el valor de Fa_re_e_fecha_proceso
	*/ 
	public java.util.Date getFa_re_e_fecha_proceso() {
		return fa_re_e_fecha_proceso;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_proceso
	* @param valor el valor de Fa_re_e_fecha_proceso
	*/ 
	public void setFa_re_e_fecha_proceso(java.util.Date valor) {
		this.fa_re_e_fecha_proceso = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_sucursal
	* @return el valor de Fa_re_e_sucursal
	*/ 
	public Double getFa_re_e_sucursal() {
		return fa_re_e_sucursal;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_sucursal
	* @param valor el valor de Fa_re_e_sucursal
	*/ 
	public void setFa_re_e_sucursal(Double valor) {
		this.fa_re_e_sucursal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_pres
	* @return el valor de Fa_re_e_fecha_pres
	*/ 
	public java.util.Date getFa_re_e_fecha_pres() {
		return fa_re_e_fecha_pres;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_pres
	* @param valor el valor de Fa_re_e_fecha_pres
	*/ 
	public void setFa_re_e_fecha_pres(java.util.Date valor) {
		this.fa_re_e_fecha_pres = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_pago
	* @return el valor de Fa_re_e_fecha_pago
	*/ 
	public java.util.Date getFa_re_e_fecha_pago() {
		return fa_re_e_fecha_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_pago
	* @param valor el valor de Fa_re_e_fecha_pago
	*/ 
	public void setFa_re_e_fecha_pago(java.util.Date valor) {
		this.fa_re_e_fecha_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_bloqueo
	* @return el valor de Fa_re_fecha_bloqueo
	*/ 
	public String getFa_re_fecha_bloqueo() {
		return fa_re_fecha_bloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_bloqueo
	* @param valor el valor de Fa_re_fecha_bloqueo
	*/ 
	public void setFa_re_fecha_bloqueo(String valor) {
		this.fa_re_fecha_bloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_bloqueo
	* @return el valor de Fa_re_usuario_bloqueo
	*/ 
	public String getFa_re_usuario_bloqueo() {
		return fa_re_usuario_bloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_bloqueo
	* @param valor el valor de Fa_re_usuario_bloqueo
	*/ 
	public void setFa_re_usuario_bloqueo(String valor) {
		this.fa_re_usuario_bloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_bloqueo
	* @return el valor de Fa_re_hora_bloqueo
	*/ 
	public java.util.Date getFa_re_hora_bloqueo() {
		return fa_re_hora_bloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_bloqueo
	* @param valor el valor de Fa_re_hora_bloqueo
	*/ 
	public void setFa_re_hora_bloqueo(java.util.Date valor) {
		this.fa_re_hora_bloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_pago_duplicado
	* @return el valor de Fa_re_pago_duplicado
	*/ 
	public Double getFa_re_pago_duplicado() {
		return fa_re_pago_duplicado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_pago_duplicado
	* @param valor el valor de Fa_re_pago_duplicado
	*/ 
	public void setFa_re_pago_duplicado(Double valor) {
		this.fa_re_pago_duplicado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_pcia_base_perc
	* @return el valor de Fa_re_pcia_base_perc
	*/ 
	public Double getFa_re_pcia_base_perc() {
		return fa_re_pcia_base_perc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_pcia_base_perc
	* @param valor el valor de Fa_re_pcia_base_perc
	*/ 
	public void setFa_re_pcia_base_perc(Double valor) {
		this.fa_re_pcia_base_perc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_movimiento
	* @return el valor de Fa_re_nro_movimiento
	*/ 
	public Double getFa_re_nro_movimiento() {
		return fa_re_nro_movimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_movimiento
	* @param valor el valor de Fa_re_nro_movimiento
	*/ 
	public void setFa_re_nro_movimiento(Double valor) {
		this.fa_re_nro_movimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_desbloqueo
	* @return el valor de Fa_re_fecha_desbloqueo
	*/ 
	public java.util.Date getFa_re_fecha_desbloqueo() {
		return fa_re_fecha_desbloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_desbloqueo
	* @param valor el valor de Fa_re_fecha_desbloqueo
	*/ 
	public void setFa_re_fecha_desbloqueo(java.util.Date valor) {
		this.fa_re_fecha_desbloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usu_desbloqueo
	* @return el valor de Fa_re_usu_desbloqueo
	*/ 
	public String getFa_re_usu_desbloqueo() {
		return fa_re_usu_desbloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usu_desbloqueo
	* @param valor el valor de Fa_re_usu_desbloqueo
	*/ 
	public void setFa_re_usu_desbloqueo(String valor) {
		this.fa_re_usu_desbloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_migracion
	* @return el valor de Fa_re_fecha_migracion
	*/ 
	public String getFa_re_fecha_migracion() {
		return fa_re_fecha_migracion;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_migracion
	* @param valor el valor de Fa_re_fecha_migracion
	*/ 
	public void setFa_re_fecha_migracion(String valor) {
		this.fa_re_fecha_migracion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cliente_atis
	* @return el valor de Fa_re_cliente_atis
	*/ 
	public String getFa_re_cliente_atis() {
		return fa_re_cliente_atis;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cliente_atis
	* @param valor el valor de Fa_re_cliente_atis
	*/ 
	public void setFa_re_cliente_atis(String valor) {
		this.fa_re_cliente_atis = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cuenta_atis
	* @return el valor de Fa_re_cuenta_atis
	*/ 
	public String getFa_re_cuenta_atis() {
		return fa_re_cuenta_atis;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cuenta_atis
	* @param valor el valor de Fa_re_cuenta_atis
	*/ 
	public void setFa_re_cuenta_atis(String valor) {
		this.fa_re_cuenta_atis = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_motivo_transf
	* @return el valor de Fa_re_motivo_transf
	*/ 
	public String getFa_re_motivo_transf() {
		return fa_re_motivo_transf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_motivo_transf
	* @param valor el valor de Fa_re_motivo_transf
	*/ 
	public void setFa_re_motivo_transf(String valor) {
		this.fa_re_motivo_transf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_operador_loc
	* @return el valor de Fa_re_operador_loc
	*/ 
	public String getFa_re_operador_loc() {
		return fa_re_operador_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_operador_loc
	* @param valor el valor de Fa_re_operador_loc
	*/ 
	public void setFa_re_operador_loc(String valor) {
		this.fa_re_operador_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_apynom
	* @return el valor de Fa_re_gp_apynom
	*/ 
	public String getFa_re_gp_apynom() {
		return fa_re_gp_apynom;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_apynom
	* @param valor el valor de Fa_re_gp_apynom
	*/ 
	public void setFa_re_gp_apynom(String valor) {
		this.fa_re_gp_apynom = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_tipo_doc
	* @return el valor de Fa_re_gp_tipo_doc
	*/ 
	public String getFa_re_gp_tipo_doc() {
		return fa_re_gp_tipo_doc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_tipo_doc
	* @param valor el valor de Fa_re_gp_tipo_doc
	*/ 
	public void setFa_re_gp_tipo_doc(String valor) {
		this.fa_re_gp_tipo_doc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_nro_doc
	* @return el valor de Fa_re_gp_nro_doc
	*/ 
	public String getFa_re_gp_nro_doc() {
		return fa_re_gp_nro_doc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_nro_doc
	* @param valor el valor de Fa_re_gp_nro_doc
	*/ 
	public void setFa_re_gp_nro_doc(String valor) {
		this.fa_re_gp_nro_doc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_domic
	* @return el valor de Fa_re_gp_domic
	*/ 
	public String getFa_re_gp_domic() {
		return fa_re_gp_domic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_domic
	* @param valor el valor de Fa_re_gp_domic
	*/ 
	public void setFa_re_gp_domic(String valor) {
		this.fa_re_gp_domic = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_cod_postal
	* @return el valor de Fa_re_gp_cod_postal
	*/ 
	public String getFa_re_gp_cod_postal() {
		return fa_re_gp_cod_postal;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_cod_postal
	* @param valor el valor de Fa_re_gp_cod_postal
	*/ 
	public void setFa_re_gp_cod_postal(String valor) {
		this.fa_re_gp_cod_postal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_localidad
	* @return el valor de Fa_re_gp_localidad
	*/ 
	public String getFa_re_gp_localidad() {
		return fa_re_gp_localidad;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_localidad
	* @param valor el valor de Fa_re_gp_localidad
	*/ 
	public void setFa_re_gp_localidad(String valor) {
		this.fa_re_gp_localidad = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_provincia
	* @return el valor de Fa_re_gp_provincia
	*/ 
	public String getFa_re_gp_provincia() {
		return fa_re_gp_provincia;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_provincia
	* @param valor el valor de Fa_re_gp_provincia
	*/ 
	public void setFa_re_gp_provincia(String valor) {
		this.fa_re_gp_provincia = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_numero_pago
	* @return el valor de Fa_re_numero_pago
	*/ 
	public String getFa_re_numero_pago() {
		return fa_re_numero_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_numero_pago
	* @param valor el valor de Fa_re_numero_pago
	*/ 
	public void setFa_re_numero_pago(String valor) {
		this.fa_re_numero_pago = valor;
	}
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_co_clave_numero
	* @return el valor de Fa_co_clave_numero
	*/ 
	public String getFa_co_clave_numero() {
		return fa_co_clave_numero;
	}
    	
	/**
	* Fija el valor del atributo Fa_co_clave_numero
	* @param valor el valor de Fa_co_clave_numero
	*/ 
	public void setFa_co_clave_numero(String valor) {
		this.fa_co_clave_numero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_co_prox_numero
	* @return el valor de Fa_co_prox_numero
	*/ 
	public Double getFa_co_prox_numero() {
		return fa_co_prox_numero;
	}
    	
	/**
	* Fija el valor del atributo Fa_co_prox_numero
	* @param valor el valor de Fa_co_prox_numero
	*/ 
	public void setFa_co_prox_numero(Double valor) {
		this.fa_co_prox_numero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_co_ident_abo
	* @return el valor de Fa_co_ident_abo
	*/ 
	public Double getFa_co_ident_abo() {
		return fa_co_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_co_ident_abo
	* @param valor el valor de Fa_co_ident_abo
	*/ 
	public void setFa_co_ident_abo(Double valor) {
		this.fa_co_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_num_reintegro
	* @return el valor de Fa_re_num_reintegro
	*/ 
	public Double getFa_re_num_reintegro() {
		return fa_re_num_reintegro;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_num_reintegro
	* @param valor el valor de Fa_re_num_reintegro
	*/ 
	public void setFa_re_num_reintegro(Double valor) {
		this.fa_re_num_reintegro = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_zona_solic
	* @return el valor de Fa_re_zona_solic
	*/ 
	public Double getFa_re_zona_solic() {
		return fa_re_zona_solic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_zona_solic
	* @param valor el valor de Fa_re_zona_solic
	*/ 
	public void setFa_re_zona_solic(Double valor) {
		this.fa_re_zona_solic = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_1
	* @return el valor de Campo_no_usado_1
	*/ 
	public Double getCampo_no_usado_1() {
		return campo_no_usado_1;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_1
	* @param valor el valor de Campo_no_usado_1
	*/ 
	public void setCampo_no_usado_1(Double valor) {
		this.campo_no_usado_1 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_zona
	* @return el valor de Fa_re_zona
	*/ 
	public Double getFa_re_zona() {
		return fa_re_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_zona
	* @param valor el valor de Fa_re_zona
	*/ 
	public void setFa_re_zona(Double valor) {
		this.fa_re_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_oficina
	* @return el valor de Fa_re_oficina
	*/ 
	public Double getFa_re_oficina() {
		if(this.fa_re_oficina != null)
			return fa_re_oficina;
		else
			return 0.d;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_oficina
	* @param valor el valor de Fa_re_oficina
	*/ 
	public void setFa_re_oficina(Double valor) {
		this.fa_re_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_abonado
	* @return el valor de Fa_re_abonado
	*/ 
	public Double getFa_re_abonado() {
		return fa_re_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_abonado
	* @param valor el valor de Fa_re_abonado
	*/ 
	public void setFa_re_abonado(Double valor) {
		this.fa_re_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tip_fact
	* @return el valor de Fa_re_tip_fact
	*/ 
	public Double getFa_re_tip_fact() {
		return fa_re_tip_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tip_fact
	* @param valor el valor de Fa_re_tip_fact
	*/ 
	public void setFa_re_tip_fact(Double valor) {
		this.fa_re_tip_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_anio_lote
	* @return el valor de Fa_re_anio_lote
	*/ 
	public Double getFa_re_anio_lote() {
		return fa_re_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_anio_lote
	* @param valor el valor de Fa_re_anio_lote
	*/ 
	public void setFa_re_anio_lote(Double valor) {
		this.fa_re_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_serie_meg
	* @return el valor de Fa_re_serie_meg
	*/ 
	public String getFa_re_serie_meg() {
		return fa_re_serie_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_serie_meg
	* @param valor el valor de Fa_re_serie_meg
	*/ 
	public void setFa_re_serie_meg(String valor) {
		this.fa_re_serie_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_n_contrato_meg
	* @return el valor de Fa_re_n_contrato_meg
	*/ 
	public Double getFa_re_n_contrato_meg() {
		return fa_re_n_contrato_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_n_contrato_meg
	* @param valor el valor de Fa_re_n_contrato_meg
	*/ 
	public void setFa_re_n_contrato_meg(Double valor) {
		this.fa_re_n_contrato_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_n_cuota_meg
	* @return el valor de Fa_re_n_cuota_meg
	*/ 
	public Double getFa_re_n_cuota_meg() {
		return fa_re_n_cuota_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_n_cuota_meg
	* @param valor el valor de Fa_re_n_cuota_meg
	*/ 
	public void setFa_re_n_cuota_meg(Double valor) {
		this.fa_re_n_cuota_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_factura
	* @return el valor de Fa_re_nro_factura
	*/ 
	public Double getFa_re_nro_factura() {
		return fa_re_nro_factura;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_factura
	* @param valor el valor de Fa_re_nro_factura
	*/ 
	public void setFa_re_nro_factura(Double valor) {
		this.fa_re_nro_factura = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_2
	* @return el valor de Campo_no_usado_2
	*/ 
	public Double getCampo_no_usado_2() {
		return campo_no_usado_2;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_2
	* @param valor el valor de Campo_no_usado_2
	*/ 
	public void setCampo_no_usado_2(Double valor) {
		this.campo_no_usado_2 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_motivo
	* @return el valor de Fa_re_motivo
	*/ 
	public Double getFa_re_motivo() {
		return fa_re_motivo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_motivo
	* @param valor el valor de Fa_re_motivo
	*/ 
	public void setFa_re_motivo(Double valor) {
		this.fa_re_motivo = valor;
	}
	
	// AGREAGADO DESCRIPCION DEL CAMPO MOTIVO
	/**
	* Recupera el valor del atributo FA_RE_MOTIVO_DESCRIPCION
	* @return el valor de FA_RE_MOTIVO_DESCRIPCION
	*/ 
	public String getFA_RE_MOTIVO_DESCRIPCION() {
		
		String Motivocompleto ="";
		int CampoMotivo;
		/// VALIDACION DEL MOTIVO
		
		CampoMotivo = this.getFa_re_motivo().intValue();
		
		switch(CampoMotivo) 
			{
			case 1:    	
				Motivocompleto = "PAGO DOBLE";																
				break;
			case 2:       
				Motivocompleto = "DIFERENCIA DE FACTURACION";					
				break;
			case 3:
				Motivocompleto = "CAMBIO DE CONTRIB./ RG17";
				break;
			case 4:
				Motivocompleto = "DESESTIMIENTO MEGATEL";
				break;
			case 5:
				Motivocompleto = "PAGO DOBLE MEGATEL";				
				break;
			case 6:
				Motivocompleto = "CARGO CONEX. S/ABONADO";
				break;
			case 7:
				Motivocompleto = "FACTURACION ESPECIAL ";
				break;
			case 8:
				Motivocompleto = "SOBRANTE DE CAJA";
				break;
			case 9:
				Motivocompleto = "INGRESOS BRUTOS ";
				break;
			case 10:
				Motivocompleto = "DIFERENCIA REFACTURACION";
				break;
			case 11:
				Motivocompleto = "ACTUALIZACION FINANCIERA";
				break;
			case 12:
				Motivocompleto = "AJUSTE DE FACTURACION ";
				break;
			case 13:
				Motivocompleto = "CARGOS VARIOS         ";
				break;
			case 14:
				Motivocompleto = "FACTURA NEGATIVA      ";										
				break;
			default:
				Motivocompleto = "DESCONOCIDO ";	
			break;
		
																															
			};				
		
		return Motivocompleto;
	}
    	


	/**
	* Fija el valor del atributo FA_RE_MOTIVO_DESCRIPCION
	* @param valor el valor de FA_RE_MOTIVO_DESCRIPCION
	*/ 
	public void setFA_RE_MOTIVO_DESCRIPCION(String valor) {
		this.FA_RE_MOTIVO_DESCRIPCION = valor;
	}
	

	
	/**
	* Recupera el valor del atributo Fa_re_estado
	* @return el valor de Fa_re_estado
	*/ 
	public Double getFa_re_estado() {
		return fa_re_estado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_estado
	* @param valor el valor de Fa_re_estado
	*/ 
	public void setFa_re_estado(Double valor) {
		this.fa_re_estado = valor;
	}
	
	// AGREGADO DESCRIPCION DEL CAMPO ESTADO
	
	/**
	* Recupera el valor del atributo FA_RE_ESTADO_DESCRIPCION
	* @return el valor de FA_RE_ESTADO_DESCRIPCION
	*/ 
	public String getFA_RE_ESTADO_DESCRIPCION() {
		
		String Estadocompleto =" ";
		int CampoEstado;
				
		/// VALIDACION DEL ESTADO			
		
		CampoEstado = this.getFa_re_estado().intValue();
		
		switch(CampoEstado) 
		{
		case 0:    	
			Estadocompleto = "ALTA";																
			break;
		case 1:       
			Estadocompleto = "AUTORIZADO";					
			break;
		case 2:
			Estadocompleto = "CON CARTA DE PAGO";
			break;
		case 3:    	
			Estadocompleto = "ANULADO";																
			break;
		case 4:       
			Estadocompleto = "PAGADO";					
			break;
		case 5:
			Estadocompleto = "ANULADO C/O. PAGO";
			break;
		case 6:    	
			Estadocompleto = "PAGADO POR MISCE";																
			break;
		case 7:       
			Estadocompleto = "AUTORIZADO RAPIPAGO";					
			break;
		case 8:
			Estadocompleto = "PAGADO RAPIPAGO";
			break;
		case 34:    	
			Estadocompleto = "MIGRANDO A ATIS";																
			break;
		case 35:       
			Estadocompleto = "MIGRADO A ATIS";					
			break;
		default:
			Estadocompleto = "DESCONOCIDO";				
		
		};
		
	
		return Estadocompleto;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_ESTADO_DESCRIPCION
	* @param valor el valor de FA_RE_ESTADO_DESCRIPCION
	*/ 
	public void setFA_RE_ESTADO_DESCRIPCION(String valor) {
		this.FA_RE_ESTADO_DESCRIPCION = valor;
	}
	

	
	/**
	* Recupera el valor del atributo Fa_re_nro_copia
	* @return el valor de Fa_re_nro_copia
	*/ 
	public Double getFa_re_nro_copia() {
		return fa_re_nro_copia;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_copia
	* @param valor el valor de Fa_re_nro_copia
	*/ 
	public void setFa_re_nro_copia(Double valor) {
		this.fa_re_nro_copia = valor;
	}
	
	// AGREGADO DE DESCRIPCION DEL CAMPO NRO COPIA
	/**
	* Recupera el valor del atributo FA_RE_NRO_COPIA_DESCRIPCION
	* @return el valor de FA_RE_NRO_COPIA_DESCRIPCION
	*/ 
	public String getFA_RE_NRO_COPIA_DESCRIPCION() {
		
	///// VALIDACION PARA EL NUMERO DE COPIA
		
			
		String NroCopiacompleto = " ";
		Double Copia;	
		Integer numero;
		
		
		Copia = this.getFa_re_nro_copia(); 
				
		if (this.getFa_re_nro_copia() != null )
		{	
			//Type cast double to int
			numero = Copia.intValue();
				
			 				
			if ((this.getFa_re_estado() == 2) || (this.getFa_re_estado() == 4))
			{
				if (this.getFa_re_nro_copia() == 0) 
				{		   
					NroCopiacompleto = "ORIGINAL";		
				}
				else
				{			  
					NroCopiacompleto = numero.toString();			 
				}
			}
	    }
		return NroCopiacompleto;		
  }
    	
	
	
	/**
	* Fija el valor del atributo FA_RE_NRO_COPIA_DESCRIPCION
	* @param valor el valor de FA_RE_NRO_COPIA_DESCRIPCION
	*/ 
	public void setFA_RE_NRO_COPIA_DESCRIPCION(String valor) {
		this.FA_RE_NRO_COPIA_DESCRIPCION = valor;
	}
	
		
	
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_concepto
	* @return el valor de Fa_re_importe_tot_concepto
	*/ 
	public java.math.BigDecimal getFa_re_importe_tot_concepto() {
		return fa_re_importe_tot_concepto;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_concepto
	* @param valor el valor de Fa_re_importe_tot_concepto
	*/ 
	public void setFa_re_importe_tot_concepto(java.math.BigDecimal valor) {
		this.fa_re_importe_tot_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_iva
	* @return el valor de Fa_re_importe_tot_iva
	*/ 
	public java.math.BigDecimal getFa_re_importe_tot_iva() {
		return fa_re_importe_tot_iva;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_iva
	* @param valor el valor de Fa_re_importe_tot_iva
	*/ 
	public void setFa_re_importe_tot_iva(java.math.BigDecimal valor) {
		this.fa_re_importe_tot_iva = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_iva_adic
	* @return el valor de Fa_re_importe_tot_iva_adic
	*/ 
	public java.math.BigDecimal getFa_re_importe_tot_iva_adic() {
		return fa_re_importe_tot_iva_adic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_iva_adic
	* @param valor el valor de Fa_re_importe_tot_iva_adic
	*/ 
	public void setFa_re_importe_tot_iva_adic(java.math.BigDecimal valor) {
		this.fa_re_importe_tot_iva_adic = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_mora
	* @return el valor de Fa_re_importe_mora
	*/ 
	public java.math.BigDecimal getFa_re_importe_mora() {
		return fa_re_importe_mora;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_mora
	* @param valor el valor de Fa_re_importe_mora
	*/ 
	public void setFa_re_importe_mora(java.math.BigDecimal valor) {
		this.fa_re_importe_mora = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_actualiz
	* @return el valor de Fa_re_importe_tot_actualiz
	*/ 
	public java.math.BigDecimal getFa_re_importe_tot_actualiz() {
		return fa_re_importe_tot_actualiz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_actualiz
	* @param valor el valor de Fa_re_importe_tot_actualiz
	*/ 
	public void setFa_re_importe_tot_actualiz(java.math.BigDecimal valor) {
		this.fa_re_importe_tot_actualiz = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_ing_b_adic
	* @return el valor de Fa_re_importe_tot_ing_b_adic
	*/ 
	public java.math.BigDecimal getFa_re_importe_tot_ing_b_adic() {
		return fa_re_importe_tot_ing_b_adic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_ing_b_adic
	* @param valor el valor de Fa_re_importe_tot_ing_b_adic
	*/ 
	public void setFa_re_importe_tot_ing_b_adic(java.math.BigDecimal valor) {
		this.fa_re_importe_tot_ing_b_adic = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_apell_nom_lin1
	* @return el valor de Fa_re_apell_nom_lin1
	*/ 
	public String getFa_re_apell_nom_lin1() {
		return fa_re_apell_nom_lin1;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_apell_nom_lin1
	* @param valor el valor de Fa_re_apell_nom_lin1
	*/ 
	public void setFa_re_apell_nom_lin1(String valor) {
		this.fa_re_apell_nom_lin1 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_an_domic_lin2
	* @return el valor de Fa_re_an_domic_lin2
	*/ 
	public String getFa_re_an_domic_lin2() {
		return fa_re_an_domic_lin2;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_an_domic_lin2
	* @param valor el valor de Fa_re_an_domic_lin2
	*/ 
	public void setFa_re_an_domic_lin2(String valor) {
		this.fa_re_an_domic_lin2 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_domicil_lin3
	* @return el valor de Fa_re_domicil_lin3
	*/ 
	public String getFa_re_domicil_lin3() {
		return fa_re_domicil_lin3;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_domicil_lin3
	* @param valor el valor de Fa_re_domicil_lin3
	*/ 
	public void setFa_re_domicil_lin3(String valor) {
		this.fa_re_domicil_lin3 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_codpos_loc_lin4
	* @return el valor de Fa_re_codpos_loc_lin4
	*/ 
	public String getFa_re_codpos_loc_lin4() {
		return fa_re_codpos_loc_lin4;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_codpos_loc_lin4
	* @param valor el valor de Fa_re_codpos_loc_lin4
	*/ 
	public void setFa_re_codpos_loc_lin4(String valor) {
		this.fa_re_codpos_loc_lin4 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_dpto_lin6
	* @return el valor de Fa_re_dpto_lin6
	*/ 
	public String getFa_re_dpto_lin6() {
		return fa_re_dpto_lin6;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_dpto_lin6
	* @param valor el valor de Fa_re_dpto_lin6
	*/ 
	public void setFa_re_dpto_lin6(String valor) {
		this.fa_re_dpto_lin6 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_categoria
	* @return el valor de Fa_re_categoria
	*/ 
	public Double getFa_re_categoria() {
		return fa_re_categoria;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_categoria
	* @param valor el valor de Fa_re_categoria
	*/ 
	public void setFa_re_categoria(Double valor) {
		this.fa_re_categoria = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tip_contrib
	* @return el valor de Fa_re_tip_contrib
	*/ 
	public Double getFa_re_tip_contrib() {
		return fa_re_tip_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tip_contrib
	* @param valor el valor de Fa_re_tip_contrib
	*/ 
	public void setFa_re_tip_contrib(Double valor) {
		this.fa_re_tip_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_contrib
	* @return el valor de Fa_re_nro_contrib
	*/ 
	public Double getFa_re_nro_contrib() {
		return fa_re_nro_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_contrib
	* @param valor el valor de Fa_re_nro_contrib
	*/ 
	public void setFa_re_nro_contrib(Double valor) {
		this.fa_re_nro_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_apell_nom_lin1_benf
	* @return el valor de Fa_re_apell_nom_lin1_benf
	*/ 
	public String getFa_re_apell_nom_lin1_benf() {
		return fa_re_apell_nom_lin1_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_apell_nom_lin1_benf
	* @param valor el valor de Fa_re_apell_nom_lin1_benf
	*/ 
	public void setFa_re_apell_nom_lin1_benf(String valor) {
		this.fa_re_apell_nom_lin1_benf = valor;
	}
	
	//AGREGADO DE LA DESCRIPCION CAMPO APELLIDO	
	/**
	* Recupera el valor del atributo FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	* @return el valor de FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	*/ 
	public String getFA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION() {
		
		/// VALIDACION DEL NOMBRE Y APELLIDO	
		
		String ApellidoCompleto = " " ;
		String Varnula=" ";
		
		if((this.getFa_re_anio_lote()<=97004) && (this.getFa_re_tip_fact() == 2 || this.getFa_re_tip_fact()==68 || this.getFa_re_tip_fact() == 36 )
				
		||
		( (this.getFa_re_anio_lote()<=97001) && (this.getFa_re_tip_fact() == 1 || this.getFa_re_tip_fact()==67)))
			
		{

			if (this.getFa_re_apell_nom_lin1_benf()==null)
			{
			ApellidoCompleto = Varnula ;
			}
			else
				
			ApellidoCompleto = this.getFa_re_apell_nom_lin1_benf() ;
		}
		
		else
		{
			// lo agrego para no mostrar nulos
			if (this.getFa_re_apell_nom_lin1_benf()==null) {
				
				Varnula = this.getFa_re_apell_nom_lin1_benf(); 
				
				Varnula = " ";
				
				ApellidoCompleto = Varnula + ' ' + this.getFa_re_an_domic_lin2_benf() ;
			}
			else			   
				if (this.getFa_re_an_domic_lin2_benf()==null)
				{
				  Varnula = this.getFa_re_an_domic_lin2_benf();
				  
				  Varnula = " ";
				  
				  ApellidoCompleto = this.getFa_re_apell_nom_lin1_benf() + ' ' + Varnula;
					
			    }
			// hasta el agregado de no mostrar nulos

		//	ApellidoCompleto = this.getFa_re_apell_nom_lin1_benf() + ' ' + this.getFa_re_an_domic_lin2_benf();
	
		}
		
		return ApellidoCompleto;
		
		
		
	}    	
	/**
	* Fija el valor del atributo FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	* @param valor el valor de FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	*/ 
	public void setFA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION(String valor) {
		this.FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION = valor;
	}
	
	
	
	
	/**
	* Recupera el valor del atributo Fa_re_an_domic_lin2_benf
	* @return el valor de Fa_re_an_domic_lin2_benf
	*/ 
	public String getFa_re_an_domic_lin2_benf() {
		return fa_re_an_domic_lin2_benf;					
				
	}
    	
	/**
	* Fija el valor del atributo Fa_re_an_domic_lin2_benf
	* @param valor el valor de Fa_re_an_domic_lin2_benf
	*/ 
	public void setFa_re_an_domic_lin2_benf(String valor) {
		this.fa_re_an_domic_lin2_benf = valor;
	}
	
	//AGREGADO DE LA DESCRIPCION CAMPO DOMICILIO
	/**
	* Recupera el valor del atributo FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	* @return el valor de FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	*/ 
	public String getFA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION() {
		
        /// VALIDACION DEL DOMICILIO
		
		String DomicilioCompleto = " " ;
		String Varnula=" ";
		Double aniolote = this.getFa_re_anio_lote();
		
		if((this.getFa_re_anio_lote()<=97004) && (this.getFa_re_tip_fact() == 2 || this.getFa_re_tip_fact()==68 || this.getFa_re_tip_fact() == 36 )
				
		||
		( (this.getFa_re_anio_lote()<=97001) && (this.getFa_re_tip_fact() == 1 || this.getFa_re_tip_fact()==67)))
			
			
		{	
			// lo agrego para no mostrar nulos
			if (this.getFa_re_an_domic_lin2_benf()==null) {
				
				Varnula = this.getFa_re_an_domic_lin2_benf(); 
				
				Varnula = "";
				
				DomicilioCompleto = Varnula + ' ' + this.getFa_re_domicil_lin3_benf() ;
			}
			else			   
				if (this.getFa_re_domicil_lin3_benf()==null)
				{
				  Varnula = this.getFa_re_domicil_lin3_benf();
				  
				  Varnula = "";
				  
				  DomicilioCompleto = this.getFa_re_an_domic_lin2_benf() + ' ' + Varnula;
					
			    }
			// hasta el agregado de no mostrar nulos
     		
	
		}
		
		else
		{
			// lo agrego para no mostrar nulos
			if (this.getFa_re_domicil_lin3_benf()==null) {
				
				Varnula = this.getFa_re_domicil_lin3_benf(); 
				
				Varnula = " ";
				
				DomicilioCompleto = Varnula + ' ' + this.getFa_re_dpto_loc_lin5_benf() ;
			}
			else			   
				if (this.getFa_re_dpto_loc_lin5_benf()==null)
				{
				  Varnula = this.getFa_re_dpto_loc_lin5_benf();
				  
				  Varnula = " ";
				  
				  DomicilioCompleto = this.getFa_re_domicil_lin3_benf() + ' ' + Varnula;
					
			    }
			// hasta el agregado de no mostrar nulos
						 			
		}

		
		return DomicilioCompleto;					
			
	}
    	
	/**
	* Fija el valor del atributo FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	* @param valor el valor de FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	*/ 
	public void setFA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION(String valor) {
		this.FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION = valor;
	}
	
	
	
	
	
	/**
	* Recupera el valor del atributo Fa_re_domicil_lin3_benf
	* @return el valor de Fa_re_domicil_lin3_benf
	*/ 
	public String getFa_re_domicil_lin3_benf() {
		return fa_re_domicil_lin3_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_domicil_lin3_benf
	* @param valor el valor de Fa_re_domicil_lin3_benf
	*/ 
	public void setFa_re_domicil_lin3_benf(String valor) {
		this.fa_re_domicil_lin3_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_codpos_loc_lin4_benf
	* @return el valor de Fa_re_codpos_loc_lin4_benf
	*/ 
	public String getFa_re_codpos_loc_lin4_benf() {
		return fa_re_codpos_loc_lin4_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_codpos_loc_lin4_benf
	* @param valor el valor de Fa_re_codpos_loc_lin4_benf
	*/ 
	public void setFa_re_codpos_loc_lin4_benf(String valor) {
		this.fa_re_codpos_loc_lin4_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_dpto_loc_lin5_benf
	* @return el valor de Fa_re_dpto_loc_lin5_benf
	*/ 
	public String getFa_re_dpto_loc_lin5_benf() {
		return fa_re_dpto_loc_lin5_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_dpto_loc_lin5_benf
	* @param valor el valor de Fa_re_dpto_loc_lin5_benf
	*/ 
	public void setFa_re_dpto_loc_lin5_benf(String valor) {
		this.fa_re_dpto_loc_lin5_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tipo_contrib_loc
	* @return el valor de Fa_re_tipo_contrib_loc
	*/ 
	public Double getFa_re_tipo_contrib_loc() {
		return fa_re_tipo_contrib_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tipo_contrib_loc
	* @param valor el valor de Fa_re_tipo_contrib_loc
	*/ 
	public void setFa_re_tipo_contrib_loc(Double valor) {
		this.fa_re_tipo_contrib_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_contrib_loc
	* @return el valor de Fa_re_nro_contrib_loc
	*/ 
	public Double getFa_re_nro_contrib_loc() {
		return fa_re_nro_contrib_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_contrib_loc
	* @param valor el valor de Fa_re_nro_contrib_loc
	*/ 
	public void setFa_re_nro_contrib_loc(Double valor) {
		this.fa_re_nro_contrib_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tipo_documento
	* @return el valor de Fa_re_tipo_documento
	*/ 
	public Double getFa_re_tipo_documento() {
		return fa_re_tipo_documento;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tipo_documento
	* @param valor el valor de Fa_re_tipo_documento
	*/ 
	public void setFa_re_tipo_documento(Double valor) {
		this.fa_re_tipo_documento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_num_documento
	* @return el valor de Fa_re_num_documento
	*/ 
	public Double getFa_re_num_documento() {
		return fa_re_num_documento;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_num_documento
	* @param valor el valor de Fa_re_num_documento
	*/ 
	public void setFa_re_num_documento(Double valor) {
		this.fa_re_num_documento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_n_cuenta_clie_gob
	* @return el valor de Fa_re_n_cuenta_clie_gob
	*/ 
	public Double getFa_re_n_cuenta_clie_gob() {
		return fa_re_n_cuenta_clie_gob;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_n_cuenta_clie_gob
	* @param valor el valor de Fa_re_n_cuenta_clie_gob
	*/ 
	public void setFa_re_n_cuenta_clie_gob(Double valor) {
		this.fa_re_n_cuenta_clie_gob = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_dias_actualiz
	* @return el valor de Fa_re_dias_actualiz
	*/ 
	public Double getFa_re_dias_actualiz() {
		return fa_re_dias_actualiz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_dias_actualiz
	* @param valor el valor de Fa_re_dias_actualiz
	*/ 
	public void setFa_re_dias_actualiz(Double valor) {
		this.fa_re_dias_actualiz = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_valor_pulso
	* @return el valor de Fa_re_valor_pulso
	*/ 
	public Double getFa_re_valor_pulso() {
		return fa_re_valor_pulso;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_valor_pulso
	* @param valor el valor de Fa_re_valor_pulso
	*/ 
	public void setFa_re_valor_pulso(Double valor) {
		this.fa_re_valor_pulso = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_3
	* @return el valor de Campo_no_usado_3
	*/ 
	public Double getCampo_no_usado_3() {
		return campo_no_usado_3;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_3
	* @param valor el valor de Campo_no_usado_3
	*/ 
	public void setCampo_no_usado_3(Double valor) {
		this.campo_no_usado_3 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_zona_pago
	* @return el valor de Fa_re_zona_pago
	*/ 
	public Double getFa_re_zona_pago() {
		return fa_re_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_zona_pago
	* @param valor el valor de Fa_re_zona_pago
	*/ 
	public void setFa_re_zona_pago(Double valor) {
		this.fa_re_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cajero
	* @return el valor de Fa_re_cajero
	*/ 
	public Double getFa_re_cajero() {
		return fa_re_cajero;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cajero
	* @param valor el valor de Fa_re_cajero
	*/ 
	public void setFa_re_cajero(Double valor) {
		this.fa_re_cajero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tipo_pago
	* @return el valor de Fa_re_tipo_pago
	*/ 
	public Double getFa_re_tipo_pago() {
		return fa_re_tipo_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tipo_pago
	* @param valor el valor de Fa_re_tipo_pago
	*/ 
	public void setFa_re_tipo_pago(Double valor) {
		this.fa_re_tipo_pago = valor;
	}
	
	// AGREGADO DESCRIPCION DEL CAMPO TIPO DE PAGO
	
	/**
	* Recupera el valor del atributo FA_RE_TIPO_PAGO_DESCRIPCION
	* @return el valor de FA_RE_TIPO_PAGO_DESCRIPCION
	*/ 
	public String getFA_RE_TIPO_PAGO_DESCRIPCION() {
		
				
		String TipoPagocompleto =" ";
		int CampoTipoPago;
		
		
	///// VALIDACION PARA EL TIPO DE PAGO 
		
		CampoTipoPago = this.getFa_re_tipo_pago().intValue();
		
		switch(CampoTipoPago) 
		{
		case 1:    	
			TipoPagocompleto = "EFECTIVO";																
			break;
		case 2:       
			TipoPagocompleto = "CHEQUE";					
			break;
		case 3:
			TipoPagocompleto = "RECURSOS";
			break;
		case 4:
			TipoPagocompleto = "MISCELAN";
			break;
		case ' ':
			TipoPagocompleto = "INEXISTENTE";
			break;
		default:
			break;
		};
		
		
		return TipoPagocompleto;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_TIPO_PAGO
	* @param valor el valor de FA_RE_TIPO_PAGO
	*/ 
	public void setFA_RE_TIPO_PAGO_DESCRIPCION(String valor) {
		this.FA_RE_TIPO_PAGO_DESCRIPCION = valor;
	}
	
	
	
	/**
	* Recupera el valor del atributo Campo_no_usado_4
	* @return el valor de Campo_no_usado_4
	*/ 
	public Double getCampo_no_usado_4() {
		return campo_no_usado_4;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_4
	* @param valor el valor de Campo_no_usado_4
	*/ 
	public void setCampo_no_usado_4(Double valor) {
		this.campo_no_usado_4 = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_5
	* @return el valor de Campo_no_usado_5
	*/ 
	public Double getCampo_no_usado_5() {
		return campo_no_usado_5;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_5
	* @param valor el valor de Campo_no_usado_5
	*/ 
	public void setCampo_no_usado_5(Double valor) {
		this.campo_no_usado_5 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_marca_tarea
	* @return el valor de Fa_re_marca_tarea
	*/ 
	public Double getFa_re_marca_tarea() {
		return fa_re_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_marca_tarea
	* @param valor el valor de Fa_re_marca_tarea
	*/ 
	public void setFa_re_marca_tarea(Double valor) {
		this.fa_re_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_orden_pago
	* @return el valor de Fa_re_nro_orden_pago
	*/ 
	public Double getFa_re_nro_orden_pago() {
		return fa_re_nro_orden_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_orden_pago
	* @param valor el valor de Fa_re_nro_orden_pago
	*/ 
	public void setFa_re_nro_orden_pago(Double valor) {
		this.fa_re_nro_orden_pago = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_6
	* @return el valor de Campo_no_usado_6
	*/ 
	public Double getCampo_no_usado_6() {
		return campo_no_usado_6;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_6
	* @param valor el valor de Campo_no_usado_6
	*/ 
	public void setCampo_no_usado_6(Double valor) {
		this.campo_no_usado_6 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_alta
	* @return el valor de Fa_re_hora_alta
	*/ 
	public String getFa_re_hora_alta() {
		return fa_re_hora_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_alta
	* @param valor el valor de Fa_re_hora_alta
	*/ 
	public void setFa_re_hora_alta(String valor) {
		this.fa_re_hora_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_alta
	* @return el valor de Fa_re_usuario_alta
	*/ 
	public String getFa_re_usuario_alta() {
		return fa_re_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_alta
	* @param valor el valor de Fa_re_usuario_alta
	*/ 
	public void setFa_re_usuario_alta(String valor) {
		this.fa_re_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_7
	* @return el valor de Campo_no_usado_7
	*/ 
	public Double getCampo_no_usado_7() {
		return campo_no_usado_7;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_7
	* @param valor el valor de Campo_no_usado_7
	*/ 
	public void setCampo_no_usado_7(Double valor) {
		this.campo_no_usado_7 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_modific
	* @return el valor de Fa_re_hora_modific
	*/ 
	public java.util.Date getFa_re_hora_modific() {
		return fa_re_hora_modific;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_modific
	* @param valor el valor de Fa_re_hora_modific
	*/ 
	public void setFa_re_hora_modific(java.util.Date valor) {
		this.fa_re_hora_modific = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_modific
	* @return el valor de Fa_re_usuario_modific
	*/ 
	public String getFa_re_usuario_modific() {
		return fa_re_usuario_modific;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_modific
	* @param valor el valor de Fa_re_usuario_modific
	*/ 
	public void setFa_re_usuario_modific(String valor) {
		this.fa_re_usuario_modific = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_8
	* @return el valor de Campo_no_usado_8
	*/ 
	public Double getCampo_no_usado_8() {
		return campo_no_usado_8;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_8
	* @param valor el valor de Campo_no_usado_8
	*/ 
	public void setCampo_no_usado_8(Double valor) {
		this.campo_no_usado_8 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_baja
	* @return el valor de Fa_re_hora_baja
	*/ 
	public java.util.Date getFa_re_hora_baja() {
		return fa_re_hora_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_baja
	* @param valor el valor de Fa_re_hora_baja
	*/ 
	public void setFa_re_hora_baja(java.util.Date valor) {
		this.fa_re_hora_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_baja
	* @return el valor de Fa_re_usuario_baja
	*/ 
	public String getFa_re_usuario_baja() {
		return fa_re_usuario_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_baja
	* @param valor el valor de Fa_re_usuario_baja
	*/ 
	public void setFa_re_usuario_baja(String valor) {
		this.fa_re_usuario_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_superv
	* @return el valor de Fa_re_usuario_superv
	*/ 
	public String getFa_re_usuario_superv() {
		return fa_re_usuario_superv;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_superv
	* @param valor el valor de Fa_re_usuario_superv
	*/ 
	public void setFa_re_usuario_superv(String valor) {
		this.fa_re_usuario_superv = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_9
	* @return el valor de Campo_no_usado_9
	*/ 
	public Double getCampo_no_usado_9() {
		return campo_no_usado_9;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_9
	* @param valor el valor de Campo_no_usado_9
	*/ 
	public void setCampo_no_usado_9(Double valor) {
		this.campo_no_usado_9 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_superv
	* @return el valor de Fa_re_hora_superv
	*/ 
	public java.util.Date getFa_re_hora_superv() {
		return fa_re_hora_superv;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_superv
	* @param valor el valor de Fa_re_hora_superv
	*/ 
	public void setFa_re_hora_superv(java.util.Date valor) {
		this.fa_re_hora_superv = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_autoriz
	* @return el valor de Fa_re_usuario_autoriz
	*/ 
	public String getFa_re_usuario_autoriz() {
		return fa_re_usuario_autoriz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_autoriz
	* @param valor el valor de Fa_re_usuario_autoriz
	*/ 
	public void setFa_re_usuario_autoriz(String valor) {
		this.fa_re_usuario_autoriz = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_10
	* @return el valor de Campo_no_usado_10
	*/ 
	public Double getCampo_no_usado_10() {
		return campo_no_usado_10;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_10
	* @param valor el valor de Campo_no_usado_10
	*/ 
	public void setCampo_no_usado_10(Double valor) {
		this.campo_no_usado_10 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_autoriz
	* @return el valor de Fa_re_hora_autoriz
	*/ 
	public String getFa_re_hora_autoriz() {
		return fa_re_hora_autoriz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_autoriz
	* @param valor el valor de Fa_re_hora_autoriz
	*/ 
	public void setFa_re_hora_autoriz(String valor) {
		this.fa_re_hora_autoriz = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cat_gran_cli
	* @return el valor de Fa_re_cat_gran_cli
	*/ 
	public Double getFa_re_cat_gran_cli() {
		return fa_re_cat_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cat_gran_cli
	* @param valor el valor de Fa_re_cat_gran_cli
	*/ 
	public void setFa_re_cat_gran_cli(Double valor) {
		this.fa_re_cat_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_gran_cli
	* @return el valor de Fa_re_nro_gran_cli
	*/ 
	public Double getFa_re_nro_gran_cli() {
		return fa_re_nro_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_gran_cli
	* @param valor el valor de Fa_re_nro_gran_cli
	*/ 
	public void setFa_re_nro_gran_cli(Double valor) {
		this.fa_re_nro_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_pcia_porc
	* @return el valor de Fa_re_pcia_porc
	*/ 
	public Double getFa_re_pcia_porc() {
		return fa_re_pcia_porc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_pcia_porc
	* @param valor el valor de Fa_re_pcia_porc
	*/ 
	public void setFa_re_pcia_porc(Double valor) {
		this.fa_re_pcia_porc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_impre
	* @return el valor de Fa_re_fecha_impre
	*/ 
	public String getFa_re_fecha_impre() {
		return fa_re_fecha_impre;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_impre
	* @param valor el valor de Fa_re_fecha_impre
	*/ 
	public void setFa_re_fecha_impre(String valor) {
		this.fa_re_fecha_impre = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_porc_iva_rg17
	* @return el valor de Fa_re_porc_iva_rg17
	*/ 
	public Double getFa_re_porc_iva_rg17() {
		return fa_re_porc_iva_rg17;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_porc_iva_rg17
	* @param valor el valor de Fa_re_porc_iva_rg17
	*/ 
	public void setFa_re_porc_iva_rg17(Double valor) {
		this.fa_re_porc_iva_rg17 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_reintegro
	* @return el valor de Fa_no_nro_reintegro
	*/ 
	public Double getFa_no_nro_reintegro() {
		return fa_no_nro_reintegro;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_reintegro
	* @param valor el valor de Fa_no_nro_reintegro
	*/ 
	public void setFa_no_nro_reintegro(Double valor) {
		this.fa_no_nro_reintegro = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_11
	* @return el valor de Campo_no_usado_11
	*/ 
	public Double getCampo_no_usado_11() {
		return campo_no_usado_11;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_11
	* @param valor el valor de Campo_no_usado_11
	*/ 
	public void setCampo_no_usado_11(Double valor) {
		this.campo_no_usado_11 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_nc
	* @return el valor de Fa_no_nro_nc
	*/ 
	public Double getFa_no_nro_nc() {
		return fa_no_nro_nc;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_nc
	* @param valor el valor de Fa_no_nro_nc
	*/ 
	public void setFa_no_nro_nc(Double valor) {
		this.fa_no_nro_nc = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_12
	* @return el valor de Campo_no_usado_12
	*/ 
	public Double getCampo_no_usado_12() {
		return campo_no_usado_12;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_12
	* @param valor el valor de Campo_no_usado_12
	*/ 
	public void setCampo_no_usado_12(Double valor) {
		this.campo_no_usado_12 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_nd
	* @return el valor de Fa_no_nro_nd
	*/ 
	public Double getFa_no_nro_nd() {
		return fa_no_nro_nd;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_nd
	* @param valor el valor de Fa_no_nro_nd
	*/ 
	public void setFa_no_nro_nd(Double valor) {
		this.fa_no_nro_nd = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_13
	* @return el valor de Campo_no_usado_13
	*/ 
	public Double getCampo_no_usado_13() {
		return campo_no_usado_13;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_13
	* @param valor el valor de Campo_no_usado_13
	*/ 
	public void setCampo_no_usado_13(Double valor) {
		this.campo_no_usado_13 = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_14
	* @return el valor de Campo_no_usado_14
	*/ 
	public Double getCampo_no_usado_14() {
		return campo_no_usado_14;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_14
	* @param valor el valor de Campo_no_usado_14
	*/ 
	public void setCampo_no_usado_14(Double valor) {
		this.campo_no_usado_14 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_motivo
	* @return el valor de Fa_no_motivo
	*/ 
	public Double getFa_no_motivo() {
		return fa_no_motivo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_motivo
	* @param valor el valor de Fa_no_motivo
	*/ 
	public void setFa_no_motivo(Double valor) {
		this.fa_no_motivo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_zona
	* @return el valor de Fa_no_zona
	*/ 
	public Double getFa_no_zona() {
		return fa_no_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_zona
	* @param valor el valor de Fa_no_zona
	*/ 
	public void setFa_no_zona(Double valor) {
		this.fa_no_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_oficina
	* @return el valor de Fa_no_oficina
	*/ 
	public Double getFa_no_oficina() {
		return fa_no_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_oficina
	* @param valor el valor de Fa_no_oficina
	*/ 
	public void setFa_no_oficina(Double valor) {
		this.fa_no_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_abonado
	* @return el valor de Fa_no_abonado
	*/ 
	public Double getFa_no_abonado() {
		return fa_no_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_abonado
	* @param valor el valor de Fa_no_abonado
	*/ 
	public void setFa_no_abonado(Double valor) {
		this.fa_no_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_tipo_fact
	* @return el valor de Fa_no_tipo_fact
	*/ 
	public Double getFa_no_tipo_fact() {
		return fa_no_tipo_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_tipo_fact
	* @param valor el valor de Fa_no_tipo_fact
	*/ 
	public void setFa_no_tipo_fact(Double valor) {
		this.fa_no_tipo_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_ano_lote
	* @return el valor de Fa_no_ano_lote
	*/ 
	public Double getFa_no_ano_lote() {
		return fa_no_ano_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_ano_lote
	* @param valor el valor de Fa_no_ano_lote
	*/ 
	public void setFa_no_ano_lote(Double valor) {
		this.fa_no_ano_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_serie_meg
	* @return el valor de Fa_no_serie_meg
	*/ 
	public String getFa_no_serie_meg() {
		return fa_no_serie_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_serie_meg
	* @param valor el valor de Fa_no_serie_meg
	*/ 
	public void setFa_no_serie_meg(String valor) {
		this.fa_no_serie_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_n_contrato_meg
	* @return el valor de Fa_no_n_contrato_meg
	*/ 
	public Double getFa_no_n_contrato_meg() {
		return fa_no_n_contrato_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_n_contrato_meg
	* @param valor el valor de Fa_no_n_contrato_meg
	*/ 
	public void setFa_no_n_contrato_meg(Double valor) {
		this.fa_no_n_contrato_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_n_cuota_meg
	* @return el valor de Fa_no_n_cuota_meg
	*/ 
	public Double getFa_no_n_cuota_meg() {
		return fa_no_n_cuota_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_n_cuota_meg
	* @param valor el valor de Fa_no_n_cuota_meg
	*/ 
	public void setFa_no_n_cuota_meg(Double valor) {
		this.fa_no_n_cuota_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_categoria
	* @return el valor de Fa_no_categoria
	*/ 
	public Double getFa_no_categoria() {
		return fa_no_categoria;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_categoria
	* @param valor el valor de Fa_no_categoria
	*/ 
	public void setFa_no_categoria(Double valor) {
		this.fa_no_categoria = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_tipo_contrib
	* @return el valor de Fa_no_tipo_contrib
	*/ 
	public Double getFa_no_tipo_contrib() {
		return fa_no_tipo_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_tipo_contrib
	* @param valor el valor de Fa_no_tipo_contrib
	*/ 
	public void setFa_no_tipo_contrib(Double valor) {
		this.fa_no_tipo_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_tipo_contrib_nvo
	* @return el valor de Fa_no_tipo_contrib_nvo
	*/ 
	public Double getFa_no_tipo_contrib_nvo() {
		return fa_no_tipo_contrib_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_tipo_contrib_nvo
	* @param valor el valor de Fa_no_tipo_contrib_nvo
	*/ 
	public void setFa_no_tipo_contrib_nvo(Double valor) {
		this.fa_no_tipo_contrib_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_15
	* @return el valor de Campo_no_usado_15
	*/ 
	public Double getCampo_no_usado_15() {
		return campo_no_usado_15;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_15
	* @param valor el valor de Campo_no_usado_15
	*/ 
	public void setCampo_no_usado_15(Double valor) {
		this.campo_no_usado_15 = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_16
	* @return el valor de Campo_no_usado_16
	*/ 
	public Double getCampo_no_usado_16() {
		return campo_no_usado_16;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_16
	* @param valor el valor de Campo_no_usado_16
	*/ 
	public void setCampo_no_usado_16(Double valor) {
		this.campo_no_usado_16 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_marca_tarea
	* @return el valor de Fa_no_marca_tarea
	*/ 
	public Double getFa_no_marca_tarea() {
		return fa_no_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_marca_tarea
	* @param valor el valor de Fa_no_marca_tarea
	*/ 
	public void setFa_no_marca_tarea(Double valor) {
		this.fa_no_marca_tarea = valor;
	}
	
	/** AGREGADO REINTEGROS
	* Recupera el valor del atributo FECHA_PAGO_DINAMICA
	* @return el valor de FECHA_PAGO_DINAMICA
	*/ 
	public String getFECHA_PAGO_DINAMICA() {
		
		String dia;
		String mes;
		String anio;
		
        String fecha;
        
		
		if (this.fa_re_tipo_pago == 4) 
		   { 		
            fecha = this.fa_re_fecha_pago_misc;
		   }
           else
           {
            fecha = this.fa_re_fecha_pago_nvo; 
           };
		
        if(fecha==null)
        	return " ";
                
      	anio =fecha.substring(0,4);      
        mes = fecha.substring(4,6);
        dia = fecha.substring(6,8);
		
		return (dia +'/'+ mes+ '/'+ anio);
		
	
	}	
	/**AGREGADO REINTEGROS
	* Fija el valor del atributo FECHA_PAGO_DINAMICA
	* @param valor el valor de FECHA_PAGO_DINAMICA
	*/ 
	public void setFECHA_PAGO_DINAMICA(String valor) {
		this.FECHA_PAGO_DINAMICA = valor;
	}
		
	
	/** AGREGADO REINTEGROS FECHA DE PAGO PRESCRIPTO
	* Recupera el valor del atributo FA_RE_FECHAPAGO_PRESC
	* @return el valor de FA_RE_FECHAPAGO_PRESC
	*/ 
	public java.util.Date getFA_RE_FECHAPAGO_PRESC() {
		return FA_RE_FECHAPAGO_PRESC;
	}
    	
	/**AGREGADO REINTEGROS FECHA DE PAGO PRESCRIPTO
	* Fija el valor del atributo FA_RE_FECHAPAGO_PRESC
	* @param valor el valor de FA_RE_FECHAPAGO_PRESC
	*/ 
	public void setFA_RE_FECHAPAGO_PRESC(java.util.Date valor) {
		this.FA_RE_FECHAPAGO_PRESC = valor;
	}
	
	
	/** AGREGADO REINTEGROS CAMPO BOOLEANO PRESCRIPTO
	* Recupera el valor del atributo CAMPO BOOLEANO PRESCRIPTO
	* @return el valor de CAMPO BOOLEANO PRESCRIPTO
	*/ 
	public Boolean getPRESCRIPTO() {
		return PRESCRIPTO;
	}
    	
	/**AGREGADO REINTEGROS CAMPO BOOLEANO PRESCRIPTO
	* Fija el valor del atributo CAMPO BOOLEANO PRESCRIPTO
	* @param valor el valor de CAMPO BOOLEANO PRESCRIPTO
	*/ 
	public void setPRESCRIPTO(Boolean valor) {
		this.PRESCRIPTO = valor;
	}
	
	
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Campo_no_usado_17=["+this.getCampo_no_usado_17()+"]\r\n");
				sb.append("Fa_no_hora_alta_nc=["+this.getFa_no_hora_alta_nc()+"]\r\n");
				sb.append("Campo_no_usado_18=["+this.getCampo_no_usado_18()+"]\r\n");
				sb.append("Fa_no_hora_alta_nd=["+this.getFa_no_hora_alta_nd()+"]\r\n");
				sb.append("Campo_no_usado_19=["+this.getCampo_no_usado_19()+"]\r\n");
				sb.append("Fa_no_hora_mod_nc=["+this.getFa_no_hora_mod_nc()+"]\r\n");
				sb.append("Campo_no_usado_20=["+this.getCampo_no_usado_20()+"]\r\n");
				sb.append("Fa_no_hora_mod_nd=["+this.getFa_no_hora_mod_nd()+"]\r\n");
				sb.append("Fa_no_cat_gran_cli=["+this.getFa_no_cat_gran_cli()+"]\r\n");
				sb.append("Fa_no_nro_gran_cli=["+this.getFa_no_nro_gran_cli()+"]\r\n");
				sb.append("Fa_no_interurb=["+this.getFa_no_interurb()+"]\r\n");
				sb.append("Fa_no_urb=["+this.getFa_no_urb()+"]\r\n");
				sb.append("Fa_no_linea=["+this.getFa_no_linea()+"]\r\n");
				sb.append("Fa_no_ident_abo=["+this.getFa_no_ident_abo()+"]\r\n");
				sb.append("Fa_re_interurb=["+this.getFa_re_interurb()+"]\r\n");
				sb.append("Fa_re_urb=["+this.getFa_re_urb()+"]\r\n");
				sb.append("Fa_re_linea=["+this.getFa_re_linea()+"]\r\n");
				sb.append("Fa_re_ident_abo=["+this.getFa_re_ident_abo()+"]\r\n");
				sb.append("Fa_re_cod_postal_loc=["+this.getFa_re_cod_postal_loc()+"]\r\n");
				sb.append("Fa_re_cod_postal_loc_benf=["+this.getFa_re_cod_postal_loc_benf()+"]\r\n");
				sb.append("Fa_re_fecha_solic_nvo=["+this.getFa_re_fecha_solic_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_emis_op_nvo=["+this.getFa_re_fecha_emis_op_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_pago_nvo=["+this.getFa_re_fecha_pago_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_gener_cheque_nvo=["+this.getFa_re_fecha_gener_cheque_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_para_dolariz_nvo=["+this.getFa_re_fecha_para_dolariz_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_alta_nvo=["+this.getFa_re_fecha_alta_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_modific_nvo=["+this.getFa_re_fecha_modific_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_baja_nvo=["+this.getFa_re_fecha_baja_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_superv_nvo=["+this.getFa_re_fecha_superv_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_autoriz_nvo=["+this.getFa_re_fecha_autoriz_nvo()+"]\r\n");
				sb.append("Fa_re_fecha_pago_misc=["+this.getFa_re_fecha_pago_misc()+"]\r\n");
				sb.append("Fa_re_origen=["+this.getFa_re_origen()+"]\r\n");
				sb.append("Fa_no_fecha_alta_reintegro_nvo=["+this.getFa_no_fecha_alta_reintegro_nvo()+"]\r\n");
				sb.append("Fa_no_fec_ing_contable_nc_nvo=["+this.getFa_no_fec_ing_contable_nc_nvo()+"]\r\n");
				sb.append("Fa_no_fec_ing_contable_nd_nvo=["+this.getFa_no_fec_ing_contable_nd_nvo()+"]\r\n");
				sb.append("Fa_no_fecha_para_dolariz_nvo=["+this.getFa_no_fecha_para_dolariz_nvo()+"]\r\n");
				sb.append("Fa_no_fecha_envio_gl_nc_nvo=["+this.getFa_no_fecha_envio_gl_nc_nvo()+"]\r\n");
				sb.append("Fa_no_fecha_envio_gl_nd_nvo=["+this.getFa_no_fecha_envio_gl_nd_nvo()+"]\r\n");
				sb.append("Fa_no_fecha_alta_nc_nvo=["+this.getFa_no_fecha_alta_nc_nvo()+"]\r\n");
				sb.append("Fa_no_fecha_alta_nd_nvo=["+this.getFa_no_fecha_alta_nd_nvo()+"]\r\n");
				sb.append("Fa_no_fecha_mod_nc_nvo=["+this.getFa_no_fecha_mod_nc_nvo()+"]\r\n");
				sb.append("Fa_no_fecha_mod_nd_nvo=["+this.getFa_no_fecha_mod_nd_nvo()+"]\r\n");
				sb.append("Fa_re_imputacion_estado=["+this.getFa_re_imputacion_estado()+"]\r\n");
				sb.append("Fa_re_cod_barra=["+this.getFa_re_cod_barra()+"]\r\n");
				sb.append("Fa_re_e_marca_tarea=["+this.getFa_re_e_marca_tarea()+"]\r\n");
				sb.append("Fa_re_e_banco=["+this.getFa_re_e_banco()+"]\r\n");
				sb.append("Fa_re_e_imp_pagado=["+this.getFa_re_e_imp_pagado()+"]\r\n");
				sb.append("Fa_re_e_fecha_acreditac=["+this.getFa_re_e_fecha_acreditac()+"]\r\n");
				sb.append("Fa_re_e_fecha_proceso=["+this.getFa_re_e_fecha_proceso()+"]\r\n");
				sb.append("Fa_re_e_sucursal=["+this.getFa_re_e_sucursal()+"]\r\n");
				sb.append("Fa_re_e_fecha_pres=["+this.getFa_re_e_fecha_pres()+"]\r\n");
				sb.append("Fa_re_e_fecha_pago=["+this.getFa_re_e_fecha_pago()+"]\r\n");
				sb.append("Fa_re_fecha_bloqueo=["+this.getFa_re_fecha_bloqueo()+"]\r\n");
				sb.append("Fa_re_usuario_bloqueo=["+this.getFa_re_usuario_bloqueo()+"]\r\n");
				sb.append("Fa_re_hora_bloqueo=["+this.getFa_re_hora_bloqueo()+"]\r\n");
				sb.append("Fa_re_pago_duplicado=["+this.getFa_re_pago_duplicado()+"]\r\n");
				sb.append("Fa_re_pcia_base_perc=["+this.getFa_re_pcia_base_perc()+"]\r\n");
				sb.append("Fa_re_nro_movimiento=["+this.getFa_re_nro_movimiento()+"]\r\n");
				sb.append("Fa_re_fecha_desbloqueo=["+this.getFa_re_fecha_desbloqueo()+"]\r\n");
				sb.append("Fa_re_usu_desbloqueo=["+this.getFa_re_usu_desbloqueo()+"]\r\n");
				sb.append("Fa_re_fecha_migracion=["+this.getFa_re_fecha_migracion()+"]\r\n");
				sb.append("Fa_re_cliente_atis=["+this.getFa_re_cliente_atis()+"]\r\n");
				sb.append("Fa_re_cuenta_atis=["+this.getFa_re_cuenta_atis()+"]\r\n");
				sb.append("Fa_re_motivo_transf=["+this.getFa_re_motivo_transf()+"]\r\n");
				sb.append("Fa_re_operador_loc=["+this.getFa_re_operador_loc()+"]\r\n");
				sb.append("Fa_re_gp_apynom=["+this.getFa_re_gp_apynom()+"]\r\n");
				sb.append("Fa_re_gp_tipo_doc=["+this.getFa_re_gp_tipo_doc()+"]\r\n");
				sb.append("Fa_re_gp_nro_doc=["+this.getFa_re_gp_nro_doc()+"]\r\n");
				sb.append("Fa_re_gp_domic=["+this.getFa_re_gp_domic()+"]\r\n");
				sb.append("Fa_re_gp_cod_postal=["+this.getFa_re_gp_cod_postal()+"]\r\n");
				sb.append("Fa_re_gp_localidad=["+this.getFa_re_gp_localidad()+"]\r\n");
				sb.append("Fa_re_gp_provincia=["+this.getFa_re_gp_provincia()+"]\r\n");
				sb.append("Fa_re_numero_pago=["+this.getFa_re_numero_pago()+"]\r\n");
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Fa_co_clave_numero=["+this.getFa_co_clave_numero()+"]\r\n");
				sb.append("Fa_co_prox_numero=["+this.getFa_co_prox_numero()+"]\r\n");
				sb.append("Fa_co_ident_abo=["+this.getFa_co_ident_abo()+"]\r\n");
				sb.append("Fa_re_num_reintegro=["+this.getFa_re_num_reintegro()+"]\r\n");
				sb.append("Fa_re_zona_solic=["+this.getFa_re_zona_solic()+"]\r\n");
				sb.append("Campo_no_usado_1=["+this.getCampo_no_usado_1()+"]\r\n");
				sb.append("Fa_re_zona=["+this.getFa_re_zona()+"]\r\n");
				sb.append("Fa_re_oficina=["+this.getFa_re_oficina()+"]\r\n");
				sb.append("Fa_re_abonado=["+this.getFa_re_abonado()+"]\r\n");
				sb.append("Fa_re_tip_fact=["+this.getFa_re_tip_fact()+"]\r\n");
				sb.append("Fa_re_anio_lote=["+this.getFa_re_anio_lote()+"]\r\n");
				sb.append("Fa_re_serie_meg=["+this.getFa_re_serie_meg()+"]\r\n");
				sb.append("Fa_re_n_contrato_meg=["+this.getFa_re_n_contrato_meg()+"]\r\n");
				sb.append("Fa_re_n_cuota_meg=["+this.getFa_re_n_cuota_meg()+"]\r\n");
				sb.append("Fa_re_nro_factura=["+this.getFa_re_nro_factura()+"]\r\n");
				sb.append("Campo_no_usado_2=["+this.getCampo_no_usado_2()+"]\r\n");
				
				sb.append("Fa_re_motivo=["+this.getFa_re_motivo()+"]\r\n");
				// AGREGADO DESCRIPCION DEL CAMPO MOTIVO
				sb.append("FA_RE_MOTIVO_DESCRIPCION=["+this.getFA_RE_MOTIVO_DESCRIPCION()+"]\r\n");
				
				
				sb.append("Fa_re_estado=["+this.getFa_re_estado()+"]\r\n");
				
				sb.append("Fa_re_nro_copia=["+this.getFa_re_nro_copia()+"]\r\n");				
				// AGREGADO DESCRIPCION DEL CAMPO NRO COPIA
				sb.append("FA_RE_NRO_COPIA_DESCRIPCION=["+this.getFA_RE_NRO_COPIA_DESCRIPCION()+"]\r\n");
								
								
				sb.append("Fa_re_importe_tot_concepto=["+this.getFa_re_importe_tot_concepto()+"]\r\n");
				sb.append("Fa_re_importe_tot_iva=["+this.getFa_re_importe_tot_iva()+"]\r\n");
				sb.append("Fa_re_importe_tot_iva_adic=["+this.getFa_re_importe_tot_iva_adic()+"]\r\n");
				sb.append("Fa_re_importe_mora=["+this.getFa_re_importe_mora()+"]\r\n");
				sb.append("Fa_re_importe_tot_actualiz=["+this.getFa_re_importe_tot_actualiz()+"]\r\n");
				sb.append("Fa_re_importe_tot_ing_b_adic=["+this.getFa_re_importe_tot_ing_b_adic()+"]\r\n");
				sb.append("Fa_re_apell_nom_lin1=["+this.getFa_re_apell_nom_lin1()+"]\r\n");
				sb.append("Fa_re_an_domic_lin2=["+this.getFa_re_an_domic_lin2()+"]\r\n");
				sb.append("Fa_re_domicil_lin3=["+this.getFa_re_domicil_lin3()+"]\r\n");
				sb.append("Fa_re_codpos_loc_lin4=["+this.getFa_re_codpos_loc_lin4()+"]\r\n");
				sb.append("Fa_re_dpto_lin6=["+this.getFa_re_dpto_lin6()+"]\r\n");
				sb.append("Fa_re_categoria=["+this.getFa_re_categoria()+"]\r\n");
				sb.append("Fa_re_tip_contrib=["+this.getFa_re_tip_contrib()+"]\r\n");
				sb.append("Fa_re_nro_contrib=["+this.getFa_re_nro_contrib()+"]\r\n");
				sb.append("Fa_re_apell_nom_lin1_benf=["+this.getFa_re_apell_nom_lin1_benf()+"]\r\n");
				//AGREGADO DE LA DESCRIPCION
				sb.append("FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION=["+this.getFA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION()+"]\r\n");
				
				
				sb.append("Fa_re_an_domic_lin2_benf=["+this.getFa_re_an_domic_lin2_benf()+"]\r\n");
				sb.append("Fa_re_domicil_lin3_benf=["+this.getFa_re_domicil_lin3_benf()+"]\r\n");
				sb.append("Fa_re_codpos_loc_lin4_benf=["+this.getFa_re_codpos_loc_lin4_benf()+"]\r\n");
				sb.append("Fa_re_dpto_loc_lin5_benf=["+this.getFa_re_dpto_loc_lin5_benf()+"]\r\n");
				sb.append("Fa_re_tipo_contrib_loc=["+this.getFa_re_tipo_contrib_loc()+"]\r\n");
				sb.append("Fa_re_nro_contrib_loc=["+this.getFa_re_nro_contrib_loc()+"]\r\n");
				sb.append("Fa_re_tipo_documento=["+this.getFa_re_tipo_documento()+"]\r\n");
				sb.append("Fa_re_num_documento=["+this.getFa_re_num_documento()+"]\r\n");
				sb.append("Fa_re_n_cuenta_clie_gob=["+this.getFa_re_n_cuenta_clie_gob()+"]\r\n");
				sb.append("Fa_re_dias_actualiz=["+this.getFa_re_dias_actualiz()+"]\r\n");
				sb.append("Fa_re_valor_pulso=["+this.getFa_re_valor_pulso()+"]\r\n");
				sb.append("Campo_no_usado_3=["+this.getCampo_no_usado_3()+"]\r\n");
				sb.append("Fa_re_zona_pago=["+this.getFa_re_zona_pago()+"]\r\n");
				sb.append("Fa_re_cajero=["+this.getFa_re_cajero()+"]\r\n");
				
				sb.append("Fa_re_tipo_pago=["+this.getFa_re_tipo_pago()+"]\r\n");
				// AGREGADO DESCRIPCION DEL CAMPO TIPO DE PAGO
				sb.append("FA_RE_TIPO_PAGO_DESCRIPCION=["+this.getFA_RE_TIPO_PAGO_DESCRIPCION()+"]\r\n");
				
				
				
				
				sb.append("Campo_no_usado_4=["+this.getCampo_no_usado_4()+"]\r\n");
				sb.append("Campo_no_usado_5=["+this.getCampo_no_usado_5()+"]\r\n");
				sb.append("Fa_re_marca_tarea=["+this.getFa_re_marca_tarea()+"]\r\n");
				sb.append("Fa_re_nro_orden_pago=["+this.getFa_re_nro_orden_pago()+"]\r\n");
				sb.append("Campo_no_usado_6=["+this.getCampo_no_usado_6()+"]\r\n");
				sb.append("Fa_re_hora_alta=["+this.getFa_re_hora_alta()+"]\r\n");
				sb.append("Fa_re_usuario_alta=["+this.getFa_re_usuario_alta()+"]\r\n");
				sb.append("Campo_no_usado_7=["+this.getCampo_no_usado_7()+"]\r\n");
				sb.append("Fa_re_hora_modific=["+this.getFa_re_hora_modific()+"]\r\n");
				sb.append("Fa_re_usuario_modific=["+this.getFa_re_usuario_modific()+"]\r\n");
				sb.append("Campo_no_usado_8=["+this.getCampo_no_usado_8()+"]\r\n");
				sb.append("Fa_re_hora_baja=["+this.getFa_re_hora_baja()+"]\r\n");
				sb.append("Fa_re_usuario_baja=["+this.getFa_re_usuario_baja()+"]\r\n");
				sb.append("Fa_re_usuario_superv=["+this.getFa_re_usuario_superv()+"]\r\n");
				sb.append("Campo_no_usado_9=["+this.getCampo_no_usado_9()+"]\r\n");
				sb.append("Fa_re_hora_superv=["+this.getFa_re_hora_superv()+"]\r\n");
				sb.append("Fa_re_usuario_autoriz=["+this.getFa_re_usuario_autoriz()+"]\r\n");
				sb.append("Campo_no_usado_10=["+this.getCampo_no_usado_10()+"]\r\n");
				sb.append("Fa_re_hora_autoriz=["+this.getFa_re_hora_autoriz()+"]\r\n");
				sb.append("Fa_re_cat_gran_cli=["+this.getFa_re_cat_gran_cli()+"]\r\n");
				sb.append("Fa_re_nro_gran_cli=["+this.getFa_re_nro_gran_cli()+"]\r\n");
				sb.append("Fa_re_pcia_porc=["+this.getFa_re_pcia_porc()+"]\r\n");
				sb.append("Fa_re_fecha_impre=["+this.getFa_re_fecha_impre()+"]\r\n");
				sb.append("Fa_re_porc_iva_rg17=["+this.getFa_re_porc_iva_rg17()+"]\r\n");
				sb.append("Fa_no_nro_reintegro=["+this.getFa_no_nro_reintegro()+"]\r\n");
				sb.append("Campo_no_usado_11=["+this.getCampo_no_usado_11()+"]\r\n");
				sb.append("Fa_no_nro_nc=["+this.getFa_no_nro_nc()+"]\r\n");
				sb.append("Campo_no_usado_12=["+this.getCampo_no_usado_12()+"]\r\n");
				sb.append("Fa_no_nro_nd=["+this.getFa_no_nro_nd()+"]\r\n");
				sb.append("Campo_no_usado_13=["+this.getCampo_no_usado_13()+"]\r\n");
				sb.append("Campo_no_usado_14=["+this.getCampo_no_usado_14()+"]\r\n");
				sb.append("Fa_no_motivo=["+this.getFa_no_motivo()+"]\r\n");
				sb.append("Fa_no_zona=["+this.getFa_no_zona()+"]\r\n");
				sb.append("Fa_no_oficina=["+this.getFa_no_oficina()+"]\r\n");
				sb.append("Fa_no_abonado=["+this.getFa_no_abonado()+"]\r\n");
				sb.append("Fa_no_tipo_fact=["+this.getFa_no_tipo_fact()+"]\r\n");
				sb.append("Fa_no_ano_lote=["+this.getFa_no_ano_lote()+"]\r\n");
				sb.append("Fa_no_serie_meg=["+this.getFa_no_serie_meg()+"]\r\n");
				sb.append("Fa_no_n_contrato_meg=["+this.getFa_no_n_contrato_meg()+"]\r\n");
				sb.append("Fa_no_n_cuota_meg=["+this.getFa_no_n_cuota_meg()+"]\r\n");
				sb.append("Fa_no_categoria=["+this.getFa_no_categoria()+"]\r\n");
				sb.append("Fa_no_tipo_contrib=["+this.getFa_no_tipo_contrib()+"]\r\n");
				sb.append("Fa_no_tipo_contrib_nvo=["+this.getFa_no_tipo_contrib_nvo()+"]\r\n");
				sb.append("Campo_no_usado_15=["+this.getCampo_no_usado_15()+"]\r\n");
				sb.append("Campo_no_usado_16=["+this.getCampo_no_usado_16()+"]\r\n");
				sb.append("Fa_no_marca_tarea=["+this.getFa_no_marca_tarea()+"]\r\n");
				sb.append("FECHA_PAGO_DINAMICA=["+this.getFECHA_PAGO_DINAMICA()+"]\r\n");
				sb.append("FA_RE_FECHAPAGO_PRESC=["+this.getFA_RE_FECHAPAGO_PRESC()+"]\r\n");
				sb.append("PRESCRIPTO=["+this.getPRESCRIPTO()+"]\r\n");
				
				return sb.toString();
	}


}