package arg.ros.acc.vo.reintegros;

/**
* SfaMMiscelaneosFisicoVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class SfaMMiscelaneosFisicoVO {

	private Double adabas_isn;
	private Double fa_m_zona;
	private Double fa_m_oficina;
	private Double fa_m_abonado;
	private Double fa_m_tipo_factura;
	private Double fa_m_anio_lote;
	private Double fa_m_zona_pago;
	private java.util.Date fa_m_fecha_pago;
	private java.util.Date fa_m_fecha_imputacion;
	private java.util.Date fa_m_fecha_proceso_tarea;
	private Double fa_m_marca_tarea;
	private java.math.BigDecimal fa_m_importe_miscelaneos;
	private Double fa_m_estado_bajada;
	private Double fa_m_concepto_fact;
	private Double fa_m_zona_facturada;
	private Double fa_m_oficina_facturada;
	private Double fa_m_abonado_facturado;
	private Double fa_m_anio_lote_facturado;
	private Double fa_m_tipo_servicio_facturado;
	private java.util.Date fa_m_fecha_alta;
	private String fa_m_usuario_alta;
	private java.util.Date fa_m_fecha_envio;
	private String fa_m_usuario_envio;
	private java.util.Date fa_m_fecha_baja;
	private String fa_m_usuario_baja;
	private String fa_m_fecha_vencimiento;
	private java.util.Date fa_m_fecha_facturacion;
	private String fa_m_usuario_facturacion;
	private Double fa_m_nro_reinteg;
	private Double fa_m_cpto_zona;
	private Double fa_m_cpto_oficina;
	private Double fa_m_cpto_abonado;
	private Double fa_m_cpto_tipo_fact;
	private Double fa_m_cpto_anio_lote;
	private Double fa_m_cpto_concepto;
	private Double fa_m_cpto_cod_tercero;
	private Double fa_m_cpto_estado;
	private Double fa_m_cpto_tarea;
	private Double fa_m_cpto_importe;
	private java.util.Date fa_m_cpto_fecha_alta;
	private String fa_m_cpto_usuario_alta;
	private java.util.Date fa_m_cpto_fecha_aplicacion;
	private String fa_m_cpto_usuario_aplicacion;
	private String fa_m_interurb;
	private String fa_m_urb;
	private String fa_m_linea;
	private Double fa_m_ident_abo;
	private String fa_m_interurb_facturado;
	private String fa_m_urb_facturado;
	private String fa_m_linea_facturada;
	
	// AGREGADO
	private String USUARIO_DINAMICO;
	//private java.util.Date FECHA_DINAMICA;
	private java.util.Date FECHA_DINAMICA;
	
	private String ESTADO_DINAMICO;
	private String DESC_MISC_R;
	

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public SfaMMiscelaneosFisicoVO() {

	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public Double getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(Double valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_zona
	* @return el valor de Fa_m_zona
	*/ 
	public Double getFa_m_zona() {
		return fa_m_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_zona
	* @param valor el valor de Fa_m_zona
	*/ 
	public void setFa_m_zona(Double valor) {
		this.fa_m_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_oficina
	* @return el valor de Fa_m_oficina
	*/ 
	public Double getFa_m_oficina() {
		return fa_m_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_oficina
	* @param valor el valor de Fa_m_oficina
	*/ 
	public void setFa_m_oficina(Double valor) {
		this.fa_m_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_abonado
	* @return el valor de Fa_m_abonado
	*/ 
	public Double getFa_m_abonado() {
		return fa_m_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_abonado
	* @param valor el valor de Fa_m_abonado
	*/ 
	public void setFa_m_abonado(Double valor) {
		this.fa_m_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_tipo_factura
	* @return el valor de Fa_m_tipo_factura
	*/ 
	public Double getFa_m_tipo_factura() {
		return fa_m_tipo_factura;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_tipo_factura
	* @param valor el valor de Fa_m_tipo_factura
	*/ 
	public void setFa_m_tipo_factura(Double valor) {
		this.fa_m_tipo_factura = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_anio_lote
	* @return el valor de Fa_m_anio_lote
	*/ 
	public Double getFa_m_anio_lote() {
		return fa_m_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_anio_lote
	* @param valor el valor de Fa_m_anio_lote
	*/ 
	public void setFa_m_anio_lote(Double valor) {
		this.fa_m_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_zona_pago
	* @return el valor de Fa_m_zona_pago
	*/ 
	public Double getFa_m_zona_pago() {
		return fa_m_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_zona_pago
	* @param valor el valor de Fa_m_zona_pago
	*/ 
	public void setFa_m_zona_pago(Double valor) {
		this.fa_m_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_pago
	* @return el valor de Fa_m_fecha_pago
	*/ 
	public java.util.Date getFa_m_fecha_pago() {
		return fa_m_fecha_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_pago
	* @param valor el valor de Fa_m_fecha_pago
	*/ 
	public void setFa_m_fecha_pago(java.util.Date valor) {
		this.fa_m_fecha_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_imputacion
	* @return el valor de Fa_m_fecha_imputacion
	*/ 
	public java.util.Date getFa_m_fecha_imputacion() {
		return fa_m_fecha_imputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_imputacion
	* @param valor el valor de Fa_m_fecha_imputacion
	*/ 
	public void setFa_m_fecha_imputacion(java.util.Date valor) {
		this.fa_m_fecha_imputacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_proceso_tarea
	* @return el valor de Fa_m_fecha_proceso_tarea
	*/ 
	public java.util.Date getFa_m_fecha_proceso_tarea() {
		return fa_m_fecha_proceso_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_proceso_tarea
	* @param valor el valor de Fa_m_fecha_proceso_tarea
	*/ 
	public void setFa_m_fecha_proceso_tarea(java.util.Date valor) {
		this.fa_m_fecha_proceso_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_marca_tarea
	* @return el valor de Fa_m_marca_tarea
	*/ 
	public Double getFa_m_marca_tarea() {
		return fa_m_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_marca_tarea
	* @param valor el valor de Fa_m_marca_tarea
	*/ 
	public void setFa_m_marca_tarea(Double valor) {
		this.fa_m_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_importe_miscelaneos
	* @return el valor de Fa_m_importe_miscelaneos
	*/ 
	public java.math.BigDecimal getFa_m_importe_miscelaneos() {
		return fa_m_importe_miscelaneos;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_importe_miscelaneos
	* @param valor el valor de Fa_m_importe_miscelaneos
	*/ 
	public void setFa_m_importe_miscelaneos(java.math.BigDecimal valor) {
		this.fa_m_importe_miscelaneos = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_estado_bajada
	* @return el valor de Fa_m_estado_bajada
	*/ 
	public Double getFa_m_estado_bajada() {
		return fa_m_estado_bajada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_estado_bajada
	* @param valor el valor de Fa_m_estado_bajada
	*/ 
	public void setFa_m_estado_bajada(Double valor) {
		this.fa_m_estado_bajada = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_concepto_fact
	* @return el valor de Fa_m_concepto_fact
	*/ 
	public Double getFa_m_concepto_fact() {
		return fa_m_concepto_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_concepto_fact
	* @param valor el valor de Fa_m_concepto_fact
	*/ 
	public void setFa_m_concepto_fact(Double valor) {
		this.fa_m_concepto_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_zona_facturada
	* @return el valor de Fa_m_zona_facturada
	*/ 
	public Double getFa_m_zona_facturada() {
		return fa_m_zona_facturada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_zona_facturada
	* @param valor el valor de Fa_m_zona_facturada
	*/ 
	public void setFa_m_zona_facturada(Double valor) {
		this.fa_m_zona_facturada = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_oficina_facturada
	* @return el valor de Fa_m_oficina_facturada
	*/ 
	public Double getFa_m_oficina_facturada() {
		return fa_m_oficina_facturada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_oficina_facturada
	* @param valor el valor de Fa_m_oficina_facturada
	*/ 
	public void setFa_m_oficina_facturada(Double valor) {
		this.fa_m_oficina_facturada = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_abonado_facturado
	* @return el valor de Fa_m_abonado_facturado
	*/ 
	public Double getFa_m_abonado_facturado() {
		return fa_m_abonado_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_abonado_facturado
	* @param valor el valor de Fa_m_abonado_facturado
	*/ 
	public void setFa_m_abonado_facturado(Double valor) {
		this.fa_m_abonado_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_anio_lote_facturado
	* @return el valor de Fa_m_anio_lote_facturado
	*/ 
	public Double getFa_m_anio_lote_facturado() {
		return fa_m_anio_lote_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_anio_lote_facturado
	* @param valor el valor de Fa_m_anio_lote_facturado
	*/ 
	public void setFa_m_anio_lote_facturado(Double valor) {
		this.fa_m_anio_lote_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_tipo_servicio_facturado
	* @return el valor de Fa_m_tipo_servicio_facturado
	*/ 
	public Double getFa_m_tipo_servicio_facturado() {
		return fa_m_tipo_servicio_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_tipo_servicio_facturado
	* @param valor el valor de Fa_m_tipo_servicio_facturado
	*/ 
	public void setFa_m_tipo_servicio_facturado(Double valor) {
		this.fa_m_tipo_servicio_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_alta
	* @return el valor de Fa_m_fecha_alta
	*/ 
	public java.util.Date getFa_m_fecha_alta() {
		return fa_m_fecha_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_alta
	* @param valor el valor de Fa_m_fecha_alta
	*/ 
	public void setFa_m_fecha_alta(java.util.Date valor) {
		this.fa_m_fecha_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_alta
	* @return el valor de Fa_m_usuario_alta
	*/ 
	public String getFa_m_usuario_alta() {
		return fa_m_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_alta
	* @param valor el valor de Fa_m_usuario_alta
	*/ 
	public void setFa_m_usuario_alta(String valor) {
		this.fa_m_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_envio
	* @return el valor de Fa_m_fecha_envio
	*/ 
	public java.util.Date getFa_m_fecha_envio() {
		return fa_m_fecha_envio;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_envio
	* @param valor el valor de Fa_m_fecha_envio
	*/ 
	public void setFa_m_fecha_envio(java.util.Date valor) {
		this.fa_m_fecha_envio = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_envio
	* @return el valor de Fa_m_usuario_envio
	*/ 
	public String getFa_m_usuario_envio() {
		return fa_m_usuario_envio;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_envio
	* @param valor el valor de Fa_m_usuario_envio
	*/ 
	public void setFa_m_usuario_envio(String valor) {
		this.fa_m_usuario_envio = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_baja
	* @return el valor de Fa_m_fecha_baja
	*/ 
	public java.util.Date getFa_m_fecha_baja() {
		return fa_m_fecha_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_baja
	* @param valor el valor de Fa_m_fecha_baja
	*/ 
	public void setFa_m_fecha_baja(java.util.Date valor) {
		this.fa_m_fecha_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_baja
	* @return el valor de Fa_m_usuario_baja
	*/ 
	public String getFa_m_usuario_baja() {
		return fa_m_usuario_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_baja
	* @param valor el valor de Fa_m_usuario_baja
	*/ 
	public void setFa_m_usuario_baja(String valor) {
		this.fa_m_usuario_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_vencimiento
	* @return el valor de Fa_m_fecha_vencimiento
	*/ 
	public String getFa_m_fecha_vencimiento() {
		return fa_m_fecha_vencimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_vencimiento
	* @param valor el valor de Fa_m_fecha_vencimiento
	*/ 
	public void setFa_m_fecha_vencimiento(String valor) {
		this.fa_m_fecha_vencimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_facturacion
	* @return el valor de Fa_m_fecha_facturacion
	*/ 
	public java.util.Date getFa_m_fecha_facturacion() {
		return fa_m_fecha_facturacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_facturacion
	* @param valor el valor de Fa_m_fecha_facturacion
	*/ 
	public void setFa_m_fecha_facturacion(java.util.Date valor) {
		this.fa_m_fecha_facturacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_facturacion
	* @return el valor de Fa_m_usuario_facturacion
	*/ 
	public String getFa_m_usuario_facturacion() {
		return fa_m_usuario_facturacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_facturacion
	* @param valor el valor de Fa_m_usuario_facturacion
	*/ 
	public void setFa_m_usuario_facturacion(String valor) {
		this.fa_m_usuario_facturacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_nro_reinteg
	* @return el valor de Fa_m_nro_reinteg
	*/ 
	public Double getFa_m_nro_reinteg() {
		return fa_m_nro_reinteg;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_nro_reinteg
	* @param valor el valor de Fa_m_nro_reinteg
	*/ 
	public void setFa_m_nro_reinteg(Double valor) {
		this.fa_m_nro_reinteg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_zona
	* @return el valor de Fa_m_cpto_zona
	*/ 
	public Double getFa_m_cpto_zona() {
		return fa_m_cpto_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_zona
	* @param valor el valor de Fa_m_cpto_zona
	*/ 
	public void setFa_m_cpto_zona(Double valor) {
		this.fa_m_cpto_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_oficina
	* @return el valor de Fa_m_cpto_oficina
	*/ 
	public Double getFa_m_cpto_oficina() {
		return fa_m_cpto_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_oficina
	* @param valor el valor de Fa_m_cpto_oficina
	*/ 
	public void setFa_m_cpto_oficina(Double valor) {
		this.fa_m_cpto_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_abonado
	* @return el valor de Fa_m_cpto_abonado
	*/ 
	public Double getFa_m_cpto_abonado() {
		return fa_m_cpto_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_abonado
	* @param valor el valor de Fa_m_cpto_abonado
	*/ 
	public void setFa_m_cpto_abonado(Double valor) {
		this.fa_m_cpto_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_tipo_fact
	* @return el valor de Fa_m_cpto_tipo_fact
	*/ 
	public Double getFa_m_cpto_tipo_fact() {
		return fa_m_cpto_tipo_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_tipo_fact
	* @param valor el valor de Fa_m_cpto_tipo_fact
	*/ 
	public void setFa_m_cpto_tipo_fact(Double valor) {
		this.fa_m_cpto_tipo_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_anio_lote
	* @return el valor de Fa_m_cpto_anio_lote
	*/ 
	public Double getFa_m_cpto_anio_lote() {
		return fa_m_cpto_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_anio_lote
	* @param valor el valor de Fa_m_cpto_anio_lote
	*/ 
	public void setFa_m_cpto_anio_lote(Double valor) {
		this.fa_m_cpto_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_concepto
	* @return el valor de Fa_m_cpto_concepto
	*/ 
	public Double getFa_m_cpto_concepto() {
		return fa_m_cpto_concepto;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_concepto
	* @param valor el valor de Fa_m_cpto_concepto
	*/ 
	public void setFa_m_cpto_concepto(Double valor) {
		this.fa_m_cpto_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_cod_tercero
	* @return el valor de Fa_m_cpto_cod_tercero
	*/ 
	public Double getFa_m_cpto_cod_tercero() {
		return fa_m_cpto_cod_tercero;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_cod_tercero
	* @param valor el valor de Fa_m_cpto_cod_tercero
	*/ 
	public void setFa_m_cpto_cod_tercero(Double valor) {
		this.fa_m_cpto_cod_tercero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_estado
	* @return el valor de Fa_m_cpto_estado
	*/ 
	public Double getFa_m_cpto_estado() {
		return fa_m_cpto_estado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_estado
	* @param valor el valor de Fa_m_cpto_estado
	*/ 
	public void setFa_m_cpto_estado(Double valor) {
		this.fa_m_cpto_estado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_tarea
	* @return el valor de Fa_m_cpto_tarea
	*/ 
	public Double getFa_m_cpto_tarea() {
		return fa_m_cpto_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_tarea
	* @param valor el valor de Fa_m_cpto_tarea
	*/ 
	public void setFa_m_cpto_tarea(Double valor) {
		this.fa_m_cpto_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_importe
	* @return el valor de Fa_m_cpto_importe
	*/ 
	public Double getFa_m_cpto_importe() {
		return fa_m_cpto_importe;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_importe
	* @param valor el valor de Fa_m_cpto_importe
	*/ 
	public void setFa_m_cpto_importe(Double valor) {
		this.fa_m_cpto_importe = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_fecha_alta
	* @return el valor de Fa_m_cpto_fecha_alta
	*/ 
	public java.util.Date getFa_m_cpto_fecha_alta() {
		return fa_m_cpto_fecha_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_fecha_alta
	* @param valor el valor de Fa_m_cpto_fecha_alta
	*/ 
	public void setFa_m_cpto_fecha_alta(java.util.Date valor) {
		this.fa_m_cpto_fecha_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_usuario_alta
	* @return el valor de Fa_m_cpto_usuario_alta
	*/ 
	public String getFa_m_cpto_usuario_alta() {
		return fa_m_cpto_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_usuario_alta
	* @param valor el valor de Fa_m_cpto_usuario_alta
	*/ 
	public void setFa_m_cpto_usuario_alta(String valor) {
		this.fa_m_cpto_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_fecha_aplicacion
	* @return el valor de Fa_m_cpto_fecha_aplicacion
	*/ 
	public java.util.Date getFa_m_cpto_fecha_aplicacion() {
		return fa_m_cpto_fecha_aplicacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_fecha_aplicacion
	* @param valor el valor de Fa_m_cpto_fecha_aplicacion
	*/ 
	public void setFa_m_cpto_fecha_aplicacion(java.util.Date valor) {
		this.fa_m_cpto_fecha_aplicacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_usuario_aplicacion
	* @return el valor de Fa_m_cpto_usuario_aplicacion
	*/ 
	public String getFa_m_cpto_usuario_aplicacion() {
		return fa_m_cpto_usuario_aplicacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_usuario_aplicacion
	* @param valor el valor de Fa_m_cpto_usuario_aplicacion
	*/ 
	public void setFa_m_cpto_usuario_aplicacion(String valor) {
		this.fa_m_cpto_usuario_aplicacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_interurb
	* @return el valor de Fa_m_interurb
	*/ 
	public String getFa_m_interurb() {
		return fa_m_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_interurb
	* @param valor el valor de Fa_m_interurb
	*/ 
	public void setFa_m_interurb(String valor) {
		this.fa_m_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_urb
	* @return el valor de Fa_m_urb
	*/ 
	public String getFa_m_urb() {
		return fa_m_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_urb
	* @param valor el valor de Fa_m_urb
	*/ 
	public void setFa_m_urb(String valor) {
		this.fa_m_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_linea
	* @return el valor de Fa_m_linea
	*/ 
	public String getFa_m_linea() {
		return fa_m_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_linea
	* @param valor el valor de Fa_m_linea
	*/ 
	public void setFa_m_linea(String valor) {
		this.fa_m_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_ident_abo
	* @return el valor de Fa_m_ident_abo
	*/ 
	public Double getFa_m_ident_abo() {
		return fa_m_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_ident_abo
	* @param valor el valor de Fa_m_ident_abo
	*/ 
	public void setFa_m_ident_abo(Double valor) {
		this.fa_m_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_interurb_facturado
	* @return el valor de Fa_m_interurb_facturado
	*/ 
	public String getFa_m_interurb_facturado() {
		return fa_m_interurb_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_interurb_facturado
	* @param valor el valor de Fa_m_interurb_facturado
	*/ 
	public void setFa_m_interurb_facturado(String valor) {
		this.fa_m_interurb_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_urb_facturado
	* @return el valor de Fa_m_urb_facturado
	*/ 
	public String getFa_m_urb_facturado() {
		return fa_m_urb_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_urb_facturado
	* @param valor el valor de Fa_m_urb_facturado
	*/ 
	public void setFa_m_urb_facturado(String valor) {
		this.fa_m_urb_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_linea_facturada
	* @return el valor de Fa_m_linea_facturada
	*/ 
	public String getFa_m_linea_facturada() {
		return fa_m_linea_facturada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_linea_facturada
	* @param valor el valor de Fa_m_linea_facturada
	*/ 
	public void setFa_m_linea_facturada(String valor) {
		this.fa_m_linea_facturada = valor;
	}
		
	
//AGREGADO -INICIO
	
	/**
	* Recupera el valor del atributo USUARIO_DINAMICO
	* @return el valor de USUARIO_DINAMICO
	*/ 
	public String getUSUARIO_DINAMICO() {
		return USUARIO_DINAMICO;
	}
    	
	/**
	* Fija el valor del atributo USUARIO_DINAMICO
	* @param valor el valor de USUARIO_DINAMICO
	*/ 
	public void setUSUARIO_DINAMICO(String valor) {
		this.USUARIO_DINAMICO = valor;
	}
	
	/**
	* Recupera el valor del atributo FECHA_DINAMICA
	* @return el valor de FECHA_DINAMICA
	*/ 
	public java.util.Date getFECHA_DINAMICA() {
		return FECHA_DINAMICA;
	}
    	
	/**
	* Fija el valor del atributo FECHA_DINAMICA
	* @param valor el valor de FECHA_DINAMICA
	*/ 
	public void setFECHA_DINAMICA(java.util.Date valor) {
		this.FECHA_DINAMICA = valor;
	}
	
	
	/**
	* Recupera el valor del atributo ESTADO_DINAMICO
	* @return el valor de ESTADO_DINAMICO
	*/ 
	public String getESTADO_DINAMICO() {
		return ESTADO_DINAMICO;
	}
    	
	/**
	* Recupera el valor del atributo ESTADO_DINAMICO
	* @return el valor de ESTADO_DINAMICO
	*/ 
	public void setESTADO_DINAMICO(String valor) {
		this.ESTADO_DINAMICO = valor;
	}
				
	/**
	* Recupera el valor del atributo DESC_MISC_R
	* @return el valor de DESC_MISC_R
	*/
	
	public String getDESC_MISC_R() {
		return  DESC_MISC_R ;
	}
	
	/**
	* Recupera el valor del atributo DESC_MISC_R
	* @return el valor de DESC_MISC_R
	*/	
	public void setDESC_MISC_R(String valor) {
		this.DESC_MISC_R = valor;
	}

//AGREGADO - FIN 
	
	
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Adabas_isn=["+this.getAdabas_isn()+"]\r\n");
				sb.append("Fa_m_zona=["+this.getFa_m_zona()+"]\r\n");
				sb.append("Fa_m_oficina=["+this.getFa_m_oficina()+"]\r\n");
				sb.append("Fa_m_abonado=["+this.getFa_m_abonado()+"]\r\n");
				sb.append("Fa_m_tipo_factura=["+this.getFa_m_tipo_factura()+"]\r\n");
				sb.append("Fa_m_anio_lote=["+this.getFa_m_anio_lote()+"]\r\n");
				sb.append("Fa_m_zona_pago=["+this.getFa_m_zona_pago()+"]\r\n");
				sb.append("Fa_m_fecha_pago=["+this.getFa_m_fecha_pago()+"]\r\n");
				sb.append("Fa_m_fecha_imputacion=["+this.getFa_m_fecha_imputacion()+"]\r\n");
				sb.append("Fa_m_fecha_proceso_tarea=["+this.getFa_m_fecha_proceso_tarea()+"]\r\n");
				sb.append("Fa_m_marca_tarea=["+this.getFa_m_marca_tarea()+"]\r\n");
				sb.append("Fa_m_importe_miscelaneos=["+this.getFa_m_importe_miscelaneos()+"]\r\n");
				sb.append("Fa_m_estado_bajada=["+this.getFa_m_estado_bajada()+"]\r\n");
				sb.append("Fa_m_concepto_fact=["+this.getFa_m_concepto_fact()+"]\r\n");
				sb.append("Fa_m_zona_facturada=["+this.getFa_m_zona_facturada()+"]\r\n");
				sb.append("Fa_m_oficina_facturada=["+this.getFa_m_oficina_facturada()+"]\r\n");
				sb.append("Fa_m_abonado_facturado=["+this.getFa_m_abonado_facturado()+"]\r\n");
				sb.append("Fa_m_anio_lote_facturado=["+this.getFa_m_anio_lote_facturado()+"]\r\n");
				sb.append("Fa_m_tipo_servicio_facturado=["+this.getFa_m_tipo_servicio_facturado()+"]\r\n");
				sb.append("Fa_m_fecha_alta=["+this.getFa_m_fecha_alta()+"]\r\n");
				sb.append("Fa_m_usuario_alta=["+this.getFa_m_usuario_alta()+"]\r\n");
				sb.append("Fa_m_fecha_envio=["+this.getFa_m_fecha_envio()+"]\r\n");
				sb.append("Fa_m_usuario_envio=["+this.getFa_m_usuario_envio()+"]\r\n");
				sb.append("Fa_m_fecha_baja=["+this.getFa_m_fecha_baja()+"]\r\n");
				sb.append("Fa_m_usuario_baja=["+this.getFa_m_usuario_baja()+"]\r\n");
				sb.append("Fa_m_fecha_vencimiento=["+this.getFa_m_fecha_vencimiento()+"]\r\n");
				sb.append("Fa_m_fecha_facturacion=["+this.getFa_m_fecha_facturacion()+"]\r\n");
				sb.append("Fa_m_usuario_facturacion=["+this.getFa_m_usuario_facturacion()+"]\r\n");
				sb.append("Fa_m_nro_reinteg=["+this.getFa_m_nro_reinteg()+"]\r\n");
				sb.append("Fa_m_cpto_zona=["+this.getFa_m_cpto_zona()+"]\r\n");
				sb.append("Fa_m_cpto_oficina=["+this.getFa_m_cpto_oficina()+"]\r\n");
				sb.append("Fa_m_cpto_abonado=["+this.getFa_m_cpto_abonado()+"]\r\n");
				sb.append("Fa_m_cpto_tipo_fact=["+this.getFa_m_cpto_tipo_fact()+"]\r\n");
				sb.append("Fa_m_cpto_anio_lote=["+this.getFa_m_cpto_anio_lote()+"]\r\n");
				sb.append("Fa_m_cpto_concepto=["+this.getFa_m_cpto_concepto()+"]\r\n");
				sb.append("Fa_m_cpto_cod_tercero=["+this.getFa_m_cpto_cod_tercero()+"]\r\n");
				sb.append("Fa_m_cpto_estado=["+this.getFa_m_cpto_estado()+"]\r\n");
				sb.append("Fa_m_cpto_tarea=["+this.getFa_m_cpto_tarea()+"]\r\n");
				sb.append("Fa_m_cpto_importe=["+this.getFa_m_cpto_importe()+"]\r\n");
				sb.append("Fa_m_cpto_fecha_alta=["+this.getFa_m_cpto_fecha_alta()+"]\r\n");
				sb.append("Fa_m_cpto_usuario_alta=["+this.getFa_m_cpto_usuario_alta()+"]\r\n");
				sb.append("Fa_m_cpto_fecha_aplicacion=["+this.getFa_m_cpto_fecha_aplicacion()+"]\r\n");
				sb.append("Fa_m_cpto_usuario_aplicacion=["+this.getFa_m_cpto_usuario_aplicacion()+"]\r\n");
				sb.append("Fa_m_interurb=["+this.getFa_m_interurb()+"]\r\n");
				sb.append("Fa_m_urb=["+this.getFa_m_urb()+"]\r\n");
				sb.append("Fa_m_linea=["+this.getFa_m_linea()+"]\r\n");
				sb.append("Fa_m_ident_abo=["+this.getFa_m_ident_abo()+"]\r\n");
				sb.append("Fa_m_interurb_facturado=["+this.getFa_m_interurb_facturado()+"]\r\n");
				sb.append("Fa_m_urb_facturado=["+this.getFa_m_urb_facturado()+"]\r\n");
				sb.append("Fa_m_linea_facturada=["+this.getFa_m_linea_facturada()+"]\r\n");
				
				//AGREGADO
				sb.append("USUARIO_DINAMICO=["+this.getUSUARIO_DINAMICO()+"]\r\n");
				sb.append("FECHA_DINAMICA=["+this.getFECHA_DINAMICA()+"]\r\n");
				sb.append("ESTADO_DINAMICO=["+this.getESTADO_DINAMICO()+"]\r\n");
				sb.append("DESC_MISC_R=["+this.getDESC_MISC_R()+"]\r\n");
				return sb.toString();
	}
}