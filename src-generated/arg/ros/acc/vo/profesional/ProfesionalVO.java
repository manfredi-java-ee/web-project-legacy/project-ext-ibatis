package arg.ros.acc.vo.profesional;

/**
* ProfesionalVO.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ProfesionalVO {

	private Integer idprofesional;
	private java.util.Date fechaalta;
	private Double idpersona;
	private String matriculanacional;
	private String matriculaprovincial;
	private String activo;

	/**
	* Constructor. Crea una nueva instancia de la clase.
	*/
	public ProfesionalVO() {

	}
	
	/**
	* Recupera el valor del atributo Idprofesional
	* @return el valor de Idprofesional
	*/ 
	public Integer getIdprofesional() {
		return idprofesional;
	}
    	
	/**
	* Fija el valor del atributo Idprofesional
	* @param valor el valor de Idprofesional
	*/ 
	public void setIdprofesional(Integer valor) {
		this.idprofesional = valor;
	}
	/**
	* Recupera el valor del atributo Fechaalta
	* @return el valor de Fechaalta
	*/ 
	public java.util.Date getFechaalta() {
		return fechaalta;
	}
    	
	/**
	* Fija el valor del atributo Fechaalta
	* @param valor el valor de Fechaalta
	*/ 
	public void setFechaalta(java.util.Date valor) {
		this.fechaalta = valor;
	}
	/**
	* Recupera el valor del atributo Idpersona
	* @return el valor de Idpersona
	*/ 
	public Double getIdpersona() {
		return idpersona;
	}
    	
	/**
	* Fija el valor del atributo Idpersona
	* @param valor el valor de Idpersona
	*/ 
	public void setIdpersona(Double valor) {
		this.idpersona = valor;
	}
	/**
	* Recupera el valor del atributo Matriculanacional
	* @return el valor de Matriculanacional
	*/ 
	public String getMatriculanacional() {
		return matriculanacional;
	}
    	
	/**
	* Fija el valor del atributo Matriculanacional
	* @param valor el valor de Matriculanacional
	*/ 
	public void setMatriculanacional(String valor) {
		this.matriculanacional = valor;
	}
	/**
	* Recupera el valor del atributo Matriculaprovincial
	* @return el valor de Matriculaprovincial
	*/ 
	public String getMatriculaprovincial() {
		return matriculaprovincial;
	}
    	
	/**
	* Fija el valor del atributo Matriculaprovincial
	* @param valor el valor de Matriculaprovincial
	*/ 
	public void setMatriculaprovincial(String valor) {
		this.matriculaprovincial = valor;
	}
	/**
	* Recupera el valor del atributo Activo
	* @return el valor de Activo
	*/ 
	public String getActivo() {
		return activo;
	}
    	
	/**
	* Fija el valor del atributo Activo
	* @param valor el valor de Activo
	*/ 
	public void setActivo(String valor) {
		this.activo = valor;
	}
		
	/**
	* Imprime el contenido a un String.
	* @return String un String con el contenido de los atributos.
	*/
	public String toString() {
		StringBuffer sb = new StringBuffer();
				sb.append("Idprofesional=["+this.getIdprofesional()+"]\r\n");
				sb.append("Fechaalta=["+this.getFechaalta()+"]\r\n");
				sb.append("Idpersona=["+this.getIdpersona()+"]\r\n");
				sb.append("Matriculanacional=["+this.getMatriculanacional()+"]\r\n");
				sb.append("Matriculaprovincial=["+this.getMatriculaprovincial()+"]\r\n");
				sb.append("Activo=["+this.getActivo()+"]\r\n");
				return sb.toString();
	}
}