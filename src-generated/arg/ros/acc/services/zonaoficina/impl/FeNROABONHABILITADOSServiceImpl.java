package arg.ros.acc.services.zonaoficina.impl;

import java.util.*;

import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.filter.FilterFactory;
import arg.ros.acc.components.filter.FilterFactoryImpl;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.daos.zonaoficina.FeNROABONHABILITADOSDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.zonaoficina.FeNROABONHABILITADOSService;
import arg.ros.acc.vo.zonaoficina.FeNROABONHABILITADOSVO;

/**
 * @author Accenture Rosario
 *
 */
public class FeNROABONHABILITADOSServiceImpl extends BaseService implements FeNROABONHABILITADOSService {

	/**
	 * DAO para el acceso a la tabla FE_NRO_ABON_HABILITADOS_1.
	 */
	private FeNROABONHABILITADOSDAO feNROABONHABILITADOSDao;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List obtener() throws ServiceException {
		List results = new ArrayList();
		try {
			results = this.feNROABONHABILITADOSDao.select();

		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECTALL, e);
			throw new ServiceException(e);
		}
		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	 @SuppressWarnings("unchecked")
	public List obtener(Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		List results = new ArrayList();
		try {
			results = this.feNROABONHABILITADOSDao.select(filter);
			return results;
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public PaginatedList obtener(PaginatedList pPaginatedList) throws ServiceException {
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.feNROABONHABILITADOSDao.select(start, limit));
			pPaginatedList.setFullListSize(this.feNROABONHABILITADOSDao.count());
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECT, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.feNROABONHABILITADOSDao.select(start, limit, filter));
			pPaginatedList.setFullListSize(this.feNROABONHABILITADOSDao.count(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList ObtenerNrosHabilitados(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.feNROABONHABILITADOSDao.selectNrosHabilitados(start, limit, filter));
			pPaginatedList.setFullListSize(this.feNROABONHABILITADOSDao.count(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
	
	

	/**
	 * {@inheritDoc}
	 */
	public int count() throws ServiceException {
		FilterFactory filterFactory = FilterFactoryImpl.getInstance();
		Filter filter = filterFactory.emptyFilter();
		
		return this.count(filter);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Filter pFilter) throws ServiceException {
		Map properties = pFilter.getProperties();
		int count = 0;
		try {
			count = this.feNROABONHABILITADOSDao.count(properties);
			
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.COUNT, e);
			throw new ServiceException(e);
		}
		return count;
	}

	/**
	 * {@inheritDoc}
	 */
	public void agregar(final FeNROABONHABILITADOSVO pFeNROABONHABILITADOSVO) throws ServiceException {
		try {
			this.feNROABONHABILITADOSDao.insert(pFeNROABONHABILITADOSVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.INSERT, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void eliminar(final FeNROABONHABILITADOSVO pFeNROABONHABILITADOSVO) throws ServiceException {
		try {
			this.feNROABONHABILITADOSDao.delete(pFeNROABONHABILITADOSVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.DELETE, e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void modificar(final FeNROABONHABILITADOSVO pFeNROABONHABILITADOSVO) throws ServiceException {
		try {
			this.feNROABONHABILITADOSDao.update(pFeNROABONHABILITADOSVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.UPDATE, e);
			throw new ServiceException(e);
		}			
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setFeNROABONHABILITADOSDAO(FeNROABONHABILITADOSDAO pFeNROABONHABILITADOSDAO) {
		this.feNROABONHABILITADOSDao = pFeNROABONHABILITADOSDAO;
	}

}
