package arg.ros.acc.services.zonaoficina;

import java.util.*;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

import arg.ros.acc.vo.zonaoficina.FeTIPOSERVVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FeTIPOSERVService {

	/**
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener() throws ServiceException;

	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener(final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList obtener(final PaginatedList pPaginatedList) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList Buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @return
	 * @throws ServiceException
	 */
	int count() throws ServiceException;
	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int count(Filter pFilter) throws ServiceException;

	/**
	 * @param pFeTIPOSERVVO
	 * @throws ServiceException
	 */
	void agregar(final FeTIPOSERVVO pFeTIPOSERVVO) throws ServiceException;
		
	/**
	 * @param pFeTIPOSERVVO
	 * @throws ServiceException
	 */
	void eliminar(final FeTIPOSERVVO pFeTIPOSERVVO) throws ServiceException;

	/**
	 * @param pFeTIPOSERVVO
	 * @throws ServiceException
	 */
	void modificar(final FeTIPOSERVVO pFeTIPOSERVVO) throws ServiceException;
}
