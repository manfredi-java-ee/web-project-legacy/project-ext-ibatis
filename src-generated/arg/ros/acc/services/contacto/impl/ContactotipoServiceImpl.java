package arg.ros.acc.services.contacto.impl;

import java.util.*;

import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.filter.FilterFactory;
import arg.ros.acc.components.filter.FilterFactoryImpl;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.daos.contacto.ContactotipoDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.contacto.ContactotipoService;
import arg.ros.acc.vo.contacto.ContactotipoVO;

/**
 * @author Accenture Rosario
 *
 */
public class ContactotipoServiceImpl extends BaseService implements ContactotipoService {

	/**
	 * DAO para el acceso a la tabla CONTACTOTIPO.
	 */
	private ContactotipoDAO contactotipoDao;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List obtener() throws ServiceException {
		List results = new ArrayList();
		try {
			results = this.contactotipoDao.select();

		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECTALL, e);
			throw new ServiceException(e);
		}
		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	 @SuppressWarnings("unchecked")
	public List obtener(Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		List results = new ArrayList();
		try {
			results = this.contactotipoDao.select(filter);
			return results;
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public PaginatedList obtener(PaginatedList pPaginatedList) throws ServiceException {
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.contactotipoDao.select(start, limit));
			pPaginatedList.setFullListSize(this.contactotipoDao.count());
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECT, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.contactotipoDao.select(start, limit, filter));
			pPaginatedList.setFullListSize(this.contactotipoDao.count(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
		/**
	 * {@inheritDoc}
	 */
	public PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.contactotipoDao.buscar(start, limit, filter));
			pPaginatedList.setFullListSize(this.contactotipoDao.countSinFiltro(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.BUSCAR, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}

	/**
	 * {@inheritDoc}
	 */
	public int count() throws ServiceException {
		FilterFactory filterFactory = FilterFactoryImpl.getInstance();
		Filter filter = filterFactory.emptyFilter();
		
		return this.count(filter);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Filter pFilter) throws ServiceException {
		Map properties = pFilter.getProperties();
		int count = 0;
		try {
			count = this.contactotipoDao.count(properties);
			
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.COUNT, e);
			throw new ServiceException(e);
		}
		return count;
	}

	/**
	 * {@inheritDoc}
	 */
	public void agregar(final ContactotipoVO pContactotipoVO) throws ServiceException {
		try {
			this.contactotipoDao.insert(pContactotipoVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.INSERT, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void eliminar(final ContactotipoVO pContactotipoVO) throws ServiceException {
		try {
			this.contactotipoDao.delete(pContactotipoVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.DELETE, e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void modificar(final ContactotipoVO pContactotipoVO) throws ServiceException {
		try {
			this.contactotipoDao.update(pContactotipoVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.UPDATE, e);
			throw new ServiceException(e);
		}			
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setContactotipoDAO(ContactotipoDAO pContactotipoDAO) {
		this.contactotipoDao = pContactotipoDAO;
	}

}
