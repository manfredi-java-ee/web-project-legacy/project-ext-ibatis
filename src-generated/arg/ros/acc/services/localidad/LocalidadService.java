package arg.ros.acc.services.localidad;

import java.util.*;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

import arg.ros.acc.vo.localidad.LocalidadVO;

/**
 * @author Accenture Rosario
 *
 */
public interface LocalidadService {

	/**
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener() throws ServiceException;

	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener(final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList obtener(final PaginatedList pPaginatedList) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @return
	 * @throws ServiceException
	 */
	int count() throws ServiceException;
	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int count(Filter pFilter) throws ServiceException;

	/**
	 * @param pLocalidadVO
	 * @throws ServiceException
	 */
	void agregar(final LocalidadVO pLocalidadVO) throws ServiceException;
		
	/**
	 * @param pLocalidadVO
	 * @throws ServiceException
	 */
	void eliminar(final LocalidadVO pLocalidadVO) throws ServiceException;

	/**
	 * @param pLocalidadVO
	 * @throws ServiceException
	 */
	void modificar(final LocalidadVO pLocalidadVO) throws ServiceException;
}
