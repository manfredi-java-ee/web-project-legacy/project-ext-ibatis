package arg.ros.acc.services.reintegros.impl;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

 
import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.filter.FilterFactory;
import arg.ros.acc.components.filter.FilterFactoryImpl;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.daos.reintegros.SfaFisicoReintegrosDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.forms.reintegros.SfaFisicoReintegrosForm;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.reintegros.SfaFisicoReintegrosService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.vo.reintegros.SfaFisicoReintegrosVO;

/**
 * @author Accenture Rosario
 *
 */
public class SfaFisicoReintegrosServiceImpl extends BaseService implements SfaFisicoReintegrosService {

	/**
	 * DAO para el acceso a la tabla SFA_FISICO_REINTEGROS.
	 */
	private SfaFisicoReintegrosDAO sfaFisicoReintegrosDao;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List obtener() throws ServiceException {
		List results = new ArrayList();
		try {
			results = this.sfaFisicoReintegrosDao.select();

		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECTALL, e);
			throw new ServiceException(e);
		}
		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	 @SuppressWarnings("unchecked")
	public List obtener(Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		List results = new ArrayList();
		try {
			results = this.sfaFisicoReintegrosDao.select(filter);
			return results;
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public PaginatedList obtener(PaginatedList pPaginatedList) throws ServiceException {
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.sfaFisicoReintegrosDao.select(start, limit));
			pPaginatedList.setFullListSize(this.sfaFisicoReintegrosDao.count());
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECT, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.sfaFisicoReintegrosDao.select(start, limit, filter));
			pPaginatedList.setFullListSize(this.sfaFisicoReintegrosDao.count(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
		/**
	 * {@inheritDoc}
	 */
	public PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.sfaFisicoReintegrosDao.buscar(start, limit, filter));
			pPaginatedList.setFullListSize(this.sfaFisicoReintegrosDao.countSinFiltro(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.BUSCAR, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}

	/**
	 * {@inheritDoc}
	 */
	public int count() throws ServiceException {
		FilterFactory filterFactory = FilterFactoryImpl.getInstance();
		Filter filter = filterFactory.emptyFilter();
		
		return this.count(filter);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Filter pFilter) throws ServiceException {
		Map properties = pFilter.getProperties();
		int count = 0;
		try {
			count = this.sfaFisicoReintegrosDao.count(properties);
			
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.COUNT, e);
			throw new ServiceException(e);
		}
		return count;
	}

	/**
	 * {@inheritDoc}
	 */
	public void agregar(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException {
		try {
			this.sfaFisicoReintegrosDao.insert(pSfaFisicoReintegrosVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.INSERT, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void eliminar(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException {
		try {
			this.sfaFisicoReintegrosDao.delete(pSfaFisicoReintegrosVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.DELETE, e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void modificar(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException {
		try {
			this.sfaFisicoReintegrosDao.update(pSfaFisicoReintegrosVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.UPDATE, e);
			throw new ServiceException(e);
		}			
	}
	
	
	

	/**
	 * {@inheritDoc}
	 */
	public void PrescribirReintegro(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException {
		try {
			this.sfaFisicoReintegrosDao.PrescribirReintegro(pSfaFisicoReintegrosVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.UPDATE, e);
			throw new ServiceException(e);
		}			
	}
	
	

	

	/**
	 * {@inheritDoc}
	 */
	public final void setSfaFisicoReintegrosDAO(SfaFisicoReintegrosDAO pSfaFisicoReintegrosDAO) {
		this.sfaFisicoReintegrosDao = pSfaFisicoReintegrosDAO;
	}

}
