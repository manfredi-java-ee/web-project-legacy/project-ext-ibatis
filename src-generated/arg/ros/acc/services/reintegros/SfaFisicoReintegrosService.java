package arg.ros.acc.services.reintegros;

import java.util.*;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.forms.reintegros.SfaFisicoReintegrosForm;
//import java.lang.Object;
import arg.ros.acc.vo.reintegros.SfaFisicoReintegrosVO;

/**
 * @author Accenture Rosario
 *
 */
public interface SfaFisicoReintegrosService {

	/**
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener() throws ServiceException;

	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener(final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList obtener(final PaginatedList pPaginatedList) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @return
	 * @throws ServiceException
	 */
	int count() throws ServiceException;
	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int count(Filter pFilter) throws ServiceException;

	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws ServiceException
	 */
	void agregar(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException;
		
	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws ServiceException
	 */
	void eliminar(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException;

	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws ServiceException
	 */
	void modificar(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException;
	
	
	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws ServiceException
	 */ 
	// AGREGADO METODO BUSCAR_SFAFISICO_REINTEGRO
	
	void PrescribirReintegro(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws ServiceException;	                              
	
}
