package arg.ros.acc.services.feriado;

import java.util.*;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

import arg.ros.acc.vo.feriado.FeriadoDetalleVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FeriadoDetalleService {

	/**
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener() throws ServiceException;

	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener(final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList obtener(final PaginatedList pPaginatedList) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @return
	 * @throws ServiceException
	 */
	int count() throws ServiceException;
	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int count(Filter pFilter) throws ServiceException;

	/**
	 * @param pFeriadoDetalleVO
	 * @throws ServiceException
	 */
	void agregar(final FeriadoDetalleVO pFeriadoDetalleVO) throws ServiceException;
		
	/**
	 * @param pFeriadoDetalleVO
	 * @throws ServiceException
	 */
	void eliminar(final FeriadoDetalleVO pFeriadoDetalleVO) throws ServiceException;

	/**
	 * @param pFeriadoDetalleVO
	 * @throws ServiceException
	 */
	void modificar(final FeriadoDetalleVO pFeriadoDetalleVO) throws ServiceException;
}
