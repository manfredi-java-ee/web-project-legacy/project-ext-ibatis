package arg.ros.acc.services.provincia;

import java.util.*;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

import arg.ros.acc.vo.provincia.ProvinciaVO;

/**
 * @author Accenture Rosario
 *
 */
public interface ProvinciaService {

	/**
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener() throws ServiceException;

	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener(final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList obtener(final PaginatedList pPaginatedList) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @return
	 * @throws ServiceException
	 */
	int count() throws ServiceException;
	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int count(Filter pFilter) throws ServiceException;

	/**
	 * @param pProvinciaVO
	 * @throws ServiceException
	 */
	void agregar(final ProvinciaVO pProvinciaVO) throws ServiceException;
		
	/**
	 * @param pProvinciaVO
	 * @throws ServiceException
	 */
	void eliminar(final ProvinciaVO pProvinciaVO) throws ServiceException;

	/**
	 * @param pProvinciaVO
	 * @throws ServiceException
	 */
	void modificar(final ProvinciaVO pProvinciaVO) throws ServiceException;
}
