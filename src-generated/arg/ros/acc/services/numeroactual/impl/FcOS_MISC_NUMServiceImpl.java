package arg.ros.acc.services.numeroactual.impl;

import java.util.*;

import arg.ros.acc.commons.logs.LoggerWeb;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.filter.FilterFactory;
import arg.ros.acc.components.filter.FilterFactoryImpl;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.daos.numeroactual.FcOS_MISC_NUMDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.exceptions.ServiceException;
import arg.ros.acc.services.commons.BaseService;
import arg.ros.acc.services.numeroactual.FcOS_MISC_NUMService;
import arg.ros.acc.vo.numeroactual.FcOS_MISC_NUMVO;

/**
 * @author Accenture Rosario
 *
 */
public class FcOS_MISC_NUMServiceImpl extends BaseService implements FcOS_MISC_NUMService {

	/**
	 * DAO para el acceso a la tabla FCOS_MISC_NUM.
	 */
	private FcOS_MISC_NUMDAO fcOS_MISC_NUMDao;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public List obtener() throws ServiceException {
		List results = new ArrayList();
		try {
			results = this.fcOS_MISC_NUMDao.select();

		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECTALL, e);
			throw new ServiceException(e);
		}
		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	 @SuppressWarnings("unchecked")
	public List obtener(Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		List results = new ArrayList();
		try {
			results = this.fcOS_MISC_NUMDao.select(filter);
			return results;
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public PaginatedList obtener(PaginatedList pPaginatedList) throws ServiceException {
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.fcOS_MISC_NUMDao.select(start, limit));
			pPaginatedList.setFullListSize(this.fcOS_MISC_NUMDao.count());
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.SELECT, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.fcOS_MISC_NUMDao.select(start, limit, filter));
			pPaginatedList.setFullListSize(this.fcOS_MISC_NUMDao.count(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException {
		final Map filter = pFilter.getProperties();
		final int start = pPaginatedList.getPageNumber();
		final int limit = pPaginatedList.getObjectsPerPage();

		try {
			pPaginatedList.setData(this.fcOS_MISC_NUMDao.buscar(start, limit, filter));
			pPaginatedList.setFullListSize(this.fcOS_MISC_NUMDao.countSinFiltro(filter));		
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.FILTER, e);
			throw new ServiceException(e);
		}
		return pPaginatedList;
	}

	/**
	 * {@inheritDoc}
	 */
	public int count() throws ServiceException {
		FilterFactory filterFactory = FilterFactoryImpl.getInstance();
		Filter filter = filterFactory.emptyFilter();
		
		return this.count(filter);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Filter pFilter) throws ServiceException {
		Map properties = pFilter.getProperties();
		int count = 0;
		try {
			count = this.fcOS_MISC_NUMDao.count(properties);
			
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.COUNT, e);
			throw new ServiceException(e);
		}
		return count;
	}
	
 
	
	/**
	 * {@inheritDoc}
	 */
	public int countSinFiltro(final Filter pFilter) throws ServiceException {
		Map properties = pFilter.getProperties();
		int count = 0;
		try {
			count = this.fcOS_MISC_NUMDao.countSinFiltro(properties);
			
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.COUNT, e);
			throw new ServiceException(e);
		}
		return count;
	}

	/**
	 * {@inheritDoc}
	 */
	public void agregar(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws ServiceException {
		try {
			this.fcOS_MISC_NUMDao.insert(pFcOS_MISC_NUMVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.INSERT, e);
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void eliminar(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws ServiceException {
		try {
			this.fcOS_MISC_NUMDao.delete(pFcOS_MISC_NUMVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.DELETE, e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void modificar(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws ServiceException {
		try {
			this.fcOS_MISC_NUMDao.update(pFcOS_MISC_NUMVO);
		} catch (DAOException e) {
			getLogger().ErrorBBDD(this, LoggerWeb.OPERATIONS.UPDATE, e);
			throw new ServiceException(e);
		}			
	}

	/**
	 * {@inheritDoc}
	 */
	public final void setFcOS_MISC_NUMDAO(FcOS_MISC_NUMDAO pFcOS_MISC_NUMDAO) {
		this.fcOS_MISC_NUMDao = pFcOS_MISC_NUMDAO;
	}

}
