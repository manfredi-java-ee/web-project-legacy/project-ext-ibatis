package arg.ros.acc.services.numeroactual;

import java.util.*;

import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.exceptions.ServiceException;

import arg.ros.acc.vo.numeroactual.FcOS_MISC_NUMVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FcOS_MISC_NUMService {

	/**
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener() throws ServiceException;

	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	List obtener(final Filter pFilter) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList obtener(final PaginatedList pPaginatedList) throws ServiceException;
	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList filtrar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;

	
	/**
	 * @param pPaginatedList
	 * @param pFilter
	 * @throws ServiceException
	 * @return
	 */
	PaginatedList buscar(final PaginatedList pPaginatedList, final Filter pFilter) throws ServiceException;
	
	/**
	 * @return
	 * @throws ServiceException
	 */
	int count() throws ServiceException;
	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int count(Filter pFilter) throws ServiceException;

	
	/**
	 * @param pFilter
	 * @return
	 * @throws ServiceException
	 */
	int countSinFiltro(Filter pFilter) throws ServiceException;
	/**
	 * @param pFcOS_MISC_NUMVO
	 * @throws ServiceException
	 */
	void agregar(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws ServiceException;
		
	/**
	 * @param pFcOS_MISC_NUMVO
	 * @throws ServiceException
	 */
	void eliminar(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws ServiceException;

	/**
	 * @param pFcOS_MISC_NUMVO
	 * @throws ServiceException
	 */
	void modificar(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws ServiceException;
}
