package arg.ros.acc.actions.numeroactual;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.actions.commons.BaseAction;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.numeroactual.FcOS_MISC_NUMForm;
import arg.ros.acc.services.numeroactual.FcOS_MISC_NUMService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;
import arg.ros.acc.vo.commons.UsuarioVO;
import arg.ros.acc.vo.numeroactual.FcOS_MISC_NUMVO;

/**
 * @author Accenture Rosario
 *
 */
public class FcOS_MISC_NUMAction extends BaseAction {
	
	/**
	 * Forward a la JSP que muestra la pantalla inicial de FcOS_MISC_NUM. 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private FcOS_MISC_NUMService fcOS_MISC_NUMService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(FcOS_MISC_NUMAction.FWD_JSP_VIEW);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward altaFcOS_MISC_NUM(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FcOS_MISC_NUMForm fcOS_MISC_NUMForm = (FcOS_MISC_NUMForm) form;
		final FcOS_MISC_NUMVO vo = fcOS_MISC_NUMForm.getVO();
		
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.fcOS_MISC_NUMService.agregar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);	
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward bajaFcOS_MISC_NUM(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FcOS_MISC_NUMForm fcOS_MISC_NUMForm = (FcOS_MISC_NUMForm) form;
		final FcOS_MISC_NUMVO vo = fcOS_MISC_NUMForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.fcOS_MISC_NUMService.eliminar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward modificarFcOS_MISC_NUM(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FcOS_MISC_NUMForm fcOS_MISC_NUMForm = (FcOS_MISC_NUMForm) form;
		final FcOS_MISC_NUMVO vo = fcOS_MISC_NUMForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.fcOS_MISC_NUMService.modificar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.fcOS_MISC_NUMService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward buscar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.fcOS_MISC_NUMService.buscar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.fcOS_MISC_NUMService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pFcOS_MISC_NUMService the fcOS_MISC_NUMService to set
	 */
	public final void setFcOS_MISC_NUMService(final FcOS_MISC_NUMService pFcOS_MISC_NUMService) {
		this.fcOS_MISC_NUMService = pFcOS_MISC_NUMService;
	}

}
