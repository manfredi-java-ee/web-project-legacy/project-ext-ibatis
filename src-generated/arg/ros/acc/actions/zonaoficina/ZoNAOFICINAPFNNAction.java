package arg.ros.acc.actions.zonaoficina;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.actions.commons.BaseAction;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.zonaoficina.ZoNAOFICINAPFNNForm;
import arg.ros.acc.services.zonaoficina.ZoNAOFICINAPFNNService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;
import arg.ros.acc.vo.commons.UsuarioVO;
import arg.ros.acc.vo.zonaoficina.ZoNAOFICINAPFNNVO;

/**
 * @author Accenture Rosario
 *
 */
public class ZoNAOFICINAPFNNAction extends BaseAction {
	
	/**
	 * Forward a la JSP que muestra la pantalla inicial de ZoNAOFICINAPFNN. 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private ZoNAOFICINAPFNNService zoNAOFICINAPFNNService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(ZoNAOFICINAPFNNAction.FWD_JSP_VIEW);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward altaZoNAOFICINAPFNN(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final ZoNAOFICINAPFNNForm zoNAOFICINAPFNNForm = (ZoNAOFICINAPFNNForm) form;
		final ZoNAOFICINAPFNNVO vo = zoNAOFICINAPFNNForm.getVO();
		
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.zoNAOFICINAPFNNService.agregar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);	
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward bajaZoNAOFICINAPFNN(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final ZoNAOFICINAPFNNForm zoNAOFICINAPFNNForm = (ZoNAOFICINAPFNNForm) form;
		final ZoNAOFICINAPFNNVO vo = zoNAOFICINAPFNNForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.zoNAOFICINAPFNNService.eliminar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward modificarZoNAOFICINAPFNN(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final ZoNAOFICINAPFNNForm zoNAOFICINAPFNNForm = (ZoNAOFICINAPFNNForm) form;
		final ZoNAOFICINAPFNNVO vo = zoNAOFICINAPFNNForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.zoNAOFICINAPFNNService.modificar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.zoNAOFICINAPFNNService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward buscar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);

		
		paginatedList= this.zoNAOFICINAPFNNService.buscar(paginatedList, filter);
		
	
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
		
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.zoNAOFICINAPFNNService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pZoNAOFICINAPFNNService the zoNAOFICINAPFNNService to set
	 */
	public final void setZoNAOFICINAPFNNService(final ZoNAOFICINAPFNNService pZoNAOFICINAPFNNService) {
		this.zoNAOFICINAPFNNService = pZoNAOFICINAPFNNService;
	}

}
