package arg.ros.acc.actions.zonaoficina;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.actions.commons.BaseAction;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.zonaoficina.FeTIPOSERVForm;
import arg.ros.acc.services.zonaoficina.FeTIPOSERVService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;
import arg.ros.acc.vo.commons.UsuarioVO;
import arg.ros.acc.vo.zonaoficina.FeTIPOSERVVO;

/**
 * @author Accenture Rosario
 *
 */
public class FeTIPOSERVAction extends BaseAction {
	
	/**
	 * Forward a la JSP que muestra la pantalla inicial de FeTIPOSERV. 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private FeTIPOSERVService feTIPOSERVService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(FeTIPOSERVAction.FWD_JSP_VIEW);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward altaFeTIPOSERV(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FeTIPOSERVForm feTIPOSERVForm = (FeTIPOSERVForm) form;
		final FeTIPOSERVVO vo = feTIPOSERVForm.getVO();
		
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.feTIPOSERVService.agregar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);	
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward bajaFeTIPOSERV(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FeTIPOSERVForm feTIPOSERVForm = (FeTIPOSERVForm) form;
		final FeTIPOSERVVO vo = feTIPOSERVForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.feTIPOSERVService.eliminar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward modificarFeTIPOSERV(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FeTIPOSERVForm feTIPOSERVForm = (FeTIPOSERVForm) form;
		final FeTIPOSERVVO vo = feTIPOSERVForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.feTIPOSERVService.modificar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.feTIPOSERVService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward buscar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.feTIPOSERVService.Buscar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.feTIPOSERVService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pFeTIPOSERVService the feTIPOSERVService to set
	 */
	public final void setFeTIPOSERVService(final FeTIPOSERVService pFeTIPOSERVService) {
		this.feTIPOSERVService = pFeTIPOSERVService;
	}

}
