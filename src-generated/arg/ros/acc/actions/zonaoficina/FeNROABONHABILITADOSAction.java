package arg.ros.acc.actions.zonaoficina;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.actions.commons.BaseAction;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.zonaoficina.FeNROABONHABILITADOSForm;
import arg.ros.acc.services.zonaoficina.FeNROABONHABILITADOSService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;
import arg.ros.acc.vo.commons.UsuarioVO;
import arg.ros.acc.vo.zonaoficina.FeNROABONHABILITADOSVO;

/**
 * @author Accenture Rosario
 *
 */
public class FeNROABONHABILITADOSAction extends BaseAction {
	
	/**
	 * Forward a la JSP que muestra la pantalla inicial de FeNROABONHABILITADOS. 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private FeNROABONHABILITADOSService feNROABONHABILITADOSService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(FeNROABONHABILITADOSAction.FWD_JSP_VIEW);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward altaFeNROABONHABILITADOS(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FeNROABONHABILITADOSForm feNROABONHABILITADOSForm = (FeNROABONHABILITADOSForm) form;
		final FeNROABONHABILITADOSVO vo = feNROABONHABILITADOSForm.getVO();
		
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.feNROABONHABILITADOSService.agregar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);	
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward bajaFeNROABONHABILITADOS(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FeNROABONHABILITADOSForm feNROABONHABILITADOSForm = (FeNROABONHABILITADOSForm) form;
		final FeNROABONHABILITADOSVO vo = feNROABONHABILITADOSForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.feNROABONHABILITADOSService.eliminar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward modificarFeNROABONHABILITADOS(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final FeNROABONHABILITADOSForm feNROABONHABILITADOSForm = (FeNROABONHABILITADOSForm) form;
		final FeNROABONHABILITADOSVO vo = feNROABONHABILITADOSForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.feNROABONHABILITADOSService.modificar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward ObtenerNumerosHabilitados(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		/*Al quitar lo filtros del formulario solo toma por ISN */
		filter.getProperties().remove("dps_pe_seq");
		filter.getProperties().remove("fe_nro_habil_desde");
		filter.getProperties().remove("fe_nro_habil_hasta");

		
		paginatedList = this.feNROABONHABILITADOSService.ObtenerNrosHabilitados(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
		
	}
	
	
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.feNROABONHABILITADOSService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	
	

	
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.feNROABONHABILITADOSService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pFeNROABONHABILITADOSService the feNROABONHABILITADOSService to set
	 */
	public final void setFeNROABONHABILITADOSService(final FeNROABONHABILITADOSService pFeNROABONHABILITADOSService) {
		this.feNROABONHABILITADOSService = pFeNROABONHABILITADOSService;
	}

}
