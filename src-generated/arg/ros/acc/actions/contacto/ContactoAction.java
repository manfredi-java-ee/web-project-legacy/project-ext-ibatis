package arg.ros.acc.actions.contacto;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.actions.commons.BaseAction;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.contacto.ContactoForm;
import arg.ros.acc.services.contacto.ContactoService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;
import arg.ros.acc.vo.commons.UsuarioVO;
import arg.ros.acc.vo.contacto.ContactoVO;

/**
 * @author Accenture Rosario
 *
 */
public class ContactoAction extends BaseAction {
	
	/**
	 * Forward a la JSP que muestra la pantalla inicial de Contacto. 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private ContactoService contactoService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(ContactoAction.FWD_JSP_VIEW);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward altaContacto(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final ContactoForm contactoForm = (ContactoForm) form;
		final ContactoVO vo = contactoForm.getVO();
		
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.contactoService.agregar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);	
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward bajaContacto(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final ContactoForm contactoForm = (ContactoForm) form;
		final ContactoVO vo = contactoForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.contactoService.eliminar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward modificarContacto(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final ContactoForm contactoForm = (ContactoForm) form;
		final ContactoVO vo = contactoForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.contactoService.modificar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.contactoService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
		/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward buscar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.contactoService.buscar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	
	
	
	
	
	
	
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.contactoService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pContactoService the contactoService to set
	 */
	public final void setContactoService(final ContactoService pContactoService) {
		this.contactoService = pContactoService;
	}

}
