package arg.ros.acc.actions.reintegros;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.actions.commons.BaseAction;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.reintegros.SfaMMiscelaneosFisicoForm;
import arg.ros.acc.services.reintegros.SfaMMiscelaneosFisicoService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;
import arg.ros.acc.vo.commons.UsuarioVO;
import arg.ros.acc.vo.reintegros.SfaMMiscelaneosFisicoVO;

/**
 * @author Accenture Rosario
 *
 */
public class SfaMMiscelaneosFisicoAction extends BaseAction {
	
	/**
	 * Forward a la JSP que muestra la pantalla inicial de SfaMMiscelaneosFisico. 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private SfaMMiscelaneosFisicoService sfaMMiscelaneosFisicoService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(SfaMMiscelaneosFisicoAction.FWD_JSP_VIEW);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward altaSfaMMiscelaneosFisico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final SfaMMiscelaneosFisicoForm sfaMMiscelaneosFisicoForm = (SfaMMiscelaneosFisicoForm) form;
		final SfaMMiscelaneosFisicoVO vo = sfaMMiscelaneosFisicoForm.getVO();
		
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.sfaMMiscelaneosFisicoService.agregar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);	
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward bajaSfaMMiscelaneosFisico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final SfaMMiscelaneosFisicoForm sfaMMiscelaneosFisicoForm = (SfaMMiscelaneosFisicoForm) form;
		final SfaMMiscelaneosFisicoVO vo = sfaMMiscelaneosFisicoForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.sfaMMiscelaneosFisicoService.eliminar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward modificarSfaMMiscelaneosFisico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final SfaMMiscelaneosFisicoForm sfaMMiscelaneosFisicoForm = (SfaMMiscelaneosFisicoForm) form;
		final SfaMMiscelaneosFisicoVO vo = sfaMMiscelaneosFisicoForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.sfaMMiscelaneosFisicoService.modificar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.sfaMMiscelaneosFisicoService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
		/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward buscar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.sfaMMiscelaneosFisicoService.buscar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.sfaMMiscelaneosFisicoService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pSfaMMiscelaneosFisicoService the sfaMMiscelaneosFisicoService to set
	 */
	public final void setSfaMMiscelaneosFisicoService(final SfaMMiscelaneosFisicoService pSfaMMiscelaneosFisicoService) {
		this.sfaMMiscelaneosFisicoService = pSfaMMiscelaneosFisicoService;
	}

}
