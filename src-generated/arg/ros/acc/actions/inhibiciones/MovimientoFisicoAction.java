package arg.ros.acc.actions.inhibiciones;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import arg.ros.acc.actions.commons.BaseAction;
import arg.ros.acc.components.filter.Filter;
import arg.ros.acc.components.paging.PaginatedList;
import arg.ros.acc.forms.inhibiciones.MovimientoFisicoForm;
import arg.ros.acc.services.inhibiciones.MovimientoFisicoService;
import arg.ros.acc.struts.JsonActionForward;
import arg.ros.acc.struts.JsonActionResponse;
import arg.ros.acc.struts.JsonListResponse;
import arg.ros.acc.vo.commons.UsuarioVO;
import arg.ros.acc.vo.inhibiciones.MovimientoFisicoVO;
import arg.ros.acc.vo.zonaoficina.ZoNAOFICINAPFNNVO;

/**
 * @author Accenture Rosario
 *
 */
public class MovimientoFisicoAction extends BaseAction {
	
	/**
	 * Forward a la JSP que muestra la pantalla inicial de MovimientoFisico. 
	 */
	private static final String FWD_JSP_VIEW = "view";
	
	/**
	 * 
	 */
	private MovimientoFisicoService movimientoFisicoService;

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return mapping.findForward(MovimientoFisicoAction.FWD_JSP_VIEW);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward altaMovimientoFisico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final MovimientoFisicoForm movimientoFisicoForm = (MovimientoFisicoForm) form;
		final MovimientoFisicoVO vo = movimientoFisicoForm.getVO();
		
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.movimientoFisicoService.agregar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);	
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward bajaMovimientoFisico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final MovimientoFisicoForm movimientoFisicoForm = (MovimientoFisicoForm) form;
		final MovimientoFisicoVO vo = movimientoFisicoForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.movimientoFisicoService.eliminar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward modificarMovimientoFisico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		final MovimientoFisicoForm movimientoFisicoForm = (MovimientoFisicoForm) form;
		final MovimientoFisicoVO vo = movimientoFisicoForm.getVO();
		JsonActionResponse jsonActionResponse = null;
		
		try {
			this.movimientoFisicoService.modificar(vo);
			jsonActionResponse = new JsonActionResponse(vo);
			
		} catch (Exception e) {
			jsonActionResponse = new JsonActionResponse(vo, e.getMessage());
		}
		
		return new JsonActionForward(jsonActionResponse);
	}

	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward filtrar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.movimientoFisicoService.filtrar(paginatedList, filter);
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward buscar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		Filter filter = getFilter(request);
		
		paginatedList = this.movimientoFisicoService.buscar(paginatedList, filter);

				
		
		
//		List<MovimientoFisicoVO> lista ;
//		
//		Object ArrayList =paginatedList.getData();
//		
//		lista = (List<MovimientoFisicoVO>) ArrayList;
//		
//		
//		for(MovimientoFisicoVO vo : lista)
//		{
//		 System.out.println("objeto: "+vo);
//		
//		}
//		
//		paginatedList.setData(lista);
		
		
		
		
		
        return new JsonActionForward(new arg.ros.acc.struts.JsonFilterResponse(paginatedList));
	}
	
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		PaginatedList paginatedList = getPaginatedList(request);
		paginatedList = this.movimientoFisicoService.obtener(paginatedList);
		
		return new JsonActionForward(new JsonListResponse(paginatedList));
	}
	
	/**
	 * @param pMovimientoFisicoService the movimientoFisicoService to set
	 */
	public final void setMovimientoFisicoService(final MovimientoFisicoService pMovimientoFisicoService) {
		this.movimientoFisicoService = pMovimientoFisicoService;
	}

}
