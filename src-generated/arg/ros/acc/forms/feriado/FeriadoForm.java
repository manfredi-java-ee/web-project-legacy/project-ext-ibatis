package arg.ros.acc.forms.feriado;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.feriado.FeriadoVO;

/**
* FeriadoForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeriadoForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String idferiado;
	private String anio;

	public FeriadoVO getVO() {
		FeriadoVO vo = new FeriadoVO();
		
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Idferiado
	* @return el valor de Idferiado
	*/ 
	public String getIdferiado() {
		return idferiado;
	}
    	
	/**
	* Fija el valor del atributo Idferiado
	* @param valor el valor de Idferiado
	*/ 
	public void setIdferiado(String valor) {
		this.idferiado = valor;
	}
	/**
	* Recupera el valor del atributo Anio
	* @return el valor de Anio
	*/ 
	public String getAnio() {
		return anio;
	}
    	
	/**
	* Fija el valor del atributo Anio
	* @param valor el valor de Anio
	*/ 
	public void setAnio(String valor) {
		this.anio = valor;
	}
}
