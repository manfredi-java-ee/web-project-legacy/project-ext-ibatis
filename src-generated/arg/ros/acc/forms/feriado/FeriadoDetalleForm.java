package arg.ros.acc.forms.feriado;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.feriado.FeriadoDetalleVO;

/**
* FeriadoDetalleForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeriadoDetalleForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String idferiadodetalle;
	private String idferiado;
	private String descripcion;
	private String fecha;

	public FeriadoDetalleVO getVO() {
		FeriadoDetalleVO vo = new FeriadoDetalleVO();
		
		vo.setIdferiadodetalle(Double.valueOf(this.idferiadodetalle));
		vo.setDescripcion(this.descripcion);
		vo.setFecha(this.getAsDate(this.fecha));
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Idferiadodetalle
	* @return el valor de Idferiadodetalle
	*/ 
	public String getIdferiadodetalle() {
		return idferiadodetalle;
	}
    	
	/**
	* Fija el valor del atributo Idferiadodetalle
	* @param valor el valor de Idferiadodetalle
	*/ 
	public void setIdferiadodetalle(String valor) {
		this.idferiadodetalle = valor;
	}
	/**
	* Recupera el valor del atributo Idferiado
	* @return el valor de Idferiado
	*/ 
	public String getIdferiado() {
		return idferiado;
	}
    	
	/**
	* Fija el valor del atributo Idferiado
	* @param valor el valor de Idferiado
	*/ 
	public void setIdferiado(String valor) {
		this.idferiado = valor;
	}
	/**
	* Recupera el valor del atributo Descripcion
	* @return el valor de Descripcion
	*/ 
	public String getDescripcion() {
		return descripcion;
	}
    	
	/**
	* Fija el valor del atributo Descripcion
	* @param valor el valor de Descripcion
	*/ 
	public void setDescripcion(String valor) {
		this.descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Fecha
	* @return el valor de Fecha
	*/ 
	public String getFecha() {
		return fecha;
	}
    	
	/**
	* Fija el valor del atributo Fecha
	* @param valor el valor de Fecha
	*/ 
	public void setFecha(String valor) {
		this.fecha = valor;
	}
}
