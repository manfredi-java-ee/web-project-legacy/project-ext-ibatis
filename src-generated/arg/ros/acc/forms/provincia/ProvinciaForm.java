package arg.ros.acc.forms.provincia;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.provincia.ProvinciaVO;

/**
* ProvinciaForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ProvinciaForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String idprovincia;
	private String nombre;
	private String idpais;

	public ProvinciaVO getVO() {
		ProvinciaVO vo = new ProvinciaVO();
		
		vo.setNombre(this.nombre);
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Idprovincia
	* @return el valor de Idprovincia
	*/ 
	public String getIdprovincia() {
		return idprovincia;
	}
    	
	/**
	* Fija el valor del atributo Idprovincia
	* @param valor el valor de Idprovincia
	*/ 
	public void setIdprovincia(String valor) {
		this.idprovincia = valor;
	}
	/**
	* Recupera el valor del atributo Nombre
	* @return el valor de Nombre
	*/ 
	public String getNombre() {
		return nombre;
	}
    	
	/**
	* Fija el valor del atributo Nombre
	* @param valor el valor de Nombre
	*/ 
	public void setNombre(String valor) {
		this.nombre = valor;
	}
	/**
	* Recupera el valor del atributo Idpais
	* @return el valor de Idpais
	*/ 
	public String getIdpais() {
		return idpais;
	}
    	
	/**
	* Fija el valor del atributo Idpais
	* @param valor el valor de Idpais
	*/ 
	public void setIdpais(String valor) {
		this.idpais = valor;
	}
}
