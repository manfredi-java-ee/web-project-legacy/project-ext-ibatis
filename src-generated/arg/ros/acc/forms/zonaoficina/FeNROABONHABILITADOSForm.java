package arg.ros.acc.forms.zonaoficina;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.zonaoficina.FeNROABONHABILITADOSVO;

/**
* FeNROABONHABILITADOSForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeNROABONHABILITADOSForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String adabas_isn;
	private String dps_pe_seq;
	private String fe_nro_habil_hasta;
	private String fe_nro_habil_desde;

	public FeNROABONHABILITADOSVO getVO() {
		FeNROABONHABILITADOSVO vo = new FeNROABONHABILITADOSVO();
		
		vo.setAdabas_isn(Double.valueOf(this.adabas_isn));
		vo.setDps_pe_seq(this.dps_pe_seq);
		vo.setFe_nro_habil_hasta(Double.valueOf(this.fe_nro_habil_hasta));
		vo.setFe_nro_habil_desde(Double.valueOf(this.fe_nro_habil_desde));
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(String valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Dps_pe_seq
	* @return el valor de Dps_pe_seq
	*/ 
	public String getDps_pe_seq() {
		return dps_pe_seq;
	}
    	
	/**
	* Fija el valor del atributo Dps_pe_seq
	* @param valor el valor de Dps_pe_seq
	*/ 
	public void setDps_pe_seq(String valor) {
		this.dps_pe_seq = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_habil_hasta
	* @return el valor de Fe_nro_habil_hasta
	*/ 
	public String getFe_nro_habil_hasta() {
		return fe_nro_habil_hasta;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_habil_hasta
	* @param valor el valor de Fe_nro_habil_hasta
	*/ 
	public void setFe_nro_habil_hasta(String valor) {
		this.fe_nro_habil_hasta = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_habil_desde
	* @return el valor de Fe_nro_habil_desde
	*/ 
	public String getFe_nro_habil_desde() {
		return fe_nro_habil_desde;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_habil_desde
	* @param valor el valor de Fe_nro_habil_desde
	*/ 
	public void setFe_nro_habil_desde(String valor) {
		this.fe_nro_habil_desde = valor;
	}
}
