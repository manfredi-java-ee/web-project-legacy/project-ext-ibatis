package arg.ros.acc.forms.zonaoficina;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.zonaoficina.FeTIPOSERVVO;

/**
* FeTIPOSERVForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FeTIPOSERVForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String adabas_isn;
	private String dps_mu_seq;
	private String fe_tipo_serv;

	public FeTIPOSERVVO getVO() {
		FeTIPOSERVVO vo = new FeTIPOSERVVO();
		
		vo.setAdabas_isn(Double.valueOf(this.adabas_isn));
		vo.setDps_mu_seq(this.dps_mu_seq);
		vo.setFe_tipo_serv(Double.valueOf(this.fe_tipo_serv));
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(String valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Dps_mu_seq
	* @return el valor de Dps_mu_seq
	*/ 
	public String getDps_mu_seq() {
		return dps_mu_seq;
	}
    	
	/**
	* Fija el valor del atributo Dps_mu_seq
	* @param valor el valor de Dps_mu_seq
	*/ 
	public void setDps_mu_seq(String valor) {
		this.dps_mu_seq = valor;
	}
	/**
	* Recupera el valor del atributo Fe_tipo_serv
	* @return el valor de Fe_tipo_serv
	*/ 
	public String getFe_tipo_serv() {
		return fe_tipo_serv;
	}
    	
	/**
	* Fija el valor del atributo Fe_tipo_serv
	* @param valor el valor de Fe_tipo_serv
	*/ 
	public void setFe_tipo_serv(String valor) {
		this.fe_tipo_serv = valor;
	}
}
