package arg.ros.acc.forms.zonaoficina;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.zonaoficina.ZoNAOFICINAPFNNVO;

/**
* ZoNAOFICINAPFNNForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ZoNAOFICINAPFNNForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String adabas_isn;
	private String fe_grupo_medido;
	private String fe_descripcion;
	private String fe_zona_ofic_pla;
	private String fe_cf_canal_dd;
	private String fe_long_abo;
	private String fe_oficina_baja_tf;
	private String fe_ciclo_sigeco;
	private String fe_provincia;
	private String fe_zona;
	private String fe_zona_baja;
	private String fe_zona_baja_tf;
	private String fe_marca_cdn;
	private String fe_cod_interurbano;
	private String fe_marca_sigeco;
	private String fe_nro_area;
	private String fe_long_abo_tec;
	private String fe_cf_canal_hh24;
	private String fe_fecha_vigencia;
	private String fe_zona_uo;
	private String fe_recolector_info;
	private String fe_adherido_843;
	private String fe_fundamental_baja;
	private String fe_cod_estado_zo_ofi;
	private String fe_oficina;
	private String fe_region;
	private String fe_cod_vto_proc;
	private String fe_territorio;
	private String fe_marca_ddi;
	private String fe_tecnologia;
	private String fe_oficina_baja;
	private String fe_cod_empresa;
	private String fe_unidad_central;
	private String fe_cod_muni;
	private String fe_fecha_carga;
	private String fe_nro_fundamental;
	private String fe_usuario;
	private String fe_cod_vto_real;
	private String ZONAVAR;
	private String OFICINAVAR;
	private String LEYENDA;
	private Double tipoServicio;
	
	public ZoNAOFICINAPFNNVO getVO() {
		ZoNAOFICINAPFNNVO vo = new ZoNAOFICINAPFNNVO();
		
		vo.setAdabas_isn(Double.valueOf(this.adabas_isn));
		vo.setFe_grupo_medido(Double.valueOf(this.fe_grupo_medido));
		vo.setFe_descripcion(this.fe_descripcion);
		vo.setFe_zona_ofic_pla(Double.valueOf(this.fe_zona_ofic_pla));
		vo.setFe_cf_canal_dd(Double.valueOf(this.fe_cf_canal_dd));
		vo.setFe_long_abo(Double.valueOf(this.fe_long_abo));
		vo.setFe_oficina_baja_tf(Double.valueOf(this.fe_oficina_baja_tf));
		vo.setFe_ciclo_sigeco(this.fe_ciclo_sigeco);
		vo.setFe_provincia(Double.valueOf(this.fe_provincia));
		vo.setFe_zona(Double.valueOf(this.fe_zona));
		vo.setFe_zona_baja(Double.valueOf(this.fe_zona_baja));
		vo.setFe_zona_baja_tf(Double.valueOf(this.fe_zona_baja_tf));
		vo.setFe_marca_cdn(this.fe_marca_cdn);
		vo.setFe_cod_interurbano(Double.valueOf(this.fe_cod_interurbano));
		vo.setFe_marca_sigeco(this.fe_marca_sigeco);
		vo.setFe_nro_area(Double.valueOf(this.fe_nro_area));
		vo.setFe_long_abo_tec(Double.valueOf(this.fe_long_abo_tec));
		vo.setFe_cf_canal_hh24(Double.valueOf(this.fe_cf_canal_hh24));
		vo.setFe_fecha_vigencia(this.getAsDate(this.fe_fecha_vigencia));
		vo.setFe_zona_uo(Double.valueOf(this.fe_zona_uo));
		vo.setFe_recolector_info(this.fe_recolector_info);
		vo.setFe_adherido_843(this.fe_adherido_843);
		vo.setFe_fundamental_baja(this.fe_fundamental_baja);
		vo.setFe_cod_estado_zo_ofi(this.fe_cod_estado_zo_ofi);
		vo.setFe_oficina(Double.valueOf(this.fe_oficina));
		vo.setFe_region(this.fe_region);
		vo.setFe_cod_vto_proc(Double.valueOf(this.fe_cod_vto_proc));
		vo.setFe_territorio(this.fe_territorio);
		vo.setFe_marca_ddi(this.fe_marca_ddi);
		vo.setFe_tecnologia(this.fe_tecnologia);
		vo.setFe_oficina_baja(Double.valueOf(this.fe_oficina_baja));
		vo.setFe_cod_empresa(Double.valueOf(this.fe_cod_empresa));
		vo.setFe_unidad_central(this.fe_unidad_central);
		vo.setFe_cod_muni(Double.valueOf(this.fe_cod_muni));
		vo.setFe_fecha_carga(this.getAsDate(this.fe_fecha_carga));
		vo.setFe_nro_fundamental(this.fe_nro_fundamental);
		vo.setFe_usuario(this.fe_usuario);
		vo.setFe_cod_vto_real(Double.valueOf(this.fe_cod_vto_real));
		vo.setZonaVar(Double.valueOf(this.ZONAVAR));
		vo.setOficinaVar(Double.valueOf(this.OFICINAVAR));
		vo.setLeyenda(this.LEYENDA);
		vo.settipoServicio(Double.valueOf(this.tipoServicio));
		
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(String valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fe_grupo_medido
	* @return el valor de Fe_grupo_medido
	*/ 
	public String getFe_grupo_medido() {
		return fe_grupo_medido;
	}
    	
	/**
	* Fija el valor del atributo Fe_grupo_medido
	* @param valor el valor de Fe_grupo_medido
	*/ 
	public void setFe_grupo_medido(String valor) {
		this.fe_grupo_medido = valor;
	}
	/**
	* Recupera el valor del atributo Fe_descripcion
	* @return el valor de Fe_descripcion
	*/ 
	public String getFe_descripcion() {
		return fe_descripcion;
	}
    	
	/**
	* Fija el valor del atributo Fe_descripcion
	* @param valor el valor de Fe_descripcion
	*/ 
	public void setFe_descripcion(String valor) {
		this.fe_descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_ofic_pla
	* @return el valor de Fe_zona_ofic_pla
	*/ 
	public String getFe_zona_ofic_pla() {
		return fe_zona_ofic_pla;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_ofic_pla
	* @param valor el valor de Fe_zona_ofic_pla
	*/ 
	public void setFe_zona_ofic_pla(String valor) {
		this.fe_zona_ofic_pla = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cf_canal_dd
	* @return el valor de Fe_cf_canal_dd
	*/ 
	public String getFe_cf_canal_dd() {
		return fe_cf_canal_dd;
	}
    	
	/**
	* Fija el valor del atributo Fe_cf_canal_dd
	* @param valor el valor de Fe_cf_canal_dd
	*/ 
	public void setFe_cf_canal_dd(String valor) {
		this.fe_cf_canal_dd = valor;
	}
	/**
	* Recupera el valor del atributo Fe_long_abo
	* @return el valor de Fe_long_abo
	*/ 
	public String getFe_long_abo() {
		return fe_long_abo;
	}
    	
	/**
	* Fija el valor del atributo Fe_long_abo
	* @param valor el valor de Fe_long_abo
	*/ 
	public void setFe_long_abo(String valor) {
		this.fe_long_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fe_oficina_baja_tf
	* @return el valor de Fe_oficina_baja_tf
	*/ 
	public String getFe_oficina_baja_tf() {
		return fe_oficina_baja_tf;
	}
    	
	/**
	* Fija el valor del atributo Fe_oficina_baja_tf
	* @param valor el valor de Fe_oficina_baja_tf
	*/ 
	public void setFe_oficina_baja_tf(String valor) {
		this.fe_oficina_baja_tf = valor;
	}
	/**
	* Recupera el valor del atributo Fe_ciclo_sigeco
	* @return el valor de Fe_ciclo_sigeco
	*/ 
	public String getFe_ciclo_sigeco() {
		return fe_ciclo_sigeco;
	}
    	
	/**
	* Fija el valor del atributo Fe_ciclo_sigeco
	* @param valor el valor de Fe_ciclo_sigeco
	*/ 
	public void setFe_ciclo_sigeco(String valor) {
		this.fe_ciclo_sigeco = valor;
	}
	/**
	* Recupera el valor del atributo Fe_provincia
	* @return el valor de Fe_provincia
	*/ 
	public String getFe_provincia() {
		return fe_provincia;
	}
    	
	/**
	* Fija el valor del atributo Fe_provincia
	* @param valor el valor de Fe_provincia
	*/ 
	public void setFe_provincia(String valor) {
		this.fe_provincia = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona
	* @return el valor de Fe_zona
	*/ 
	public String getFe_zona() {
		return fe_zona;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona
	* @param valor el valor de Fe_zona
	*/ 
	public void setFe_zona(String valor) {
		this.fe_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_baja
	* @return el valor de Fe_zona_baja
	*/ 
	public String getFe_zona_baja() {
		return fe_zona_baja;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_baja
	* @param valor el valor de Fe_zona_baja
	*/ 
	public void setFe_zona_baja(String valor) {
		this.fe_zona_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_baja_tf
	* @return el valor de Fe_zona_baja_tf
	*/ 
	public String getFe_zona_baja_tf() {
		return fe_zona_baja_tf;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_baja_tf
	* @param valor el valor de Fe_zona_baja_tf
	*/ 
	public void setFe_zona_baja_tf(String valor) {
		this.fe_zona_baja_tf = valor;
	}
	/**
	* Recupera el valor del atributo Fe_marca_cdn
	* @return el valor de Fe_marca_cdn
	*/ 
	public String getFe_marca_cdn() {
		return fe_marca_cdn;
	}
    	
	/**
	* Fija el valor del atributo Fe_marca_cdn
	* @param valor el valor de Fe_marca_cdn
	*/ 
	public void setFe_marca_cdn(String valor) {
		this.fe_marca_cdn = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_interurbano
	* @return el valor de Fe_cod_interurbano
	*/ 
	public String getFe_cod_interurbano() {
		return fe_cod_interurbano;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_interurbano
	* @param valor el valor de Fe_cod_interurbano
	*/ 
	public void setFe_cod_interurbano(String valor) {
		this.fe_cod_interurbano = valor;
	}
	/**
	* Recupera el valor del atributo Fe_marca_sigeco
	* @return el valor de Fe_marca_sigeco
	*/ 
	public String getFe_marca_sigeco() {
		return fe_marca_sigeco;
	}
    	
	/**
	* Fija el valor del atributo Fe_marca_sigeco
	* @param valor el valor de Fe_marca_sigeco
	*/ 
	public void setFe_marca_sigeco(String valor) {
		this.fe_marca_sigeco = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_area
	* @return el valor de Fe_nro_area
	*/ 
	public String getFe_nro_area() {
		return fe_nro_area;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_area
	* @param valor el valor de Fe_nro_area
	*/ 
	public void setFe_nro_area(String valor) {
		this.fe_nro_area = valor;
	}
	/**
	* Recupera el valor del atributo Fe_long_abo_tec
	* @return el valor de Fe_long_abo_tec
	*/ 
	public String getFe_long_abo_tec() {
		return fe_long_abo_tec;
	}
    	
	/**
	* Fija el valor del atributo Fe_long_abo_tec
	* @param valor el valor de Fe_long_abo_tec
	*/ 
	public void setFe_long_abo_tec(String valor) {
		this.fe_long_abo_tec = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cf_canal_hh24
	* @return el valor de Fe_cf_canal_hh24
	*/ 
	public String getFe_cf_canal_hh24() {
		return fe_cf_canal_hh24;
	}
    	
	/**
	* Fija el valor del atributo Fe_cf_canal_hh24
	* @param valor el valor de Fe_cf_canal_hh24
	*/ 
	public void setFe_cf_canal_hh24(String valor) {
		this.fe_cf_canal_hh24 = valor;
	}
	/**
	* Recupera el valor del atributo Fe_fecha_vigencia
	* @return el valor de Fe_fecha_vigencia
	*/ 
	public String getFe_fecha_vigencia() {
		return fe_fecha_vigencia;
	}
    	
	/**
	* Fija el valor del atributo Fe_fecha_vigencia
	* @param valor el valor de Fe_fecha_vigencia
	*/ 
	public void setFe_fecha_vigencia(String valor) {
		this.fe_fecha_vigencia = valor;
	}
	/**
	* Recupera el valor del atributo Fe_zona_uo
	* @return el valor de Fe_zona_uo
	*/ 
	public String getFe_zona_uo() {
		return fe_zona_uo;
	}
    	
	/**
	* Fija el valor del atributo Fe_zona_uo
	* @param valor el valor de Fe_zona_uo
	*/ 
	public void setFe_zona_uo(String valor) {
		this.fe_zona_uo = valor;
	}
	/**
	* Recupera el valor del atributo Fe_recolector_info
	* @return el valor de Fe_recolector_info
	*/ 
	public String getFe_recolector_info() {
		return fe_recolector_info;
	}
    	
	/**
	* Fija el valor del atributo Fe_recolector_info
	* @param valor el valor de Fe_recolector_info
	*/ 
	public void setFe_recolector_info(String valor) {
		this.fe_recolector_info = valor;
	}
	/**
	* Recupera el valor del atributo Fe_adherido_843
	* @return el valor de Fe_adherido_843
	*/ 
	public String getFe_adherido_843() {
		return fe_adherido_843;
	}
    	
	/**
	* Fija el valor del atributo Fe_adherido_843
	* @param valor el valor de Fe_adherido_843
	*/ 
	public void setFe_adherido_843(String valor) {
		this.fe_adherido_843 = valor;
	}
	/**
	* Recupera el valor del atributo Fe_fundamental_baja
	* @return el valor de Fe_fundamental_baja
	*/ 
	public String getFe_fundamental_baja() {
		return fe_fundamental_baja;
	}
    	
	/**
	* Fija el valor del atributo Fe_fundamental_baja
	* @param valor el valor de Fe_fundamental_baja
	*/ 
	public void setFe_fundamental_baja(String valor) {
		this.fe_fundamental_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_estado_zo_ofi
	* @return el valor de Fe_cod_estado_zo_ofi
	*/ 
	public String getFe_cod_estado_zo_ofi() {
		return fe_cod_estado_zo_ofi;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_estado_zo_ofi
	* @param valor el valor de Fe_cod_estado_zo_ofi
	*/ 
	public void setFe_cod_estado_zo_ofi(String valor) {
		this.fe_cod_estado_zo_ofi = valor;
	}
	/**
	* Recupera el valor del atributo Fe_oficina
	* @return el valor de Fe_oficina
	*/ 
	public String getFe_oficina() {
		return fe_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fe_oficina
	* @param valor el valor de Fe_oficina
	*/ 
	public void setFe_oficina(String valor) {
		this.fe_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fe_region
	* @return el valor de Fe_region
	*/ 
	public String getFe_region() {
		return fe_region;
	}
    	
	/**
	* Fija el valor del atributo Fe_region
	* @param valor el valor de Fe_region
	*/ 
	public void setFe_region(String valor) {
		this.fe_region = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_vto_proc
	* @return el valor de Fe_cod_vto_proc
	*/ 
	public String getFe_cod_vto_proc() {
		return fe_cod_vto_proc;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_vto_proc
	* @param valor el valor de Fe_cod_vto_proc
	*/ 
	public void setFe_cod_vto_proc(String valor) {
		this.fe_cod_vto_proc = valor;
	}
	/**
	* Recupera el valor del atributo Fe_territorio
	* @return el valor de Fe_territorio
	*/ 
	public String getFe_territorio() {
		return fe_territorio;
	}
    	
	/**
	* Fija el valor del atributo Fe_territorio
	* @param valor el valor de Fe_territorio
	*/ 
	public void setFe_territorio(String valor) {
		this.fe_territorio = valor;
	}
	/**
	* Recupera el valor del atributo Fe_marca_ddi
	* @return el valor de Fe_marca_ddi
	*/ 
	public String getFe_marca_ddi() {
		return fe_marca_ddi;
	}
    	
	/**
	* Fija el valor del atributo Fe_marca_ddi
	* @param valor el valor de Fe_marca_ddi
	*/ 
	public void setFe_marca_ddi(String valor) {
		this.fe_marca_ddi = valor;
	}
	/**
	* Recupera el valor del atributo Fe_tecnologia
	* @return el valor de Fe_tecnologia
	*/ 
	public String getFe_tecnologia() {
		return fe_tecnologia;
	}
    	
	/**
	* Fija el valor del atributo Fe_tecnologia
	* @param valor el valor de Fe_tecnologia
	*/ 
	public void setFe_tecnologia(String valor) {
		this.fe_tecnologia = valor;
	}
	/**
	* Recupera el valor del atributo Fe_oficina_baja
	* @return el valor de Fe_oficina_baja
	*/ 
	public String getFe_oficina_baja() {
		return fe_oficina_baja;
	}
    	
	/**
	* Fija el valor del atributo Fe_oficina_baja
	* @param valor el valor de Fe_oficina_baja
	*/ 
	public void setFe_oficina_baja(String valor) {
		this.fe_oficina_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_empresa
	* @return el valor de Fe_cod_empresa
	*/ 
	public String getFe_cod_empresa() {
		return fe_cod_empresa;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_empresa
	* @param valor el valor de Fe_cod_empresa
	*/ 
	public void setFe_cod_empresa(String valor) {
		this.fe_cod_empresa = valor;
	}
	/**
	* Recupera el valor del atributo Fe_unidad_central
	* @return el valor de Fe_unidad_central
	*/ 
	public String getFe_unidad_central() {
		return fe_unidad_central;
	}
    	
	/**
	* Fija el valor del atributo Fe_unidad_central
	* @param valor el valor de Fe_unidad_central
	*/ 
	public void setFe_unidad_central(String valor) {
		this.fe_unidad_central = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_muni
	* @return el valor de Fe_cod_muni
	*/ 
	public String getFe_cod_muni() {
		return fe_cod_muni;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_muni
	* @param valor el valor de Fe_cod_muni
	*/ 
	public void setFe_cod_muni(String valor) {
		this.fe_cod_muni = valor;
	}
	/**
	* Recupera el valor del atributo Fe_fecha_carga
	* @return el valor de Fe_fecha_carga
	*/ 
	public String getFe_fecha_carga() {
		return fe_fecha_carga;
	}
    	
	/**
	* Fija el valor del atributo Fe_fecha_carga
	* @param valor el valor de Fe_fecha_carga
	*/ 
	public void setFe_fecha_carga(String valor) {
		this.fe_fecha_carga = valor;
	}
	/**
	* Recupera el valor del atributo Fe_nro_fundamental
	* @return el valor de Fe_nro_fundamental
	*/ 
	public String getFe_nro_fundamental() {
		return fe_nro_fundamental;
	}
    	
	/**
	* Fija el valor del atributo Fe_nro_fundamental
	* @param valor el valor de Fe_nro_fundamental
	*/ 
	public void setFe_nro_fundamental(String valor) {
		this.fe_nro_fundamental = valor;
	}
	/**
	* Recupera el valor del atributo Fe_usuario
	* @return el valor de Fe_usuario
	*/ 
	public String getFe_usuario() {
		return fe_usuario;
	}
    	
	/**
	* Fija el valor del atributo Fe_usuario
	* @param valor el valor de Fe_usuario
	*/ 
	public void setFe_usuario(String valor) {
		this.fe_usuario = valor;
	}
	/**
	* Recupera el valor del atributo Fe_cod_vto_real
	* @return el valor de Fe_cod_vto_real
	*/ 
	public String getFe_cod_vto_real() {
		return fe_cod_vto_real;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_vto_real
	* @param valor el valor de Fe_cod_vto_real
	*/ 
	public void setFe_cod_vto_real(String valor) {
		this.fe_cod_vto_real = valor;
	}
	

	/**
	* Recupera el valor del atributo Fe_cod_vto_real
	* @return el valor de Fe_cod_vto_real
	*/ 
	public String getZonaVar() {
		return ZONAVAR;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_vto_real
	* @param valor el valor de Fe_cod_vto_real
	*/ 
	public void setZonaVar(String zona) {
		this.ZONAVAR = zona;
	}
	
	/**
	* Recupera el valor del atributo Fe_cod_vto_real
	* @return el valor de Fe_cod_vto_real
	*/ 
	public String getOficinaVar() {
		return OFICINAVAR;
	}
    	
	/**
	* Fija el valor del atributo Fe_cod_vto_real
	* @param valor el valor de Fe_cod_vto_real
	*/ 
	public void setoficinaVar(String oficina) {
		this.OFICINAVAR = oficina;
	}
	
	
	/**
	* Recupera el valor del atributo Fe_cod_vto_real
	* @return el valor de Fe_cod_vto_real
	*/ 
	public Double gettipoServicio() {
		return tipoServicio;
	}
    	
	
	
	/**
	* Fija el valor del atributo Fe_cod_vto_real
	* @param valor el valor de Fe_cod_vto_real
	*/ 
	public void settipoServicio(Double Descripcion) {
		this.tipoServicio = tipoServicio;
	}
	
	
	
	/**
	* Recupera el valor del atributo Fe_cod_vto_real
	* @return el valor de Fe_cod_vto_real
	*/ 
	public String getLeyenda() {
		return LEYENDA;
	}
	/**
	* Fija el valor del atributo Fe_cod_vto_real
	* @param valor el valor de Fe_cod_vto_real
	*/ 
	public void setLeyenda(String leyenda) {
		this.LEYENDA = leyenda;
	}
	
	
}
