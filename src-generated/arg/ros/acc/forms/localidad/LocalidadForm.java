package arg.ros.acc.forms.localidad;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.localidad.LocalidadVO;

/**
* LocalidadForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class LocalidadForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String idlocalidad;
	private String nombre;
	private String idprovincia;
	private String codigopostal;

	public LocalidadVO getVO() {
		LocalidadVO vo = new LocalidadVO();
		
		vo.setIdlocalidad(Double.valueOf(this.idlocalidad));
		vo.setNombre(this.nombre);
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Idlocalidad
	* @return el valor de Idlocalidad
	*/ 
	public String getIdlocalidad() {
		return idlocalidad;
	}
    	
	/**
	* Fija el valor del atributo Idlocalidad
	* @param valor el valor de Idlocalidad
	*/ 
	public void setIdlocalidad(String valor) {
		this.idlocalidad = valor;
	}
	/**
	* Recupera el valor del atributo Nombre
	* @return el valor de Nombre
	*/ 
	public String getNombre() {
		return nombre;
	}
    	
	/**
	* Fija el valor del atributo Nombre
	* @param valor el valor de Nombre
	*/ 
	public void setNombre(String valor) {
		this.nombre = valor;
	}
	/**
	* Recupera el valor del atributo Idprovincia
	* @return el valor de Idprovincia
	*/ 
	public String getIdprovincia() {
		return idprovincia;
	}
    	
	/**
	* Fija el valor del atributo Idprovincia
	* @param valor el valor de Idprovincia
	*/ 
	public void setIdprovincia(String valor) {
		this.idprovincia = valor;
	}
	/**
	* Recupera el valor del atributo Codigopostal
	* @return el valor de Codigopostal
	*/ 
	public String getCodigopostal() {
		return codigopostal;
	}
    	
	/**
	* Fija el valor del atributo Codigopostal
	* @param valor el valor de Codigopostal
	*/ 
	public void setCodigopostal(String valor) {
		this.codigopostal = valor;
	}
}
