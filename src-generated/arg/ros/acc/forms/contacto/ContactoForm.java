package arg.ros.acc.forms.contacto;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.contacto.ContactoVO;

/**
* ContactoForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ContactoForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String idpersonacontacto;
	private String idpersona;
	private String idcontactotipo;
	private String valor;
	private String observacion;

	public ContactoVO getVO() {
		ContactoVO vo = new ContactoVO();
		
		vo.setIdpersonacontacto(Double.valueOf(this.idpersonacontacto));
		vo.setIdpersona(Double.valueOf(this.idpersona));
		vo.setValor(this.valor);
		vo.setObservacion(this.observacion);
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Idpersonacontacto
	* @return el valor de Idpersonacontacto
	*/ 
	public String getIdpersonacontacto() {
		return idpersonacontacto;
	}
    	
	/**
	* Fija el valor del atributo Idpersonacontacto
	* @param valor el valor de Idpersonacontacto
	*/ 
	public void setIdpersonacontacto(String valor) {
		this.idpersonacontacto = valor;
	}
	/**
	* Recupera el valor del atributo Idpersona
	* @return el valor de Idpersona
	*/ 
	public String getIdpersona() {
		return idpersona;
	}
    	
	/**
	* Fija el valor del atributo Idpersona
	* @param valor el valor de Idpersona
	*/ 
	public void setIdpersona(String valor) {
		this.idpersona = valor;
	}
	/**
	* Recupera el valor del atributo Idcontactotipo
	* @return el valor de Idcontactotipo
	*/ 
	public String getIdcontactotipo() {
		return idcontactotipo;
	}
    	
	/**
	* Fija el valor del atributo Idcontactotipo
	* @param valor el valor de Idcontactotipo
	*/ 
	public void setIdcontactotipo(String valor) {
		this.idcontactotipo = valor;
	}
	/**
	* Recupera el valor del atributo Valor
	* @return el valor de Valor
	*/ 
	public String getValor() {
		return valor;
	}
    	
	/**
	* Fija el valor del atributo Valor
	* @param valor el valor de Valor
	*/ 
	public void setValor(String valor) {
		this.valor = valor;
	}
	/**
	* Recupera el valor del atributo Observacion
	* @return el valor de Observacion
	*/ 
	public String getObservacion() {
		return observacion;
	}
    	
	/**
	* Fija el valor del atributo Observacion
	* @param valor el valor de Observacion
	*/ 
	public void setObservacion(String valor) {
		this.observacion = valor;
	}
}
