package arg.ros.acc.forms.contacto;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.contacto.ContactotipoVO;

/**
* ContactotipoForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ContactotipoForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String idcontactotipo;
	private String descripcion;

	public ContactotipoVO getVO() {
		ContactotipoVO vo = new ContactotipoVO();
		vo.setIdcontactotipo(Integer.valueOf(this.idcontactotipo == null?"0":this.idcontactotipo));
		vo.setDescripcion(this.descripcion);
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Idcontactotipo
	* @return el valor de Idcontactotipo
	*/ 
	public String getIdcontactotipo() {
		return idcontactotipo;
	}
    	
	/**
	* Fija el valor del atributo Idcontactotipo
	* @param valor el valor de Idcontactotipo
	*/ 
	public void setIdcontactotipo(String valor) {
		this.idcontactotipo = valor;
	}
	/**
	* Recupera el valor del atributo Descripcion
	* @return el valor de Descripcion
	*/ 
	public String getDescripcion() {
		return descripcion;
	}
    	
	/**
	* Fija el valor del atributo Descripcion
	* @param valor el valor de Descripcion
	*/ 
	public void setDescripcion(String valor) {
		this.descripcion = valor;
	}
}
