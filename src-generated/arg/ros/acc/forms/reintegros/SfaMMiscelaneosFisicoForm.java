package arg.ros.acc.forms.reintegros;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.reintegros.SfaMMiscelaneosFisicoVO;

/**
* SfaMMiscelaneosFisicoForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class SfaMMiscelaneosFisicoForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String adabas_isn;
	private String fa_m_zona;
	private String fa_m_oficina;
	private String fa_m_abonado;
	private String fa_m_tipo_factura;
	private String fa_m_anio_lote;
	private String fa_m_zona_pago;
	private String fa_m_fecha_pago;
	private String fa_m_fecha_imputacion;
	private String fa_m_fecha_proceso_tarea;
	private String fa_m_marca_tarea;
	private String fa_m_importe_miscelaneos;
	private String fa_m_estado_bajada;
	private String fa_m_concepto_fact;
	private String fa_m_zona_facturada;
	private String fa_m_oficina_facturada;
	private String fa_m_abonado_facturado;
	private String fa_m_anio_lote_facturado;
	private String fa_m_tipo_servicio_facturado;
	private String fa_m_fecha_alta;
	private String fa_m_usuario_alta;
	private String fa_m_fecha_envio;
	private String fa_m_usuario_envio;
	private String fa_m_fecha_baja;
	private String fa_m_usuario_baja;
	private String fa_m_fecha_vencimiento;
	private String fa_m_fecha_facturacion;
	private String fa_m_usuario_facturacion;
	private String fa_m_nro_reinteg;
	private String fa_m_cpto_zona;
	private String fa_m_cpto_oficina;
	private String fa_m_cpto_abonado;
	private String fa_m_cpto_tipo_fact;
	private String fa_m_cpto_anio_lote;
	private String fa_m_cpto_concepto;
	private String fa_m_cpto_cod_tercero;
	private String fa_m_cpto_estado;
	private String fa_m_cpto_tarea;
	private String fa_m_cpto_importe;
	private String fa_m_cpto_fecha_alta;
	private String fa_m_cpto_usuario_alta;
	private String fa_m_cpto_fecha_aplicacion;
	private String fa_m_cpto_usuario_aplicacion;
	private String fa_m_interurb;
	private String fa_m_urb;
	private String fa_m_linea;
	private String fa_m_ident_abo;
	private String fa_m_interurb_facturado;
	private String fa_m_urb_facturado;
	private String fa_m_linea_facturada;
	
	// AGREGADO
	private String USUARIO_DINAMICO;
	private java.util.Date FECHA_DINAMICA;
	private String ESTADO_DINAMICO;
	private String DESC_MISC_R;
	

	public SfaMMiscelaneosFisicoVO getVO() {
		SfaMMiscelaneosFisicoVO vo = new SfaMMiscelaneosFisicoVO();
		
		vo.setAdabas_isn(Double.valueOf(this.adabas_isn));
		vo.setFa_m_zona(Double.valueOf(this.fa_m_zona));
		vo.setFa_m_oficina(Double.valueOf(this.fa_m_oficina));
		vo.setFa_m_abonado(Double.valueOf(this.fa_m_abonado));
		vo.setFa_m_tipo_factura(Double.valueOf(this.fa_m_tipo_factura));
		vo.setFa_m_anio_lote(Double.valueOf(this.fa_m_anio_lote));
		vo.setFa_m_zona_pago(Double.valueOf(this.fa_m_zona_pago));
		vo.setFa_m_fecha_pago(this.getAsDate(this.fa_m_fecha_pago));
		vo.setFa_m_fecha_imputacion(this.getAsDate(this.fa_m_fecha_imputacion));
		vo.setFa_m_fecha_proceso_tarea(this.getAsDate(this.fa_m_fecha_proceso_tarea));
		vo.setFa_m_marca_tarea(Double.valueOf(this.fa_m_marca_tarea));
		vo.setFa_m_importe_miscelaneos(new java.math.BigDecimal(this.fa_m_importe_miscelaneos));
		vo.setFa_m_estado_bajada(Double.valueOf(this.fa_m_estado_bajada));
		vo.setFa_m_concepto_fact(Double.valueOf(this.fa_m_concepto_fact));
		vo.setFa_m_zona_facturada(Double.valueOf(this.fa_m_zona_facturada));
		vo.setFa_m_oficina_facturada(Double.valueOf(this.fa_m_oficina_facturada));
		vo.setFa_m_abonado_facturado(Double.valueOf(this.fa_m_abonado_facturado));
		vo.setFa_m_anio_lote_facturado(Double.valueOf(this.fa_m_anio_lote_facturado));
		vo.setFa_m_tipo_servicio_facturado(Double.valueOf(this.fa_m_tipo_servicio_facturado));
		vo.setFa_m_fecha_alta(this.getAsDate(this.fa_m_fecha_alta));
		vo.setFa_m_usuario_alta(this.fa_m_usuario_alta);
		vo.setFa_m_fecha_envio(this.getAsDate(this.fa_m_fecha_envio));
		vo.setFa_m_usuario_envio(this.fa_m_usuario_envio);
		vo.setFa_m_fecha_baja(this.getAsDate(this.fa_m_fecha_baja));
		vo.setFa_m_usuario_baja(this.fa_m_usuario_baja);
		vo.setFa_m_fecha_vencimiento(this.fa_m_fecha_vencimiento);
		vo.setFa_m_fecha_facturacion(this.getAsDate(this.fa_m_fecha_facturacion));
		vo.setFa_m_usuario_facturacion(this.fa_m_usuario_facturacion);
		vo.setFa_m_nro_reinteg(Double.valueOf(this.fa_m_nro_reinteg));
		vo.setFa_m_cpto_zona(Double.valueOf(this.fa_m_cpto_zona));
		vo.setFa_m_cpto_oficina(Double.valueOf(this.fa_m_cpto_oficina));
		vo.setFa_m_cpto_abonado(Double.valueOf(this.fa_m_cpto_abonado));
		vo.setFa_m_cpto_tipo_fact(Double.valueOf(this.fa_m_cpto_tipo_fact));
		vo.setFa_m_cpto_anio_lote(Double.valueOf(this.fa_m_cpto_anio_lote));
		vo.setFa_m_cpto_concepto(Double.valueOf(this.fa_m_cpto_concepto));
		vo.setFa_m_cpto_cod_tercero(Double.valueOf(this.fa_m_cpto_cod_tercero));
		vo.setFa_m_cpto_estado(Double.valueOf(this.fa_m_cpto_estado));
		vo.setFa_m_cpto_tarea(Double.valueOf(this.fa_m_cpto_tarea));
		vo.setFa_m_cpto_importe(Double.valueOf(this.fa_m_cpto_importe));
		vo.setFa_m_cpto_fecha_alta(this.getAsDate(this.fa_m_cpto_fecha_alta));
		vo.setFa_m_cpto_usuario_alta(this.fa_m_cpto_usuario_alta);
		vo.setFa_m_cpto_fecha_aplicacion(this.getAsDate(this.fa_m_cpto_fecha_aplicacion));
		vo.setFa_m_cpto_usuario_aplicacion(this.fa_m_cpto_usuario_aplicacion);
		vo.setFa_m_interurb(this.fa_m_interurb);
		vo.setFa_m_urb(this.fa_m_urb);
		vo.setFa_m_linea(this.fa_m_linea);
		vo.setFa_m_ident_abo(Double.valueOf(this.fa_m_ident_abo));
		vo.setFa_m_interurb_facturado(this.fa_m_interurb_facturado);
		vo.setFa_m_urb_facturado(this.fa_m_urb_facturado);
		vo.setFa_m_linea_facturada(this.fa_m_linea_facturada);
		
		
		// AGREGADO
		
		vo.setUSUARIO_DINAMICO(this.USUARIO_DINAMICO);
		vo.setFECHA_DINAMICA(this.FECHA_DINAMICA);
		vo.setESTADO_DINAMICO(this.ESTADO_DINAMICO);
		vo.setDESC_MISC_R(this.DESC_MISC_R);
	
		return vo;
	}
	 
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(String valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_zona
	* @return el valor de Fa_m_zona
	*/ 
	public String getFa_m_zona() {
		return fa_m_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_zona
	* @param valor el valor de Fa_m_zona
	*/ 
	public void setFa_m_zona(String valor) {
		this.fa_m_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_oficina
	* @return el valor de Fa_m_oficina
	*/ 
	public String getFa_m_oficina() {
		return fa_m_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_oficina
	* @param valor el valor de Fa_m_oficina
	*/ 
	public void setFa_m_oficina(String valor) {
		this.fa_m_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_abonado
	* @return el valor de Fa_m_abonado
	*/ 
	public String getFa_m_abonado() {
		return fa_m_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_abonado
	* @param valor el valor de Fa_m_abonado
	*/ 
	public void setFa_m_abonado(String valor) {
		this.fa_m_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_tipo_factura
	* @return el valor de Fa_m_tipo_factura
	*/ 
	public String getFa_m_tipo_factura() {
		return fa_m_tipo_factura;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_tipo_factura
	* @param valor el valor de Fa_m_tipo_factura
	*/ 
	public void setFa_m_tipo_factura(String valor) {
		this.fa_m_tipo_factura = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_anio_lote
	* @return el valor de Fa_m_anio_lote
	*/ 
	public String getFa_m_anio_lote() {
		return fa_m_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_anio_lote
	* @param valor el valor de Fa_m_anio_lote
	*/ 
	public void setFa_m_anio_lote(String valor) {
		this.fa_m_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_zona_pago
	* @return el valor de Fa_m_zona_pago
	*/ 
	public String getFa_m_zona_pago() {
		return fa_m_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_zona_pago
	* @param valor el valor de Fa_m_zona_pago
	*/ 
	public void setFa_m_zona_pago(String valor) {
		this.fa_m_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_pago
	* @return el valor de Fa_m_fecha_pago
	*/ 
	public String getFa_m_fecha_pago() {
		return fa_m_fecha_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_pago
	* @param valor el valor de Fa_m_fecha_pago
	*/ 
	public void setFa_m_fecha_pago(String valor) {
		this.fa_m_fecha_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_imputacion
	* @return el valor de Fa_m_fecha_imputacion
	*/ 
	public String getFa_m_fecha_imputacion() {
		return fa_m_fecha_imputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_imputacion
	* @param valor el valor de Fa_m_fecha_imputacion
	*/ 
	public void setFa_m_fecha_imputacion(String valor) {
		this.fa_m_fecha_imputacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_proceso_tarea
	* @return el valor de Fa_m_fecha_proceso_tarea
	*/ 
	public String getFa_m_fecha_proceso_tarea() {
		return fa_m_fecha_proceso_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_proceso_tarea
	* @param valor el valor de Fa_m_fecha_proceso_tarea
	*/ 
	public void setFa_m_fecha_proceso_tarea(String valor) {
		this.fa_m_fecha_proceso_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_marca_tarea
	* @return el valor de Fa_m_marca_tarea
	*/ 
	public String getFa_m_marca_tarea() {
		return fa_m_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_marca_tarea
	* @param valor el valor de Fa_m_marca_tarea
	*/ 
	public void setFa_m_marca_tarea(String valor) {
		this.fa_m_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_importe_miscelaneos
	* @return el valor de Fa_m_importe_miscelaneos
	*/ 
	public String getFa_m_importe_miscelaneos() {
		return fa_m_importe_miscelaneos;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_importe_miscelaneos
	* @param valor el valor de Fa_m_importe_miscelaneos
	*/ 
	public void setFa_m_importe_miscelaneos(String valor) {
		this.fa_m_importe_miscelaneos = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_estado_bajada
	* @return el valor de Fa_m_estado_bajada
	*/ 
	public String getFa_m_estado_bajada() {
		return fa_m_estado_bajada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_estado_bajada
	* @param valor el valor de Fa_m_estado_bajada
	*/ 
	public void setFa_m_estado_bajada(String valor) {
		this.fa_m_estado_bajada = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_concepto_fact
	* @return el valor de Fa_m_concepto_fact
	*/ 
	public String getFa_m_concepto_fact() {
		return fa_m_concepto_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_concepto_fact
	* @param valor el valor de Fa_m_concepto_fact
	*/ 
	public void setFa_m_concepto_fact(String valor) {
		this.fa_m_concepto_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_zona_facturada
	* @return el valor de Fa_m_zona_facturada
	*/ 
	public String getFa_m_zona_facturada() {
		return fa_m_zona_facturada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_zona_facturada
	* @param valor el valor de Fa_m_zona_facturada
	*/ 
	public void setFa_m_zona_facturada(String valor) {
		this.fa_m_zona_facturada = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_oficina_facturada
	* @return el valor de Fa_m_oficina_facturada
	*/ 
	public String getFa_m_oficina_facturada() {
		return fa_m_oficina_facturada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_oficina_facturada
	* @param valor el valor de Fa_m_oficina_facturada
	*/ 
	public void setFa_m_oficina_facturada(String valor) {
		this.fa_m_oficina_facturada = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_abonado_facturado
	* @return el valor de Fa_m_abonado_facturado
	*/ 
	public String getFa_m_abonado_facturado() {
		return fa_m_abonado_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_abonado_facturado
	* @param valor el valor de Fa_m_abonado_facturado
	*/ 
	public void setFa_m_abonado_facturado(String valor) {
		this.fa_m_abonado_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_anio_lote_facturado
	* @return el valor de Fa_m_anio_lote_facturado
	*/ 
	public String getFa_m_anio_lote_facturado() {
		return fa_m_anio_lote_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_anio_lote_facturado
	* @param valor el valor de Fa_m_anio_lote_facturado
	*/ 
	public void setFa_m_anio_lote_facturado(String valor) {
		this.fa_m_anio_lote_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_tipo_servicio_facturado
	* @return el valor de Fa_m_tipo_servicio_facturado
	*/ 
	public String getFa_m_tipo_servicio_facturado() {
		return fa_m_tipo_servicio_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_tipo_servicio_facturado
	* @param valor el valor de Fa_m_tipo_servicio_facturado
	*/ 
	public void setFa_m_tipo_servicio_facturado(String valor) {
		this.fa_m_tipo_servicio_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_alta
	* @return el valor de Fa_m_fecha_alta
	*/ 
	public String getFa_m_fecha_alta() {
		return fa_m_fecha_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_alta
	* @param valor el valor de Fa_m_fecha_alta
	*/ 
	public void setFa_m_fecha_alta(String valor) {
		this.fa_m_fecha_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_alta
	* @return el valor de Fa_m_usuario_alta
	*/ 
	public String getFa_m_usuario_alta() {
		return fa_m_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_alta
	* @param valor el valor de Fa_m_usuario_alta
	*/ 
	public void setFa_m_usuario_alta(String valor) {
		this.fa_m_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_envio
	* @return el valor de Fa_m_fecha_envio
	*/ 
	public String getFa_m_fecha_envio() {
		return fa_m_fecha_envio;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_envio
	* @param valor el valor de Fa_m_fecha_envio
	*/ 
	public void setFa_m_fecha_envio(String valor) {
		this.fa_m_fecha_envio = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_envio
	* @return el valor de Fa_m_usuario_envio
	*/ 
	public String getFa_m_usuario_envio() {
		return fa_m_usuario_envio;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_envio
	* @param valor el valor de Fa_m_usuario_envio
	*/ 
	public void setFa_m_usuario_envio(String valor) {
		this.fa_m_usuario_envio = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_baja
	* @return el valor de Fa_m_fecha_baja
	*/ 
	public String getFa_m_fecha_baja() {
		return fa_m_fecha_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_baja
	* @param valor el valor de Fa_m_fecha_baja
	*/ 
	public void setFa_m_fecha_baja(String valor) {
		this.fa_m_fecha_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_baja
	* @return el valor de Fa_m_usuario_baja
	*/ 
	public String getFa_m_usuario_baja() {
		return fa_m_usuario_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_baja
	* @param valor el valor de Fa_m_usuario_baja
	*/ 
	public void setFa_m_usuario_baja(String valor) {
		this.fa_m_usuario_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_vencimiento
	* @return el valor de Fa_m_fecha_vencimiento
	*/ 
	public String getFa_m_fecha_vencimiento() {
		return fa_m_fecha_vencimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_vencimiento
	* @param valor el valor de Fa_m_fecha_vencimiento
	*/ 
	public void setFa_m_fecha_vencimiento(String valor) {
		this.fa_m_fecha_vencimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_fecha_facturacion
	* @return el valor de Fa_m_fecha_facturacion
	*/ 
	public String getFa_m_fecha_facturacion() {
		return fa_m_fecha_facturacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_fecha_facturacion
	* @param valor el valor de Fa_m_fecha_facturacion
	*/ 
	public void setFa_m_fecha_facturacion(String valor) {
		this.fa_m_fecha_facturacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_usuario_facturacion
	* @return el valor de Fa_m_usuario_facturacion
	*/ 
	public String getFa_m_usuario_facturacion() {
		return fa_m_usuario_facturacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_usuario_facturacion
	* @param valor el valor de Fa_m_usuario_facturacion
	*/ 
	public void setFa_m_usuario_facturacion(String valor) {
		this.fa_m_usuario_facturacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_nro_reinteg
	* @return el valor de Fa_m_nro_reinteg
	*/ 
	public String getFa_m_nro_reinteg() {
		return fa_m_nro_reinteg;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_nro_reinteg
	* @param valor el valor de Fa_m_nro_reinteg
	*/ 
	public void setFa_m_nro_reinteg(String valor) {
		this.fa_m_nro_reinteg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_zona
	* @return el valor de Fa_m_cpto_zona
	*/ 
	public String getFa_m_cpto_zona() {
		return fa_m_cpto_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_zona
	* @param valor el valor de Fa_m_cpto_zona
	*/ 
	public void setFa_m_cpto_zona(String valor) {
		this.fa_m_cpto_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_oficina
	* @return el valor de Fa_m_cpto_oficina
	*/ 
	public String getFa_m_cpto_oficina() {
		return fa_m_cpto_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_oficina
	* @param valor el valor de Fa_m_cpto_oficina
	*/ 
	public void setFa_m_cpto_oficina(String valor) {
		this.fa_m_cpto_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_abonado
	* @return el valor de Fa_m_cpto_abonado
	*/ 
	public String getFa_m_cpto_abonado() {
		return fa_m_cpto_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_abonado
	* @param valor el valor de Fa_m_cpto_abonado
	*/ 
	public void setFa_m_cpto_abonado(String valor) {
		this.fa_m_cpto_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_tipo_fact
	* @return el valor de Fa_m_cpto_tipo_fact
	*/ 
	public String getFa_m_cpto_tipo_fact() {
		return fa_m_cpto_tipo_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_tipo_fact
	* @param valor el valor de Fa_m_cpto_tipo_fact
	*/ 
	public void setFa_m_cpto_tipo_fact(String valor) {
		this.fa_m_cpto_tipo_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_anio_lote
	* @return el valor de Fa_m_cpto_anio_lote
	*/ 
	public String getFa_m_cpto_anio_lote() {
		return fa_m_cpto_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_anio_lote
	* @param valor el valor de Fa_m_cpto_anio_lote
	*/ 
	public void setFa_m_cpto_anio_lote(String valor) {
		this.fa_m_cpto_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_concepto
	* @return el valor de Fa_m_cpto_concepto
	*/ 
	public String getFa_m_cpto_concepto() {
		return fa_m_cpto_concepto;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_concepto
	* @param valor el valor de Fa_m_cpto_concepto
	*/ 
	public void setFa_m_cpto_concepto(String valor) {
		this.fa_m_cpto_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_cod_tercero
	* @return el valor de Fa_m_cpto_cod_tercero
	*/ 
	public String getFa_m_cpto_cod_tercero() {
		return fa_m_cpto_cod_tercero;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_cod_tercero
	* @param valor el valor de Fa_m_cpto_cod_tercero
	*/ 
	public void setFa_m_cpto_cod_tercero(String valor) {
		this.fa_m_cpto_cod_tercero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_estado
	* @return el valor de Fa_m_cpto_estado
	*/ 
	public String getFa_m_cpto_estado() {
		return fa_m_cpto_estado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_estado
	* @param valor el valor de Fa_m_cpto_estado
	*/ 
	public void setFa_m_cpto_estado(String valor) {
		this.fa_m_cpto_estado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_tarea
	* @return el valor de Fa_m_cpto_tarea
	*/ 
	public String getFa_m_cpto_tarea() {
		return fa_m_cpto_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_tarea
	* @param valor el valor de Fa_m_cpto_tarea
	*/ 
	public void setFa_m_cpto_tarea(String valor) {
		this.fa_m_cpto_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_importe
	* @return el valor de Fa_m_cpto_importe
	*/ 
	public String getFa_m_cpto_importe() {
		return fa_m_cpto_importe;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_importe
	* @param valor el valor de Fa_m_cpto_importe
	*/ 
	public void setFa_m_cpto_importe(String valor) {
		this.fa_m_cpto_importe = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_fecha_alta
	* @return el valor de Fa_m_cpto_fecha_alta
	*/ 
	public String getFa_m_cpto_fecha_alta() {
		return fa_m_cpto_fecha_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_fecha_alta
	* @param valor el valor de Fa_m_cpto_fecha_alta
	*/ 
	public void setFa_m_cpto_fecha_alta(String valor) {
		this.fa_m_cpto_fecha_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_usuario_alta
	* @return el valor de Fa_m_cpto_usuario_alta
	*/ 
	public String getFa_m_cpto_usuario_alta() {
		return fa_m_cpto_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_usuario_alta
	* @param valor el valor de Fa_m_cpto_usuario_alta
	*/ 
	public void setFa_m_cpto_usuario_alta(String valor) {
		this.fa_m_cpto_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_fecha_aplicacion
	* @return el valor de Fa_m_cpto_fecha_aplicacion
	*/ 
	public String getFa_m_cpto_fecha_aplicacion() {
		return fa_m_cpto_fecha_aplicacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_fecha_aplicacion
	* @param valor el valor de Fa_m_cpto_fecha_aplicacion
	*/ 
	public void setFa_m_cpto_fecha_aplicacion(String valor) {
		this.fa_m_cpto_fecha_aplicacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_cpto_usuario_aplicacion
	* @return el valor de Fa_m_cpto_usuario_aplicacion
	*/ 
	public String getFa_m_cpto_usuario_aplicacion() {
		return fa_m_cpto_usuario_aplicacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_cpto_usuario_aplicacion
	* @param valor el valor de Fa_m_cpto_usuario_aplicacion
	*/ 
	public void setFa_m_cpto_usuario_aplicacion(String valor) {
		this.fa_m_cpto_usuario_aplicacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_interurb
	* @return el valor de Fa_m_interurb
	*/ 
	public String getFa_m_interurb() {
		return fa_m_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_interurb
	* @param valor el valor de Fa_m_interurb
	*/ 
	public void setFa_m_interurb(String valor) {
		this.fa_m_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_urb
	* @return el valor de Fa_m_urb
	*/ 
	public String getFa_m_urb() {
		return fa_m_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_urb
	* @param valor el valor de Fa_m_urb
	*/ 
	public void setFa_m_urb(String valor) {
		this.fa_m_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_linea
	* @return el valor de Fa_m_linea
	*/ 
	public String getFa_m_linea() {
		return fa_m_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_linea
	* @param valor el valor de Fa_m_linea
	*/ 
	public void setFa_m_linea(String valor) {
		this.fa_m_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_ident_abo
	* @return el valor de Fa_m_ident_abo
	*/ 
	public String getFa_m_ident_abo() {
		return fa_m_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_ident_abo
	* @param valor el valor de Fa_m_ident_abo
	*/ 
	public void setFa_m_ident_abo(String valor) {
		this.fa_m_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_interurb_facturado
	* @return el valor de Fa_m_interurb_facturado
	*/ 
	public String getFa_m_interurb_facturado() {
		return fa_m_interurb_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_interurb_facturado
	* @param valor el valor de Fa_m_interurb_facturado
	*/ 
	public void setFa_m_interurb_facturado(String valor) {
		this.fa_m_interurb_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_urb_facturado
	* @return el valor de Fa_m_urb_facturado
	*/ 
	public String getFa_m_urb_facturado() {
		return fa_m_urb_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_urb_facturado
	* @param valor el valor de Fa_m_urb_facturado
	*/ 
	public void setFa_m_urb_facturado(String valor) {
		this.fa_m_urb_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_m_linea_facturada
	* @return el valor de Fa_m_linea_facturada
	*/ 
	public String getFa_m_linea_facturada() {
		return fa_m_linea_facturada;
	}
    	
	/**
	* Fija el valor del atributo Fa_m_linea_facturada
	* @param valor el valor de Fa_m_linea_facturada
	*/ 
	public void setFa_m_linea_facturada(String valor) {
		this.fa_m_linea_facturada = valor;
	}
	
	
	
	//AGREGADO PARA MISCELANEOS INICIO
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getUSUARIO_DINAMICO() {
		return USUARIO_DINAMICO;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setUSUARIO_DINAMICO(String valor) {
		this.USUARIO_DINAMICO = valor;
	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public java.util.Date getFECHA_DINAMICA() {
		return FECHA_DINAMICA;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setFECHA_DINAMICA(java.util.Date valor) {
		this.FECHA_DINAMICA = valor;
	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getESTADO_DINAMICO() {
		return ESTADO_DINAMICO;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setESTADO_DINAMICO(String valor) {
		this.ESTADO_DINAMICO = valor;
	}
		
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getDESC_MISC_R() {
		return DESC_MISC_R;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setDESC_MISC_R(String valor) {
		this.DESC_MISC_R = valor;
	}
	
	
	//AGREGADO PARA MISCELANEOS FIN
		
}
