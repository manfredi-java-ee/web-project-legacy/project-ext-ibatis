package arg.ros.acc.forms.reintegros;

import java.util.Date;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.reintegros.SfaFisicoReintegrosVO;
//import java.lang.Object;
/**
* SfaFisicoReintegrosForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class SfaFisicoReintegrosForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String campo_no_usado_17;
	private String fa_no_hora_alta_nc;
	private String campo_no_usado_18;
	private String fa_no_hora_alta_nd;
	private String campo_no_usado_19;
	private String fa_no_hora_mod_nc;
	private String campo_no_usado_20;
	private String fa_no_hora_mod_nd;
	private String fa_no_cat_gran_cli;
	private String fa_no_nro_gran_cli;
	private String fa_no_interurb;
	private String fa_no_urb;
	private String fa_no_linea;
	private String fa_no_ident_abo;
	private String fa_re_interurb;
	private String fa_re_urb;
	private String fa_re_linea;
	private String fa_re_ident_abo;
	private String fa_re_cod_postal_loc;
	private String fa_re_cod_postal_loc_benf;
	private String fa_re_fecha_solic_nvo;
	private String fa_re_fecha_emis_op_nvo;
	private String fa_re_fecha_pago_nvo;
	private String fa_re_fecha_gener_cheque_nvo;
	private String fa_re_fecha_para_dolariz_nvo;
	private String fa_re_fecha_alta_nvo;
	private String fa_re_fecha_modific_nvo;
	private String fa_re_fecha_baja_nvo;
	private String fa_re_fecha_superv_nvo;
	private String fa_re_fecha_autoriz_nvo;
	private String fa_re_fecha_pago_misc;
	private String fa_re_origen;
	private String fa_no_fecha_alta_reintegro_nvo;
	private String fa_no_fec_ing_contable_nc_nvo;
	private String fa_no_fec_ing_contable_nd_nvo;
	private String fa_no_fecha_para_dolariz_nvo;
	private String fa_no_fecha_envio_gl_nc_nvo;
	private String fa_no_fecha_envio_gl_nd_nvo;
	private String fa_no_fecha_alta_nc_nvo;
	private String fa_no_fecha_alta_nd_nvo;
	private String fa_no_fecha_mod_nc_nvo;
	private String fa_no_fecha_mod_nd_nvo;
	private String fa_re_imputacion_estado;
	private String fa_re_cod_barra;
	private String fa_re_e_marca_tarea;
	private String fa_re_e_banco;
	private String fa_re_e_imp_pagado;
	private String fa_re_e_fecha_acreditac;
	private String fa_re_e_fecha_proceso;
	private String fa_re_e_sucursal;
	private String fa_re_e_fecha_pres;
	private String fa_re_e_fecha_pago;
	private String fa_re_fecha_bloqueo;
	private String fa_re_usuario_bloqueo;
	private String fa_re_hora_bloqueo;
	private String fa_re_pago_duplicado;
	private String fa_re_pcia_base_perc;
	private String fa_re_nro_movimiento;
	private String fa_re_fecha_desbloqueo;
	private String fa_re_usu_desbloqueo;
	private String fa_re_fecha_migracion;
	private String fa_re_cliente_atis;
	private String fa_re_cuenta_atis;
	private String fa_re_motivo_transf;
	private String fa_re_operador_loc;
	private String fa_re_gp_apynom;
	private String fa_re_gp_tipo_doc;
	private String fa_re_gp_nro_doc;
	private String fa_re_gp_domic;
	private String fa_re_gp_cod_postal;
	private String fa_re_gp_localidad;
	private String fa_re_gp_provincia;
	private String fa_re_numero_pago;
	private String adabas_isn;
	private String fa_co_clave_numero;
	private String fa_co_prox_numero;
	private String fa_co_ident_abo;
	private String fa_re_num_reintegro;
	private String fa_re_zona_solic;
	private String campo_no_usado_1;
	private String fa_re_zona;
	private String fa_re_oficina;
	private String fa_re_abonado;
	private String fa_re_tip_fact;
	private String fa_re_anio_lote;
	private String fa_re_serie_meg;
	private String fa_re_n_contrato_meg;
	private String fa_re_n_cuota_meg;
	private String fa_re_nro_factura;
	private String campo_no_usado_2;
	private String fa_re_motivo;
	
	//AGREGADO DESCRIPCION DEL CAMPO MOTIVO
	private String FA_RE_MOTIVO_DESCRIPCION;
	
	
	private String fa_re_estado;
	// AGREGADO DESCRIPCION DEL CAMPO ESTADO
	private String FA_RE_ESTADO_DESCRIPCION;
	

	
	
	
	private String fa_re_nro_copia;
	
	//AGREGADO DESCRIPCION DEL CAMPO NRO COPIA
	private String FA_RE_NRO_COPIA_DESCRIPCION; 
	
	
	
	
	private String fa_re_importe_tot_concepto;
	private String fa_re_importe_tot_iva;
	private String fa_re_importe_tot_iva_adic;
	private String fa_re_importe_mora;
	private String fa_re_importe_tot_actualiz;
	private String fa_re_importe_tot_ing_b_adic;
	private String fa_re_apell_nom_lin1;
	private String fa_re_an_domic_lin2;
	private String fa_re_domicil_lin3;
	private String fa_re_codpos_loc_lin4;
	private String fa_re_dpto_lin6;
	private String fa_re_categoria;
	private String fa_re_tip_contrib;
	private String fa_re_nro_contrib;
	private String fa_re_apell_nom_lin1_benf;
    //AGREGADO DE LA DESCRIPCION PARA EL CAMPO APELLIDO	
	private String FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION;

	private String fa_re_an_domic_lin2_benf;
	//AGREGADO DE LA DESCRIPCION PARA EL CAMPO DOMICILIO
	private String FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION;
	
	
	
	private String fa_re_domicil_lin3_benf;
	private String fa_re_codpos_loc_lin4_benf;
	private String fa_re_dpto_loc_lin5_benf;
	private String fa_re_tipo_contrib_loc;
	private String fa_re_nro_contrib_loc;
	private String fa_re_tipo_documento;
	private String fa_re_num_documento;
	private String fa_re_n_cuenta_clie_gob;
	private String fa_re_dias_actualiz;
	private String fa_re_valor_pulso;
	private String campo_no_usado_3;
	private String fa_re_zona_pago;
	private String fa_re_cajero;
	
	
	private String fa_re_tipo_pago;
	// AGREGADO DESCRIPCION DEL CAMPO TIPO DE PAGO
	private String FA_RE_TIPO_PAGO_DESCRIPCION;
	
	
	
	
	private String campo_no_usado_4;	
	private String campo_no_usado_5;
	private String fa_re_marca_tarea;
	private String fa_re_nro_orden_pago;
	private String campo_no_usado_6;
	private String fa_re_hora_alta;
	private String fa_re_usuario_alta;
	private String campo_no_usado_7;
	private String fa_re_hora_modific;
	private String fa_re_usuario_modific;
	private String campo_no_usado_8;
	private String fa_re_hora_baja;
	private String fa_re_usuario_baja;
	private String fa_re_usuario_superv;
	private String campo_no_usado_9;
	private String fa_re_hora_superv;
	private String fa_re_usuario_autoriz;
	private String campo_no_usado_10;
	private String fa_re_hora_autoriz;
	private String fa_re_cat_gran_cli;
	private String fa_re_nro_gran_cli;
	private String fa_re_pcia_porc;
	private String fa_re_fecha_impre;
	private String fa_re_porc_iva_rg17;
	private String fa_no_nro_reintegro;
	private String campo_no_usado_11;
	private String fa_no_nro_nc;
	private String campo_no_usado_12;
	private String fa_no_nro_nd;
	private String campo_no_usado_13;
	private String campo_no_usado_14;
	private String fa_no_motivo;
	private String fa_no_zona;
	private String fa_no_oficina;
	private String fa_no_abonado;
	private String fa_no_tipo_fact;
	private String fa_no_ano_lote;
	private String fa_no_serie_meg;
	private String fa_no_n_contrato_meg;
	private String fa_no_n_cuota_meg;
	private String fa_no_categoria;
	private String fa_no_tipo_contrib;
	private String fa_no_tipo_contrib_nvo;
	private String campo_no_usado_15;
	private String campo_no_usado_16;
	private String fa_no_marca_tarea;
	
	/**
	 * AGREGADO DE FECHA_DINAMICA PARA REINTEGROS
	 * @return
	 */
	private String FECHA_PAGO_DINAMICA;
	
	/**
	 * AGREGADO DE FECHA PAGO PRESCRIPTO PARA REINTEGROS
	 * @return
	 */
	private String FA_RE_FECHAPAGO_PRESC;
	
	/**
	 * AGREGADO DE CAMPO BOOLEANO PRESCRIPTO
	 * @return
	 */
	private String PRESCRIPTO;
	
	
	
	

	
	public SfaFisicoReintegrosVO getVO() {
		SfaFisicoReintegrosVO vo = new SfaFisicoReintegrosVO();
				
		vo.setCampo_no_usado_17(Double.valueOf(this.campo_no_usado_17 == null?"0":this.campo_no_usado_17));
		vo.setFa_no_hora_alta_nc(this.fa_no_hora_alta_nc);
		vo.setCampo_no_usado_18(Double.valueOf(this.campo_no_usado_18 == null?"0":this.campo_no_usado_18));
		vo.setFa_no_hora_alta_nd(this.getAsDate(this.fa_no_hora_alta_nd));
		vo.setCampo_no_usado_19(Double.valueOf(this.campo_no_usado_19 == null?"0":this.campo_no_usado_19));
		vo.setFa_no_hora_mod_nc(this.getAsDate(this.fa_no_hora_mod_nc));
		vo.setCampo_no_usado_20(Double.valueOf(this.campo_no_usado_20 == null?"0":this.campo_no_usado_20));
		vo.setFa_no_hora_mod_nd(this.getAsDate(this.fa_no_hora_mod_nd));
		vo.setFa_no_cat_gran_cli(Double.valueOf(this.fa_no_cat_gran_cli == null?"0":this.fa_no_cat_gran_cli));
		vo.setFa_no_nro_gran_cli(Double.valueOf(this.fa_no_nro_gran_cli == null?"0":this.fa_no_nro_gran_cli));
		vo.setFa_no_interurb(this.fa_no_interurb);
		vo.setFa_no_urb(this.fa_no_urb);
		vo.setFa_no_linea(this.fa_no_linea);
		vo.setFa_no_ident_abo(Double.valueOf(this.fa_no_ident_abo == null?"0":this.fa_no_ident_abo));
		vo.setFa_re_interurb(this.fa_re_interurb);
		vo.setFa_re_urb(this.fa_re_urb);
		vo.setFa_re_linea(this.fa_re_linea);
		vo.setFa_re_ident_abo(Double.valueOf(this.fa_re_ident_abo == null?"0":this.fa_re_ident_abo));
		vo.setFa_re_cod_postal_loc(this.fa_re_cod_postal_loc);
		vo.setFa_re_cod_postal_loc_benf(this.fa_re_cod_postal_loc_benf);
		vo.setFa_re_fecha_solic_nvo(this.getAsDate(this.fa_re_fecha_solic_nvo));
		vo.setFa_re_fecha_emis_op_nvo(this.getAsDate(this.fa_re_fecha_emis_op_nvo));
		vo.setFa_re_fecha_pago_nvo(this.fa_re_fecha_pago_nvo);
		vo.setFa_re_fecha_gener_cheque_nvo(this.getAsDate(this.fa_re_fecha_gener_cheque_nvo));
		vo.setFa_re_fecha_para_dolariz_nvo(this.getAsDate(this.fa_re_fecha_para_dolariz_nvo));
		vo.setFa_re_fecha_alta_nvo(this.getAsDate(this.fa_re_fecha_alta_nvo));
		vo.setFa_re_fecha_modific_nvo(this.getAsDate(this.fa_re_fecha_modific_nvo));
		vo.setFa_re_fecha_baja_nvo(this.getAsDate(this.fa_re_fecha_baja_nvo));
		vo.setFa_re_fecha_superv_nvo(this.getAsDate(this.fa_re_fecha_superv_nvo));
		vo.setFa_re_fecha_autoriz_nvo(this.getAsDate(this.fa_re_fecha_autoriz_nvo));
		vo.setFa_re_fecha_pago_misc(this.fa_re_fecha_pago_misc);
		vo.setFa_re_origen(Double.valueOf(this.fa_re_origen == null?"0":this.fa_re_origen));
		vo.setFa_no_fecha_alta_reintegro_nvo(this.getAsDate(this.fa_no_fecha_alta_reintegro_nvo));
		vo.setFa_no_fec_ing_contable_nc_nvo(Double.valueOf(this.fa_no_fec_ing_contable_nc_nvo == null?"0":this.fa_no_fec_ing_contable_nc_nvo));
		vo.setFa_no_fec_ing_contable_nd_nvo(Double.valueOf(this.fa_no_fec_ing_contable_nd_nvo == null?"0":this.fa_no_fec_ing_contable_nd_nvo));
		vo.setFa_no_fecha_para_dolariz_nvo(this.getAsDate(this.fa_no_fecha_para_dolariz_nvo));
		vo.setFa_no_fecha_envio_gl_nc_nvo(this.getAsDate(this.fa_no_fecha_envio_gl_nc_nvo));
		vo.setFa_no_fecha_envio_gl_nd_nvo(this.getAsDate(this.fa_no_fecha_envio_gl_nd_nvo));
		vo.setFa_no_fecha_alta_nc_nvo(this.getAsDate(this.fa_no_fecha_alta_nc_nvo));
		vo.setFa_no_fecha_alta_nd_nvo(this.getAsDate(this.fa_no_fecha_alta_nd_nvo));
		vo.setFa_no_fecha_mod_nc_nvo(this.getAsDate(this.fa_no_fecha_mod_nc_nvo));
		vo.setFa_no_fecha_mod_nd_nvo(this.getAsDate(this.fa_no_fecha_mod_nd_nvo));
		vo.setFa_re_imputacion_estado(Double.valueOf(this.fa_re_imputacion_estado == null?"0":this.fa_re_imputacion_estado));
		vo.setFa_re_cod_barra(this.fa_re_cod_barra);
		vo.setFa_re_e_marca_tarea(Double.valueOf(this.fa_re_e_marca_tarea == null?"0":this.fa_re_e_marca_tarea));
		vo.setFa_re_e_banco(Double.valueOf(this.fa_re_e_banco == null?"0":this.fa_re_e_banco));
		vo.setFa_re_e_imp_pagado(this.fa_re_e_imp_pagado);
		vo.setFa_re_e_fecha_acreditac(this.getAsDate(this.fa_re_e_fecha_acreditac));
		vo.setFa_re_e_fecha_proceso(this.getAsDate(this.fa_re_e_fecha_proceso));
		vo.setFa_re_e_sucursal(Double.valueOf(this.fa_re_e_sucursal == null?"0":this.fa_re_e_sucursal));
		vo.setFa_re_e_fecha_pres(this.getAsDate(this.fa_re_e_fecha_pres));
		vo.setFa_re_e_fecha_pago(this.getAsDate(this.fa_re_e_fecha_pago));
		vo.setFa_re_fecha_bloqueo(this.fa_re_fecha_bloqueo);
		vo.setFa_re_usuario_bloqueo(this.fa_re_usuario_bloqueo);
		vo.setFa_re_hora_bloqueo(this.getAsDate(this.fa_re_hora_bloqueo));
		vo.setFa_re_pago_duplicado(Double.valueOf(this.fa_re_pago_duplicado == null?"0":this.fa_re_pago_duplicado));
		vo.setFa_re_pcia_base_perc(Double.valueOf(this.fa_re_pcia_base_perc == null?"0":this.fa_re_pcia_base_perc));
		vo.setFa_re_nro_movimiento(Double.valueOf(this.fa_re_nro_movimiento == null?"0":this.fa_re_nro_movimiento));
		vo.setFa_re_fecha_desbloqueo(this.getAsDate(this.fa_re_fecha_desbloqueo));
		vo.setFa_re_usu_desbloqueo(this.fa_re_usu_desbloqueo);
		vo.setFa_re_fecha_migracion(this.fa_re_fecha_migracion);
		vo.setFa_re_cliente_atis(this.fa_re_cliente_atis);
		vo.setFa_re_cuenta_atis(this.fa_re_cuenta_atis);
		vo.setFa_re_motivo_transf(this.fa_re_motivo_transf);
		vo.setFa_re_operador_loc(this.fa_re_operador_loc);
		vo.setFa_re_gp_apynom(this.fa_re_gp_apynom);
		vo.setFa_re_gp_tipo_doc(this.fa_re_gp_tipo_doc);
		vo.setFa_re_gp_nro_doc(this.fa_re_gp_nro_doc);
		vo.setFa_re_gp_domic(this.fa_re_gp_domic);
		vo.setFa_re_gp_cod_postal(this.fa_re_gp_cod_postal);
		vo.setFa_re_gp_localidad(this.fa_re_gp_localidad);
		vo.setFa_re_gp_provincia(this.fa_re_gp_provincia);
		vo.setFa_re_numero_pago(this.fa_re_numero_pago);
		vo.setAdabas_isn(Double.valueOf(this.adabas_isn == null?"0":this.adabas_isn));
		vo.setFa_co_clave_numero(this.fa_co_clave_numero);
		vo.setFa_co_prox_numero(Double.valueOf(this.fa_co_prox_numero == null?"0":this.fa_co_prox_numero));
		vo.setFa_co_ident_abo(Double.valueOf(this.fa_co_ident_abo == null?"0":this.fa_co_ident_abo));
		vo.setFa_re_num_reintegro(Double.valueOf(this.fa_re_num_reintegro == null?"0":this.fa_re_num_reintegro));
		vo.setFa_re_zona_solic(Double.valueOf(this.fa_re_zona_solic == null?"0":this.fa_re_zona_solic));
		vo.setCampo_no_usado_1(Double.valueOf(this.campo_no_usado_1 == null?"0":this.campo_no_usado_1));
		vo.setFa_re_zona(Double.valueOf(this.fa_re_zona == null?"0":this.fa_re_zona));
		vo.setFa_re_oficina(Double.valueOf(this.fa_re_oficina == null?"0":this.fa_re_oficina));
		vo.setFa_re_abonado(Double.valueOf(this.fa_re_abonado == null?"0":this.fa_re_abonado));
		vo.setFa_re_tip_fact(Double.valueOf(this.fa_re_tip_fact == null?"0":this.fa_re_tip_fact));
		vo.setFa_re_anio_lote(Double.valueOf(this.fa_re_anio_lote == null?"0":this.fa_re_anio_lote));
		vo.setFa_re_serie_meg(this.fa_re_serie_meg);
		vo.setFa_re_n_contrato_meg(Double.valueOf(this.fa_re_n_contrato_meg == null?"0":this.fa_re_n_contrato_meg));
		vo.setFa_re_n_cuota_meg(Double.valueOf(this.fa_re_n_cuota_meg == null?"0":this.fa_re_n_cuota_meg));
		vo.setFa_re_nro_factura(Double.valueOf(this.fa_re_nro_factura == null?"0":this.fa_re_nro_factura));
		vo.setCampo_no_usado_2(Double.valueOf(this.campo_no_usado_2 == null?"0":this.campo_no_usado_2));
		
		vo.setFa_re_motivo(Double.valueOf(this.fa_re_motivo == null?"0":this.fa_re_motivo));
		// AGREGADO DESCRIPCION DEL CAMPO MOTIVO
		vo.setFA_RE_MOTIVO_DESCRIPCION(this.FA_RE_MOTIVO_DESCRIPCION);
				
		
		vo.setFa_re_estado(Double.valueOf(this.fa_re_estado == null?"0":this.fa_re_estado));
		// AGREAGADO DESCRIPCION DEL CAMPO ESTADO
		vo.setFA_RE_ESTADO_DESCRIPCION(this.FA_RE_ESTADO_DESCRIPCION);
		
		
		vo.setFa_re_nro_copia(Double.valueOf(this.fa_re_nro_copia == null?"0":this.fa_re_nro_copia));
		// AGREGADO DESCRIPCION DEL CAMPO NRO COPIA
		vo.setFA_RE_NRO_COPIA_DESCRIPCION(this.FA_RE_NRO_COPIA_DESCRIPCION);
						
		vo.setFa_re_importe_tot_concepto(new java.math.BigDecimal(this.fa_re_importe_tot_concepto));
		vo.setFa_re_importe_tot_iva(new java.math.BigDecimal(this.fa_re_importe_tot_iva));
		vo.setFa_re_importe_tot_iva_adic(new java.math.BigDecimal(this.fa_re_importe_tot_iva_adic));
		vo.setFa_re_importe_mora(new java.math.BigDecimal(this.fa_re_importe_mora));
		vo.setFa_re_importe_tot_actualiz(new java.math.BigDecimal(this.fa_re_importe_tot_actualiz));
		vo.setFa_re_importe_tot_ing_b_adic(new java.math.BigDecimal(this.fa_re_importe_tot_ing_b_adic));
		vo.setFa_re_apell_nom_lin1(this.fa_re_apell_nom_lin1);
		vo.setFa_re_an_domic_lin2(this.fa_re_an_domic_lin2);
		vo.setFa_re_domicil_lin3(this.fa_re_domicil_lin3);
		vo.setFa_re_codpos_loc_lin4(this.fa_re_codpos_loc_lin4);
		vo.setFa_re_dpto_lin6(this.fa_re_dpto_lin6);
		vo.setFa_re_categoria(Double.valueOf(this.fa_re_categoria == null?"0":this.fa_re_categoria));
		vo.setFa_re_tip_contrib(Double.valueOf(this.fa_re_tip_contrib == null?"0":this.fa_re_tip_contrib));
		vo.setFa_re_nro_contrib(Double.valueOf(this.fa_re_nro_contrib == null?"0":this.fa_re_nro_contrib));
		vo.setFa_re_apell_nom_lin1_benf(this.fa_re_apell_nom_lin1_benf);
		//AGREGADO DE LA DESCRIPCION PARA EL CAMPO APELLIDO
		vo.setFA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION(this.FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION);
		
		
		vo.setFa_re_an_domic_lin2_benf(this.fa_re_an_domic_lin2_benf);
		//AGREGADO DE LA DESCRIPCION PARA EL CAMPO DOMICILIO
		vo.setFA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION(this.FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION);
		
		vo.setFa_re_domicil_lin3_benf(this.fa_re_domicil_lin3_benf);
		vo.setFa_re_codpos_loc_lin4_benf(this.fa_re_codpos_loc_lin4_benf);
		vo.setFa_re_dpto_loc_lin5_benf(this.fa_re_dpto_loc_lin5_benf);
		vo.setFa_re_tipo_contrib_loc(Double.valueOf(this.fa_re_tipo_contrib_loc == null?"0":this.fa_re_tipo_contrib_loc));
		vo.setFa_re_nro_contrib_loc(Double.valueOf(this.fa_re_nro_contrib_loc == null?"0":this.fa_re_nro_contrib_loc));
		vo.setFa_re_tipo_documento(Double.valueOf(this.fa_re_tipo_documento == null?"0":this.fa_re_tipo_documento));
		vo.setFa_re_num_documento(Double.valueOf(this.fa_re_num_documento == null?"0":this.fa_re_num_documento));
		vo.setFa_re_n_cuenta_clie_gob(Double.valueOf(this.fa_re_n_cuenta_clie_gob == null?"0":this.fa_re_n_cuenta_clie_gob));
		vo.setFa_re_dias_actualiz(Double.valueOf(this.fa_re_dias_actualiz == null?"0":this.fa_re_dias_actualiz));
		vo.setFa_re_valor_pulso(Double.valueOf(this.fa_re_valor_pulso == null?"0":this.fa_re_valor_pulso));
		vo.setCampo_no_usado_3(Double.valueOf(this.campo_no_usado_3 == null?"0":this.campo_no_usado_3));
		vo.setFa_re_zona_pago(Double.valueOf(this.fa_re_zona_pago == null?"0":this.fa_re_zona_pago));
		vo.setFa_re_cajero(Double.valueOf(this.fa_re_cajero == null?"0":this.fa_re_cajero));
		
		vo.setFa_re_tipo_pago(Double.valueOf(this.fa_re_tipo_pago == null?"0":this.fa_re_tipo_pago));
		// AGREAGDO DESCRIPCION DEL CAMPO TIPO DE PAGO
		vo.setFA_RE_TIPO_PAGO_DESCRIPCION(this.FA_RE_TIPO_PAGO_DESCRIPCION);
				
		vo.setCampo_no_usado_4(Double.valueOf(this.campo_no_usado_4 == null?"0":this.campo_no_usado_4));
		vo.setCampo_no_usado_5(Double.valueOf(this.campo_no_usado_5 == null?"0":this.campo_no_usado_5));
		vo.setFa_re_marca_tarea(Double.valueOf(this.fa_re_marca_tarea == null?"0":this.fa_re_marca_tarea));
		vo.setFa_re_nro_orden_pago(Double.valueOf(this.fa_re_nro_orden_pago == null?"0":this.fa_re_nro_orden_pago));
		vo.setCampo_no_usado_6(Double.valueOf(this.campo_no_usado_6 == null?"0":this.campo_no_usado_6));
		vo.setFa_re_hora_alta(this.fa_re_hora_alta);
		vo.setFa_re_usuario_alta(this.fa_re_usuario_alta);
		vo.setCampo_no_usado_7(Double.valueOf(this.campo_no_usado_7 == null?"0":this.campo_no_usado_7));
		vo.setFa_re_hora_modific(this.getAsDate(this.fa_re_hora_modific));
		vo.setFa_re_usuario_modific(this.fa_re_usuario_modific);
		vo.setCampo_no_usado_8(Double.valueOf(this.campo_no_usado_8 == null?"0":this.campo_no_usado_8));
		vo.setFa_re_hora_baja(this.getAsDate(this.fa_re_hora_baja));
		vo.setFa_re_usuario_baja(this.fa_re_usuario_baja);
		vo.setFa_re_usuario_superv(this.fa_re_usuario_superv);
		vo.setCampo_no_usado_9(Double.valueOf(this.campo_no_usado_9 == null?"0":this.campo_no_usado_9));
		vo.setFa_re_hora_superv(this.getAsDate(this.fa_re_hora_superv));
		vo.setFa_re_usuario_autoriz(this.fa_re_usuario_autoriz);
		vo.setCampo_no_usado_10(Double.valueOf(this.campo_no_usado_10 == null?"0":this.campo_no_usado_10));
		vo.setFa_re_hora_autoriz(this.fa_re_hora_autoriz);
		vo.setFa_re_cat_gran_cli(Double.valueOf(this.fa_re_cat_gran_cli == null?"0":this.fa_re_cat_gran_cli));
		vo.setFa_re_nro_gran_cli(Double.valueOf(this.fa_re_nro_gran_cli == null?"0":this.fa_re_nro_gran_cli));
		vo.setFa_re_pcia_porc(Double.valueOf(this.fa_re_pcia_porc == null?"0":this.fa_re_pcia_porc));
		vo.setFa_re_fecha_impre(this.fa_re_fecha_impre);
		vo.setFa_re_porc_iva_rg17(Double.valueOf(this.fa_re_porc_iva_rg17 == null?"0":this.fa_re_porc_iva_rg17));
		vo.setFa_no_nro_reintegro(Double.valueOf(this.fa_no_nro_reintegro == null?"0":this.fa_no_nro_reintegro));
		vo.setCampo_no_usado_11(Double.valueOf(this.campo_no_usado_11 == null?"0":this.campo_no_usado_11));
		vo.setFa_no_nro_nc(Double.valueOf(this.fa_no_nro_nc == null?"0":this.fa_no_nro_nc));
		vo.setCampo_no_usado_12(Double.valueOf(this.campo_no_usado_12 == null?"0":this.campo_no_usado_12));
		vo.setFa_no_nro_nd(Double.valueOf(this.fa_no_nro_nd == null?"0":this.fa_no_nro_nd));
		vo.setCampo_no_usado_13(Double.valueOf(this.campo_no_usado_13 == null?"0":this.campo_no_usado_13));
		vo.setCampo_no_usado_14(Double.valueOf(this.campo_no_usado_14 == null?"0":this.campo_no_usado_14));
		vo.setFa_no_motivo(Double.valueOf(this.fa_no_motivo == null?"0":this.fa_no_motivo));
		vo.setFa_no_zona(Double.valueOf(this.fa_no_zona == null?"0":this.fa_no_zona));
		vo.setFa_no_oficina(Double.valueOf(this.fa_no_oficina == null?"0":this.fa_no_oficina));
		vo.setFa_no_abonado(Double.valueOf(this.fa_no_abonado == null?"0":this.fa_no_abonado));
		vo.setFa_no_tipo_fact(Double.valueOf(this.fa_no_tipo_fact == null?"0":this.fa_no_tipo_fact));
		vo.setFa_no_ano_lote(Double.valueOf(this.fa_no_ano_lote == null?"0":this.fa_no_ano_lote));
		vo.setFa_no_serie_meg(this.fa_no_serie_meg);
		vo.setFa_no_n_contrato_meg(Double.valueOf(this.fa_no_n_contrato_meg == null?"0":this.fa_no_n_contrato_meg));
		vo.setFa_no_n_cuota_meg(Double.valueOf(this.fa_no_n_cuota_meg == null?"0":this.fa_no_n_cuota_meg));
		vo.setFa_no_categoria(Double.valueOf(this.fa_no_categoria == null?"0":this.fa_no_categoria));
		vo.setFa_no_tipo_contrib(Double.valueOf(this.fa_no_tipo_contrib == null?"0":this.fa_no_tipo_contrib));
		vo.setFa_no_tipo_contrib_nvo(Double.valueOf(this.fa_no_tipo_contrib_nvo == null?"0":this.fa_no_tipo_contrib_nvo));
		vo.setCampo_no_usado_15(Double.valueOf(this.campo_no_usado_15 == null?"0":this.campo_no_usado_15));
		vo.setCampo_no_usado_16(Double.valueOf(this.campo_no_usado_16 == null?"0":this.campo_no_usado_16));
		vo.setFa_no_marca_tarea(Double.valueOf(this.fa_no_marca_tarea == null?"0":this.fa_no_marca_tarea));
		
		//AGREGADO PARA REINTEGROS
		
		vo.setFECHA_PAGO_DINAMICA(this.FECHA_PAGO_DINAMICA);
		
		
        //AGREGADO PARA REINTEGROS
		
		vo.setFA_RE_FECHAPAGO_PRESC(this.getAsDate(this.FA_RE_FECHAPAGO_PRESC));
		
		//AGREGADO PARA REINTEGROS
		
		vo.setPRESCRIPTO(Boolean.valueOf(this.PRESCRIPTO));
										
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Campo_no_usado_17
	* @return el valor de Campo_no_usado_17
	*/ 
	public String getCampo_no_usado_17() {
		return campo_no_usado_17;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_17
	* @param valor el valor de Campo_no_usado_17
	*/ 
	public void setCampo_no_usado_17(String valor) {
		this.campo_no_usado_17 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_alta_nc
	* @return el valor de Fa_no_hora_alta_nc
	*/ 
	public String getFa_no_hora_alta_nc() {
		return fa_no_hora_alta_nc;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_alta_nc
	* @param valor el valor de Fa_no_hora_alta_nc
	*/ 
	public void setFa_no_hora_alta_nc(String valor) {
		this.fa_no_hora_alta_nc = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_18
	* @return el valor de Campo_no_usado_18
	*/ 
	public String getCampo_no_usado_18() {
		return campo_no_usado_18;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_18
	* @param valor el valor de Campo_no_usado_18
	*/ 
	public void setCampo_no_usado_18(String valor) {
		this.campo_no_usado_18 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_alta_nd
	* @return el valor de Fa_no_hora_alta_nd
	*/ 
	public String getFa_no_hora_alta_nd() {
		return fa_no_hora_alta_nd;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_alta_nd
	* @param valor el valor de Fa_no_hora_alta_nd
	*/ 
	public void setFa_no_hora_alta_nd(String valor) {
		this.fa_no_hora_alta_nd = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_19
	* @return el valor de Campo_no_usado_19
	*/ 
	public String getCampo_no_usado_19() {
		return campo_no_usado_19;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_19
	* @param valor el valor de Campo_no_usado_19
	*/ 
	public void setCampo_no_usado_19(String valor) {
		this.campo_no_usado_19 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_mod_nc
	* @return el valor de Fa_no_hora_mod_nc
	*/ 
	public String getFa_no_hora_mod_nc() {
		return fa_no_hora_mod_nc;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_mod_nc
	* @param valor el valor de Fa_no_hora_mod_nc
	*/ 
	public void setFa_no_hora_mod_nc(String valor) {
		this.fa_no_hora_mod_nc = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_20
	* @return el valor de Campo_no_usado_20
	*/ 
	public String getCampo_no_usado_20() {
		return campo_no_usado_20;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_20
	* @param valor el valor de Campo_no_usado_20
	*/ 
	public void setCampo_no_usado_20(String valor) {
		this.campo_no_usado_20 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_hora_mod_nd
	* @return el valor de Fa_no_hora_mod_nd
	*/ 
	public String getFa_no_hora_mod_nd() {
		return fa_no_hora_mod_nd;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_hora_mod_nd
	* @param valor el valor de Fa_no_hora_mod_nd
	*/ 
	public void setFa_no_hora_mod_nd(String valor) {
		this.fa_no_hora_mod_nd = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_cat_gran_cli
	* @return el valor de Fa_no_cat_gran_cli
	*/ 
	public String getFa_no_cat_gran_cli() {
		return fa_no_cat_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_cat_gran_cli
	* @param valor el valor de Fa_no_cat_gran_cli
	*/ 
	public void setFa_no_cat_gran_cli(String valor) {
		this.fa_no_cat_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_gran_cli
	* @return el valor de Fa_no_nro_gran_cli
	*/ 
	public String getFa_no_nro_gran_cli() {
		return fa_no_nro_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_gran_cli
	* @param valor el valor de Fa_no_nro_gran_cli
	*/ 
	public void setFa_no_nro_gran_cli(String valor) {
		this.fa_no_nro_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_interurb
	* @return el valor de Fa_no_interurb
	*/ 
	public String getFa_no_interurb() {
		return fa_no_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_interurb
	* @param valor el valor de Fa_no_interurb
	*/ 
	public void setFa_no_interurb(String valor) {
		this.fa_no_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_urb
	* @return el valor de Fa_no_urb
	*/ 
	public String getFa_no_urb() {
		return fa_no_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_urb
	* @param valor el valor de Fa_no_urb
	*/ 
	public void setFa_no_urb(String valor) {
		this.fa_no_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_linea
	* @return el valor de Fa_no_linea
	*/ 
	public String getFa_no_linea() {
		return fa_no_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_linea
	* @param valor el valor de Fa_no_linea
	*/ 
	public void setFa_no_linea(String valor) {
		this.fa_no_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_ident_abo
	* @return el valor de Fa_no_ident_abo
	*/ 
	public String getFa_no_ident_abo() {
		return fa_no_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_ident_abo
	* @param valor el valor de Fa_no_ident_abo
	*/ 
	public void setFa_no_ident_abo(String valor) {
		this.fa_no_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_interurb
	* @return el valor de Fa_re_interurb
	*/ 
	public String getFa_re_interurb() {
		return fa_re_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_interurb
	* @param valor el valor de Fa_re_interurb
	*/ 
	public void setFa_re_interurb(String valor) {
		this.fa_re_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_urb
	* @return el valor de Fa_re_urb
	*/ 
	public String getFa_re_urb() {
		return fa_re_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_urb
	* @param valor el valor de Fa_re_urb
	*/ 
	public void setFa_re_urb(String valor) {
		this.fa_re_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_linea
	* @return el valor de Fa_re_linea
	*/ 
	public String getFa_re_linea() {
		return fa_re_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_linea
	* @param valor el valor de Fa_re_linea
	*/ 
	public void setFa_re_linea(String valor) {
		this.fa_re_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_ident_abo
	* @return el valor de Fa_re_ident_abo
	*/ 
	public String getFa_re_ident_abo() {
		return fa_re_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_ident_abo
	* @param valor el valor de Fa_re_ident_abo
	*/ 
	public void setFa_re_ident_abo(String valor) {
		this.fa_re_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cod_postal_loc
	* @return el valor de Fa_re_cod_postal_loc
	*/ 
	public String getFa_re_cod_postal_loc() {
		return fa_re_cod_postal_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cod_postal_loc
	* @param valor el valor de Fa_re_cod_postal_loc
	*/ 
	public void setFa_re_cod_postal_loc(String valor) {
		this.fa_re_cod_postal_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cod_postal_loc_benf
	* @return el valor de Fa_re_cod_postal_loc_benf
	*/ 
	public String getFa_re_cod_postal_loc_benf() {
		return fa_re_cod_postal_loc_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cod_postal_loc_benf
	* @param valor el valor de Fa_re_cod_postal_loc_benf
	*/ 
	public void setFa_re_cod_postal_loc_benf(String valor) {
		this.fa_re_cod_postal_loc_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_solic_nvo
	* @return el valor de Fa_re_fecha_solic_nvo
	*/ 
	public String getFa_re_fecha_solic_nvo() {
		return fa_re_fecha_solic_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_solic_nvo
	* @param valor el valor de Fa_re_fecha_solic_nvo
	*/ 
	public void setFa_re_fecha_solic_nvo(String valor) {
		this.fa_re_fecha_solic_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_emis_op_nvo
	* @return el valor de Fa_re_fecha_emis_op_nvo
	*/ 
	public String getFa_re_fecha_emis_op_nvo() {
		return fa_re_fecha_emis_op_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_emis_op_nvo
	* @param valor el valor de Fa_re_fecha_emis_op_nvo
	*/ 
	public void setFa_re_fecha_emis_op_nvo(String valor) {
		this.fa_re_fecha_emis_op_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_pago_nvo
	* @return el valor de Fa_re_fecha_pago_nvo
	*/ 
	public String getFa_re_fecha_pago_nvo() {
		return fa_re_fecha_pago_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_pago_nvo
	* @param valor el valor de Fa_re_fecha_pago_nvo
	*/ 
	public void setFa_re_fecha_pago_nvo(String valor) {
		this.fa_re_fecha_pago_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_gener_cheque_nvo
	* @return el valor de Fa_re_fecha_gener_cheque_nvo
	*/ 
	public String getFa_re_fecha_gener_cheque_nvo() {
		return fa_re_fecha_gener_cheque_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_gener_cheque_nvo
	* @param valor el valor de Fa_re_fecha_gener_cheque_nvo
	*/ 
	public void setFa_re_fecha_gener_cheque_nvo(String valor) {
		this.fa_re_fecha_gener_cheque_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_para_dolariz_nvo
	* @return el valor de Fa_re_fecha_para_dolariz_nvo
	*/ 
	public String getFa_re_fecha_para_dolariz_nvo() {
		return fa_re_fecha_para_dolariz_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_para_dolariz_nvo
	* @param valor el valor de Fa_re_fecha_para_dolariz_nvo
	*/ 
	public void setFa_re_fecha_para_dolariz_nvo(String valor) {
		this.fa_re_fecha_para_dolariz_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_alta_nvo
	* @return el valor de Fa_re_fecha_alta_nvo
	*/ 
	public String getFa_re_fecha_alta_nvo() {
		return fa_re_fecha_alta_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_alta_nvo
	* @param valor el valor de Fa_re_fecha_alta_nvo
	*/ 
	public void setFa_re_fecha_alta_nvo(String valor) {
		this.fa_re_fecha_alta_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_modific_nvo
	* @return el valor de Fa_re_fecha_modific_nvo
	*/ 
	public String getFa_re_fecha_modific_nvo() {
		return fa_re_fecha_modific_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_modific_nvo
	* @param valor el valor de Fa_re_fecha_modific_nvo
	*/ 
	public void setFa_re_fecha_modific_nvo(String valor) {
		this.fa_re_fecha_modific_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_baja_nvo
	* @return el valor de Fa_re_fecha_baja_nvo
	*/ 
	public String getFa_re_fecha_baja_nvo() {
		return fa_re_fecha_baja_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_baja_nvo
	* @param valor el valor de Fa_re_fecha_baja_nvo
	*/ 
	public void setFa_re_fecha_baja_nvo(String valor) {
		this.fa_re_fecha_baja_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_superv_nvo
	* @return el valor de Fa_re_fecha_superv_nvo
	*/ 
	public String getFa_re_fecha_superv_nvo() {
		return fa_re_fecha_superv_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_superv_nvo
	* @param valor el valor de Fa_re_fecha_superv_nvo
	*/ 
	public void setFa_re_fecha_superv_nvo(String valor) {
		this.fa_re_fecha_superv_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_autoriz_nvo
	* @return el valor de Fa_re_fecha_autoriz_nvo
	*/ 
	public String getFa_re_fecha_autoriz_nvo() {
		return fa_re_fecha_autoriz_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_autoriz_nvo
	* @param valor el valor de Fa_re_fecha_autoriz_nvo
	*/ 
	public void setFa_re_fecha_autoriz_nvo(String valor) {
		this.fa_re_fecha_autoriz_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_pago_misc
	* @return el valor de Fa_re_fecha_pago_misc
	*/ 
	public String getFa_re_fecha_pago_misc() {
		return fa_re_fecha_pago_misc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_pago_misc
	* @param valor el valor de Fa_re_fecha_pago_misc
	*/ 
	public void setFa_re_fecha_pago_misc(String valor) {
		this.fa_re_fecha_pago_misc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_origen
	* @return el valor de Fa_re_origen
	*/ 
	public String getFa_re_origen() {
		return fa_re_origen;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_origen
	* @param valor el valor de Fa_re_origen
	*/ 
	public void setFa_re_origen(String valor) {
		this.fa_re_origen = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_alta_reintegro_nvo
	* @return el valor de Fa_no_fecha_alta_reintegro_nvo
	*/ 
	public String getFa_no_fecha_alta_reintegro_nvo() {
		return fa_no_fecha_alta_reintegro_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_alta_reintegro_nvo
	* @param valor el valor de Fa_no_fecha_alta_reintegro_nvo
	*/ 
	public void setFa_no_fecha_alta_reintegro_nvo(String valor) {
		this.fa_no_fecha_alta_reintegro_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fec_ing_contable_nc_nvo
	* @return el valor de Fa_no_fec_ing_contable_nc_nvo
	*/ 
	public String getFa_no_fec_ing_contable_nc_nvo() {
		return fa_no_fec_ing_contable_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fec_ing_contable_nc_nvo
	* @param valor el valor de Fa_no_fec_ing_contable_nc_nvo
	*/ 
	public void setFa_no_fec_ing_contable_nc_nvo(String valor) {
		this.fa_no_fec_ing_contable_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fec_ing_contable_nd_nvo
	* @return el valor de Fa_no_fec_ing_contable_nd_nvo
	*/ 
	public String getFa_no_fec_ing_contable_nd_nvo() {
		return fa_no_fec_ing_contable_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fec_ing_contable_nd_nvo
	* @param valor el valor de Fa_no_fec_ing_contable_nd_nvo
	*/ 
	public void setFa_no_fec_ing_contable_nd_nvo(String valor) {
		this.fa_no_fec_ing_contable_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_para_dolariz_nvo
	* @return el valor de Fa_no_fecha_para_dolariz_nvo
	*/ 
	public String getFa_no_fecha_para_dolariz_nvo() {
		return fa_no_fecha_para_dolariz_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_para_dolariz_nvo
	* @param valor el valor de Fa_no_fecha_para_dolariz_nvo
	*/ 
	public void setFa_no_fecha_para_dolariz_nvo(String valor) {
		this.fa_no_fecha_para_dolariz_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_envio_gl_nc_nvo
	* @return el valor de Fa_no_fecha_envio_gl_nc_nvo
	*/ 
	public String getFa_no_fecha_envio_gl_nc_nvo() {
		return fa_no_fecha_envio_gl_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_envio_gl_nc_nvo
	* @param valor el valor de Fa_no_fecha_envio_gl_nc_nvo
	*/ 
	public void setFa_no_fecha_envio_gl_nc_nvo(String valor) {
		this.fa_no_fecha_envio_gl_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_envio_gl_nd_nvo
	* @return el valor de Fa_no_fecha_envio_gl_nd_nvo
	*/ 
	public String getFa_no_fecha_envio_gl_nd_nvo() {
		return fa_no_fecha_envio_gl_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_envio_gl_nd_nvo
	* @param valor el valor de Fa_no_fecha_envio_gl_nd_nvo
	*/ 
	public void setFa_no_fecha_envio_gl_nd_nvo(String valor) {
		this.fa_no_fecha_envio_gl_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_alta_nc_nvo
	* @return el valor de Fa_no_fecha_alta_nc_nvo
	*/ 
	public String getFa_no_fecha_alta_nc_nvo() {
		return fa_no_fecha_alta_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_alta_nc_nvo
	* @param valor el valor de Fa_no_fecha_alta_nc_nvo
	*/ 
	public void setFa_no_fecha_alta_nc_nvo(String valor) {
		this.fa_no_fecha_alta_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_alta_nd_nvo
	* @return el valor de Fa_no_fecha_alta_nd_nvo
	*/ 
	public String getFa_no_fecha_alta_nd_nvo() {
		return fa_no_fecha_alta_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_alta_nd_nvo
	* @param valor el valor de Fa_no_fecha_alta_nd_nvo
	*/ 
	public void setFa_no_fecha_alta_nd_nvo(String valor) {
		this.fa_no_fecha_alta_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_mod_nc_nvo
	* @return el valor de Fa_no_fecha_mod_nc_nvo
	*/ 
	public String getFa_no_fecha_mod_nc_nvo() {
		return fa_no_fecha_mod_nc_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_mod_nc_nvo
	* @param valor el valor de Fa_no_fecha_mod_nc_nvo
	*/ 
	public void setFa_no_fecha_mod_nc_nvo(String valor) {
		this.fa_no_fecha_mod_nc_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_fecha_mod_nd_nvo
	* @return el valor de Fa_no_fecha_mod_nd_nvo
	*/ 
	public String getFa_no_fecha_mod_nd_nvo() {
		return fa_no_fecha_mod_nd_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_fecha_mod_nd_nvo
	* @param valor el valor de Fa_no_fecha_mod_nd_nvo
	*/ 
	public void setFa_no_fecha_mod_nd_nvo(String valor) {
		this.fa_no_fecha_mod_nd_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_imputacion_estado
	* @return el valor de Fa_re_imputacion_estado
	*/ 
	public String getFa_re_imputacion_estado() {
		return fa_re_imputacion_estado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_imputacion_estado
	* @param valor el valor de Fa_re_imputacion_estado
	*/ 
	public void setFa_re_imputacion_estado(String valor) {
		this.fa_re_imputacion_estado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cod_barra
	* @return el valor de Fa_re_cod_barra
	*/ 
	public String getFa_re_cod_barra() {
		return fa_re_cod_barra;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cod_barra
	* @param valor el valor de Fa_re_cod_barra
	*/ 
	public void setFa_re_cod_barra(String valor) {
		this.fa_re_cod_barra = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_marca_tarea
	* @return el valor de Fa_re_e_marca_tarea
	*/ 
	public String getFa_re_e_marca_tarea() {
		return fa_re_e_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_marca_tarea
	* @param valor el valor de Fa_re_e_marca_tarea
	*/ 
	public void setFa_re_e_marca_tarea(String valor) {
		this.fa_re_e_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_banco
	* @return el valor de Fa_re_e_banco
	*/ 
	public String getFa_re_e_banco() {
		return fa_re_e_banco;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_banco
	* @param valor el valor de Fa_re_e_banco
	*/ 
	public void setFa_re_e_banco(String valor) {
		this.fa_re_e_banco = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_imp_pagado
	* @return el valor de Fa_re_e_imp_pagado
	*/ 
	public String getFa_re_e_imp_pagado() {
		return fa_re_e_imp_pagado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_imp_pagado
	* @param valor el valor de Fa_re_e_imp_pagado
	*/ 
	public void setFa_re_e_imp_pagado(String valor) {
		this.fa_re_e_imp_pagado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_acreditac
	* @return el valor de Fa_re_e_fecha_acreditac
	*/ 
	public String getFa_re_e_fecha_acreditac() {
		return fa_re_e_fecha_acreditac;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_acreditac
	* @param valor el valor de Fa_re_e_fecha_acreditac
	*/ 
	public void setFa_re_e_fecha_acreditac(String valor) {
		this.fa_re_e_fecha_acreditac = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_proceso
	* @return el valor de Fa_re_e_fecha_proceso
	*/ 
	public String getFa_re_e_fecha_proceso() {
		return fa_re_e_fecha_proceso;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_proceso
	* @param valor el valor de Fa_re_e_fecha_proceso
	*/ 
	public void setFa_re_e_fecha_proceso(String valor) {
		this.fa_re_e_fecha_proceso = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_sucursal
	* @return el valor de Fa_re_e_sucursal
	*/ 
	public String getFa_re_e_sucursal() {
		return fa_re_e_sucursal;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_sucursal
	* @param valor el valor de Fa_re_e_sucursal
	*/ 
	public void setFa_re_e_sucursal(String valor) {
		this.fa_re_e_sucursal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_pres
	* @return el valor de Fa_re_e_fecha_pres
	*/ 
	public String getFa_re_e_fecha_pres() {
		return fa_re_e_fecha_pres;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_pres
	* @param valor el valor de Fa_re_e_fecha_pres
	*/ 
	public void setFa_re_e_fecha_pres(String valor) {
		this.fa_re_e_fecha_pres = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_e_fecha_pago
	* @return el valor de Fa_re_e_fecha_pago
	*/ 
	public String getFa_re_e_fecha_pago() {
		return fa_re_e_fecha_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_e_fecha_pago
	* @param valor el valor de Fa_re_e_fecha_pago
	*/ 
	public void setFa_re_e_fecha_pago(String valor) {
		this.fa_re_e_fecha_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_bloqueo
	* @return el valor de Fa_re_fecha_bloqueo
	*/ 
	public String getFa_re_fecha_bloqueo() {
		return fa_re_fecha_bloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_bloqueo
	* @param valor el valor de Fa_re_fecha_bloqueo
	*/ 
	public void setFa_re_fecha_bloqueo(String valor) {
		this.fa_re_fecha_bloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_bloqueo
	* @return el valor de Fa_re_usuario_bloqueo
	*/ 
	public String getFa_re_usuario_bloqueo() {
		return fa_re_usuario_bloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_bloqueo
	* @param valor el valor de Fa_re_usuario_bloqueo
	*/ 
	public void setFa_re_usuario_bloqueo(String valor) {
		this.fa_re_usuario_bloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_bloqueo
	* @return el valor de Fa_re_hora_bloqueo
	*/ 
	public String getFa_re_hora_bloqueo() {
		return fa_re_hora_bloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_bloqueo
	* @param valor el valor de Fa_re_hora_bloqueo
	*/ 
	public void setFa_re_hora_bloqueo(String valor) {
		this.fa_re_hora_bloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_pago_duplicado
	* @return el valor de Fa_re_pago_duplicado
	*/ 
	public String getFa_re_pago_duplicado() {
		return fa_re_pago_duplicado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_pago_duplicado
	* @param valor el valor de Fa_re_pago_duplicado
	*/ 
	public void setFa_re_pago_duplicado(String valor) {
		this.fa_re_pago_duplicado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_pcia_base_perc
	* @return el valor de Fa_re_pcia_base_perc
	*/ 
	public String getFa_re_pcia_base_perc() {
		return fa_re_pcia_base_perc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_pcia_base_perc
	* @param valor el valor de Fa_re_pcia_base_perc
	*/ 
	public void setFa_re_pcia_base_perc(String valor) {
		this.fa_re_pcia_base_perc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_movimiento
	* @return el valor de Fa_re_nro_movimiento
	*/ 
	public String getFa_re_nro_movimiento() {
		return fa_re_nro_movimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_movimiento
	* @param valor el valor de Fa_re_nro_movimiento
	*/ 
	public void setFa_re_nro_movimiento(String valor) {
		this.fa_re_nro_movimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_desbloqueo
	* @return el valor de Fa_re_fecha_desbloqueo
	*/ 
	public String getFa_re_fecha_desbloqueo() {
		return fa_re_fecha_desbloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_desbloqueo
	* @param valor el valor de Fa_re_fecha_desbloqueo
	*/ 
	public void setFa_re_fecha_desbloqueo(String valor) {
		this.fa_re_fecha_desbloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usu_desbloqueo
	* @return el valor de Fa_re_usu_desbloqueo
	*/ 
	public String getFa_re_usu_desbloqueo() {
		return fa_re_usu_desbloqueo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usu_desbloqueo
	* @param valor el valor de Fa_re_usu_desbloqueo
	*/ 
	public void setFa_re_usu_desbloqueo(String valor) {
		this.fa_re_usu_desbloqueo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_migracion
	* @return el valor de Fa_re_fecha_migracion
	*/ 
	public String getFa_re_fecha_migracion() {
		return fa_re_fecha_migracion;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_migracion
	* @param valor el valor de Fa_re_fecha_migracion
	*/ 
	public void setFa_re_fecha_migracion(String valor) {
		this.fa_re_fecha_migracion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cliente_atis
	* @return el valor de Fa_re_cliente_atis
	*/ 
	public String getFa_re_cliente_atis() {
		return fa_re_cliente_atis;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cliente_atis
	* @param valor el valor de Fa_re_cliente_atis
	*/ 
	public void setFa_re_cliente_atis(String valor) {
		this.fa_re_cliente_atis = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cuenta_atis
	* @return el valor de Fa_re_cuenta_atis
	*/ 
	public String getFa_re_cuenta_atis() {
		return fa_re_cuenta_atis;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cuenta_atis
	* @param valor el valor de Fa_re_cuenta_atis
	*/ 
	public void setFa_re_cuenta_atis(String valor) {
		this.fa_re_cuenta_atis = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_motivo_transf
	* @return el valor de Fa_re_motivo_transf
	*/ 
	public String getFa_re_motivo_transf() {
		return fa_re_motivo_transf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_motivo_transf
	* @param valor el valor de Fa_re_motivo_transf
	*/ 
	public void setFa_re_motivo_transf(String valor) {
		this.fa_re_motivo_transf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_operador_loc
	* @return el valor de Fa_re_operador_loc
	*/ 
	public String getFa_re_operador_loc() {
		return fa_re_operador_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_operador_loc
	* @param valor el valor de Fa_re_operador_loc
	*/ 
	public void setFa_re_operador_loc(String valor) {
		this.fa_re_operador_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_apynom
	* @return el valor de Fa_re_gp_apynom
	*/ 
	public String getFa_re_gp_apynom() {
		return fa_re_gp_apynom;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_apynom
	* @param valor el valor de Fa_re_gp_apynom
	*/ 
	public void setFa_re_gp_apynom(String valor) {
		this.fa_re_gp_apynom = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_tipo_doc
	* @return el valor de Fa_re_gp_tipo_doc
	*/ 
	public String getFa_re_gp_tipo_doc() {
		return fa_re_gp_tipo_doc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_tipo_doc
	* @param valor el valor de Fa_re_gp_tipo_doc
	*/ 
	public void setFa_re_gp_tipo_doc(String valor) {
		this.fa_re_gp_tipo_doc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_nro_doc
	* @return el valor de Fa_re_gp_nro_doc
	*/ 
	public String getFa_re_gp_nro_doc() {
		return fa_re_gp_nro_doc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_nro_doc
	* @param valor el valor de Fa_re_gp_nro_doc
	*/ 
	public void setFa_re_gp_nro_doc(String valor) {
		this.fa_re_gp_nro_doc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_domic
	* @return el valor de Fa_re_gp_domic
	*/ 
	public String getFa_re_gp_domic() {
		return fa_re_gp_domic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_domic
	* @param valor el valor de Fa_re_gp_domic
	*/ 
	public void setFa_re_gp_domic(String valor) {
		this.fa_re_gp_domic = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_cod_postal
	* @return el valor de Fa_re_gp_cod_postal
	*/ 
	public String getFa_re_gp_cod_postal() {
		return fa_re_gp_cod_postal;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_cod_postal
	* @param valor el valor de Fa_re_gp_cod_postal
	*/ 
	public void setFa_re_gp_cod_postal(String valor) {
		this.fa_re_gp_cod_postal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_localidad
	* @return el valor de Fa_re_gp_localidad
	*/ 
	public String getFa_re_gp_localidad() {
		return fa_re_gp_localidad;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_localidad
	* @param valor el valor de Fa_re_gp_localidad
	*/ 
	public void setFa_re_gp_localidad(String valor) {
		this.fa_re_gp_localidad = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_gp_provincia
	* @return el valor de Fa_re_gp_provincia
	*/ 
	public String getFa_re_gp_provincia() {
		return fa_re_gp_provincia;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_gp_provincia
	* @param valor el valor de Fa_re_gp_provincia
	*/ 
	public void setFa_re_gp_provincia(String valor) {
		this.fa_re_gp_provincia = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_numero_pago
	* @return el valor de Fa_re_numero_pago
	*/ 
	public String getFa_re_numero_pago() {
		return fa_re_numero_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_numero_pago
	* @param valor el valor de Fa_re_numero_pago
	*/ 
	public void setFa_re_numero_pago(String valor) {
		this.fa_re_numero_pago = valor;
	}
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(String valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_co_clave_numero
	* @return el valor de Fa_co_clave_numero
	*/ 
	public String getFa_co_clave_numero() {
		return fa_co_clave_numero;
	}
    	
	/**
	* Fija el valor del atributo Fa_co_clave_numero
	* @param valor el valor de Fa_co_clave_numero
	*/ 
	public void setFa_co_clave_numero(String valor) {
		this.fa_co_clave_numero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_co_prox_numero
	* @return el valor de Fa_co_prox_numero
	*/ 
	public String getFa_co_prox_numero() {
		return fa_co_prox_numero;
	}
    	
	/**
	* Fija el valor del atributo Fa_co_prox_numero
	* @param valor el valor de Fa_co_prox_numero
	*/ 
	public void setFa_co_prox_numero(String valor) {
		this.fa_co_prox_numero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_co_ident_abo
	* @return el valor de Fa_co_ident_abo
	*/ 
	public String getFa_co_ident_abo() {
		return fa_co_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_co_ident_abo
	* @param valor el valor de Fa_co_ident_abo
	*/ 
	public void setFa_co_ident_abo(String valor) {
		this.fa_co_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_num_reintegro
	* @return el valor de Fa_re_num_reintegro
	*/ 
	public String getFa_re_num_reintegro() {
		return fa_re_num_reintegro;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_num_reintegro
	* @param valor el valor de Fa_re_num_reintegro
	*/ 
	public void setFa_re_num_reintegro(String valor) {
		this.fa_re_num_reintegro = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_zona_solic
	* @return el valor de Fa_re_zona_solic
	*/ 
	public String getFa_re_zona_solic() {
		return fa_re_zona_solic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_zona_solic
	* @param valor el valor de Fa_re_zona_solic
	*/ 
	public void setFa_re_zona_solic(String valor) {
		this.fa_re_zona_solic = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_1
	* @return el valor de Campo_no_usado_1
	*/ 
	public String getCampo_no_usado_1() {
		return campo_no_usado_1;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_1
	* @param valor el valor de Campo_no_usado_1
	*/ 
	public void setCampo_no_usado_1(String valor) {
		this.campo_no_usado_1 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_zona
	* @return el valor de Fa_re_zona
	*/ 
	public String getFa_re_zona() {
		return fa_re_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_zona
	* @param valor el valor de Fa_re_zona
	*/ 
	public void setFa_re_zona(String valor) {
		this.fa_re_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_oficina
	* @return el valor de Fa_re_oficina
	*/ 
	public String getFa_re_oficina() {
		return fa_re_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_oficina
	* @param valor el valor de Fa_re_oficina
	*/ 
	public void setFa_re_oficina(String valor) {
		this.fa_re_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_abonado
	* @return el valor de Fa_re_abonado
	*/ 
	public String getFa_re_abonado() {
		return fa_re_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_abonado
	* @param valor el valor de Fa_re_abonado
	*/ 
	public void setFa_re_abonado(String valor) {
		this.fa_re_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tip_fact
	* @return el valor de Fa_re_tip_fact
	*/ 
	public String getFa_re_tip_fact() {
		return fa_re_tip_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tip_fact
	* @param valor el valor de Fa_re_tip_fact
	*/ 
	public void setFa_re_tip_fact(String valor) {
		this.fa_re_tip_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_anio_lote
	* @return el valor de Fa_re_anio_lote
	*/ 
	public String getFa_re_anio_lote() {
		return fa_re_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_anio_lote
	* @param valor el valor de Fa_re_anio_lote
	*/ 
	public void setFa_re_anio_lote(String valor) {
		this.fa_re_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_serie_meg
	* @return el valor de Fa_re_serie_meg
	*/ 
	public String getFa_re_serie_meg() {
		return fa_re_serie_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_serie_meg
	* @param valor el valor de Fa_re_serie_meg
	*/ 
	public void setFa_re_serie_meg(String valor) {
		this.fa_re_serie_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_n_contrato_meg
	* @return el valor de Fa_re_n_contrato_meg
	*/ 
	public String getFa_re_n_contrato_meg() {
		return fa_re_n_contrato_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_n_contrato_meg
	* @param valor el valor de Fa_re_n_contrato_meg
	*/ 
	public void setFa_re_n_contrato_meg(String valor) {
		this.fa_re_n_contrato_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_n_cuota_meg
	* @return el valor de Fa_re_n_cuota_meg
	*/ 
	public String getFa_re_n_cuota_meg() {
		return fa_re_n_cuota_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_n_cuota_meg
	* @param valor el valor de Fa_re_n_cuota_meg
	*/ 
	public void setFa_re_n_cuota_meg(String valor) {
		this.fa_re_n_cuota_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_factura
	* @return el valor de Fa_re_nro_factura
	*/ 
	public String getFa_re_nro_factura() {
		return fa_re_nro_factura;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_factura
	* @param valor el valor de Fa_re_nro_factura
	*/ 
	public void setFa_re_nro_factura(String valor) {
		this.fa_re_nro_factura = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_2
	* @return el valor de Campo_no_usado_2
	*/ 
	public String getCampo_no_usado_2() {
		return campo_no_usado_2;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_2
	* @param valor el valor de Campo_no_usado_2
	*/ 
	public void setCampo_no_usado_2(String valor) {
		this.campo_no_usado_2 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_motivo
	* @return el valor de Fa_re_motivo
	*/ 
	public String getFa_re_motivo() {
		return fa_re_motivo;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_motivo
	* @param valor el valor de Fa_re_motivo
	*/ 
	public void setFa_re_motivo(String valor) {
		this.fa_re_motivo = valor;
	}
	
	// AGREGADO DESCRIPCION DEL CAMPO MOTIVO
	/**
	* Recupera el valor del atributo FA_RE_MOTIVO_DESCRIPCION
	* @return el valor de FA_RE_MOTIVO_DESCRIPCION
	*/ 
	public String getFA_RE_MOTIVO_DESCRIPCION() {
		return FA_RE_MOTIVO_DESCRIPCION;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_MOTIVO_DESCRIPCION
	* @param valor el valor de FA_RE_MOTIVO_DESCRIPCION
	*/ 
	public void setFA_RE_MOTIVO_DESCRIPCION(String valor) {
		this.FA_RE_MOTIVO_DESCRIPCION = valor;
	}
	
	
	/**
	* Recupera el valor del atributo Fa_re_estado
	* @return el valor de Fa_re_estado
	*/ 
	public String getFa_re_estado() {
		return fa_re_estado;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_estado
	* @param valor el valor de Fa_re_estado
	*/ 
	public void setFa_re_estado(String valor) {
		this.fa_re_estado = valor;
	}
	
	// AGREGADO DESCRIPCION DEL CAMPO ESTADO	
	/**
	* Recupera el valor del atributo FA_RE_ESTADO_DESCRIPCION
	* @return el valor de FA_RE_ESTADO_DESCRIPCION
	*/ 
	public String getFA_RE_ESTADO_DESCRIPCION() {
		return FA_RE_ESTADO_DESCRIPCION;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_ESTADO_DESCRIPCION
	* @param valor el valor de FA_RE_ESTADO_DESCRIPCION
	*/ 
	public void setFA_RE_ESTADO_DESCRIPCION(String valor) {
		this.FA_RE_ESTADO_DESCRIPCION = valor;
	}
	
	
	/**
	* Recupera el valor del atributo Fa_re_nro_copia
	* @return el valor de Fa_re_nro_copia
	*/ 
	public String getFa_re_nro_copia() {
		return fa_re_nro_copia;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_copia
	* @param valor el valor de Fa_re_nro_copia
	*/ 
	public void setFa_re_nro_copia(String valor) {
		this.fa_re_nro_copia = valor;
	}
	
	// AGREGADO DE DESCRIPCION DEL CAMPO NRO COPIA
	/**
	* Recupera el valor del atributo FA_RE_NRO_COPIA_DESCRIPCION
	* @return el valor de FA_RE_NRO_COPIA_DESCRIPCION
	*/ 
	public String getFA_RE_NRO_COPIA_DESCRIPCION() {
		return FA_RE_NRO_COPIA_DESCRIPCION;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_NRO_COPIA_DESCRIPCION
	* @param valor el valor de FA_RE_NRO_COPIA_DESCRIPCION
	*/ 
	public void setFA_RE_NRO_COPIA_DESCRIPCION(String valor) {
		this.FA_RE_NRO_COPIA_DESCRIPCION = valor;
	}
	
	

	/**
	* Recupera el valor del atributo Fa_re_importe_tot_concepto
	* @return el valor de Fa_re_importe_tot_concepto
	*/ 
	public String getFa_re_importe_tot_concepto() {
		return fa_re_importe_tot_concepto;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_concepto
	* @param valor el valor de Fa_re_importe_tot_concepto
	*/ 
	public void setFa_re_importe_tot_concepto(String valor) {
		this.fa_re_importe_tot_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_iva
	* @return el valor de Fa_re_importe_tot_iva
	*/ 
	public String getFa_re_importe_tot_iva() {
		return fa_re_importe_tot_iva;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_iva
	* @param valor el valor de Fa_re_importe_tot_iva
	*/ 
	public void setFa_re_importe_tot_iva(String valor) {
		this.fa_re_importe_tot_iva = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_iva_adic
	* @return el valor de Fa_re_importe_tot_iva_adic
	*/ 
	public String getFa_re_importe_tot_iva_adic() {
		return fa_re_importe_tot_iva_adic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_iva_adic
	* @param valor el valor de Fa_re_importe_tot_iva_adic
	*/ 
	public void setFa_re_importe_tot_iva_adic(String valor) {
		this.fa_re_importe_tot_iva_adic = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_mora
	* @return el valor de Fa_re_importe_mora
	*/ 
	public String getFa_re_importe_mora() {
		return fa_re_importe_mora;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_mora
	* @param valor el valor de Fa_re_importe_mora
	*/ 
	public void setFa_re_importe_mora(String valor) {
		this.fa_re_importe_mora = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_actualiz
	* @return el valor de Fa_re_importe_tot_actualiz
	*/ 
	public String getFa_re_importe_tot_actualiz() {
		return fa_re_importe_tot_actualiz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_actualiz
	* @param valor el valor de Fa_re_importe_tot_actualiz
	*/ 
	public void setFa_re_importe_tot_actualiz(String valor) {
		this.fa_re_importe_tot_actualiz = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_importe_tot_ing_b_adic
	* @return el valor de Fa_re_importe_tot_ing_b_adic
	*/ 
	public String getFa_re_importe_tot_ing_b_adic() {
		return fa_re_importe_tot_ing_b_adic;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_importe_tot_ing_b_adic
	* @param valor el valor de Fa_re_importe_tot_ing_b_adic
	*/ 
	public void setFa_re_importe_tot_ing_b_adic(String valor) {
		this.fa_re_importe_tot_ing_b_adic = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_apell_nom_lin1
	* @return el valor de Fa_re_apell_nom_lin1
	*/ 
	public String getFa_re_apell_nom_lin1() {
		return fa_re_apell_nom_lin1;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_apell_nom_lin1
	* @param valor el valor de Fa_re_apell_nom_lin1
	*/ 
	public void setFa_re_apell_nom_lin1(String valor) {
		this.fa_re_apell_nom_lin1 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_an_domic_lin2
	* @return el valor de Fa_re_an_domic_lin2
	*/ 
	public String getFa_re_an_domic_lin2() {
		return fa_re_an_domic_lin2;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_an_domic_lin2
	* @param valor el valor de Fa_re_an_domic_lin2
	*/ 
	public void setFa_re_an_domic_lin2(String valor) {
		this.fa_re_an_domic_lin2 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_domicil_lin3
	* @return el valor de Fa_re_domicil_lin3
	*/ 
	public String getFa_re_domicil_lin3() {
		return fa_re_domicil_lin3;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_domicil_lin3
	* @param valor el valor de Fa_re_domicil_lin3
	*/ 
	public void setFa_re_domicil_lin3(String valor) {
		this.fa_re_domicil_lin3 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_codpos_loc_lin4
	* @return el valor de Fa_re_codpos_loc_lin4
	*/ 
	public String getFa_re_codpos_loc_lin4() {
		return fa_re_codpos_loc_lin4;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_codpos_loc_lin4
	* @param valor el valor de Fa_re_codpos_loc_lin4
	*/ 
	public void setFa_re_codpos_loc_lin4(String valor) {
		this.fa_re_codpos_loc_lin4 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_dpto_lin6
	* @return el valor de Fa_re_dpto_lin6
	*/ 
	public String getFa_re_dpto_lin6() {
		return fa_re_dpto_lin6;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_dpto_lin6
	* @param valor el valor de Fa_re_dpto_lin6
	*/ 
	public void setFa_re_dpto_lin6(String valor) {
		this.fa_re_dpto_lin6 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_categoria
	* @return el valor de Fa_re_categoria
	*/ 
	public String getFa_re_categoria() {
		return fa_re_categoria;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_categoria
	* @param valor el valor de Fa_re_categoria
	*/ 
	public void setFa_re_categoria(String valor) {
		this.fa_re_categoria = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tip_contrib
	* @return el valor de Fa_re_tip_contrib
	*/ 
	public String getFa_re_tip_contrib() {
		return fa_re_tip_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tip_contrib
	* @param valor el valor de Fa_re_tip_contrib
	*/ 
	public void setFa_re_tip_contrib(String valor) {
		this.fa_re_tip_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_contrib
	* @return el valor de Fa_re_nro_contrib
	*/ 
	public String getFa_re_nro_contrib() {
		return fa_re_nro_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_contrib
	* @param valor el valor de Fa_re_nro_contrib
	*/ 
	public void setFa_re_nro_contrib(String valor) {
		this.fa_re_nro_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_apell_nom_lin1_benf
	* @return el valor de Fa_re_apell_nom_lin1_benf
	*/ 
	public String getFa_re_apell_nom_lin1_benf() {
		return fa_re_apell_nom_lin1_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_apell_nom_lin1_benf
	* @param valor el valor de Fa_re_apell_nom_lin1_benf
	*/ 
	public void setFa_re_apell_nom_lin1_benf(String valor) {
		this.fa_re_apell_nom_lin1_benf = valor;
	}
	
	
	
	//AGREGADO DESCRIPCION DEL CAMPO APELLIDO
	
	/**
	* Recupera el valor del atributo FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	* @return el valor de FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	*/ 
	public String getFA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION() {
		return FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	* @param valor el valor de FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION
	*/ 
	public void setFA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION(String valor) {
		this.FA_RE_APELL_NOM_LIN1_BENF_DESCRIPCION = valor;
	}
	
	
	
	/**
	* Recupera el valor del atributo Fa_re_an_domic_lin2_benf
	* @return el valor de Fa_re_an_domic_lin2_benf
	*/ 
	public String getFa_re_an_domic_lin2_benf() {
		return fa_re_an_domic_lin2_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_an_domic_lin2_benf
	* @param valor el valor de Fa_re_an_domic_lin2_benf
	*/ 
	public void setFa_re_an_domic_lin2_benf(String valor) {
		this.fa_re_an_domic_lin2_benf = valor;
	}
	
	//AGREGADO DESCRIPCION DEL CAMPO APELLIDO
	
	/**
	* Recupera el valor del atributo FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	* @return el valor de FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	*/ 
	public String getFA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION() {
		return FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	* @param valor el valor de FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION
	*/ 
	public void setFA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION(String valor) {
		this.FA_RE_AN_DOMIC_LIN2_BENF_DESCRIPCION = valor;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	* Recupera el valor del atributo Fa_re_domicil_lin3_benf
	* @return el valor de Fa_re_domicil_lin3_benf
	*/ 
	public String getFa_re_domicil_lin3_benf() {
		return fa_re_domicil_lin3_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_domicil_lin3_benf
	* @param valor el valor de Fa_re_domicil_lin3_benf
	*/ 
	public void setFa_re_domicil_lin3_benf(String valor) {
		this.fa_re_domicil_lin3_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_codpos_loc_lin4_benf
	* @return el valor de Fa_re_codpos_loc_lin4_benf
	*/ 
	public String getFa_re_codpos_loc_lin4_benf() {
		return fa_re_codpos_loc_lin4_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_codpos_loc_lin4_benf
	* @param valor el valor de Fa_re_codpos_loc_lin4_benf
	*/ 
	public void setFa_re_codpos_loc_lin4_benf(String valor) {
		this.fa_re_codpos_loc_lin4_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_dpto_loc_lin5_benf
	* @return el valor de Fa_re_dpto_loc_lin5_benf
	*/ 
	public String getFa_re_dpto_loc_lin5_benf() {
		return fa_re_dpto_loc_lin5_benf;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_dpto_loc_lin5_benf
	* @param valor el valor de Fa_re_dpto_loc_lin5_benf
	*/ 
	public void setFa_re_dpto_loc_lin5_benf(String valor) {
		this.fa_re_dpto_loc_lin5_benf = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tipo_contrib_loc
	* @return el valor de Fa_re_tipo_contrib_loc
	*/ 
	public String getFa_re_tipo_contrib_loc() {
		return fa_re_tipo_contrib_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tipo_contrib_loc
	* @param valor el valor de Fa_re_tipo_contrib_loc
	*/ 
	public void setFa_re_tipo_contrib_loc(String valor) {
		this.fa_re_tipo_contrib_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_contrib_loc
	* @return el valor de Fa_re_nro_contrib_loc
	*/ 
	public String getFa_re_nro_contrib_loc() {
		return fa_re_nro_contrib_loc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_contrib_loc
	* @param valor el valor de Fa_re_nro_contrib_loc
	*/ 
	public void setFa_re_nro_contrib_loc(String valor) {
		this.fa_re_nro_contrib_loc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tipo_documento
	* @return el valor de Fa_re_tipo_documento
	*/ 
	public String getFa_re_tipo_documento() {
		return fa_re_tipo_documento;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tipo_documento
	* @param valor el valor de Fa_re_tipo_documento
	*/ 
	public void setFa_re_tipo_documento(String valor) {
		this.fa_re_tipo_documento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_num_documento
	* @return el valor de Fa_re_num_documento
	*/ 
	public String getFa_re_num_documento() {
		return fa_re_num_documento;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_num_documento
	* @param valor el valor de Fa_re_num_documento
	*/ 
	public void setFa_re_num_documento(String valor) {
		this.fa_re_num_documento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_n_cuenta_clie_gob
	* @return el valor de Fa_re_n_cuenta_clie_gob
	*/ 
	public String getFa_re_n_cuenta_clie_gob() {
		return fa_re_n_cuenta_clie_gob;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_n_cuenta_clie_gob
	* @param valor el valor de Fa_re_n_cuenta_clie_gob
	*/ 
	public void setFa_re_n_cuenta_clie_gob(String valor) {
		this.fa_re_n_cuenta_clie_gob = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_dias_actualiz
	* @return el valor de Fa_re_dias_actualiz
	*/ 
	public String getFa_re_dias_actualiz() {
		return fa_re_dias_actualiz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_dias_actualiz
	* @param valor el valor de Fa_re_dias_actualiz
	*/ 
	public void setFa_re_dias_actualiz(String valor) {
		this.fa_re_dias_actualiz = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_valor_pulso
	* @return el valor de Fa_re_valor_pulso
	*/ 
	public String getFa_re_valor_pulso() {
		return fa_re_valor_pulso;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_valor_pulso
	* @param valor el valor de Fa_re_valor_pulso
	*/ 
	public void setFa_re_valor_pulso(String valor) {
		this.fa_re_valor_pulso = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_3
	* @return el valor de Campo_no_usado_3
	*/ 
	public String getCampo_no_usado_3() {
		return campo_no_usado_3;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_3
	* @param valor el valor de Campo_no_usado_3
	*/ 
	public void setCampo_no_usado_3(String valor) {
		this.campo_no_usado_3 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_zona_pago
	* @return el valor de Fa_re_zona_pago
	*/ 
	public String getFa_re_zona_pago() {
		return fa_re_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_zona_pago
	* @param valor el valor de Fa_re_zona_pago
	*/ 
	public void setFa_re_zona_pago(String valor) {
		this.fa_re_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cajero
	* @return el valor de Fa_re_cajero
	*/ 
	public String getFa_re_cajero() {
		return fa_re_cajero;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cajero
	* @param valor el valor de Fa_re_cajero
	*/ 
	public void setFa_re_cajero(String valor) {
		this.fa_re_cajero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_tipo_pago
	* @return el valor de Fa_re_tipo_pago
	*/ 
	public String getFa_re_tipo_pago() {
		return fa_re_tipo_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_tipo_pago
	* @param valor el valor de Fa_re_tipo_pago
	*/ 
	public void setFa_re_tipo_pago(String valor) {
		this.fa_re_tipo_pago = valor;
	}
	
	// AGREGADO DESCRIPCION DEL CAMPO TIPO DE PAGO
	/**
	* Recupera el valor del atributo FA_RE_TIPO_PAGO_DESCRIPCION
	* @return el valor de FA_RE_TIPO_PAGO_DESCRIPCION
	*/ 
	public String getFA_RE_TIPO_PAGO_DESCRIPCION() {
		return FA_RE_TIPO_PAGO_DESCRIPCION;
	}
    	
	/**
	* Fija el valor del atributo FA_RE_TIPO_PAGO_DESCRIPCION
	* @param valor el valor de FA_RE_TIPO_PAGO_DESCRIPCION
	*/ 
	public void setFA_RE_TIPO_PAGO_DESCRIPCION(String valor) {
		this.FA_RE_TIPO_PAGO_DESCRIPCION = valor;
	}
	
	
	
	
	/**
	* Recupera el valor del atributo Campo_no_usado_4
	* @return el valor de Campo_no_usado_4
	*/ 
	public String getCampo_no_usado_4() {
		return campo_no_usado_4;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_4
	* @param valor el valor de Campo_no_usado_4
	*/ 
	public void setCampo_no_usado_4(String valor) {
		this.campo_no_usado_4 = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_5
	* @return el valor de Campo_no_usado_5
	*/ 
	public String getCampo_no_usado_5() {
		return campo_no_usado_5;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_5
	* @param valor el valor de Campo_no_usado_5
	*/ 
	public void setCampo_no_usado_5(String valor) {
		this.campo_no_usado_5 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_marca_tarea
	* @return el valor de Fa_re_marca_tarea
	*/ 
	public String getFa_re_marca_tarea() {
		return fa_re_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_marca_tarea
	* @param valor el valor de Fa_re_marca_tarea
	*/ 
	public void setFa_re_marca_tarea(String valor) {
		this.fa_re_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_orden_pago
	* @return el valor de Fa_re_nro_orden_pago
	*/ 
	public String getFa_re_nro_orden_pago() {
		return fa_re_nro_orden_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_orden_pago
	* @param valor el valor de Fa_re_nro_orden_pago
	*/ 
	public void setFa_re_nro_orden_pago(String valor) {
		this.fa_re_nro_orden_pago = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_6
	* @return el valor de Campo_no_usado_6
	*/ 
	public String getCampo_no_usado_6() {
		return campo_no_usado_6;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_6
	* @param valor el valor de Campo_no_usado_6
	*/ 
	public void setCampo_no_usado_6(String valor) {
		this.campo_no_usado_6 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_alta
	* @return el valor de Fa_re_hora_alta
	*/ 
	public String getFa_re_hora_alta() {
		return fa_re_hora_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_alta
	* @param valor el valor de Fa_re_hora_alta
	*/ 
	public void setFa_re_hora_alta(String valor) {
		this.fa_re_hora_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_alta
	* @return el valor de Fa_re_usuario_alta
	*/ 
	public String getFa_re_usuario_alta() {
		return fa_re_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_alta
	* @param valor el valor de Fa_re_usuario_alta
	*/ 
	public void setFa_re_usuario_alta(String valor) {
		this.fa_re_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_7
	* @return el valor de Campo_no_usado_7
	*/ 
	public String getCampo_no_usado_7() {
		return campo_no_usado_7;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_7
	* @param valor el valor de Campo_no_usado_7
	*/ 
	public void setCampo_no_usado_7(String valor) {
		this.campo_no_usado_7 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_modific
	* @return el valor de Fa_re_hora_modific
	*/ 
	public String getFa_re_hora_modific() {
		return fa_re_hora_modific;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_modific
	* @param valor el valor de Fa_re_hora_modific
	*/ 
	public void setFa_re_hora_modific(String valor) {
		this.fa_re_hora_modific = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_modific
	* @return el valor de Fa_re_usuario_modific
	*/ 
	public String getFa_re_usuario_modific() {
		return fa_re_usuario_modific;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_modific
	* @param valor el valor de Fa_re_usuario_modific
	*/ 
	public void setFa_re_usuario_modific(String valor) {
		this.fa_re_usuario_modific = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_8
	* @return el valor de Campo_no_usado_8
	*/ 
	public String getCampo_no_usado_8() {
		return campo_no_usado_8;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_8
	* @param valor el valor de Campo_no_usado_8
	*/ 
	public void setCampo_no_usado_8(String valor) {
		this.campo_no_usado_8 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_baja
	* @return el valor de Fa_re_hora_baja
	*/ 
	public String getFa_re_hora_baja() {
		return fa_re_hora_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_baja
	* @param valor el valor de Fa_re_hora_baja
	*/ 
	public void setFa_re_hora_baja(String valor) {
		this.fa_re_hora_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_baja
	* @return el valor de Fa_re_usuario_baja
	*/ 
	public String getFa_re_usuario_baja() {
		return fa_re_usuario_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_baja
	* @param valor el valor de Fa_re_usuario_baja
	*/ 
	public void setFa_re_usuario_baja(String valor) {
		this.fa_re_usuario_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_superv
	* @return el valor de Fa_re_usuario_superv
	*/ 
	public String getFa_re_usuario_superv() {
		return fa_re_usuario_superv;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_superv
	* @param valor el valor de Fa_re_usuario_superv
	*/ 
	public void setFa_re_usuario_superv(String valor) {
		this.fa_re_usuario_superv = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_9
	* @return el valor de Campo_no_usado_9
	*/ 
	public String getCampo_no_usado_9() {
		return campo_no_usado_9;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_9
	* @param valor el valor de Campo_no_usado_9
	*/ 
	public void setCampo_no_usado_9(String valor) {
		this.campo_no_usado_9 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_superv
	* @return el valor de Fa_re_hora_superv
	*/ 
	public String getFa_re_hora_superv() {
		return fa_re_hora_superv;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_superv
	* @param valor el valor de Fa_re_hora_superv
	*/ 
	public void setFa_re_hora_superv(String valor) {
		this.fa_re_hora_superv = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_usuario_autoriz
	* @return el valor de Fa_re_usuario_autoriz
	*/ 
	public String getFa_re_usuario_autoriz() {
		return fa_re_usuario_autoriz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_usuario_autoriz
	* @param valor el valor de Fa_re_usuario_autoriz
	*/ 
	public void setFa_re_usuario_autoriz(String valor) {
		this.fa_re_usuario_autoriz = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_10
	* @return el valor de Campo_no_usado_10
	*/ 
	public String getCampo_no_usado_10() {
		return campo_no_usado_10;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_10
	* @param valor el valor de Campo_no_usado_10
	*/ 
	public void setCampo_no_usado_10(String valor) {
		this.campo_no_usado_10 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_hora_autoriz
	* @return el valor de Fa_re_hora_autoriz
	*/ 
	public String getFa_re_hora_autoriz() {
		return fa_re_hora_autoriz;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_hora_autoriz
	* @param valor el valor de Fa_re_hora_autoriz
	*/ 
	public void setFa_re_hora_autoriz(String valor) {
		this.fa_re_hora_autoriz = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_cat_gran_cli
	* @return el valor de Fa_re_cat_gran_cli
	*/ 
	public String getFa_re_cat_gran_cli() {
		return fa_re_cat_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_cat_gran_cli
	* @param valor el valor de Fa_re_cat_gran_cli
	*/ 
	public void setFa_re_cat_gran_cli(String valor) {
		this.fa_re_cat_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_nro_gran_cli
	* @return el valor de Fa_re_nro_gran_cli
	*/ 
	public String getFa_re_nro_gran_cli() {
		return fa_re_nro_gran_cli;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_nro_gran_cli
	* @param valor el valor de Fa_re_nro_gran_cli
	*/ 
	public void setFa_re_nro_gran_cli(String valor) {
		this.fa_re_nro_gran_cli = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_pcia_porc
	* @return el valor de Fa_re_pcia_porc
	*/ 
	public String getFa_re_pcia_porc() {
		return fa_re_pcia_porc;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_pcia_porc
	* @param valor el valor de Fa_re_pcia_porc
	*/ 
	public void setFa_re_pcia_porc(String valor) {
		this.fa_re_pcia_porc = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_fecha_impre
	* @return el valor de Fa_re_fecha_impre
	*/ 
	public String getFa_re_fecha_impre() {
		return fa_re_fecha_impre;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_fecha_impre
	* @param valor el valor de Fa_re_fecha_impre
	*/ 
	public void setFa_re_fecha_impre(String valor) {
		this.fa_re_fecha_impre = valor;
	}
	/**
	* Recupera el valor del atributo Fa_re_porc_iva_rg17
	* @return el valor de Fa_re_porc_iva_rg17
	*/ 
	public String getFa_re_porc_iva_rg17() {
		return fa_re_porc_iva_rg17;
	}
    	
	/**
	* Fija el valor del atributo Fa_re_porc_iva_rg17
	* @param valor el valor de Fa_re_porc_iva_rg17
	*/ 
	public void setFa_re_porc_iva_rg17(String valor) {
		this.fa_re_porc_iva_rg17 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_reintegro
	* @return el valor de Fa_no_nro_reintegro
	*/ 
	public String getFa_no_nro_reintegro() {
		return fa_no_nro_reintegro;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_reintegro
	* @param valor el valor de Fa_no_nro_reintegro
	*/ 
	public void setFa_no_nro_reintegro(String valor) {
		this.fa_no_nro_reintegro = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_11
	* @return el valor de Campo_no_usado_11
	*/ 
	public String getCampo_no_usado_11() {
		return campo_no_usado_11;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_11
	* @param valor el valor de Campo_no_usado_11
	*/ 
	public void setCampo_no_usado_11(String valor) {
		this.campo_no_usado_11 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_nc
	* @return el valor de Fa_no_nro_nc
	*/ 
	public String getFa_no_nro_nc() {
		return fa_no_nro_nc;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_nc
	* @param valor el valor de Fa_no_nro_nc
	*/ 
	public void setFa_no_nro_nc(String valor) {
		this.fa_no_nro_nc = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_12
	* @return el valor de Campo_no_usado_12
	*/ 
	public String getCampo_no_usado_12() {
		return campo_no_usado_12;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_12
	* @param valor el valor de Campo_no_usado_12
	*/ 
	public void setCampo_no_usado_12(String valor) {
		this.campo_no_usado_12 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_nro_nd
	* @return el valor de Fa_no_nro_nd
	*/ 
	public String getFa_no_nro_nd() {
		return fa_no_nro_nd;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_nro_nd
	* @param valor el valor de Fa_no_nro_nd
	*/ 
	public void setFa_no_nro_nd(String valor) {
		this.fa_no_nro_nd = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_13
	* @return el valor de Campo_no_usado_13
	*/ 
	public String getCampo_no_usado_13() {
		return campo_no_usado_13;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_13
	* @param valor el valor de Campo_no_usado_13
	*/ 
	public void setCampo_no_usado_13(String valor) {
		this.campo_no_usado_13 = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_14
	* @return el valor de Campo_no_usado_14
	*/ 
	public String getCampo_no_usado_14() {
		return campo_no_usado_14;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_14
	* @param valor el valor de Campo_no_usado_14
	*/ 
	public void setCampo_no_usado_14(String valor) {
		this.campo_no_usado_14 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_motivo
	* @return el valor de Fa_no_motivo
	*/ 
	public String getFa_no_motivo() {
		return fa_no_motivo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_motivo
	* @param valor el valor de Fa_no_motivo
	*/ 
	public void setFa_no_motivo(String valor) {
		this.fa_no_motivo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_zona
	* @return el valor de Fa_no_zona
	*/ 
	public String getFa_no_zona() {
		return fa_no_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_zona
	* @param valor el valor de Fa_no_zona
	*/ 
	public void setFa_no_zona(String valor) {
		this.fa_no_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_oficina
	* @return el valor de Fa_no_oficina
	*/ 
	public String getFa_no_oficina() {
		return fa_no_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_oficina
	* @param valor el valor de Fa_no_oficina
	*/ 
	public void setFa_no_oficina(String valor) {
		this.fa_no_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_abonado
	* @return el valor de Fa_no_abonado
	*/ 
	public String getFa_no_abonado() {
		return fa_no_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_abonado
	* @param valor el valor de Fa_no_abonado
	*/ 
	public void setFa_no_abonado(String valor) {
		this.fa_no_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_tipo_fact
	* @return el valor de Fa_no_tipo_fact
	*/ 
	public String getFa_no_tipo_fact() {
		return fa_no_tipo_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_tipo_fact
	* @param valor el valor de Fa_no_tipo_fact
	*/ 
	public void setFa_no_tipo_fact(String valor) {
		this.fa_no_tipo_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_ano_lote
	* @return el valor de Fa_no_ano_lote
	*/ 
	public String getFa_no_ano_lote() {
		return fa_no_ano_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_ano_lote
	* @param valor el valor de Fa_no_ano_lote
	*/ 
	public void setFa_no_ano_lote(String valor) {
		this.fa_no_ano_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_serie_meg
	* @return el valor de Fa_no_serie_meg
	*/ 
	public String getFa_no_serie_meg() {
		return fa_no_serie_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_serie_meg
	* @param valor el valor de Fa_no_serie_meg
	*/ 
	public void setFa_no_serie_meg(String valor) {
		this.fa_no_serie_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_n_contrato_meg
	* @return el valor de Fa_no_n_contrato_meg
	*/ 
	public String getFa_no_n_contrato_meg() {
		return fa_no_n_contrato_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_n_contrato_meg
	* @param valor el valor de Fa_no_n_contrato_meg
	*/ 
	public void setFa_no_n_contrato_meg(String valor) {
		this.fa_no_n_contrato_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_n_cuota_meg
	* @return el valor de Fa_no_n_cuota_meg
	*/ 
	public String getFa_no_n_cuota_meg() {
		return fa_no_n_cuota_meg;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_n_cuota_meg
	* @param valor el valor de Fa_no_n_cuota_meg
	*/ 
	public void setFa_no_n_cuota_meg(String valor) {
		this.fa_no_n_cuota_meg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_categoria
	* @return el valor de Fa_no_categoria
	*/ 
	public String getFa_no_categoria() {
		return fa_no_categoria;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_categoria
	* @param valor el valor de Fa_no_categoria
	*/ 
	public void setFa_no_categoria(String valor) {
		this.fa_no_categoria = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_tipo_contrib
	* @return el valor de Fa_no_tipo_contrib
	*/ 
	public String getFa_no_tipo_contrib() {
		return fa_no_tipo_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_tipo_contrib
	* @param valor el valor de Fa_no_tipo_contrib
	*/ 
	public void setFa_no_tipo_contrib(String valor) {
		this.fa_no_tipo_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_tipo_contrib_nvo
	* @return el valor de Fa_no_tipo_contrib_nvo
	*/ 
	public String getFa_no_tipo_contrib_nvo() {
		return fa_no_tipo_contrib_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_tipo_contrib_nvo
	* @param valor el valor de Fa_no_tipo_contrib_nvo
	*/ 
	public void setFa_no_tipo_contrib_nvo(String valor) {
		this.fa_no_tipo_contrib_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_15
	* @return el valor de Campo_no_usado_15
	*/ 
	public String getCampo_no_usado_15() {
		return campo_no_usado_15;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_15
	* @param valor el valor de Campo_no_usado_15
	*/ 
	public void setCampo_no_usado_15(String valor) {
		this.campo_no_usado_15 = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_16
	* @return el valor de Campo_no_usado_16
	*/ 
	public String getCampo_no_usado_16() {
		return campo_no_usado_16;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_16
	* @param valor el valor de Campo_no_usado_16
	*/ 
	public void setCampo_no_usado_16(String valor) {
		this.campo_no_usado_16 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_no_marca_tarea
	* @return el valor de Fa_no_marca_tarea
	*/ 
	public String getFa_no_marca_tarea() {
		return fa_no_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_no_marca_tarea
	* @param valor el valor de Fa_no_marca_tarea
	*/ 
	public void setFa_no_marca_tarea(String valor) {
		this.fa_no_marca_tarea = valor;
	}
	
	/**
	* Fija el valor del atributo FECHA_PAGO_DINAMICA
	* @param valor el valor de FECHA_PAGO_DINAMICA
	*/ 
	public void setFECHA_PAGO_DINAMICA(String valor) {
		this.FECHA_PAGO_DINAMICA = valor;
	}
	/**
	* Recupera el valor del atributo FECHA_DINAMICA
	* @return el valor de FECHA_DINAMICA
	*/ 
	public String getFECHA_PAGO_DINAMICA() {
		return FECHA_PAGO_DINAMICA;
	}			
	
	/**
	* Fija el valor del atributo FA_RE_FECHAPAGO_PRESC
	* @param valor el valor de FA_RE_FECHAPAGO_PRESC
	*/ 
	public void setFA_RE_FECHAPAGO_PRESC(String valor) {
		this.FA_RE_FECHAPAGO_PRESC = valor;
	}
	/**
	* Recupera el valor del atributo FA_RE_FECHAPAGO_PRESC
	* @return el valor de FA_RE_FECHAPAGO_PRESC
	*/ 
	public String getFA_RE_FECHAPAGO_PRESC() {
		return FA_RE_FECHAPAGO_PRESC;
	}
	
	/**
	* Fija el valor del atributo PRESCCRIPTO
	* @param valor el valor de PRESCRIPTO
	*/ 
	public void setPRESCRIPTO(String valor) {
		this.PRESCRIPTO = valor;
	}
	/**
	* Recupera el valor del atributo PRESCRIPTO
	* @return el valor de PRESCRIPTO
	*/ 
	public String getPRESCRIPTO() {
		return PRESCRIPTO;
	}	
}


