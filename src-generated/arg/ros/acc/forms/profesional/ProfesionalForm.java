package arg.ros.acc.forms.profesional;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.profesional.ProfesionalVO;

/**
* ProfesionalForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class ProfesionalForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String idprofesional;
	private String fechaalta;
	private String idpersona;
	private String matriculanacional;
	private String matriculaprovincial;
	private String activo;

	public ProfesionalVO getVO() {
		ProfesionalVO vo = new ProfesionalVO();
		
		vo.setFechaalta(this.getAsDate(this.fechaalta));
		vo.setIdpersona(Double.valueOf(this.idpersona));
		vo.setMatriculanacional(this.matriculanacional);
		vo.setMatriculaprovincial(this.matriculaprovincial);
		vo.setActivo(this.activo);
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Idprofesional
	* @return el valor de Idprofesional
	*/ 
	public String getIdprofesional() {
		return idprofesional;
	}
    	
	/**
	* Fija el valor del atributo Idprofesional
	* @param valor el valor de Idprofesional
	*/ 
	public void setIdprofesional(String valor) {
		this.idprofesional = valor;
	}
	/**
	* Recupera el valor del atributo Fechaalta
	* @return el valor de Fechaalta
	*/ 
	public String getFechaalta() {
		return fechaalta;
	}
    	
	/**
	* Fija el valor del atributo Fechaalta
	* @param valor el valor de Fechaalta
	*/ 
	public void setFechaalta(String valor) {
		this.fechaalta = valor;
	}
	/**
	* Recupera el valor del atributo Idpersona
	* @return el valor de Idpersona
	*/ 
	public String getIdpersona() {
		return idpersona;
	}
    	
	/**
	* Fija el valor del atributo Idpersona
	* @param valor el valor de Idpersona
	*/ 
	public void setIdpersona(String valor) {
		this.idpersona = valor;
	}
	/**
	* Recupera el valor del atributo Matriculanacional
	* @return el valor de Matriculanacional
	*/ 
	public String getMatriculanacional() {
		return matriculanacional;
	}
    	
	/**
	* Fija el valor del atributo Matriculanacional
	* @param valor el valor de Matriculanacional
	*/ 
	public void setMatriculanacional(String valor) {
		this.matriculanacional = valor;
	}
	/**
	* Recupera el valor del atributo Matriculaprovincial
	* @return el valor de Matriculaprovincial
	*/ 
	public String getMatriculaprovincial() {
		return matriculaprovincial;
	}
    	
	/**
	* Fija el valor del atributo Matriculaprovincial
	* @param valor el valor de Matriculaprovincial
	*/ 
	public void setMatriculaprovincial(String valor) {
		this.matriculaprovincial = valor;
	}
	/**
	* Recupera el valor del atributo Activo
	* @return el valor de Activo
	*/ 
	public String getActivo() {
		return activo;
	}
    	
	/**
	* Fija el valor del atributo Activo
	* @param valor el valor de Activo
	*/ 
	public void setActivo(String valor) {
		this.activo = valor;
	}
}
