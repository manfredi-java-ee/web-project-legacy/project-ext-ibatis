package arg.ros.acc.forms.inhibiciones;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.inhibiciones.MovimientoFisicoVO;

/**
* MovimientoFisicoForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class MovimientoFisicoForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String adabas_isn;
	private String fa_r_oficina;
	private String fa_c_importe_total;
	private String fa_abono;
	private String fa_r_cajero;
	private String fa_nro_tarjeta;
	private String campo_no_usado_3;
	private String fa_r_usuario;
	private String fa_r_tipo_pago;
	private String fa_c_estado_conciliacion;
	private String fa_r_total_sin_recargo;
	private String fa_ident_abo;
	private String fa_fecha_acreditacion_nvo;
	private String fa_localidad;
	private String fa_r_zona_pago;
	private String fa_marca_desimputacion;
	private String fa_c_usuario_baja;
	private String fa_c_fecha_pago_nvo;
	private String fa_r_fecha_proceso_nvo;
	private String fa_r_categoria_gran_cliente;
	private String fa_urb;
	private String fa_c_fecha_hora_alta;
	private String fa_c_zona_pago;
	private String fa_c_fecha_hora_modif;
	private String campo_no_usado_7;
	private String fa_fecha_marca_nvo;
	private String fa_serv_medido;
	private String fa_r_factura;
	private String fa_c_tipo_registro;
	private String fa_r_terminal;
	private String campo_no_usado;
	private String fa_razon_social;
	private String campo_no_usado_8;
	private String fa_r_iva_mora_r;
	private String fa_domicilio;
	private String fa_r_nro_nota_cred;
	private String fa_c_cant_concp_c_apert;
	private String fa_r_marca_tarea;
	private String fa_r_hora_ingreso;
	private String fa_r_marca_carta_motivo;
	private String fa_cod_entrega;
	private String fa_r_ident_abo;
	private String fa_r_fecha_reconcil_nvo;
	private String fa_r_reconciliacion;
	private String fa_fecha_desimputacion;
	private String campo_no_usado_5;
	private String fa_c_ident_aper_man;
	private String fa_c_nro_conciliacion;
	private String fa_tipo_contrib;
	private String fa_r_importe_fuera_term;
	private String campo_no_usado_4;
	private String fa_r_linea;
	private String fa_cod_postal;
	private String fa_tipo_fact;
	private String fa_c_cod_banco_depos;
	private String fa_total_facturado;
	private String fa_r_fecha_pago_orig;
	private String fa_c_usuario_modif;
	private String fa_oficina;
	private String fa_c_nro_boleta_deposito;
	private String fa_linea;
	private String fa_r_anio_lote;
	private String fa_r_codigo_movimiento;
	private String fa_r_importe_cobrado;
	private String fa_importe_pdte_gire;
	private String fa_r_zona_pago_orig;
	private String fa_c_usuario_alta;
	private String fa_numero_pago;
	private String campo_no_usado_9;
	private String fa_cod_banco;
	private String fa_c_cant_reg;
	private String fa_r_urb;
	private String fa_marca_tarea;
	private String fa_r_codigo_iva;
	private String fa_r_nro_reintegro;
	private String fa_tipo_contrib_nvo;
	private String fa_r_abonado;
	private String fa_r_tipo_facturacion;
	private String fa_c_fecha_hora_baja;
	private String fa_r_numero_pago;
	private String fa_codigo_movimiento;
	private String fa_anio_lote;
	private String fa_banco_zona;
	private String fa_c_estado_asiento;
	private String fa_abonado;
	private String campo_no_usado_6;
	private String fa_r_interurb;
	private String fa_r_nro_copia;
	private String fa_r_fecha_pago_nvo;
	private String fa_r_marca_pago;
	private String fa_zona_banco_pago_doble;
	private String fa_usuario_terminal;
	private String fa_fecha_proc_ctgo;
	private String fa_c_marca_tarea;
	private String fa_fecha_entrega;
	private String fa_marca_recargo;
	private String fa_c_cod_concepto;
	private String fa_interurb;
	private String fa_r_zona;
	private String fa_r_fecha_ingreso_nvo;
	private String fa_hora_ingreso;
	private String fa_c_fecha_env_contab_nvo;
	private String fa_r_estado_real_base;
	private String fa_importe_iva;
	private String fa_zona;
	private String fa_fecha_ingreso_nvo;
	private String desc_movimiento;

	public MovimientoFisicoVO getVO() {
		MovimientoFisicoVO vo = new MovimientoFisicoVO();
		
		vo.setAdabas_isn(Double.valueOf(this.adabas_isn));
		vo.setFa_r_oficina(Double.valueOf(this.fa_r_oficina));
		vo.setFa_c_importe_total(new java.math.BigDecimal(this.fa_c_importe_total));
		vo.setFa_abono(new java.math.BigDecimal(this.fa_abono));
		vo.setFa_r_cajero(Double.valueOf(this.fa_r_cajero));
		vo.setFa_nro_tarjeta(Double.valueOf(this.fa_nro_tarjeta));
		vo.setCampo_no_usado_3(Double.valueOf(this.campo_no_usado_3));
		vo.setFa_r_usuario(this.fa_r_usuario);
		vo.setFa_r_tipo_pago(Double.valueOf(this.fa_r_tipo_pago));
		vo.setFa_c_estado_conciliacion(Double.valueOf(this.fa_c_estado_conciliacion));
		vo.setFa_r_total_sin_recargo(new java.math.BigDecimal(this.fa_r_total_sin_recargo));
		vo.setFa_ident_abo(Double.valueOf(this.fa_ident_abo));
		vo.setFa_fecha_acreditacion_nvo(this.fa_fecha_acreditacion_nvo);
		vo.setFa_localidad(this.fa_localidad);
		vo.setFa_r_zona_pago(Double.valueOf(this.fa_r_zona_pago));
		vo.setFa_marca_desimputacion(this.fa_marca_desimputacion);
		vo.setFa_c_usuario_baja(this.fa_c_usuario_baja);
		vo.setFa_c_fecha_pago_nvo(this.getAsDate(this.fa_c_fecha_pago_nvo));
		vo.setFa_r_fecha_proceso_nvo(this.getAsDate(this.fa_r_fecha_proceso_nvo));
		vo.setFa_r_categoria_gran_cliente(Double.valueOf(this.fa_r_categoria_gran_cliente));
		vo.setFa_urb(this.fa_urb);
		vo.setFa_c_fecha_hora_alta(this.getAsDate(this.fa_c_fecha_hora_alta));
		vo.setFa_c_zona_pago(Double.valueOf(this.fa_c_zona_pago));
		vo.setFa_c_fecha_hora_modif(this.getAsDate(this.fa_c_fecha_hora_modif));
		vo.setCampo_no_usado_7(Double.valueOf(this.campo_no_usado_7));
		vo.setFa_fecha_marca_nvo(this.getAsDate(this.fa_fecha_marca_nvo));
		vo.setFa_serv_medido(new java.math.BigDecimal(this.fa_serv_medido));
		vo.setFa_r_factura(Double.valueOf(this.fa_r_factura));
		vo.setFa_c_tipo_registro(Double.valueOf(this.fa_c_tipo_registro));
		vo.setFa_r_terminal(this.fa_r_terminal);
		vo.setCampo_no_usado(Double.valueOf(this.campo_no_usado));
		vo.setFa_razon_social(this.fa_razon_social);
		vo.setCampo_no_usado_8(Double.valueOf(this.campo_no_usado_8));
		vo.setFa_r_iva_mora_r(new java.math.BigDecimal(this.fa_r_iva_mora_r));
		vo.setFa_domicilio(this.fa_domicilio);
		vo.setFa_r_nro_nota_cred(Double.valueOf(this.fa_r_nro_nota_cred));
		vo.setFa_c_cant_concp_c_apert(Double.valueOf(this.fa_c_cant_concp_c_apert));
		vo.setFa_r_marca_tarea(Double.valueOf(this.fa_r_marca_tarea));
		vo.setFa_r_hora_ingreso(this.fa_r_hora_ingreso);
		vo.setFa_r_marca_carta_motivo(Double.valueOf(this.fa_r_marca_carta_motivo));
		vo.setFa_cod_entrega(Double.valueOf(this.fa_cod_entrega));
		vo.setFa_r_ident_abo(Double.valueOf(this.fa_r_ident_abo));
		vo.setFa_r_fecha_reconcil_nvo(this.getAsDate(this.fa_r_fecha_reconcil_nvo));
		vo.setFa_r_reconciliacion(Double.valueOf(this.fa_r_reconciliacion));
		vo.setFa_fecha_desimputacion(this.getAsDate(this.fa_fecha_desimputacion));
		vo.setCampo_no_usado_5(Double.valueOf(this.campo_no_usado_5));
		vo.setFa_c_ident_aper_man(this.fa_c_ident_aper_man);
		vo.setFa_c_nro_conciliacion(Double.valueOf(this.fa_c_nro_conciliacion));
		vo.setFa_tipo_contrib(Double.valueOf(this.fa_tipo_contrib));
		vo.setFa_r_importe_fuera_term(new java.math.BigDecimal(this.fa_r_importe_fuera_term));
		vo.setCampo_no_usado_4(Double.valueOf(this.campo_no_usado_4));
		vo.setFa_r_linea(this.fa_r_linea);
		vo.setFa_cod_postal(this.fa_cod_postal);
		vo.setFa_tipo_fact(Double.valueOf(this.fa_tipo_fact));
		vo.setFa_c_cod_banco_depos(Double.valueOf(this.fa_c_cod_banco_depos));
		vo.setFa_total_facturado(new java.math.BigDecimal(this.fa_total_facturado));
		vo.setFa_r_fecha_pago_orig(this.getAsDate(this.fa_r_fecha_pago_orig));
		vo.setFa_c_usuario_modif(this.fa_c_usuario_modif);
		vo.setFa_oficina(Double.valueOf(this.fa_oficina));
		vo.setFa_c_nro_boleta_deposito(Double.valueOf(this.fa_c_nro_boleta_deposito));
		vo.setFa_linea(this.fa_linea);
		vo.setFa_r_anio_lote(Double.valueOf(this.fa_r_anio_lote));
		vo.setFa_r_codigo_movimiento(this.fa_r_codigo_movimiento);
		vo.setFa_r_importe_cobrado(new java.math.BigDecimal(this.fa_r_importe_cobrado));
		vo.setFa_importe_pdte_gire(new java.math.BigDecimal(this.fa_importe_pdte_gire));
		vo.setFa_r_zona_pago_orig(Double.valueOf(this.fa_r_zona_pago_orig));
		vo.setFa_c_usuario_alta(this.fa_c_usuario_alta);
		vo.setFa_numero_pago(Double.valueOf(this.fa_numero_pago));
		vo.setCampo_no_usado_9(Double.valueOf(this.campo_no_usado_9));
		vo.setFa_cod_banco(Double.valueOf(this.fa_cod_banco));
		vo.setFa_c_cant_reg(Double.valueOf(this.fa_c_cant_reg));
		vo.setFa_r_urb(this.fa_r_urb);
		vo.setFa_marca_tarea(Double.valueOf(this.fa_marca_tarea));
		vo.setFa_r_codigo_iva(Double.valueOf(this.fa_r_codigo_iva));
		vo.setFa_r_nro_reintegro(Double.valueOf(this.fa_r_nro_reintegro));
		vo.setFa_tipo_contrib_nvo(Double.valueOf(this.fa_tipo_contrib_nvo));
		vo.setFa_r_abonado(Double.valueOf(this.fa_r_abonado));
		vo.setFa_r_tipo_facturacion(Double.valueOf(this.fa_r_tipo_facturacion));
		vo.setFa_c_fecha_hora_baja(this.getAsDate(this.fa_c_fecha_hora_baja));
		vo.setFa_r_numero_pago(Double.valueOf(this.fa_r_numero_pago));
		vo.setFa_codigo_movimiento(this.fa_codigo_movimiento);
		vo.setFa_anio_lote(this.fa_anio_lote);
		vo.setFa_banco_zona(Double.valueOf(this.fa_banco_zona));
		vo.setFa_c_estado_asiento(Double.valueOf(this.fa_c_estado_asiento));
		vo.setFa_abonado(Double.valueOf(this.fa_abonado));
		vo.setCampo_no_usado_6(Double.valueOf(this.campo_no_usado_6));
		vo.setFa_r_interurb(this.fa_r_interurb);
		vo.setFa_r_nro_copia(Double.valueOf(this.fa_r_nro_copia));
		vo.setFa_r_fecha_pago_nvo(this.getAsDate(this.fa_r_fecha_pago_nvo));
		vo.setFa_r_marca_pago(this.fa_r_marca_pago);
		vo.setFa_zona_banco_pago_doble(Double.valueOf(this.fa_zona_banco_pago_doble));
		vo.setFa_usuario_terminal(this.fa_usuario_terminal);
		vo.setFa_fecha_proc_ctgo(this.fa_fecha_proc_ctgo);
		vo.setFa_c_marca_tarea(Double.valueOf(this.fa_c_marca_tarea));
		vo.setFa_fecha_entrega(this.getAsDate(this.fa_fecha_entrega));
		vo.setFa_marca_recargo(Double.valueOf(this.fa_marca_recargo));
		vo.setFa_c_cod_concepto(Double.valueOf(this.fa_c_cod_concepto));
		vo.setFa_interurb(this.fa_interurb);
		vo.setFa_r_zona(Double.valueOf(this.fa_r_zona));
		vo.setFa_r_fecha_ingreso_nvo(this.getAsDate(this.fa_r_fecha_ingreso_nvo));
		vo.setFa_hora_ingreso(this.fa_hora_ingreso);
		vo.setFa_c_fecha_env_contab_nvo(this.getAsDate(this.fa_c_fecha_env_contab_nvo));
		vo.setFa_r_estado_real_base(Double.valueOf(this.fa_r_estado_real_base));
		vo.setFa_importe_iva(new java.math.BigDecimal(this.fa_importe_iva));
		vo.setFa_zona(Double.valueOf(this.fa_zona));
		vo.setFa_fecha_ingreso_nvo(this.getAsDate(this.fa_fecha_ingreso_nvo));
		vo.setdesc_movimiento(this.desc_movimiento);
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(String valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_oficina
	* @return el valor de Fa_r_oficina
	*/ 
	public String getFa_r_oficina() {
		return fa_r_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_oficina
	* @param valor el valor de Fa_r_oficina
	*/ 
	public void setFa_r_oficina(String valor) {
		this.fa_r_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_importe_total
	* @return el valor de Fa_c_importe_total
	*/ 
	public String getFa_c_importe_total() {
		return fa_c_importe_total;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_importe_total
	* @param valor el valor de Fa_c_importe_total
	*/ 
	public void setFa_c_importe_total(String valor) {
		this.fa_c_importe_total = valor;
	}
	/**
	* Recupera el valor del atributo Fa_abono
	* @return el valor de Fa_abono
	*/ 
	public String getFa_abono() {
		return fa_abono;
	}
    	
	/**
	* Fija el valor del atributo Fa_abono
	* @param valor el valor de Fa_abono
	*/ 
	public void setFa_abono(String valor) {
		this.fa_abono = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_cajero
	* @return el valor de Fa_r_cajero
	*/ 
	public String getFa_r_cajero() {
		return fa_r_cajero;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_cajero
	* @param valor el valor de Fa_r_cajero
	*/ 
	public void setFa_r_cajero(String valor) {
		this.fa_r_cajero = valor;
	}
	/**
	* Recupera el valor del atributo Fa_nro_tarjeta
	* @return el valor de Fa_nro_tarjeta
	*/ 
	public String getFa_nro_tarjeta() {
		return fa_nro_tarjeta;
	}
    	
	/**
	* Fija el valor del atributo Fa_nro_tarjeta
	* @param valor el valor de Fa_nro_tarjeta
	*/ 
	public void setFa_nro_tarjeta(String valor) {
		this.fa_nro_tarjeta = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_3
	* @return el valor de Campo_no_usado_3
	*/ 
	public String getCampo_no_usado_3() {
		return campo_no_usado_3;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_3
	* @param valor el valor de Campo_no_usado_3
	*/ 
	public void setCampo_no_usado_3(String valor) {
		this.campo_no_usado_3 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_usuario
	* @return el valor de Fa_r_usuario
	*/ 
	public String getFa_r_usuario() {
		return fa_r_usuario;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_usuario
	* @param valor el valor de Fa_r_usuario
	*/ 
	public void setFa_r_usuario(String valor) {
		this.fa_r_usuario = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_tipo_pago
	* @return el valor de Fa_r_tipo_pago
	*/ 
	public String getFa_r_tipo_pago() {
		return fa_r_tipo_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_tipo_pago
	* @param valor el valor de Fa_r_tipo_pago
	*/ 
	public void setFa_r_tipo_pago(String valor) {
		this.fa_r_tipo_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_estado_conciliacion
	* @return el valor de Fa_c_estado_conciliacion
	*/ 
	public String getFa_c_estado_conciliacion() {
		return fa_c_estado_conciliacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_estado_conciliacion
	* @param valor el valor de Fa_c_estado_conciliacion
	*/ 
	public void setFa_c_estado_conciliacion(String valor) {
		this.fa_c_estado_conciliacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_total_sin_recargo
	* @return el valor de Fa_r_total_sin_recargo
	*/ 
	public String getFa_r_total_sin_recargo() {
		return fa_r_total_sin_recargo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_total_sin_recargo
	* @param valor el valor de Fa_r_total_sin_recargo
	*/ 
	public void setFa_r_total_sin_recargo(String valor) {
		this.fa_r_total_sin_recargo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_ident_abo
	* @return el valor de Fa_ident_abo
	*/ 
	public String getFa_ident_abo() {
		return fa_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_ident_abo
	* @param valor el valor de Fa_ident_abo
	*/ 
	public void setFa_ident_abo(String valor) {
		this.fa_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_acreditacion_nvo
	* @return el valor de Fa_fecha_acreditacion_nvo
	*/ 
	public String getFa_fecha_acreditacion_nvo() {
		return fa_fecha_acreditacion_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_acreditacion_nvo
	* @param valor el valor de Fa_fecha_acreditacion_nvo
	*/ 
	public void setFa_fecha_acreditacion_nvo(String valor) {
		this.fa_fecha_acreditacion_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_localidad
	* @return el valor de Fa_localidad
	*/ 
	public String getFa_localidad() {
		return fa_localidad;
	}
    	
	/**
	* Fija el valor del atributo Fa_localidad
	* @param valor el valor de Fa_localidad
	*/ 
	public void setFa_localidad(String valor) {
		this.fa_localidad = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_zona_pago
	* @return el valor de Fa_r_zona_pago
	*/ 
	public String getFa_r_zona_pago() {
		return fa_r_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_zona_pago
	* @param valor el valor de Fa_r_zona_pago
	*/ 
	public void setFa_r_zona_pago(String valor) {
		this.fa_r_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_desimputacion
	* @return el valor de Fa_marca_desimputacion
	*/ 
	public String getFa_marca_desimputacion() {
		return fa_marca_desimputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_desimputacion
	* @param valor el valor de Fa_marca_desimputacion
	*/ 
	public void setFa_marca_desimputacion(String valor) {
		this.fa_marca_desimputacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_usuario_baja
	* @return el valor de Fa_c_usuario_baja
	*/ 
	public String getFa_c_usuario_baja() {
		return fa_c_usuario_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_usuario_baja
	* @param valor el valor de Fa_c_usuario_baja
	*/ 
	public void setFa_c_usuario_baja(String valor) {
		this.fa_c_usuario_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_pago_nvo
	* @return el valor de Fa_c_fecha_pago_nvo
	*/ 
	public String getFa_c_fecha_pago_nvo() {
		return fa_c_fecha_pago_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_pago_nvo
	* @param valor el valor de Fa_c_fecha_pago_nvo
	*/ 
	public void setFa_c_fecha_pago_nvo(String valor) {
		this.fa_c_fecha_pago_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_proceso_nvo
	* @return el valor de Fa_r_fecha_proceso_nvo
	*/ 
	public String getFa_r_fecha_proceso_nvo() {
		return fa_r_fecha_proceso_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_proceso_nvo
	* @param valor el valor de Fa_r_fecha_proceso_nvo
	*/ 
	public void setFa_r_fecha_proceso_nvo(String valor) {
		this.fa_r_fecha_proceso_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_categoria_gran_cliente
	* @return el valor de Fa_r_categoria_gran_cliente
	*/ 
	public String getFa_r_categoria_gran_cliente() {
		return fa_r_categoria_gran_cliente;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_categoria_gran_cliente
	* @param valor el valor de Fa_r_categoria_gran_cliente
	*/ 
	public void setFa_r_categoria_gran_cliente(String valor) {
		this.fa_r_categoria_gran_cliente = valor;
	}
	/**
	* Recupera el valor del atributo Fa_urb
	* @return el valor de Fa_urb
	*/ 
	public String getFa_urb() {
		return fa_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_urb
	* @param valor el valor de Fa_urb
	*/ 
	public void setFa_urb(String valor) {
		this.fa_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_hora_alta
	* @return el valor de Fa_c_fecha_hora_alta
	*/ 
	public String getFa_c_fecha_hora_alta() {
		return fa_c_fecha_hora_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_hora_alta
	* @param valor el valor de Fa_c_fecha_hora_alta
	*/ 
	public void setFa_c_fecha_hora_alta(String valor) {
		this.fa_c_fecha_hora_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_zona_pago
	* @return el valor de Fa_c_zona_pago
	*/ 
	public String getFa_c_zona_pago() {
		return fa_c_zona_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_zona_pago
	* @param valor el valor de Fa_c_zona_pago
	*/ 
	public void setFa_c_zona_pago(String valor) {
		this.fa_c_zona_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_hora_modif
	* @return el valor de Fa_c_fecha_hora_modif
	*/ 
	public String getFa_c_fecha_hora_modif() {
		return fa_c_fecha_hora_modif;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_hora_modif
	* @param valor el valor de Fa_c_fecha_hora_modif
	*/ 
	public void setFa_c_fecha_hora_modif(String valor) {
		this.fa_c_fecha_hora_modif = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_7
	* @return el valor de Campo_no_usado_7
	*/ 
	public String getCampo_no_usado_7() {
		return campo_no_usado_7;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_7
	* @param valor el valor de Campo_no_usado_7
	*/ 
	public void setCampo_no_usado_7(String valor) {
		this.campo_no_usado_7 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_marca_nvo
	* @return el valor de Fa_fecha_marca_nvo
	*/ 
	public String getFa_fecha_marca_nvo() {
		return fa_fecha_marca_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_marca_nvo
	* @param valor el valor de Fa_fecha_marca_nvo
	*/ 
	public void setFa_fecha_marca_nvo(String valor) {
		this.fa_fecha_marca_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_serv_medido
	* @return el valor de Fa_serv_medido
	*/ 
	public String getFa_serv_medido() {
		return fa_serv_medido;
	}
    	
	/**
	* Fija el valor del atributo Fa_serv_medido
	* @param valor el valor de Fa_serv_medido
	*/ 
	public void setFa_serv_medido(String valor) {
		this.fa_serv_medido = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_factura
	* @return el valor de Fa_r_factura
	*/ 
	public String getFa_r_factura() {
		return fa_r_factura;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_factura
	* @param valor el valor de Fa_r_factura
	*/ 
	public void setFa_r_factura(String valor) {
		this.fa_r_factura = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_tipo_registro
	* @return el valor de Fa_c_tipo_registro
	*/ 
	public String getFa_c_tipo_registro() {
		return fa_c_tipo_registro;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_tipo_registro
	* @param valor el valor de Fa_c_tipo_registro
	*/ 
	public void setFa_c_tipo_registro(String valor) {
		this.fa_c_tipo_registro = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_terminal
	* @return el valor de Fa_r_terminal
	*/ 
	public String getFa_r_terminal() {
		return fa_r_terminal;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_terminal
	* @param valor el valor de Fa_r_terminal
	*/ 
	public void setFa_r_terminal(String valor) {
		this.fa_r_terminal = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado
	* @return el valor de Campo_no_usado
	*/ 
	public String getCampo_no_usado() {
		return campo_no_usado;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado
	* @param valor el valor de Campo_no_usado
	*/ 
	public void setCampo_no_usado(String valor) {
		this.campo_no_usado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_razon_social
	* @return el valor de Fa_razon_social
	*/ 
	public String getFa_razon_social() {
		return fa_razon_social;
	}
    	
	/**
	* Fija el valor del atributo Fa_razon_social
	* @param valor el valor de Fa_razon_social
	*/ 
	public void setFa_razon_social(String valor) {
		this.fa_razon_social = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_8
	* @return el valor de Campo_no_usado_8
	*/ 
	public String getCampo_no_usado_8() {
		return campo_no_usado_8;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_8
	* @param valor el valor de Campo_no_usado_8
	*/ 
	public void setCampo_no_usado_8(String valor) {
		this.campo_no_usado_8 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_iva_mora_r
	* @return el valor de Fa_r_iva_mora_r
	*/ 
	public String getFa_r_iva_mora_r() {
		return fa_r_iva_mora_r;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_iva_mora_r
	* @param valor el valor de Fa_r_iva_mora_r
	*/ 
	public void setFa_r_iva_mora_r(String valor) {
		this.fa_r_iva_mora_r = valor;
	}
	/**
	* Recupera el valor del atributo Fa_domicilio
	* @return el valor de Fa_domicilio
	*/ 
	public String getFa_domicilio() {
		return fa_domicilio;
	}
    	
	/**
	* Fija el valor del atributo Fa_domicilio
	* @param valor el valor de Fa_domicilio
	*/ 
	public void setFa_domicilio(String valor) {
		this.fa_domicilio = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_nro_nota_cred
	* @return el valor de Fa_r_nro_nota_cred
	*/ 
	public String getFa_r_nro_nota_cred() {
		return fa_r_nro_nota_cred;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_nro_nota_cred
	* @param valor el valor de Fa_r_nro_nota_cred
	*/ 
	public void setFa_r_nro_nota_cred(String valor) {
		this.fa_r_nro_nota_cred = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cant_concp_c_apert
	* @return el valor de Fa_c_cant_concp_c_apert
	*/ 
	public String getFa_c_cant_concp_c_apert() {
		return fa_c_cant_concp_c_apert;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cant_concp_c_apert
	* @param valor el valor de Fa_c_cant_concp_c_apert
	*/ 
	public void setFa_c_cant_concp_c_apert(String valor) {
		this.fa_c_cant_concp_c_apert = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_marca_tarea
	* @return el valor de Fa_r_marca_tarea
	*/ 
	public String getFa_r_marca_tarea() {
		return fa_r_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_marca_tarea
	* @param valor el valor de Fa_r_marca_tarea
	*/ 
	public void setFa_r_marca_tarea(String valor) {
		this.fa_r_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_hora_ingreso
	* @return el valor de Fa_r_hora_ingreso
	*/ 
	public String getFa_r_hora_ingreso() {
		return fa_r_hora_ingreso;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_hora_ingreso
	* @param valor el valor de Fa_r_hora_ingreso
	*/ 
	public void setFa_r_hora_ingreso(String valor) {
		this.fa_r_hora_ingreso = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_marca_carta_motivo
	* @return el valor de Fa_r_marca_carta_motivo
	*/ 
	public String getFa_r_marca_carta_motivo() {
		return fa_r_marca_carta_motivo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_marca_carta_motivo
	* @param valor el valor de Fa_r_marca_carta_motivo
	*/ 
	public void setFa_r_marca_carta_motivo(String valor) {
		this.fa_r_marca_carta_motivo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_entrega
	* @return el valor de Fa_cod_entrega
	*/ 
	public String getFa_cod_entrega() {
		return fa_cod_entrega;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_entrega
	* @param valor el valor de Fa_cod_entrega
	*/ 
	public void setFa_cod_entrega(String valor) {
		this.fa_cod_entrega = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_ident_abo
	* @return el valor de Fa_r_ident_abo
	*/ 
	public String getFa_r_ident_abo() {
		return fa_r_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_ident_abo
	* @param valor el valor de Fa_r_ident_abo
	*/ 
	public void setFa_r_ident_abo(String valor) {
		this.fa_r_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_reconcil_nvo
	* @return el valor de Fa_r_fecha_reconcil_nvo
	*/ 
	public String getFa_r_fecha_reconcil_nvo() {
		return fa_r_fecha_reconcil_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_reconcil_nvo
	* @param valor el valor de Fa_r_fecha_reconcil_nvo
	*/ 
	public void setFa_r_fecha_reconcil_nvo(String valor) {
		this.fa_r_fecha_reconcil_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_reconciliacion
	* @return el valor de Fa_r_reconciliacion
	*/ 
	public String getFa_r_reconciliacion() {
		return fa_r_reconciliacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_reconciliacion
	* @param valor el valor de Fa_r_reconciliacion
	*/ 
	public void setFa_r_reconciliacion(String valor) {
		this.fa_r_reconciliacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_desimputacion
	* @return el valor de Fa_fecha_desimputacion
	*/ 
	public String getFa_fecha_desimputacion() {
		return fa_fecha_desimputacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_desimputacion
	* @param valor el valor de Fa_fecha_desimputacion
	*/ 
	public void setFa_fecha_desimputacion(String valor) {
		this.fa_fecha_desimputacion = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_5
	* @return el valor de Campo_no_usado_5
	*/ 
	public String getCampo_no_usado_5() {
		return campo_no_usado_5;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_5
	* @param valor el valor de Campo_no_usado_5
	*/ 
	public void setCampo_no_usado_5(String valor) {
		this.campo_no_usado_5 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_ident_aper_man
	* @return el valor de Fa_c_ident_aper_man
	*/ 
	public String getFa_c_ident_aper_man() {
		return fa_c_ident_aper_man;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_ident_aper_man
	* @param valor el valor de Fa_c_ident_aper_man
	*/ 
	public void setFa_c_ident_aper_man(String valor) {
		this.fa_c_ident_aper_man = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_nro_conciliacion
	* @return el valor de Fa_c_nro_conciliacion
	*/ 
	public String getFa_c_nro_conciliacion() {
		return fa_c_nro_conciliacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_nro_conciliacion
	* @param valor el valor de Fa_c_nro_conciliacion
	*/ 
	public void setFa_c_nro_conciliacion(String valor) {
		this.fa_c_nro_conciliacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_contrib
	* @return el valor de Fa_tipo_contrib
	*/ 
	public String getFa_tipo_contrib() {
		return fa_tipo_contrib;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_contrib
	* @param valor el valor de Fa_tipo_contrib
	*/ 
	public void setFa_tipo_contrib(String valor) {
		this.fa_tipo_contrib = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_importe_fuera_term
	* @return el valor de Fa_r_importe_fuera_term
	*/ 
	public String getFa_r_importe_fuera_term() {
		return fa_r_importe_fuera_term;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_importe_fuera_term
	* @param valor el valor de Fa_r_importe_fuera_term
	*/ 
	public void setFa_r_importe_fuera_term(String valor) {
		this.fa_r_importe_fuera_term = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_4
	* @return el valor de Campo_no_usado_4
	*/ 
	public String getCampo_no_usado_4() {
		return campo_no_usado_4;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_4
	* @param valor el valor de Campo_no_usado_4
	*/ 
	public void setCampo_no_usado_4(String valor) {
		this.campo_no_usado_4 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_linea
	* @return el valor de Fa_r_linea
	*/ 
	public String getFa_r_linea() {
		return fa_r_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_linea
	* @param valor el valor de Fa_r_linea
	*/ 
	public void setFa_r_linea(String valor) {
		this.fa_r_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_postal
	* @return el valor de Fa_cod_postal
	*/ 
	public String getFa_cod_postal() {
		return fa_cod_postal;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_postal
	* @param valor el valor de Fa_cod_postal
	*/ 
	public void setFa_cod_postal(String valor) {
		this.fa_cod_postal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_fact
	* @return el valor de Fa_tipo_fact
	*/ 
	public String getFa_tipo_fact() {
		return fa_tipo_fact;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_fact
	* @param valor el valor de Fa_tipo_fact
	*/ 
	public void setFa_tipo_fact(String valor) {
		this.fa_tipo_fact = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cod_banco_depos
	* @return el valor de Fa_c_cod_banco_depos
	*/ 
	public String getFa_c_cod_banco_depos() {
		return fa_c_cod_banco_depos;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cod_banco_depos
	* @param valor el valor de Fa_c_cod_banco_depos
	*/ 
	public void setFa_c_cod_banco_depos(String valor) {
		this.fa_c_cod_banco_depos = valor;
	}
	/**
	* Recupera el valor del atributo Fa_total_facturado
	* @return el valor de Fa_total_facturado
	*/ 
	public String getFa_total_facturado() {
		return fa_total_facturado;
	}
    	
	/**
	* Fija el valor del atributo Fa_total_facturado
	* @param valor el valor de Fa_total_facturado
	*/ 
	public void setFa_total_facturado(String valor) {
		this.fa_total_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_pago_orig
	* @return el valor de Fa_r_fecha_pago_orig
	*/ 
	public String getFa_r_fecha_pago_orig() {
		return fa_r_fecha_pago_orig;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_pago_orig
	* @param valor el valor de Fa_r_fecha_pago_orig
	*/ 
	public void setFa_r_fecha_pago_orig(String valor) {
		this.fa_r_fecha_pago_orig = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_usuario_modif
	* @return el valor de Fa_c_usuario_modif
	*/ 
	public String getFa_c_usuario_modif() {
		return fa_c_usuario_modif;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_usuario_modif
	* @param valor el valor de Fa_c_usuario_modif
	*/ 
	public void setFa_c_usuario_modif(String valor) {
		this.fa_c_usuario_modif = valor;
	}
	/**
	* Recupera el valor del atributo Fa_oficina
	* @return el valor de Fa_oficina
	*/ 
	public String getFa_oficina() {
		return fa_oficina;
	}
    	
	/**
	* Fija el valor del atributo Fa_oficina
	* @param valor el valor de Fa_oficina
	*/ 
	public void setFa_oficina(String valor) {
		this.fa_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_nro_boleta_deposito
	* @return el valor de Fa_c_nro_boleta_deposito
	*/ 
	public String getFa_c_nro_boleta_deposito() {
		return fa_c_nro_boleta_deposito;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_nro_boleta_deposito
	* @param valor el valor de Fa_c_nro_boleta_deposito
	*/ 
	public void setFa_c_nro_boleta_deposito(String valor) {
		this.fa_c_nro_boleta_deposito = valor;
	}
	/**
	* Recupera el valor del atributo Fa_linea
	* @return el valor de Fa_linea
	*/ 
	public String getFa_linea() {
		return fa_linea;
	}
    	
	/**
	* Fija el valor del atributo Fa_linea
	* @param valor el valor de Fa_linea
	*/ 
	public void setFa_linea(String valor) {
		this.fa_linea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_anio_lote
	* @return el valor de Fa_r_anio_lote
	*/ 
	public String getFa_r_anio_lote() {
		return fa_r_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_anio_lote
	* @param valor el valor de Fa_r_anio_lote
	*/ 
	public void setFa_r_anio_lote(String valor) {
		this.fa_r_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_codigo_movimiento
	* @return el valor de Fa_r_codigo_movimiento
	*/ 
	public String getFa_r_codigo_movimiento() {
		return fa_r_codigo_movimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_codigo_movimiento
	* @param valor el valor de Fa_r_codigo_movimiento
	*/ 
	public void setFa_r_codigo_movimiento(String valor) {
		this.fa_r_codigo_movimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_importe_cobrado
	* @return el valor de Fa_r_importe_cobrado
	*/ 
	public String getFa_r_importe_cobrado() {
		return fa_r_importe_cobrado;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_importe_cobrado
	* @param valor el valor de Fa_r_importe_cobrado
	*/ 
	public void setFa_r_importe_cobrado(String valor) {
		this.fa_r_importe_cobrado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_importe_pdte_gire
	* @return el valor de Fa_importe_pdte_gire
	*/ 
	public String getFa_importe_pdte_gire() {
		return fa_importe_pdte_gire;
	}
    	
	/**
	* Fija el valor del atributo Fa_importe_pdte_gire
	* @param valor el valor de Fa_importe_pdte_gire
	*/ 
	public void setFa_importe_pdte_gire(String valor) {
		this.fa_importe_pdte_gire = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_zona_pago_orig
	* @return el valor de Fa_r_zona_pago_orig
	*/ 
	public String getFa_r_zona_pago_orig() {
		return fa_r_zona_pago_orig;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_zona_pago_orig
	* @param valor el valor de Fa_r_zona_pago_orig
	*/ 
	public void setFa_r_zona_pago_orig(String valor) {
		this.fa_r_zona_pago_orig = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_usuario_alta
	* @return el valor de Fa_c_usuario_alta
	*/ 
	public String getFa_c_usuario_alta() {
		return fa_c_usuario_alta;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_usuario_alta
	* @param valor el valor de Fa_c_usuario_alta
	*/ 
	public void setFa_c_usuario_alta(String valor) {
		this.fa_c_usuario_alta = valor;
	}
	/**
	* Recupera el valor del atributo Fa_numero_pago
	* @return el valor de Fa_numero_pago
	*/ 
	public String getFa_numero_pago() {
		return fa_numero_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_numero_pago
	* @param valor el valor de Fa_numero_pago
	*/ 
	public void setFa_numero_pago(String valor) {
		this.fa_numero_pago = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_9
	* @return el valor de Campo_no_usado_9
	*/ 
	public String getCampo_no_usado_9() {
		return campo_no_usado_9;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_9
	* @param valor el valor de Campo_no_usado_9
	*/ 
	public void setCampo_no_usado_9(String valor) {
		this.campo_no_usado_9 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_cod_banco
	* @return el valor de Fa_cod_banco
	*/ 
	public String getFa_cod_banco() {
		return fa_cod_banco;
	}
    	
	/**
	* Fija el valor del atributo Fa_cod_banco
	* @param valor el valor de Fa_cod_banco
	*/ 
	public void setFa_cod_banco(String valor) {
		this.fa_cod_banco = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cant_reg
	* @return el valor de Fa_c_cant_reg
	*/ 
	public String getFa_c_cant_reg() {
		return fa_c_cant_reg;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cant_reg
	* @param valor el valor de Fa_c_cant_reg
	*/ 
	public void setFa_c_cant_reg(String valor) {
		this.fa_c_cant_reg = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_urb
	* @return el valor de Fa_r_urb
	*/ 
	public String getFa_r_urb() {
		return fa_r_urb;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_urb
	* @param valor el valor de Fa_r_urb
	*/ 
	public void setFa_r_urb(String valor) {
		this.fa_r_urb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_tarea
	* @return el valor de Fa_marca_tarea
	*/ 
	public String getFa_marca_tarea() {
		return fa_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_tarea
	* @param valor el valor de Fa_marca_tarea
	*/ 
	public void setFa_marca_tarea(String valor) {
		this.fa_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_codigo_iva
	* @return el valor de Fa_r_codigo_iva
	*/ 
	public String getFa_r_codigo_iva() {
		return fa_r_codigo_iva;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_codigo_iva
	* @param valor el valor de Fa_r_codigo_iva
	*/ 
	public void setFa_r_codigo_iva(String valor) {
		this.fa_r_codigo_iva = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_nro_reintegro
	* @return el valor de Fa_r_nro_reintegro
	*/ 
	public String getFa_r_nro_reintegro() {
		return fa_r_nro_reintegro;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_nro_reintegro
	* @param valor el valor de Fa_r_nro_reintegro
	*/ 
	public void setFa_r_nro_reintegro(String valor) {
		this.fa_r_nro_reintegro = valor;
	}
	/**
	* Recupera el valor del atributo Fa_tipo_contrib_nvo
	* @return el valor de Fa_tipo_contrib_nvo
	*/ 
	public String getFa_tipo_contrib_nvo() {
		return fa_tipo_contrib_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_tipo_contrib_nvo
	* @param valor el valor de Fa_tipo_contrib_nvo
	*/ 
	public void setFa_tipo_contrib_nvo(String valor) {
		this.fa_tipo_contrib_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_abonado
	* @return el valor de Fa_r_abonado
	*/ 
	public String getFa_r_abonado() {
		return fa_r_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_abonado
	* @param valor el valor de Fa_r_abonado
	*/ 
	public void setFa_r_abonado(String valor) {
		this.fa_r_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_tipo_facturacion
	* @return el valor de Fa_r_tipo_facturacion
	*/ 
	public String getFa_r_tipo_facturacion() {
		return fa_r_tipo_facturacion;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_tipo_facturacion
	* @param valor el valor de Fa_r_tipo_facturacion
	*/ 
	public void setFa_r_tipo_facturacion(String valor) {
		this.fa_r_tipo_facturacion = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_hora_baja
	* @return el valor de Fa_c_fecha_hora_baja
	*/ 
	public String getFa_c_fecha_hora_baja() {
		return fa_c_fecha_hora_baja;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_hora_baja
	* @param valor el valor de Fa_c_fecha_hora_baja
	*/ 
	public void setFa_c_fecha_hora_baja(String valor) {
		this.fa_c_fecha_hora_baja = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_numero_pago
	* @return el valor de Fa_r_numero_pago
	*/ 
	public String getFa_r_numero_pago() {
		return fa_r_numero_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_numero_pago
	* @param valor el valor de Fa_r_numero_pago
	*/ 
	public void setFa_r_numero_pago(String valor) {
		this.fa_r_numero_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_codigo_movimiento
	* @return el valor de Fa_codigo_movimiento
	*/ 
	public String getFa_codigo_movimiento() {
		return fa_codigo_movimiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_codigo_movimiento
	* @param valor el valor de Fa_codigo_movimiento
	*/ 
	public void setFa_codigo_movimiento(String valor) {
		this.fa_codigo_movimiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_anio_lote
	* @return el valor de Fa_anio_lote
	*/ 
	public String getFa_anio_lote() {
		return fa_anio_lote;
	}
    	
	/**
	* Fija el valor del atributo Fa_anio_lote
	* @param valor el valor de Fa_anio_lote
	*/ 
	public void setFa_anio_lote(String valor) {
		this.fa_anio_lote = valor;
	}
	/**
	* Recupera el valor del atributo Fa_banco_zona
	* @return el valor de Fa_banco_zona
	*/ 
	public String getFa_banco_zona() {
		return fa_banco_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_banco_zona
	* @param valor el valor de Fa_banco_zona
	*/ 
	public void setFa_banco_zona(String valor) {
		this.fa_banco_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_estado_asiento
	* @return el valor de Fa_c_estado_asiento
	*/ 
	public String getFa_c_estado_asiento() {
		return fa_c_estado_asiento;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_estado_asiento
	* @param valor el valor de Fa_c_estado_asiento
	*/ 
	public void setFa_c_estado_asiento(String valor) {
		this.fa_c_estado_asiento = valor;
	}
	/**
	* Recupera el valor del atributo Fa_abonado
	* @return el valor de Fa_abonado
	*/ 
	public String getFa_abonado() {
		return fa_abonado;
	}
    	
	/**
	* Fija el valor del atributo Fa_abonado
	* @param valor el valor de Fa_abonado
	*/ 
	public void setFa_abonado(String valor) {
		this.fa_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Campo_no_usado_6
	* @return el valor de Campo_no_usado_6
	*/ 
	public String getCampo_no_usado_6() {
		return campo_no_usado_6;
	}
    	
	/**
	* Fija el valor del atributo Campo_no_usado_6
	* @param valor el valor de Campo_no_usado_6
	*/ 
	public void setCampo_no_usado_6(String valor) {
		this.campo_no_usado_6 = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_interurb
	* @return el valor de Fa_r_interurb
	*/ 
	public String getFa_r_interurb() {
		return fa_r_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_interurb
	* @param valor el valor de Fa_r_interurb
	*/ 
	public void setFa_r_interurb(String valor) {
		this.fa_r_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_nro_copia
	* @return el valor de Fa_r_nro_copia
	*/ 
	public String getFa_r_nro_copia() {
		return fa_r_nro_copia;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_nro_copia
	* @param valor el valor de Fa_r_nro_copia
	*/ 
	public void setFa_r_nro_copia(String valor) {
		this.fa_r_nro_copia = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_pago_nvo
	* @return el valor de Fa_r_fecha_pago_nvo
	*/ 
	public String getFa_r_fecha_pago_nvo() {
		return fa_r_fecha_pago_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_pago_nvo
	* @param valor el valor de Fa_r_fecha_pago_nvo
	*/ 
	public void setFa_r_fecha_pago_nvo(String valor) {
		this.fa_r_fecha_pago_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_marca_pago
	* @return el valor de Fa_r_marca_pago
	*/ 
	public String getFa_r_marca_pago() {
		return fa_r_marca_pago;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_marca_pago
	* @param valor el valor de Fa_r_marca_pago
	*/ 
	public void setFa_r_marca_pago(String valor) {
		this.fa_r_marca_pago = valor;
	}
	/**
	* Recupera el valor del atributo Fa_zona_banco_pago_doble
	* @return el valor de Fa_zona_banco_pago_doble
	*/ 
	public String getFa_zona_banco_pago_doble() {
		return fa_zona_banco_pago_doble;
	}
    	
	/**
	* Fija el valor del atributo Fa_zona_banco_pago_doble
	* @param valor el valor de Fa_zona_banco_pago_doble
	*/ 
	public void setFa_zona_banco_pago_doble(String valor) {
		this.fa_zona_banco_pago_doble = valor;
	}
	/**
	* Recupera el valor del atributo Fa_usuario_terminal
	* @return el valor de Fa_usuario_terminal
	*/ 
	public String getFa_usuario_terminal() {
		return fa_usuario_terminal;
	}
    	
	/**
	* Fija el valor del atributo Fa_usuario_terminal
	* @param valor el valor de Fa_usuario_terminal
	*/ 
	public void setFa_usuario_terminal(String valor) {
		this.fa_usuario_terminal = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_proc_ctgo
	* @return el valor de Fa_fecha_proc_ctgo
	*/ 
	public String getFa_fecha_proc_ctgo() {
		return fa_fecha_proc_ctgo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_proc_ctgo
	* @param valor el valor de Fa_fecha_proc_ctgo
	*/ 
	public void setFa_fecha_proc_ctgo(String valor) {
		this.fa_fecha_proc_ctgo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_marca_tarea
	* @return el valor de Fa_c_marca_tarea
	*/ 
	public String getFa_c_marca_tarea() {
		return fa_c_marca_tarea;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_marca_tarea
	* @param valor el valor de Fa_c_marca_tarea
	*/ 
	public void setFa_c_marca_tarea(String valor) {
		this.fa_c_marca_tarea = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_entrega
	* @return el valor de Fa_fecha_entrega
	*/ 
	public String getFa_fecha_entrega() {
		return fa_fecha_entrega;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_entrega
	* @param valor el valor de Fa_fecha_entrega
	*/ 
	public void setFa_fecha_entrega(String valor) {
		this.fa_fecha_entrega = valor;
	}
	/**
	* Recupera el valor del atributo Fa_marca_recargo
	* @return el valor de Fa_marca_recargo
	*/ 
	public String getFa_marca_recargo() {
		return fa_marca_recargo;
	}
    	
	/**
	* Fija el valor del atributo Fa_marca_recargo
	* @param valor el valor de Fa_marca_recargo
	*/ 
	public void setFa_marca_recargo(String valor) {
		this.fa_marca_recargo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_cod_concepto
	* @return el valor de Fa_c_cod_concepto
	*/ 
	public String getFa_c_cod_concepto() {
		return fa_c_cod_concepto;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_cod_concepto
	* @param valor el valor de Fa_c_cod_concepto
	*/ 
	public void setFa_c_cod_concepto(String valor) {
		this.fa_c_cod_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Fa_interurb
	* @return el valor de Fa_interurb
	*/ 
	public String getFa_interurb() {
		return fa_interurb;
	}
    	
	/**
	* Fija el valor del atributo Fa_interurb
	* @param valor el valor de Fa_interurb
	*/ 
	public void setFa_interurb(String valor) {
		this.fa_interurb = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_zona
	* @return el valor de Fa_r_zona
	*/ 
	public String getFa_r_zona() {
		return fa_r_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_zona
	* @param valor el valor de Fa_r_zona
	*/ 
	public void setFa_r_zona(String valor) {
		this.fa_r_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_fecha_ingreso_nvo
	* @return el valor de Fa_r_fecha_ingreso_nvo
	*/ 
	public String getFa_r_fecha_ingreso_nvo() {
		return fa_r_fecha_ingreso_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_fecha_ingreso_nvo
	* @param valor el valor de Fa_r_fecha_ingreso_nvo
	*/ 
	public void setFa_r_fecha_ingreso_nvo(String valor) {
		this.fa_r_fecha_ingreso_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_hora_ingreso
	* @return el valor de Fa_hora_ingreso
	*/ 
	public String getFa_hora_ingreso() {
		return fa_hora_ingreso;
	}
    	
	/**
	* Fija el valor del atributo Fa_hora_ingreso
	* @param valor el valor de Fa_hora_ingreso
	*/ 
	public void setFa_hora_ingreso(String valor) {
		this.fa_hora_ingreso = valor;
	}
	/**
	* Recupera el valor del atributo Fa_c_fecha_env_contab_nvo
	* @return el valor de Fa_c_fecha_env_contab_nvo
	*/ 
	public String getFa_c_fecha_env_contab_nvo() {
		return fa_c_fecha_env_contab_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_c_fecha_env_contab_nvo
	* @param valor el valor de Fa_c_fecha_env_contab_nvo
	*/ 
	public void setFa_c_fecha_env_contab_nvo(String valor) {
		this.fa_c_fecha_env_contab_nvo = valor;
	}
	/**
	* Recupera el valor del atributo Fa_r_estado_real_base
	* @return el valor de Fa_r_estado_real_base
	*/ 
	public String getFa_r_estado_real_base() {
		return fa_r_estado_real_base;
	}
    	
	/**
	* Fija el valor del atributo Fa_r_estado_real_base
	* @param valor el valor de Fa_r_estado_real_base
	*/ 
	public void setFa_r_estado_real_base(String valor) {
		this.fa_r_estado_real_base = valor;
	}
	/**
	* Recupera el valor del atributo Fa_importe_iva
	* @return el valor de Fa_importe_iva
	*/ 
	public String getFa_importe_iva() {
		return fa_importe_iva;
	}
    	
	/**
	* Fija el valor del atributo Fa_importe_iva
	* @param valor el valor de Fa_importe_iva
	*/ 
	public void setFa_importe_iva(String valor) {
		this.fa_importe_iva = valor;
	}
	/**
	* Recupera el valor del atributo Fa_zona
	* @return el valor de Fa_zona
	*/ 
	public String getFa_zona() {
		return fa_zona;
	}
    	
	/**
	* Fija el valor del atributo Fa_zona
	* @param valor el valor de Fa_zona
	*/ 
	public void setFa_zona(String valor) {
		this.fa_zona = valor;
	}
	/**
	* Recupera el valor del atributo Fa_fecha_ingreso_nvo
	* @return el valor de Fa_fecha_ingreso_nvo
	*/ 
	public String getFa_fecha_ingreso_nvo() {
		return fa_fecha_ingreso_nvo;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_ingreso_nvo
	* @param valor el valor de Fa_fecha_ingreso_nvo
	*/ 
	public void setFa_fecha_ingreso_nvo(String valor) {
		this.fa_fecha_ingreso_nvo = valor;
	}
	
	
	/** Julian Manfredi
	* Recupera el valor del atributo Fa_fecha_ingreso_nvo
	* @return el valor de Fa_fecha_ingreso_nvo
	*/ 
	public String getdesc_movimiento() {
		
		String descripcionVieja;
		String descripcionNueva;
		String codigo = this.fa_codigo_movimiento;
		codigo.substring( (codigo.length()-2),codigo.length());
	
		
		if (this.fa_codigo_movimiento == "AASDRS01")
			
		{
			descripcionVieja = "DESINHIBIDO";
			descripcionNueva = "DESINHIBIDO";
		}
		else
			
		{
			descripcionVieja = "Prueba";
			
		}
		
		
		return descripcionVieja;
	}
    	
	/**
	* Fija el valor del atributo Fa_fecha_ingreso_nvo
	* @param valor el valor de Fa_fecha_ingreso_nvo
	*/ 
	public void setdesc_movimiento(String valor) {
		this.desc_movimiento = valor;
	}
		
	
	
	
	
	
}
