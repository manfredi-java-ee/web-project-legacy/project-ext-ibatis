package arg.ros.acc.forms.numeroactual;

import arg.ros.acc.forms.commons.ActionFormBase;
import arg.ros.acc.vo.numeroactual.FcOS_MISC_NUMVO;

/**
* FcOS_MISC_NUMForm.java
*
* @author accenture.rosario
* @version 1.0.0
*/
public class FcOS_MISC_NUMForm extends ActionFormBase  {

	private static final long serialVersionUID = 1L;
	private String adabas_isn;
	private String os_cod_concepto;
	private String os_marca_cl;
	private String os_descripcion;
	private String os_act_u_clase;
	private String os_num_sec_num;
	private String os_num_os_misc;
	private String os_interurb_ant;
	private String os_estado_renm;
	private String os_act_est_medidor;
	private String os_importe;
	private String os_urb_act;
	private String os_estado_misc;
	private String os_abonado;
	private String os_fecha_cambio_renm;
	private String os_facturado;
	private String os_u_medida;
	private String os_oficina;
	private String os_urb_ant;
	private String os_periodo_misc;
	private String os_fecha_expir;
	private String os_cod_mov_num;
	private String os_num_sec_misc;
	private String os_cod_mov_renm;
	private String os_linea_ant;
	private String os_ant_u_clase;
	private String os_interurb_act;
	private String os_ant_cod_clie;
	private String os_anio_misc;
	private String os_act_cod_clie;
	private String os_ident_abo;
	private String os_num_os_renm;
	private String os_num_os_num;
	private String os_num_sec_renm;
	private String os_cod_mov_misc;
	private String os_importe_mensual;
	private String os_zona;
	private String os_act_oficina;
	private String os_act_zona;
	private String os_ant_zona;
	private String os_vto_misc;
	private String os_ant_est_medidor;
	private String os_act_abonado;
	private String os_fecha_empalme;
	private String os_fecha_expir_renm;
	private String os_fecha_cambio;
	private String os_fecha_cumplim;
	private String os_ant_oficina;
	private String os_ant_abonado;
	private String os_estado_num;
	private String os_linea_act;
	private String os_fecha_bajada_misc;

	public FcOS_MISC_NUMVO getVO() {
		FcOS_MISC_NUMVO vo = new FcOS_MISC_NUMVO();
		
		vo.setAdabas_isn(Double.valueOf(this.adabas_isn));
		vo.setOs_cod_concepto(Double.valueOf(this.os_cod_concepto));
		vo.setOs_marca_cl(Double.valueOf(this.os_marca_cl));
		vo.setOs_descripcion(this.os_descripcion);
		vo.setOs_act_u_clase(this.os_act_u_clase);
		vo.setOs_num_sec_num(Double.valueOf(this.os_num_sec_num));
		vo.setOs_num_os_misc(Double.valueOf(this.os_num_os_misc));
		vo.setOs_interurb_ant(this.os_interurb_ant);
		vo.setOs_estado_renm(this.os_estado_renm);
		vo.setOs_act_est_medidor(this.os_act_est_medidor);
		vo.setOs_importe(Double.valueOf(this.os_importe));
		vo.setOs_urb_act(this.os_urb_act);
		vo.setOs_estado_misc(this.os_estado_misc);
		vo.setOs_abonado(Double.valueOf(this.os_abonado));
		vo.setOs_fecha_cambio_renm(this.getAsDate(this.os_fecha_cambio_renm));
		vo.setOs_facturado(this.os_facturado);
		vo.setOs_u_medida(this.os_u_medida);
		vo.setOs_oficina(Double.valueOf(this.os_oficina));
		vo.setOs_urb_ant(this.os_urb_ant);
		vo.setOs_periodo_misc(Double.valueOf(this.os_periodo_misc));
		vo.setOs_fecha_expir(this.getAsDate(this.os_fecha_expir));
		vo.setOs_cod_mov_num(this.os_cod_mov_num);
		vo.setOs_num_sec_misc(Double.valueOf(this.os_num_sec_misc));
		vo.setOs_cod_mov_renm(this.os_cod_mov_renm);
		vo.setOs_linea_ant(this.os_linea_ant);
		vo.setOs_ant_u_clase(this.os_ant_u_clase);
		vo.setOs_interurb_act(this.os_interurb_act);
		vo.setOs_ant_cod_clie(Double.valueOf(this.os_ant_cod_clie));
		vo.setOs_anio_misc(Double.valueOf(this.os_anio_misc));
		vo.setOs_act_cod_clie(Double.valueOf(this.os_act_cod_clie));
		vo.setOs_ident_abo(Double.valueOf(this.os_ident_abo));
		vo.setOs_num_os_renm(Double.valueOf(this.os_num_os_renm));
		vo.setOs_num_os_num(Double.valueOf(this.os_num_os_num));
		vo.setOs_num_sec_renm(Double.valueOf(this.os_num_sec_renm));
		vo.setOs_cod_mov_misc(this.os_cod_mov_misc);
		vo.setOs_importe_mensual(Double.valueOf(this.os_importe_mensual));
		vo.setOs_zona(Double.valueOf(this.os_zona));
		vo.setOs_act_oficina(Double.valueOf(this.os_act_oficina));
		vo.setOs_act_zona(Double.valueOf(this.os_act_zona));
		vo.setOs_ant_zona(Double.valueOf(this.os_ant_zona));
		vo.setOs_vto_misc(Double.valueOf(this.os_vto_misc));
		vo.setOs_ant_est_medidor(this.os_ant_est_medidor);
		vo.setOs_act_abonado(Double.valueOf(this.os_act_abonado));
		vo.setOs_fecha_empalme(this.getAsDate(this.os_fecha_empalme));
		vo.setOs_fecha_expir_renm(this.getAsDate(this.os_fecha_expir_renm));
		vo.setOs_fecha_cambio(this.getAsDate(this.os_fecha_cambio));
		vo.setOs_fecha_cumplim(this.getAsDate(this.os_fecha_cumplim));
		vo.setOs_ant_oficina(Double.valueOf(this.os_ant_oficina));
		vo.setOs_ant_abonado(Double.valueOf(this.os_ant_abonado));
		vo.setOs_estado_num(this.os_estado_num);
		vo.setOs_linea_act(this.os_linea_act);
		vo.setOs_fecha_bajada_misc(this.getAsDate(this.os_fecha_bajada_misc));
		
		return vo;
	}
	
	/**
	* Recupera el valor del atributo Adabas_isn
	* @return el valor de Adabas_isn
	*/ 
	public String getAdabas_isn() {
		return adabas_isn;
	}
    	
	/**
	* Fija el valor del atributo Adabas_isn
	* @param valor el valor de Adabas_isn
	*/ 
	public void setAdabas_isn(String valor) {
		this.adabas_isn = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_concepto
	* @return el valor de Os_cod_concepto
	*/ 
	public String getOs_cod_concepto() {
		return os_cod_concepto;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_concepto
	* @param valor el valor de Os_cod_concepto
	*/ 
	public void setOs_cod_concepto(String valor) {
		this.os_cod_concepto = valor;
	}
	/**
	* Recupera el valor del atributo Os_marca_cl
	* @return el valor de Os_marca_cl
	*/ 
	public String getOs_marca_cl() {
		return os_marca_cl;
	}
    	
	/**
	* Fija el valor del atributo Os_marca_cl
	* @param valor el valor de Os_marca_cl
	*/ 
	public void setOs_marca_cl(String valor) {
		this.os_marca_cl = valor;
	}
	/**
	* Recupera el valor del atributo Os_descripcion
	* @return el valor de Os_descripcion
	*/ 
	public String getOs_descripcion() {
		return os_descripcion;
	}
    	
	/**
	* Fija el valor del atributo Os_descripcion
	* @param valor el valor de Os_descripcion
	*/ 
	public void setOs_descripcion(String valor) {
		this.os_descripcion = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_u_clase
	* @return el valor de Os_act_u_clase
	*/ 
	public String getOs_act_u_clase() {
		return os_act_u_clase;
	}
    	
	/**
	* Fija el valor del atributo Os_act_u_clase
	* @param valor el valor de Os_act_u_clase
	*/ 
	public void setOs_act_u_clase(String valor) {
		this.os_act_u_clase = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_sec_num
	* @return el valor de Os_num_sec_num
	*/ 
	public String getOs_num_sec_num() {
		return os_num_sec_num;
	}
    	
	/**
	* Fija el valor del atributo Os_num_sec_num
	* @param valor el valor de Os_num_sec_num
	*/ 
	public void setOs_num_sec_num(String valor) {
		this.os_num_sec_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_os_misc
	* @return el valor de Os_num_os_misc
	*/ 
	public String getOs_num_os_misc() {
		return os_num_os_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_num_os_misc
	* @param valor el valor de Os_num_os_misc
	*/ 
	public void setOs_num_os_misc(String valor) {
		this.os_num_os_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_interurb_ant
	* @return el valor de Os_interurb_ant
	*/ 
	public String getOs_interurb_ant() {
		return os_interurb_ant;
	}
    	
	/**
	* Fija el valor del atributo Os_interurb_ant
	* @param valor el valor de Os_interurb_ant
	*/ 
	public void setOs_interurb_ant(String valor) {
		this.os_interurb_ant = valor;
	}
	/**
	* Recupera el valor del atributo Os_estado_renm
	* @return el valor de Os_estado_renm
	*/ 
	public String getOs_estado_renm() {
		return os_estado_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_estado_renm
	* @param valor el valor de Os_estado_renm
	*/ 
	public void setOs_estado_renm(String valor) {
		this.os_estado_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_est_medidor
	* @return el valor de Os_act_est_medidor
	*/ 
	public String getOs_act_est_medidor() {
		return os_act_est_medidor;
	}
    	
	/**
	* Fija el valor del atributo Os_act_est_medidor
	* @param valor el valor de Os_act_est_medidor
	*/ 
	public void setOs_act_est_medidor(String valor) {
		this.os_act_est_medidor = valor;
	}
	/**
	* Recupera el valor del atributo Os_importe
	* @return el valor de Os_importe
	*/ 
	public String getOs_importe() {
		return os_importe;
	}
    	
	/**
	* Fija el valor del atributo Os_importe
	* @param valor el valor de Os_importe
	*/ 
	public void setOs_importe(String valor) {
		this.os_importe = valor;
	}
	/**
	* Recupera el valor del atributo Os_urb_act
	* @return el valor de Os_urb_act
	*/ 
	public String getOs_urb_act() {
		return os_urb_act;
	}
    	
	/**
	* Fija el valor del atributo Os_urb_act
	* @param valor el valor de Os_urb_act
	*/ 
	public void setOs_urb_act(String valor) {
		this.os_urb_act = valor;
	}
	/**
	* Recupera el valor del atributo Os_estado_misc
	* @return el valor de Os_estado_misc
	*/ 
	public String getOs_estado_misc() {
		return os_estado_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_estado_misc
	* @param valor el valor de Os_estado_misc
	*/ 
	public void setOs_estado_misc(String valor) {
		this.os_estado_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_abonado
	* @return el valor de Os_abonado
	*/ 
	public String getOs_abonado() {
		return os_abonado;
	}
    	
	/**
	* Fija el valor del atributo Os_abonado
	* @param valor el valor de Os_abonado
	*/ 
	public void setOs_abonado(String valor) {
		this.os_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_cambio_renm
	* @return el valor de Os_fecha_cambio_renm
	*/ 
	public String getOs_fecha_cambio_renm() {
		return os_fecha_cambio_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_cambio_renm
	* @param valor el valor de Os_fecha_cambio_renm
	*/ 
	public void setOs_fecha_cambio_renm(String valor) {
		this.os_fecha_cambio_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_facturado
	* @return el valor de Os_facturado
	*/ 
	public String getOs_facturado() {
		return os_facturado;
	}
    	
	/**
	* Fija el valor del atributo Os_facturado
	* @param valor el valor de Os_facturado
	*/ 
	public void setOs_facturado(String valor) {
		this.os_facturado = valor;
	}
	/**
	* Recupera el valor del atributo Os_u_medida
	* @return el valor de Os_u_medida
	*/ 
	public String getOs_u_medida() {
		return os_u_medida;
	}
    	
	/**
	* Fija el valor del atributo Os_u_medida
	* @param valor el valor de Os_u_medida
	*/ 
	public void setOs_u_medida(String valor) {
		this.os_u_medida = valor;
	}
	/**
	* Recupera el valor del atributo Os_oficina
	* @return el valor de Os_oficina
	*/ 
	public String getOs_oficina() {
		return os_oficina;
	}
    	
	/**
	* Fija el valor del atributo Os_oficina
	* @param valor el valor de Os_oficina
	*/ 
	public void setOs_oficina(String valor) {
		this.os_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Os_urb_ant
	* @return el valor de Os_urb_ant
	*/ 
	public String getOs_urb_ant() {
		return os_urb_ant;
	}
    	
	/**
	* Fija el valor del atributo Os_urb_ant
	* @param valor el valor de Os_urb_ant
	*/ 
	public void setOs_urb_ant(String valor) {
		this.os_urb_ant = valor;
	}
	/**
	* Recupera el valor del atributo Os_periodo_misc
	* @return el valor de Os_periodo_misc
	*/ 
	public String getOs_periodo_misc() {
		return os_periodo_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_periodo_misc
	* @param valor el valor de Os_periodo_misc
	*/ 
	public void setOs_periodo_misc(String valor) {
		this.os_periodo_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_expir
	* @return el valor de Os_fecha_expir
	*/ 
	public String getOs_fecha_expir() {
		return os_fecha_expir;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_expir
	* @param valor el valor de Os_fecha_expir
	*/ 
	public void setOs_fecha_expir(String valor) {
		this.os_fecha_expir = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_mov_num
	* @return el valor de Os_cod_mov_num
	*/ 
	public String getOs_cod_mov_num() {
		return os_cod_mov_num;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_mov_num
	* @param valor el valor de Os_cod_mov_num
	*/ 
	public void setOs_cod_mov_num(String valor) {
		this.os_cod_mov_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_sec_misc
	* @return el valor de Os_num_sec_misc
	*/ 
	public String getOs_num_sec_misc() {
		return os_num_sec_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_num_sec_misc
	* @param valor el valor de Os_num_sec_misc
	*/ 
	public void setOs_num_sec_misc(String valor) {
		this.os_num_sec_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_mov_renm
	* @return el valor de Os_cod_mov_renm
	*/ 
	public String getOs_cod_mov_renm() {
		return os_cod_mov_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_mov_renm
	* @param valor el valor de Os_cod_mov_renm
	*/ 
	public void setOs_cod_mov_renm(String valor) {
		this.os_cod_mov_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_linea_ant
	* @return el valor de Os_linea_ant
	*/ 
	public String getOs_linea_ant() {
		return os_linea_ant;
	}
    	
	/**
	* Fija el valor del atributo Os_linea_ant
	* @param valor el valor de Os_linea_ant
	*/ 
	public void setOs_linea_ant(String valor) {
		this.os_linea_ant = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_u_clase
	* @return el valor de Os_ant_u_clase
	*/ 
	public String getOs_ant_u_clase() {
		return os_ant_u_clase;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_u_clase
	* @param valor el valor de Os_ant_u_clase
	*/ 
	public void setOs_ant_u_clase(String valor) {
		this.os_ant_u_clase = valor;
	}
	/**
	* Recupera el valor del atributo Os_interurb_act
	* @return el valor de Os_interurb_act
	*/ 
	public String getOs_interurb_act() {
		return os_interurb_act;
	}
    	
	/**
	* Fija el valor del atributo Os_interurb_act
	* @param valor el valor de Os_interurb_act
	*/ 
	public void setOs_interurb_act(String valor) {
		this.os_interurb_act = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_cod_clie
	* @return el valor de Os_ant_cod_clie
	*/ 
	public String getOs_ant_cod_clie() {
		return os_ant_cod_clie;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_cod_clie
	* @param valor el valor de Os_ant_cod_clie
	*/ 
	public void setOs_ant_cod_clie(String valor) {
		this.os_ant_cod_clie = valor;
	}
	/**
	* Recupera el valor del atributo Os_anio_misc
	* @return el valor de Os_anio_misc
	*/ 
	public String getOs_anio_misc() {
		return os_anio_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_anio_misc
	* @param valor el valor de Os_anio_misc
	*/ 
	public void setOs_anio_misc(String valor) {
		this.os_anio_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_cod_clie
	* @return el valor de Os_act_cod_clie
	*/ 
	public String getOs_act_cod_clie() {
		return os_act_cod_clie;
	}
    	
	/**
	* Fija el valor del atributo Os_act_cod_clie
	* @param valor el valor de Os_act_cod_clie
	*/ 
	public void setOs_act_cod_clie(String valor) {
		this.os_act_cod_clie = valor;
	}
	/**
	* Recupera el valor del atributo Os_ident_abo
	* @return el valor de Os_ident_abo
	*/ 
	public String getOs_ident_abo() {
		return os_ident_abo;
	}
    	
	/**
	* Fija el valor del atributo Os_ident_abo
	* @param valor el valor de Os_ident_abo
	*/ 
	public void setOs_ident_abo(String valor) {
		this.os_ident_abo = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_os_renm
	* @return el valor de Os_num_os_renm
	*/ 
	public String getOs_num_os_renm() {
		return os_num_os_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_num_os_renm
	* @param valor el valor de Os_num_os_renm
	*/ 
	public void setOs_num_os_renm(String valor) {
		this.os_num_os_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_os_num
	* @return el valor de Os_num_os_num
	*/ 
	public String getOs_num_os_num() {
		return os_num_os_num;
	}
    	
	/**
	* Fija el valor del atributo Os_num_os_num
	* @param valor el valor de Os_num_os_num
	*/ 
	public void setOs_num_os_num(String valor) {
		this.os_num_os_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_num_sec_renm
	* @return el valor de Os_num_sec_renm
	*/ 
	public String getOs_num_sec_renm() {
		return os_num_sec_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_num_sec_renm
	* @param valor el valor de Os_num_sec_renm
	*/ 
	public void setOs_num_sec_renm(String valor) {
		this.os_num_sec_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_cod_mov_misc
	* @return el valor de Os_cod_mov_misc
	*/ 
	public String getOs_cod_mov_misc() {
		return os_cod_mov_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_cod_mov_misc
	* @param valor el valor de Os_cod_mov_misc
	*/ 
	public void setOs_cod_mov_misc(String valor) {
		this.os_cod_mov_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_importe_mensual
	* @return el valor de Os_importe_mensual
	*/ 
	public String getOs_importe_mensual() {
		return os_importe_mensual;
	}
    	
	/**
	* Fija el valor del atributo Os_importe_mensual
	* @param valor el valor de Os_importe_mensual
	*/ 
	public void setOs_importe_mensual(String valor) {
		this.os_importe_mensual = valor;
	}
	/**
	* Recupera el valor del atributo Os_zona
	* @return el valor de Os_zona
	*/ 
	public String getOs_zona() {
		return os_zona;
	}
    	
	/**
	* Fija el valor del atributo Os_zona
	* @param valor el valor de Os_zona
	*/ 
	public void setOs_zona(String valor) {
		this.os_zona = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_oficina
	* @return el valor de Os_act_oficina
	*/ 
	public String getOs_act_oficina() {
		return os_act_oficina;
	}
    	
	/**
	* Fija el valor del atributo Os_act_oficina
	* @param valor el valor de Os_act_oficina
	*/ 
	public void setOs_act_oficina(String valor) {
		this.os_act_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_zona
	* @return el valor de Os_act_zona
	*/ 
	public String getOs_act_zona() {
		return os_act_zona;
	}
    	
	/**
	* Fija el valor del atributo Os_act_zona
	* @param valor el valor de Os_act_zona
	*/ 
	public void setOs_act_zona(String valor) {
		this.os_act_zona = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_zona
	* @return el valor de Os_ant_zona
	*/ 
	public String getOs_ant_zona() {
		return os_ant_zona;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_zona
	* @param valor el valor de Os_ant_zona
	*/ 
	public void setOs_ant_zona(String valor) {
		this.os_ant_zona = valor;
	}
	/**
	* Recupera el valor del atributo Os_vto_misc
	* @return el valor de Os_vto_misc
	*/ 
	public String getOs_vto_misc() {
		return os_vto_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_vto_misc
	* @param valor el valor de Os_vto_misc
	*/ 
	public void setOs_vto_misc(String valor) {
		this.os_vto_misc = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_est_medidor
	* @return el valor de Os_ant_est_medidor
	*/ 
	public String getOs_ant_est_medidor() {
		return os_ant_est_medidor;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_est_medidor
	* @param valor el valor de Os_ant_est_medidor
	*/ 
	public void setOs_ant_est_medidor(String valor) {
		this.os_ant_est_medidor = valor;
	}
	/**
	* Recupera el valor del atributo Os_act_abonado
	* @return el valor de Os_act_abonado
	*/ 
	public String getOs_act_abonado() {
		return os_act_abonado;
	}
    	
	/**
	* Fija el valor del atributo Os_act_abonado
	* @param valor el valor de Os_act_abonado
	*/ 
	public void setOs_act_abonado(String valor) {
		this.os_act_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_empalme
	* @return el valor de Os_fecha_empalme
	*/ 
	public String getOs_fecha_empalme() {
		return os_fecha_empalme;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_empalme
	* @param valor el valor de Os_fecha_empalme
	*/ 
	public void setOs_fecha_empalme(String valor) {
		this.os_fecha_empalme = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_expir_renm
	* @return el valor de Os_fecha_expir_renm
	*/ 
	public String getOs_fecha_expir_renm() {
		return os_fecha_expir_renm;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_expir_renm
	* @param valor el valor de Os_fecha_expir_renm
	*/ 
	public void setOs_fecha_expir_renm(String valor) {
		this.os_fecha_expir_renm = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_cambio
	* @return el valor de Os_fecha_cambio
	*/ 
	public String getOs_fecha_cambio() {
		return os_fecha_cambio;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_cambio
	* @param valor el valor de Os_fecha_cambio
	*/ 
	public void setOs_fecha_cambio(String valor) {
		this.os_fecha_cambio = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_cumplim
	* @return el valor de Os_fecha_cumplim
	*/ 
	public String getOs_fecha_cumplim() {
		return os_fecha_cumplim;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_cumplim
	* @param valor el valor de Os_fecha_cumplim
	*/ 
	public void setOs_fecha_cumplim(String valor) {
		this.os_fecha_cumplim = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_oficina
	* @return el valor de Os_ant_oficina
	*/ 
	public String getOs_ant_oficina() {
		return os_ant_oficina;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_oficina
	* @param valor el valor de Os_ant_oficina
	*/ 
	public void setOs_ant_oficina(String valor) {
		this.os_ant_oficina = valor;
	}
	/**
	* Recupera el valor del atributo Os_ant_abonado
	* @return el valor de Os_ant_abonado
	*/ 
	public String getOs_ant_abonado() {
		return os_ant_abonado;
	}
    	
	/**
	* Fija el valor del atributo Os_ant_abonado
	* @param valor el valor de Os_ant_abonado
	*/ 
	public void setOs_ant_abonado(String valor) {
		this.os_ant_abonado = valor;
	}
	/**
	* Recupera el valor del atributo Os_estado_num
	* @return el valor de Os_estado_num
	*/ 
	public String getOs_estado_num() {
		return os_estado_num;
	}
    	
	/**
	* Fija el valor del atributo Os_estado_num
	* @param valor el valor de Os_estado_num
	*/ 
	public void setOs_estado_num(String valor) {
		this.os_estado_num = valor;
	}
	/**
	* Recupera el valor del atributo Os_linea_act
	* @return el valor de Os_linea_act
	*/ 
	public String getOs_linea_act() {
		return os_linea_act;
	}
    	
	/**
	* Fija el valor del atributo Os_linea_act
	* @param valor el valor de Os_linea_act
	*/ 
	public void setOs_linea_act(String valor) {
		this.os_linea_act = valor;
	}
	/**
	* Recupera el valor del atributo Os_fecha_bajada_misc
	* @return el valor de Os_fecha_bajada_misc
	*/ 
	public String getOs_fecha_bajada_misc() {
		return os_fecha_bajada_misc;
	}
    	
	/**
	* Fija el valor del atributo Os_fecha_bajada_misc
	* @param valor el valor de Os_fecha_bajada_misc
	*/ 
	public void setOs_fecha_bajada_misc(String valor) {
		this.os_fecha_bajada_misc = valor;
	}
}
