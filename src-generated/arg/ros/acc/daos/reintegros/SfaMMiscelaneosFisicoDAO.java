package arg.ros.acc.daos.reintegros;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.reintegros.SfaMMiscelaneosFisicoVO;

/**
 * @author Accenture Rosario
 *
 */
public interface SfaMMiscelaneosFisicoDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param SfaMMiscelaneosFisicoVO
	 * @throws DAOException
	 */
	void insert(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException;
    
	/**
	 * @param pSfaMMiscelaneosFisicoVO
	 * @throws DAOException
	 */
	void delete(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException;
    
	/**
	 * @param pSfaMMiscelaneosFisicoVO
	 * @throws DAOException
	 */
	void update(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException;

	/**
	 * @param pSfaMMiscelaneosFisicoVO
	 * @throws DAOException
	 */
	void updateOrInsert(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
