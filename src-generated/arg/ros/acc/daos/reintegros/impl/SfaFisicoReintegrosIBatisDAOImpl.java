package arg.ros.acc.daos.reintegros.impl;

import java.util.*;

import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.reintegros.SfaFisicoReintegrosDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.reintegros.SfaFisicoReintegrosVO;
import arg.ros.acc.vo.zonaoficina.ZoNAOFICINAPFNNVO;
import arg.ros.acc.vo.reintegros.SfaMMiscelaneosFisicoVO;

import org.springframework.dao.DataAccessException;
import arg.ros.acc.commons.Constants;

/**
 * @author Accenture Rosario
 *
 */
public class SfaFisicoReintegrosIBatisDAOImpl extends IBatisBaseDAO implements SfaFisicoReintegrosDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select() throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("insertarSfaFisicoReintegros");
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECTALL_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("obtenerFiltroSfaFisicoReintegros", pFilter);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List select(int pStart, int pLimit) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerSfaFisicoReintegros", 
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFiltroSfaFisicoReintegros", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException {		
		try {
			
	//    	return getSqlMapClientTemplate().queryForList(
	//			"obtenerSfaFisicoReintegros", 
	//			pFilter,
	//			pStart,
	//			pLimit
	//		);
	    	
			List<SfaFisicoReintegrosVO> lista = getSqlMapClientTemplate().queryForList(
					"obtenerSfaFisicoReintegros", 
					pFilter,
					pStart,
					pLimit
					
			);	
			
			
			// Validacion para el campo OFICINA para REINTEGROS
			
		//	double Var_Oficina; 
		//	int VarOficinaInt;
		    java.util.Date fecha;
		//    int dia;
			
			
			
			for(SfaFisicoReintegrosVO vo : lista)
			{
					 System.out.println("objeto: "+vo);			 
					
					fecha = vo.getFA_RE_FECHAPAGO_PRESC();
					 
					if (fecha != null)
					 {	
						 vo.setPRESCRIPTO(true);
					 }
						 else
					 {	 
						 vo.setPRESCRIPTO(false);
					 }
				 	 System.out.println("objeto: "+vo);
					 
	 
			} // Fin-for
			
			return lista;
			
			
							
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void insert(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException {
		try {
			getSqlMapClientTemplate().insert(
				"insertarSfaFisicoReintegros", 
				pSfaFisicoReintegrosVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.INSERT, e);
			throw new DAOException(Constants.BBDD_ERRORS.INSERT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException {
		try {
			getSqlMapClientTemplate().delete(
				"eliminarSfaFisicoReintegros",
				pSfaFisicoReintegrosVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.DELETE, e);
			throw new DAOException(Constants.BBDD_ERRORS.DELETE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException {
		try {
			getSqlMapClientTemplate().update(
				"actualizarSfaFisicoReintegros", 
				pSfaFisicoReintegrosVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}

	
	/**
	 * {@inheritDoc}
	 */
	public void PrescribirReintegro(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException {
		try {
			getSqlMapClientTemplate().update(
				"PrescribirReintegro", 
				pSfaFisicoReintegrosVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}

	
	/**
	 * {@inheritDoc}
	 */
	public void updateOrInsert(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException {
		try {
			int rta = getSqlMapClientTemplate().update(
				"actualizarSfaFisicoReintegros", 
				pSfaFisicoReintegrosVO
			);
			if (rta == 0) this.insert(pSfaFisicoReintegrosVO);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public int count() throws DAOException {
		return this.count(new HashMap());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalSfaFisicoReintegros", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
	
	
		/**
	 * {@inheritDoc}
	 */
	public int countSinFiltro(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalSinFiltroSfaFisicoReintegros", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
}
