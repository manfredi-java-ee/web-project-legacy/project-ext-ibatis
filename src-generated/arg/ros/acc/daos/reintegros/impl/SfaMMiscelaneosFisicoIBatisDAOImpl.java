package arg.ros.acc.daos.reintegros.impl;

import java.util.*;
import java.util.Formatter;

import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.reintegros.SfaMMiscelaneosFisicoDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.reintegros.SfaMMiscelaneosFisicoVO;
//import arg.ros.acc.vo.zonaoficina.ZoNAOFICINAPFNNVO;
import org.springframework.dao.DataAccessException;
import arg.ros.acc.commons.Constants;


/**
 * @author Accenture Rosario
 *
 */
public class SfaMMiscelaneosFisicoIBatisDAOImpl extends IBatisBaseDAO implements SfaMMiscelaneosFisicoDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select() throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("insertarSfaMMiscelaneosFisico");
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECTALL_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("obtenerFiltroSfaMMiscelaneosFisico", pFilter);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List select(int pStart, int pLimit) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerSfaMMiscelaneosFisico", 
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFiltroSfaMMiscelaneosFisico", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			
		//	
		//	 getSqlMapClientTemplate().queryForList(
		//			"obtenerByNumeroFundamental", 
		//			pFilter,
		//			pStart,
		//			pLimit
		//		);
		//	
			
			 List<SfaMMiscelaneosFisicoVO> lista = getSqlMapClientTemplate().queryForList(
				"obtenerSfaMMiscelaneosFisico", 
				pFilter,
				pStart,
				pLimit
			);
			
			
			
			 // LOGICA  FOR Y SWITCH PARA MISCELANEOS
			 				   				 			      
				
			    double Estado_Bajada ;
			    int EstadoBajadaInt;
			    
			 // Declaro los contadores por cada estado de bajada
			    
			    int ContBaja_1 = 0;
			    int ContBaja_2 = 0;
			    int ContBaja_3 = 0;
			    int ContBaja_4 = 0;
			    int ContBaja_5 = 0;
			    int ContBaja_6 = 0;
			    int ContBaja_7 = 0;
			    int ContBaja_8 = 0;
			    int ContBaja_9 = 0;
			    int ContBaja_10 = 0;
			    int ContBaja_77 = 0;
			    int ContBaja_88 = 0;
			    int ContBaja_98 = 0;

		//	    int ContBaja_ERROR = 0;
			    int ContTot_Misc = 0;
			    
			    

				for(SfaMMiscelaneosFisicoVO vo : lista)
				{
					
				 System.out.println("objeto: "+vo);
				 				 

				 Estado_Bajada =  vo.getFa_m_estado_bajada();
				
				 
				 
			if (Estado_Bajada != ' ' ) {	
					  
				//Type cast double to int
			 
				 EstadoBajadaInt = (int) Estado_Bajada;
			
				 
				 switch(EstadoBajadaInt)
				 {	    			
				 case 1:    	
					 vo.setESTADO_DINAMICO("GENERADO");
					 ContBaja_1 = ContBaja_1 + 1;
             		 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_alta());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_alta());
				 break;
				 case 2:
					 vo.setESTADO_DINAMICO("AUTORI.MISC");
					 ContBaja_2 = ContBaja_2 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_envio());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_envio());					 											
				 break;
				 case 3:
					 vo.setESTADO_DINAMICO("ENV.FACTURACI");
					 ContBaja_3 = ContBaja_3 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_envio());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_envio());
				 break;
				 case 4:
					 vo.setESTADO_DINAMICO("FACTURADO");
					 ContBaja_4 = ContBaja_4 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_facturacion());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_facturacion());						
				 break;
				 case 5:
					 vo.setESTADO_DINAMICO("RECH.FACTU");
					 ContBaja_5 = ContBaja_5 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_facturacion());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_facturacion());						
				 break;
				 case 6:
					 vo.setESTADO_DINAMICO("RECH.CONCEP");
					 ContBaja_6 = ContBaja_6 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_facturacion());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_facturacion());
				 break;
				 case 7:
					 vo.setESTADO_DINAMICO("FACTU.NEGA");
					 ContBaja_7 = ContBaja_7 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_facturacion());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_facturacion());
					 break;
				 case 8:
					 vo.setESTADO_DINAMICO("ENV. ATIS FAC");
					 ContBaja_8 = ContBaja_8 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_facturacion());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_facturacion());
					 break;
				 case 9:
					 vo.setESTADO_DINAMICO("ENV.LOTE FINAL");
					 ContBaja_9 = ContBaja_9 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_baja());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_baja());
					 break;
				 case 10:
					 vo.setESTADO_DINAMICO("LINEA CERO");
					 ContBaja_10 = ContBaja_10 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_envio());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_envio());
					 break;
				 case 77:
					 vo.setESTADO_DINAMICO("AUTORI.REIN");
					 ContBaja_77 = ContBaja_77 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_alta());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_alta());
					 break;
				 case 88:
					 vo.setESTADO_DINAMICO("ANULADO REINT");
					 ContBaja_88 = ContBaja_88 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_baja());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_baja());
					 break;
				 case 98:
					 vo.setESTADO_DINAMICO("BLOQUEADO");
					 ContBaja_98 = ContBaja_98 + 1;
					 vo.setFECHA_DINAMICA(vo.getFa_m_fecha_baja());
					 vo.setUSUARIO_DINAMICO(vo.getFa_m_usuario_baja());
					 break;
				 default:
					 
	    			}
				 
				 ContTot_Misc = ContTot_Misc +1;
				 
				 System.out.println("objeto: "+vo);
				 
	}//Fin if Abonado
		
		     
				
				}// Fin For 
				
				
//			    if ( this.count() == ContBaja_1 || this.count() == ContBaja_2 || this.count() == ContBaja_3 || this.count() == ContBaja_4 
//						  || this.count() == ContBaja_5 || this.count() == ContBaja_6 || this.count() == ContBaja_7 || this.count() == ContBaja_8
//						  || this.count() == ContBaja_9 || this.count() == ContBaja_10 || this.count() == ContBaja_77 || this.count() == ContBaja_88
//						  || this.count() == ContBaja_98) 
//					    {    
//
//						 vo.setDESC_MISC_R("LOS ESTADOS DE MISCELANEOS SON DISTINTOS");
//									      			  
//					    }
			return lista;
			
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public void insert(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException {
		try {
			getSqlMapClientTemplate().insert(
				"insertarSfaMMiscelaneosFisico", 
				pSfaMMiscelaneosFisicoVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.INSERT, e);
			throw new DAOException(Constants.BBDD_ERRORS.INSERT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException {
		try {
			getSqlMapClientTemplate().delete(
				"eliminarSfaMMiscelaneosFisico",
				pSfaMMiscelaneosFisicoVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.DELETE, e);
			throw new DAOException(Constants.BBDD_ERRORS.DELETE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException {
		try {
			getSqlMapClientTemplate().update(
				"actualizarSfaMMiscelaneosFisico", 
				pSfaMMiscelaneosFisicoVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void updateOrInsert(final SfaMMiscelaneosFisicoVO pSfaMMiscelaneosFisicoVO) throws DAOException {
		try {
			int rta = getSqlMapClientTemplate().update(
				"actualizarSfaMMiscelaneosFisico", 
				pSfaMMiscelaneosFisicoVO
			);
			if (rta == 0) this.insert(pSfaMMiscelaneosFisicoVO);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public int count() throws DAOException {
		return this.count(new HashMap());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalSfaMMiscelaneosFisico", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
	
	
		/**
	 * {@inheritDoc}
	 */
	public int countSinFiltro(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalSinFiltroSfaMMiscelaneosFisico", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
}
