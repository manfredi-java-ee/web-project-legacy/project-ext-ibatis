package arg.ros.acc.daos.reintegros;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.reintegros.SfaFisicoReintegrosVO;

/**
 * @author Accenture Rosario
 *
 */
public interface SfaFisicoReintegrosDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param SfaFisicoReintegrosVO
	 * @throws DAOException
	 */
	void insert(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException;
    
	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws DAOException
	 */
	void delete(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException;
    
	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws DAOException
	 */
	void update(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException;

	
	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws DAOException
	 */
	void PrescribirReintegro(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException;

	
	
	
	
	/**
	 * @param pSfaFisicoReintegrosVO
	 * @throws DAOException
	 */
	void updateOrInsert(final SfaFisicoReintegrosVO pSfaFisicoReintegrosVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
