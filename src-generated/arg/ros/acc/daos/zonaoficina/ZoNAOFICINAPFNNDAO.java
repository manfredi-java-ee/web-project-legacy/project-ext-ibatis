package arg.ros.acc.daos.zonaoficina;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.zonaoficina.ZoNAOFICINAPFNNVO;

/**
 * @author Accenture Rosario
 *
 */
public interface ZoNAOFICINAPFNNDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	/**
	 * @param ZoNAOFICINAPFNNVO
	 * @throws DAOException
	 */
	void insert(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException;
    
	/**
	 * @param pZoNAOFICINAPFNNVO
	 * @throws DAOException
	 */
	void delete(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException;
    
	/**
	 * @param pZoNAOFICINAPFNNVO
	 * @throws DAOException
	 */
	void update(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException;

	/**
	 * @param pZoNAOFICINAPFNNVO
	 * @throws DAOException
	 */
	void updateOrInsert(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
	int countsinfiltro(final Map pFilter) throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
}
