package arg.ros.acc.daos.zonaoficina;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.zonaoficina.FeTIPOSERVVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FeTIPOSERVDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List Buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	/**
	 * @param FeTIPOSERVVO
	 * @throws DAOException
	 */
	void insert(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException;
    
	/**
	 * @param pFeTIPOSERVVO
	 * @throws DAOException
	 */
	void delete(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException;
    
	/**
	 * @param pFeTIPOSERVVO
	 * @throws DAOException
	 */
	void update(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException;

	/**
	 * @param pFeTIPOSERVVO
	 * @throws DAOException
	 */
	void updateOrInsert(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
}
