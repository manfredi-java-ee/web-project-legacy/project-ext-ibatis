package arg.ros.acc.daos.zonaoficina;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.zonaoficina.FeNROABONHABILITADOSVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FeNROABONHABILITADOSDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List selectNrosHabilitados(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param FeNROABONHABILITADOSVO
	 * @throws DAOException
	 */
	void insert(final FeNROABONHABILITADOSVO pFeNROABONHABILITADOSVO) throws DAOException;
    
	/**
	 * @param pFeNROABONHABILITADOSVO
	 * @throws DAOException
	 */
	void delete(final FeNROABONHABILITADOSVO pFeNROABONHABILITADOSVO) throws DAOException;
    
	/**
	 * @param pFeNROABONHABILITADOSVO
	 * @throws DAOException
	 */
	void update(final FeNROABONHABILITADOSVO pFeNROABONHABILITADOSVO) throws DAOException;

	/**
	 * @param pFeNROABONHABILITADOSVO
	 * @throws DAOException
	 */
	void updateOrInsert(final FeNROABONHABILITADOSVO pFeNROABONHABILITADOSVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
}
