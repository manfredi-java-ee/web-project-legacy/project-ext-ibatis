package arg.ros.acc.daos.zonaoficina.impl;

import java.util.*;

import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.zonaoficina.ZoNAOFICINAPFNNDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.zonaoficina.ZoNAOFICINAPFNNVO;
import org.springframework.dao.DataAccessException;
import arg.ros.acc.commons.Constants;

/**
 * @author Accenture Rosario
 *
 */
public class ZoNAOFICINAPFNNIBatisDAOImpl extends IBatisBaseDAO implements ZoNAOFICINAPFNNDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select() throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("insertarZoNAOFICINAPFNN");
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECTALL_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("obtenerFiltroZoNAOFICINAPFNN", pFilter);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List select(int pStart, int pLimit) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerZoNAOFICINAPFNN", 
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFiltroZoNAOFICINAPFNN", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
						
			List<ZoNAOFICINAPFNNVO> lista = getSqlMapClientTemplate().queryForList(
					"obtenerByNumeroFundamental", 
					pFilter,
					pStart,
					pLimit
				);
			
			//Le solicito a la lista que me devuelva un iterador con todos los el elementos contenidos en ella

	//		Iterator iter = lista.listIterator();
	
				
	//		while (iter.hasNext())
	//		{
	//		  System.out.println(iter.next());
	//		}
			
			// Recorror la lista de objetos 
			
			Double VarZona;
			Double VarOficina;
			
			for(ZoNAOFICINAPFNNVO vo : lista)
			{
			 System.out.println("objeto: "+vo);
			
			 		 
			 
			if(vo.getFe_zona()== 0 &&  vo.getFe_zona_baja() > 0)
			{
			
				if(vo.getFe_marca_cdn()== "*")
				{
				
					vo.setLeyenda("ZONA OFICINA DADA DE BAJA POR CBIO.DE NRO");	
				}
				else
					
				{
					vo.setLeyenda("ZONA OFICINA DADA DE BAJA");	
				}
			
			}
			else 
			{
		
				if(vo.getFe_zona()== 0 &&  vo.getFe_zona_baja_tf() > 0)
				{
				vo.setLeyenda("ZONA OFI.DADA DE BAJA POR TELEFONIA");	
				
				//Cargo la variable Zona
				
				VarZona = vo.getFe_zona_baja_tf(); 
				
				vo.setZonaVar(VarZona);	
				
							
				//Cargo la variable Oficina
				
				VarOficina =  vo.getFe_oficina_baja_tf();
				vo.setOficinaVar(VarOficina);
				
				}
				else
				{
					//Cargo la variable Zona
					
					VarZona = vo.getFe_zona(); 
					
					vo.setZonaVar(VarZona);	
					
					//Cargo la variable Oficina
					
					VarOficina =  vo.getFe_oficina();
					
					vo.setOficinaVar(VarOficina);
					
				}
				
				
			}
			
			System.out.println("objeto: "+vo);
			
			}
			
			return lista;
		
			
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	
	
	
	
	
	
	/**
	 * {@inheritDoc}
	 */
	public void insert(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException {
		try {
			getSqlMapClientTemplate().insert(
				"insertarZoNAOFICINAPFNN", 
				pZoNAOFICINAPFNNVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.INSERT, e);
			throw new DAOException(Constants.BBDD_ERRORS.INSERT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException {
		try {
			getSqlMapClientTemplate().delete(
				"eliminarZoNAOFICINAPFNN",
				pZoNAOFICINAPFNNVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.DELETE, e);
			throw new DAOException(Constants.BBDD_ERRORS.DELETE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException {
		try {
			getSqlMapClientTemplate().update(
				"actualizarZoNAOFICINAPFNN", 
				pZoNAOFICINAPFNNVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void updateOrInsert(final ZoNAOFICINAPFNNVO pZoNAOFICINAPFNNVO) throws DAOException {
		try {
			int rta = getSqlMapClientTemplate().update(
				"actualizarZoNAOFICINAPFNN", 
				pZoNAOFICINAPFNNVO
			);
			if (rta == 0) this.insert(pZoNAOFICINAPFNNVO);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public int count() throws DAOException {
		return this.count(new HashMap());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalZoNAOFICINAPFNN", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int countsinfiltro(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalZoNAOFICINAPFNNSinfiltro", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
	
	
	
	
	
	
	
}
