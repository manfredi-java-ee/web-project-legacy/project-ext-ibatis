package arg.ros.acc.daos.zonaoficina.impl;

import java.util.*;

import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.zonaoficina.FeTIPOSERVDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.zonaoficina.FeTIPOSERVVO;
import org.springframework.dao.DataAccessException;
import arg.ros.acc.commons.Constants;

/**
 * @author Accenture Rosario
 *
 */
public class FeTIPOSERVIBatisDAOImpl extends IBatisBaseDAO implements FeTIPOSERVDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select() throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("insertarFeTIPOSERV");
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECTALL_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("obtenerFiltroFeTIPOSERV", pFilter);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List select(int pStart, int pLimit) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFeTIPOSERV", 
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFiltroFeTIPOSERV", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List Buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFeTIPOSERV", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public void insert(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException {
		try {
			getSqlMapClientTemplate().insert(
				"insertarFeTIPOSERV", 
				pFeTIPOSERVVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.INSERT, e);
			throw new DAOException(Constants.BBDD_ERRORS.INSERT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException {
		try {
			getSqlMapClientTemplate().delete(
				"eliminarFeTIPOSERV",
				pFeTIPOSERVVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.DELETE, e);
			throw new DAOException(Constants.BBDD_ERRORS.DELETE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException {
		try {
			getSqlMapClientTemplate().update(
				"actualizarFeTIPOSERV", 
				pFeTIPOSERVVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void updateOrInsert(final FeTIPOSERVVO pFeTIPOSERVVO) throws DAOException {
		try {
			int rta = getSqlMapClientTemplate().update(
				"actualizarFeTIPOSERV", 
				pFeTIPOSERVVO
			);
			if (rta == 0) this.insert(pFeTIPOSERVVO);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public int count() throws DAOException {
		return this.count(new HashMap());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalFeTIPOSERV", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
}
