package arg.ros.acc.daos.feriado;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.feriado.FeriadoDetalleVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FeriadoDetalleDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param FeriadoDetalleVO
	 * @throws DAOException
	 */
	void insert(final FeriadoDetalleVO pFeriadoDetalleVO) throws DAOException;
    
	/**
	 * @param pFeriadoDetalleVO
	 * @throws DAOException
	 */
	void delete(final FeriadoDetalleVO pFeriadoDetalleVO) throws DAOException;
    
	/**
	 * @param pFeriadoDetalleVO
	 * @throws DAOException
	 */
	void update(final FeriadoDetalleVO pFeriadoDetalleVO) throws DAOException;

	/**
	 * @param pFeriadoDetalleVO
	 * @throws DAOException
	 */
	void updateOrInsert(final FeriadoDetalleVO pFeriadoDetalleVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
