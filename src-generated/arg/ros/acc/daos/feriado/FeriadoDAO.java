package arg.ros.acc.daos.feriado;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.feriado.FeriadoVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FeriadoDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param FeriadoVO
	 * @throws DAOException
	 */
	void insert(final FeriadoVO pFeriadoVO) throws DAOException;
    
	/**
	 * @param pFeriadoVO
	 * @throws DAOException
	 */
	void delete(final FeriadoVO pFeriadoVO) throws DAOException;
    
	/**
	 * @param pFeriadoVO
	 * @throws DAOException
	 */
	void update(final FeriadoVO pFeriadoVO) throws DAOException;

	/**
	 * @param pFeriadoVO
	 * @throws DAOException
	 */
	void updateOrInsert(final FeriadoVO pFeriadoVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
