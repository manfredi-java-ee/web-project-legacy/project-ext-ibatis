package arg.ros.acc.daos.localidad;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.localidad.LocalidadVO;

/**
 * @author Accenture Rosario
 *
 */
public interface LocalidadDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param LocalidadVO
	 * @throws DAOException
	 */
	void insert(final LocalidadVO pLocalidadVO) throws DAOException;
    
	/**
	 * @param pLocalidadVO
	 * @throws DAOException
	 */
	void delete(final LocalidadVO pLocalidadVO) throws DAOException;
    
	/**
	 * @param pLocalidadVO
	 * @throws DAOException
	 */
	void update(final LocalidadVO pLocalidadVO) throws DAOException;

	/**
	 * @param pLocalidadVO
	 * @throws DAOException
	 */
	void updateOrInsert(final LocalidadVO pLocalidadVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
