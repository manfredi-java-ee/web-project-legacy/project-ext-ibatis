package arg.ros.acc.daos.inhibiciones;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.inhibiciones.MovimientoFisicoVO;

/**
 * @author Accenture Rosario
 *
 */
public interface MovimientoFisicoDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param MovimientoFisicoVO
	 * @throws DAOException
	 */
	void insert(final MovimientoFisicoVO pMovimientoFisicoVO) throws DAOException;
    
	/**
	 * @param pMovimientoFisicoVO
	 * @throws DAOException
	 */
	void delete(final MovimientoFisicoVO pMovimientoFisicoVO) throws DAOException;
    
	/**
	 * @param pMovimientoFisicoVO
	 * @throws DAOException
	 */
	void update(final MovimientoFisicoVO pMovimientoFisicoVO) throws DAOException;

	/**
	 * @param pMovimientoFisicoVO
	 * @throws DAOException
	 */
	void updateOrInsert(final MovimientoFisicoVO pMovimientoFisicoVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
}
