package arg.ros.acc.daos.numeroactual;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.numeroactual.FcOS_MISC_NUMVO;

/**
 * @author Accenture Rosario
 *
 */
public interface FcOS_MISC_NUMDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;

	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param FcOS_MISC_NUMVO
	 * @throws DAOException
	 */
	void insert(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws DAOException;
    
	/**
	 * @param pFcOS_MISC_NUMVO
	 * @throws DAOException
	 */
	void delete(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws DAOException;
    
	/**
	 * @param pFcOS_MISC_NUMVO
	 * @throws DAOException
	 */
	void update(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws DAOException;

	/**
	 * @param pFcOS_MISC_NUMVO
	 * @throws DAOException
	 */
	void updateOrInsert(final FcOS_MISC_NUMVO pFcOS_MISC_NUMVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;

    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
}
