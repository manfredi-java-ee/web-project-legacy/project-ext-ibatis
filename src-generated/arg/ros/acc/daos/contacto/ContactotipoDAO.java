package arg.ros.acc.daos.contacto;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.contacto.ContactotipoVO;

/**
 * @author Accenture Rosario
 *
 */
public interface ContactotipoDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param ContactotipoVO
	 * @throws DAOException
	 */
	void insert(final ContactotipoVO pContactotipoVO) throws DAOException;
    
	/**
	 * @param pContactotipoVO
	 * @throws DAOException
	 */
	void delete(final ContactotipoVO pContactotipoVO) throws DAOException;
    
	/**
	 * @param pContactotipoVO
	 * @throws DAOException
	 */
	void update(final ContactotipoVO pContactotipoVO) throws DAOException;

	/**
	 * @param pContactotipoVO
	 * @throws DAOException
	 */
	void updateOrInsert(final ContactotipoVO pContactotipoVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
