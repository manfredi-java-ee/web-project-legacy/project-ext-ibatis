package arg.ros.acc.daos.contacto;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.contacto.ContactoVO;

/**
 * @author Accenture Rosario
 *
 */
public interface ContactoDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param ContactoVO
	 * @throws DAOException
	 */
	void insert(final ContactoVO pContactoVO) throws DAOException;
    
	/**
	 * @param pContactoVO
	 * @throws DAOException
	 */
	void delete(final ContactoVO pContactoVO) throws DAOException;
    
	/**
	 * @param pContactoVO
	 * @throws DAOException
	 */
	void update(final ContactoVO pContactoVO) throws DAOException;

	/**
	 * @param pContactoVO
	 * @throws DAOException
	 */
	void updateOrInsert(final ContactoVO pContactoVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
