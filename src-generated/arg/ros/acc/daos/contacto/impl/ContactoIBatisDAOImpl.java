package arg.ros.acc.daos.contacto.impl;

import java.util.*;

import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.contacto.ContactoDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.contacto.ContactoVO;
import org.springframework.dao.DataAccessException;
import arg.ros.acc.commons.Constants;

/**
 * @author Accenture Rosario
 *
 */
public class ContactoIBatisDAOImpl extends IBatisBaseDAO implements ContactoDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select() throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("insertarContacto");
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECTALL_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("obtenerFiltroContacto", pFilter);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List select(int pStart, int pLimit) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerContacto", 
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFiltroContacto", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerContacto", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public void insert(final ContactoVO pContactoVO) throws DAOException {
		try {
			getSqlMapClientTemplate().insert(
				"insertarContacto", 
				pContactoVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.INSERT, e);
			throw new DAOException(Constants.BBDD_ERRORS.INSERT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(final ContactoVO pContactoVO) throws DAOException {
		try {
			getSqlMapClientTemplate().delete(
				"eliminarContacto",
				pContactoVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.DELETE, e);
			throw new DAOException(Constants.BBDD_ERRORS.DELETE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(final ContactoVO pContactoVO) throws DAOException {
		try {
			getSqlMapClientTemplate().update(
				"actualizarContacto", 
				pContactoVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void updateOrInsert(final ContactoVO pContactoVO) throws DAOException {
		try {
			int rta = getSqlMapClientTemplate().update(
				"actualizarContacto", 
				pContactoVO
			);
			if (rta == 0) this.insert(pContactoVO);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public int count() throws DAOException {
		return this.count(new HashMap());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalContacto", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
	
	
		/**
	 * {@inheritDoc}
	 */
	public int countSinFiltro(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalSinFiltroContacto", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
}
