package arg.ros.acc.daos.profesional.impl;

import java.util.*;

import arg.ros.acc.daos.commons.IBatisBaseDAO;
import arg.ros.acc.daos.profesional.ProfesionalDAO;
import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.profesional.ProfesionalVO;
import org.springframework.dao.DataAccessException;
import arg.ros.acc.commons.Constants;

/**
 * @author Accenture Rosario
 *
 */
public class ProfesionalIBatisDAOImpl extends IBatisBaseDAO implements ProfesionalDAO {

	/**
	 * {@inheritDoc}
	 */
	public List select() throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("insertarProfesional");
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECTALL, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECTALL_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList("obtenerFiltroProfesional", pFilter);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List select(int pStart, int pLimit) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerProfesional", 
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerFiltroProfesional", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException {
		try {
			return getSqlMapClientTemplate().queryForList(
				"obtenerProfesional", 
				pFilter,
				pStart,
				pLimit
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.SELECT, e);
			throw new DAOException(Constants.BBDD_ERRORS.SELECT_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public void insert(final ProfesionalVO pProfesionalVO) throws DAOException {
		try {
			getSqlMapClientTemplate().insert(
				"insertarProfesional", 
				pProfesionalVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.INSERT, e);
			throw new DAOException(Constants.BBDD_ERRORS.INSERT_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(final ProfesionalVO pProfesionalVO) throws DAOException {
		try {
			getSqlMapClientTemplate().delete(
				"eliminarProfesional",
				pProfesionalVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.DELETE, e);
			throw new DAOException(Constants.BBDD_ERRORS.DELETE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void update(final ProfesionalVO pProfesionalVO) throws DAOException {
		try {
			getSqlMapClientTemplate().update(
				"actualizarProfesional", 
				pProfesionalVO
			);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void updateOrInsert(final ProfesionalVO pProfesionalVO) throws DAOException {
		try {
			int rta = getSqlMapClientTemplate().update(
				"actualizarProfesional", 
				pProfesionalVO
			);
			if (rta == 0) this.insert(pProfesionalVO);
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.UPDATE, e);
			throw new DAOException(Constants.BBDD_ERRORS.UPDATE_MSG, e);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	public int count() throws DAOException {
		return this.count(new HashMap());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int count(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalProfesional", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
	
	
		/**
	 * {@inheritDoc}
	 */
	public int countSinFiltro(final Map pProperties) throws DAOException {
		try {
			final Integer total = (Integer) getSqlMapClientTemplate().queryForObject("totalSinFiltroProfesional", pProperties);
			return total.intValue();
		} catch (DataAccessException e) {
			this.logger.ErrorBBDD(this, Constants.OPERATIONS.COUNT, e);
			throw new DAOException(Constants.BBDD_ERRORS.COUNT_MSG, e);
		}
	}
}
