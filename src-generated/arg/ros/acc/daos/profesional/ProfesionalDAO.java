package arg.ros.acc.daos.profesional;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.profesional.ProfesionalVO;

/**
 * @author Accenture Rosario
 *
 */
public interface ProfesionalDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param ProfesionalVO
	 * @throws DAOException
	 */
	void insert(final ProfesionalVO pProfesionalVO) throws DAOException;
    
	/**
	 * @param pProfesionalVO
	 * @throws DAOException
	 */
	void delete(final ProfesionalVO pProfesionalVO) throws DAOException;
    
	/**
	 * @param pProfesionalVO
	 * @throws DAOException
	 */
	void update(final ProfesionalVO pProfesionalVO) throws DAOException;

	/**
	 * @param pProfesionalVO
	 * @throws DAOException
	 */
	void updateOrInsert(final ProfesionalVO pProfesionalVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
