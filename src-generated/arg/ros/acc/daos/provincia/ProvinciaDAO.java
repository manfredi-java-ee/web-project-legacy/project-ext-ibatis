package arg.ros.acc.daos.provincia;

import java.util.*;

import arg.ros.acc.exceptions.DAOException;
import arg.ros.acc.vo.provincia.ProvinciaVO;

/**
 * @author Accenture Rosario
 *
 */
public interface ProvinciaDAO {

	/**
	 * @return
	 * @throws DAOException
	 */
	List select() throws DAOException;

	/**
	 * @param pFilter
	 * @return
	 * @throws DAOException
	 */
	List select(final Map pFilter) throws DAOException;

	/**
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(int pStart, int pLimit) throws DAOException;
    
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List select(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	/**
	 * @param pFilter
	 * @param pStart
	 * @param pLimit
	 * @return
	 * @throws DAOException
	 */
	List buscar(final int pStart, final int pLimit, final Map pFilter) throws DAOException;
	
	
	
	/**
	 * @param ProvinciaVO
	 * @throws DAOException
	 */
	void insert(final ProvinciaVO pProvinciaVO) throws DAOException;
    
	/**
	 * @param pProvinciaVO
	 * @throws DAOException
	 */
	void delete(final ProvinciaVO pProvinciaVO) throws DAOException;
    
	/**
	 * @param pProvinciaVO
	 * @throws DAOException
	 */
	void update(final ProvinciaVO pProvinciaVO) throws DAOException;

	/**
	 * @param pProvinciaVO
	 * @throws DAOException
	 */
	void updateOrInsert(final ProvinciaVO pProvinciaVO) throws DAOException;
	
	/**
	 * @return
	 * @throws DAOException
	 */
	int count() throws DAOException;
	
    /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int count(final Map pFilter) throws DAOException;
    
    
    
     /**
     * @param pFilter
     * @return
     * @throws DAOException
     */
    int countSinFiltro(final Map pFilter) throws DAOException;
    
    
    
}
